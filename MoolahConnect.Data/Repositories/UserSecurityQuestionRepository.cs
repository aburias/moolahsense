﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IUserSecurityQuestion : IRepository<UserSecurityQuestion>
    {
        IEnumerable<UserSecurityQuestion> GetQuestionsByUserId(long userId);
    }
    public class UserSecurityQuestionRepository : Repository<UserSecurityQuestion>, IUserSecurityQuestion
    {
        public UserSecurityQuestionRepository(IDatabaseFactory context) : base(context)
        {
        }

        public IEnumerable<UserSecurityQuestion> GetQuestionsByUserId(long userId)
        {
            return Find(usq => usq.User_ID == userId);
        }
    }
}
