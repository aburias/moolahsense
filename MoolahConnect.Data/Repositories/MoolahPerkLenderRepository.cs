﻿using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IMoolahPerkLenderRepository : IRepository<MoolahPerkLender>
    {
    }

    public class MoolahPerkLenderRepository : Repository<MoolahPerkLender>, IMoolahPerkLenderRepository
    {
        public MoolahPerkLenderRepository(IDatabaseFactory factory) : base(factory)
        {
        }
    }
}