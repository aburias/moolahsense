﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IMoolahCoreRepository : IRepository<MoolahCore>
    {
        void CreateOrUpdate(MoolahCore moolah);
    }
   public class MoolahCoreRepository:Repository<MoolahCore>,IMoolahCoreRepository
   {
       public MoolahCoreRepository(IDatabaseFactory factory) : base(factory) { }
       public void CreateOrUpdate(MoolahCore moolah)
       {
           var model = this.Find(a=> a.MoolahCoreId == moolah.MoolahCoreId).FirstOrDefault();
           if (model == null)
           {
               this.Create(moolah);
           }
           else
           {
               var objContext = ((IObjectContextAdapter)_context).ObjectContext;
               objContext.Detach(model);
               this.Update(moolah);
           }
       }
   }
}
