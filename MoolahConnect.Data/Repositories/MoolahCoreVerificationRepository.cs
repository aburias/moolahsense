﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public class MoolahCoreVerificationRepository : Repository<MoolahCoreVerification>, IMoolahCoreVerificationRepository
    {
        public MoolahCoreVerificationRepository(IDatabaseFactory factory)
            : base(factory)
        {
        }
    }

    public interface IMoolahCoreVerificationRepository : IRepository<MoolahCoreVerification>
    {
 
    }
}