﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IKnowledgeAssesmentParentRepository : IRepository<KnowledgeAssessmentParent>
    {
        
    }
    public class KnowledgeAssesmentParentRepository : Repository<KnowledgeAssessmentParent>, IKnowledgeAssesmentParentRepository
    {
        public KnowledgeAssesmentParentRepository(IDatabaseFactory context) : base(context)
        {
        }
    }
}
