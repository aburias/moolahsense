﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IUserRepository : IRepository<User>
    {
        User GetByUsername(string username);
    }
   public class UserRepository:Repository<User>,IUserRepository
   {
       public UserRepository(IDatabaseFactory factory)
            : base(factory)
        {
         
        }

       public User GetByUsername(string username)
       {
           return Find(u => u.UserName.Equals(username, StringComparison.CurrentCultureIgnoreCase)).SingleOrDefault();
       }
   }
}
