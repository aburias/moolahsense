﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq.Expressions;
using System.Reflection;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface ILoanRequestRepository : IRepository<LoanRequest>
    {
        void Update(LoanRequest loanRequest, params Expression<Func<LoanRequest, object>>[] modifiedProperties);

    }

    public class LoanRequestRepository : Repository<LoanRequest>, ILoanRequestRepository
    {
        public LoanRequestRepository(IDatabaseFactory factory) : base(factory)
        {

        }

        public virtual void Update(LoanRequest loanRequest, params Expression<Func<LoanRequest, object>>[] modifiedProperties)
        {
            base.Update(loanRequest);
            var contextEntry = _context.Entry(loanRequest);

            foreach (var propname in typeof(LoanRequest).GetProperties(BindingFlags.Instance | BindingFlags.Public))
            {
                try
                {
                    contextEntry.Property(propname.Name).IsModified = false;
                }
                catch
                {
                    continue;
                }

            }
            foreach (var prop in modifiedProperties)
                contextEntry.Property(prop).IsModified = true;
           

        }
    }
}