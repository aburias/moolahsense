﻿using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface ILoanPurposeRepository : IRepository<LoanPurpose>
    {
    }

    public class LoanPurposeRepository : Repository<LoanPurpose>, ILoanPurposeRepository
    {
        public LoanPurposeRepository(IDatabaseFactory factory) : base(factory)
        {
        }
    }
}