﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public abstract class Repository<TEntity>  where TEntity : class
    {
        protected DbContext _context;
        private DbSet<TEntity> _set;

        public Repository(IDatabaseFactory context)
        {
            _context = context.GetContext();
            _set = _context.Set<TEntity>();
        }

       

        public virtual void Create(TEntity entity)
        {
            _set.Add(entity);
        }

        public virtual TEntity GetById(long id, List<string> childEntities = null)
        {
            var set = _set.AsQueryable();
            if (childEntities != null) childEntities.ForEach(ce => set = set.Include(ce));
            return (set as DbSet<TEntity>).Find(id);
        }

        public virtual IQueryable<TEntity> GetAll(List<string> childEntities = null)
        {
            var set = _set.AsQueryable();
            if (childEntities != null) childEntities.ForEach(ce => set = set.Include(ce));
            return set;
        }

        public virtual IQueryable<TEntity> Find(Func<TEntity, bool> query, List<string> childEntities = null)
        {
            var set = _set.AsQueryable();
          if (childEntities != null) childEntities.ForEach(ce => set = set.Include(ce));
            return set.Where(query).AsQueryable();
        }

        public virtual void Update(TEntity entity)
        {
            _context.Entry(entity).State = EntityState.Modified;
        }
        
      
        public virtual void Delete(long id)
        {
            _set.Remove(GetById(id));
        }
      
       
    }
}
