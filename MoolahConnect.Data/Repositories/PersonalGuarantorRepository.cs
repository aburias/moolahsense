﻿using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IPersonalGuarantorRepository : IRepository<PersonalGuaranteeInfo>
    {
    }

    public class PersonalGuarantorRepository : Repository<PersonalGuaranteeInfo>, IPersonalGuarantorRepository
    {
        public PersonalGuarantorRepository(IDatabaseFactory factory) : base(factory)
        {
        }
    }
}