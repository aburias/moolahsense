﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IRepaymentMessageRepository : IRepository<RepaymentMessage>
    {
 
    }

    public class RepaymentMessageRepository : Repository<RepaymentMessage>, IRepaymentMessageRepository
    {
        public RepaymentMessageRepository(IDatabaseFactory factory)
            : base(factory)
        {
        }
    }
}
