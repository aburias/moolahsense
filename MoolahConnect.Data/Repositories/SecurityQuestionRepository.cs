﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface ISecurityQuestionRepository : IRepository<SecurityQuestion>
    {
        IEnumerable<SecurityQuestion> GetByStatus(bool status);
    }
    public class SecurityQuestionRepository : Repository<SecurityQuestion>, ISecurityQuestionRepository
    {
        public SecurityQuestionRepository(IDatabaseFactory context) : base(context)
        {
        }

        public IEnumerable<SecurityQuestion> GetByStatus(bool status)
        {
            return Find(sq => sq.Status == status);
        }
    }
}
