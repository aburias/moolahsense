﻿using System.Linq;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IAccountDetailRepository : IRepository<AccountDetail>
    {
        AccountDetail GetByUserId(long userId);
    }
    public class AccountDetailRepository : Repository<AccountDetail>, IAccountDetailRepository
    {
        public AccountDetailRepository(IDatabaseFactory factory)
            : base(factory)
        {
        }

        public AccountDetail GetByUserId(long userId)
        {
            return Find(ad => ad.User_ID == userId).SingleOrDefault();
        }
    }
}