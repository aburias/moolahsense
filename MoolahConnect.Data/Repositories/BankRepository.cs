﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Repositories
{
    public interface IBankRepository : IRepository<Bank>
    {
        string GetBankNameById(int bankId);
        IEnumerable<Bank> GetActiveBanks();
    }
    public class BankRepository : Repository<Bank>, IBankRepository
    {
        public BankRepository(IDatabaseFactory context) : base(context)
        {
        }

        public string GetBankNameById(int bankId)
        {
            var bank = GetById(bankId);
            return bank != null ? bank.BankName : string.Empty;
        }

        public IEnumerable<Bank> GetActiveBanks()
        {
            return Find(b => b.Isactive.HasValue && b.Isactive.Value);
        }
    }
}
