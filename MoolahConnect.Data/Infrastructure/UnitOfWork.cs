﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Databases;
using MoolahConnect.Data.Interfaces;

namespace MoolahConnect.Data.Infrastructure
{
  public  class UnitOfWork:IUnitOfWork
    {
        private readonly IDatabaseFactory databaseFactory;
        private MoolahConnectDbContext dataContext;

      public UnitOfWork(IDatabaseFactory databaseFactory)
      {
          this.databaseFactory = databaseFactory;
      }

      protected MoolahConnectDbContext DataContext
      {
          get { return dataContext ?? (dataContext = databaseFactory.GetContext()); }
      }
      public void Commit()
      {
          DataContext.SaveChanges();
      }
    }
}
