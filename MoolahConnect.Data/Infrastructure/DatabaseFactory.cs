﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Databases;

namespace MoolahConnect.Data.Infrastructure
{
    public interface IDatabaseFactory : IDisposable
    {
        MoolahConnectDbContext GetContext();
    }

    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private MoolahConnectDbContext dataContext;

        public MoolahConnectDbContext GetContext()
        {
            return dataContext ?? (dataContext = new MoolahConnectDbContext());
        }

        protected override void DisposeCore()
        {
            if (dataContext != null)
                dataContext.Dispose();
        }
    }
}
