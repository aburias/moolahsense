﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Interfaces
{
    public interface IRepository<TEntity> where TEntity : class
    {
        void Create(TEntity entity);
        TEntity GetById(long id, List<string> childEntities = null);
        IQueryable<TEntity> GetAll(List<string> childEntities = null);
        IQueryable<TEntity> Find(Func<TEntity, bool> query, List<string> childEntities = null);
        void Update(TEntity entity);
        void Delete(long id);
        
    }
}