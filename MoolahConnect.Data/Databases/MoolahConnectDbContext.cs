using System.Data.Entity;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Data.Databases
{
    public class MoolahConnectDbContext : DbContext
    {
        public MoolahConnectDbContext()
            : base("name=MoolahConnectDbContext")
        {
            this.Configuration.ProxyCreationEnabled = false; 
        }

        public virtual DbSet<AspnetApplication> AspnetApplications { get; set; }
        public virtual DbSet<AspnetMembership> AspnetMemberships { get; set; }
        public virtual DbSet<AspnetProfile> AspnetProfiles { get; set; }
        public virtual DbSet<AspnetRole> AspnetRoles { get; set; }
        public virtual DbSet<AspnetSchemaVersion> AspnetSchemaVersions { get; set; }
        public virtual DbSet<AspnetUser> AspnetUsers { get; set; }
        public virtual DbSet<AspnetUsersInRole> AspnetUsersInRoles { get; set; }
        public virtual DbSet<AccountAdjustment> AccountAdjustments { get; set; }
        public virtual DbSet<AccountDetail> AccountDetails { get; set; }
        public virtual DbSet<AdminActionLog> AdminActionLogs { get; set; }
        public virtual DbSet<AuditLog> AuditLogs { get; set; }
        public virtual DbSet<AuditTrial> AuditTrials { get; set; }
        public virtual DbSet<Balance> Balances { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<Comment> Comments { get; set; }
        public virtual DbSet<Country> Countries { get; set; }
        public virtual DbSet<ErrorLog> ErrorLogs { get; set; }
        public virtual DbSet<File> Files { get; set; }
        public virtual DbSet<GlobalVariable> GlobalVariables { get; set; }
        public virtual DbSet<KnowledgeAssessmentOption> KnowledgeAssessmentOptions { get; set; }
        public virtual DbSet<KnowledgeAssessmentParent> KnowledgeAssessmentParents { get; set; }
        public virtual DbSet<KnowledgeAssessmentUserDetail> KnowledgeAssessmentUserDetails { get; set; }
        public virtual DbSet<LatefeeDetail> LatefeeDetails { get; set; }
        public virtual DbSet<LoanAmortization> LoanAmortizationd { get; set; }
        public virtual DbSet<LoanContract> LoanContracts { get; set; }
        public virtual DbSet<LoanFundsDetail> LoanFundsDetails { get; set; }
        public virtual DbSet<Loanoffer> Loanoffers { get; set; }
        public virtual DbSet<LoanPayment> LoanPayments { get; set; }
        public virtual DbSet<LoanPurpose> LoanPurposes { get; set; }
        public virtual DbSet<LoanRequestInvestorMessage> LoanRequestInvestorMessages { get; set; }
        public virtual DbSet<LoanRequestReference> LoanRequestReferences { get; set; }
        public virtual DbSet<LoanRequest> LoanRequests { get; set; }
        public virtual DbSet<LoanTransaction> LoanTransactions { get; set; }
        public virtual DbSet<MoolahCore> MoolahCores { get; set; }
        public virtual DbSet<MoolahCoreVerification> MoolahCoreVerifications { get; set; }
        public virtual DbSet<MoolahPeri> MoolahPeris { get; set; }
        public virtual DbSet<MoolahPeriVerification> MoolahPeriVerifications { get; set; }
        public virtual DbSet<MoolahPerkLender> tbl_MoolahPerksForLenders { get; set; }
        public virtual DbSet<NatureofBusiness> NatureofBusinesses { get; set; }
        public virtual DbSet<OutStandingLitigation> OutStandingLitigations { get; set; }
        public virtual DbSet<PersonalGuaranteeInfo> PersonalGuaranteeInfos { get; set; }
        public virtual DbSet<PersonalGuranteeVerification> PersonalGuranteeVerifications { get; set; }
        public virtual DbSet<SecurityQuestion> SecurityQuestions { get; set; }
        public virtual DbSet<UserSecurityQuestion> SecurityQuestionsForUsers { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<UserVerification> UserVerifications { get; set; }
        public virtual DbSet<WatchList> WatchList { get; set; }
        public virtual DbSet<WithdrawAdminNote> WithDrawAdminNotes { get; set; }
        public virtual DbSet<WithdrawMoney> WithdrawMoneys { get; set; }
        public virtual DbSet<VerificationToken> VerificationTokens { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.Title)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.AccountType)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.Nationality)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.Gender)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.ResidentialAddress)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.PostalCode)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.OptionalContactnumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.Occupation)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.IncomeRange)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BankBranch)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.AccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.AccountName)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.LevelofEducation)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.FinancialInvestmentProducts)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.MoolahConnectInvestment)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.ReactionOndefaulted)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.RoleJobTitle)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BusinessName)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BusinessOrganisation)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.RegisteredAddress)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BusinessMailingAddress)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.Main_OfficeContactnumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.Mobilenumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.IC_CompanyRegistrationNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.NRIC_Number)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.PassportNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.SiccCode)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BranchName)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BranchNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AccountDetail>()
                .Property(e => e.BankNumber)
                .IsUnicode(false);

            modelBuilder.Entity<AuditTrial>()
                .HasOptional(e => e.tbl_AuditTrial1)
                .WithRequired(e => e.tbl_AuditTrial2);

            modelBuilder.Entity<Bank>()
                .Property(e => e.BankName)
                .IsUnicode(false);

            modelBuilder.Entity<Bank>()
                .HasMany(e => e.tbl_AccountDetails)
                .WithOptional(e => e.Bank)
                .HasForeignKey(e => e.Bank_Id);

            modelBuilder.Entity<Country>()
                .Property(e => e.ISO2)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CountryName)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.LongCountryName)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.ISO3)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.NumCode)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.UNMemberState)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CallingCode)
                .IsUnicode(false);

            modelBuilder.Entity<Country>()
                .Property(e => e.CCTLD)
                .IsUnicode(false);

            modelBuilder.Entity<KnowledgeAssessmentOption>()
                .Property(e => e.Optiontext)
                .IsUnicode(false);

            modelBuilder.Entity<KnowledgeAssessmentOption>()
                .Property(e => e.Optiontype)
                .IsUnicode(false);

            modelBuilder.Entity<KnowledgeAssessmentOption>()
                .Property(e => e.url)
                .IsUnicode(false);

            modelBuilder.Entity<KnowledgeAssessmentParent>()
                .Property(e => e.Parenttext)
                .IsUnicode(false);

            modelBuilder.Entity<KnowledgeAssessmentParent>()
                .Property(e => e.InvType)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<KnowledgeAssessmentParent>()
                .HasMany(e => e.tbl_Knowledgeassessment_OptionsList)
                .WithOptional(e => e.tbl_Knowledgeassessment_Parentlist)
                .HasForeignKey(e => e.Parent_Id);

            modelBuilder.Entity<KnowledgeAssessmentUserDetail>()
                .Property(e => e.Option_Value)
                .IsUnicode(false);

            modelBuilder.Entity<LoanAmortization>()
                .Property(e => e.Interest)
                .HasPrecision(18, 10);

            modelBuilder.Entity<LoanAmortization>()
                .Property(e => e.Principal)
                .HasPrecision(18, 10);

            modelBuilder.Entity<LoanAmortization>()
                .HasMany(e => e.tbl_AccountAdjustment)
                .WithRequired(e => e.tbl_LoanAmortization)
                .HasForeignKey(e => e.RepaymentID)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoanAmortization>()
                .HasMany(e => e.tbl_LoanPayments)
                .WithOptional(e => e.tbl_LoanAmortization)
                .HasForeignKey(e => e.RepaymentID);

            modelBuilder.Entity<LoanContract>()
                .Property(e => e.Reference)
                .IsUnicode(false);

            modelBuilder.Entity<Loanoffer>()
                .Property(e => e.ReasonToReject)
                .IsUnicode(false);

            modelBuilder.Entity<LoanPurpose>()
                .Property(e => e.PurposeName)
                .IsUnicode(false);

            modelBuilder.Entity<LoanPurpose>()
                .HasMany(e => e.tbl_LoanRequests)
                .WithOptional(e => e.LoanPurposes)
                .HasForeignKey(e => e.LoanPurpose_Id);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.Terms)
                .IsUnicode(false);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.Question)
                .IsUnicode(false);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.PaymentTerms)
                .IsUnicode(false);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.MoolahFees)
                .HasPrecision(18, 0);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.MoolahSenseComments)
                .IsUnicode(false);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.ResonReject)
                .IsUnicode(false);

            modelBuilder.Entity<LoanRequest>()
                .Property(e => e.Headline)
                .IsUnicode(false);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.Comments)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_Id);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.LoanAmortizations)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.LoanRequest_ID);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.LoanFundsDetails)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_Id);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.Loanoffers)
                .WithRequired(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.LoanRequest_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.LoanRequestInvestorMessages)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.LoanRequestID);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.MoolahCores)
                .WithOptional(e => e.LoanRequest)
                .HasForeignKey(e => e.LoanRequest_Id);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.LoanTransactions)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_Id);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.MoolahCoreVerifications)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_ID);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.MoolahPeris)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_ID);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.MoolahPeriVerifications)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_ID);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.RepaymentMessages)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.LoanRequestId);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.PersonalGuaranteeInfos)
                .WithOptional(e => e.LoanRequest)
                .HasForeignKey(e => e.Request_Id);

            modelBuilder.Entity<LoanRequest>()
                .HasMany(e => e.WatchList)
                .WithOptional(e => e.tbl_LoanRequests)
                .HasForeignKey(e => e.Request_ID);

            modelBuilder.Entity<LoanTransaction>()
                .Property(e => e.PaymentTerms)
                .IsUnicode(false);

            modelBuilder.Entity<LoanTransaction>()
                .Property(e => e.PaymentMode)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahCore>()
                .Property(e => e.LatestYear)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahCore>()
                .Property(e => e.PreiviousYear)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahCore>()
                .HasMany(e => e.OutStandingLitigations)
                .WithOptional(e => e.tbl_MoolahCore)
                .HasForeignKey(e => e.MoolahCore_Id);

            modelBuilder.Entity<MoolahPeri>()
                .Property(e => e.BusinessAwards)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahPeri>()
                .Property(e => e.Accreditation)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahPeri>()
                .Property(e => e.MTA)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahPeri>()
                .Property(e => e.DigitalFootPrint)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahPeri>()
                .Property(e => e.CommunityInitiative)
                .IsUnicode(false);

            modelBuilder.Entity<MoolahPerkLender>()
                .Property(e => e.MoolahPerk)
                .IsUnicode(false);

            modelBuilder.Entity<NatureofBusiness>()
                .Property(e => e.NatureofBusinessName)
                .IsUnicode(false);

            modelBuilder.Entity<NatureofBusiness>()
                .HasMany(e => e.tbl_AccountDetails)
                .WithOptional(e => e.NatureofBusiness)
                .HasForeignKey(e => e.Natureofbusiness_Id);

            modelBuilder.Entity<OutStandingLitigation>()
                .Property(e => e.RepaymentPeriod)
                .IsUnicode(false);

            modelBuilder.Entity<OutStandingLitigation>()
                .Property(e => e.LitigationType)
                .IsFixedLength();

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.NameAsinNRIC)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.Designation)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.NRIC_Passport)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.PassportNumber)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.ResidentialAddress)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.Telephone)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuaranteeInfo>()
                .Property(e => e.Postalcode)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.NameAsinNRICVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.DesignationVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.NRIC_PassportVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.PassportNumber)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.ResidentialAddressVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.TelephoneVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.EmailVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.ReasonsVerification)
                .IsUnicode(false);

            modelBuilder.Entity<PersonalGuranteeVerification>()
                .Property(e => e.PostalcodeVerification)
                .IsUnicode(false);

            modelBuilder.Entity<SecurityQuestion>()
                .Property(e => e.QuestionName)
                .IsUnicode(false);

            modelBuilder.Entity<UserSecurityQuestion>()
                .Property(e => e.Answer)
                .IsUnicode(false);

            modelBuilder.Entity<UserSecurityQuestion>()
                .Property(e => e.IsVerified)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserName)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.UserRole)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.InvestorType)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .Property(e => e.AdminVerification)
                .IsUnicode(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.AccountDetails)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_Balances)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_Comments)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.Commentor_Id);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_Knowledgeassessmentdetails_ForUser)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_LoanFundsDetails)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_Loanoffers)
                .WithRequired(e => e.tbl_Users)
                .HasForeignKey(e => e.Investor_Id)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_LoanRequests)
                .WithOptional(e => e.Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_LoanTransactions)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_LoanTransactions1)
                .WithOptional(e => e.tbl_Users1)
                .HasForeignKey(e => e.RefUserID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_MoolahCore)
                .WithOptional(e => e.User)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_MoolahPeri)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_OutStandingLitigation)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_SecurityQuestionsForUsers)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<User>()
                .HasMany(e => e.tbl_WithDrawMoney)
                .WithOptional(e => e.tbl_Users)
                .HasForeignKey(e => e.User_ID);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.TitleVerification)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.FirstName)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.LastName)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.DisplayName)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.RoleOrJobTitle)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BussinessName)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BussinessRegistrationNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BussinessOrganisation)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.NatureofBussiness)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.DateofIncorporation)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.RegisteredAddress)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BussinessMailingAddress)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.DirectLine)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.Mobile)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.OptinalContact)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.Bank)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BankAccountNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BankAccountName)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.PaidUpCapital)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.NumOfEmployees)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.NricNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.PassportNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.SiccCode)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BranchName)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BranchNumber)
                .IsUnicode(false);

            modelBuilder.Entity<UserVerification>()
                .Property(e => e.BankNumber)
                .IsUnicode(false);

            modelBuilder.Entity<WithdrawMoney>()
                .Property(e => e.WithDrawAmount)
                .HasPrecision(16, 2);

            modelBuilder.Entity<WithdrawMoney>()
                .HasMany(e => e.tbl_WithDrawAdminNotes)
                .WithRequired(e => e.tbl_WithDrawMoney)
                .HasForeignKey(e => e.WithDraw_ID)
                .WillCascadeOnDelete(false);
        }
    }
}
