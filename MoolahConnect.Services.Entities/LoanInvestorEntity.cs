﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class LoanInvestorEntity
    {
        public int OfferId { get; set; }
        public long LoanId { get; set; }
        public long RepaymentId { get; set; }
        public string DisplayName { get; set; }

        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public decimal? Amount { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? RepaymentAmount { get; set; }
        public double LatePayment { get; set; }
        public long UserId { get; set; }

        public string Username { get; set; }

        public string Tenor { get; set; }
        public decimal? Rate { get; set; }
    }
}
