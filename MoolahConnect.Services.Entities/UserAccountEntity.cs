﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class UserAccountEntity
    {
        public long UserID { get; set; }
        public string UserRole { get; set; }
        public string NRIC { get; set; }
        public string BusinessRegNumber { get; set; }

        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
