﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class BorrowerLoanContractModel
    {
        public BorrowerLoanContractModel()
        {
            AcceptedOffers = new List<Lender>();
        }

        public Guid UserId { get; set; }
        public long UserIDInt { get; set; }
        public string Username { get; set; }
        public long LoanRequestId { get; set; }
        public string Name { get; set; }
        public string RegistrationNumber { get; set; }
        public DateTime AgreementDate { get; set; }
        public decimal Amount { get; set; }
        public string Tenure { get; set; }
        public int NumberOfAcceptedOffers { get; set; }
        public decimal AcceptedRate { get; set; }
        public decimal EffectiveRate { get; set; }
        public decimal TotalInterestPayable { get; set; }
        public decimal MonthlyRepaymentAmount { get; set; }

        public string RegisteredAddress { get; set; }
        public string DirectorPartnetName { get; set; }

        public List<Lender> AcceptedOffers { get; set; }
    }

    public class Lender
    {
        public int Id { get; set; }
        public long LoanRequestId { get; set; }
        public string Username { get; set; }
        public long UserID { get; set; }
        public string ContractIdentifier { get; set; }
        public string Name { get; set; }
        public string IdNumber { get; set; }
        public decimal Amount { get; set; }
        public double RepaymentAmount { get; set; }
    }
}
