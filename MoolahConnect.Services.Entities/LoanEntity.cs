﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class LoanEntity
    {
        public long RequestId { get; set; }
        public decimal? Amount { get; set; }
        public decimal? FinalAmount { get; set; }
        public string Terms { get; set; }
        public decimal? EIR { get; set; }
        public decimal? Rate { get; set; }
        public bool? IsotherLoanType { get; set; }
        public string Question { get; set; }
        public bool? IsPersonalGuarantee { get; set; }
        public long? User_ID { get; set; }
        public bool IsApproved { get; set; }
        public string PaymentTerms { get; set; }
        public string VideoDescription { get; set; }
        public int LoanStatus { get; set; }
        public DateTime? DateCreated { get; set; }
        public int? LoanPurpose_Id { get; set; }
        public DateTime? AcceptedDate { get; set; }
        public decimal? MoolahFees { get; set; }

        public decimal? PrincipalPaidTillDate { get; set; }
        public decimal? InterestPaidTillDate { get; set; }

        public long AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public decimal? AmountPaidTillDate { get; set; }

        public int MonthsTillMaturity { get; set; }

        public string FinalIssuerContractName { get; set; }
        public string FinalGuaranorContractName { get; set; }
    }

    
}
