﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class InvestorLoanContractModel
    {
        public string Name { get; set; }
        public string LenderIDNumber { get; set; }
        public string BorrowerName { get; set; }
        public string BorrowerRegNumber { get; set; }
        public string ContractRef { get; set; }
        public DateTime AgreementDate { get; set; }
        public decimal PrincipalAmount { get; set; }
        public string Tenure { get; set; }
        public decimal Rate { get; set; }
        public decimal TotalInterestReceivable { get; set; }
        public decimal MonthlyRepaymentAmount { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal LoanAcceptedRate { get; set; }
        public int AcceptedOffersCount { get; set; }
        public string ContractId { get; set; }

        public Guid UserId { get; set; }
    }
}
