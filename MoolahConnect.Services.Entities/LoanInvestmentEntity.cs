﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class LoanInvestmentEntity
    {
        public LoanInvestmentEntity()
        {
            offerIds = new List<int>();
        }

        public int OfferID { get; set; }
        public long LoanRequestId { get; set; }
        public string CompanyName { get; set; }
        public string Tenor { get; set; }
        public DateTime? LoanStartDate { get; set; }
        public decimal? LoanAmount { get; set; }
        public decimal? OffersAmount { get; set; }
        public decimal? Rate { get; set; }
        public int MonthsTillMaturity { get; set; }
        public decimal? PrincipalRecevived { get; set; }
        public decimal? InterestReceived { get; set; }
        public int LoanRequestStatus { get; set; }

        public int LatestRepaymentStatus { get; set; }

        public List<int> offerIds { get; set; }
    }
}
