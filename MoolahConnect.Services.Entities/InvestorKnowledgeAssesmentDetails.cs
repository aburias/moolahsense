﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class InvestorKnowledgeAssesmentDetails
    {
        public long Userid { get; set; }
        public decimal? OfferAmount { get; set; }
        public decimal? OfferedRate { get; set; }
        public decimal? AcceptedAmount { get; set; }
        public decimal? AcceptedRate { get; set; }
        public string OfferStatus { get; set; }
        public DateTime CreatedDate { get; set; }

        public string AccountNumber { get; set; }
        public string Username { get; set; }
        public string DisplayName { get; set; }
        public string Fullname { get; set; }
        public string Education { get; set; }
        public string InvestedFinancialProductsInd { get; set; }
        public string InvestedFinancialProductsCop { get; set; }
        public string Employment { get; set; }
        public string Occupation { get; set; }
        public string Other { get; set; }

    }
}
