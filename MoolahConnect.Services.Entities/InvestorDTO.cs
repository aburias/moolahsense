﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
  public  class InvestorDTO
    {
      public long UserID { get; set; }
      public string UserName { get; set; }
      public string UserRole { get; set; }
      public bool? IsActive { get; set; }
      public DateTime? DateCreated { get; set; }
      public string InvestorType { get; set; }
      public string AdminVerification { get; set; }
      public bool? IsSubmitted { get; set; }
      public bool? IsEmailVerified { get; set; }
      public string FirstName { get; set; }
      public string LastName { get; set; }
      public string DisplayName { get; set; }
      public string AccountType { get; set; }
      public string Nationality { get; set; }
      public DateTime? DateofBirth { get; set; }
      public string Gender { get; set; }
      public string MainOfficeContactnumber { get; set; }
      public decimal CurrentBalance { get; set; }
    }
}
