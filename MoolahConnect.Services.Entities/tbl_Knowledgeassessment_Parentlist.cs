//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoolahConnect.Services.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Knowledgeassessment_Parentlist
    {
        public tbl_Knowledgeassessment_Parentlist()
        {
            this.tbl_Knowledgeassessment_OptionsList = new HashSet<tbl_Knowledgeassessment_OptionsList>();
        }
    
        public int ParentId { get; set; }
        public string Parenttext { get; set; }
        public Nullable<int> Priority { get; set; }
        public Nullable<bool> Isactive { get; set; }
        public string InvType { get; set; }
    
        public virtual ICollection<tbl_Knowledgeassessment_OptionsList> tbl_Knowledgeassessment_OptionsList { get; set; }
    }
}
