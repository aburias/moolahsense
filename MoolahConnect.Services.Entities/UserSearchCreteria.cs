﻿namespace MoolahConnect.Services.Entities
{
    public class UserSearchCreteria
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string BusinessName { get; set; }
        public string NRIC { get; set; }
        public string FullName { get; set; }
    }
}