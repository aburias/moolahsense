﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class RepaymentEntity
    {
        public DateTime DueDate { get; set; }
        public double StartBalance { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double Payment { get; set; }
        public double EndBalance { get; set; }
        public int Status { get; set; }
    }
}
