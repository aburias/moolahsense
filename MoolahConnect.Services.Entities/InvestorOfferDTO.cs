﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class InvestorOfferDTO
    {
        public string UserName { get; set; }
        public string DisplayName { get; set; }
        public string NRICName { get; set; }
        public string FullName { get; set; }
        public string AccountNumber { get; set; }
        public int OfferID { get; set; }
        public string NRICOrPassportNumber { get; set; }
        public double OfferedAmount { get; set; }
        public double OfferedRate { get; set; }
        public double AcceptedAmount { get; set; }
        public double AcceptedRate { get; set; }
        public double RepaymentAmountPortion { get; set; }
        public double TotalInterest { get; set; }
        public DateTime DateCreated { get; set; }

        public double AmountRefunded { get; set; }
        public string OfferStatus { get; set; }
        public decimal OfferedAmountDecimal { get; set; }
        public decimal OfferedRatedDecimal { get; set; }
        public DateTime? WithdrawalDate { get; set; }
    }
}
