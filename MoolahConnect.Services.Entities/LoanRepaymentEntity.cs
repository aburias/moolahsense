﻿using MoolahConnect.Util.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Entities
{
    public class LoanRepaymentEntity : LoanEntity
    {
        public long RepaymentId { get; set; }
        public decimal? Repayment { get; set; }
        public RepaymentPayStatus Status { get; set; }
        public string Comment { get; set; }
        public DateTime? RepaymentDate { get; set; }

        public DateTime? LateRepaymentStartDate { get; set; }
        public int LateRepaymentPayStatus { get; set; }
        public DateTime? LateRepaymentPaidDate { get; set; }
        public decimal? LateInterest { get; set; }

        public decimal? PaidAmount { get; set; }
        public decimal? LateFees { get; set; }
    }
}
