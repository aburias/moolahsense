//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MoolahConnect.Services.Entities
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_Users
    {
        public tbl_Users()
        {
            this.tbl_AccountDetails = new HashSet<tbl_AccountDetails>();
            this.tbl_Balances = new HashSet<tbl_Balances>();
            this.tbl_Comments = new HashSet<tbl_Comments>();
            this.tbl_Knowledgeassessmentdetails_ForUser = new HashSet<tbl_Knowledgeassessmentdetails_ForUser>();
            this.tbl_LoanFundsDetails = new HashSet<tbl_LoanFundsDetails>();
            this.tbl_Loanoffers = new HashSet<tbl_Loanoffers>();
            this.tbl_LoanRequests = new HashSet<tbl_LoanRequests>();
            this.tbl_LoanTransactions = new HashSet<tbl_LoanTransactions>();
            this.tbl_LoanTransactions1 = new HashSet<tbl_LoanTransactions>();
            this.tbl_MoolahCore = new HashSet<tbl_MoolahCore>();
            this.tbl_MoolahPeri = new HashSet<tbl_MoolahPeri>();
            this.tbl_OutStandingLitigation = new HashSet<tbl_OutStandingLitigation>();
            this.tbl_SecurityQuestionsForUsers = new HashSet<tbl_SecurityQuestionsForUsers>();
            this.tbl_WithDrawMoney = new HashSet<tbl_WithDrawMoney>();
        }
    
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public Nullable<bool> Isactive { get; set; }
        public string UserRole { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<bool> SubscribeforUpdates { get; set; }
        public string InvestorType { get; set; }
        public string AdminVerification { get; set; }
        public Nullable<bool> IsSubmitted { get; set; }
        public System.Guid AspnetUserId { get; set; }
        public Nullable<bool> IsEmailVerified { get; set; }
        public string DigitalSignature { get; set; }
    
        public virtual ICollection<tbl_AccountDetails> tbl_AccountDetails { get; set; }
        public virtual ICollection<tbl_Balances> tbl_Balances { get; set; }
        public virtual ICollection<tbl_Comments> tbl_Comments { get; set; }
        public virtual ICollection<tbl_Knowledgeassessmentdetails_ForUser> tbl_Knowledgeassessmentdetails_ForUser { get; set; }
        public virtual ICollection<tbl_LoanFundsDetails> tbl_LoanFundsDetails { get; set; }
        public virtual ICollection<tbl_Loanoffers> tbl_Loanoffers { get; set; }
        public virtual ICollection<tbl_LoanRequests> tbl_LoanRequests { get; set; }
        public virtual ICollection<tbl_LoanTransactions> tbl_LoanTransactions { get; set; }
        public virtual ICollection<tbl_LoanTransactions> tbl_LoanTransactions1 { get; set; }
        public virtual ICollection<tbl_MoolahCore> tbl_MoolahCore { get; set; }
        public virtual ICollection<tbl_MoolahPeri> tbl_MoolahPeri { get; set; }
        public virtual ICollection<tbl_OutStandingLitigation> tbl_OutStandingLitigation { get; set; }
        public virtual ICollection<tbl_SecurityQuestionsForUsers> tbl_SecurityQuestionsForUsers { get; set; }
        public virtual ICollection<tbl_WithDrawMoney> tbl_WithDrawMoney { get; set; }
    }
}
