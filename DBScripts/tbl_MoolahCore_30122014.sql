alter table [dbo].[tbl_MoolahCore] add EBITDA_LY decimal(18,2) null
alter table [dbo].[tbl_MoolahCore] add EBITDA_PY decimal(18,2) null

alter table [dbo].[tbl_MoolahCore] add DebtEBITDARatio_LY decimal(18,2) null
alter table [dbo].[tbl_MoolahCore] add DebtEBITDARatio_PY decimal(18,2) null

alter table [dbo].[tbl_MoolahCoreVerification] add EBITDA_LY_Verification int null
alter table [dbo].[tbl_MoolahCoreVerification] add EBITDA_PY_Verification int null

alter table [dbo].[tbl_MoolahCoreVerification] add DebtEBITDARatio_LY_Verification int null
alter table [dbo].[tbl_MoolahCoreVerification] add DebtEBITDARatio_PY_Verification int null

