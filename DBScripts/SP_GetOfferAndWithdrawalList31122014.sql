-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE SP_GetOfferAndWithdrawalList
(@loanRequestId bigint)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

select 
u.UserName,
a.DisplayName,
'FullName' = a.FirstName,
AccountNumber = 'INV' + CAST(u.UserID AS nvarchar),
lo.offerId as 'OfferID',
lo.DateCreated,
lo.OfferedAmount as 'OfferedAmountDecimal',
lo.OfferedRate as 'OfferedRatedDecimal',
'OfferStatus' =
case 
when lo.OfferStatus = 0 then 'Pending'
when lo.OfferStatus = 1 then 'Accepted'
when lo.OfferStatus = 2 then 'Rejected'
when lo.OfferStatus = 3 then 'Withdrawn'
end,
case when lo.OfferStatus = 3 then offerlog.TimeStamp
end as 'WithdrawalDate'
from tbl_Loanoffers lo
left join tbl_Users u on u.UserID = lo.Investor_Id
left join tbl_AccountDetails a on a.User_ID = u.UserID
left join tbl_Balances b on b.User_ID = u.UserID
left join tbl_LoanOffersLog offerlog on offerlog.OfferID = lo.OfferId
where lo.LoanRequest_Id = @loanRequestId
order by lo.OfferStatus, lo.OfferedRate
END
GO
