GO

/****** Object:  StoredProcedure [dbo].[SP_GetInvestorKnowledgeAssesmentDetails]    Script Date: 12/30/2014 11:13:34 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


ALTER PROCEDURE [dbo].[SP_GetInvestorKnowledgeAssesmentDetails] 
(
@requestID bigint output
)
AS
BEGIN
	select 
Userid,OfferAmount,OfferedRate,AcceptedAmount,AcceptedRate,[OfferStatus], AccountNumber,
Username,DisplayName,Fullname,
[1] as Education,
[2] as 'InvestedFinancialProductsInd',
[7] as 'InvestedFinancialProductsCop',
[10] as 'Employment',
[11] as 'Occupation',
[12] as 'Other'
from (
select ko.Parent_Id as pid,
STUFF(
         (SELECT DISTINCT ',' + Optiontext
          FROM tbl_Knowledgeassessment_OptionsList _ko
		  join tbl_Knowledgeassessmentdetails_ForUser _ku on _ko.Optionid = _ku.Option_Id
          WHERE Parent_Id = ko.Parent_Id and _ku.User_ID = ku.User_ID
          FOR XML PATH (''))
          , 1, 1, '')  AS Value,
		  'INV' +cast (ku.User_ID as nvarchar) as AccountNumber,
		  max(u.username) as username,
		  max(a.displayname) as displayname,
		  max(a.firstname) as fullname,
		  max(u.userid) as UserID,
		  max(lo.OfferedAmount) as OfferAmount,
		  max(lo.OfferedRate) as OfferedRate,
		  max(lo.AcceptedAmount) as AcceptedAmount,
		  max(lo.AcceptedRate) as AcceptedRate,
		  'OfferStatus' =
case 
when max(lo.OfferStatus) = 0 then 'Pending'
when max(lo.OfferStatus) = 1 then 'Accepted'
when max(lo.OfferStatus) = 2 then 'Rejected'
when max(lo.OfferStatus) = 3 then 'Withdrawn'
end
from tbl_Loanoffers lo
left join tbl_Knowledgeassessmentdetails_ForUser  ku on ku.User_ID = lo.Investor_Id
join tbl_Knowledgeassessment_OptionsList ko on ku.option_id = ko.optionid
left join tbl_Users u on u.userid = ku.user_id
left join tbl_AccountDetails a on a.user_id = ku.user_id
where u.UserRole = 'Investor' and lo.LoanRequest_Id = @requestID
group by lo.OfferId,ko.Parent_Id, ku.User_ID
) as s
PIVOT (
max(Value) 
FOR pid IN ([1],[2],[7],[10],[11],[12])) AS p

END


GO


