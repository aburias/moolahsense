GO
/****** Object:  StoredProcedure [dbo].[USP_tbl_LoanRequest_SaveLoanrequestdetails]    Script Date: 12/17/2014 11:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USP_tbl_LoanRequest_SaveLoanrequestdetails]    
(        
@Amount decimal(18,2),    
@Terms varchar(10),    
@Rate decimal(18, 2),    
@Ispersonalguarantee bit,    
@LoanpurposeID bigint,    
@LoanPurposeDescription nvarchar(max),    
@User_Id bigint,    
@requestID bigint output  ,  
@requestIDold int=0,
@VideoDiscription nvarchar(max),  ---------------------- 0 for insert else for update  
@EIR decimal(18, 2),
@DetailedCompanyPro nvarchar(max),
@SummaryCompanyPro nvarchar(max),
@NumberOfEmployees int,
@Headline varchar(200)
)        
AS        
begin        
declare @Pdoc int     
        -- ///////////////////////////////  save Loan request info into tbl_Loanrequest table  ///////////////////////      
   update tbl_LoanRequests set Amount=@Amount,Terms=@Terms,Rate=@Rate,EIR=@EIR,
   IsPersonalGuarantee=@Ispersonalguarantee,LoanPurpose_Id=@LoanpurposeID ,
   VideoDescription = @VideoDiscription ,DetailedCompanyPro = @DetailedCompanyPro,
    SummaryCompanyPro = @SummaryCompanyPro, NumberOfEmployees = @NumberOfEmployees ,
	 LoanPurposeDescription = @LoanPurposeDescription, Headline = @Headline
   where RequestId=@requestIDold  
   select @requestID=@requestIDold  
       
   --///////////////////////////////// save personal guarantee info into tbl_PersonalGuaranteeInfo if ispersonalguarantee is true ///////////////////////    
  
End   


