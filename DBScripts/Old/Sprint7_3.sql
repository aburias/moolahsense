alter table tbl_loanpayments add LateFees decimal(18,0) null

alter table [dbo].[tbl_LoanAmortization] alter column LateInterest decimal (18,2) null

/****** Object:  Table [dbo].[tbl_AccountAdjustment]    Script Date: 9/28/2014 4:40:01 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_AccountAdjustment](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[TotalReceivable] [decimal](18, 2) NOT NULL,
	[Difference] [decimal](18, 2) NOT NULL,
	[RepaymentID] [bigint] NOT NULL,
	[Adjusted] [bit] NULL,
	[AdjustedDate] [datetime] NULL,
	[TimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_AccountAdjustment] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[tbl_AccountAdjustment]  WITH CHECK ADD  CONSTRAINT [FK_tbl_AccountAdjustment_tbl_LoanAmortization] FOREIGN KEY([RepaymentID])
REFERENCES [dbo].[tbl_LoanAmortization] ([AmortizationID])
GO

ALTER TABLE [dbo].[tbl_AccountAdjustment] CHECK CONSTRAINT [FK_tbl_AccountAdjustment_tbl_LoanAmortization]
GO

alter table tbl_loanpayments alter column  Amount decimal (18,2) null
alter table tbl_loanpayments alter column  LateFees decimal (18,2) null

EXEC sp_RENAME 'tbl_balances.Amount' , 'ActualAmount', 'COLUMN'
alter table tbl_balances add LedgerAmount decimal(18,2) 