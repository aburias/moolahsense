update tbl_Knowledgeassessment_OptionsList set Optiontext = 'I understand that in making a decision to fund a note, I will rely on my own examination of the issuer. MoolahSense neither recommends nor endorses any issuer and/or <br>associated note requests.'
where Optionid = 13

delete tbl_Knowledgeassessment_OptionsList where optionid = 16

update tbl_Knowledgeassessment_Parentlist set Parenttext = 'By checking the following boxes, I have read, understood, accept and agree to be bound by (i) the Terms of Service (ii) the Code of Conduct and (iii) the Privacy and Data Protection Policy of MoolahSense'
where [ParentId] = 13