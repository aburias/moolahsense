ALTER TABLE tbl_loanrequests ALTER COLUMN LoanPurposeDescription NVARCHAR(500) NULL

UPDATE tbl_loanRequests SET LoanPurposeDescription= 'test'

ALTER TABLE tbl_loanrequests ALTER COLUMN LoanPurposeDescription NVARCHAR(500) NOT NULL

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LoanRequests_QuestionsAnswerbyUser]') AND type in (N'U'))
BEGIN
DROP TABLE tbl_LoanRequests_QuestionsAnswerbyUser
END

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_QuestionsForSelectedLoanpurpose]') AND type in (N'U'))
BEGIN
DROP TABLE tbl_QuestionsForSelectedLoanpurpose
END

IF  EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS 
WHERE TABLE_NAME = 'tbl_accountdetails' AND  COLUMN_NAME = 'EmailAddress')
BEGIN
ALTER TABLE tbl_accountdetails DROP COLUMN EmailAddress
END

ALTER TABLE tbl_auditlog ALTER COLUMN Reference NVARCHAR(30) NOT NULL

ALTER TABLE tbl_LoanTransactions ALTER COLUMN Reference NVARCHAR(30) NOT NULL

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LoanContracts]') AND type in (N'U'))
BEGIN
DROP TABLE tbl_LoanContracts
END

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_LoanContracts]') AND type in (N'U'))
BEGIN
DROP TABLE [tbl_LoanContracts]
END

CREATE TABLE [dbo].[tbl_LoanContracts](
	[Reference] [varchar](30) NOT NULL,
	[LoanRequestId] [bigint] NOT NULL,
	[InvestorId] [bigint] NOT NULL,
 CONSTRAINT [PK_tbl_LoanContracts] PRIMARY KEY CLUSTERED 
(
	[Reference] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]