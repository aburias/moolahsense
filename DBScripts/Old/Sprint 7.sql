###8/8/2014
#Altered tbl_LoanAmortization
Alter PayStatus
Add LateStartDate
Add LateInterestPayStatus
Add LateInterestPaidDate

###12/8/2014
#Altered tbl_LoanAmortization
Add LateInterest
#Alter tbl_MoolahPeri
Add UpdateMoolahPost

###18/8/2014
#Added new table tbl_loanRequestReference

/****** Object:  Table [dbo].[tbl_loanRequestReference]    Script Date: 8/18/2014 2:27:53 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_loanRequestReference](
	[RequestId] [bigint] NOT NULL,
 CONSTRAINT [PK_tbl_loanRequestReference] PRIMARY KEY CLUSTERED 
(
	[RequestId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

#Altered tbl_loanRequest RequestId no Identity

#Add SP
/****** Object:  StoredProcedure [dbo].[SP_GetLatestLoanReference]    Script Date: 8/18/2014 2:32:28 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[SP_GetLatestLoanReference]            
AS        
begin        
declare @newrequestid bigint
select @newrequestid = RequestId from tbl_LoanRequestReference
set @newrequestid = @newrequestid + 1
update tbl_LoanRequestReference set RequestId = @newrequestid
select RequestId from tbl_LoanRequestReference

End

GO


##23082014
update tbl_globalvariables set RequestCompletionDays = 30 

##28082014
alter table [tbl_LoanAmortization] add Principal  decimal(18,10) null