/****** Object:  Table [dbo].[tbl_AdminActionLogs]    Script Date: 2/27/2014 9:27:43 PM ******/

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_AdminActionLogs]') AND type in (N'U'))
BEGIN
DROP TABLE tbl_AdminActionLogs
END

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_AdminActionLogs](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Message] [nvarchar](max) NULL,
	[Timestamp] [datetime] NULL,
	[UserName] [nvarchar](50) NULL,
	[LoanRequestId] [bigint] NULL,
	[ActionUserId] [bigint] NULL,
	[Reference] [nvarchar](20) NULL,
	[FieldName] [nvarchar](100) NULL,
	[OldValue] [nvarchar](100) NULL,
	[NewValue] [nvarchar](100) NULL,
 CONSTRAINT [PK_tbl_AdminActionLogs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO

--Dan 27/02/2014

ALTER TABLE tbl_accountDocuments
DROP Constraint FK_tbl_AccountDocuments_tbl_Users
GO
ALTER TABLE tbl_accountDocuments
DROP Constraint FK_tbl_AccountDocuments_tbl_LoanRequests
GO
ALTER TABLE tbl_DocumentComments
DROP Constraint FK_tbl_DocumentComments_tbl_AccountDocuments
GO
DROP TABLE tbl_accountDocuments
GO

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_DocumentComments]') AND type in (N'U'))
BEGIN
DROP TABLE tbl_DocumentComments
END

IF  EXISTS (SELECT * FROM sys.objects 
WHERE object_id = OBJECT_ID(N'[dbo].[tbl_Files]') AND type in (N'U'))
BEGIN
DROP TABLE tbl_Files
END

CREATE TABLE [dbo].[tbl_Files] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FileName] nvarchar(250)  NOT NULL,
    [Type] nvarchar(50)  NOT NULL,
    [UserId] uniqueidentifier  NOT NULL,
    [CreatedDate] datetime  NOT NULL
);
ALTER TABLE [dbo].[tbl_Files]
ADD CONSTRAINT [PK_tbl_Files]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

--Dan 2/3/2014
alter table tbl_files add Comments nvarchar(500) null
---------------

-- Knowladge assesment new items
TRUNCATE TABLE [dbo].[tbl_Knowledgeassessment_OptionsList]

INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Primary',1,1,'option');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Secondary',1,1,'option'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Pre-University',1,1,'option'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Diploma/Vocational Education',1,1,'option'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('University',1,1,'option');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('None at all',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Stocks',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Mutuals Funds',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Bonds',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Leveraged Foreign Exchange',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Structure Products',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Other Alternative Investments',2,1,'checkbox');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that in making a decision to fund a loan, I will rely on my own examination of the Borrowe. <br/>MSense neither recommends nor endorses any Borrower and/or associated loan requests.',3,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that if the borrower default on the loan, I may not be able to recover any or all of the invested amounts.',3,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that the loans are illiquid qnd do not trade on a secondary market. I will have to hold the loan(s) till maturity.',3,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that the platform and the loans arenot regulated by the Monetary Authorities of Singapore(MAS).',3,1,'checkbox');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Master Agreement between Lender and MSence []',4,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Code of Conduct []',4,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Privacy and Data Protection Policy []',4,1,'checkbox');

UPDATE  [dbo].[tbl_Knowledgeassessment_Parentlist] 
SET [Parenttext]='Summary Risks'
WHERE [ParentId]=3;    
UPDATE  [dbo].[tbl_Knowledgeassessment_Parentlist] 
SET [Parenttext]='I have read and understand the contents in the following documents'
WHERE [ParentId]=4;
UPDATE  [dbo].[tbl_Knowledgeassessment_Parentlist] 
SET [Parenttext]='Please note that by providing the information (and any other information in this questionnaire) will not of itself have any effect of limiting your potential losses. 
I declare that the above information provided by me is true and accurate.'
WHERE [ParentId]=5;
DELETE [dbo].[tbl_Knowledgeassessment_Parentlist]
WHERE [ParentId]=6;

ALTER TABLE [dbo].[tbl_Knowledgeassessment_Parentlist] ADD IsCorp BIT NULL
GO
UPDATE [dbo].[tbl_Knowledgeassessment_Parentlist]
SET IsCorp = 'True'
WHERE ParentId <> 1
UPDATE [dbo].[tbl_Knowledgeassessment_Parentlist]
SET IsCorp = 'False'
WHERE ParentId = 1

TRUNCATE TABLE [dbo].[tbl_Knowledgeassessment_OptionsList]

INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Primary',1,1,'option');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Secondary',1,1,'option'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Pre-University',1,1,'option'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Diploma/Vocational Education',1,1,'option'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('University',1,1,'option');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('None at all',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Stocks',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Mutuals Funds',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Bonds',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Leveraged Foreign Exchange',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Structure Products',2,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Other Alternative Investments',2,1,'checkbox');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that in making a decision to fund a loan, I will rely on my own examination of the Borrower. <br/>MSense neither recommends nor endorses any Borrower and/or associated loan requests.',3,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that if the borrower default on the loan, I may not be able to recover any or all of the invested amounts.',3,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that the loans are illiquid and do not trade on a secondary market. I will have to hold the loan(s) till maturity.',3,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('I understand that the platform and the loans are not regulated by the Monetary Authorities of Singapore(MAS).',3,1,'checkbox');   
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Master Agreement between Lender and MSence []',4,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Code of Conduct []',4,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Privacy and Data Protection Policy []',4,1,'checkbox');

--Dan 5/3/2014
alter table tbl_LoanAmortization add Debited bit null
--
GO
ALTER TABLE [dbo].[tbl_Knowledgeassessment_Parentlist]
DROP COLUMN [IsCorp]
GO
ALTER TABLE [dbo].[tbl_Knowledgeassessment_Parentlist]
ADD [InvType] CHAR(1) NULL
GO
UPDATE [dbo].[tbl_Knowledgeassessment_Parentlist]
SET InvType = 'B'

UPDATE [dbo].[tbl_Knowledgeassessment_Parentlist]
SET InvType = 'I'
WHERE ParentId IN (1, 2)

INSERT INTO [dbo].[tbl_Knowledgeassessment_Parentlist]([Parenttext],[Priority],[Isactive],[InvType])
VALUES ('Have your company invested in the following financial products?',1,'True','C')

UPDATE [dbo].[tbl_Knowledgeassessment_Parentlist] 
SET [Parenttext] = 'Have you invested in the following financial products?'
WHERE ParentId = 2

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'Mutual Funds'
WHERE Optiontext = 'Mutuals Funds'

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'Structured Products'
WHERE Optiontext = 'Structure Products'

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'I understand that in making a decision to fund a loan, I will rely on my own examination of the Borrower. <br/>MoolahSense neither recommends nor endorses any Borrower and/or associated loan requests.'
WHERE Optiontext = 'I understand that in making a decision to fund a loan, I will rely on my own examination of the Borrower. <br/>MSense neither recommends nor endorses any Borrower and/or associated loan requests.'

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'I understand that if the borrower defaults on the loan, I may not be able to recover any or all of the invested amounts.'
WHERE Optiontext = 'I understand that if the borrower default on the loan, I may not be able to recover any or all of the invested amounts.'

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'I understand that the platform and the loans are not regulated by the Monetary Authority of Singapore (MAS).'
WHERE Optiontext = 'I understand that the platform and the loans are not regulated by the Monetary Authorities of Singapore(MAS).'

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'Master Agreement between Lender and MoolahSence []'
WHERE Optiontext = 'Master Agreement between Lender and MSence []'

INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('None at all',7,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Stocks',7,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Mutual Funds',7,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Bonds',7,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Leveraged Foreign Exchange',7,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Structured Products',7,1,'checkbox'); 
INSERT INTO  [dbo].[tbl_Knowledgeassessment_OptionsList]([Optiontext],[Parent_Id],[Isactive],[Optiontype]) 
VALUES ('Other Alternative Investments',7,1,'checkbox');   

ALTER TABLE [dbo].[tbl_AccountDetails]
ADD BranchName VARCHAR(100) NULL,
BranchNumber VARCHAR(100) NULL,
BankNumber VARCHAR(100) NULL
GO
ALTER TABLE [dbo].[tbl_UserVerification]
ADD BranchName VARCHAR(16) NULL,
BranchNumber VARCHAR(16) NULL,
BankNumber VARCHAR(16) NULL
GO
--Dan 11/03/2014

alter table tbl_Files add RequestId bigint null
GO
--

UPDATE [tbl_Knowledgeassessment_OptionsList]
SET Optiontext = 'Master Agreement between Lender and MoolahSense []'
WHERE Optiontext = 'Master Agreement between Lender and MoolahSence []'

ALTER TABLE [dbo].[tbl_LoanRequests] 
ADD MoolahSenseComments VARCHAR(MAX) NULL
GO
----Dan 17/03

alter table tbl_Knowledgeassessment_OptionsList
add url varchar(300) null
GO
update tbl_Knowledgeassessment_OptionsList
set Optiontext = 'Standard Terms and Conditions',
url = 'https://www.moolahsense.com/wp-content/uploads/2014/03/MSense-Lender-Standard-TC.pdf'
where Optionid = 17

update tbl_Knowledgeassessment_OptionsList
set Optiontext = 'Code of Conduct',
url = 'https://www.moolahsense.com/wp-content/uploads/2014/03/MSense-Lender-Standard-TC.pdf'
where Optionid = 18

update tbl_Knowledgeassessment_OptionsList
set Optiontext = 'Privacy and Data Protection Policy',
url = 'https://www.moolahsense.com/wp-content/uploads/2014/03/MSense-Privacy-and-Data-Protection-Policy.pdf'
where Optionid = 19

--

delete tbl_SecurityQuestions
GO
SET IDENTITY_INSERT [dbo].[tbl_SecurityQuestions] ON 

GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (1, N'Which city were you born in?', CAST(0x0000A17F00F61F5B AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (2, N'What is your pet''s name?', CAST(0x0000A18501024DC7 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (3, N'What is your mother''s name?', CAST(0x0000A220010588AF AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (4, N'What is your father''s name?', CAST(0x0000A22D010B5B3B AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (5, N'What is the brand of your first car?', CAST(0x0000A2320052F2FF AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (6, N'What is your favourite colour?', CAST(0x0000A2B200801966 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (7, N'When is your spouse''s birthday (dd/mm/yyyy)?', CAST(0x0000A2B200804C91 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (8, N'What is your favourite sport?', CAST(0x0000A2B200805C9E AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (9, N'What is your vehicle number?', CAST(0x0000A2B2008073F6 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (10, N'What is the name of your best friend?', CAST(0x0000A2B200809934 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (12, N'Who is your favourite actor/actress?', CAST(0x0000A2B200A3AB04 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (13, N'Which primary school did you attend?', CAST(0x0000A2B200000000 AS DateTime), 1)
GO
INSERT [dbo].[tbl_SecurityQuestions] ([QuestionId], [QuestionName], [DateCreated], [Status]) VALUES (14, N'What is your favourite movie?', CAST(0x0000A2F40039B019 AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[tbl_SecurityQuestions] OFF
GO

--Dan 21/02/2014
update tbl_Users set UniqueAccountNumber = 100000
alter table tbl_users alter column UniqueAccountNumber bigint not null
--

ALTER TABLE [dbo].[tbl_MoolahCore] ALTER COLUMN [LatestYear] VARCHAR(MAX)
GO
ALTER TABLE [dbo].[tbl_MoolahCore] ALTER COLUMN [PreiviousYear] VARCHAR(MAX)
GO
--Dan 27/03/2014
alter table tbl_Files add OfferId int null
--
GO
--Dan 30/03/2014
alter table tbl_files add ContractId nvarchar(50) null
--
GO
ALTER TABLE [dbo].[tbl_LoanRequests]
ADD TransferDate DATETIME NULL, ResonReject VARCHAR(100) NULL

--Dan 30/03/2014
alter table tbl_users drop column UniqueAccountNumber
--
GO
alter table [dbo].[tbl_Loanoffers]
ADD ContractId NVARCHAR(50)

--Dan 30/03/2014
alter table tbl_files drop column ContractId
--

--Dan 02/04/2013
alter table tbl_AccountDetails alter column BusinessMailingAddress varchar(300)
--