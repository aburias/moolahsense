ALTER TABLE [dbo].[tbl_LoanPayments]  WITH CHECK ADD  CONSTRAINT [FK_tbl_LoanPayments_tbl_LoanAmortization] FOREIGN KEY([RepaymentID])
REFERENCES [dbo].[tbl_LoanAmortization] ([AmortizationID])
GO

ALTER TABLE [dbo].[tbl_LoanPayments] CHECK CONSTRAINT [FK_tbl_LoanPayments_tbl_LoanAmortization]
GO

ALTER TABLE [dbo].[tbl_LoanPayments]  WITH CHECK ADD  CONSTRAINT [FK_tbl_LoanPayments_tbl_LoanRequests] FOREIGN KEY([LoanRequestID])
REFERENCES [dbo].[tbl_LoanRequests] ([RequestId])
GO

ALTER TABLE [dbo].[tbl_LoanPayments] CHECK CONSTRAINT [FK_tbl_LoanPayments_tbl_LoanRequests]
GO