GO
/****** Object:  StoredProcedure [dbo].[USP_tbl_PersonalGuaranteeInfo_Delete]    Script Date: 12/17/2014 11:49:47 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[USP_tbl_PersonalGuaranteeInfo_Delete]
@inforid int,
@requestid bigint

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if exists(select InfoId from tbl_PersonalGuaranteeInfo where InfoId=@inforid AND Request_Id = @requestid)
   begin
  
    DELETE FROM tbl_PersonalGuaranteeInfo WHERE InfoId  = @inforid
    
    end 
          
END
