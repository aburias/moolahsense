alter table tbl_loanrequests add Headline varchar(200) null

/****** Object:  StoredProcedure [dbo].[USP_tbl_LoanRequest_SaveLoanrequestdetails]    Script Date: 9/15/2014 6:48:58 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



ALTER PROCEDURE [dbo].[USP_tbl_LoanRequest_SaveLoanrequestdetails]    
(        
@Amount decimal(18,2),    
@Terms varchar(10),    
@Rate decimal(18, 2),    
@Ispersonalguarantee bit,    
@LoanpurposeID bigint,    
@LoanPurposeDescription nvarchar(max),    
@NameasinNRICPassport varchar(200),    
@Designation varchar(100),    
@NRICPassport varchar(20),    
@Reasons nvarchar(max),    
@Residentialaddress varchar(200),    
@Telephone varchar(20),    
@Email varchar(50),    
@Postalcode varchar(10),    
@User_Id bigint,    
@requestID bigint output  ,  
@requestIDold int=0,
@VideoDiscription nvarchar(max),  ---------------------- 0 for insert else for update  
@EIR decimal(18, 2),
@DetailedCompanyPro nvarchar(max),
@SummaryCompanyPro nvarchar(max),
@PassportNumber varchar(200),
@NumberOfEmployees int,
@Nationality varchar(200),
@Headline varchar(200)
)        
AS        
begin        
declare @Pdoc int     
        -- ///////////////////////////////  save Loan request info into tbl_Loanrequest table  ///////////////////////      
   update tbl_LoanRequests set Amount=@Amount,Terms=@Terms,Rate=@Rate,EIR=@EIR,
   IsPersonalGuarantee=@Ispersonalguarantee,LoanPurpose_Id=@LoanpurposeID ,
   VideoDescription = @VideoDiscription ,DetailedCompanyPro = @DetailedCompanyPro,
    SummaryCompanyPro = @SummaryCompanyPro, NumberOfEmployees = @NumberOfEmployees ,
	 LoanPurposeDescription = @LoanPurposeDescription, Headline = @Headline
   where RequestId=@requestIDold  
   select @requestID=@requestIDold  
       
   --///////////////////////////////// save personal guarantee info into tbl_PersonalGuaranteeInfo if ispersonalguarantee is true ///////////////////////    
   if exists(select NameAsinNRIC from tbl_PersonalGuaranteeInfo where Request_Id=@requestIDold)
   begin
   if @Ispersonalguarantee=1    
     Begin      
       update tbl_PersonalGuaranteeInfo set NameAsinNRIC=@NameasinNRICPassport,Designation=@Designation,NRIC_Passport=@NRICPassport,ResidentialAddress=@Residentialaddress,  
       Telephone=@Telephone,Email=@Email,Reasons=@Reasons,Postalcode=@Postalcode, PassportNumber = @PassportNumber, Nationality = @Nationality   
       where Request_Id=@requestIDold       
     End
     else  
     Begin  
      delete from tbl_PersonalGuaranteeInfo where Request_Id=@requestIDold  
     End  
    end 
    else
    begin
      insert into tbl_PersonalGuaranteeInfo(NameAsinNRIC,Designation,NRIC_Passport,ResidentialAddress,Telephone,Email,Reasons,Request_Id,Postalcode,PassportNumber)    
       values(@NameasinNRICPassport,@Designation,@NRICPassport,@Residentialaddress,@Telephone,@Email,@Reasons,@requestid,@Postalcode, @PassportNumber)    
    end          
End   
GO




***11/09/2014

Alter table tbl_LoanAmortization add PaidAmount decimal(18,2) null
Alter table tbl_LoanAmortization add LateFees decimal(18,2) null
Alter table tbl_LoanAmortization add Principal decimal(18,2) null

***16/09/2014

/****** Object:  Table [dbo].[tbl_LoanPayments]    Script Date: 9/16/2014 12:21:37 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_LoanPayments](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[LoanRequestID] [bigint] NOT NULL,
	[RepaymentID] [bigint] NULL,
	[Amount] [decimal](18, 0) NOT NULL,
	[PaidDate] [datetime] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
 CONSTRAINT [PK_tbl_LoanPayments] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

--18/09/2014
-------------------

drop table tbl_Knowledgeassessment_OptionsList
drop table tbl_Knowledgeassessment_Parentlist

GO
/****** Object:  Table [dbo].[tbl_Knowledgeassessment_OptionsList]    Script Date: 9/19/2014 11:25:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Knowledgeassessment_OptionsList](
	[Optionid] [int] IDENTITY(1,1) NOT NULL,
	[Optiontext] [varchar](500) NULL,
	[Parent_Id] [int] NULL,
	[Isactive] [bit] NULL,
	[Optiontype] [varchar](50) NULL,
	[url] [varchar](300) NULL,
 CONSTRAINT [PK_tbl_Knowledgeassessment_OptionsList] PRIMARY KEY CLUSTERED 
(
	[Optionid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[tbl_Knowledgeassessment_Parentlist]    Script Date: 9/19/2014 11:25:56 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[tbl_Knowledgeassessment_Parentlist](
	[ParentId] [int] IDENTITY(1,1) NOT NULL,
	[Parenttext] [varchar](500) NULL,
	[Priority] [int] NULL,
	[Isactive] [bit] NULL,
	[InvType] [char](1) NULL,
 CONSTRAINT [PK_tbl_Knowledgeassessment_Parentlist] PRIMARY KEY CLUSTERED 
(
	[ParentId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ON 

INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (1, N'Primary', 1, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (2, N'Secondary', 1, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (3, N'Pre-University', 1, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (4, N'Diploma/Vocational Education', 1, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (5, N'University', 1, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (6, N'None at all', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (7, N'Stocks', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (8, N'Mutual Funds', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (9, N'Bonds', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (10, N'Leveraged Foreign Exchange', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (11, N'Structured Products', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (12, N'Other Alternative Investments', 2, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (13, N'I understand that in making a decision to fund a note, I will rely on my own examination of the issuer. <br/>MoolahSense neither recommends nor endorses any issuer and/or associated note requests.', 3, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (14, N'I understand that if the issuer defaults on the note, I may not be able to recover any or all of the invested amounts.', 3, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (15, N'I understand that the notes are illiquid and do not trade on a secondary market. I will have to hold the note(s) till maturity.', 3, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (16, N'I understand that the platform and the notes are not regulated by the Monetary Authority of Singapore (MAS).', 3, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (17, N'Standard Terms and Conditions', 4, 1, N'checkbox', N'https://www.moolahsense.com/wp-content/uploads/2014/03/MSense-Lender-Standard-TC.pdf')
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (18, N'Code of Conduct', 4, 1, N'checkbox', N'https://www.moolahsense.com/wp-content/uploads/2014/03/MSense-Code-of-Conduct.pdf')
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (19, N'Privacy and Data Protection Policy', 4, 1, N'checkbox', N'https://www.moolahsense.com/wp-content/uploads/2014/03/MSense-Privacy-and-Data-Protection-Policy.pdf')
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (20, N'None at all', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (21, N'Stocks', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (22, N'Mutual Funds', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (23, N'Bonds', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (24, N'Leveraged Foreign Exchange', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (25, N'Structured Products', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (26, N'Other Alternative Investments', 7, 1, N'checkbox', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (32, N'Employed', 10, 1, N'radio', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (33, N'Self-Employed', 10, 1, N'radio', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (34, N'Student', 10, 1, N'radio', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (35, N'Retired', 10, 1, N'radio', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (36, N'Unemployed', 10, 1, N'radio', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (37, N'Banking and Finance', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (38, N'CEO/Executive', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (39, N'Sales and Marketing', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (40, N'Government/Public Services', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (41, N'Education', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (42, N'IT', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (43, N'Healthcare', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (44, N'Hospitality and Recreation', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (45, N'Services (Retail and Food)', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (46, N'Real Estate Services', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (47, N'Accounting', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (48, N'Consulting', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (49, N'Transport Services', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (50, N'Manufacturing Industry', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (51, N'Others', 11, 1, N'option', NULL)
INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] ([Optionid], [Optiontext], [Parent_Id], [Isactive], [Optiontype], [url]) VALUES (52, N'', 12, 1, N'text', NULL)
SET IDENTITY_INSERT [dbo].[tbl_Knowledgeassessment_OptionsList] OFF
SET IDENTITY_INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ON 

INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (1, N'What is your highest level of education ?', 1, 1, N'I')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (2, N'Have you invested in the following financial products?', 5, 1, N'I')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (3, N'Summary Risks', 6, 1, N'B')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (4, N'I have read and understand the contents in the following documents', 7, 1, N'B')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (5, N'Please note that by providing the information (and any other information in this questionnaire) will not of itself have any effect of limiting your potential losses. 
I declare that the above information provided by me is true and accurate.', 8, 1, N'B')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (7, N'Have your company invested in the following financial products?', 1, 1, N'C')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (10, N'Employment Status ?', 2, 1, N'I')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (11, N'What is your occupation?', 3, 1, N'I')
INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId], [Parenttext], [Priority], [Isactive], [InvType]) VALUES (12, N'Please provide details', 4, 1, N'I')
SET IDENTITY_INSERT [dbo].[tbl_Knowledgeassessment_Parentlist] OFF
ALTER TABLE [dbo].[tbl_Knowledgeassessment_OptionsList]  WITH CHECK ADD  CONSTRAINT [FK_tbl_Knowledgeassessment_OptionsList_tbl_Knowledgeassessment_OptionsList] FOREIGN KEY([Parent_Id])
REFERENCES [dbo].[tbl_Knowledgeassessment_Parentlist] ([ParentId])
GO
ALTER TABLE [dbo].[tbl_Knowledgeassessment_OptionsList] CHECK CONSTRAINT [FK_tbl_Knowledgeassessment_OptionsList_tbl_Knowledgeassessment_OptionsList]
GO

-------------------

