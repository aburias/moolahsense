--select * from [dbo].[tbl_Knowledgeassessment_Parentlist]
--select * from [dbo].[tbl_Knowledgeassessment_OptionsList]

update [tbl_Knowledgeassessment_OptionsList] 
set Optiontext = 
'I understand that in making a decision to fund a note, I will rely on my own examination of the issuer. <br/>MoolahSense neither recommends nor endorses any issuer and/or associated note requests.'
where Optionid = 13

update [tbl_Knowledgeassessment_OptionsList] 
set Optiontext = 
'I understand that if the issuer defaults on the note, I may not be able to recover any or all of the invested amounts.'
where Optionid = 14

update [tbl_Knowledgeassessment_OptionsList] 
set Optiontext = 
'I understand that the notes are illiquid and do not trade on a secondary market. I will have to hold the note(s) till maturity.'
where Optionid = 15

update [tbl_Knowledgeassessment_OptionsList] 
set Optiontext = 
'I understand that the platform and the notes are not regulated by the Monetary Authority of Singapore (MAS).'
where Optionid = 16

update [tbl_Knowledgeassessment_OptionsList] 
set url = 
'http://www.moolahsense.com/wp-content/uploads/2014/09/MSense-Standard-TC.pdf'
where Optionid = 17

update [tbl_Knowledgeassessment_OptionsList] 
set url = 
'http://www.moolahsense.com/wp-content/uploads/2014/09/MSense-Code-of-Conduct.pdf'
where Optionid = 18

update [tbl_Knowledgeassessment_OptionsList] 
set url = 
'http://www.moolahsense.com/wp-content/uploads/2014/09/MSense-Privacy-and-Data-Protection-Policy.pdf'
where Optionid = 19