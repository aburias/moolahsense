
declare @requestid bigint
declare @newRequestid bigint
set @requestid = 4
set @newRequestid = 1001

update tbl_loanoffers set loanrequest_id = @newRequestid where loanrequest_id = @requestid
update tbl_LoanPayments set loanrequestid = @newRequestid where loanrequestid = @requestid
update [dbo].[tbl_LoanAmortization] set loanrequest_id = @newRequestid where loanrequest_id = @requestid

update [dbo].[tbl_LoanTransactions] set request_id = @newRequestid where request_id = @requestid
update tbl_MoolahCore set loanrequest_id = @newRequestid where loanrequest_id = @requestid
update tbl_MoolahPeri set request_id = @newRequestid where request_id = @requestid
update tbl_MoolahPeriverification set request_id = @newRequestid where request_id = @requestid
update tbl_MoolahCoreverification set request_id = @newRequestid where request_id = @requestid
update tbl_PersonalGuaranteeInfo set request_id = @newRequestid where request_id = @requestid
update tbl_files set requestid = @newRequestid where [RequestId]= @requestid
update tbl_WatchList set request_id = @newRequestid where [Request_Id]= @requestid

update tbl_LoanRequests set requestid = @newRequestid where requestid = @requestid

