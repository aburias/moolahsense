GO
/****** Object:  StoredProcedure [dbo].[USP_tbl_PersonalGuaranteeInfo_Save]    Script Date: 12/17/2014 11:49:50 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Create PROCEDURE [dbo].[USP_tbl_PersonalGuaranteeInfo_Save]
@inforid int,
@requestid bigint,
@NameasinNRICPassport varchar(200),    
@Designation varchar(100),    
@NRICPassport varchar(20),    
@Reasons nvarchar(max),    
@Residentialaddress varchar(200),    
@Telephone varchar(20),    
@Email varchar(50),    
@Postalcode varchar(10) ,
@PassportNumber varchar(200),
@Nationality varchar(200)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

    if exists(select InfoId from tbl_PersonalGuaranteeInfo where InfoId=@inforid AND Request_Id = @requestid)
   begin
  
       update tbl_PersonalGuaranteeInfo set NameAsinNRIC=@NameasinNRICPassport,Designation=@Designation,NRIC_Passport=@NRICPassport,ResidentialAddress=@Residentialaddress,  
       Telephone=@Telephone,Email=@Email,Reasons=@Reasons,Postalcode=@Postalcode, PassportNumber = @PassportNumber, Nationality = @Nationality   
       where InfoId=@inforid      
    
    
    end 
    else
    begin
      insert into tbl_PersonalGuaranteeInfo(NameAsinNRIC,Designation,NRIC_Passport,ResidentialAddress,Telephone,Email,Reasons,Request_Id,Postalcode,PassportNumber,Nationality)    
       values(@NameasinNRICPassport,@Designation,@NRICPassport,@Residentialaddress,@Telephone,@Email,@Reasons,@requestid,@Postalcode, @PassportNumber,@Nationality)    
    end          
END
