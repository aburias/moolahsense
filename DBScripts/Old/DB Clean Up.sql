delete from tbl_accountdetails where user_id in (select userid from tbl_users where userid >= 100000)
delete from tbl_moolahcore where loanrequest_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_moolahcoreverification where request_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_moolahperi where request_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_moolahperiverification where request_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_PersonalGuaranteeInfo where request_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_LoanTransactions where request_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_Loanoffers where loanrequest_id in (select requestid from tbl_loanrequests where user_id >= 100000)
delete from tbl_LoanAmortization where loanrequest_id in (select requestid from tbl_loanrequests where user_id >= 100000)

delete from tbl_SecurityQuestionsForUsers where user_id in (select userid from tbl_users where userid >= 100000)
delete from tbl_Balances where user_id in (select userid from tbl_users where userid >= 100000)
delete from tbl_Knowledgeassessmentdetails_ForUser where user_id in (select userid from tbl_users where userid >= 100000)
delete from tbl_LoanTransactions where user_id in (select userid from tbl_users where userid >= 100000)
delete from tbl_WithDrawMoney where user_id in (select userid from tbl_users where userid >= 100000)

delete from tbl_loanrequests where user_id >= 100000
delete from tbl_users where userid >= 100000

delete from tbl_auditlog
delete from tbl_errorlog
delete from [dbo].[tbl_AdminActionLogs]