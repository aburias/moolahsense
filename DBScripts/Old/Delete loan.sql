--select * from tbl_LoanRequests order by RequestId


declare @requestid bigint
set @requestid = 1027

delete from tbl_AccountAdjustment where RepaymentID in (select AmortizationID from tbl_LoanAmortization where LoanRequest_ID = @requestid)
delete from tbl_loanoffers where loanrequest_id = @requestid
delete from [dbo].[tbl_LoanPayments] where loanrequestid = @requestid
delete from [dbo].[tbl_LoanAmortization] where loanrequest_id = @requestid

delete from [dbo].[tbl_LoanTransactions] where request_id = @requestid
delete from tbl_MoolahCore where loanrequest_id = @requestid
delete from tbl_MoolahPeri where request_id = @requestid
delete from tbl_MoolahPeriverification where request_id = @requestid
delete from tbl_MoolahCoreverification where request_id = @requestid
delete from tbl_PersonalGuaranteeInfo where request_id = @requestid
delete from tbl_files where [RequestId]= @requestid
delete from tbl_WatchList where [Request_Id]= @requestid

delete from tbl_LoanRequests where requestid = @requestid

--update tbl_balances set actualamount= 0, ledgeramount = 0
--delete from tbl_loantransactions

