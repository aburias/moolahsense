GO

/****** Object:  Table [dbo].[tbl_LoanOffersLog]    Script Date: 12/29/2014 4:48:49 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[tbl_LoanOffersLog](
	[OfferLogID] [int] IDENTITY(1,1) NOT NULL,
	[OfferStatus] [int] NOT NULL,
	[TimeStamp] [datetime] NOT NULL,
	[OfferID] [int] NOT NULL,
 CONSTRAINT [PK_tbl_loanOffersLog] PRIMARY KEY CLUSTERED 
(
	[OfferLogID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO


