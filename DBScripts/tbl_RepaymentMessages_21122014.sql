--drop table [tbl_RepaymentMessages]
GO

/****** Object:  Table [dbo].[tbl_RepaymentMessages]    Script Date: 12/21/2014 11:02:21 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

Create TABLE [dbo].[tbl_RepaymentMessages](
	[ID] [bigint] IDENTITY(1,1) NOT NULL,
	[LoanRequestId] [bigint] NULL,
	[Message] [varchar](250) NULL,
	[CreatedDate] [datetime] NULL,
 CONSTRAINT [PK_tbl_RepaymentMessages] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO

ALTER TABLE [dbo].[tbl_RepaymentMessages]  WITH CHECK ADD  CONSTRAINT [FK_tbl_RepaymentMessages_tbl_LoanRequests] FOREIGN KEY([LoanRequestId])
REFERENCES [dbo].[tbl_LoanRequests] ([RequestId])
GO

ALTER TABLE [dbo].[tbl_RepaymentMessages] CHECK CONSTRAINT [FK_tbl_RepaymentMessages_tbl_LoanRequests]
GO


