alter table tbl_moolahcore add ProfitBeforIntAndTax_LY decimal(18,2) null
alter table tbl_moolahcore add ProfitBeforIntAndTax_PY decimal(18,2) null

alter table tbl_moolahcore add InterestExpense_LY decimal(18,2) null
alter table tbl_moolahcore add InterestExpense_PY decimal(18,2) null

alter table tbl_moolahcore add InterestCoverageRatio_LY decimal(18,2) null
alter table tbl_moolahcore add InterestCoverageRatio_PY decimal(18,2) null

alter table [dbo].[tbl_MoolahCoreVerification] add [ProfitBeforIntAndTax_LY_Verification] int null
alter table [dbo].[tbl_MoolahCoreVerification] add [ProfitBeforIntAndTax_PY_Verification] int null

alter table [dbo].[tbl_MoolahCoreVerification] add [InterestExpense_LY_Verification] int null
alter table [dbo].[tbl_MoolahCoreVerification] add [InterestExpense_PY_Verification] int null

alter table [dbo].[tbl_MoolahCoreVerification] add [InterestCoverageRatio_LY_Verification] int null
alter table [dbo].[tbl_MoolahCoreVerification] add [InterestCoverageRatio_PY_Verification] int null

