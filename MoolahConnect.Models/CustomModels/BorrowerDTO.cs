﻿using Foolproof;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Models.CustomModels
{
    public class BorrowerDTO
    {
    }

    #region Borrower

    public class BorrowerAccountDetailsModel
    {
        public string Title { get; set; }

        [Required(ErrorMessage = "* Full Name is required")]
        [Display(Name = "Full Name")]
        [StringLength(50)]
        [DataType(DataType.Text)]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        public string FirstName { get; set; }

         [Required(ErrorMessage = "* Last Name is required")]
        [Display(Name = "LastName")]
        [StringLength(50)]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        public string LastName { get; set; }

         [Required(ErrorMessage = "* Display Name is required")]
         [DataType(DataType.Text)]
         [Display(Name = "DisplayName")]
         [StringLength(100)]
         [System.Web.Mvc.Remote("CheckDisplayNameAvailbilty", "Investor", AdditionalFields = "Emailaddress", HttpMethod = "POST", ErrorMessage = "* This name is already in use by other investor.")]
        public string DisplayName { get; set; }

        [Required(ErrorMessage = "* Position Held is required")]
        [Display(Name = "PositionHeld")]
        [StringLength(100)]
        public string RoleJobTitle { get; set; }
        [Required(ErrorMessage = "* Business Name is required")]
        [Display(Name = "BusinessName")]
        [StringLength(500)]
        public string BusinessName { get; set; }

        [Required(ErrorMessage = "* Business Registration Number is required")]
        [DataType(DataType.Text)]
        [Display(Name = "Business Registration Number")]
        [StringLength(20)]
        [System.Web.Mvc.Remote("CheckBusinessRegNumberAvailbilty", "Shared", HttpMethod = "POST", ErrorMessage = "* Business registration number is already in use by other user.")]
        public string BusinessRegistrationnumber { get; set; }
        
        [Display(Name = "BusinessOrganisation")]
        public string BusinessOrganisation { get; set; }
        public int NatureofBusinessId { get; set; }
        public string NatureofBusinessName { get; set; }
        public IEnumerable<NatureofBusiness> NatureofBusinessList { get; set; }

        [DataType(DataType.Date, ErrorMessage = "* Only Date formate is allowed")]
        [Display(Name = "DateofIncorporation")]
        [Required(ErrorMessage = "* Date of Incorporation is required")]
        public DateTime? DateofIncorporation { get; set; }

        public string DateofIncorporationStr { get; set; }

        [Required(ErrorMessage = "* Registered Address 1 is required")]
        [Display(Name = "Registered Address 1")]
        [StringLength(240)]
        public string RegisteredAddress1 { get; set; }

        [Required(ErrorMessage = "* Registered Address 2 is required")]
        [Display(Name = "Registered Address 2")]
        [StringLength(240)]
        public string RegisteredAddress2 { get; set; }

        [Required(ErrorMessage = "* Registered Address Postal Code is required")]
        [Display(Name = "Registered Address Postal Code")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "* No characters are allowed or number must contain 6 digits")]
        public string RegisteredPostalCode { get; set; }

        [Required(ErrorMessage = "* Business Mailing Address 1 is required")]
        [Display(Name = "Business Mailing Address 1")]
        [StringLength(240)]
        public string BusinessMailingAddress1 { get; set; }

        [Required(ErrorMessage = "* Business Mailing Address 2 is required")]
        [Display(Name = "Business Mailing Address 2")]
        [StringLength(240)]
        public string BusinessMailingAddress2 { get; set; }

        [Required(ErrorMessage = "* Business Mailing Address Postal Code is required")]
        [Display(Name = "Business Mailing Address Postal Code")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "* No characters are allowed or number must contain 6 digits")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "No characters are allowed")]
        public string BusinessMailingPostalCode { get; set; }

        [Required(ErrorMessage = "* Paid Up Capital is required")]
        public double? PaidUpCapital { get; set; }

        [Required(ErrorMessage = "* Number Of Employees is required")]
        public int? NumOfEmployees { get; set; }

        [Required(ErrorMessage = "* Direct Line is required")]
        [Display(Name = "DirectLine")]
        
        [DataType(DataType.PhoneNumber, ErrorMessage = "* No characters are allowed")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits")]                
        public string DirectLine { get; set; }

        [Required(ErrorMessage = "* Mobile Number is required")]
        [Display(Name = "Mobilenumber")]
        [StringLength(8,MinimumLength=8,ErrorMessage = "* No characters are allowed or number must contain 8 digits")]                
        public string Mobilenumber { get; set; }

        [Display(Name = "Official Correspondence Email")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid e-mail adress")]
        public string Othercontactnumber { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "* Email Address is required")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid e-mail adress")]
        [System.Web.Mvc.Remote("CheckEmailavailability", "User", HttpMethod = "POST", ErrorMessage = "* Email already registered. Please enter a different email.")]
        public string EmailAddress { get; set; }

        public int BankId { get; set; }

        public string BankName { get; set; }

        [Required(ErrorMessage = "* Bank Code is required")]
        [Display(Name = "Bank Code")]
        [StringLength(100)]
        public string BankNumber { get; set; }

        [Required(ErrorMessage = "* Branch Name is required")]
        [Display(Name = "Branch Name")]
        [StringLength(100)]
        public string BranchName { get; set; }

        [Required(ErrorMessage = "* Branch Code is required")]
        [Display(Name = "Branch Code")]
        [StringLength(100)]
        public string BranchNumber { get; set; }

        [Display(Name = "SICC Code")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "* No characters are allowed")]
        public string SiccCode { get; set; }

        public IEnumerable<Bank> BanksList { get; set; }

        [Required(ErrorMessage = "* Bank Account Number is required")]
        [Display(Name = "BankAccountNumber")]
        [StringLength(20)]
        
        public string BankAccountNumber { get; set; }

        [Required(ErrorMessage = "* Bank Account Name is required")]
        [Display(Name = "BankAccountName")]
        [StringLength(100)]
        [DataType(DataType.Text)]
        [Compare("BusinessName", ErrorMessage = "* Account name should be same as Business Name ")]
        public string BankAccountName { get; set; }

        public IEnumerable<SecurityQuestion> SecurityQuestionList { get; set; }

        public int? SecurityQuestion1 { get; set; }
        public int? SecurityQuestion2 { get; set; }

        public string SecurityQuestion1Name { get; set; }
        public string SecurityQuestion2name { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer1")]
        [StringLength(200)]
        public string SecurityAnswer1 { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer2")]
        [StringLength(200)]
        public string SecurityAnswer2 { get; set; }

        [Required(ErrorMessage = "* Nationality is required")]
        [Display(Name = "Nationality")]
        [StringLength(100)]
        public string Nationality { get; set; }

        [Display(Name = "NRIC Number")]
        [Required(ErrorMessage = "* NRIC Number Or FIN is required")]
        //[RequiredIfRegExMatch("Nationality", "^(Singapore Citizen|Singapore PR)$", ErrorMessage = "* NRIC Number is required")]
        [RegularExpression(@"^[a-z|A-Z]{1}\d{7}[a-z|A-Z]{1}$", ErrorMessage = "* NRIC / FIN number must be in format A1234567B ")]
        public string NricNumber { get; set; }

        [Display(Name = "Passport Number")]
        [RequiredIfNotRegExMatch("Nationality", "^(Singapore Citizen|Singapore PR)$", ErrorMessage = "Passport Number is required")]
        public string PassportNumber { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> _Titles { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _Nationality { get; set; }


        /// <summary>
        /// user verification indicator///////////////
        /// </summary>
        public string TitleVerificationIndicator { get; set; }
        public string FirstNameIndicator { get; set; }
        public string LastNameIndicator { get; set; }
        public string DisplayNameIndicator { get; set; }
        public string RoleOrJobTitleIndicator { get; set; }
        public string BussinessNameIndicator { get; set; }
        public string BussinessRegistrationNumberIndicator { get; set; }
        public string BussinessOrganisationIndicator { get; set; }
        public string NatureofBussinessIndicator { get; set; }
        public string DateofIncorporationIndicator { get; set; }
        public string RegisteredAddressIndicator { get; set; }
        public string BussinessMailingAddressIndicator { get; set; }
        public string DirectLineIndicator { get; set; }
        public string MobileIndicator { get; set; }
        public string OptinalContactIndicator { get; set; }
        public string BankIndicator { get; set; }
        public string BankAccountNumberIndicator { get; set; }
        public string BankAccountNameIndicator { get; set; }
        public string CommentsOnField { get; set; }
        public string CommentsOnDocuments { get; set; }
    }

    public class BorrowerDocuments
    {
        public Int64 DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public Nullable<bool> Documentstatus { get; set; }
        public string Documenttype { get; set; }
        public int Userid { get; set; }
        public string Comment { get; set; }
        public IList<commentlist> CommentsList { get; set; }



    }
    public class commentlist
    {
        public string Comment { get; set; }
        public string Username { get; set; }
        public string DocumentName { get; set; }
    }


    public class BorrowerAccount
    {
        public BorrowerAccountDetailsModel BorrowerAccountDetailsModel { get; set; }
        public Knowledgeassessment Knowledgeassessment { get; set; }
        public BorrowerDocuments BorrowerDocuments { get; set; }
       
        public BorrowerAccount(BorrowerAccountDetailsModel borrowerAccountDetailsModel, Knowledgeassessment knowledgeassessment, BorrowerDocuments borrowerDocuments)
        {
            BorrowerAccountDetailsModel = borrowerAccountDetailsModel;
            Knowledgeassessment = knowledgeassessment;
            BorrowerDocuments = borrowerDocuments;
            
            if (BorrowerAccountDetailsModel == null)
            {
                BorrowerAccountDetailsModel = new BorrowerAccountDetailsModel();
            }
            if (BorrowerDocuments == null)
            {
                BorrowerDocuments = new BorrowerDocuments();
            }                
        }

        public BorrowerAccount(BorrowerAccountDetailsModel borrowerAccountDetailsModel, Knowledgeassessment knowledgeassessment)
        {
            BorrowerAccountDetailsModel = borrowerAccountDetailsModel;
            Knowledgeassessment = knowledgeassessment;

            if (BorrowerAccountDetailsModel == null)
            {
                BorrowerAccountDetailsModel = new BorrowerAccountDetailsModel();
            }
        }

        public string Step { get; set; }
    }
  
 
    #endregion
}