﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Foolproof;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Models.CustomModels
{
    public class InvestorDTO
    {
    }

    #region Investor

    public class InvestorAccountDetailsModel
    {
        [Required(ErrorMessage = "* Display name is required")]
        [DataType(DataType.Text)]
        [Display(Name = "DisplayName")]
        [StringLength(100)]
        [System.Web.Mvc.Remote("CheckDisplayNameAvailbilty", "Investor",AdditionalFields="Emailaddress", HttpMethod = "POST", ErrorMessage = "* This name is already in use by other investor.")]
        public string DisplayName { get; set; }
        public string Title { get; set; }

        [Required(ErrorMessage = "* Full Name is required")]
        [Display(Name = "Full Name")]
        [StringLength(100)]
        [RegularExpression(@"[A-Za-z@ ]+", ErrorMessage = "* You have entered an invalid character ")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "* Nationality is required")]
        [Display(Name = "Nationality")]
        [StringLength(100)]
        public string Nationality { get; set; }

        [Display(Name = "NRIC Number")]
        [Required(ErrorMessage = "* NRIC Number Or FIN is required")]
        //[RequiredIfRegExMatch("Nationality", "^(Singapore Citizen|Singapore PR)$", ErrorMessage = "* NRIC Number is required")]
        [RegularExpression(@"^[a-z|A-Z]{1}\d{7}[a-z|A-Z]{1}$", ErrorMessage = "* NRIC / FIN number must be in format A1234567B ")]
        [System.Web.Mvc.Remote("IsValidNRICFIN", "Investor", AdditionalFields="DisplayName", HttpMethod = "POST", ErrorMessage = "* NRIC / FIN number already registered. Please check your number.")]
        public string ICNumber { get; set; }

        [Display(Name = "Passport Number")]
        [RequiredIfNotRegExMatch("Nationality", "^(Singapore Citizen|Singapore PR)$", ErrorMessage = "Passport Number is required")]
        public string PassportNumber { get; set; }

        [Display(Name = "PRStatus")]
        public bool? PRStatus { get; set; }


        [Required(ErrorMessage = "* DOB is required")]


        public DateTime? DateofBirth { get; set; }


        [Display(Name = "Gender")]
        public string Gender { get; set; }

        [Required(ErrorMessage = "* Residential Address Line 1 is required")]
        [Display(Name = "Residential Address Line 1")]
        [StringLength(240)]
        public string ResidentialAddress1 { get; set; }

        //[Required(ErrorMessage = "* Residential Address 2 is required")]
        [Display(Name = "Residential Address Line 2")]
        [StringLength(240)]
        public string ResidentialAddress2 { get; set; }

        [Required(ErrorMessage = "* Residential Address Postal Code is required")]
        [Display(Name = "Residential Address Postal Code")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "* No characters are allowed or number must contain 6 digits")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "No characters are allowed")]
        public string ResidentialPostalCode { get; set; }

        [Required(ErrorMessage = "* Main Contact Number is required")]
        [Display(Name = "Main Contact number")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "* No characters are allowed")]
        public string MainContactNumber { get; set; }

        //[Required(ErrorMessage = "* Main Contact number is required")]
        [Display(Name = "Othercontactnumber")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits")]

        [DataType(DataType.PhoneNumber, ErrorMessage = "*  No characters are allowed")]
        public string Othercontactnumber { get; set; }

        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid e-mail adress")]
        [Display(Name = "Emailaddress")]
        public string Emailaddress { get; set; }

        [Display(Name = "Occupation")]
        [StringLength(50)]
        public string Occupation { get; set; }

        [Display(Name = "Income Range")]
        [StringLength(50)]
        public string IncomeRange { get; set; }

        public int BankId { get; set; }

        public string BankName { get; set; }

        //[Required(ErrorMessage = "* Bank Code is required")]
        [Display(Name = "Bank Code")]
        [StringLength(100)]
        public string BankNumber { get; set; }

        //[Required(ErrorMessage = "* Branch Name is required")]
        [Display(Name = "Branch Name")]
        [StringLength(100)]
        public string BranchName { get; set; }

        //[Required(ErrorMessage = "* Branch Code is required")]
        [Display(Name = "Branch Code")]
        [StringLength(100)]
        public string BranchNumber { get; set; }

        public IEnumerable<Bank> BanksList { get; set; }

        [Required(ErrorMessage = "* Bank Account Number is required")]
        [Display(Name = "BankAccountNumber")]
        [StringLength(20)]
        public string BankAccountNumber { get; set; }

        //[Required(ErrorMessage = "* Bank Account Name is required")]
        [Display(Name = "BankAccountName")]
        [StringLength(100)]
        [Compare("FullName", ErrorMessage = "* Account name should be same as full name ")]
        public string BankAccountName { get; set; }

        public IEnumerable<SecurityQuestion> SecurityQuestionList { get; set; }

        public int? SecurityQuestion1 { get; set; }
        public int? SecurityQuestion2 { get; set; }

        public string SecurityQuestion1Name { get; set; }
        public string SecurityQuestion2name { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer1")]
        [StringLength(200)]
        public string SecurityAnswer1 { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer2")]
        [StringLength(200)]
        public string SecurityAnswer2 { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> _Titles { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _Nationality { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _Gender { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _IncomeRange { get; set; }
        public string UserVerificationStatus { get; set; }
        /// <summary>
        /// user verification indicator///////////////
        /// </summary>
        public string TitleVerificationIndicator { get; set; }
        public string FullNameIndicator { get; set; }
        public string ICNumberIndicator { get; set; }
        public string NationalityIndicator { get; set; }
        public string PRStatusIndicator { get; set; }
        public string DateofBirthIndicator { get; set; }
        public string GenderIndicator { get; set; }
        public string ResidentialAddressIndicator { get; set; }
        public string NatureofBussinessIndicator { get; set; }
        public string DateofIncorporationIndicator { get; set; }
        public string RegisteredAddressIndicator { get; set; }
        public string PostalCodeIndicator { get; set; }
        public string MainContactNumberIndicator { get; set; }
        public string OthercontactnumberIndicator { get; set; }
        public string EmailaddressIndicator { get; set; }
        public string OccupationIndicator { get; set; }
        public string IncomeRangeIndicator { get; set; }
        public string BankIdIndicator { get; set; }
        public string BankNameIndicator { get; set; }
        public string BanksListIndicator { get; set; }
        public string BankAccountNumberIndicator { get; set; }

        public string BankAccountNameIndicator { get; set; }
        public string CommentsOnField { get; set; }


    }

    public class InvestorDocuments
    {
        public Int64 DocumentId { get; set; }
        public string DocumentName { get; set; }
        public string DocumentPath { get; set; }
        public Nullable<bool> Documentstatus { get; set; }
        public string Documenttype { get; set; }
        public long Userid { get; set; }
        public string Comment { get; set; }
        public IList<commentlist> CommentsList { get; set; }



    }

    public class InvestorAccount
    {
        public InvestorAccount(InvestorAccountDetailsModel InvestorAccountDetailsModel, InvestorDocuments InvestorDocuments, Knowledgeassessment Knowledgeassessment)
        {
            this.InvestorAccountDetailsModel = InvestorAccountDetailsModel;
            this.InvestorDocuments = InvestorDocuments;
            this.Knowledgeassessment = Knowledgeassessment;
        }

        public InvestorAccount(InvestorAccountDetailsModel InvestorAccountDetailsModel, Knowledgeassessment Knowledgeassessment)
        {
            this.InvestorAccountDetailsModel = InvestorAccountDetailsModel;
            this.Knowledgeassessment = Knowledgeassessment;
        }

        public InvestorAccountDetailsModel InvestorAccountDetailsModel { get; set; }
        public InvestorDocuments InvestorDocuments { get; set; }
        public Knowledgeassessment Knowledgeassessment { get; set; }
        public string Step { get; set; }


    }

    public class Knowledgeassessment
    {
        public IEnumerable<KnowledgeAssessmentParent> Parentlist { get; set; }
        public IEnumerable<KnowledgeAssessmentOption> Optionlist { get; set; }

    }

    public class KnowledgeassessmentWrapper
    {
        public Knowledgeassessment Knowledgeassessment { get; set; }
        public string Userid { get; set; }
    }


    #endregion
}