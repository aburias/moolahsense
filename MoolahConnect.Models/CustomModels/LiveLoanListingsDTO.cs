﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnect.Models.CustomModels
{
    public class LiveLoanListingsDTO
    {
        public List<LoanListings> LoanListings { get; set; }
        public List<FundedLoansListings> FundedListings { get; set; }
        public List<InvestorMaturedLoans> MaturedListings { get; set; }
    }

}
public class LoanListings
{
    public long LoanRequestID { get; set; }
    public string Company { get; set; }
    public string Purpose { get; set; }
    public string Tenor { get; set; }
    public decimal? InterestRate { get; set; }
    public decimal? LoanAmount { get; set; }
    public string PaymentGrade { get; set; }
    public string PG { get; set; }
    public int Days { get; set; }
    public int Hours { get; set; }
    public decimal? Funded { get; set; }
    public int Lenders { get; set; }


}
public class FundedLoansListings
{
    public long LoanRequestID { get; set; }

    public string Purpose { get; set; }
    public string Tenor { get; set; }
    public decimal? InterestRate { get; set; }
    public decimal? LoanAmount { get; set; }
    public string PaymentGrade { get; set; }
    public string PG { get; set; }
    public decimal? Funded { get; set; }
    public string MaturityDate { get; set; }
    public string Referece { get; set; }
}

public class InvestorMaturedLoans
{
    public long LoanReqestID { get; set; }
    public string Purpose { get; set; }
    public string Tenor { get; set; }
    public decimal? LoanAmount { get; set; }
    public string PaymentGrade { get; set; }
    public string PG { get; set; }
    public decimal? Interest { get; set; }
    public string MaturityDate { get; set; }
    public int NumberofLenders { get; set; }
    public string Referece { get; set; }
}