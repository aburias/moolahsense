﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoolahConnect.Models.CustomModels
{
    public class UsersSummaryDTO
    {
        public int TotalUsers { get; set; }
        public int TotalBorrower { get; set; }
        public int TotalInvestors { get; set; }
    }
}
