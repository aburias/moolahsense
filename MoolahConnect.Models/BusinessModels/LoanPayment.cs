using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanPayments")]
    public class LoanPayment
    {
        public int ID { get; set; }

        public long LoanRequestID { get; set; }

        public long? RepaymentID { get; set; }

        public decimal? Amount { get; set; }

        public DateTime PaidDate { get; set; }

        public DateTime TimeStamp { get; set; }

        public decimal? LateFees { get; set; }

        public virtual LoanAmortization tbl_LoanAmortization { get; set; }
    }
}