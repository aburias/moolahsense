using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_BanksList")]
    public class Bank
    {
        public Bank()
        {
            tbl_AccountDetails = new HashSet<AccountDetail>();
        }

        [Key]
        public int BankId { get; set; }

        [StringLength(200)]
        public string BankName { get; set; }

        public bool? Isactive { get; set; }

        public virtual ICollection<AccountDetail> tbl_AccountDetails { get; set; }
    }
}