using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_WithDrawMoney")]
    public class WithdrawMoney
    {
        public WithdrawMoney()
        {
            tbl_WithDrawAdminNotes = new HashSet<WithdrawAdminNote>();
        }

        [Key]
        public long WithDrawID { get; set; }

        public long? User_ID { get; set; }

        public decimal? WithDrawAmount { get; set; }

        [StringLength(16)]
        public string Status { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? ActionbyAdmin { get; set; }

        public long? AdminID { get; set; }

        public decimal? CurrentBalance { get; set; }

        public decimal? AfterBalance { get; set; }

        public virtual User tbl_Users { get; set; }

        public virtual ICollection<WithdrawAdminNote> tbl_WithDrawAdminNotes { get; set; }
    }
}