using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_PersonalGuaranteeInfo")]
    public class PersonalGuaranteeInfo
    {
        [Key]
        public int InfoId { get; set; }

        [StringLength(200)]
        public string NameAsinNRIC { get; set; }

        [StringLength(200)]
        public string Designation { get; set; }

        [StringLength(200)]
        public string NRIC_Passport { get; set; }

        [StringLength(200)]
        public string PassportNumber { get; set; }

        [StringLength(500)]
        public string ResidentialAddress { get; set; }

        [StringLength(20)]
        public string Telephone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        public string Reasons { get; set; }

        public long? Request_Id { get; set; }

        [StringLength(10)]
        public string Postalcode { get; set; }

        [StringLength(50)]
        public string Nationality { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }
    }
}