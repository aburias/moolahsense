using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanContracts")]
    public class LoanContract
    {
        [Key]
        [StringLength(30)]
        public string Reference { get; set; }

        public long LoanRequestId { get; set; }

        public long InvestorId { get; set; }
    }
}