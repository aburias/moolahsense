using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Knowledgeassessmentdetails_ForUser")]
    public class KnowledgeAssessmentUserDetail
    {
        [Key]
        public int RecId { get; set; }

        public int? Option_Id { get; set; }

        [StringLength(50)]
        public string Option_Value { get; set; }

        public long? User_ID { get; set; }

        public DateTime? DateCreated { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}