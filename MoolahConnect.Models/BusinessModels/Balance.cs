using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Balances")]
    public class Balance
    {
        [Key]
        public int BalanceId { get; set; }

        public decimal? ActualAmount { get; set; }

        public long? User_ID { get; set; }

        public DateTime? DateCreated { get; set; }

        public decimal? LedgerAmount { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}