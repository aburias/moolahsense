﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoolahConnect.Models.Interfaces;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Users")]
    public class User : IEntity
    {
        public User()
        {
            AccountDetails = new HashSet<AccountDetail>();
            tbl_Balances = new HashSet<Balance>();
            tbl_Comments = new HashSet<Comment>();
            tbl_Knowledgeassessmentdetails_ForUser = new HashSet<KnowledgeAssessmentUserDetail>();
            tbl_LoanFundsDetails = new HashSet<LoanFundsDetail>();
            tbl_Loanoffers = new HashSet<Loanoffer>();
            tbl_LoanRequests = new HashSet<LoanRequest>();
            tbl_LoanTransactions = new HashSet<LoanTransaction>();
            tbl_LoanTransactions1 = new HashSet<LoanTransaction>();
            tbl_MoolahCore = new HashSet<MoolahCore>();
            tbl_MoolahPeri = new HashSet<MoolahPeri>();
            tbl_OutStandingLitigation = new HashSet<OutStandingLitigation>();
            tbl_SecurityQuestionsForUsers = new HashSet<UserSecurityQuestion>();
            tbl_WithDrawMoney = new HashSet<WithdrawMoney>();
        }

        [Key]
        public long UserID { get; set; }

        [StringLength(50)]
        public string UserName { get; set; }

        [StringLength(50)]
        public string Password { get; set; }

        public bool? Isactive { get; set; }

        [StringLength(50)]
        public string UserRole { get; set; }

        public DateTime? DateCreated { get; set; }

        public bool? SubscribeforUpdates { get; set; }

        [StringLength(32)]
        public string InvestorType { get; set; }

        [StringLength(16)]
        public string AdminVerification { get; set; }

        public bool? IsSubmitted { get; set; }

        public Guid AspnetUserId { get; set; }

        public bool? IsEmailVerified { get; set; }

        [StringLength(128)]
        public string DigitalSignature { get; set; }

        public virtual ICollection<AccountDetail> AccountDetails { get; set; }

        public virtual ICollection<Balance> tbl_Balances { get; set; }

        public virtual ICollection<Comment> tbl_Comments { get; set; }

        public virtual ICollection<KnowledgeAssessmentUserDetail> tbl_Knowledgeassessmentdetails_ForUser { get; set; }

        public virtual ICollection<LoanFundsDetail> tbl_LoanFundsDetails { get; set; }

        public virtual ICollection<Loanoffer> tbl_Loanoffers { get; set; }

        public virtual ICollection<LoanRequest> tbl_LoanRequests { get; set; }

        public virtual ICollection<LoanTransaction> tbl_LoanTransactions { get; set; }

        public virtual ICollection<LoanTransaction> tbl_LoanTransactions1 { get; set; }

        public virtual ICollection<MoolahCore> tbl_MoolahCore { get; set; }

        public virtual ICollection<MoolahPeri> tbl_MoolahPeri { get; set; }

        public virtual ICollection<OutStandingLitigation> tbl_OutStandingLitigation { get; set; }

        public virtual ICollection<UserSecurityQuestion> tbl_SecurityQuestionsForUsers { get; set; }

        public virtual ICollection<WithdrawMoney> tbl_WithDrawMoney { get; set; }
    }
}