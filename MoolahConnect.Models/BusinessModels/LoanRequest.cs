using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoolahConnect.Models.Interfaces;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanRequests")]
    public class LoanRequest : IEntity
    {
        public LoanRequest()
        {
            Comments = new HashSet<Comment>();
            LoanAmortizations = new HashSet<LoanAmortization>();
            LoanFundsDetails = new HashSet<LoanFundsDetail>();
            Loanoffers = new HashSet<Loanoffer>();
            LoanRequestInvestorMessages = new HashSet<LoanRequestInvestorMessage>();
            MoolahCores = new HashSet<MoolahCore>();
            LoanTransactions = new HashSet<LoanTransaction>();
            MoolahCoreVerifications = new HashSet<MoolahCoreVerification>();
            MoolahPeris = new HashSet<MoolahPeri>();
            MoolahPeriVerifications = new HashSet<MoolahPeriVerification>();
            PersonalGuaranteeInfos = new HashSet<PersonalGuaranteeInfo>();
            RepaymentMessages = new HashSet<RepaymentMessage>();
            WatchList = new HashSet<WatchList>();
            Files = new List<File>();
        }

        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long RequestId { get; set; }

        public decimal? Amount { get; set; }

        public decimal? FinalAmount { get; set; }

        [StringLength(50)]
        public string Terms { get; set; }

        public decimal? EIR { get; set; }

        public decimal? Rate { get; set; }

        public bool? IsotherLoanType { get; set; }

        [StringLength(2000)]
        public string Question { get; set; }

        public bool? IsPersonalGuarantee { get; set; }

        public long? User_ID { get; set; }

        [ForeignKey("User_ID")]
        public User User { get; set; }

        public bool? IsApproved { get; set; }

        [StringLength(20)]
        public string PaymentTerms { get; set; }

        public string VideoDescription { get; set; }

        public int LoanStatus { get; set; }

        public DateTime? DateCreated { get; set; }

        public int? LoanPurpose_Id { get; set; }

        public DateTime? ApproveDate { get; set; }

        public DateTime? AcceptedDate { get; set; }

        public decimal? MoolahFees { get; set; }

        public string SummaryCompanyPro { get; set; }

        public string DetailedCompanyPro { get; set; }

        public bool? Submitted { get; set; }

        public int? NumberOfEmployees { get; set; }

        public decimal RequestedRate { get; set; }

        public int PreliminaryLoanStatus { get; set; }

        public DateTime? PublishedDate { get; set; }

        [Required]
        [StringLength(500)]
        public string LoanPurposeDescription { get; set; }

        public string MoolahSenseComments { get; set; }

        public DateTime? TransferDate { get; set; }

        [StringLength(100)]
        public string ResonReject { get; set; }

        [StringLength(200)]
        public string Headline { get; set; }

        public virtual ICollection<Comment> Comments { get; set; }

        public virtual ICollection<LoanAmortization> LoanAmortizations { get; set; }

        public virtual ICollection<LoanFundsDetail> LoanFundsDetails { get; set; }

        public virtual ICollection<Loanoffer> Loanoffers { get; set; }

        public virtual LoanPurpose LoanPurposes { get; set; }

        public virtual ICollection<LoanRequestInvestorMessage> LoanRequestInvestorMessages { get; set; }

        public virtual ICollection<MoolahCore> MoolahCores { get; set; }

        public virtual User Users { get; set; }

        public virtual ICollection<LoanTransaction> LoanTransactions { get; set; }

        public virtual ICollection<MoolahCoreVerification> MoolahCoreVerifications { get; set; }

        public virtual ICollection<MoolahPeri> MoolahPeris { get; set; }

        public virtual ICollection<MoolahPeriVerification> MoolahPeriVerifications { get; set; }

        public virtual ICollection<PersonalGuaranteeInfo> PersonalGuaranteeInfos { get; set; }

        public virtual ICollection<RepaymentMessage> RepaymentMessages { get; set; }

        public virtual ICollection<WatchList> WatchList { get; set; }
        public virtual ICollection<File> Files { get; set; }
    }
}