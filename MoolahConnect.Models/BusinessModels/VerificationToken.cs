using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("u_VerificationToken")]
    public class VerificationToken
    {
        [Key]
        public int TokenId { get; set; }

        public Guid Token { get; set; }

        public DateTime CreatedDate { get; set; }

        public Guid UserId { get; set; }
    }
}