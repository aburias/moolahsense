using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Comments")]
    public class Comment
    {
        [Key]
        public long CommentId { get; set; }

        public long? Request_Id { get; set; }

        public long? Commentor_Id { get; set; }

        public DateTime? DateCreated { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}