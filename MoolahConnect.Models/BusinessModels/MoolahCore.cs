using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_MoolahCore")]
    public class MoolahCore
    {
        public MoolahCore()
        {
            OutStandingLitigations = new HashSet<OutStandingLitigation>();
        }

        [Key]
        public long MoolahCoreId { get; set; }

        public long? LoanRequest_Id { get; set; }

        public long? User_ID { get; set; }

        public decimal? Turnovers_LY { get; set; }

        public decimal? NetprofitafterTax_LY { get; set; }

        public decimal? CurrentAssests_LY { get; set; }

        public decimal? CurrentLiabilities_LY { get; set; }

        public decimal? CurrentRatio_LY { get; set; }

        public decimal? TotalDebt_LY { get; set; }

        public decimal? TotalShareholdersequity_LY { get; set; }

        public decimal? Debt_Equity_LY { get; set; }

        public decimal? CashFlowfromOperations_LY { get; set; }

        public DateTime? Datecreated { get; set; }

        public decimal? Turnovers_PY { get; set; }

        public decimal? NetprofitafterTax_PY { get; set; }

        public decimal? CurrentAssests_PY { get; set; }

        public decimal? CurrentLiabilities_PY { get; set; }

        public decimal? CurrentRatio_PY { get; set; }

        public decimal? TotalDebt_PY { get; set; }

        public decimal? TotalShareholdersequity_PY { get; set; }

        public decimal? Debt_Equity_PY { get; set; }

        public decimal? CashFlowfromOperations_PY { get; set; }

        public bool? IsoutstandingLitigation { get; set; }

        public string LatestYear { get; set; }

        public string PreiviousYear { get; set; }

        public bool? Profitable { get; set; }

        public bool? Profitable_PY { get; set; }

        public int? DPCreditPaymentGrade { get; set; }

        public decimal? Gearing { get; set; }

        public decimal? AvgThreeMntCashBalBankAcc_LY { get; set; }

        public decimal? AvgThreeMntCashBalBankAcc_PY { get; set; }

        public decimal? ProfitBeforIntAndTax_LY { get; set; }

        public decimal? ProfitBeforIntAndTax_PY { get; set; }

        public decimal? InterestExpense_LY { get; set; }

        public decimal? InterestExpense_PY { get; set; }

        public decimal? InterestCoverageRatio_LY { get; set; }

        public decimal? InterestCoverageRatio_PY { get; set; }

        //public decimal? EBITDA_LY { get; set; }

        //public decimal? EBITDA_PY { get; set; }

        //public decimal? DebtEBITDARatio_LY { get; set; }

        //public decimal? DebtEBITDARatio_PY { get; set; }

        public virtual LoanRequest LoanRequest { get; set; }

        public virtual User User { get; set; }

        public virtual ICollection<OutStandingLitigation> OutStandingLitigations { get; set; }
    }
}