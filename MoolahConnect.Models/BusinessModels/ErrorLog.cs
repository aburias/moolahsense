using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_ErrorLog")]
    public class ErrorLog
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Level { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        [StringLength(200)]
        public string UserName { get; set; }

        public string StackTrace { get; set; }

        public string InnerMessage { get; set; }
    }
}