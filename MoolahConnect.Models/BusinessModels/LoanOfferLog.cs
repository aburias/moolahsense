using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanOffersLog")]
    public class LoanOfferLog
    {
        [Key]
        public int OfferLogID { get; set; }

        public int OfferStatus { get; set; }

        public DateTime TimeStamp { get; set; }

        public int OfferID { get; set; }
    }
}