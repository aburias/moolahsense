using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Files")]
    public class File
    {
        public int Id { get; set; }

        [Required]
        [StringLength(250)]
        public string FileName { get; set; }

        [Required]
        [StringLength(50)]
        public string Type { get; set; }

        public Guid UserId { get; set; }

        public DateTime CreatedDate { get; set; }

        [StringLength(500)]
        public string Comments { get; set; }

        public long? RequestId { get; set; }

        [ForeignKey("RequestId")]
        public LoanRequest LoanRequests { get; set; }

        public int? OfferId { get; set; }
    }
}