using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Knowledgeassessment_OptionsList")]
    public class KnowledgeAssessmentOption
    {
        [Key]
        public int Optionid { get; set; }

        [StringLength(500)]
        public string Optiontext { get; set; }

        public int? Parent_Id { get; set; }

        public bool? Isactive { get; set; }

        [StringLength(50)]
        public string Optiontype { get; set; }

        [StringLength(300)]
        public string url { get; set; }

        public virtual KnowledgeAssessmentParent tbl_Knowledgeassessment_Parentlist { get; set; }
    }
}