namespace MoolahConnect.Models.BusinessModels
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    [Table("tbl_AccountDetails")]
    public partial class AccountDetail
    {
        [Key]
        public long AccountId { get; set; }

        [StringLength(10)]
        public string Title { get; set; }

        [StringLength(100)]
        public string FirstName { get; set; }

        [StringLength(100)]
        public string LastName { get; set; }

        [StringLength(100)]
        public string DisplayName { get; set; }

        [StringLength(50)]
        public string AccountType { get; set; }

        public long? User_ID { get; set; }

        [StringLength(100)]
        public string Nationality { get; set; }

        public bool? PRstatus { get; set; }

        public DateTime? DateofBirth { get; set; }

        [StringLength(20)]
        public string Gender { get; set; }

        [StringLength(500)]
        public string ResidentialAddress { get; set; }

        [StringLength(10)]
        public string PostalCode { get; set; }

        [StringLength(20)]
        public string OptionalContactnumber { get; set; }

        [StringLength(100)]
        public string Occupation { get; set; }

        [StringLength(50)]
        public string IncomeRange { get; set; }

        [StringLength(100)]
        public string BankBranch { get; set; }

        [StringLength(50)]
        public string AccountNumber { get; set; }

        [StringLength(200)]
        public string AccountName { get; set; }

        [StringLength(500)]
        public string LevelofEducation { get; set; }

        [StringLength(500)]
        public string FinancialInvestmentProducts { get; set; }

        [StringLength(500)]
        public string MoolahConnectInvestment { get; set; }

        public string InvestmentObjectives { get; set; }

        [StringLength(500)]
        public string ReactionOndefaulted { get; set; }

        [StringLength(200)]
        public string RoleJobTitle { get; set; }

        [StringLength(500)]
        public string BusinessName { get; set; }

        [StringLength(200)]
        public string BusinessOrganisation { get; set; }

        public int? Natureofbusiness_Id { get; set; }

        public DateTime? DateofIncorporation { get; set; }

        [StringLength(500)]
        public string RegisteredAddress { get; set; }

        [StringLength(300)]
        public string BusinessMailingAddress { get; set; }

        public int? Bank_Id { get; set; }

        [StringLength(20)]
        public string Main_OfficeContactnumber { get; set; }

        [StringLength(20)]
        public string Mobilenumber { get; set; }

        [StringLength(20)]
        public string IC_CompanyRegistrationNumber { get; set; }

        public double? PaidUpCapital { get; set; }

        public int? NumOfEmployees { get; set; }

        [StringLength(100)]
        public string NRIC_Number { get; set; }

        [StringLength(100)]
        public string PassportNumber { get; set; }

        [StringLength(50)]
        public string SiccCode { get; set; }

        [StringLength(100)]
        public string BranchName { get; set; }

        [StringLength(100)]
        public string BranchNumber { get; set; }

        [StringLength(100)]
        public string BankNumber { get; set; }

        public virtual Bank Bank { get; set; }

        public virtual NatureofBusiness NatureofBusiness { get; set; }

        public virtual User User { get; set; }
    }
}
