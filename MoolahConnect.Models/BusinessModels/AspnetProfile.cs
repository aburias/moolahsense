using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("aspnet_Profile")]
    public class AspnetProfile
    {
        [Key]
        public Guid UserId { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string PropertyNames { get; set; }

        [Column(TypeName = "ntext")]
        [Required]
        public string PropertyValuesString { get; set; }

        [Column(TypeName = "image")]
        [Required]
        public byte[] PropertyValuesBinary { get; set; }

        public DateTime LastUpdatedDate { get; set; }
    }
}