using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_AuditLog")]
    public class AuditLog
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Level { get; set; }

        public string Message { get; set; }

        public DateTime Timestamp { get; set; }

        [Required]
        [StringLength(200)]
        public string UserName { get; set; }

        public long? LoanRequestId { get; set; }

        [Required]
        [StringLength(30)]
        public string Reference { get; set; }
    }
}