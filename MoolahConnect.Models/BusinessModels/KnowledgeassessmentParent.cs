using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Knowledgeassessment_Parentlist")]
    public class KnowledgeAssessmentParent
    {
        public KnowledgeAssessmentParent()
        {
            tbl_Knowledgeassessment_OptionsList = new HashSet<KnowledgeAssessmentOption>();
        }

        [Key]
        public int ParentId { get; set; }

        [StringLength(500)]
        public string Parenttext { get; set; }

        public int? Priority { get; set; }

        public bool? Isactive { get; set; }

        [StringLength(1)]
        public string InvType { get; set; }

        public virtual ICollection<KnowledgeAssessmentOption> tbl_Knowledgeassessment_OptionsList { get; set; }
    }
}