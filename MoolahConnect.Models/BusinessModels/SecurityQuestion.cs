using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_SecurityQuestions")]
    public class SecurityQuestion
    {
        [Key]
        public int QuestionId { get; set; }

        [StringLength(500)]
        public string QuestionName { get; set; }

        public DateTime? DateCreated { get; set; }

        public bool? Status { get; set; }
    }
}