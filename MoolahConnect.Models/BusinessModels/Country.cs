using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Countries")]
    public class Country
    {
        [Key]
        public int CountryID { get; set; }

        [StringLength(2)]
        public string ISO2 { get; set; }

        [StringLength(128)]
        public string CountryName { get; set; }

        [StringLength(128)]
        public string LongCountryName { get; set; }

        [StringLength(3)]
        public string ISO3 { get; set; }

        [StringLength(8)]
        public string NumCode { get; set; }

        [StringLength(16)]
        public string UNMemberState { get; set; }

        [StringLength(8)]
        public string CallingCode { get; set; }

        [StringLength(8)]
        public string CCTLD { get; set; }
    }
}