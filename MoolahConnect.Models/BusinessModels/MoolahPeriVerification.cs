using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_MoolahPeriVerification")]
    public class MoolahPeriVerification
    {
        [Key]
        [Column("MoolahPeriVerification")]
        public long MoolahPeriVerificationID { get; set; }

        public int? BussinessAwardsVerification { get; set; }

        public int? AcrediationVerification { get; set; }

        public int? MTAVerificationVerification { get; set; }

        public int? DigitalFootPrintVerification { get; set; }

        public int? CommunityVerification { get; set; }

        public int? BiodataofDSVerification { get; set; }

        public int? MoolahPerkVerification { get; set; }

        public long? Request_ID { get; set; }

        public DateTime? DateCreated { get; set; }

        public string AdminComments { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }
    }
}