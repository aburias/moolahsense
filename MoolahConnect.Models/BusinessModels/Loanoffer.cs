using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_Loanoffers")]
    public class Loanoffer
    {
        [Key]
        public int OfferId { get; set; }

        public long Investor_Id { get; set; }

        public long LoanRequest_Id { get; set; }

        public decimal OfferedAmount { get; set; }

        public decimal? AcceptedAmount { get; set; }

        public decimal OfferedRate { get; set; }

        public decimal? AcceptedRate { get; set; }

        public DateTime DateCreated { get; set; }

        public int OfferStatus { get; set; }

        public DateTime? ModifiedDate { get; set; }

        [StringLength(250)]
        public string ReasonToReject { get; set; }

        [StringLength(50)]
        public string ContractId { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}