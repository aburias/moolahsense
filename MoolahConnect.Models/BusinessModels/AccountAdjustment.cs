using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_AccountAdjustment")]
    public class AccountAdjustment
    {
        public long ID { get; set; }

        public decimal TotalReceivable { get; set; }

        public decimal Difference { get; set; }

        public long RepaymentID { get; set; }

        public bool? Adjusted { get; set; }

        public DateTime? AdjustedDate { get; set; }

        public DateTime TimeStamp { get; set; }

        public virtual LoanAmortization tbl_LoanAmortization { get; set; }
    }
}