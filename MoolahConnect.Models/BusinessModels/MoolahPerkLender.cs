using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_MoolahPerksForLenders")]
    public class MoolahPerkLender
    {
        [Key]
        public long MoolahPerkId { get; set; }

        [StringLength(500)]
        public string MoolahPerk { get; set; }

        public DateTime? DateCreated { get; set; }

        public bool? Isactive { get; set; }
    }
}