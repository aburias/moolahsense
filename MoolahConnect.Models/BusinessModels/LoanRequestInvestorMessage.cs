using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanRequest_messages_By_Investors")]
    public class LoanRequestInvestorMessage
    {
        [Key]
        public long MessageID { get; set; }

        public long? MessageFrom { get; set; }

        public long? MessageTo { get; set; }

        public long? ChildOf { get; set; }

        public bool? IsRead { get; set; }

        public string Message { get; set; }

        public long? LoanRequestID { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? ReplyDate { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }
    }
}