using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_loanRequestReference")]
    public class LoanRequestReference
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long RequestId { get; set; }
    }
}