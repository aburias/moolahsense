using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_AuditTrial")]
    public class AuditTrial
    {
        [Key]
        public long AuditTrialID { get; set; }

        public long? UserID { get; set; }

        public string ColumnChanged { get; set; }

        public string ColumnOldValue { get; set; }

        public string ColumnNewValue { get; set; }

        public DateTime? DateCreated { get; set; }

        [StringLength(64)]
        public string AdminUserName { get; set; }

        public virtual AuditTrial tbl_AuditTrial1 { get; set; }

        public virtual AuditTrial tbl_AuditTrial2 { get; set; }
    }
}