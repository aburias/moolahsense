using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_WithDrawAdminNotes")]
    public class WithdrawAdminNote
    {
        [Key]
        public long AdminNoteID { get; set; }

        public long WithDraw_ID { get; set; }

        public string Note { get; set; }

        public DateTime DateCreated { get; set; }

        public long? AdminID { get; set; }

        public virtual WithdrawMoney tbl_WithDrawMoney { get; set; }
    }
}