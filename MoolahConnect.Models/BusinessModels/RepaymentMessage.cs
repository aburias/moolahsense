using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_RepaymentMessages")]
    public class RepaymentMessage
    {
        public long ID { get; set; }

        public long? LoanRequestId { get; set; }

        [StringLength(250)]
        public string Message { get; set; }

        public DateTime? CreatedDate { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }
    }
}