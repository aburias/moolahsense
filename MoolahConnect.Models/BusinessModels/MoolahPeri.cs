using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_MoolahPeri")]
    public class MoolahPeri
    {
        [Key]
        public long MoolahPeriId { get; set; }

        [StringLength(500)]
        public string BusinessAwards { get; set; }

        [StringLength(500)]
        public string Accreditation { get; set; }

        [StringLength(500)]
        public string MTA { get; set; }

        [StringLength(500)]
        public string DigitalFootPrint { get; set; }

        [StringLength(500)]
        public string CommunityInitiative { get; set; }

        public string BiodataOfDS { get; set; }

        public long? MoolahPerk_Id { get; set; }

        public string MoolahPerk_Custom { get; set; }

        public long? Request_ID { get; set; }

        public long? User_ID { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        public bool? UpdateMoolahPost { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}