using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanPurposesList")]
    public class LoanPurpose
    {
        public LoanPurpose()
        {
            tbl_LoanRequests = new HashSet<LoanRequest>();
        }

        [Key]
        public int LoanPurposeId { get; set; }

        [StringLength(200)]
        public string PurposeName { get; set; }

        public bool? Isactive { get; set; }

        public virtual ICollection<LoanRequest> tbl_LoanRequests { get; set; }
    }
}