using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_SecurityQuestionsForUsers")]
    public class UserSecurityQuestion
    {
        [Key]
        public long RecId { get; set; }

        public int? Question_Id { get; set; }

        [StringLength(100)]
        public string Answer { get; set; }

        public long? User_ID { get; set; }

        public DateTime? DateCreated { get; set; }

        public DateTime? DateUpdated { get; set; }

        [StringLength(16)]
        public string IsVerified { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}