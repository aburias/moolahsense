using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_WatchList")]
    public class WatchList
    {
        [Key]
        public long WatchListID { get; set; }

        public long? User_ID { get; set; }

        public long? Request_ID { get; set; }

        public DateTime? Datecreated { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }
    }
}