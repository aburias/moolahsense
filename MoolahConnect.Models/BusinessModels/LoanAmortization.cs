using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using MoolahConnect.Models.Interfaces;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanAmortization")]
    public class LoanAmortization : IEntity
    {
        public LoanAmortization()
        {
            tbl_AccountAdjustment = new HashSet<AccountAdjustment>();
            tbl_LoanPayments = new HashSet<LoanPayment>();
        }

        [Key]
        public long AmortizationID { get; set; }

        public long? LoanRequest_ID { get; set; }

        [ForeignKey("LoanRequest_ID")]
        public LoanRequest LoanRequest { get; set; }

        public DateTime? EmiDate { get; set; }

        public DateTime? PayedOn { get; set; }

        public decimal? EmiAmount { get; set; }

        public decimal? Interest { get; set; }

        public decimal? AmountTotal { get; set; }

        public decimal? Balance { get; set; }

        public int? PayStatus { get; set; }

        public bool? Debited { get; set; }

        public DateTime? LateStartDate { get; set; }

        public int? LateInterestPayStatus { get; set; }

        public DateTime? LateInterestPaidDate { get; set; }

        public decimal? LateInterest { get; set; }

        public decimal? Principal { get; set; }

        public decimal? PaidAmount { get; set; }

        public decimal? LateFees { get; set; }

        //public bool? IsReminderSent { get; set; }

        public virtual ICollection<AccountAdjustment> tbl_AccountAdjustment { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }

        public virtual ICollection<LoanPayment> tbl_LoanPayments { get; set; }
    }
}