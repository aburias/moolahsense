using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_NatureofBusinessList")]
    public class NatureofBusiness
    {
        public NatureofBusiness()
        {
            tbl_AccountDetails = new HashSet<AccountDetail>();
        }

        [Key]
        public int NatureofBusinessId { get; set; }

        [StringLength(500)]
        public string NatureofBusinessName { get; set; }

        public bool? Isactive { get; set; }

        public virtual ICollection<AccountDetail> tbl_AccountDetails { get; set; }
    }
}