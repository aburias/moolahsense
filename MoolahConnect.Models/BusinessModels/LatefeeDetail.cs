
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LatefeeDetails")]
    public class LatefeeDetail
    {
        [Key]
        public int LatefeeId { get; set; }

        public int? MinRangeofTime { get; set; }

        public int? MaxRangeofTime { get; set; }

        public decimal? PenaltyPercent { get; set; }
    }
}