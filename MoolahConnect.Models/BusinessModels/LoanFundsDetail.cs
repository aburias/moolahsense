using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MoolahConnect.Models.BusinessModels
{
    [Table("tbl_LoanFundsDetails")]
    public class LoanFundsDetail
    {
        [Key]
        public long RecId { get; set; }

        public long? Request_Id { get; set; }

        public long? User_ID { get; set; }

        public decimal? AmountFunded { get; set; }

        public DateTime? DateCreated { get; set; }

        public virtual LoanRequest tbl_LoanRequests { get; set; }

        public virtual User tbl_Users { get; set; }
    }
}