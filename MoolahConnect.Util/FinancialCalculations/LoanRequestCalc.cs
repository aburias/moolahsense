﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.Logging;

namespace MoolahConnect.Util.FinancialCalculations
{
    public static class LoanRequestCalc
    {
        public static double CalculateTargetEIR(double amount, string term, double targetRate)
        {
            double periodicInterestRate1 = 0;
            double periodicInterestRate2 = 0;
            double tenorinYears = 0.0;

            double monthlyRepayment = 0;
            double principalAmount = 0;
            double moolahFees = 0;

            int totalNoOfPayments = 0;

            double targetEIR = 0;
            try
            {
                tenorinYears = int.Parse(term) / 12;
                moolahFees = amount * Settings.MoolahFees * (int.Parse(term) / 12);
                totalNoOfPayments = (int)(Settings.PaymentsPerYear * int.Parse(term) / 12);
                principalAmount = amount - moolahFees;

                periodicInterestRate1 = targetRate / Settings.PaymentsPerYear;

                monthlyRepayment = Financial.Pmt(periodicInterestRate1 / 100, totalNoOfPayments, amount * -1);

                periodicInterestRate2 = Financial.Rate(totalNoOfPayments, monthlyRepayment, principalAmount * -1);

                targetEIR = (Math.Pow((1 + periodicInterestRate2), Settings.PaymentsPerYear) - 1) * 100;

                return Math.Round(targetEIR, 2);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static double CalculateMoolahFees(double amount, string tenor)
        {
            double moolahFees = 0;

            try
            {
                int tenorInYears = int.Parse(tenor) / 12;

                moolahFees = (amount * Settings.MoolahFees * tenorInYears);

                return moolahFees;
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public static double CalculateAccuredInterest(decimal? amount, DateTime? dueDate)
        {
            if (!amount.HasValue || !dueDate.HasValue)
            {
                return 0;
            }

            dueDate = DateTimeExtenstion.SingaporeTime(dueDate.Value).AddDays(1).Date;
            DateTime sinNowDate = DateTimeExtenstion.SingaporeTime(DateTime.Now).Date;

            double interest = 0;
            double amountWithinterest = (double)amount.Value;
            int days = sinNowDate.Subtract(dueDate.Value).Days + 1;

            for (int i = 1; i <= days; i++)
            {
                var interestTemp = (amountWithinterest) * (Settings.LatePayInterest / 100) / Settings.DayCountConvention;

                amountWithinterest += interestTemp;
                interest += interestTemp;
            }

            return Math.Round(interest, 2);
        }

        public static double CalculateLateFees(decimal? amount, DateTime? dueDate)
        {
            if (!amount.HasValue || !dueDate.HasValue)
            {
                return 0;
            }

            dueDate = DateTimeExtenstion.SingaporeTime(dueDate.Value).AddDays(1).Date;
            DateTime sinNowDate = DateTimeExtenstion.SingaporeTime().Date;

            double lateFees = 0;

            DateTime iterationDate = dueDate.Value;

            double lateFeesPerMonth = 0;

            if (amount / 100 < 200)
            {
                lateFeesPerMonth = 200;
            }
            else
            {
                lateFeesPerMonth = (double)amount / 100;
            }

            if (dueDate <= sinNowDate)
            {
                do
                {
                    lateFees += lateFeesPerMonth;
                    iterationDate = iterationDate.AddMonths(1);
                } while (iterationDate <= sinNowDate);
            }

            return Math.Round(lateFees, 2);
        }

        public static double RepaymentAmount(double amount, double rate, string Tenor)
        {
            try
            {
                double monthlyRepayment = 0;
                double periodicInterestRate = 0;
                int tenorinYears = 0;
                int totalNoOfPayments = 0;

                tenorinYears = int.Parse(Tenor) / 12;
                totalNoOfPayments = tenorinYears * Settings.PaymentsPerYear;
                periodicInterestRate = rate / Settings.PaymentsPerYear;

                monthlyRepayment = Financial.Pmt(periodicInterestRate / 100, totalNoOfPayments, amount * -1);

                return Math.Round(monthlyRepayment, 2);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return 0;
            }
        }
    }
}
