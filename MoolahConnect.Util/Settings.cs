﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace MoolahConnect.Util
{
    public static class GlobalVar
    {
        public const double gBorrowerMinAmount = 100000;
        public const double gBorrowerMaxAmount = 1000000;
        public const int gBorrowerStep = 1000;

        public const string gBorrowerTextErrorString = "* Enter amount in between 100K-1000K";
        public const string gBorrowergMinAmountString = "$100,000";
        public const string gBorrowerMaxAmountString = "$1,000,000";



        public const double gInvestorMinAmount = 1000;
        public const double gInvestorMaxAmount = 30000;
        public const int gInvestorStep = 1000;

        public const string gDataMsgRange = "Amount must be between S$100,000 and S$300,000";
        public const string gDataMsgIncrement = "Incremental amount by S$1,000";
        public const string gDataRuleRange = "[100000,1000000]";

        //public static int[] gTenure = { 6,12 };

        public static int[] gTenure = { 12 };

    }
    public static class Settings
    {



        static double moolahFees = 0;
        static double minThreshold = 0;
        static double minQuantum = 0;
        static double minLot = 0;
        static int minInvestors = 0;
        static int loanRequestExpiryDays = 0;
        static int loanDraftExpiryDays = 0;
        static double latePayInterest = 0;
        static int dayCountConvention = 0;
        private static int verificationRequestExpireTime;

        private static int smtpPort;
        private static bool enabledSSL;

        static int earmarkAccountBalanceID = 0;
        static int msenseAccountBalanceID = 0;
        static int fundTransferAccountBalanceID = 0;

        public static double LatePayInterest
        {
            get
            {
                return double.TryParse(ConfigurationManager.AppSettings["LatePayInterest"], out latePayInterest) == true ? latePayInterest
                    : 0;
            }
        }
        public static int DayCountConvention
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["DayCountConvention"], out dayCountConvention) == true ? dayCountConvention
                    : 0;
            }
        }

        public static int VerificationRequestExpireTime
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["VerificationRequestExpireTime"], out verificationRequestExpireTime) == true ? verificationRequestExpireTime
                    : 0;
            }
        }

        public static double MoolahFees
        {
            get { return double.TryParse(ConfigurationManager.AppSettings["MoolahFees"].ToString(), out moolahFees) ? moolahFees : 0; }
        }

        public static double MinThreshold
        {
            get { return double.TryParse(ConfigurationManager.AppSettings["MinThreshold"].ToString(), out minThreshold) ? minThreshold : 0; }
        }

        public static double MinQuantum
        {
            get { return double.TryParse(ConfigurationManager.AppSettings["MinQuantum"].ToString(), out minQuantum) ? minQuantum : 0; }
        }

        public static double MinLot
        {
            get { return double.TryParse(ConfigurationManager.AppSettings["MinLot"].ToString(), out minLot) ? minLot : 0; }
        }

        public static int MinInvestors
        {
            get { return int.TryParse(ConfigurationManager.AppSettings["minInvestors"].ToString(), out minInvestors) ? minInvestors : 0; }
        }

        public static int PaymentsPerYear
        {
            get { return 12; }
        }

        public static DateTime CurrentTime
        {
            get { return DateTime.UtcNow; }
        }

        public static string SmtpHost
        {
            get
            {
                return ConfigurationManager.AppSettings["smtpHost"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["smtpHost"];
            }
        }

        public static string EmailAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["emailAddress"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["emailAddress"];
            }
        }

        public static string EmailPassword
        {
            get
            {
                return ConfigurationManager.AppSettings["emailPassword"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["emailPassword"];
            }
        }

        public static int SmtpPort
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["smtpPort"], out smtpPort) == true ? smtpPort
                    : 0;
            }
        }

        public static bool EnabledSSL
        {
            get
            {
                return bool.TryParse(ConfigurationManager.AppSettings["enabledSSL"], out enabledSSL)
                    ? enabledSSL : false;
            }
        }

        public static int LoanRequestExpiryDays
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["LoanRequestExpiryDays"], out loanRequestExpiryDays)
                    ? loanRequestExpiryDays : 0;
            }
        }

        public static int LoanDraftExpiryDays
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["LoanDraftExpiryDays"], out loanDraftExpiryDays)
                    ? loanDraftExpiryDays : 0;
            }
        }

        public static string AzureStorageConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["AzureStorageConnectionString"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["AzureStorageConnectionString"];
            }
        }

        public static string AzureDNSSafeHostUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["AzureDNSSafeHostUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["AzureDNSSafeHostUrl"];
            }
        }

        public static string BorrowerTermsAndConditionsUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["BorrowerTermsAndConditionsUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["BorrowerTermsAndConditionsUrl"];
            }
        }

        public static string DataProtectionPolicyUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["DataProtectionPolicyUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["DataProtectionPolicyUrl"];
            }
        }

        public static string HomePageUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["HomePageUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["HomePageUrl"];
            }
        }

        public static string AdminEmailAddress
        {
            get
            {
                return ConfigurationManager.AppSettings["AdminEmailAddress"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["AdminEmailAddress"];
            }
        }

        public static string LendingSiteUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["LendingSiteUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["LendingSiteUrl"];
            }
        }

        public static string ForumUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["ForumUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["ForumUrl"];
            }
        }

        public static string PromissoryNoteTC
        {
            get
            {
                return ConfigurationManager.AppSettings["PromissoryNoteTC"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["PromissoryNoteTC"];
            }
        }

        public static string TermsOfServiceUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["TermsOfServiceUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["TermsOfServiceUrl"];
            }
        }

        public static string CodeOfConductUrl
        {
            get
            {
                return ConfigurationManager.AppSettings["CodeOfConductUrl"] == null ? string.Empty
                    : ConfigurationManager.AppSettings["CodeOfConductUrl"];
            }
        }

        public static int EarmarkAccountBalanceID
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["EarmarkAccountBalanceID"], out earmarkAccountBalanceID) == true ? earmarkAccountBalanceID
                    : 0;
            }
        }

        public static int MsenseAccountBalanceID
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["MsenseAccountBalanceID"], out msenseAccountBalanceID) == true ? msenseAccountBalanceID
                    : 0;
            }
        }

        public static int FundTransferAccountBalanceID
        {
            get
            {
                return int.TryParse(ConfigurationManager.AppSettings["FundTransferAccountBalanceID"], out fundTransferAccountBalanceID) == true 
                    ? fundTransferAccountBalanceID
                    : 0;
            }
        }
    }
}
