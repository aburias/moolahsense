﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.Logging;

namespace MoolahConnect.Util
{
    public static class CommonMethods
    {
        public static string GetLoanStatus(PreliminaryLoanRequestStatus preStatus, LoanRequestStatus status)
        {
            if ((int)status < 2)
                return ((int)preStatus).ToPreliminaryLoanStatusDisplayName();
            else
                return ((int)status).ToLoanStatusDisplayName();
        }

        public static double GetTotalInterestPayable(string tenor, double amount, double rate)
        {
            double payable = 0;
            double monthlyRepayment = 0;
            double periodicInterestRate = 0;
            int tenorinYears = 0;
            int totalNoOfPayments = 0;

            try
            {
                tenorinYears = int.Parse(tenor) / 12;
                totalNoOfPayments = tenorinYears * Settings.PaymentsPerYear;
                periodicInterestRate = rate / Settings.PaymentsPerYear;

                monthlyRepayment = Financial.Pmt(periodicInterestRate / 100, totalNoOfPayments, amount * -1);

                payable = Math.Round(((monthlyRepayment * tenorinYears * 12) - amount), 2);

                return payable;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return 0;
            }
        }

        public static string GetAccountNumber(long number, string role)
        {
            if (role == "Borrower")
                return string.Format("BRW{0}", number.ToString());
            else
                return string.Format("INV{0}", number.ToString());
        }

        public static string[] LoadAddressField(string addressString)
        {
            if (string.IsNullOrEmpty(addressString))
                return new string[] { null, null, null };

            var tmp = addressString.Replace(",", "@##@");
            tmp = tmp.Replace("@#@", ",");
            var split = tmp.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (split.Length == 0)
            {
                return new string[] { null, null, null };
            }

            if (split.Length == 1)
            {
                return new[] { string.IsNullOrEmpty(split[0]) ? null : split[0].Replace("@##@", ","), null, null };
            }

            if (split.Length == 2)
            {
                return new[]
                {
                    string.IsNullOrEmpty(split[0]) ? null : split[0].Replace("@##@", ","),
                    null,
                    string.IsNullOrEmpty(split[1]) ? null : split[1].Replace("@##@", ",")
                };
            }

            return split.Length == 3
                       ? new[]
                       {
                           string.IsNullOrEmpty(split[0]) ? null : split[0].Replace("@##@", ","),
                           string.IsNullOrEmpty(split[1]) ? null : split[1].Replace("@##@", ","),
                           string.IsNullOrEmpty(split[2]) ? null : split[2].Replace("@##@", ",")
                       }
                       : new string[] { null, null, null };
        }

        public static string LoadAddressFieldStr(string addressString)
        {
            if (string.IsNullOrEmpty(addressString))
                return string.Empty;

            var tmp = addressString.Replace(",", "@##@");
            tmp = tmp.Replace("@#@", ",");
            var split = tmp.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);

            if (split.Length == 0)
            {
                return string.Empty;
            }

            if (split.Length == 2)
            {
                split[1] = string.Format("(S) {0}", split[1]);
            }
            else if (split.Length == 3)
            {
                split[2] = string.Format("(S) {0}", split[2]);
            }
            return string.Join(", ", split);

        }

        public static string GetAccountFullName(string title, string firstName, string lastName)
        {
            string accountFullName = string.Empty;

            //if (!string.IsNullOrEmpty(title))
            //{
            //    accountFullName += title + " ";
            //}

            if (!string.IsNullOrEmpty(firstName))
            {
                accountFullName += firstName + " ";
            }

            if (!string.IsNullOrEmpty(lastName))
            {
                accountFullName += lastName + " ";
            }

            return accountFullName;
        }

        public static string RepaymentDisplayName(RepaymentPayStatus status)
        {
            switch (status)
            {
                case RepaymentPayStatus.Pending:
                    return "Pending";
                case RepaymentPayStatus.Paid:
                    return "Fully Paid";
                case RepaymentPayStatus.Late:
                    return "Late";
                case RepaymentPayStatus.Default:
                    return "Default";
                case RepaymentPayStatus.PartiallyPaid:
                    return "Partially Paid";
                default:
                    return string.Empty;
            }
        }

        public static IEnumerable<object> GetTurnOverRange()
        {
            return new List<Object>{
                       new { value = 1 , text = "<S$30,000"  },
                       new { value =30001 , text = "S$30,001-S$100,000" },
                       new { value =100001 , text = "S$100,001-S$200,000" },
                       new { value =200001 , text = "S$200,001-S$300,000" },
                       new { value =300001 , text = "S$300,001-S$400,000" },
                       new { value =40001 , text = "S$400,001-S$500,000" },
                       new { value =50001 , text = ">S$500,000" }
                                                   };
        }

        //////////Cheeck How many days hours and minutes left for a loan to be in offerperiod/////////////////
        public static int[] GetDaysHoursandMinutes(DateTime? publishedDate, int DaysFromGlobalVariable)
        {
            if (!publishedDate.HasValue)
                return new int[0];

            DateTime DateCreationofLoan = publishedDate.Value;

            int[] arrTimeRemaining = new int[3];
            if (DateCreationofLoan.AddDays(DaysFromGlobalVariable) > DateTime.UtcNow)
            {
                var _14DaysAfterDate = Convert.ToDateTime(DateCreationofLoan.Date.AddDays(DaysFromGlobalVariable).AddHours(DateCreationofLoan.Hour).AddMinutes(DateCreationofLoan.Minute + 1));



                var DateTimeRemaing = _14DaysAfterDate - Convert.ToDateTime(DateTime.UtcNow);
                arrTimeRemaining[0] = DateTimeRemaing.Days;
                arrTimeRemaining[1] = DateTimeRemaing.Hours;
                arrTimeRemaining[2] = DateTimeRemaing.Minutes;
            }
            else
            {
                arrTimeRemaining[0] = 0;//days
                arrTimeRemaining[1] = 0;//hours
                arrTimeRemaining[2] = 0;//minutes
            }
            return arrTimeRemaining;

        }

    }
}
