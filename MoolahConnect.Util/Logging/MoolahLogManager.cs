﻿using System;
using NLog;
using MoolahConnect.Util.GenerateReference;

namespace MoolahConnect.Util.Logging
{
    public class MoolahLogManager
    {
        private static Logger log = NLog.LogManager.GetCurrentClassLogger();

        //Log system events
        public static void Log(string message, LogType logType, string userName, long? loanRequestId = null)
        {
            LogEventInfo _event;

            switch (logType)
            {
                case LogType.Info:
                    _event = new LogEventInfo(LogLevel.Info, "", message);
                    _event.TimeStamp = DateTime.UtcNow;
                    break;
                case LogType.Warn:
                    _event = new LogEventInfo(LogLevel.Warn, "", message);
                    _event.TimeStamp = DateTime.UtcNow;
                    break;
                case LogType.Debug:
                    _event = new LogEventInfo(LogLevel.Debug, "", message);
                    _event.TimeStamp = DateTime.UtcNow;
                    break;
                default:
                    _event = new LogEventInfo(LogLevel.Info, "", message);
                    _event.TimeStamp = DateTime.UtcNow;
                    break;
            }

            _event.Properties["UserName"] = userName;
            _event.Properties["LoanRequestId"] = loanRequestId;
            _event.Properties["Reference"] = UniqueReference.GetRandomUniqueNumber();
            log.Log(_event);
        }

        //Log exceptions

        public static void LogExceptionEvent(string message, string userName,
                                             string stackTrace = null, string innerMessage = null)
        {
            LogEventInfo _event = new LogEventInfo(LogLevel.Error, "", message);
            ;

            _event.Properties["UserName"] = userName;
            _event.Properties["StackTrace"] = stackTrace;
            _event.Properties["InnerMessage"] = innerMessage;
            log.Log(_event);
        }

        public static void LogException(Exception ex)
        {
            try
            {
                try
                {
                    LogExceptionEvent(ex.Message, null, stackTrace: ex.StackTrace,
                                      innerMessage: ex.InnerException != null ? ex.InnerException.Message : null);
                }
                catch (Exception)
                {
                    string message = "Exception: " + ex.Message +
                                     (ex.InnerException == null ? "" : "; Inner Exception: " + ex.InnerException.Message);
                    LogExceptionEvent(message, null);
                }
            }
            catch (Exception ex2)
            {
                if (ex2 != null && ex2.Message != null)
                {
                    LogExceptionEvent(ex2.Message, null);
                }
                else
                {
                    LogExceptionEvent("Exception can not be logged", null);
                }
            }
        }

        //Log client side exceptions.
        public static void LogClientSideException(string error, string file, string lineNo, string userName)
        {
            string message = string.Format("File : {0}, LineNo : {1}, Error : {2}", file, lineNo, error);

            LogExceptionEvent(message, userName);
        }

        // Log admin audit record
        public static void LogAdminAudit(string message, string userName, long? loanRequestId, long? actionUserId,
                                         string fieldName, string oldValue, string newValue)
        {
            decimal resultOld;
            var parsedOld = decimal.TryParse(oldValue, out resultOld);

            decimal resultNew;
            var parsedNew = decimal.TryParse(newValue, out resultNew);

            if (parsedNew && parsedOld)
            {
                if (resultOld == resultNew)
                {
                    return;
                }
            }

            var refNumber = UniqueReference.GetRandomUniqueNumber();

            var _event = new LogEventInfo(LogLevel.Info, "", message) { TimeStamp = DateTime.UtcNow };
            _event.Properties["userName"] = userName;
            _event.Properties["loanRequestId"] = loanRequestId;
            _event.Properties["reference"] = refNumber;
            _event.Properties["fieldName"] = fieldName;
            _event.Properties["oldValue"] = oldValue;
            _event.Properties["newValue"] = newValue;
            _event.Properties["actionUserId"] = actionUserId;
            log.Log(_event);
        }
    }
}
