﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Logging
{
    public enum LogType
    {
        Info = 1,
        Warn,
        Debug
    }
}
