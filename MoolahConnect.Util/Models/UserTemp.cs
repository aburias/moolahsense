﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Models
{
    public class UserTemp
    {
        public int Type { get; set; }
        public string Username { get; set; }
    }
}
