﻿using System;
using System.Linq;
using ExcelLibrary.SpreadSheet;

namespace MoolahConnect.Util.Excel
{
    public class WorkBookBuilder
    {
        public static Workbook CreateWorkBook(params Worksheet[] sheets)
        {
            var workbook = new Workbook();
            foreach (Worksheet worksheet in sheets)
            {
                workbook.Worksheets.Add(worksheet);
            }
            return workbook;
        }
    }

    public class WorksheetBuilder
    {
        private readonly Worksheet _worksheet;

        public WorksheetBuilder(string sheetName)
        {
            SheetName = sheetName;
            _worksheet = new Worksheet(SheetName);
        }

        public string SheetName { get; private set; }

        public void CreateHeader(params string[] headers)
        {
            for (int i = 0; i < headers.Count(); i++)
            {
                _worksheet.Cells[0, i] = new Cell(headers[i]);
            }
        }

        public void CreateBody(Action<Worksheet> buildBody)
        {
            buildBody(_worksheet);
        }
    }
}