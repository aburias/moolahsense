﻿using MoolahConnect.Util.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.ExtensionMethods
{
    public static class GeneralExtensions
    {
        public static string ToPrecentage(this double value)
        {
            return Math.Round(value, 2).ToString();

        }

        public static string ToRate(this double value)
        {
            return Math.Round(value, 2).ToString();
        }

        public static string ToRate(this double? value)
        {
            if (!value.HasValue)
                value = 0;

            return string.Format("{0:0.00}", value.Value); ;
        }

        public static string ToCurrency(this double value)
        {
            return string.Format("S{0:C}", value);
        }

        public static string ToCurrency(this float value)
        {
            return string.Format("S{0:C}", value);
        }

        public static string ToCurrency(this decimal? value)
        {
            decimal defaultVal = 0;

            if (value.HasValue)
                defaultVal = value.Value;

            return string.Format("S{0:C}", defaultVal < 0 ? defaultVal * -1 : defaultVal);
        }

        public static string ToCurrency(this decimal value)
        {
            return string.Format("S{0:C}", value < 0 ? value * -1 : value);
        }

        public static decimal ValueOrZero(this decimal? value)
        { return value.HasValue ? value.Value : 0; }

        public static int ToYear(this string value)
        {
            int years = 0;

            try
            {
                int months = int.Parse(value);
                years = months / 12;
                return years;
            }
            catch { return 0; }
        }

        public static int ToPeriod(this string term)
        {
            int period = 0;
            int.TryParse(term, out period);
            return period;
        }

        public static string ToLoanStatusDisplayName(this int value)
        {
            switch (value)
            {
                case (int)LoanRequestStatus.Pending:
                    return "Pending";
                case (int)LoanRequestStatus.InProgress:
                    return "In Progress";
                case (int)LoanRequestStatus.Matured:
                    return LoanRequestStatus.Matured.ToString();
                case (int)LoanRequestStatus.Cancelled:
                    return LoanRequestStatus.Cancelled.ToString();
                case (int)LoanRequestStatus.Rejected:
                    return LoanRequestStatus.Rejected.ToString();
                case (int)LoanRequestStatus.Matched:
                    return "Provisionally Matched";
                case (int)LoanRequestStatus.FundApproved:
                    return "Matched Loan Processed";
                case (int)LoanRequestStatus.FundRejected:
                    return "Matched Loan Cancelled";
                default:
                    return string.Empty;
            }
        }

        public static string ToPreliminaryLoanStatusDisplayName(this int value)
        {
            switch (value)
            {
                case (int)PreliminaryLoanRequestStatus.DraftCreated:
                    return "Draft Created";
                case (int)PreliminaryLoanRequestStatus.DraftSubmitted:
                    return "Draft Submitted";
                case (int)PreliminaryLoanRequestStatus.DraftApproved:
                    return "Draft Approved";
                case (int)PreliminaryLoanRequestStatus.DraftRejected:
                    return "Draft Rejected";
                case (int)PreliminaryLoanRequestStatus.DraftDeleted:
                    return "Draft Deleted";
                case (int)PreliminaryLoanRequestStatus.DraftExpired:
                    return "Draft Expired";
                case (int)PreliminaryLoanRequestStatus.DraftPublished:
                    return "Draft Published";
                case (int)PreliminaryLoanRequestStatus.LoanExpired:
                    return "Listing Expired";
                default:
                    return string.Empty;
            }
        }

        public static string ToApprovedDisplayName(this bool value)
        {
            if (value)
                return "Approved";
            else
                return "Not Approved";
        }

        public static string ToQueryStringValue(this string value)
        {
            return string.IsNullOrEmpty(value) ? string.Empty : value.Replace("+", "%2B");
        }
    }
}   
