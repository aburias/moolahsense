﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.ExtensionMethods
{
    public static class DateTimeExtenstion
    {
        public static string ToShortDate(this DateTime? date)
        {
            if (date == null)
                return string.Empty;

            return date.Value.ToShortDateString();
        }

        public static DateTime SingaporeTime()
        {
            return TimeZoneInfo.ConvertTime(DateTime.UtcNow, TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time"));
        }

        public static DateTime SingaporeTime(DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTime(dateTime, TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time"));
        }

        public static string ToDate(this DateTime date)
        {
            return SingaporeTime(date).ToString("dd MMM yyyy");
        }

        public static string ToDate(this DateTime? date)
        {
            if (!date.HasValue)
                return string.Empty;

            return SingaporeTime(date.Value).ToString("dd MMM yyyy");
        }
        public static string ToDate(this DateTime? date,string format)
        {
            if (!date.HasValue)
                return string.Empty;

            return SingaporeTime(date.Value).ToString(format);
        }
        public static string ToDateTime(this DateTime? date)
        {
            if (!date.HasValue)
                return string.Empty;

            return SingaporeTime(date.Value).ToString("dd MMM yyyy   HH:mm");
        }

        public static string ToDateTime(this DateTime date)
        {
            return SingaporeTime(date).ToString("dd MMM yyyy   HH:mm");
        }

        public static string ToDate(this string date)
        {
            DateTime dateTemp;

            if (DateTime.TryParse(date, out dateTemp))
                return dateTemp.ToString("dd MMM yyyy");

            return string.Empty;
        }

        public static string ToDateTime(this int[] time)
        {
            if (time.Length < 3)
                return string.Empty;

            return string.Format("[{0}]d [{1}]h [{2}]m", time[0], time[1], time[2]);
        }
    }
}
