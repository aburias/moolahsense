﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Web;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.Models;
using MoolahConnect.Util.ExtensionMethods;
using System.IO;
using System.Net;

namespace MoolahConnect.Util.Email
{
    public class EmailSender : IEmailSender
    {
        private string _AppUrl;

        public EmailSender()
        {

            var requestUrl = HttpContext.Current.Request.Url.AbsoluteUri;
            var requestRawUrl = HttpContext.Current.Request.Url.PathAndQuery;
            var rootUrl = requestUrl.Replace(requestRawUrl, string.Empty);

            _AppUrl = rootUrl.ToQueryStringValue();
        }

        private static bool SendEMailMessage(string toEmail, string subject, string body, out string error, Attachment attachment = null)
        {
            error = string.Empty;

            try
            {
                var message = new MailMessage();
                message.Subject = subject;
                message.To.Add(toEmail);
                if (Settings.AdminEmailAddress != string.Empty && toEmail != Settings.AdminEmailAddress)
                    message.Bcc.Add(Settings.AdminEmailAddress);
                message.Body = body;
                message.IsBodyHtml = true;

                if (attachment != null)
                    message.Attachments.Add(attachment);

                return SendEMail(message, out error);

            }
            catch (Exception ex)
            {
                error = ex.Message;
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        private static bool SendEMail(MailMessage message, out string error)
        {
            error = string.Empty;

            try
            {
                message.From = new MailAddress(Settings.EmailAddress);

                using (var smtpClient = new SmtpClient(Settings.SmtpHost, Settings.SmtpPort))
                {
                    smtpClient.EnableSsl = Settings.EnabledSSL;
                    smtpClient.Credentials = new NetworkCredential(Settings.EmailAddress, Settings.EmailPassword);

                    smtpClient.Send(message);
                }
                return true;
            }
            catch (Exception ex)
            {
                error = ex.Message;
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool OnAcceptedLoanRequestToBorrower(string emailAddress, long loanReference)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanAllocationMailToborrower.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);
                sb = sb.Replace("<%LoanReference%>", loanReference.ToString());

                ////Generate the provisional contract and upload to file store
                //_IPdfGeneratorTask.GenerateBorrowerLoanContract(loanReference);

                return SendEMailMessage(emailAddress, "MoolahSense: Provisionally Funded", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool OnRejectedLoanOfferToInvetor(string emailAddress, string loanReference, double amount,
            double rate, DateTime createdDate, string businessName, string reason)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanOfferRejction.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%rate%>", rate.ToRate());
                sb = sb.Replace("<%datetime%>", createdDate.ToDateTime());
                sb = sb.Replace("<%companyname%>", businessName);
                sb = sb.Replace("<%noteid%>", loanReference);
                sb = sb.Replace("<%Reason%>", reason);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(emailAddress, "MoolahSense: Offer Not Accepted", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnAcceptedLoanOfferToInvetor(string emailAddress, long requestId, int offerId, string contractIdentifier, int acceptedOffersCount)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanOffersApprovalInformation.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);
                sb = sb.Replace("<%LoanReference%>", requestId.ToString());

                ////Generate the provisional contract and upload to file store
                //_IPdfGeneratorTask.GenerateInvestorLoanContract(requestId, offerId, contractIdentifier, acceptedOffersCount);


                return SendEMailMessage(emailAddress, "MoolahSense: Provisionally Accepted", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);

                return false;
            }
        }


        public bool OnUserVerificationChanged(string emailAddress, string status, bool isBorrower)
        {
            try
            {
                /*string approvedPortion = "Your account is now approved! <br><br>We greatly appreciate if you can help us complete " +
                    "verification on your bank account by funding your account. As a small token, we will credit $10 into your MoolahSense account if you fund it " +
                    "within 2 weeks from today.<br><br>Instructions on funding your account can be found on the " +
                    "\"Transfer Moolah\" tab on the dashboard. <br><br><a href=\"https://lending.moolahsense.com/User/Login\">Login now</a>, " +
                    "start investing in local businesses and earn attractive yields!";*/
                string approvedPortion = "Your account is now approved! <br><br> Current campaigns can be found on the \"MoolahBoard\"" +
                    " tab and instructions on funding your account can be found on the \"Transfer Moolah\" tab on the dashboard." +
                    "<br><br><a href=\"https://lending.moolahsense.com/User/Login\">Login now</a>, " +
                    "start investing in local businesses and earn attractive yields!";
                string otherPortion = "Your account has been {0} by the Admin.";

                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/UserVerificationChanged.html")));
                sb = sb.Replace("<%username%>", emailAddress);

                if (status.ToLower() == "approved" && !isBorrower)
                    sb = sb.Replace("<%Status%>", approvedPortion);
                else
                    sb = sb.Replace("<%Status%>", string.Format(otherPortion, status.ToLower()));

                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(emailAddress, "MoolahSense: Account Opening Status", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnAccountUnlocked(string emailAddress)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/AccountUnlocked.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(emailAddress, "MoolahSense: Account Unlocked", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool OnApprovedLoanRequest(string emailAddress)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/NotifyBorrowerforApprovalofLoan-AcceptedByAdmin.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(emailAddress, "MoolahSense: Listing Reviewed", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool OnRejectedLoanRequest(string emailAddress, string comments)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/NotifyBorrowerforApprovalofLoan-RejectedByAdmin.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AdminCommentsondata%>", comments);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(emailAddress, "MoolahSense: Listing Rejected", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool EmailConfirmation(string Username, string url, UserRole role)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath(
                    role == UserRole.Borrower ? "/Content/Template/Email/BorrowerEmailConfirmation.html"
                    : "/Content/Template/Email/InvestorEmailConfirmation.html"
                    )));
                sb = sb.Replace("<%username%>", Username);
                sb = sb.Replace("<%url%>", url.ToQueryStringValue());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(Username, "Welcome to MoolahSense", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool EmailUpdateConfirmation(string username, string url, UserRole role)
        {
            try
            {
                string error;
                var sb = new StringBuilder();
                sb.Append(File.ReadAllText(HttpContext.Current.Server.MapPath(
                    role == UserRole.Borrower ? "/Content/Template/Email/BorrowerEmailUpdateConfirmation.html"
                    : "/Content/Template/Email/InvestorEmailUpdateConfirmation.html"
                    )));
                sb = sb.Replace("<%username%>", username);
                sb = sb.Replace("<%url%>", url.ToQueryStringValue());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(username, "MoolahSense : Email Address Changed", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnLoanCompletionToBorrower(string LoanReference, string UserName)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanCompletionBorrowerNotification.html")));

                sb = sb.Replace("<%LoanReference%>", LoanReference);
                sb = sb.Replace("<%username%>", UserName);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(UserName, "Your Loan on MoolahSense is completed", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnWithdrawalRejectionToInvestor(string Email, string comments)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/SendRejectionMailNotificationforWithrawl.html")));

                sb = sb.Replace("<%username%>", Email);
                sb = sb.Replace("<%Comments%>", comments);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(Email, "MoolahSense: Unable to Process Funds Transfer", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnResetPassword(string Emailid, string url)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/ForgetPassword.html")));
                sb = sb.Replace("<%username%>", Emailid);
                sb = sb.Replace("<%url%>", url.ToQueryStringValue());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);


                return SendEMailMessage(Emailid, "MoolahSense: Reset Password", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool OnResetSecurityQuestion(string Emailid, string url)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/forgotSecurityQuestion.html")));
                sb = sb.Replace("<%username%>", Emailid);
                sb = sb.Replace("<%url%>", url.ToQueryStringValue());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);


                return SendEMailMessage(Emailid, "MoolahSense: Reset Security Question", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnLoanRequestWithdrawn(string email, string loanRequestId)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanOfferCancelled.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%LoanReference%>", loanRequestId);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);


                return SendEMailMessage(email, "MoolahSense: Listing Cancelled", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool SendRejectFundTransferEmail(IEnumerable<UserTemp> users, long loanId, string reason)
        {
            try
            {
                var invTmp =
                    File.ReadAllText(
                        HttpContext.Current.Server.MapPath(
                            "/Content/Template/Email/MatchedLoanRejectInv.html"));
                var borTmp =
                    File.ReadAllText(
                        HttpContext.Current.Server.MapPath(
                            "/Content/Template/Email/MatchedLoanRejectBor.html"));

                const string subject = @"MoolahSense: Issuance Cancelled";
                var error = string.Empty;

                foreach (var user in users)
                {
                    var sb = new StringBuilder();
                    sb.Append(user.Type == 1 ? invTmp.Clone() : borTmp.Clone());
                    sb = sb.Replace("<%username%>", user.Username);
                    sb = sb.Replace("<%LoanReference%>", loanId.ToString());
                    sb = sb.Replace("<%Reason%>", reason);
                    sb = sb.Replace("<%AppUrl%>", _AppUrl);
                    SendEMailMessage(user.Username, subject, sb.ToString(), out error);
                }

                return string.IsNullOrEmpty(error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool onNoteAcceptedByAdminToBorrower(string emailAddress, long loanReference)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/NoteApprovedByAdminToBorrower.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);
                sb = sb.Replace("<%LoanReference%>", loanReference.ToString());

                return SendEMailMessage(emailAddress, "MoolahSense: Note Issued", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool onNoteAcceptedByAdminToInvestor(string emailAddress, long loanReference)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/NoteApprovedByAdminToInvestor.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);
                sb = sb.Replace("<%LoanReference%>", loanReference.ToString());

                return SendEMailMessage(emailAddress, "MoolahSense: Note Issued", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool OnUserSubmittedRegistration(string emailAddress, string accountID)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/SubmitRegistrationDetails.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%accountid%>", accountID);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(Settings.AdminEmailAddress, "MoolahSense: User Submiited Registration Details", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool FundTransferIn(string email, double amount, double balance)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/FundTransferIn.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%balance%>", balance.ToCurrency());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: Account Funded", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool FundTransferOut(string email, double amount, double balance)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/FundTransferOut.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%balance%>", balance.ToCurrency());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: Funds Withdrawal Processed", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool InvestorOfferMade(string email, string companyName, double amount, double rate)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/InvOfferMade.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%companyname%>", companyName);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%rate%>", rate.ToPrecentage());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: Offer Submitted", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool BorrowerRateChanged(string email, double preRate, double newRate)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/BrwRateChanged.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%prerate%>", preRate.ToRate());
                sb = sb.Replace("<%newrate%>", newRate.ToRate());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: Target Rate Successfully Changed", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool InvestorRateChanged(string email, string companyName, double preRate, double newRate)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/InvRateChanged.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%companyname%>", companyName);
                sb = sb.Replace("<%prerate%>", preRate.ToRate());
                sb = sb.Replace("<%newrate%>", newRate.ToRate());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: An issuer has changed their target rate", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool OnLoanListingExpired(string email, string loanRequestId)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanListingExpired.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%LoanReference%>", loanRequestId);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: Listing Expired", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool onLoanOfferCancelledDueToLoanRequestExpire(string email, string loanRequestId)
        {
            try
            {
                var error = string.Empty;
                StringBuilder sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/LoanOfferCancelledDueToLoanExpired.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%LoanReference%>", loanRequestId);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense: Listing Expired", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool AdminUserCreatedFundTransferRequest(string emailAddress, string accountID, double amount, double balance)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/AdminFundTransferRequestCreated.html")));
                sb = sb.Replace("<%username%>", emailAddress);
                sb = sb.Replace("<%accountid%>", accountID);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%balance%>", balance.ToCurrency());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(Settings.AdminEmailAddress, "MoolahSense: User Created Fund Transfer Request", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool InvestorOfferWithdrawn(string email, string companyName, double amount, double rate)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/InvOfferWithdrawn.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%companyname%>", companyName);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%rate%>", rate.ToRate());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense:  Offer Successfully Withdrawn", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool BorrowerLoanPublished(string email, double amount, double rate)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/BrwLoanPublished.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%rate%>", rate.ToRate());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense:  Campaign Launched", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool InvestorLoanPublished(string email, string name, double amount, double rate)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/InvLoanPublished.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%name%>", name);
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%rate%>", rate.ToRate());
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense:  New Campaign Launched", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool UserSubmittedRegistration(string email)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/UserSubmitRegistrationDetails.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);

                return SendEMailMessage(email, "MoolahSense:  Submitting of Information", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool BorrowerRepaymentStatusChanged(string email, RepaymentPayStatus status, DateTime date, double amount, int monthsTillMat)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/BrwRepaymentStatusChanged.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);
                sb = sb.Replace("<%status%>", CommonMethods.RepaymentDisplayName(status));
                sb = sb.Replace("<%date%>", date.ToDate());
                sb = sb.Replace("<%amount%>", amount.ToCurrency());
                sb = sb.Replace("<%months%>", monthsTillMat.ToString());

                return SendEMailMessage(email, "MoolahSense:  Monthly Repayment Status", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool InvestorRepaymentStatusChanged(string email, string issuerName, RepaymentPayStatus status, DateTime date, double amount, int monthsTillMat)
        {
            try
            {
                var error = string.Empty;
                var sb = new StringBuilder();
                sb.Append(System.IO.File.ReadAllText(HttpContext.Current.Server.MapPath("/Content/Template/Email/InvRepaymentStatusChanged.html")));
                sb = sb.Replace("<%username%>", email);
                sb = sb.Replace("<%AppUrl%>", _AppUrl);
                sb = sb.Replace("<%name%>", issuerName);
                sb = sb.Replace("<%status%>", CommonMethods.RepaymentDisplayName(status));
                sb = sb.Replace("<%date%>", date.ToDate());
                switch (status)
                {
                    case RepaymentPayStatus.Paid:
                    case RepaymentPayStatus.PartiallyPaid:
                        sb = sb.Replace("<%amount%>", string.Format("<tr><td>Repayment Amount Received</td><td> : {0}</td></tr>", amount.ToCurrency()));
                        break;
                    case RepaymentPayStatus.Pending:
                    case RepaymentPayStatus.Late:
                    case RepaymentPayStatus.Default:
                    default:
                        sb = sb.Replace("<%amount%>", string.Empty);
                        break;
                }

                sb = sb.Replace("<%months%>", monthsTillMat.ToString());

                return SendEMailMessage(email, "MoolahSense:  Monthly Repayment Status", sb.ToString(), out error);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}
