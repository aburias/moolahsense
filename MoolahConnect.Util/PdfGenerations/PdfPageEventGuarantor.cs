﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace MoolahConnect.Util.PdfGenerations
{
    public class PdfPageEventGuarantor : PdfPageEventHelper
    {
        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);

            PdfPTable tabHeader = new PdfPTable(1);
            tabHeader.TotalWidth = document.Right - document.Left;

            PdfPCell cellHeader1 = new PdfPCell(new PdfPCell(new Phrase("THIS IS AN IMPORTANT LEGAL DOCUMENT \n",
                    FontFactory.GetFont("Calibri", 11, Font.BOLD, BaseColor.LIGHT_GRAY))));
            cellHeader1.HorizontalAlignment = Element.ALIGN_CENTER;
            cellHeader1.Border = 0;
            tabHeader.AddCell(cellHeader1);

            PdfPCell cellHeader2 = new PdfPCell(new PdfPCell(new Phrase("Please read this Guarantee carefully.  You are advised to fully understand your obligations and risks under this Guarantee.  If you have any concerns with this Guarantee, consult a lawyer before signing this Guarantee. \n",
                FontFactory.GetFont("Calibri", 10, Font.BOLDITALIC, BaseColor.LIGHT_GRAY))));
            cellHeader2.HorizontalAlignment = Element.ALIGN_CENTER;
            cellHeader2.Border = 0;
            tabHeader.AddCell(cellHeader2);

            tabHeader.WriteSelectedRows(0, -1, 35, document.PageSize.Height - 15, writer.DirectContent);

            PdfPTable tabFot = new PdfPTable(new float[] { 1F, 2F });
            tabFot.TotalWidth = 100F;
            tabFot.DefaultCell.BorderColor = BaseColor.LIGHT_GRAY;
            tabFot.AddCell(new PdfPCell(new Phrase("Initial", FontFactory.GetFont("Calibri", 9, Font.NORMAL, BaseColor.GRAY))));
            tabFot.AddCell(new PdfPCell(new Phrase(" ")));
            tabFot.WriteSelectedRows(0, -1, 475, document.Bottom, writer.DirectContent);
        }
    }
}
