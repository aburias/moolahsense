﻿using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.PdfGenerations
{
    public class PdfPageEvents : PdfPageEventHelper
    {
        // write on end of each page
        public override void OnEndPage(PdfWriter writer, Document document)
        {
            base.OnEndPage(writer, document);
            PdfPTable tabFot = new PdfPTable(new float[] { 1F, 2F });
            tabFot.TotalWidth = 100F;
            tabFot.DefaultCell.BorderColor = BaseColor.LIGHT_GRAY;
            tabFot.AddCell(new PdfPCell(new Phrase("Initial", FontFactory.GetFont("Calibri", 9, Font.NORMAL,BaseColor.GRAY))));
            tabFot.AddCell(new PdfPCell(new Phrase(" ")));
            tabFot.WriteSelectedRows(0, -1, 475, document.Bottom, writer.DirectContent);
        }
    }
}
