﻿using MoolahConnect.Util.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.GenerateReference
{
    public static class UniqueReference
    {
        public static string GetRandomUniqueNumber()
        {
            try
            {
                long i = 1;
                foreach (byte b in Guid.NewGuid().ToByteArray())
                {
                    i *= ((int)b + 1);
                }
                return string.Format("{0:x}", i - DateTime.Now.Ticks);                

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return string.Empty;
            }
        }

        //random value will be left padded with 0
        //private static string Generate4number()
        //{
        //    Random rnd = new Random();
        //    int randomNumber = rnd.Next(1,10000);

        //    string returnValue = randomNumber.ToString("D4");
        //    return returnValue;
        //}

        //need to get the database table for this random value
        //generate the number
        //Check against the right table if this randam value exist, if it does, generate again
        //if it does not return the value
        //public static string GetRandomUniqueNumber2(String prefix, String suffix,String Database)
        //{
        //    bool found = false;
        //    string randomValue = "";            

        //    do
        //    {
        //        string set1 = UniqueReference::Generate4number();
        //        string set2 = UniqueReference::Generate4number();
        //        string set3 = UniqueReference::Generate4number();

        //        randomValue += prefix + "-" + set1 + "-" + set2 + "-" + set3 + "-" +  suffix;


        //        //search appropriate table to determine if this random value is valid



        //    } while (found);
        //    return randomValue;            
        //}
    }
}
