﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Enums
{
    public enum TimePeriodFilterEnum
    {
        All = 0,
        Current,
        LAST3,
        LAST6,
        LAST9,
        LAST12

    }
}
