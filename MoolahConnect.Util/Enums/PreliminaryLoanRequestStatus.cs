﻿namespace MoolahConnect.Util.Enums
{
    public enum PreliminaryLoanRequestStatus
    {
        DraftCreated = 0,
        DraftSubmitted,
        DraftApproved,
        DraftRejected,
        DraftDeleted,
        DraftExpired,
        DraftPublished,
        LoanExpired
    }
}
