﻿namespace MoolahConnect.Util.Enums
{
    public enum DPCreditPaymentGrade
    {
        NA = 0,
        DP1,
        DP2,
        DP3Plus,
        DP3,
        DP4Plus,
        DP4,
        DP4Minus,
        DP5Plus,
        DP5,
        DP5Minus,
        DP6Plus,
        DP6,
        DP6Minus,
        DP7Plus,
        DP7,
        DP7Minus,
        DP8Plus,
        DP8
    }
}
