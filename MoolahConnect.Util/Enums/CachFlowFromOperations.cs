﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Enums
{
    public enum CashFlowFromOperations
    {
        Negative = 0,
        Positive = 1,
        Undisclosed = 2
    }
}
