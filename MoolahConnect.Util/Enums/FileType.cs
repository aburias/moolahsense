﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Enums
{
    public enum FileType
    {
        Registration,
        LoanVideo,
        LoanPhoto,
        LoanDocuments,
        LoanAdminDocuments,
        LoanCover,
        GurantorDetails,
        IssuerLoanContract,
        PayeeLoanContract,
        GuarantorContract,
        FinalIssuerLoanContract,
        FinalGuarantorContract,
        YouTubeLink
    }
}
