﻿namespace MoolahConnect.Util.Enums
{
    public enum LoanRequestStatus
    {
        Pending = 0,
        Rejected,
        InProgress,
        Matched,
        Cancelled,
        FundApproved,
        FundRejected,
        Matured
    }

    public enum PreliminaryLoanRequestStatus
    {
        DraftCreated = 0,
        DraftSubmitted,
        DraftApproved,
        DraftRejected,
        DraftDeleted,
        DraftExpired,
        DraftPublished,
        LoanExpired
    }
}