﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Enums
{
    public enum LoanOfferStatus
    {
        Pending = 0,
        Accepted,
        Rejected,
        Withdrawn,
        Expired,
        Cancelled,
        RejectedByAdmin
    }
}
