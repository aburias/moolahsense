﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Enums
{
    public enum RepaymentPayStatus
    {
        Pending = 0,
        Paid,
        Late,
        Default,
        PartiallyPaid
    }
}
