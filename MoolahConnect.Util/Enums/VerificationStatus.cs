﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.Enums
{
    public enum VerificationStatus
    {
        Unverified = 0,
        Verified = 1,
        Undisclosed = 2,
        Disclosed = 3
    }
}
