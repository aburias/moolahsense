﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.SealedClasses
{

    public sealed class GlobalConstansValues
    {
        public const string SEC_QUES_ANSWER = "test";
    }

    public sealed class ErrorMessages
    {
        public const string
            ACCOUNT_NOT_VERIFIED = "You have not verified your account. Please check your inbox and verify your account before continuing.",
            UNEXPECTED_ERROR = "Unexpected error. Please try again.",
            USERNAME_DOES_NOT_EXIST = "The email address does not exist",
            INVALID_PASSWORD = "The password is incorrect.",
            INVALID_SECURITY_QUEST = "Security question and/or answer is incorrect.",
            ACCOUNT_LOCKED = "Your account has been locked.",
            ROLE_CONSTRAINT = "You do not have permission to access the site.",
            TOKEN_EXPIRED = "Your token has been expired.",
            PASSWORDRESET_SAME = "You cannot set the same password.",
            ACCOUNT_LOCKOUT_W = "Warning! This is your LAST attempt to login. For security purposes, your account will be locked after 4 unsuccessful attempts."
            + " Please request for a password reset if you have forgotten your password.",
            LOAN_OFFER_CANNOT_WITHDRAW_INSUFFICIENT_FUNDS = "All offers made by you in an auction are final and cannot be withdrawn unless there are sufficient active offers from other investors to fully fund the total note requested.",
            FAILED_LOAN_REQUEST_DELETE = "Note request can not be deleted.",
            LOCK_OFFER_WITHDRAW_PROCESSING = "Please note that withdrawal of offers are not permitted as the issuer is in the process of accepting the offers",
            LOCK_OFFER_CREATE_PROCESSING = "Please note that new submission of offers are not permitted as the issuer is in the process of accepting the offers",
            IDLE_LOGOFF = "You have been automatic logout after inactivity/idle for 15 minutes. Please login again.",
            LOGIN_ACCOUNT_REJECTED_ERROR = "Your request to open account has been rejected.",
            REG_ACCOUNTDETAILS_MISSING_ERROR = "You have not completed the registration information in Step 1. Please click the \"Back\" button to go back to Step 1.";
    }

    public sealed class StatusMessages
    {
        public const string SUCCESS = "Success",
            DUPLICATE = "Duplicate",
            FAILED = "Failed",
            ACCOUNT_LOCKED = "Locked",
            MIN_LOAN_ACCEPTANCE_NOTMET = "Minimum note acceptance criteria not met",
            LOAN_ALREADY_MATCHED = "Invalid request",
            SYSTEM_ERROR = "System Error. Please contact <a href=\"#\" >MoolahSense support</a> ",
            MAIL_SEND_FAILURE = "We are unable to send the mail to email address you provided. Please try again!",
           PASSWORD_RESET_SUCCESS = "We have sent you a link to reset your password. Please check.",
           SEC_QUEST_RESET_SUCCESS = "We have sent you a link to reset your security questions. Please check.",
            FILE_UPLOAD_FAILED = "File upload Failed! Please try again.",
        FILE_DELETE_ERROR = "System can not delete the file you selected. Please try again.",
        LINK_DELETE_ERROR = "System can not delete the link you selected. Please try again.",
        SUCCESSFULL_LOGIN = "Logged-in Successfully",
        SUCCESSFULL_LOGOUT = "Logged-out Successfully",
        SUCCESSFULL_PASSWORD_VALIDATION = "Password Validated Successfully";
    }

    public sealed class ReasonForRejectedOffers
    {
        public const string DUE_TO_HIGH_RATE = "Your rate offered is higher than the accepted rate",
            DUE_TO_DEMAND = "Your offer was not accepted as there were other eligible offers submitted at a lower rate or at an earlier time to meet the issuer’s request.";
    }

    public sealed class PaymentStatus
    {
        public const string LATE = "Late",
            PAID = "Paid",
            CURRENT = "Current",
            COMING_DUE = "Coming Due";
    }

    public sealed class PaymentNote
    {
        public const string LATE = "Please arrange payment as soon as possible.",
            PAID = "",
            CURRENT = "Please arrange payment at least 5 working days before due date.",
            COMING_DUE = "Please arrange payment at least 5 working days before due date.";
    }
}
