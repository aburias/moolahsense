﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Util.SealedClasses
{
    public sealed class AuditMessages
    {
        public const string RAISE_A_NEW_LOAN = "Raise a New Request";
        public const string LOAN_LISTING_PUT_TO_PENDING_BY_ADMIN = "Note Listing put to pending approval by Admin";
        public const string LOAN_LISTING_APPROVED_BY_ADMIN = "Note Listing Reviewed by Admin";
        public const string LOAN_LISTING_REJECTED_BY_ADMIN = "Note Listing Rejected by Admin";
        public const string CANCEL_LOAN_LISTING = "Cancel Note Listing";
        public const string LOAN_OFFER_REJECTED = "Offer Rejected";
        public const string PUBLISH_LOAN_LISTING = "Request Published";
        public const string DELETE_LOAN_LISTING = "Note Listing Deleted";
        public const string RATE_CHANGE = "Target Rate Changed From {0}% to {1}%";
        public const string LOAN_MATCHED = "Note Matched Provisonally";
        public const string LOAN_AMOUNT_TRANSFERRED = "Note Amount Transferred to Issuer's Bank Account";
        public const string MATCHED_LOAN_REJECTED_BY_ADMIN = "Provisional Note Rejected by Admin";
        public const string LOAN_OFFER_PARTIALLY_ALLOCATED = "Offer Partially Allocated";
        public const string LOAN_MATCHED_INV = "Note Matched Provisionally";
        public const string MOOLAHSENSE_FEES = "MoolahSense Fees";
        public const string MONTHLY_LOAN_REPAYMENT = "Monthly Repayment";
        public const string PENDING_TRANSFER_ACCEPTED_BY_ADMIN = "Pending Transfer Accepted";
        public const string PENDING_TRANSFER_REJECTED_BY_ADMIN = "Pending Transfer Rejected";
        public const string NOTE_EXECUTED_BY_ISSUER = "Note Executed by Issuer";

        public const string DEPOSIT_FUND = "Fund Transfer In";
        public const string WITHDRAW_FUND = "Fund Transfer Out";

    }
}
