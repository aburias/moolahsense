﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Models.CustomModels;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Util;
using MoolahConnect.Util.Logging;
using AccountDetail = MoolahConnect.Models.BusinessModels.AccountDetail;

namespace MoolahConnect.Services.Implemantation1
{
    public class InvestorService : IInvestorService
    {
        private readonly IUserRepository _userRepo;
        private readonly IAccountDetailRepository _accountDetailRepo;
        private readonly IBankRepository _bankRepo;
        private readonly IUserSecurityQuestion _userSecurityQuestionRepo;
        private readonly ISecurityQuestionRepository _securityQuestionRepo;
        private readonly IKnowledgeAssesmentParentRepository _knowledgeAssesmentParentRepo;

        public InvestorService(IUserRepository userRepo, IAccountDetailRepository accountDetailRepo,
            IBankRepository bankRepo, IUserSecurityQuestion userSecurityQuestionRepo,
            ISecurityQuestionRepository securityQuestionRepo, IKnowledgeAssesmentParentRepository knowledgeAssesmentParentRepo)
        {
            _userRepo = userRepo;
            _accountDetailRepo = accountDetailRepo;
            _bankRepo = bankRepo;
            _userSecurityQuestionRepo = userSecurityQuestionRepo;
            _securityQuestionRepo = securityQuestionRepo;
            _knowledgeAssesmentParentRepo = knowledgeAssesmentParentRepo;
        }

        public string IndexLoad()
        {
            var controller = string.Empty;
            try
            {
                var username = HttpContext.Current.User.Identity.Name.Trim();
                var loggedInUser = _userRepo.GetByUsername(username);
                if (loggedInUser != null)
                {
                    if (loggedInUser.AdminVerification != "Approved" || loggedInUser.AdminVerification == null)
                    {
                        HttpContext.Current.Session["step"] = 1;
                        if (loggedInUser.IsSubmitted != null && (bool)loggedInUser.IsSubmitted)
                            controller = loggedInUser.InvestorType == "Corporate"
                                ? "SubmittedCorporate"
                                : "SubmittedInvestor";
                        else
                        {
                            HttpContext.Current.Session["step"] = 1;
                            controller = loggedInUser.InvestorType == "Corporate" ? "Corporate" : "Investor";
                        }
                    }
                    else
                        controller = "";

                    if (loggedInUser.UserRole == "Borrower")
                        controller = "User";
                }
            }

            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                controller = string.Empty;
            }

            return controller;
        }

        public InvestorAccount GetRegistrationDetailsForPDF()
        {
            InvestorAccount objinvestor;
            try
            {
                var username = HttpContext.Current.User.Identity.Name.Trim();
                var currentUser = _userRepo.GetByUsername(username);//.ToString(CultureInfo.CurrentCulture);


                var objaccount = new InvestorAccountDetailsModel();
                var objdocuments = new InvestorDocuments();
                var onjknowledgeassessment = new Knowledgeassessment();


                //////////////////////////////////// If user is logged in then get details of user from Userid for displaying in account details ///////////////////

                if (currentUser != null)
                {
                    objdocuments.Userid = currentUser.UserID;
                    var objuser = _accountDetailRepo.GetByUserId(currentUser.UserID);//db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);

                    if (objuser != null)
                    {
                        objaccount.Title = objuser.Title;
                        objaccount.Nationality = objuser.Nationality;
                        objaccount.Gender = objuser.Gender;
                        objaccount.IncomeRange = objuser.IncomeRange;

                        objaccount.FullName = objuser.FirstName + " " + objuser.LastName;
                        objaccount.ICNumber = objuser.IC_CompanyRegistrationNumber;
                        objaccount.PRStatus = objuser.PRstatus;
                        objaccount.DateofBirth = objuser.DateofBirth;

                        var resAddress = CommonMethods.LoadAddressField(objuser.ResidentialAddress);
                        objaccount.ResidentialAddress1 = resAddress[0];
                        objaccount.ResidentialAddress2 = resAddress[1];
                        objaccount.ResidentialPostalCode = resAddress[2];

                        objaccount.MainContactNumber = objuser.Main_OfficeContactnumber;
                        objaccount.Othercontactnumber = objuser.OptionalContactnumber;
                        objaccount.Occupation = objuser.Occupation;
                        objaccount.IncomeRange = objuser.IncomeRange;
                        objaccount.BankAccountName = objuser.AccountName;
                        objaccount.BankAccountNumber = objuser.AccountNumber;
                        objaccount.BankId = Convert.ToInt32(objuser.Bank_Id);
                        objaccount.BankName = _bankRepo.GetBankNameById(objaccount.BankId);//db.tbl_BanksList.Where(p => p.BankId == objaccount.BankId).Select(p => p.BankName).FirstOrDefault();

                        var userquest = _userSecurityQuestionRepo.GetQuestionsByUserId(currentUser.UserID).ToList();//db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == Userid).ToList();
                        var q = 0;
                        foreach (var ite in userquest)
                        {
                            if (q == 0)
                            {
                                objaccount.SecurityQuestion1 = ite.Question_Id;
                                objaccount.SecurityAnswer1 = ite.Answer;
                            }
                            else
                            {
                                objaccount.SecurityQuestion2 = ite.Question_Id;
                                objaccount.SecurityAnswer2 = ite.Answer;
                            }

                            q++;
                        }

                    }
                }

                objinvestor = new InvestorAccount(objaccount, objdocuments, onjknowledgeassessment);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objinvestor = null;
            }

            return objinvestor;
        }

        public InvestorAccount GetInvestorAccountInfo()
        {
            try
            {
                var objaccount = new InvestorAccountDetailsModel();
                //InvestorDocuments objdocuments = new InvestorDocuments();
                var objknowledgeassessment = new Knowledgeassessment();
                var username = HttpContext.Current.User.Identity.Name.Trim();
                var currentUser = _userRepo.GetByUsername(username);

                //////////////////////////////////// Get list of Security Questions , Nature of buisness and banks list for binding with drop downs ///////////////////////// 

                objaccount.SecurityQuestionList = _securityQuestionRepo.GetByStatus(true).ToList();//db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();
                objaccount.BanksList = _bankRepo.GetActiveBanks().ToList();//db.tbl_BanksList.Where(p => p.Isactive == true).ToList();

                objknowledgeassessment.Parentlist =
                    _knowledgeAssesmentParentRepo.Find(k => k.InvType == "B" || k.InvType == "I")
                        .OrderBy(p => p.Priority)
                        .ToList();
                    //db.tbl_Knowledgeassessment_Parentlist.Where(k => k.InvType == "B" || k.InvType == "I")
                    //  .OrderBy(p => p.Priority)
                    //  .ToList();


                //////////////////////////////////// If user is logged in then get details of user from Userid for displaying in account details ///////////////////

                if (currentUser != null)
                {
                    //int Userid = Convert.ToInt32(id);
                    //objdocuments.Userid = Userid;
                    var objuser = _accountDetailRepo.GetByUserId(currentUser.UserID);//db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid));

                    //var commonUser = new UserCommonOperations();

                    if (objuser != null)
                    {

                        objaccount.UserVerificationStatus = "unverified";
                        objaccount._Titles = new SelectList(TitleItems(), "Value", "Text", objuser.Title);

                        objaccount.Title = objuser.Title;
                        objaccount._Gender = new SelectList(GenderItems(), "Value", "Text", objuser.Gender);
                        objaccount._Nationality = new SelectList(NationalityItems(), "Value", "Text", objuser.Nationality);
                        objaccount._IncomeRange = new SelectList(IncomerangeItems(), "Value", "Text", objuser.IncomeRange);
                        objaccount.IncomeRange = objuser.IncomeRange;

                        objaccount.FullName = objuser.FirstName + "" + objuser.LastName;
                        objaccount.ICNumber = objuser.NRIC_Number;
                        objaccount.PassportNumber = objuser.PassportNumber;
                        objaccount.PRStatus = objuser.PRstatus;
                        //chnage format 01/02/2013
                        objaccount.DateofBirth = objuser.DateofBirth;
                        objaccount.DisplayName = objuser.DisplayName;
                        objaccount.Nationality = objuser.Nationality;

                        var resAddress = CommonMethods.LoadAddressField(objuser.ResidentialAddress);
                        objaccount.ResidentialAddress1 = resAddress[0];
                        objaccount.ResidentialAddress2 = resAddress[1];
                        objaccount.ResidentialPostalCode = resAddress[2];

                        objaccount.MainContactNumber = objuser.Main_OfficeContactnumber;
                        objaccount.Othercontactnumber = objuser.OptionalContactnumber;
                        objaccount.Emailaddress = currentUser.UserName;//.GetCurrentUserEmail();
                        objaccount.Occupation = objuser.Occupation;
                        objaccount.BankAccountName = objuser.AccountName;
                        objaccount.BankAccountNumber = objuser.AccountNumber;
                        objaccount.BankId = Convert.ToInt32(objuser.Bank_Id);
                        objaccount.BankAccountNumber = objuser.AccountNumber;
                        objaccount.BankAccountName = objuser.AccountName;
                        objaccount.BranchName = objuser.BranchName;
                        objaccount.BranchNumber = objuser.BranchNumber;
                        objaccount.BankNumber = objuser.BankNumber;
                        objaccount.SecurityQuestion1 =
                            _userSecurityQuestionRepo.GetQuestionsByUserId(currentUser.UserID)
                                .Select(q => q.Question_Id)
                                .FirstOrDefault();
                                //Convert.ToInt32(db.tbl_SecurityQuestionsForUsers.Where(p => p.User_ID == Userid).Select(p => p.Question_Id).FirstOrDefault());
                        objaccount.SecurityQuestion2 =
                            _userSecurityQuestionRepo.GetQuestionsByUserId(currentUser.UserID)
                                .Where(q => q.Question_Id != objaccount.SecurityQuestion1.Value)
                                .Select(p => p.Question_Id)
                                .FirstOrDefault();
                                //Convert.ToInt32(db.tbl_SecurityQuestionsForUsers.Where(p => p.Question_Id != objaccount.SecurityQuestion1 && p.User_ID == Userid).Select(p => p.Question_Id).FirstOrDefault())))));
                        objaccount.SecurityAnswer1 =
                            _userSecurityQuestionRepo.GetById(objaccount.SecurityQuestion1.Value).Answer;
                                                                        //Convert.ToString((from p in db.tbl_SecurityQuestionsForUsers
                                                                       //where p.Question_Id == objaccount.SecurityQuestion1 && p.User_ID == Userid
                                                                       //select p.Answer).FirstOrDefault());

                        objaccount.SecurityAnswer2 =
                            _userSecurityQuestionRepo.GetById(objaccount.SecurityQuestion2.Value).Answer; 
                                                                       //Convert.ToString((from p in db.tbl_SecurityQuestionsForUsers
                                                                       //where p.Question_Id == objaccount.SecurityQuestion2 && p.User_ID == Userid
                                                                       //select p.Answer).FirstOrDefault());
                    }
                    else
                    {
                        objaccount._Titles = new SelectList(TitleItems(), "Value", "Text");
                        objaccount._Gender = new SelectList(GenderItems(), "Value", "Text", "Male");
                        objaccount._Nationality = new SelectList(NationalityItems(), "Value", "Text", "Singapore");
                        objaccount._IncomeRange = new SelectList(IncomerangeItems(), "Value", "Text", "50,000-1,00,000");
                        objaccount.Emailaddress = currentUser.UserName;//.GetCurrentUserEmail();
                    }
                }
                else
                {
                    objaccount._Titles = new SelectList(TitleItems(), "Value", "Text");
                    objaccount._Gender = new SelectList(GenderItems(), "Value", "Text", "Male");
                    objaccount._Nationality = new SelectList(NationalityItems(), "Value", "Text", "Singapore");
                    objaccount._IncomeRange = new SelectList(IncomerangeItems(), "Value", "Text", "50,000-1,00,000");
                }

                ///////////////////////////// for setting steps value for enabling the tabs depending upon the current step //////////////////////////////////////////

                var objinvestor = new InvestorAccount(objaccount, objknowledgeassessment);
                if (HttpContext.Current.Session["step"] == null || HttpContext.Current.Session["step"].ToString() == "")
                {
                    HttpContext.Current.Session["step"] = "1";
                }
                return objinvestor;

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;

            }
        }

        public string CreateInvestorAccount(InvestorAccountDetailsModel objinvestor)
        {
            throw new NotImplementedException();
        }

        public void LogDetailsOnError(InvestorAccountDetailsModel investor)
        {
            throw new NotImplementedException();
        }

        public string GetKnowledgeAssessmentOptions(int parentid)
        {
            throw new NotImplementedException();
        }

        public string GetKnowledgeAssessmentOptions(int parentid, long userid)
        {
            throw new NotImplementedException();
        }

        public string SaveKnowledgeAssesmentOptions(string XMLdocs)
        {
            throw new NotImplementedException();
        }

        public string SaveKnowledgeAssesmentOptions(string XMLdocs, long userid)
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> TitleItems()
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> GenderItems()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<SelectListItem> NationalityItems()
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> IncomerangeItems()
        {
            throw new NotImplementedException();
        }

        public InvestorLoanOfferDTO GetLoanAndBorrowerBriefDetailByRequestId(long loanRequestId)
        {
            throw new NotImplementedException();
        }

        public InvestorLoanOfferDTO GetLoanAndBorrowerBriefDetailForPreviewByRequestId(long loanRequestId)
        {
            throw new NotImplementedException();
        }

        public string SaveLoanOffer(string amount, string rate, string loanRequestId, string term)
        {
            throw new NotImplementedException();
        }

        public LiveLoanListingsDTO GetLiveLoanListings()
        {
            throw new NotImplementedException();
        }

        public DateTime? GetLastEmiDateByLoanRequestId(long loanRequestId)
        {
            throw new NotImplementedException();
        }

        public bool? ChkLastEmiIsPending(long requestId)
        {
            throw new NotImplementedException();
        }

        public InvestorDashboardDTO LoadDashboardInformation()
        {
            throw new NotImplementedException();
        }

        public AccountDetail GetUserProfileDetailbyUserId()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LoanTransaction> GetAllTransactionbyUserId()
        {
            throw new NotImplementedException();
        }

        public string CheckCurrentBalance(decimal amount)
        {
            throw new NotImplementedException();
        }

        public string WithDrawOfferbyOfferId(int offerId)
        {
            throw new NotImplementedException();
        }

        public void LoanTransactionCredit(decimal? amountBefore, decimal? amountToCredited, long? loanRequestId, string paymentMode,
            string paymentTerms, long? userId, decimal? balanceAfter)
        {
            throw new NotImplementedException();
        }

        public void LoanTransactionDebit(decimal? amountBefore, decimal? amountTobeDebited, long? loanRequestId, string paymentMode,
            string paymentTerms, long? userId, decimal? balanceAfter)
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> OutstandingLitigation()
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> IsPersonalGuarantee()
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> AddCustomValueInEnd()
        {
            throw new NotImplementedException();
        }

        public List<SelectListItem> MoolaCoreTime()
        {
            throw new NotImplementedException();
        }

        public string TestUserVerification(int? status)
        {
            throw new NotImplementedException();
        }

        public bool CheckVerificationStatus(string columnValue)
        {
            throw new NotImplementedException();
        }

        public string AddToWatchList(long loanRequestId)
        {
            throw new NotImplementedException();
        }

        public BorrowerAccount CorporateUserLoad()
        {
            throw new NotImplementedException();
        }

        public string CreateCorporateInvestor(BorrowerAccountDetailsModel objborrower)
        {
            throw new NotImplementedException();
        }

        public bool CheckDisplayNameAvailability(string displayname, string emailaddress)
        {
            throw new NotImplementedException();
        }
    }
}
