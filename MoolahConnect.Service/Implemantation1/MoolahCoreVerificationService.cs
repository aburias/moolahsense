﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
    public class MoolahCoreVerificationService : IMoolahCoreVerificationService
    {
         private IMoolahCoreVerificationRepository _IMoolahCoreVerificationRepository;

         public MoolahCoreVerificationService(IMoolahCoreVerificationRepository IMoolahCoreVerificationRepository)
        {
            _IMoolahCoreVerificationRepository = IMoolahCoreVerificationRepository;
        }

         public Models.BusinessModels.MoolahCoreVerification GetById(long requestID)
        {
            return _IMoolahCoreVerificationRepository.GetById(requestID);
        }

        public void Update(Models.BusinessModels.MoolahCoreVerification moolahCoreVerification)
        {
            _IMoolahCoreVerificationRepository.Update(moolahCoreVerification);
        }

        public void Create(Models.BusinessModels.MoolahCoreVerification moolahCoreVerification)
        {
            _IMoolahCoreVerificationRepository.Create(moolahCoreVerification);
        }
    }
}
