﻿using System.Collections.Generic;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
    public class PersonGuarantorService : IPersonGuarantorService
    {
        private readonly IPersonalGuarantorRepository _personalGuarantorRepository;

        public PersonGuarantorService(IPersonalGuarantorRepository personalGuarantorRepository)
        {
            _personalGuarantorRepository = personalGuarantorRepository;
        }

        public IEnumerable<PersonalGuaranteeInfo> Get(long requestId)
        {
            return _personalGuarantorRepository.Find(a => a.Request_Id == requestId);
        }

        public void Update(PersonalGuaranteeInfo personalGuaranteeInfo)
        {
            _personalGuarantorRepository.Update(personalGuaranteeInfo);
        }

        public void Save(PersonalGuaranteeInfo personalGuaranteeInfo)
        {
            _personalGuarantorRepository.Create(personalGuaranteeInfo);
        }

        public void Delete(PersonalGuaranteeInfo personalGuaranteeInfo)
        {
            _personalGuarantorRepository.Delete(personalGuaranteeInfo.InfoId);
        }
    }
}