﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
  public  class LoanPurposeService:ILoanPurposeService
    {
      private readonly ILoanPurposeRepository _loanPurposeRepository;

      public LoanPurposeService(ILoanPurposeRepository loanPurposeRepository)
      {
          _loanPurposeRepository = loanPurposeRepository;
      }

      public IEnumerable<LoanPurpose> Get()
      {
          return _loanPurposeRepository.GetAll();
      }
    }
}
