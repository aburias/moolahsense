﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
    public class RepaymentMessageService : IRepaymentMessageService
    {
        IRepaymentMessageRepository _IRepaymentMessageRepository;

        public RepaymentMessageService(IRepaymentMessageRepository iRepaymentMessageRepository)
        {
            _IRepaymentMessageRepository = iRepaymentMessageRepository;
        }

        public void Create(RepaymentMessage repaymentMessage)
        {
            _IRepaymentMessageRepository.Create(repaymentMessage);
        }

        public void Update(RepaymentMessage repaymentMessage)
        {
            _IRepaymentMessageRepository.Update(repaymentMessage);
        }


        public void Delete(long repaymentID)
        {
            _IRepaymentMessageRepository.Delete(repaymentID);
        }

        public RepaymentMessage GetByID(long repaymentID)
        {
            return _IRepaymentMessageRepository.GetById(repaymentID);
        }
    }
}
