﻿using System.Collections.Generic;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
    public class MoolahPerkLenderService : IMoolahPerkLenderService
    {
        private readonly IMoolahPerkLenderRepository _moolahPerkLenderRepository;

        public MoolahPerkLenderService(IMoolahPerkLenderRepository moolahPerkLenderRepository)
        {
            _moolahPerkLenderRepository = moolahPerkLenderRepository;
        }

        public IEnumerable<MoolahPerkLender> GetAll()
        {
            return _moolahPerkLenderRepository.GetAll();
        }
    }
}