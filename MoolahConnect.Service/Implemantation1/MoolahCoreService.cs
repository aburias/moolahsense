﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
    public class MoolahCoreService : IMoolahCoreService
    {
        private readonly IMoolahCoreRepository _moolahCoreRepository;

        public MoolahCoreService(IMoolahCoreRepository moolahCoreRepository)
        {
            _moolahCoreRepository = moolahCoreRepository;
        }

        public MoolahCore Get(long loanRequestId)
        {
           return _moolahCoreRepository.Find(a => a.LoanRequest_Id == loanRequestId).FirstOrDefault();
        }

        public void Create(MoolahCore moolahCore)
        {
            _moolahCoreRepository.Create(moolahCore);
        }

        public void Update(MoolahCore moolah)
        {
            _moolahCoreRepository.Update(moolah);
        }

        public void Deattched(MoolahCore moolahCore)
        {
            throw new NotImplementedException();
        }

        public void CreateOrUpdate(MoolahCore moolah)
        {
            _moolahCoreRepository.CreateOrUpdate(moolah);
        }
    }
}
