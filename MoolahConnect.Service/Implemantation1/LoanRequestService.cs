﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Infrastructure;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
   public class LoanRequestService:ILoanRequestService
    {
       private readonly ILoanRequestRepository _loanRequestRepository;


       public LoanRequestService(ILoanRequestRepository loanRequestRepository)
       {
           _loanRequestRepository = loanRequestRepository;
       }

       public void Save(LoanRequest loanRequest)
       {
           throw new NotImplementedException();
       }

       public void Update(LoanRequest loanRequest, params Expression<Func<LoanRequest, object>> [] modifiedProperties)
       {
           _loanRequestRepository.Update(loanRequest,modifiedProperties);
       }
      public IEnumerable<LoanRequest> GetLoanRequestbyUserID(long userId)
       {
           return _loanRequestRepository.Find(a => a.User_ID == userId,
               new List<string> { EntityRelationshipName.LoanPurposes, EntityRelationshipName.PersonalGuaranteeInfos, EntityRelationshipName.MoolahCores });
       }

       public LoanRequest GetLoanRequestbyRequestID(long requestId)
       {
           return _loanRequestRepository.Find(a => a.RequestId == requestId,
               new List<string> { EntityRelationshipName.LoanPurposes,
                   EntityRelationshipName.PersonalGuaranteeInfos,
                   EntityRelationshipName.MoolahCores,
               EntityRelationshipName.MoolahPeris,
               EntityRelationshipName.MoolahCoreVerifications,
               EntityRelationshipName.MoolahPeriVerifications,
               EntityRelationshipName.RepaymentMessages}).FirstOrDefault();
       }


       public void Update(LoanRequest loanRequest)
       {
           throw new NotImplementedException();
       }
    }
}
