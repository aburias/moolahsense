﻿using System;
using System.Collections.Generic;
using System.Linq;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Models.CustomModels;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;

namespace MoolahConnect.Services.Implemantation1
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _iUserRepository;

        public UserService(IUserRepository iUserRepository)
        {
            _iUserRepository = iUserRepository;
        }

        public string GetUserRoleByUsername(string username)
        {
            throw new NotImplementedException();
        }

        public bool Remove(string username)
        {
            throw new NotImplementedException();
        }

        public int GetFailedPasswordAttemptsCount(Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool LockUser(Guid userId)
        {
            throw new NotImplementedException();
        }

        public bool IsEmailVerified(string username)
        {
            throw new NotImplementedException();
        }

        public bool UpdateEmailVerification(string username, bool verified)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<string> GetAllAdminUsers()
        {
            throw new NotImplementedException();
        }

        public bool ValidateAdminUser(string username, string password)
        {
            throw new NotImplementedException();
        }

        public void SaveDigitalSignature(long userID)
        {
            throw new NotImplementedException();
        }

        public User GetUserFromAspNetId(Guid id)
        {
            throw new NotImplementedException();
        }

        public User GetUserFromId(long id)
        {
            throw new NotImplementedException();
        }

        public MoolahConnect.Models.BusinessModels.AccountDetail GetAccountDetailsByID(long id)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<User> GetApprovedInvestors()
        {
            throw new NotImplementedException();
        }

        public User GetUser(string userName)
        {
            return _iUserRepository.Find(a => a.UserName.Equals(userName,StringComparison.OrdinalIgnoreCase)).FirstOrDefault();
        }


        AccountDetailDTO IUserService.GetAccountDetailsByID(long id)
        {
            throw new NotImplementedException();
        }

        public UsersSummaryDTO GetUsersSummary()
        {
            try
            {
                var usersSummary = new UsersSummaryDTO();
                var users = _iUserRepository.GetAll();

                usersSummary.TotalUsers = users.Count();
                usersSummary.TotalBorrower = users.Where(u => u.UserRole == UserRole.Borrower.ToString()).Count();
                usersSummary.TotalInvestors = users.Where(u => u.UserRole == UserRole.Investor.ToString()).Count();

                return usersSummary;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new UsersSummaryDTO();
            }
        }
    }
}