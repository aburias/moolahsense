﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnect.Services.Implemantation1
{
   public class AccountDetailService : IAccountDetailService
    {
       private IAccountDetailRepository _accountRepository;

       public AccountDetailService(IAccountDetailRepository repository)
        {
            this._accountRepository = repository;
        }


        public IEnumerable<Models.BusinessModels.AccountDetail> Get()
        {
            //using (var db = new dbMoolahConnectEntities())
            //{
            //    return db.tbl_AccountDetails.ToList();
            //}
            return _accountRepository.GetAll();
        }

        public IEnumerable<Models.BusinessModels.AccountDetail> Get(List<long> idList)
        {
            //using (var db = new dbMoolahConnectEntities())
            //{
            //    return db.tbl_AccountDetails.Where(e => e.User_ID != null && userIds.Contains((long) e.User_ID)).ToList();
            //}

            return _accountRepository.Find(a => a.User_ID != null && idList.Contains((long)a.User_ID)).ToList();
        }

       public AccountDetail GetByUser(long id)
       {
           return _accountRepository.Find(a => a.User_ID == id).FirstOrDefault();
       }

       public User GetUser(string name)
        {
            throw new NotImplementedException();
        }
    }
}
