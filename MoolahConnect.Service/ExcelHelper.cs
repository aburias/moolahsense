﻿using Microsoft.Office.Interop.Excel;

namespace MoolahConnect.Services
{
    public class ExcelHelper
    {
        private Application _app;
        private Workbook _workbook;
        private Worksheet _worksheet;
        private Range _workSheetRange;

        public ExcelHelper()
        {
            CreateDoc();
        }

        public void Save(string path)
        {
            _worksheet.SaveAs(path);
            _workbook.Close();
            _app.Quit();
        }

        public void CreateDoc()
        {
            _app = new Application();
            _workbook = _app.Workbooks.Add(1);
            _worksheet = (Worksheet) _workbook.Sheets[1];
        }

        public void CreateHeaders(int row, int col, string htext, string cell1, string cell2, int mergeColumns, string b,
                                  bool font, int size, string fcolor)
        {
            _worksheet.Cells[row, col] = htext;
            _workSheetRange = _worksheet.Range[cell1, cell2];
            _workSheetRange.Merge(mergeColumns);
            switch (b)
            {
                case "YELLOW":
                    _workSheetRange.Interior.Color = System.Drawing.Color.Yellow.ToArgb();
                    break;
                case "GRAY":
                    _workSheetRange.Interior.Color = System.Drawing.Color.Gray.ToArgb();
                    break;
                case "GAINSBORO":
                    _workSheetRange.Interior.Color = System.Drawing.Color.Gainsboro.ToArgb();
                    break;
                case "Turquoise":
                    _workSheetRange.Interior.Color = System.Drawing.Color.Turquoise.ToArgb();
                    break;
                case "PeachPuff":
                    _workSheetRange.Interior.Color = System.Drawing.Color.PeachPuff.ToArgb();
                    break;
                default:
                    _workSheetRange.Interior.Color = System.Drawing.Color.White.ToArgb();
                    break;

            }

            _workSheetRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            _workSheetRange.Font.Bold = font;
            _workSheetRange.ColumnWidth = size;
            _workSheetRange.Font.Color = fcolor.Equals("")
                                             ? System.Drawing.Color.White.ToArgb()
                                             : System.Drawing.Color.Black.ToArgb();

        }

        public void AddData(int row, int col, string data, string cell1, string cell2, string format)
        {
            _worksheet.Cells[row, col] = data;
            _workSheetRange = _worksheet.Range[cell1, cell2];
            _workSheetRange.Borders.Color = System.Drawing.Color.Black.ToArgb();
            _workSheetRange.NumberFormat = format;
        }
    }
}