﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.FinancialCalculations;
using MoolahConnect.Util.GenerateReference;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.Models;

namespace MoolahConnect.Services.Implementation
{
    public class LoanAllocationDA : ILoanAllocationDA
    {

        public tbl_LoanRequests GetLoanRequestByRequestId(long requestId)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_LoanRequests.Include("tbl_Users.tbl_AccountDetails").Where(x => x.RequestId == requestId).SingleOrDefault();
            }
        }

        public IEnumerable<tbl_LoanRequests> GetAllPublishedLoanRequests()
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return
                    db.tbl_LoanRequests.Include("tbl_LoanPurposesList").Include("tbl_Users.tbl_AccountDetails").Where(
                        e =>
                            e.IsApproved.HasValue && e.IsApproved.Value
                            && e.LoanStatus == (int)LoanRequestStatus.InProgress).OrderBy(e => e.DateCreated).ToList();
            }
        }

        public IEnumerable<tbl_Loanoffers> GetLoanOffersByrequestId(long requestId)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_Loanoffers
                    .Include("tbl_Users.tbl_AccountDetails")
                    .Include("tbl_LoanRequests.tbl_Users.tbl_AccountDetails")
                    .Where(x => x.LoanRequest_Id == requestId).ToList();
            }
        }

        public bool UpdateLoanRequestStatusByRequestId(long requestId, LoanRequestStatus status)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == requestId).SingleOrDefault();

                    if (loanRequest == null)
                        return false;

                    loanRequest.LoanStatus = (int)status;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {

                return false;
            }
        }

        public bool UpdateLoanRequestOnApproval(long requestId, double acceptedAmount, decimal acceptedRate)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == requestId).SingleOrDefault();

                    if (loanRequest == null)
                        return false;

                    loanRequest.LoanStatus = (int)LoanRequestStatus.Matched;
                    loanRequest.FinalAmount = (decimal)acceptedAmount;
                    loanRequest.MoolahFees = (decimal)LoanRequestCalc.CalculateMoolahFees(acceptedAmount, loanRequest.Terms);
                    loanRequest.RequestedRate = loanRequest.Rate.Value;
                    loanRequest.Rate = acceptedRate;
                    loanRequest.EIR = (decimal)LoanRequestCalc.CalculateTargetEIR(acceptedAmount, loanRequest.Terms, (double)acceptedRate);
                    loanRequest.AcceptedDate = DateTime.Now.ToUniversalTime();

                    //Remove from watchlist.
                    var loanRequestsInWatchlist = db.tbl_WatchList.Where(x => x.Request_ID == requestId);
                    db.tbl_WatchList.RemoveRange(loanRequestsInWatchlist);

                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool UpdateLoanOffer(int offerId, double acceptedAmount, double? acceptedRate, Util.Enums.LoanOfferStatus status, string contractIdentifier, string reasonToReject = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var offer = db.tbl_Loanoffers.Where(x => x.OfferId == offerId).SingleOrDefault();

                    if (offer == null)
                        return false;

                    offer.AcceptedAmount = (decimal)acceptedAmount;
                    offer.AcceptedRate = acceptedRate.HasValue ? (decimal?)acceptedRate.Value : null;
                    offer.OfferStatus = (int)status;
                    offer.ReasonToReject = reasonToReject;
                    offer.ModifiedDate = DateTime.UtcNow;
                    offer.ContractId = contractIdentifier;

                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public bool InsertLoanAmortization(tbl_LoanAmortization ammortization)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    db.tbl_LoanAmortization.Add(ammortization);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {

                return false;
            }
        }


        public IEnumerable<tbl_Loanoffers> GetLoanOffersByInvestor(string username, DateTime? _from = null, DateTime? _to = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_Loanoffers.Include("tbl_LoanRequests.tbl_Users.tbl_AccountDetails").AsEnumerable()
                        .Where(l => (l.tbl_Users != null
                            && l.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower())
                            && (_from == null || l.DateCreated.Date >= _from.Value.ToUniversalTime().Date)
                            && (_to == null || l.DateCreated.Date <= _to.Value.ToUniversalTime().Date))
                            .ToList();
                }
            }
            catch (Exception ex)
            {

                return new List<tbl_Loanoffers>();
            }
        }

        public IEnumerable<LoanInvestmentEntity> GetLoanInvestmentsByInvestor(string username, DateTime? _from = null, DateTime? _to = null)
        {
            try
            {
                DateTime fromUniversalTime = new DateTime();
                if (_from.HasValue)
                    fromUniversalTime = _from.Value.ToUniversalTime();

                DateTime toUniversalTime = new DateTime();
                if (_to.HasValue)
                    toUniversalTime = _to.Value.ToUniversalTime();

                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();

                    if (user == null)
                    {
                        return new List<LoanInvestmentEntity>();
                    }

                    var loanInvestments = new List<LoanInvestmentEntity>();

                    var requestsWithOffers = (from lo in db.tbl_Loanoffers
                                              where (lo.tbl_Users != null && lo.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower())
                                              && (lo.OfferStatus == (int)LoanOfferStatus.Accepted || lo.OfferStatus == (int)LoanOfferStatus.RejectedByAdmin)
                                              join lr in
                                                  (from x in db.tbl_LoanRequests
                                                   where
                                                       (x.LoanStatus == (int)LoanRequestStatus.Matched
                                                       || x.LoanStatus == (int)LoanRequestStatus.Matured
                                                       || x.LoanStatus == (int)LoanRequestStatus.FundApproved
                                                       || x.LoanStatus == (int)LoanRequestStatus.FundRejected)
                                                       && (_from == null || x.ApproveDate >= fromUniversalTime)
                                                       && (_to == null || x.ApproveDate <= toUniversalTime)
                                                   select x)
                                              on lo.LoanRequest_Id equals lr.RequestId
                                              group new { LR = lr, LO = lo } by new { lr.RequestId } into grp
                                              select new LoanInvestmentEntity()
                                              {
                                                  LoanRequestId = grp.Key.RequestId,
                                                  LoanRequestStatus = grp.Max(a => a.LR.LoanStatus),
                                                  CompanyName = grp.Max(x => x.LR.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                                                      ? x.LR.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName : string.Empty),
                                                  // InterestReceived = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == grp.Key.RequestId
                                                  // && la.PayStatus == (int)RepaymentPayStatus.Paid)
                                                  //.Sum(l => Math.Round((l.Interest.Value * grp.Sum(x => x.LO.AcceptedAmount.Value) / grp.Max(x => x.LR.FinalAmount.Value)), 2)),
                                                  LoanAmount = grp.DefaultIfEmpty().Sum(x => x.LO.AcceptedAmount),
                                                  LoanStartDate = grp.DefaultIfEmpty().Max(x => x.LR.AcceptedDate),
                                                  OffersAmount = grp.DefaultIfEmpty().Sum(x => x.LO.OfferedAmount),
                                                  //                     PrincipalRecevived = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == grp.Key.RequestId
                                                  //&& l.PayStatus == (int)RepaymentPayStatus.Paid)
                                                  //.Sum(
                                                  //l => Math.Round(l.Principal.Value * grp.Sum(x => x.LO.AcceptedAmount.Value) / grp.Max(x => x.LR.FinalAmount.Value), 2)
                                                  //),
                                                  Rate = grp.DefaultIfEmpty().Max(x => x.LR.Rate),
                                                  Tenor = grp.DefaultIfEmpty().Max(x => x.LR.Terms),
                                                  offerIds = db.tbl_Loanoffers.Where(x => x.LoanRequest_Id == grp.DefaultIfEmpty().Max(a => a.LR.RequestId)
                                                  && x.tbl_Users.UserID == user.UserID
                                                  && x.OfferStatus == (int)LoanOfferStatus.Accepted
                                                  ).Select(x => x.OfferId).ToList()
                                              }).ToList();


                    for (int i = 0; i < requestsWithOffers.Count; i++)
                    {
                        LoanInvestmentEntity requestWithOffer = requestsWithOffers[i];

                        if (db.tbl_LoanAmortization.Any(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                            && l.PayStatus != (int)RepaymentPayStatus.Paid))
                        {
                            if (db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                                && l.PayStatus == (int)RepaymentPayStatus.Paid).Any())
                            {
                                var pendingRep = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                                    && l.PayStatus == (int)RepaymentPayStatus.Paid)
                                    .Sum(
                                    l => l.Principal.Value * requestWithOffer.LoanAmount.Value / l.tbl_LoanRequests.FinalAmount.Value
                                    );

                                requestsWithOffers[i].PrincipalRecevived = Math.Round(pendingRep, 2);
                            }
                            else
                            {
                                requestsWithOffers[i].PrincipalRecevived = 0;
                            }
                        }
                        else
                        {
                            requestsWithOffers[i].PrincipalRecevived = requestWithOffer.LoanAmount;
                        }


                        if (db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                            && l.PayStatus == (int)RepaymentPayStatus.Paid).Any())
                        {
                            requestsWithOffers[i].InterestReceived = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == requestWithOffer.LoanRequestId
                                && la.PayStatus == (int)RepaymentPayStatus.Paid).Sum(l => Math.Round((l.Interest.Value * requestWithOffer.LoanAmount.Value / l.tbl_LoanRequests.FinalAmount.Value), 2));
                        }
                        else
                        {
                            requestsWithOffers[i].InterestReceived = 0;
                        }



                        var passedRepayments = db.tbl_LoanAmortization
                            .Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                            && l.PayStatus != (int)RepaymentPayStatus.Pending).ToList();

                        if (passedRepayments.Count() <= 0)
                        {
                            requestsWithOffers[i].LatestRepaymentStatus = (int)RepaymentPayStatus.Pending;
                        }
                        else
                        {
                            requestsWithOffers[i].LatestRepaymentStatus = passedRepayments[passedRepayments.Count - 1].PayStatus.Value;
                        }

                    }

                    return requestsWithOffers;
                }
            }
            catch (Exception ex)
            {

                return new List<LoanInvestmentEntity>();
            }
        }

        public IEnumerable<LoanInvestmentEntity> GetIndividualLoanInvestmentsByInvestor(string username, DateTime? _from = null, DateTime? _to = null)
        {
            try
            {
                DateTime fromUniversalTime = new DateTime();
                if (_from.HasValue)
                    fromUniversalTime = _from.Value.ToUniversalTime();

                DateTime toUniversalTime = new DateTime();
                if (_to.HasValue)
                    toUniversalTime = _to.Value.ToUniversalTime();

                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();

                    if (user == null)
                    {
                        return new List<LoanInvestmentEntity>();
                    }

                    var loanInvestments = new List<LoanInvestmentEntity>();

                    var requestsWithOffers = (from lo in db.tbl_Loanoffers
                                              where
                                              (lo.tbl_Users != null && lo.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower())
                                              && (lo.OfferStatus == (int)LoanOfferStatus.Accepted || lo.OfferStatus == (int)LoanOfferStatus.RejectedByAdmin)
                                              && lo.tbl_LoanRequests != null
                                              && (lo.tbl_LoanRequests.LoanStatus == (int)LoanRequestStatus.Matched
                                                       || lo.tbl_LoanRequests.LoanStatus == (int)LoanRequestStatus.Matured
                                                       || lo.tbl_LoanRequests.LoanStatus == (int)LoanRequestStatus.FundApproved
                                                       || lo.tbl_LoanRequests.LoanStatus == (int)LoanRequestStatus.FundRejected)
                                              && (_from == null || lo.tbl_LoanRequests.ApproveDate >= fromUniversalTime)
                                              && (_to == null || lo.tbl_LoanRequests.ApproveDate <= toUniversalTime)
                                              select new LoanInvestmentEntity()
                                              {
                                                  LoanRequestId = lo.tbl_LoanRequests.RequestId,
                                                  LoanRequestStatus = lo.tbl_LoanRequests.LoanStatus,
                                                  CompanyName = lo.tbl_LoanRequests.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                                                      ? lo.tbl_LoanRequests.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName : string.Empty,
                                                  // InterestReceived = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == grp.Key.RequestId
                                                  // && la.PayStatus == (int)RepaymentPayStatus.Paid)
                                                  //.Sum(l => Math.Round((l.Interest.Value * grp.Sum(x => x.LO.AcceptedAmount.Value) / grp.Max(x => x.LR.FinalAmount.Value)), 2)),
                                                  LoanAmount = lo.AcceptedAmount,
                                                  LoanStartDate = lo.tbl_LoanRequests.AcceptedDate,
                                                  OffersAmount = lo.OfferedAmount,
                                                  //                     PrincipalRecevived = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == grp.Key.RequestId
                                                  //&& l.PayStatus == (int)RepaymentPayStatus.Paid)
                                                  //.Sum(
                                                  //l => Math.Round(l.Principal.Value * grp.Sum(x => x.LO.AcceptedAmount.Value) / grp.Max(x => x.LR.FinalAmount.Value), 2)
                                                  //),
                                                  Rate = lo.tbl_LoanRequests.Rate,
                                                  Tenor = lo.tbl_LoanRequests.Terms,
                                                  OfferID = lo.OfferId,
                                              }).ToList();


                    for (int i = 0; i < requestsWithOffers.Count; i++)
                    {
                        LoanInvestmentEntity requestWithOffer = requestsWithOffers[i];

                        if (db.tbl_LoanAmortization.Any(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                            && l.PayStatus != (int)RepaymentPayStatus.Paid))
                        {
                            if (db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                                && l.PayStatus == (int)RepaymentPayStatus.Paid).Any())
                            {
                                var pendingRep = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                                    && l.PayStatus == (int)RepaymentPayStatus.Paid)
                                    .Sum(
                                    l => l.Principal.Value * requestWithOffer.LoanAmount.Value / l.tbl_LoanRequests.FinalAmount.Value
                                    );

                                requestsWithOffers[i].PrincipalRecevived = Math.Round(pendingRep, 2);
                            }
                            else
                            {
                                requestsWithOffers[i].PrincipalRecevived = 0;
                            }
                        }
                        else
                        {
                            requestsWithOffers[i].PrincipalRecevived = requestWithOffer.LoanAmount;
                        }


                        if (db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                            && l.PayStatus == (int)RepaymentPayStatus.Paid).Any())
                        {
                            requestsWithOffers[i].InterestReceived = Math.Round(db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == requestWithOffer.LoanRequestId
                                && la.PayStatus == (int)RepaymentPayStatus.Paid).Sum(l => (l.Interest.Value * requestWithOffer.LoanAmount.Value / l.tbl_LoanRequests.FinalAmount.Value)), 2);
                        }
                        else
                        {
                            requestsWithOffers[i].InterestReceived = 0;
                        }



                        var passedRepayments = db.tbl_LoanAmortization
                            .Where(l => l.LoanRequest_ID == requestWithOffer.LoanRequestId
                            && l.PayStatus != (int)RepaymentPayStatus.Pending).ToList();

                        if (passedRepayments.Count() <= 0)
                        {
                            requestsWithOffers[i].LatestRepaymentStatus = (int)RepaymentPayStatus.Pending;
                        }
                        else
                        {
                            requestsWithOffers[i].LatestRepaymentStatus = passedRepayments[passedRepayments.Count - 1].PayStatus.Value;
                        }

                    }

                    return requestsWithOffers;
                }
            }
            catch (Exception ex)
            {

                return new List<LoanInvestmentEntity>();
            }
        }

        public IEnumerable<tbl_LoanRequests> GetWatchList(long investorId)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_LoanRequests.Include("tbl_LoanPurposesList").Include("tbl_Users.tbl_AccountDetails").Where(e => e.tbl_WatchList.Any(f => f.User_ID == investorId)).ToList();
            }
        }


        public void AddToWatchList(long userId, long requestId)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var wl = db.tbl_WatchList.FirstOrDefault(e => e.User_ID == userId && e.Request_ID == requestId);
                if (wl != null) return;
                wl = new tbl_WatchList
                {
                    Datecreated = DateTime.Now,
                    Request_ID = requestId,
                    User_ID = userId
                };
                db.tbl_WatchList.Add(wl);
                db.SaveChanges();
            }
        }

        public void RemoveFromWatchList(long userId, long requestId)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var wl = db.tbl_WatchList.FirstOrDefault(e => e.User_ID == userId && e.Request_ID == requestId);
                if (wl == null) return;
                db.tbl_WatchList.Remove(wl);
                db.SaveChanges();
            }
        }


        public bool CanWithdrawLoanOffer(long loanRequestId, int offerId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == loanRequestId).SingleOrDefault();
                    var offer = db.tbl_Loanoffers.Where(x => x.OfferId == offerId).SingleOrDefault();
                    if (loanRequest == null || offer == null)
                        return false;

                    var timeRemaining = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(loanRequest.PublishedDate),
                        Settings.LoanRequestExpiryDays);

                    if (timeRemaining.Length >= 3 && timeRemaining[2] <= 0)
                    {
                        return false;
                    }

                    var pendingQualifiedOffersExThisOffer = db.tbl_Loanoffers.Where(x =>
                        x.LoanRequest_Id == loanRequestId
                        && x.OfferId != offerId
                        && x.OfferStatus == (int)LoanOfferStatus.Pending
                        && x.OfferedRate <= loanRequest.Rate);

                    if (pendingQualifiedOffersExThisOffer.Count() > 0
                        && pendingQualifiedOffersExThisOffer.Sum(x => x.OfferedAmount) >= loanRequest.Amount)
                    {
                        return true;
                    }

                    var pendingAllOffersExThisOffer = db.tbl_Loanoffers.Where(x =>
                        x.LoanRequest_Id == loanRequestId
                        && x.OfferId != offerId
                        && x.OfferStatus == (int)LoanOfferStatus.Pending);

                    if (offer.OfferedRate > loanRequest.Rate
                        && pendingAllOffersExThisOffer.Count() > 0
                        && pendingAllOffersExThisOffer.Sum(x => x.OfferedAmount) >= loanRequest.Amount)
                    {
                        return true;
                    }

                    return false;

                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public BorrowerLoanContractModel GetBorrowerLoanContractModel(long requestId)
        {
            try
            {
                var model = new BorrowerLoanContractModel();

                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == requestId).SingleOrDefault();
                    var repayment = db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == requestId).FirstOrDefault();

                    if (loanRequest != null)
                    {
                        model.LoanRequestId = loanRequest.RequestId;
                        model.UserIDInt = loanRequest.tbl_Users.UserID;
                        model.Username = loanRequest.tbl_Users.UserName;
                        model.AcceptedRate = loanRequest.Rate.HasValue ? loanRequest.Rate.Value : 0;
                        model.AgreementDate = loanRequest.AcceptedDate.HasValue ? loanRequest.AcceptedDate.Value : DateTime.MinValue;
                        model.Amount = loanRequest.FinalAmount.HasValue ? loanRequest.FinalAmount.Value : 0;
                        model.EffectiveRate = loanRequest.EIR.HasValue ? loanRequest.EIR.Value : 0;
                        if (loanRequest.tbl_Users.tbl_AccountDetails.Count == 1)
                        {
                            model.Name = loanRequest.tbl_Users.tbl_AccountDetails.SingleOrDefault().BusinessName;
                            model.RegistrationNumber = loanRequest.tbl_Users.tbl_AccountDetails.SingleOrDefault().IC_CompanyRegistrationNumber;
                        }
                        model.Tenure = loanRequest.Terms;


                        model.TotalInterestPayable = (decimal)CommonMethods.GetTotalInterestPayable(loanRequest.Terms,
                            loanRequest.FinalAmount.HasValue ? (double)loanRequest.FinalAmount.Value : 0,
                            loanRequest.Rate.HasValue ? (double)loanRequest.Rate.Value : 0);
                        model.UserId = loanRequest.tbl_Users.AspnetUserId;

                        model.RegisteredAddress = CommonMethods.LoadAddressFieldStr(loanRequest.tbl_Users.tbl_AccountDetails.FirstOrDefault().RegisteredAddress);
                        model.DirectorPartnetName = CommonMethods.GetAccountFullName(
                            null,
                            loanRequest.tbl_Users.tbl_AccountDetails.FirstOrDefault().FirstName,
                            loanRequest.tbl_Users.tbl_AccountDetails.FirstOrDefault().LastName);
                        model.MonthlyRepaymentAmount = repayment.EmiAmount.HasValue ? repayment.EmiAmount.Value : 0;
                    }

                    var acceptedOffers = db.tbl_Loanoffers.Where(x => x.LoanRequest_Id == requestId
                        && x.OfferStatus == (int)LoanOfferStatus.Accepted).OrderBy(x=>x.ContractId);

                    model.NumberOfAcceptedOffers = acceptedOffers.Count();

                    foreach (tbl_Loanoffers offer in acceptedOffers)
                    {
                        model.AcceptedOffers.Add(new Lender()
                        {
                            Id = offer.OfferId,
                            LoanRequestId = offer.LoanRequest_Id,
                            UserID = offer.tbl_Users.UserID,
                            Username = offer.tbl_Users.UserName,
                            IdNumber = GetIdentificationNumber(offer.tbl_Users.tbl_AccountDetails.First(), offer.tbl_Users.InvestorType),
                            Name = GetName(offer.tbl_Users),
                            Amount = offer.AcceptedAmount.HasValue ? offer.AcceptedAmount.Value : 0,
                            ContractIdentifier = offer.ContractId,
                            RepaymentAmount =
                            LoanRequestCalc.RepaymentAmount(offer.AcceptedAmount.HasValue ? (double)offer.AcceptedAmount.Value : 0,
                            (double)model.AcceptedRate, model.Tenure)
                        });
                    }
                }

                return model;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new BorrowerLoanContractModel();
            }
        }

        public string GetIdentificationNumber(tbl_AccountDetails account, string investorType)
        {
            if (investorType == "Corporate")
            {
                return account.IC_CompanyRegistrationNumber;
            }
            else
            {
                if (account.Nationality != null && account.Nationality.ToLower().Contains("singapore"))
                {
                    return account.NRIC_Number;
                }
                else
                {
                    return account.PassportNumber;
                }
            }
        }

        public string GetName(tbl_Users userAaccount)
        {
            var accountDetails = userAaccount.tbl_AccountDetails.First();

            if (accountDetails == null)
                return string.Empty;

            if (userAaccount.InvestorType == "Corporate")
            {
                return accountDetails.BusinessName;
            }
            else
            {
                return CommonMethods.GetAccountFullName(accountDetails.Title, accountDetails.FirstName, accountDetails.LastName);
            }
        }


        public InvestorLoanContractModel GetInvestorLoanContractModel(int offerId)
        {
            try
            {
                var model = new InvestorLoanContractModel();

                using (var db = new dbMoolahConnectEntities())
                {
                    var offer = db.tbl_Loanoffers.Where(x => x.OfferId == offerId).SingleOrDefault();

                    if (offer != null)
                    {
                        var repayment = db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == offer.LoanRequest_Id).FirstOrDefault();

                        model.AgreementDate = offer.tbl_LoanRequests.AcceptedDate.HasValue ? offer.tbl_LoanRequests.AcceptedDate.Value : DateTime.MinValue;
                        model.BorrowerName = offer.tbl_LoanRequests.tbl_Users.tbl_AccountDetails.First().BusinessName;
                        model.LenderIDNumber = GetIdentificationNumber(offer.tbl_Users.tbl_AccountDetails.First(), offer.tbl_Users.InvestorType);
                        model.ContractRef = offer.LoanRequest_Id.ToString();
                        model.ContractId = offer.ContractId;
                        model.BorrowerRegNumber = offer.tbl_LoanRequests.tbl_Users.tbl_AccountDetails.First().IC_CompanyRegistrationNumber;
                        model.Name = GetName(offer.tbl_Users);
                        model.PrincipalAmount = (decimal)offer.AcceptedAmount;
                        model.Tenure = offer.tbl_LoanRequests.Terms;
                        model.Rate = (decimal)offer.AcceptedRate;
                        model.LoanAmount = offer.tbl_LoanRequests.FinalAmount.HasValue ? offer.tbl_LoanRequests.FinalAmount.Value : 0;
                        model.LoanAcceptedRate = offer.tbl_LoanRequests.Rate.HasValue ? offer.tbl_LoanRequests.Rate.Value : 0;
                        model.TotalInterestReceivable = (decimal)CommonMethods.GetTotalInterestPayable(offer.tbl_LoanRequests.Terms,
                            offer.AcceptedAmount.HasValue ? (double)offer.AcceptedAmount.Value : 0,
                            offer.tbl_LoanRequests.Rate.HasValue ? (double)offer.tbl_LoanRequests.Rate.Value : 0);
                        model.UserId = offer.tbl_Users.AspnetUserId;
                        model.MonthlyRepaymentAmount =
                            (decimal)LoanRequestCalc.RepaymentAmount(
                            offer.AcceptedAmount.HasValue ? (double)offer.AcceptedAmount.Value : 0,
                            offer.AcceptedRate.HasValue ? (double)offer.AcceptedRate.Value : 0,
                            offer.tbl_LoanRequests.Terms);
                    }

                    var acceptedOffers = db.tbl_Loanoffers.Where(x => x.LoanRequest_Id == offer.LoanRequest_Id
                        && x.OfferStatus == (int)LoanOfferStatus.Accepted);

                    model.AcceptedOffersCount = acceptedOffers.Count();
                }

                return model;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new InvestorLoanContractModel();
            }
        }

        public bool AcceptPendingTransfer(long id, DateTime transferDate, decimal moolahFee, string adminUser)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var request = db.tbl_LoanRequests.SingleOrDefault(l => l.RequestId == id);

                var transactionDA = new TransactionsDA();

                if (request.FinalAmount.HasValue)
                    transactionDA.LoanTransactionDebit((int)request.User_ID.Value, request.FinalAmount.Value - moolahFee, id,
                        "Internet", "Note Amount Transferred to Bank Account", TransactionType.Info);

                request.MoolahFees = moolahFee;
                request.TransferDate = transferDate;
                request.LoanStatus = (int)LoanRequestStatus.FundApproved;

                IAuditLogDA audit = new AuditLogDA();
                const string auditMsg = AuditMessages.PENDING_TRANSFER_ACCEPTED_BY_ADMIN;
                audit.InsertAuditRecords(request, db, auditMsg, adminUser, request.RequestId, request.User_ID);

                db.SaveChanges();
                //}
            }
            return false;
        }

        public IEnumerable<UserTemp> RejectPendingTransfer(long id, string reson, string adminUser)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var request = db.tbl_LoanRequests.SingleOrDefault(l => l.RequestId == id);
                if (request != null)
                {
                    // Set loan status master fields
                    request.ResonReject = reson;
                    request.LoanStatus = (int)LoanRequestStatus.FundRejected;

                    var loanPayments = db.tbl_LoanPayments.Where(lp => lp.LoanRequestID == request.RequestId);

                    foreach (tbl_LoanPayments lp in loanPayments)
                    {
                        db.tbl_LoanPayments.Remove(lp);
                    }

                    // Remove repayment details
                    var amt = db.tbl_LoanAmortization.Where(ln => ln.LoanRequest_ID == request.RequestId);
                    foreach (var amortization in amt)
                    {
                        db.tbl_LoanAmortization.Remove(amortization);
                    }

                    // Update loan offer status
                    var offers = db.tbl_Loanoffers.Where(ln => ln.LoanRequest_Id == request.RequestId);
                    foreach (var offer in offers)
                    {
                        offer.OfferStatus = (int)LoanOfferStatus.RejectedByAdmin;
                    }

                    // Update borrower's account
                    var borwBal = db.tbl_Balances.SingleOrDefault(b => b.User_ID == request.User_ID);
                    if (borwBal == null)
                    {
                        borwBal = new tbl_Balances { ActualAmount = 0, LedgerAmount = 0, DateCreated = DateTime.UtcNow, User_ID = request.User_ID };
                        db.tbl_Balances.Add(borwBal);
                    }

                    var trnReject = new tbl_LoanTransactions
                    {
                        Amount = borwBal.ActualAmount,
                        DateCreated = DateTime.Now,
                        Request_Id = id,
                        PaymentTerms = @"Matched Note Cancelled by Admin",
                        User_ID = request.User_ID,
                        PaymentMode = "Internet",
                        Reference = UniqueReference.GetRandomUniqueNumber(),
                        Credited = 0,
                        Debted = request.FinalAmount,
                        Balance = borwBal.ActualAmount - request.FinalAmount
                    };

                    var trnMoFee = new tbl_LoanTransactions
                    {
                        Amount = trnReject.Balance,
                        DateCreated = DateTime.Now,
                        Request_Id = id,
                        PaymentTerms = @"Refund MoolahSense Fees",
                        User_ID = request.User_ID,
                        PaymentMode = "Internet",
                        Reference = UniqueReference.GetRandomUniqueNumber(),
                        Credited = request.MoolahFees,
                        Debted = 0,
                        Balance = trnReject.Balance + request.MoolahFees
                    };

                    var tranEarmarkAmount = new tbl_LoanTransactions
                    {
                        BalanceID = Settings.EarmarkAccountBalanceID,
                        DateCreated = DateTime.Now,
                        Request_Id = id,
                        PaymentTerms = @"Matched Note Cancelled by Admin",
                        RefUserID = request.User_ID,
                        PaymentMode = "Internet",
                        Reference = UniqueReference.GetRandomUniqueNumber(),
                        Credited = request.FinalAmount,
                        Balance = borwBal.ActualAmount - request.FinalAmount
                    };

                    // Update borrower's account balance
                    borwBal.ActualAmount = trnMoFee.Balance;
                    borwBal.LedgerAmount = borwBal.LedgerAmount - request.FinalAmount + request.MoolahFees;

                    db.tbl_LoanTransactions.Add(trnReject);
                    db.tbl_LoanTransactions.Add(trnMoFee);

                    // Update all relavent investors accounts
                    var dataMap = new Dictionary<long?, decimal?[]>();
                    var invList = new List<UserTemp>();
                    var trans = db.tbl_LoanTransactions.Where(p => p.Request_Id == id);
                    foreach (var tran in trans)
                    {
                        if (tran.User_ID == null || tran.User_ID == request.User_ID) continue;

                        var userId = (long)tran.User_ID;

                        if (dataMap.ContainsKey(userId))
                        {
                            var val = dataMap[userId];
                            val[0] += tran.Credited ?? 0;
                            val[1] += tran.Debted ?? 0;
                        }
                        else
                        {
                            var val = new decimal?[2];
                            val[0] = tran.Credited ?? 0;
                            val[1] = tran.Debted ?? 0;
                            dataMap.Add(tran.User_ID, val);

                            var userObj = db.tbl_Users.SingleOrDefault(u => u.UserID == tran.User_ID);
                            if (userObj != null) invList.Add(new UserTemp() { Type = 1, Username = userObj.UserName });
                        }
                    }

                    foreach (var tran in dataMap)
                    {
                        var val = tran.Value;

                        var bal = db.tbl_Balances.SingleOrDefault(b => b.User_ID == tran.Key);

                        if (bal == null)
                        {
                            bal = new tbl_Balances { ActualAmount = 0, LedgerAmount = 0, DateCreated = DateTime.Now, User_ID = request.User_ID };
                            db.tbl_Balances.Add(bal);
                        }

                        var trnRec = new tbl_LoanTransactions
                        {
                            Amount = bal.LedgerAmount,
                            DateCreated = DateTime.Now,
                            Request_Id = id,
                            PaymentTerms = @"Matched Note Cancelled by Admin",
                            User_ID = tran.Key,
                            PaymentMode = "Internet",
                            Reference = UniqueReference.GetRandomUniqueNumber()
                        };

                        var balance = (val[1] ?? 0) - (val[0] ?? 0);

                        if (balance > 0)
                        {
                            // Credit record
                            trnRec.Credited = balance;
                            trnRec.Debted = 0;
                            trnRec.Balance = trnRec.Amount + trnRec.Credited;
                            bal.ActualAmount = bal.ActualAmount + trnRec.Credited;
                            bal.LedgerAmount = bal.LedgerAmount + trnRec.Credited;
                            db.tbl_LoanTransactions.Add(trnRec);
                        }
                        else if (balance < 0)
                        {
                            // Debit record
                            trnRec.Debted = balance * -1;
                            trnRec.Credited = 0;
                            trnRec.Balance = trnRec.Amount - trnRec.Debted;
                            bal.ActualAmount = bal.ActualAmount - trnRec.Debted;
                            bal.LedgerAmount = bal.LedgerAmount - trnRec.Debted;
                            db.tbl_LoanTransactions.Add(trnRec);
                        }
                    }

                    // Create audit logs
                    IAuditLogDA audit = new AuditLogDA();
                    const string auditMsg = AuditMessages.PENDING_TRANSFER_REJECTED_BY_ADMIN;
                    audit.InsertAuditRecords(request, db, auditMsg, adminUser, request.RequestId, request.User_ID);

                    db.SaveChanges();

                    var borrower = db.tbl_Users.SingleOrDefault(u => u.UserID == request.User_ID);
                    if (borrower != null) invList.Add(new UserTemp() { Type = 0, Username = borrower.UserName });
                    return invList;
                }
            }

            return null;
        }
    }
}
