﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Implementation
{
    public class LoanPurposeDA : ILoanPurposeDA
    {
        public IEnumerable<tbl_LoanPurposesList> Get()
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_LoanPurposesList.ToList();
            }
        }
    }
}
