﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;

namespace MoolahConnect.Services.Implementation
{
    public class GlobalVariableDA: IGlobalVariableDA
    {
        public IEnumerable<tbl_GlobalVariables> Get()
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_GlobalVariables.ToList();
            }
        }
    }
}
