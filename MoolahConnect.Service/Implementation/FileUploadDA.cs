﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Services.Implementation
{
    public class FileUploadDA : IFileUploadDA
    {
        public bool InsertUploadedFile(string filename, Util.Enums.FileType fileType, Guid userId, long? requestId = null, int? offerId = null, bool multi = true)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var file = new tbl_Files();
                    tbl_Files existingFile = null;
                    var _fileType = fileType.ToString();

                    if (!multi)
                    {
                        existingFile = db.tbl_Files.Where(x =>
                           x.UserId == userId
                           && x.Type == _fileType
                           && x.RequestId == requestId).SingleOrDefault();
                    }

                    if (existingFile != null)
                    {
                        existingFile.FileName = filename;
                    }
                    else
                    {
                        file = new tbl_Files()
                        {
                            FileName = filename,
                            Type = _fileType,
                            UserId = userId,
                            CreatedDate = DateTime.UtcNow,
                            RequestId = requestId,
                            OfferId = offerId
                        };
                        db.tbl_Files.Add(file);
                    }

                    db.SaveChanges();
                }
                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public IEnumerable<tbl_Files> GetFilesFromUserIdAndTypeAndRequestId(Guid userid, Util.Enums.FileType type, long? requestId = null)
        {
            try
            {
                string _type = type.ToString();
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_Files.Where(x => x.UserId == userid
                        && x.Type == _type
                        && (requestId == null || x.RequestId == requestId)).ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_Files>();
            }
        }


        public bool DeleteFile(int fileId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var file = db.tbl_Files.Where(x => x.Id == fileId).SingleOrDefault();

                    if (file == null)
                        return false;

                    db.tbl_Files.Remove(file);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public void UpdateUploadedFileComments(List<string> IdsAndComments)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    foreach (string idAndComment in IdsAndComments)
                    {
                        var arr = idAndComment.Split(',');
                        int id = 0;
                        if (arr.Length > 1)
                        {
                            if (int.TryParse(arr[0], out id))
                            {
                                var file = db.tbl_Files.Where(x => x.Id == id).SingleOrDefault();

                                if (file != null)
                                {
                                    file.Comments = arr[1];
                                }
                            }
                        }
                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public tbl_Files GetCoverPhotoFile(Guid userId, long requestId)
        {
            try
            {
                string fileType = FileType.LoanCover.ToString();

                using (var db = new dbMoolahConnectEntities())
                {
                    var file = db.tbl_Files.Where(x =>
                        x.UserId == userId
                        && x.RequestId == requestId
                        && x.Type == fileType
                        ).SingleOrDefault();


                    return file;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public IEnumerable<tbl_Files> GetFilesByRequestId(long requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var files = db.tbl_Files.Where(x => x.RequestId == requestId).ToList();

                    return files;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_Files>();
            }
        }
    }
}
