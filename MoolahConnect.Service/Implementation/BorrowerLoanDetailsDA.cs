﻿using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;

namespace MoolahConnect.Services.Implementation
{
    public class BorrowerLoanDetailsDA : IBorrowerLoanDetailsDA
    {
        public IEnumerable<LoanEntity> GetLoansByUser(string username, List<LoanRequestStatus> statuses)
        {
            try
            {
                var integerStatuses = statuses.Select(x => (int)x).ToList();

                var finalContractType = FileType.FinalIssuerLoanContract.ToString();
                var guarantorContractType = FileType.FinalGuarantorContract.ToString();

                using (var db = new dbMoolahConnectEntities())
                {
                    var query = from lr in db.tbl_LoanRequests
                                join la in
                                    (from x in db.tbl_LoanAmortization where x.PayStatus == (int)RepaymentPayStatus.Paid select x)
                                on lr.RequestId equals la.LoanRequest_ID into x
                                from la in x.DefaultIfEmpty()
                                join u in db.tbl_Users on lr.User_ID equals u.UserID
                                join f in db.tbl_Files on lr.RequestId equals f.RequestId
                                where u.UserName != null && u.UserName.Trim().ToLower() == username.Trim().ToLower()
                                && lr.IsApproved.HasValue && lr.IsApproved.Value && integerStatuses.Any(y => y == lr.LoanStatus)
                                group new { LA = la, LR = lr, F = f } by lr.RequestId into grp
                                select new LoanEntity()
                                {
                                    RequestId = grp.Max(x => x.LR.RequestId),
                                    Amount = grp.DefaultIfEmpty().Max(x => x.LR.Amount),
                                    AcceptedDate = grp.DefaultIfEmpty().Max(x => x.LR.AcceptedDate),
                                    DateCreated = grp.DefaultIfEmpty().Max(x => x.LR.DateCreated),
                                    EIR = grp.DefaultIfEmpty().Max(x => x.LR.EIR),
                                    FinalAmount = grp.DefaultIfEmpty().Max(x => x.LR.FinalAmount),
                                    InterestPaidTillDate = grp.DefaultIfEmpty().Sum(x => x.LA.Interest),
                                    MoolahFees = grp.DefaultIfEmpty().Sum(x => x.LR.MoolahFees),

                                    IsApproved = grp.Any(x => x.LR.IsApproved == true),
                                    IsotherLoanType = grp.Any(x => x.LR.IsotherLoanType.HasValue && x.LR.IsotherLoanType == true),
                                    IsPersonalGuarantee = grp.Any(x => x.LR.IsPersonalGuarantee.HasValue && x.LR.IsPersonalGuarantee == true),

                                    LoanPurpose_Id = grp.DefaultIfEmpty().Max(x => x.LR.LoanPurpose_Id),
                                    LoanStatus = grp.Max(x => x.LR.LoanStatus),
                                    PaymentTerms = grp.Max(x => x.LR.PaymentTerms),
                                    PrincipalPaidTillDate = grp.DefaultIfEmpty().Sum(x => x.LA.EmiAmount) - grp.DefaultIfEmpty().Sum(x => x.LA.Interest),
                                    Question = grp.Max(x => x.LR.Question),
                                    Rate = grp.DefaultIfEmpty().Max(x => x.LR.Rate),
                                    Terms = grp.Max(x => x.LR.Terms),
                                    User_ID = grp.DefaultIfEmpty().Max(x => x.LR.User_ID),
                                    VideoDescription = grp.Max(x => x.LR.VideoDescription),
                                    FinalIssuerContractName = grp.FirstOrDefault(f => f.F.Type == finalContractType).F.FileName,
                                    FinalGuaranorContractName = grp.FirstOrDefault(f => f.F.Type == guarantorContractType).F.FileName
                                    
                                    

                                };
                    return query.ToList();
                }
            }
            catch (Exception ex)
            {

                return new List<LoanEntity>();
            }
        }

        public decimal? OutstandingLoanAmount(decimal requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var totCount =
                        db.tbl_LoanAmortization.Count(la => la.LoanRequest_ID == requestId);
                    var paidCount =
                        db.tbl_LoanAmortization.Count(la => la.LoanRequest_ID == requestId && la.PayStatus == (int)RepaymentPayStatus.Paid);

                    var request = db.tbl_LoanRequests.SingleOrDefault(l => l.RequestId == requestId);
                    if (request != null)
                    {
                        var loanAmt = request.FinalAmount ?? 0;
                        var rate = request.Rate ?? 0;

                        var intr = rate / 1200;
                        var x = Convert.ToDouble(1 / (1 + intr));
                        var y = Convert.ToDouble(totCount);
                        var z = Convert.ToDecimal(Math.Pow(x, y));
                        var installment = Convert.ToDecimal(loanAmt * intr / (1 - (z)));

                        var outstanding = (totCount * installment) - (paidCount * installment);
                        return outstanding;
                    }

                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<tbl_LoanAmortization> Get3MonthsOutstandingAnlAllLatePaymentsByUser(string username)
        {
            try
            {
                DateTime tempMinDate = DateTime.UtcNow.AddMonths(-1);
                DateTime tempMaxDate = DateTime.UtcNow.AddMonths(1);

                DateTime minDate = tempMinDate.AddDays(-1 * tempMinDate.Day);
                DateTime maxDate = tempMaxDate.AddMonths(1).AddDays(-tempMaxDate.Day);

                DateTime last7Day = DateTime.Now.AddDays(-7);

                using (var db = new dbMoolahConnectEntities())
                {                   
                    var query = from la in db.tbl_LoanAmortization
                                where
                                (from lr in db.tbl_LoanRequests
                                 where
                                 lr.tbl_Users != null
                                 && lr.tbl_Users.UserName != null
                                 && lr.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower()
                                 select lr.RequestId).Any(x => x == la.LoanRequest_ID)
                                && (la.PayStatus == (int)RepaymentPayStatus.Late
                                || (la.LateInterestPaidDate.HasValue && la.LateInterestPaidDate >= last7Day)
                                || (la.EmiDate.HasValue
                                && la.EmiDate >= minDate
                                && la.EmiDate.Value <= maxDate))
                                select la;

                    return query.ToList();
                }
            }
            catch (Exception ex)
            {

                return new List<tbl_LoanAmortization>();
            }
        }

        public decimal? GetMaxLoanAmount(string username)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var query = from lr in db.tbl_LoanRequests
                            join u in db.tbl_Users on lr.User_ID equals u.UserID
                            where u.UserName != null && u.UserName.Trim().ToLower() == username.Trim().ToLower()
                                  && (lr.LoanStatus == (int)LoanRequestStatus.Matched
                                  || lr.LoanStatus == (int)LoanRequestStatus.Matured
                                  || lr.LoanStatus == (int)LoanRequestStatus.FundApproved)
                            select lr;
                return query.Max(e => e.FinalAmount) ?? 0;
            }
        }

        public decimal? GetMaxLoanInvestmentAmount(string username)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var query = from lr in db.tbl_LoanRequests
                            join u in db.tbl_Users on lr.User_ID equals u.UserID
                            join lo in db.tbl_Loanoffers on lr.RequestId equals lo.LoanRequest_Id
                            where u.UserName != null && u.UserName.Trim().ToLower() == username.Trim().ToLower()
                                  && (lr.LoanStatus == (int)LoanRequestStatus.Matched
                                      || lr.LoanStatus == (int)LoanRequestStatus.Matured
                                      || lr.LoanStatus == (int)LoanRequestStatus.FundApproved)
                                  && lo.OfferStatus == (int)LoanOfferStatus.Accepted
                            select lo;

                return query.Max(e => e.AcceptedAmount) ?? 0;
            }
        }

        public decimal? GetTotalMatchedAndMaturedLoan(string username)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var query = from lr in db.tbl_LoanRequests
                            join u in db.tbl_Users on lr.User_ID equals u.UserID
                            where u.UserName != null && u.UserName.Trim().ToLower() == username.Trim().ToLower()
                                  && (lr.LoanStatus == (int)LoanRequestStatus.Matched
                                  || lr.LoanStatus == (int)LoanRequestStatus.Matured
                                  || lr.LoanStatus == (int)LoanRequestStatus.FundApproved)
                            select lr;
                return query.Sum(e => e.FinalAmount) ?? 0;
            }
        }


        public int GetTotalUniqueInvestor(string username)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var query = from lr in db.tbl_LoanRequests
                            join u in db.tbl_Users on lr.User_ID equals u.UserID
                            join lo in db.tbl_Loanoffers on lr.RequestId equals lo.LoanRequest_Id
                            where u.UserName != null && u.UserName.Trim().ToLower() == username.Trim().ToLower()
                                  && (lr.LoanStatus == (int)LoanRequestStatus.Matched
                                      || lr.LoanStatus == (int)LoanRequestStatus.Matured
                                      || lr.LoanStatus == (int)LoanRequestStatus.FundApproved)
                                  && lo.OfferStatus == (int)LoanOfferStatus.Accepted
                            group new { id = lo.Investor_Id } by lo.Investor_Id
                                into g
                                select g.Key;
                return query.Count();
            }
        }


        public tbl_LoanRequests GetLoanDraftByUser(string username)
        {
            var loanRequest = new tbl_LoanRequests();

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequests = db.tbl_LoanRequests.Include("tbl_LoanPurposesList")
                        .Include("tbl_Users.tbl_AccountDetails").Where(x => x.tbl_Users != null
                        && x.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower()
                        && x.PreliminaryLoanStatus == (int)PreliminaryLoanRequestStatus.DraftCreated)
                        .OrderByDescending(x => x.DateCreated);

                    loanRequest = loanRequests
                        .FirstOrDefault();

                    foreach (tbl_LoanRequests request in loanRequests)
                    {
                        if (request.RequestId != loanRequest.RequestId)
                            DeleteLoan(request.RequestId);
                    }

                    return loanRequest;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public bool RemoveLoanRequestFromId(long requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var moolaPeri = db.tbl_MoolahPeri.Where(x => x.Request_ID == requestId);
                    if (moolaPeri.Count() > 0)
                    {
                        db.tbl_MoolahPeri.RemoveRange(moolaPeri);
                        db.SaveChanges();
                    }

                    var moolaPeriVerifications = db.tbl_MoolahPeriVerification.Where(x => x.Request_ID == requestId);
                    if (moolaPeriVerifications.Count() > 0)
                    {
                        db.tbl_MoolahPeriVerification.RemoveRange(moolaPeriVerifications);
                        db.SaveChanges();
                    }

                    var moolaCore = db.tbl_MoolahCore.Where(x => x.LoanRequest_Id == requestId);
                    if (moolaCore.Count() > 0)
                    {
                        db.tbl_MoolahCore.RemoveRange(moolaCore);
                        db.SaveChanges();
                    }

                    var moolaCoreVerifications = db.tbl_MoolahCoreVerification.Where(x => x.Request_ID == requestId);
                    if (moolaCoreVerifications.Count() > 0)
                    {
                        db.tbl_MoolahCoreVerification.RemoveRange(moolaCoreVerifications);
                        db.SaveChanges();
                    }

                    var personalGurantee = db.tbl_PersonalGuaranteeInfo.Where(x => x.Request_Id == requestId);
                    if (personalGurantee.Count() > 0)
                    {
                        db.tbl_PersonalGuaranteeInfo.RemoveRange(personalGurantee);
                        db.SaveChanges();
                    }

                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == requestId);
                    if (loanRequest.Count() > 0)
                    {
                        db.tbl_LoanRequests.RemoveRange(loanRequest);
                        db.SaveChanges();
                        return true;
                    }
                    return false;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool PublishLoan(long requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == requestId).SingleOrDefault();

                    if (loanRequest == null)
                        return false;

                    loanRequest.PreliminaryLoanStatus = (int)PreliminaryLoanRequestStatus.DraftPublished;
                    loanRequest.LoanStatus = (int)LoanRequestStatus.InProgress;
                    loanRequest.PublishedDate = DateTime.UtcNow;
                    db.SaveChanges();
                    MoolahLogManager.Log(AuditMessages.PUBLISH_LOAN_LISTING, LogType.Info,
                        loanRequest.tbl_Users != null ? loanRequest.tbl_Users.UserName : string.Empty, loanRequest.RequestId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool DeleteLoan(long requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRequest = db.tbl_LoanRequests.Where(x => x.RequestId == requestId).SingleOrDefault();

                    if (loanRequest == null)
                        return false;

                    loanRequest.PreliminaryLoanStatus = (int)PreliminaryLoanRequestStatus.DraftDeleted;
                    db.SaveChanges();
                    MoolahLogManager.Log(AuditMessages.DELETE_LOAN_LISTING, LogType.Info,
                        loanRequest.tbl_Users != null ? loanRequest.tbl_Users.UserName : string.Empty, loanRequest.RequestId);
                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public long InsertInitialLoanRequest(decimal amount, string terms, decimal rate, string username)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    IEnumerable<tbl_loanRequestReference> latestLoanReference = db.Database.SqlQuery<tbl_loanRequestReference>("SP_GetLatestLoanReference")
                        .AsEnumerable();

                    var user = db.tbl_Users.FirstOrDefault(p => p.UserName.Trim().ToLower() == username.Trim().ToLower());

                    long userid = 0;
                    if (user != null)
                        userid = user.UserID;

                    var newLoanRequest = new tbl_LoanRequests()
                    {
                        RequestId = latestLoanReference.Max(c => c.RequestId),
                        Amount = amount,
                        Rate = rate,
                        Terms = terms,
                        LoanStatus = (int)LoanRequestStatus.Pending,
                        LoanPurpose_Id = 6,
                        LoanPurposeDescription = string.Empty,
                        User_ID = userid,
                        DateCreated = DateTime.Now
                    };
                    db.tbl_LoanRequests.Add(newLoanRequest);
                    db.SaveChanges();
                    return newLoanRequest.RequestId;
                }


            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return 0;
            }
        }


        public bool AddYouTubeLink(string link, string username, long requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.FirstOrDefault(p => p.UserName.Trim().ToLower() == username.Trim().ToLower());

                    Guid userid = Guid.Empty;
                    if (user != null)
                        userid = user.AspnetUserId;

                    tbl_Files youTubeLink = new tbl_Files()
                    {
                        FileName = link,
                        CreatedDate = DateTime.Now,
                        Type = FileType.YouTubeLink.ToString(),
                        RequestId = requestId,
                        UserId = userid
                    };

                    db.tbl_Files.Add(youTubeLink);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}
