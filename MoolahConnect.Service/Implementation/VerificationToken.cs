﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Logging;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util;

namespace MoolahConnect.Services.Implementation
{
    public class VerificationToken : IVerificationToken
    {

        public bool IsValidToken(string token, bool isExpire)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    Guid tokenGuid = Guid.Parse(token);
                    var verToken = db.u_VerificationToken.SingleOrDefault(u => u.Token == tokenGuid);

                    if (verToken == null)
                        return false;

                    if (isExpire && verToken.CreatedDate.Subtract(DateTime.UtcNow).Minutes >= Settings.VerificationRequestExpireTime)
                        return false;

                    db.u_VerificationToken.Remove(verToken);
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool Save(Guid userId, Guid token)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var verToken = new u_VerificationToken();
                    verToken.CreatedDate = DateTime.Now.ToUniversalTime();
                    verToken.Token = token;
                    verToken.UserId = userId;

                    db.u_VerificationToken.Add(verToken);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}
