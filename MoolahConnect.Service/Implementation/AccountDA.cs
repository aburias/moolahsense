﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Implementation
{
    public class AccountDA: IAccountDA
    {
        public IEnumerable<tbl_AccountDetails> Get()
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_AccountDetails.ToList();
            }
        }

        public IEnumerable<tbl_AccountDetails> Get(List<long> userIds)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_AccountDetails.Where(e => e.User_ID != null && userIds.Contains((long) e.User_ID)).ToList();
            }
        }

        public tbl_Users GetUser(string name)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_Users.FirstOrDefault(e => e.UserName.Equals(name));
            }
        }
    }
}
