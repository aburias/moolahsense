﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.Security;
using MoolahConnect.Util;
using System.Data.SqlClient;

namespace MoolahConnect.Services.Implementation
{
    public class User : IUser
    {
        public string GetUserRoleByUsername(string username)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(u => u.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();

                    if (user == null)
                        return string.Empty;

                    return user.UserRole;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return string.Empty;
            }
        }


        public bool Remove(string username)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();

                if (user == null)
                    return false;

                db.tbl_Users.Remove(user);
                db.SaveChanges();
                return true;
            }
        }


        public int GetFailedPasswordAttemptsCount(Guid userId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.aspnet_Membership.Where(u => u.UserId == userId).SingleOrDefault();

                    if (user == null)
                        return 0;

                    return user.FailedPasswordAttemptCount;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return 0;
            }
        }


        public bool LockUser(Guid userId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.aspnet_Membership.Where(u => u.UserId == userId).SingleOrDefault();

                    if (user == null)
                        return false;

                    user.IsLockedOut = true;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool IsEmailVerified(string username)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(u => u.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();

                    if (user == null)
                        return false;

                    return user.IsEmailVerified != null ? user.IsEmailVerified.Value : false;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool UpdateEmailVerification(string username, bool verified)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(u => u.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();
                    user.IsEmailVerified = verified;
                    db.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public IEnumerable<string> GetAllAdminUsers()
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var users = db.tbl_Users.Where(u => u.UserRole == "Admin");
                var userList = users.Select(user => user.UserName).ToList();
                return userList;
            }
        }

        public bool ValidateAdminUser(string username, string password)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var user = db.tbl_Users.Where(u => u.UserName.Trim().ToLower() == username.Trim().ToLower() && u.Password == password);
                return user.Any();
            }
        }


        public void SaveDigitalSignature(long userID)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.SingleOrDefault(u => u.UserID == userID);
                    var userAccount = db.tbl_AccountDetails.SingleOrDefault(u => u.User_ID == userID);

                    if (user != null && userAccount != null)
                    {
                        user.DigitalSignature = SecurityProvider.GenerateDigitalSignature(
                            new string[3] { user.UserName, userAccount.NRIC_Number, user.DateCreated.Value.ToString("ddMMyyyyhhmmss") }
                            );
                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public tbl_Users GetUserFromAspNetId(Guid id)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_Users.Where(u => u.AspnetUserId == id).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public IEnumerable<tbl_Users> GetApprovedInvestors()
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_Users.Where(u => u.AdminVerification == "Approved" && u.UserRole == "Investor").ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_Users>();
            }
        }


        public tbl_Users GetUserFromId(long id)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_Users.Where(u => u.UserID == id).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public tbl_AccountDetails GetAccountDetailsByID(long id)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_AccountDetails.Where(u => u.User_ID == id).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public UserAccountEntity GetUserAccountFromUserID(long userID)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return (from u in db.tbl_Users
                            join a in db.tbl_AccountDetails on u.UserID equals a.User_ID
                            where u.UserID == userID
                            select new UserAccountEntity()
                            {
                                UserID = u.UserID,
                                UserRole = u.UserRole,
                                Title = a.Title,
                                FirstName = a.FirstName,
                                LastName = a.LastName,
                                NRIC = a.NRIC_Number,
                                BusinessRegNumber = a.IC_CompanyRegistrationNumber
                            }).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public IEnumerable<InvestorKnowledgeAssesmentDetails> GetInvestorKnowledgeAssesmentDetails(long requestID)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var details = new List<InvestorKnowledgeAssesmentDetails>();
                    details = db.Database.SqlQuery<InvestorKnowledgeAssesmentDetails>
                        ("SP_GetInvestorKnowledgeAssesmentDetails @requestID ", new SqlParameter("requestID", requestID))
                        .ToList<InvestorKnowledgeAssesmentDetails>();

                    return details;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<InvestorKnowledgeAssesmentDetails>();
            }
        }
    }
}
