﻿using System.Data.SqlClient;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util;
using MoolahConnect.Util.FinancialCalculations;

namespace MoolahConnect.Services.Implementation
{
    public class LoanDetailsDA : ILoanDetailsDA
    {
        private ITransactionsDA _ITransactionDA;

        public LoanDetailsDA()
        {
            _ITransactionDA = new TransactionsDA();
        }

        public tbl_LoanRequests GetLoanRequestFromId(long requestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_LoanRequests.Include("tbl_LoanPurposesList").Include("tbl_Users.tbl_AccountDetails")
                        .Include("tbl_LoanAmortization")
                        .Where(x => x.RequestId == requestId).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public tbl_AccountDetails GetAccountDetailsFromUserId(int userId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_AccountDetails.Include("tbl_NatureofBusinessList").Where(x => x.User_ID == userId).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }


        public string GetMoolahPerkFromId(int id)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var query = db.tbl_MoolahPerksForLenders.Where(x => x.MoolahPerkId == id).SingleOrDefault();

                    return query != null ? query.MoolahPerk : null;
                }
            }
            catch (Exception ex)
            {

                return null;
            }
        }

        public IEnumerable<InvestorDTO> GetInvestorList(bool hasBalance)
        {
            try
            {

                using (var db = new dbMoolahConnectEntities())
                {
                    var res = (from users in db.tbl_Users
                               join b in db.tbl_Balances on users.UserID equals b.User_ID
                               into y
                               from b in y.DefaultIfEmpty()
                               join accountDetails in db.tbl_AccountDetails on users.UserID equals accountDetails.User_ID
                               into x
                               from accountDetails in x.DefaultIfEmpty()
                               where users.UserRole == "Investor"
                               where (!hasBalance || b.LedgerAmount != null)
                               orderby users.DateCreated descending
                               select new InvestorDTO()
                               {
                                   UserID = users.UserID,
                                   UserName = users.UserName,
                                   UserRole = users.UserRole,
                                   IsActive = users.Isactive,
                                   DateCreated = users.DateCreated,
                                   InvestorType = users.InvestorType,
                                   AdminVerification = users.AdminVerification,
                                   IsSubmitted = users.IsSubmitted,
                                   IsEmailVerified = users.IsEmailVerified,
                                   FirstName = accountDetails.FirstName,
                                   LastName = accountDetails.LastName,
                                   DisplayName = accountDetails.DisplayName,
                                   AccountType = accountDetails.AccountType,
                                   Nationality = accountDetails.Nationality,
                                   DateofBirth = accountDetails.DateofBirth,
                                   Gender = accountDetails.Gender,
                                   MainOfficeContactnumber = accountDetails.Main_OfficeContactnumber,
                                   CurrentBalance = b.LedgerAmount != null ? b.LedgerAmount.Value : 0

                               }).ToList();
                    return res;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }
        public IEnumerable<tbl_LoanTransactions> GetAllLoanTransactionsByDateRange(DateTime? _from, DateTime? _to)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var result =
                        db.tbl_LoanTransactions
                        .Include("tbl_Users")
                        .Include("tbl_Users.tbl_AccountDetails")
                        .ToList()
                        .Where(l =>
                                l.PaymentTerms != AuditMessages.DEPOSIT_FUND
                                && (l.Type == null || l.Type.Value != 0)
                                && (_from == null || (l.DateCreated.HasValue && l.DateCreated.Value.Date >= _from.Value.ToUniversalTime()))
                                && (_to == null || (l.DateCreated.HasValue && l.DateCreated.Value.Date <= _to.Value.ToUniversalTime()))
                        )
                        .OrderByDescending(d => d.DateCreated);
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_LoanTransactions>();
            }
        }

        public IEnumerable<tbl_LoanTransactions> GetUserLoanTransactionsByDateRange(DateTime? _from, DateTime? _to)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var result =
                        db.tbl_LoanTransactions
                        .Include("tbl_Users")
                        .Include("tbl_Users.tbl_AccountDetails")
                        .ToList()
                        .Where(l =>
                                l.RefUserID == null
                                && (_from == null || (l.DateCreated.HasValue && l.DateCreated.Value.Date >= _from.Value.ToUniversalTime()))
                                && (_to == null || (l.DateCreated.HasValue && l.DateCreated.Value.Date <= _to.Value.ToUniversalTime()))
                        )
                        .OrderByDescending(d => d.DateCreated);
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_LoanTransactions>();
            }
        }


        public IEnumerable<LoanEntity> GetLoans(DateTime? _from, DateTime? _to, Util.Enums.LoanRequestStatus loanRequestStatus)
        {
            var loans = new List<LoanEntity>();
            int intloanRequestStatus = (int)loanRequestStatus;

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var query = from lr in db.tbl_LoanRequests
                                join la in
                                    (from x in db.tbl_LoanAmortization where x.PayStatus == (int)RepaymentPayStatus.Paid select x)
                                on lr.RequestId equals la.LoanRequest_ID into x
                                from la in x.DefaultIfEmpty()
                                join u in db.tbl_Users on lr.User_ID equals u.UserID
                                join a in db.tbl_AccountDetails on lr.User_ID equals a.User_ID
                                where lr.IsApproved.HasValue && lr.IsApproved.Value && intloanRequestStatus == lr.LoanStatus
                                && (_from == null || lr.AcceptedDate >= _from)
                                && (_to == null || lr.AcceptedDate <= _to)
                                orderby lr.AcceptedDate
                                group new { LA = la, LR = lr, A = a } by lr.RequestId into grp
                                select new LoanEntity()
                                {
                                    RequestId = grp.Max(x => x.LR.RequestId),
                                    Amount = grp.DefaultIfEmpty().Max(x => x.LR.Amount),
                                    AcceptedDate = grp.DefaultIfEmpty().Max(x => x.LR.AcceptedDate),
                                    DateCreated = grp.DefaultIfEmpty().Max(x => x.LR.DateCreated),
                                    EIR = grp.DefaultIfEmpty().Max(x => x.LR.EIR),
                                    FinalAmount = grp.DefaultIfEmpty().Max(x => x.LR.FinalAmount),
                                    InterestPaidTillDate = grp.DefaultIfEmpty().Sum(x => x.LA.Interest),
                                    MoolahFees = grp.DefaultIfEmpty().Sum(x => x.LR.MoolahFees),

                                    IsApproved = grp.Any(x => x.LR.IsApproved == true),
                                    IsotherLoanType = grp.Any(x => x.LR.IsotherLoanType.HasValue && x.LR.IsotherLoanType == true),
                                    IsPersonalGuarantee = grp.Any(x => x.LR.IsPersonalGuarantee.HasValue && x.LR.IsPersonalGuarantee == true),

                                    LoanPurpose_Id = grp.DefaultIfEmpty().Max(x => x.LR.LoanPurpose_Id),
                                    LoanStatus = grp.Max(x => x.LR.LoanStatus),
                                    PaymentTerms = grp.Max(x => x.LR.PaymentTerms),
                                    PrincipalPaidTillDate = grp.DefaultIfEmpty().Sum(x => x.LA.EmiAmount) - grp.DefaultIfEmpty().Sum(x => x.LA.Interest),
                                    AmountPaidTillDate = grp.DefaultIfEmpty().Sum(x => x.LA.EmiAmount),
                                    Question = grp.Max(x => x.LR.Question),
                                    Rate = grp.DefaultIfEmpty().Max(x => x.LR.Rate),
                                    Terms = grp.Max(x => x.LR.Terms),
                                    User_ID = grp.DefaultIfEmpty().Max(x => x.LR.User_ID),
                                    VideoDescription = grp.Max(x => x.LR.VideoDescription),

                                    AccountNumber = grp.Max(x => x.A.User_ID.HasValue ? x.A.User_ID.Value : 0),
                                    CompanyName = grp.Max(x => x.A.BusinessName),
                                    MonthsTillMaturity = grp.Where(y => y.LA.PayStatus != (int)RepaymentPayStatus.Paid).Count()

                                };

                    loans = query.ToList();
                }

                return loans;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanEntity>();
            }
        }


        public IEnumerable<LoanRepaymentEntity> GetRepayments(DateTime? _from, DateTime? _to)
        {
            var repayments = new List<LoanRepaymentEntity>();

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var query = from r in db.tbl_LoanAmortization
                                join lr in db.tbl_LoanRequests on r.LoanRequest_ID equals lr.RequestId
                                into lrs
                                from x in lrs
                                join a in db.tbl_AccountDetails on x.User_ID equals a.User_ID
                                where ((_from == null || r.EmiDate >= _from) && x.LoanStatus == (int)LoanRequestStatus.FundApproved)
                                && (_to == null || r.EmiDate <= _to)
                                select new LoanRepaymentEntity()
                                {
                                    RequestId = x.RequestId,
                                    AccountNumber = a.tbl_Users.UserID,
                                    CompanyName = a.BusinessName,
                                    AcceptedDate = x.AcceptedDate,
                                    Amount = x.Amount,
                                    FinalAmount = x.FinalAmount,
                                    Rate = x.Rate,
                                    Repayment = r.EmiAmount,
                                    Status = (RepaymentPayStatus)r.PayStatus,
                                    RepaymentDate = r.EmiDate,
                                    Terms = x.Terms,
                                    RepaymentId = r.AmortizationID,
                                    MonthsTillMaturity = r.tbl_LoanRequests
                                    .tbl_LoanAmortization.Where(y => y.PayStatus != (int)RepaymentPayStatus.Paid).Count(),
                                    PaidAmount = r.PaidAmount,
                                    LateFees = r.LateFees,
                                    LateRepaymentPayStatus = r.LateInterestPayStatus.HasValue ? r.LateInterestPayStatus.Value : 0,
                                    LateRepaymentPaidDate = r.LateInterestPaidDate,
                                    LateInterest = r.LateInterest

                                };

                    repayments = query.ToList();
                }

                return repayments;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanRepaymentEntity>();
            }
        }

        public IEnumerable<LoanRepaymentEntity> GetRepaymentsByStats(DateTime? _from, DateTime? _to, RepaymentPayStatus status)
        {
            var repayments = new List<LoanRepaymentEntity>();

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var query = from r in db.tbl_LoanAmortization
                                join lr in db.tbl_LoanRequests on r.LoanRequest_ID equals lr.RequestId
                                into lrs
                                from x in lrs
                                join a in db.tbl_AccountDetails on x.User_ID equals a.User_ID
                                where
                                r.PayStatus == (int)status
                                && (_from == null || r.EmiDate >= _from)
                                && (_to == null || r.EmiDate <= _to)
                                select new LoanRepaymentEntity()
                                {
                                    RequestId = x.RequestId,
                                    AccountNumber = a.tbl_Users.UserID,
                                    CompanyName = a.BusinessName,
                                    AcceptedDate = x.AcceptedDate,
                                    Amount = x.Amount,
                                    Rate = x.Rate,
                                    Repayment = r.EmiAmount,
                                    Status = (RepaymentPayStatus)r.PayStatus,
                                    RepaymentDate = r.EmiDate,
                                    Terms = x.Terms,
                                    RepaymentId = r.AmortizationID,
                                    MonthsTillMaturity = r.tbl_LoanRequests
                                    .tbl_LoanAmortization.Where(y => y.PayStatus != (int)RepaymentPayStatus.Paid).Count()
                                };

                    repayments = query.ToList();
                }

                return repayments;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanRepaymentEntity>();
            }
        }

        public IEnumerable<LoanRepaymentEntity> GetLateRepayments(DateTime? _from, DateTime? _to)
        {
            var repayments = new List<LoanRepaymentEntity>();

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var query = from r in db.tbl_LoanAmortization
                                join lr in db.tbl_LoanRequests on r.LoanRequest_ID equals lr.RequestId
                                into lrs
                                from x in lrs
                                join a in db.tbl_AccountDetails on x.User_ID equals a.User_ID
                                where
                                r.LateStartDate.HasValue
                                && (_from == null || r.EmiDate >= _from)
                                && (_to == null || r.EmiDate <= _to)
                                select new LoanRepaymentEntity()
                                {
                                    RequestId = x.RequestId,
                                    AccountNumber = a.tbl_Users.UserID,
                                    CompanyName = a.BusinessName,
                                    AcceptedDate = x.AcceptedDate,
                                    Amount = x.Amount,
                                    FinalAmount = x.FinalAmount,
                                    Rate = x.Rate,
                                    Repayment = r.EmiAmount,
                                    Status = (RepaymentPayStatus)r.PayStatus,
                                    RepaymentDate = r.EmiDate,
                                    Terms = x.Terms,
                                    RepaymentId = r.AmortizationID,
                                    LateRepaymentStartDate = r.LateStartDate,
                                    LateRepaymentPayStatus = r.LateInterestPayStatus.HasValue ? r.LateInterestPayStatus.Value : 0,
                                    LateRepaymentPaidDate = r.LateInterestPaidDate,
                                    LateInterest = r.LateInterest,
                                    LateFees = r.LateFees,
                                    MonthsTillMaturity = r.tbl_LoanRequests
                                    .tbl_LoanAmortization.Where(y => y.PayStatus != (int)RepaymentPayStatus.Paid).Count()
                                };

                    repayments = query.ToList();
                }

                return repayments;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanRepaymentEntity>();
            }
        }


        public IEnumerable<LoanInvestorEntity> GetLoanInvestors(long requestId, long repId)
        {
            var loanInvestors = new List<LoanInvestorEntity>();

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var repaymentAmount = db.tbl_LoanAmortization.Where(x => x.AmortizationID == repId).SingleOrDefault().EmiAmount;

                    var query = from lo in db.tbl_Loanoffers
                                where
                                lo.LoanRequest_Id == requestId
                                && lo.OfferStatus == (int)LoanOfferStatus.Accepted
                                select new LoanInvestorEntity()
                                {
                                    Amount = lo.AcceptedAmount,
                                    DisplayName = lo.tbl_Users != null && lo.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null ? lo.tbl_Users.tbl_AccountDetails.FirstOrDefault().DisplayName : string.Empty,
                                    Title = lo.tbl_Users != null && lo.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null ? lo.tbl_Users.tbl_AccountDetails.FirstOrDefault().Title : string.Empty,
                                    FirstName = lo.tbl_Users != null && lo.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null ? lo.tbl_Users.tbl_AccountDetails.FirstOrDefault().FirstName : string.Empty,
                                    LastName = lo.tbl_Users != null && lo.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null ? lo.tbl_Users.tbl_AccountDetails.FirstOrDefault().LastName : string.Empty,
                                    
                                    LoanId = requestId,
                                    LoanAmount = lo.tbl_LoanRequests != null ? lo.tbl_LoanRequests.FinalAmount : 0,
                                    RepaymentAmount = repaymentAmount,
                                    RepaymentId = repId,
                                    UserId = lo.tbl_Users != null ? lo.tbl_Users.UserID : 0,
                                    Tenor = lo.tbl_LoanRequests != null ? lo.tbl_LoanRequests.Terms : string.Empty,
                                    Rate = lo.tbl_LoanRequests != null ? lo.tbl_LoanRequests.Rate : 0
                                };

                    /*var query = from i in db.tbl_Loanoffers
                                where i.LoanRequest_Id == requestId && i.OfferStatus == (int)LoanOfferStatus.Accepted
                                join a in db.tbl_AccountDetails on i.tbl_Users.UserID equals a.User_ID
                                group new { LO = i, A = a } by a.User_ID into grp
                                select new LoanInvestorEntity()
                                {
                                    Amount = grp.Sum(x => x.LO.AcceptedAmount),
                                    DisplayName = grp.Max(x => x.A.DisplayName),
                                    LoanId = requestId,
                                    LoanAmount = grp.Max(x => x.LO.tbl_LoanRequests.Amount),
                                    RepaymentAmount = repaymentAmount,
                                    RepaymentId = repId,
                                    UserId = grp.Max(x => x.A.User_ID.HasValue ? x.A.User_ID.Value : 0),
                                    Tenor = grp.Max(x => x.LO.tbl_LoanRequests.Terms),
                                    Rate = grp.Max(x => x.LO.tbl_LoanRequests.Rate)
                                };
                    */
                    loanInvestors = query.ToList();
                }

                return loanInvestors;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanInvestorEntity>();
            }
        }

        public IEnumerable<LoanInvestorEntity> GetLoanInvestorsForDepLatePayments(long requestId, double latePayment)
        {
            var loanInvestors = new List<LoanInvestorEntity>();

            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var query = from lo in db.tbl_Loanoffers
                                where
                                lo.LoanRequest_Id == requestId
                                && lo.OfferStatus == (int)LoanOfferStatus.Accepted
                                select new LoanInvestorEntity()
                                {
                                    OfferId = lo.OfferId,
                                    Amount = lo.AcceptedAmount,
                                    DisplayName = lo.tbl_Users != null && lo.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null ? lo.tbl_Users.tbl_AccountDetails.FirstOrDefault().DisplayName : string.Empty,
                                    LoanId = requestId,
                                    LoanAmount = lo.tbl_LoanRequests != null ? lo.tbl_LoanRequests.FinalAmount : 0,
                                    LatePayment = latePayment * (double)lo.AcceptedAmount.Value / (double)lo.tbl_LoanRequests.FinalAmount.Value,
                                    UserId = lo.tbl_Users != null ? lo.tbl_Users.UserID : 0,
                                    Username = lo.tbl_Users != null ? lo.tbl_Users.UserName : string.Empty,
                                    Tenor = lo.tbl_LoanRequests != null ? lo.tbl_LoanRequests.Terms : string.Empty,
                                    Rate = lo.tbl_LoanRequests != null ? lo.tbl_LoanRequests.Rate : 0
                                };

                    /*var query = from i in db.tbl_Loanoffers
                                where i.LoanRequest_Id == requestId && i.OfferStatus == (int)LoanOfferStatus.Accepted
                                join a in db.tbl_AccountDetails on i.tbl_Users.UserID equals a.User_ID
                                group new { LO = i, A = a } by a.User_ID into grp
                                select new LoanInvestorEntity()
                                {
                                    OfferId = grp.Max(x => x.LO.OfferId),
                                    Amount = grp.Sum(x => x.LO.AcceptedAmount),
                                    DisplayName = grp.Max(x => x.A.DisplayName),
                                    LoanId = requestId,
                                    LoanAmount = grp.Max(x => x.LO.tbl_LoanRequests.Amount),
                                    RepaymentAmount = (decimal)latePayment,
                                    UserId = grp.Max(x => x.A.User_ID.HasValue ? x.A.User_ID.Value : 0),
                                    Tenor = grp.Max(x => x.LO.tbl_LoanRequests.Terms),
                                    Rate = grp.Max(x => x.LO.tbl_LoanRequests.Rate),
                                };
                    */
                    loanInvestors = query.ToList();
                }

                return loanInvestors;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanInvestorEntity>();
            }
        }


        public int MonthsTillMaturity(long loanRequest)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {

                    return db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == loanRequest && x.PayStatus != 1).Count();

                }

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return 0;
            }
        }


        public tbl_LoanAmortization GetRepaymentByID(long repaymentID)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_LoanAmortization.
                        Where(x => x.AmortizationID == repaymentID).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public IEnumerable<RepaymentEntity> GetRepaymentsByRequestID(long requestID)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var repayments = new List<RepaymentEntity>();
                    var tempRepayments = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestID).ToList();


                    for (int i = 0; i < tempRepayments.Count; i++)
                    {
                        repayments.Add(new RepaymentEntity()
                        {
                            DueDate = tempRepayments[i].EmiDate.HasValue ? tempRepayments[i].EmiDate.Value : DateTime.MinValue,
                            EndBalance = tempRepayments[i].Balance.HasValue ? (double)Math.Round(tempRepayments[i].Balance.Value, 2) : 0,
                            Interest = tempRepayments[i].Interest.HasValue ? (double)Math.Round(tempRepayments[i].Interest.Value, 2) : 0,
                            Payment = tempRepayments[i].EmiAmount.HasValue ? (double)Math.Round(tempRepayments[i].EmiAmount.Value, 2) : 0,
                            Principal = tempRepayments[i].Principal.HasValue ? (double)Math.Round(tempRepayments[i].Principal.Value, 2) : 0,
                            StartBalance = i != 0 && tempRepayments[i - 1].Balance.HasValue ? (double)Math.Round(tempRepayments[i - 1].Balance.Value, 2) :
                             (double)tempRepayments[i].tbl_LoanRequests.FinalAmount.Value,
                            Status = (tempRepayments[i].PayStatus == (int)RepaymentPayStatus.Late && tempRepayments[i].PaidAmount.HasValue && tempRepayments[i].PaidAmount.Value > 0)
                            ? (int)RepaymentPayStatus.PartiallyPaid : tempRepayments[i].PayStatus.Value
                        });
                    }

                    return repayments;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<RepaymentEntity>();
            }
        }


        public tbl_LoanRequests ExpireLoanRequests(long requestID)
        {
            try
            {

                using (var db = new dbMoolahConnectEntities())
                {
                    var request = db.tbl_LoanRequests.SingleOrDefault(t => t.RequestId == requestID);

                    if (request != null)
                    {
                        request.PreliminaryLoanStatus = (int)PreliminaryLoanRequestStatus.LoanExpired;
                        request.LoanStatus = (int)LoanRequestStatus.Pending;
                        MoolahLogManager.Log("Listing Expired",
                            LogType.Info,
                            request.tbl_Users != null ? request.tbl_Users.UserName : string.Empty,
                            request.RequestId);

                        /*if (request.tbl_Users != null)
                            EmailSender.OnLoanListingExpired(request.tbl_Users.UserName, request.RequestId.ToString());*/

                        var pendingLoanOffers = db.tbl_Loanoffers.Where(x => x.LoanRequest_Id == request.RequestId
                            && x.OfferStatus == (int)LoanOfferStatus.Pending).ToList();

                        foreach (tbl_Loanoffers offer in pendingLoanOffers)
                        {
                            offer.OfferStatus = (int)LoanOfferStatus.Expired;
                            _ITransactionDA.LoanTransactionCredit((int)offer.tbl_Users.UserID, offer.OfferedAmount,
                                request.RequestId, "Interent", "Listing Expired");
                            _ITransactionDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID, offer.OfferedAmount,
                                request.RequestId, "Interent", "Listing Expired", offer.tbl_Users.UserID);
                            //EmailSender.onLoanOfferCancelledDueToLoanRequestExpire(offer.tbl_Users.UserName, request.RequestId.ToString());*/
                        }
                    }
                    db.SaveChanges();
                    return request;
                }

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public IEnumerable<tbl_Loanoffers> GetLoanOffersByRequestID(long requestID, LoanOfferStatus status)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_Loanoffers.Include("tbl_Users")
                        .Where(o => o.LoanRequest_Id == requestID
                        && o.OfferStatus == (int)status)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public void ChangeRequestStatusByID(long id, LoanRequestStatus? status = null, PreliminaryLoanRequestStatus? preStatus = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loan = db.tbl_LoanRequests.Where(r => r.RequestId == id).SingleOrDefault();

                    if (loan != null)
                    {
                        if (status.HasValue)
                        {
                            loan.LoanStatus = (int)status;
                        }

                        if (preStatus.HasValue)
                        {
                            loan.PreliminaryLoanStatus = (int)preStatus;
                        }

                        db.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public IEnumerable<InvestorOfferDTO> GetAcceptedOfferDetails(long requestID)
        {
            try
            {
                var list = new List<InvestorOfferDTO>();

                using (var db = new dbMoolahConnectEntities())
                {
                    var query = db.tbl_Loanoffers.Where(o => o.LoanRequest_Id == requestID &&
                        o.OfferStatus == (int)LoanOfferStatus.Accepted)
                        .ToList()
                        .Select(o => new InvestorOfferDTO()
                        {
                            AcceptedAmount = o.AcceptedAmount.HasValue ? (double)o.AcceptedAmount.Value : 0,
                            AcceptedRate = o.AcceptedRate.HasValue ? (double)o.AcceptedRate.Value : 0,
                            DisplayName = o.tbl_Users != null && o.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                            ? o.tbl_Users.tbl_AccountDetails.FirstOrDefault().DisplayName : string.Empty,
                            NRICName = o.tbl_Users != null && o.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                            ? CommonMethods.GetAccountFullName(
                            o.tbl_Users.tbl_AccountDetails.FirstOrDefault().Title,
                            o.tbl_Users.tbl_AccountDetails.FirstOrDefault().FirstName,
                            o.tbl_Users.tbl_AccountDetails.FirstOrDefault().LastName
                            ) : string.Empty,
                            NRICOrPassportNumber = o.tbl_Users != null && o.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                            ? o.tbl_Users.tbl_AccountDetails.FirstOrDefault().NRIC_Number != null
                            ? o.tbl_Users.tbl_AccountDetails.FirstOrDefault().NRIC_Number : o.tbl_Users.tbl_AccountDetails.FirstOrDefault().PassportNumber
                            : string.Empty,
                            OfferedAmount = (double)o.OfferedAmount,
                            OfferedRate = (double)o.OfferedRate,
                            RepaymentAmountPortion = LoanRequestCalc.RepaymentAmount((double)o.AcceptedAmount.Value,
                            (double)o.AcceptedRate.Value, o.tbl_LoanRequests.Terms),
                            TotalInterest = CommonMethods.GetTotalInterestPayable(o.tbl_LoanRequests.Terms,
                            (double)o.AcceptedAmount.Value, (double)o.AcceptedRate.Value)
                        });

                    list = query.ToList();
                }

                return list;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<InvestorOfferDTO>();
            }
        }

        public IEnumerable<InvestorOfferDTO> GetRejectedOfferDetails(long requestID)
        {
            try
            {
                var list = new List<InvestorOfferDTO>();

                using (var db = new dbMoolahConnectEntities())
                {
                    var query = db.tbl_Loanoffers.Where(o => o.LoanRequest_Id == requestID &&
                    o.OfferStatus == (int)LoanOfferStatus.Rejected)
                    .ToList()
                    .Select(o => new InvestorOfferDTO()
                    {
                        DisplayName = o.tbl_Users != null && o.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                        ? o.tbl_Users.tbl_AccountDetails.FirstOrDefault().DisplayName : string.Empty,
                        NRICName = o.tbl_Users != null && o.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                        ? CommonMethods.GetAccountFullName(
                        o.tbl_Users.tbl_AccountDetails.FirstOrDefault().Title,
                        o.tbl_Users.tbl_AccountDetails.FirstOrDefault().FirstName,
                        o.tbl_Users.tbl_AccountDetails.FirstOrDefault().LastName
                        ) : string.Empty,
                        NRICOrPassportNumber = o.tbl_Users != null && o.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                        ? o.tbl_Users.tbl_AccountDetails.FirstOrDefault().NRIC_Number != null
                        ? o.tbl_Users.tbl_AccountDetails.FirstOrDefault().NRIC_Number : o.tbl_Users.tbl_AccountDetails.FirstOrDefault().PassportNumber
                        : string.Empty,
                        OfferedRate = (double)o.OfferedRate,
                        AmountRefunded = (double)o.OfferedAmount

                    });

                    list = query.ToList();
                }

                return list;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<InvestorOfferDTO>();
            }
        }

        public IEnumerable<InvestorOfferDTO> GetOfferAndWithdrawal(long requestID)
        {
            var list = new List<InvestorOfferDTO>();
            try
            {
              

                using (var db = new dbMoolahConnectEntities())
                {
                    list = db.Database.SqlQuery<InvestorOfferDTO>("SP_GetOfferAndWithdrawalList @loanRequestId ",
                        new SqlParameter("loanRequestId", requestID)).ToList();

                }

                return list;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return list;
            }
        }
        public IEnumerable<tbl_RepaymentMessages> GetRepaymentMessageses(long requestID)
        {
            try
            {
            using (var db = new dbMoolahConnectEntities())
            {
                return
                    db.tbl_RepaymentMessages.Where(a => a.LoanRequestId == requestID)
                        .OrderByDescending(a => a.CreatedDate).ToList();
            }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_RepaymentMessages>();
            }
        }


        public bool AddOfferLog(LoanOfferStatus status, int offerID)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var newloanOfferLog = new tbl_LoanOffersLog() { 
                        OfferID = offerID,
                        OfferStatus = (int)status,
                        TimeStamp = DateTime.UtcNow
                    };

                    db.tbl_LoanOffersLog.Add(newloanOfferLog);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}
