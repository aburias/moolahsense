﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Logging;

namespace MoolahConnect.Services.Implementation
{
    public class AuditLogDA : IAuditLogDA
    {
        public IEnumerable<tbl_AuditLog> GetBorrowerActionLogsByUsername(string username, DateTime? from = null, DateTime? to = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_AuditLog.AsEnumerable()
                        .Where(x => x.UserName.ToLower() == username.ToLower()
                        && ((from == null || x.Timestamp.Date >= from.Value.ToUniversalTime().Date)
                        && (to == null || x.Timestamp.Date <= to.Value.ToUniversalTime().Date)))
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_AuditLog>();
            }
        }


        public IEnumerable<tbl_AuditLog> GetUserWarnings()
        {
            string logType = LogType.Warn.ToString();
            using (var db = new dbMoolahConnectEntities())
            {
                return db.tbl_AuditLog.Where(
                    x => x.Level == logType)
                    .OrderByDescending(x=>x.Timestamp)
                    .ToList();
            }
        }


        public DateTime? GetLastUpdateTimeStamp(int category, int loanRequestId)
        {
            DateTime? latestTimestamp = null;

            using (var db = new dbMoolahConnectEntities())
            {
                if (category == 1)
                {
                    latestTimestamp = db.tbl_AdminActionLogs.Where(
                        al => al.LoanRequestId == loanRequestId && al.Message == "Update Note Request")
                      .Max(al => al.Timestamp);
                }

                if (category == 2)
                {
                    var tblLoanRequests = db.tbl_LoanRequests.FirstOrDefault(lr => lr.RequestId == loanRequestId);
                    if (tblLoanRequests != null)
                    {
                        var acc = db.tbl_AccountDetails.FirstOrDefault(lr => lr.User_ID == tblLoanRequests.User_ID);
                        latestTimestamp = db.tbl_AdminActionLogs.Where(
                        al => al.ActionUserId == acc.AccountId && 
                            ((al.Message == "Update Account Details" && (al.FieldName == "SiccCode" || al.FieldName == "IC_CompanyRegistrationNumber" || 
                            al.FieldName == "BusinessMailingAddress" || al.FieldName == "RegisteredAddress" || al.FieldName == "DateofIncorporation" || 
                            al.FieldName == "BusinessOrganisation" || al.FieldName == "BusinessName" || al.FieldName == "PaidUpCapital"))
                            || (al.Message == "Update Note Request" && (al.FieldName == "DetailedCompanyPro"))))
                      .Max(al => al.Timestamp);
                    }
                }

                if (category == 3)
                {
                    var tblLoanRequests = db.tbl_LoanRequests.FirstOrDefault(lr => lr.RequestId == loanRequestId);
                    if (tblLoanRequests != null)
                    {
                        var acc = db.tbl_AccountDetails.FirstOrDefault(lr => lr.User_ID == tblLoanRequests.User_ID);
                        latestTimestamp = db.tbl_AdminActionLogs.Where(
                        al => al.ActionUserId == acc.AccountId &&
                            (al.Message == "Update Note Request" && (al.FieldName == "MoolahSenseComments" || al.FieldName == "LoanPurposeDescription" ||
                            al.FieldName == "LoanPurpose_Id" || al.FieldName == "IsPersonalGuarantee" || al.FieldName == "Rate" ||
                            al.FieldName == "Terms" || al.FieldName == "Amount")))
                      .Max(al => al.Timestamp);
                    }
                }

                if (category == 4)
                {
                    latestTimestamp = db.tbl_AdminActionLogs.Where(
                        al => al.LoanRequestId == loanRequestId && al.Message == "Update Note Request Moolah Core")
                      .Max(al => al.Timestamp);
                }

                if (category == 5)
                {
                    latestTimestamp = db.tbl_AdminActionLogs.Where(
                        al => al.LoanRequestId == loanRequestId && al.Message == "Update Note Request Moolah Peri")
                      .Max(al => al.Timestamp);
                }

                if (latestTimestamp == null)
                {
                    latestTimestamp = db.tbl_AdminActionLogs.Where(
                        al => al.LoanRequestId == loanRequestId && al.Message == "Update Loan Status")
                      .Max(al => al.Timestamp);
                }
            }

            return latestTimestamp;
        }

        public void InsertAuditRecords(object objectToAudit, System.Data.Entity.DbContext db, string msg, string username, long? requestId, long? userId)
        {
            // Create audit records for changed fields
            var type = objectToAudit.GetType();
            DbPropertyValues originals;
            try
            {
                originals = db.Entry(objectToAudit).OriginalValues;
            }
            catch (Exception)
            {
                originals = null;
            }
            var properties = type.GetProperties();
            foreach (var property in properties)
            {
                if (property.Name.StartsWith(@"tbl_")) continue;

                var before = originals != null ? originals[property.Name] : "N/A";
                var after = type.GetProperty(property.Name).GetValue(objectToAudit, null);
                if ((before == null && after == null)) continue;

                if ((before == null || after == null) || (before.ToString() != after.ToString()))
                {
                    var br = before == null ? "NA" : before.ToString();
                    var af = after == null ? "NA" : after.ToString();
                    MoolahLogManager.LogAdminAudit(msg, username, requestId, userId, property.Name, br, af);
                }
            }
        }
    }
}
