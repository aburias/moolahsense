﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.GenerateReference;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util;
using System.Data.Entity.SqlServer;

namespace MoolahConnect.Services.Implementation
{
    public class TransactionsDA : ITransactionsDA
    {
        public IEnumerable<tbl_LoanTransactions> GetTransactionbyUser(string username, DateTime? from = null,
            DateTime? to = null)
        {
            try
            {
                if (from.HasValue)
                {
                    from = from.Value.ToUniversalTime();
                }

                if (to.HasValue)
                {
                    to = to.Value.AddDays(1).ToUniversalTime();
                }

                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_LoanTransactions
                        .Where(x => (x.tbl_Users != null && x.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower())
                        && x.DateCreated.HasValue
                        && ((from == null || x.DateCreated.Value >= from)
                        && (to == null || x.DateCreated.Value <= to)))
                        .OrderByDescending(x => x.DateCreated)
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_LoanTransactions>();
            }
        }


        public void LoanTransactionCredit(int userId, decimal offerAmount, long? requestId, string mode, string terms, TransactionType? type = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var balance = db.tbl_Balances.Where(x => x.User_ID == userId).SingleOrDefault();

                    if (balance == null)
                    {
                        var _balance = new tbl_Balances()
                        {
                            User_ID = userId,
                            DateCreated = DateTime.Now,
                            ActualAmount = 0,
                            LedgerAmount = 0
                        };
                        db.tbl_Balances.Add(_balance);
                        balance = _balance;
                    }

                    var newBalance = balance.ActualAmount + offerAmount;

                    var objTransaction = new tbl_LoanTransactions();
                    objTransaction.Amount = balance.ActualAmount;
                    objTransaction.Credited = offerAmount;
                    objTransaction.DateCreated = DateTime.UtcNow;
                    objTransaction.Request_Id = requestId;
                    objTransaction.PaymentMode = mode;
                    objTransaction.PaymentTerms = terms;
                    objTransaction.User_ID = userId;
                    objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    objTransaction.Balance = newBalance;

                    if (type.HasValue)
                    {
                        objTransaction.Type = (int)type;
                    }

                    db.tbl_LoanTransactions.Add(objTransaction);

                    balance.ActualAmount = newBalance;
                    balance.LedgerAmount = balance.LedgerAmount + offerAmount;

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }

        public void LoanTransactionDebit(int userId, decimal offerAmount, long? requestId, string mode, string terms, TransactionType? type = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var balance = db.tbl_Balances.Where(x => x.User_ID == userId).SingleOrDefault();

                    if (balance == null)
                    {
                        var _balance = new tbl_Balances()
                        {
                            User_ID = userId,
                            DateCreated = DateTime.Now,
                            ActualAmount = 0,
                            LedgerAmount = 0
                        };
                        db.tbl_Balances.Add(_balance);
                        balance = _balance;
                    }

                    var newBalance = balance.ActualAmount - offerAmount;

                    var objTransaction = new tbl_LoanTransactions();
                    objTransaction.Amount = balance.ActualAmount;
                    objTransaction.Debted = offerAmount;
                    objTransaction.DateCreated = DateTime.UtcNow;
                    objTransaction.Request_Id = requestId;
                    objTransaction.PaymentMode = mode;
                    objTransaction.PaymentTerms = terms;
                    objTransaction.User_ID = userId;
                    objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    objTransaction.Balance = newBalance;

                    if (type.HasValue)
                    {
                        objTransaction.Type = (int)type;
                    }

                    db.tbl_LoanTransactions.Add(objTransaction);

                    balance.ActualAmount = newBalance;
                    balance.LedgerAmount = balance.LedgerAmount - offerAmount;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public void LoanTransaction(int userId, long requestId, string mode, string terms, TransactionType? type = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var balance = db.tbl_Balances.Where(x => x.User_ID == userId).SingleOrDefault();

                    var objTransaction = new tbl_LoanTransactions();
                    objTransaction.Amount = balance.ActualAmount;
                    objTransaction.DateCreated = DateTime.UtcNow;
                    objTransaction.Request_Id = requestId;
                    objTransaction.PaymentMode = mode;
                    objTransaction.PaymentTerms = terms;
                    objTransaction.User_ID = userId;
                    objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    objTransaction.Balance = balance.ActualAmount;

                    if (type.HasValue)
                    {
                        objTransaction.Type = (int)type;
                    }

                    db.tbl_LoanTransactions.Add(objTransaction);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public bool TransferFundsByUsername(long userID, bool isDebit, decimal amount, string mode, string terms, out string error)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(x => x.UserID == userID).SingleOrDefault();

                    if (user == null)
                    {
                        error = "User is not found";
                        return false;
                    }

                    var balance = db.tbl_Balances.Where(x => x.User_ID == user.UserID).SingleOrDefault();

                    if (isDebit && user.UserRole == UserRole.Investor.ToString() && (balance == null || balance.ActualAmount < amount))
                    {
                        error = "User do not have enough balance";
                        return false;
                    }

                    if (balance == null)
                    {
                        var _balance = new tbl_Balances()
                        {
                            User_ID = user.UserID,
                            DateCreated = DateTime.Now,
                            ActualAmount = 0,
                            LedgerAmount = 0
                        };
                        db.tbl_Balances.Add(_balance);
                        balance = _balance;
                    }

                    var newBalance = isDebit ? balance.ActualAmount - amount : balance.ActualAmount + amount;

                    var objTransaction = new tbl_LoanTransactions();
                    objTransaction.Amount = balance.ActualAmount;
                    if (isDebit)
                    {
                        objTransaction.Debted = amount;
                    }
                    else
                    {
                        objTransaction.Credited = amount;
                    }
                    objTransaction.DateCreated = DateTime.UtcNow;
                    objTransaction.PaymentMode = mode;
                    objTransaction.PaymentTerms = terms;
                    objTransaction.User_ID = user.UserID;
                    objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    objTransaction.Balance = newBalance;

                    db.tbl_LoanTransactions.Add(objTransaction);

                    balance.ActualAmount = newBalance;
                    balance.LedgerAmount = isDebit ? balance.LedgerAmount - amount : balance.LedgerAmount + amount; ;
                    db.SaveChanges();
                }

                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                error = StatusMessages.SYSTEM_ERROR;
                return false;
            }
        }


        public void ChangeRepaymentStatus(long id, RepaymentPayStatus status, DateTime? date = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var a = db.tbl_LoanAmortization.Where(x => x.AmortizationID == id).SingleOrDefault();

                    if (a != null)
                    {
                        a.PayStatus = (int)status;

                        if (status == RepaymentPayStatus.Late)
                            a.LateStartDate = a.EmiDate;

                        if (status == RepaymentPayStatus.Paid)
                            a.PayedOn = date.HasValue ? date.Value : DateTime.Now;

                    }

                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public void ChangeLateRepaymentsStatus(long id, double latePayment, RepaymentPayStatus status, DateTime? date = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var loanRepayments = db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == id
                        && x.LateStartDate.HasValue
                        && (!x.LateInterestPayStatus.HasValue || x.LateInterestPayStatus.Value == (int)RepaymentPayStatus.Pending));


                    foreach (tbl_LoanAmortization repayment in loanRepayments)
                    {
                        repayment.LateInterestPayStatus = (int)status;
                        repayment.LateInterest = (decimal)latePayment;
                        repayment.LateInterestPaidDate = date.HasValue ? date.Value : DateTime.Now;
                    }


                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public bool UpdatePaidAmount(long requestId, int repaymentId, double amount, DateTime date, bool repaymentPaid, bool accruedPaid,
            bool lateFeesPaid, double? accrued = null, double? lateFees = null)
        {
            try
            {

                using (var db = new dbMoolahConnectEntities())
                {
                    var repayment = db.tbl_LoanAmortization.Where(r => r.AmortizationID == repaymentId).SingleOrDefault();

                    if (repayment == null)
                        return false;

                    if (repayment.PaidAmount.HasValue)
                    {
                        repayment.PaidAmount += (decimal)amount;
                    }
                    else
                    {
                        repayment.PaidAmount = (decimal)amount;
                    }

                    if (repaymentPaid)
                        repayment.PayStatus = (int)RepaymentPayStatus.Paid;

                    var lateRepayments = db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == requestId
                        && (x.PayStatus == (int)RepaymentPayStatus.Late ||
                        (x.PayStatus == (int)RepaymentPayStatus.Paid && x.LateInterestPayStatus == null))).ToList();

                    foreach (tbl_LoanAmortization rep in lateRepayments)
                    {
                        if (accruedPaid)
                        {
                            rep.LateInterestPaidDate = date;
                            rep.LateInterestPayStatus = (int)RepaymentPayStatus.Paid;
                            rep.LateInterest = (decimal)accrued;

                        }

                        if (lateFeesPaid)
                            rep.LateFees = (decimal)lateFees;
                    }



                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool AddLoanPayment(long requestId, int repaymentId, double amount, DateTime paidDate, double? lateFees = null)
        {
            try
            {

                using (var db = new dbMoolahConnectEntities())
                {
                    var loanPayment = new tbl_LoanPayments()
                    {
                        Amount = (decimal)amount,
                        LoanRequestID = requestId,
                        PaidDate = paidDate,
                        RepaymentID = repaymentId,
                        TimeStamp = DateTime.Now,
                        LateFees = (decimal?)lateFees
                    };

                    db.tbl_LoanPayments.Add(loanPayment);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public double[] GetAccruedInterestAndLateFees(long repaymentId, DateTime? toDate = null)
        {
            try
            {
                double[] AccruedInterestAndLateFees = new double[3];
                double interest = 0, amountWithinterest = 0, amount = 0;
                DateTime dueDate = DateTime.MinValue, startDate = DateTime.MinValue, sinNowDate = DateTime.MinValue;
                var lateRepayments = new List<tbl_LoanAmortization>();
                var payments = new List<tbl_LoanPayments>();
                double lateFees = 0, totalRepaymentsAmount = 0;
                tbl_LoanAmortization repayment = new tbl_LoanAmortization();

                using (var db = new dbMoolahConnectEntities())
                {
                    repayment = db.tbl_LoanAmortization.Where(x => x.AmortizationID == repaymentId).SingleOrDefault();

                    if (repayment == null)
                    {
                        return new double[2] { 0, 0 };
                    }

                    var loanId = repayment.LoanRequest_ID;

                    dueDate = db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == loanId
                        && (x.PayStatus == (int)RepaymentPayStatus.Late ||
                        (x.PayStatus == (int)RepaymentPayStatus.Paid && x.LateInterestPayStatus == null)))
                        .OrderBy(x => x.EmiDate).First().EmiDate.Value;

                    lateRepayments = db.tbl_LoanAmortization.Where(x => x.LoanRequest_ID == loanId
                        && (x.PayStatus == (int)RepaymentPayStatus.Late ||
                        (x.PayStatus == (int)RepaymentPayStatus.Paid && x.LateInterestPayStatus == null)))
                        .OrderBy(x => x.EmiDate).ToList();

                    payments = db.tbl_LoanPayments.Where(x => x.LoanRequestID == loanId
                        && x.PaidDate >= dueDate).OrderBy(x => x.PaidDate).ToList();

                    startDate = DateTimeExtenstion.SingaporeTime(dueDate).AddDays(1).Date;
                    sinNowDate = toDate.HasValue ? toDate.Value : DateTimeExtenstion.SingaporeTime().Date;

                    amount = (double)repayment.EmiAmount;
                    amountWithinterest = amount;
                }

                if (startDate < sinNowDate)
                {
                    do
                    {
                        if (lateRepayments.Any(a => DateTimeExtenstion.SingaporeTime(a.EmiDate.Value).AddDays(1).Date == startDate)
                            )
                        {
                            if (DateTimeExtenstion.SingaporeTime(lateRepayments.First().EmiDate.Value).AddDays(1).Date != startDate)
                                amountWithinterest += amount;

                            lateFees += amountWithinterest / 100 < 200 ? 200 : (double)amountWithinterest / 100;

                            totalRepaymentsAmount += amount;

                        }

                        var interestTemp = (amountWithinterest)
                            * (Settings.LatePayInterest / 100) / Settings.DayCountConvention;

                        amountWithinterest += interestTemp;
                        interest += interestTemp;

                        //The partial payment should be only considered for the next day accrued interest calucation, 
                        //since for this day it has already calculated on morning 0000. So it added after the calculation
                        if (payments.Any(a => a.PaidDate.Date == startDate))
                        {
                            double amountWithoutLateFees = 0;
                            IEnumerable<tbl_LoanPayments> _payments = payments.Where(x => x.PaidDate.Date == startDate);

                            amountWithoutLateFees = (double)_payments.Sum(x => x.Amount) -
                                (_payments.Sum(x => x.LateFees).HasValue ? (double)_payments.Sum(x => x.LateFees).Value : 0);

                            amountWithinterest -= amountWithoutLateFees;
                        }

                        startDate = startDate.AddDays(1);
                    } while (startDate <= sinNowDate);
                }

                AccruedInterestAndLateFees[0] = Math.Round(interest, 2);
                AccruedInterestAndLateFees[1] = Math.Round(lateFees, 2);
                AccruedInterestAndLateFees[2] = Math.Round(totalRepaymentsAmount, 2);
                return AccruedInterestAndLateFees;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new double[2] { 0, 0 };
            }
        }


        public IEnumerable<tbl_LoanPayments> GetLoanPayments(DateTime? _from, DateTime? _to)
        {
            using (var db = new dbMoolahConnectEntities())
            {
                var result =
                    db.tbl_LoanPayments
                    .Include("tbl_LoanAmortization")
                    .Include("tbl_LoanRequests")
                    .ToList()
                    .Where(l =>
                            (_from == null || (l.PaidDate.Date >= _from.Value.ToUniversalTime()))
                            && (_to == null || (l.PaidDate.Date <= _to.Value.ToUniversalTime()))
                    )
                    .OrderByDescending(d => d.PaidDate);
                return result.ToList();
            }
        }


        public bool AddAccountAdjustment(long repaymentId, double PaidAmountSum, double adjustment)
        {
            try
            {
                using (dbMoolahConnectEntities db = new dbMoolahConnectEntities())
                {
                    tbl_AccountAdjustment accAdjustment = new tbl_AccountAdjustment();
                    accAdjustment.RepaymentID = repaymentId;
                    accAdjustment.TotalReceivable = (decimal)PaidAmountSum;
                    accAdjustment.Difference = (decimal)adjustment;
                    accAdjustment.TimeStamp = DateTime.UtcNow;

                    db.tbl_AccountAdjustment.Add(accAdjustment);
                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public IEnumerable<tbl_AccountAdjustment> GetAccountAdjustments(DateTime? _from, DateTime? _to)
        {
            try
            {
                using (dbMoolahConnectEntities db = new dbMoolahConnectEntities())
                {
                    var result =
                   db.tbl_AccountAdjustment
                   .Include("tbl_LoanAmortization")
                   .Include("tbl_LoanAmortization.tbl_LoanRequests")
                   .ToList()
                   .Where(l =>
                           (_from == null || (l.TimeStamp.Date >= _from.Value.ToUniversalTime()))
                           && (_to == null || (l.TimeStamp.Date <= _to.Value.ToUniversalTime()))
                   )
                   .OrderByDescending(d => d.TimeStamp);
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_AccountAdjustment>();
            }
        }


        public void TransactionDebit(decimal? AmountBefore, decimal? AmountTobeDebited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
                    objTransaction.Amount = AmountBefore;///its the the amount user have in tbl_blances  before the transaction has occured
                    objTransaction.Debted = AmountTobeDebited;//amount of offer that should be debited in user balace
                    objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime
                    if (PaymentMode != "Withdrawl money")
                        objTransaction.Request_Id = LoanRequestID;//loan request from which offer has withdrawn
                    else
                        objTransaction.Withdraw_ID = LoanRequestID;
                    objTransaction.PaymentMode = PaymentMode;
                    objTransaction.PaymentTerms = PaymentTerms;
                    objTransaction.User_ID = UserID;
                    objTransaction.Balance = BalanceAfter;//balance of user after debting the amount from his account
                    objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    db.tbl_LoanTransactions.Add(objTransaction);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }

        public void TransactionCredit(decimal? AmountBefore, decimal? AmountToCredited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
                    objTransaction.Amount = Convert.ToDecimal(AmountBefore);///its the the amount user have in tbl_blances  before the transaction has occured
                    objTransaction.Credited = Convert.ToDecimal(AmountToCredited);//amount of offer that should be credit in user balace
                    objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime
                    objTransaction.Request_Id = LoanRequestID;//loan request from which offer has withdrawn
                    objTransaction.PaymentMode = PaymentMode;
                    objTransaction.PaymentTerms = PaymentTerms;
                    objTransaction.User_ID = UserID;
                    objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    objTransaction.Balance = Convert.ToDecimal(BalanceAfter);//balance of user after crediting the amount in his account

                    db.tbl_LoanTransactions.Add(objTransaction);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public bool InternalTransactionDebit(int balanceID, decimal amount, long? requestId, string mode,
            string terms, long referenceUserID, TransactionType? type = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var balanceRef = db.tbl_Balances.SingleOrDefault(b => b.BalanceId == balanceID);

                    if (balanceRef == null)
                    {
                        return false;
                    }

                    var currentAccountBalance = balanceRef.ActualAmount;

                    var transaction = new tbl_LoanTransactions();
                    transaction.Amount = balanceRef.ActualAmount;
                    transaction.Balance = currentAccountBalance - amount;
                    transaction.Debted = amount;
                    transaction.PaymentMode = mode;
                    transaction.PaymentTerms = terms;
                    transaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    transaction.RefUserID = referenceUserID;
                    transaction.Request_Id = requestId;
                    transaction.BalanceID = balanceID;
                    transaction.DateCreated = DateTime.UtcNow;

                    if (type.HasValue)
                    {
                        transaction.Type = (int)type;
                    }

                    db.tbl_LoanTransactions.Add(transaction);

                    balanceRef.ActualAmount = currentAccountBalance - amount;

                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool InternalTransactionCredit(int balanceID, decimal amount, long? requestId,
            string mode, string terms, long referenceUserID, TransactionType? type = null)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var balanceRef = db.tbl_Balances.SingleOrDefault(b => b.BalanceId == balanceID);

                    if (balanceRef == null)
                    {
                        return false;
                    }

                    var currentAccountBalance = balanceRef.ActualAmount;

                    var transaction = new tbl_LoanTransactions();
                    transaction.Amount = balanceRef.ActualAmount;
                    transaction.Balance = currentAccountBalance + amount;
                    transaction.Credited = amount;
                    transaction.PaymentMode = mode;
                    transaction.PaymentTerms = terms;
                    transaction.Reference = UniqueReference.GetRandomUniqueNumber();
                    transaction.RefUserID = referenceUserID;
                    transaction.Request_Id = requestId;
                    transaction.BalanceID = balanceID;
                    transaction.DateCreated = DateTime.UtcNow;

                    if (type.HasValue)
                    {
                        transaction.Type = (int)type;
                    }

                    db.tbl_LoanTransactions.Add(transaction);

                    balanceRef.ActualAmount = currentAccountBalance + amount;

                    db.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public IEnumerable<tbl_LoanTransactions> GetInternalTransactions(DateTime? _from, DateTime? _to)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var result =
                        db.tbl_LoanTransactions
                        .Include("tbl_Users")
                        .Include("tbl_Users.tbl_AccountDetails")
                        .ToList()
                        .Where(l =>
                               l.BalanceID.HasValue && (l.BalanceID == Settings.MsenseAccountBalanceID || l.BalanceID == Settings.EarmarkAccountBalanceID)
                                && (_from == null || (l.DateCreated.HasValue && l.DateCreated.Value.Date >= _from.Value.ToUniversalTime()))
                                && (_to == null || (l.DateCreated.HasValue && l.DateCreated.Value.Date <= _to.Value.ToUniversalTime()))
                        )
                        .OrderByDescending(d => d.DateCreated);
                    return result.ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_LoanTransactions>();
            }
        }


        public bool ChangeAccountAdjustment(long id, bool adjusted)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var accountAdjustment = db.tbl_AccountAdjustment.Where(aa => aa.ID == id).SingleOrDefault();

                    if (accountAdjustment != null)
                    {
                        accountAdjustment.Adjusted = adjusted;
                        accountAdjustment.AdjustedDate = DateTime.UtcNow;
                        db.SaveChanges();
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }

        }
    }
}
