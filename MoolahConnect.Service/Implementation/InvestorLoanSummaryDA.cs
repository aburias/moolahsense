﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Services.Implementation
{
    public class InvestorLoanSummaryDA : IInvestorLoanSummaryDA
    {

        public tbl_MoolahPeriVerification GetMoolahPeriVerificationForLoanRequest(long loanRequestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_MoolahPeriVerification.Where(p => p.Request_ID == loanRequestId).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }

        public tbl_MoolahPeri GetMoolahPeriForLoanRequest(long loanRequestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_MoolahPeri.Where(p => p.Request_ID == loanRequestId).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }

        public tbl_MoolahCoreVerification GetMoolahCoreVerificationForLoanRequest(long loanRequestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_MoolahCoreVerification.Where(p => p.Request_ID == loanRequestId).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }

        public tbl_MoolahCore GetLoanRequestMoolahCoreForLoanRequest(long loanRequestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_MoolahCore.Where(p => p.LoanRequest_Id == loanRequestId).SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }

        public tbl_AccountDetails GetAccountDetailsFromRequestId(long loanRequestId)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var accountDetails = from ad in db.tbl_AccountDetails
                                         join lr in db.tbl_LoanRequests on ad.User_ID equals lr.User_ID
                                         where lr.RequestId == loanRequestId
                                         select ad;

                    return accountDetails.SingleOrDefault();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public double GetWeightedAverageNominalYeildForUser(string username)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var acceptedLoanOffers = db.tbl_Loanoffers.Where(x =>
                        x.tbl_Users != null
                        && x.tbl_Users.UserName.ToLower() == username.ToLower()
                        && x.OfferStatus == (int)LoanOfferStatus.Accepted);

                    if (acceptedLoanOffers.Count() == 0)
                        return 0;

                    var sumA = acceptedLoanOffers.Select(x => x.AcceptedAmount * x.AcceptedRate).Sum();

                    if (!sumA.HasValue)
                        return 0;

                    var subB = acceptedLoanOffers.Sum(x => x.AcceptedAmount);

                    if (!subB.HasValue)
                        return 0;

                    return (double)(sumA.Value / subB);
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                throw;
            }
        }
    }
}
