﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Logging;

namespace MoolahConnect.Services.Implementation
{
    public class SecurityQuestions : ISecurityQuestions
    {
        public IEnumerable<tbl_SecurityQuestions> Get()
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    return db.tbl_SecurityQuestions.ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_SecurityQuestions>();
            }
        }

        public IEnumerable<tbl_SecurityQuestions> GetByUserName(string username)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.Where(u => u.UserName.Trim().ToLower() == username.Trim().ToLower()).SingleOrDefault();

                    if (user == null)
                    {
                        return new List<tbl_SecurityQuestions>();
                    }

                    IEnumerable<int> questionIds = db.tbl_SecurityQuestionsForUsers.Where(su => su.User_ID == user.UserID).Select(su => su.Question_Id.Value).ToList();

                    return db.tbl_SecurityQuestions
                        .Where(s => questionIds.Any(su => su == s.QuestionId))
                        .ToList();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_SecurityQuestions>();
            }
        }


        public bool Add(long userId, int questionId, string answer)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var securityQuestion = new tbl_SecurityQuestionsForUsers();
                    securityQuestion.User_ID = userId;
                    securityQuestion.Question_Id = questionId;
                    securityQuestion.Answer = answer;

                    db.tbl_SecurityQuestionsForUsers.Add(securityQuestion);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool IsValidCombination(string username, int questionId, string answer)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    if (db.tbl_SecurityQuestionsForUsers.AsEnumerable().Any(x =>
                        (x.tbl_Users != null && x.tbl_Users.UserName.Equals(username, StringComparison.CurrentCultureIgnoreCase))
                        && x.Question_Id == questionId
                        && x.Answer.Equals(answer, StringComparison.CurrentCultureIgnoreCase)
                        ))
                    {
                        return true;
                    }

                    return false;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool RemoveAllByUsername(string username)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var questions = db.tbl_SecurityQuestionsForUsers
                        .Where(u => u.tbl_Users != null && u.tbl_Users.UserName.Trim().ToLower() == username.Trim().ToLower());

                    db.tbl_SecurityQuestionsForUsers.RemoveRange(questions);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool Add(string username, int questionId, string answer)
        {
            try
            {
                using (var db = new dbMoolahConnectEntities())
                {
                    var user = db.tbl_Users.SingleOrDefault(u => u.UserName.Trim().ToLower() == username.Trim().ToLower());

                    if (user == null)
                        return false;

                    var securityQuestion = new tbl_SecurityQuestionsForUsers();
                    securityQuestion.User_ID = user.UserID;
                    securityQuestion.Question_Id = questionId;
                    securityQuestion.Answer = answer;

                    db.tbl_SecurityQuestionsForUsers.Add(securityQuestion);
                    db.SaveChanges();

                    return true;
                }
            }
            catch (Exception ex)
            {

                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}
