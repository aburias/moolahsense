﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
  public  interface IMoolahPeriService
  {
      MoolahPeri Get(int loanRequestId);
      string Save(MoolahPeri moolah, bool isDraft);
  }
}
