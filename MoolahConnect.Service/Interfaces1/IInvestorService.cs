﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Models.CustomModels;

namespace MoolahConnect.Services.Interfaces1
{
    public interface IInvestorService
    {
        string IndexLoad();
        InvestorAccount GetRegistrationDetailsForPDF();
        InvestorAccount GetInvestorAccountInfo();
        string CreateInvestorAccount(InvestorAccountDetailsModel objinvestor);
        void LogDetailsOnError(InvestorAccountDetailsModel investor);
        string GetKnowledgeAssessmentOptions(int parentid);
        string GetKnowledgeAssessmentOptions(int parentid, long userid);
        string SaveKnowledgeAssesmentOptions(string XMLdocs);
        string SaveKnowledgeAssesmentOptions(string XMLdocs, long userid);
        List<SelectListItem> TitleItems();
        List<SelectListItem> GenderItems();
        IEnumerable<SelectListItem> NationalityItems();
        List<SelectListItem> IncomerangeItems();
        InvestorLoanOfferDTO GetLoanAndBorrowerBriefDetailByRequestId(long loanRequestId);
        InvestorLoanOfferDTO GetLoanAndBorrowerBriefDetailForPreviewByRequestId(long loanRequestId);
        string SaveLoanOffer(string amount, string rate, string loanRequestId, string term);
        LiveLoanListingsDTO GetLiveLoanListings();
        DateTime? GetLastEmiDateByLoanRequestId(long loanRequestId);
        bool? ChkLastEmiIsPending(long requestId);
        InvestorDashboardDTO LoadDashboardInformation();
        Models.BusinessModels.AccountDetail GetUserProfileDetailbyUserId();
        IEnumerable<LoanTransaction> GetAllTransactionbyUserId();
        string CheckCurrentBalance(decimal amount);
        string WithDrawOfferbyOfferId(int offerId);
        void LoanTransactionCredit(decimal? amountBefore, decimal? amountToCredited, long? loanRequestId,
            string paymentMode, string paymentTerms, long? userId, decimal? balanceAfter);
        void LoanTransactionDebit(decimal? amountBefore, decimal? amountTobeDebited, long? loanRequestId,
            string paymentMode, string paymentTerms, long? userId, decimal? balanceAfter);
        List<SelectListItem> OutstandingLitigation();
        List<SelectListItem> IsPersonalGuarantee();
        List<SelectListItem> AddCustomValueInEnd();
        List<SelectListItem> MoolaCoreTime();
        string TestUserVerification(int? status);
        bool CheckVerificationStatus(string columnValue);
        string AddToWatchList(long loanRequestId);
        BorrowerAccount CorporateUserLoad();
        string CreateCorporateInvestor(BorrowerAccountDetailsModel objborrower);
        bool CheckDisplayNameAvailability(string displayname, string emailaddress);
    }
}
