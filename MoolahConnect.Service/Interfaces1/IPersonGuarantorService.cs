﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations.Model;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
   public interface IPersonGuarantorService
   {
       IEnumerable<PersonalGuaranteeInfo> Get(long requestId);
       void Update(PersonalGuaranteeInfo personalGuaranteeInfo );
       void Save(PersonalGuaranteeInfo personalGuaranteeInfo);
       void Delete(PersonalGuaranteeInfo personalGuaranteeInfo);
   }
    
}
