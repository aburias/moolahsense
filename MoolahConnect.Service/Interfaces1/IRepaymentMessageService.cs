﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
    public interface IRepaymentMessageService
    {
        RepaymentMessage GetByID(long repaymentID);
        void Create(RepaymentMessage repaymentMessage);
        void Update(RepaymentMessage repaymentMessage);
        void Delete(long repaymentID);
    }
}
