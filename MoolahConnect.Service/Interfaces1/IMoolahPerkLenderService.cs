﻿using System.Collections.Generic;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
    public interface IMoolahPerkLenderService
    {
        IEnumerable<MoolahPerkLender> GetAll();
    }
}