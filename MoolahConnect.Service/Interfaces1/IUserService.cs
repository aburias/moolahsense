﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Models.CustomModels;

namespace MoolahConnect.Services.Interfaces1
{
   public interface IUserService
    {
        string GetUserRoleByUsername(string username);
        bool Remove(string username);
        int GetFailedPasswordAttemptsCount(Guid userId);
        bool LockUser(Guid userId);
        bool IsEmailVerified(string username);
        bool UpdateEmailVerification(string username, bool verified);
        IEnumerable<string> GetAllAdminUsers();
        bool ValidateAdminUser(string username, string password);

        void SaveDigitalSignature(long userID);

        User GetUserFromAspNetId(Guid id);
        User GetUserFromId(long id);
        AccountDetailDTO GetAccountDetailsByID(long id);

        IEnumerable<User> GetApprovedInvestors();

        User GetUser(string userName);

        UsersSummaryDTO GetUsersSummary();
    }
}
