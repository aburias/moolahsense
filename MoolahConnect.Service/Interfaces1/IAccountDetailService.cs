﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;
using Org.BouncyCastle.Security;

namespace MoolahConnect.Services.Interfaces1
{
    public interface IAccountDetailService
    {
        IEnumerable<AccountDetail> Get();

        IEnumerable<AccountDetail> Get(List<long> idList);

        AccountDetail GetByUser(long id);
    }
}
