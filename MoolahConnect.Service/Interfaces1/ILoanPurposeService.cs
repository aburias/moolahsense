﻿using System.Collections.Generic;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
    public interface ILoanPurposeService
    {
        IEnumerable<LoanPurpose> Get();
    }
}