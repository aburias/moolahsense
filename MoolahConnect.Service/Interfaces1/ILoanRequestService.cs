﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
   public interface ILoanRequestService
   {
        void Save(LoanRequest loanRequest);
       void Update(LoanRequest loanRequest);
       void Update(LoanRequest loanRequest, params Expression<Func<LoanRequest, object>>[] modifiedProperties);
       IEnumerable<LoanRequest> GetLoanRequestbyUserID(long userId);
       LoanRequest GetLoanRequestbyRequestID(long requestId);
   }
}
