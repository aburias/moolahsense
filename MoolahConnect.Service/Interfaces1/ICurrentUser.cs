﻿using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
    public interface ICurrentUser
    {
        User ApplicationUser { get; }
    }
}