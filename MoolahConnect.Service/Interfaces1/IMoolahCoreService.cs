﻿using MoolahConnect.Models.BusinessModels;

namespace MoolahConnect.Services.Interfaces1
{
   public interface IMoolahCoreService
   {
        MoolahCore Get(long loanRequestId);
        void Create(MoolahCore moolahCore);
        void Update(MoolahCore moolah);
        void Deattched(MoolahCore moolahCore);
        void CreateOrUpdate(MoolahCore moolah);
   }
}
