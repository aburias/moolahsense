﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoolahConnect.Services.Interfaces1
{
   public interface ISessionService
   {
       T Get<T>(string key);
       void Set<T>(string key, T t);
       void Remove(string key);
   }
}
