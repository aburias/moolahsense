﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Interfaces
{
    public interface IInvestorLoanSummaryDA
    {
        tbl_MoolahPeriVerification GetMoolahPeriVerificationForLoanRequest(long loanRequestId);
        tbl_MoolahPeri GetMoolahPeriForLoanRequest(long loanRequestId);
        tbl_MoolahCoreVerification GetMoolahCoreVerificationForLoanRequest(long loanRequestId);
        tbl_MoolahCore GetLoanRequestMoolahCoreForLoanRequest(long loanRequestId);
        tbl_AccountDetails GetAccountDetailsFromRequestId(long loanRequestId);
        double GetWeightedAverageNominalYeildForUser(string username);
    }
}
