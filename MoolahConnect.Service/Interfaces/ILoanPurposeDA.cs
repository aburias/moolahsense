﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Interfaces
{
    public interface ILoanPurposeDA
    {
        IEnumerable<tbl_LoanPurposesList> Get();
    }
}
