﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Interfaces
{
    public interface IUser
    {
        string GetUserRoleByUsername(string username);
        bool Remove(string username);
        int GetFailedPasswordAttemptsCount(Guid userId);
        bool LockUser(Guid userId);
        bool IsEmailVerified(string username);
        bool UpdateEmailVerification(string username,bool verified);
        IEnumerable<string> GetAllAdminUsers();
        bool ValidateAdminUser(string username, string password);

        void SaveDigitalSignature(long userID);

        tbl_Users GetUserFromAspNetId(Guid id);
        tbl_Users GetUserFromId(long id);
        tbl_AccountDetails GetAccountDetailsByID(long id);

        IEnumerable<tbl_Users> GetApprovedInvestors();

        UserAccountEntity GetUserAccountFromUserID(long userID);

        IEnumerable<InvestorKnowledgeAssesmentDetails> GetInvestorKnowledgeAssesmentDetails(long requestID);

    }
}
