﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Services.Interfaces
{
    public interface ITransactionsDA
    {
        IEnumerable<tbl_LoanTransactions> GetTransactionbyUser(string username, DateTime? from = null,
            DateTime? to = null);

        void LoanTransactionCredit(int userId, decimal offerAmount, long? requestId, string mode, string terms, TransactionType? type = null);
        void LoanTransactionDebit(int userId, decimal offerAmount, long? requestId, string mode, string terms, TransactionType? type = null);
        void LoanTransaction(int userId, long requestId, string mode, string terms, TransactionType? type = null);

        bool TransferFundsByUsername(long userID, bool isDebit, decimal amount,string mode, string terms,out string error);

        void ChangeRepaymentStatus(long id, RepaymentPayStatus status,DateTime? date = null);
        void ChangeLateRepaymentsStatus(long id, double latePayment, RepaymentPayStatus status, DateTime? date = null);

        bool UpdatePaidAmount(long requestId, int repaymentId, double amount,DateTime date, bool repaymentPaid, bool accruedPaid, bool lateFeesPaid,
            double? accrued = null, double? lateFees = null);
        bool AddLoanPayment(long requestId, int repaymentId, double amount, DateTime paidDate, double? lateFees = null);

        double[] GetAccruedInterestAndLateFees(long repaymentId,DateTime? toDate = null);

        IEnumerable<tbl_LoanPayments> GetLoanPayments(DateTime? from, DateTime? to);

        bool AddAccountAdjustment(long repaymentId, double PaidAmountSum, double adjustment);
        IEnumerable<tbl_AccountAdjustment> GetAccountAdjustments(DateTime? from, DateTime? to);

        void TransactionDebit(decimal? AmountBefore, decimal? AmountTobeDebited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter);
        void TransactionCredit(decimal? AmountBefore, decimal? AmountToCredited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter);

        bool InternalTransactionDebit(int balanceID, decimal amount, long? requestId, string mode, string terms, long referenceUserID, TransactionType? type = null);
        bool InternalTransactionCredit(int balanceID, decimal amount, long? requestId, string mode, string terms, long referenceUserID, TransactionType? type = null);

        IEnumerable<tbl_LoanTransactions> GetInternalTransactions(DateTime? from, DateTime? to);

        bool ChangeAccountAdjustment(long id, bool adjusted);
    }
}
