﻿using System.Diagnostics.Eventing.Reader;
using MoolahConnect.Services.Entities;
using System.Collections.Generic;
using System;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Services.Interfaces
{
    public interface ILoanDetailsDA
    {
        tbl_LoanRequests GetLoanRequestFromId(long requestId);
        tbl_AccountDetails GetAccountDetailsFromUserId(int userId);
        string GetMoolahPerkFromId(int id);
        IEnumerable<tbl_LoanTransactions> GetAllLoanTransactionsByDateRange(DateTime? from, DateTime? to);
        IEnumerable<tbl_LoanTransactions> GetUserLoanTransactionsByDateRange(DateTime? from, DateTime? to);

        IEnumerable<LoanEntity> GetLoans(DateTime? from, DateTime? to, LoanRequestStatus loanRequestStatus);

        tbl_LoanAmortization GetRepaymentByID(long repaymentID);
        IEnumerable<LoanRepaymentEntity> GetRepayments(DateTime? from, DateTime? to);
        IEnumerable<LoanRepaymentEntity> GetRepaymentsByStats(DateTime? from, DateTime? to, RepaymentPayStatus status);
        IEnumerable<LoanRepaymentEntity> GetLateRepayments(DateTime? from, DateTime? to);
        IEnumerable<RepaymentEntity> GetRepaymentsByRequestID(long requestID);

        IEnumerable<LoanInvestorEntity> GetLoanInvestors(long requestId, long repId);
        IEnumerable<InvestorOfferDTO> GetOfferAndWithdrawal(long requestID);
        IEnumerable<InvestorDTO> GetInvestorList(bool hasBalance);
        IEnumerable<LoanInvestorEntity> GetLoanInvestorsForDepLatePayments(long requestId, double LatePayment);

        int MonthsTillMaturity(long loanRequest);

        tbl_LoanRequests ExpireLoanRequests(long requestID);

        IEnumerable<tbl_Loanoffers> GetLoanOffersByRequestID(long requestID, LoanOfferStatus status);

        void ChangeRequestStatusByID(long id, LoanRequestStatus? status = null, PreliminaryLoanRequestStatus? preStatus= null);

        IEnumerable<InvestorOfferDTO> GetAcceptedOfferDetails(long requestID);
        IEnumerable<InvestorOfferDTO> GetRejectedOfferDetails(long requestID);

        IEnumerable<tbl_RepaymentMessages> GetRepaymentMessageses(long requestID);

        bool AddOfferLog(LoanOfferStatus status, int offerID);
    }
}
