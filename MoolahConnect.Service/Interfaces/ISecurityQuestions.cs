﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Interfaces
{
    public interface ISecurityQuestions
    {
        IEnumerable<tbl_SecurityQuestions> Get();
        IEnumerable<tbl_SecurityQuestions> GetByUserName(string username);
        bool Add(long userId,int questionId,string answer);
        bool Add(string username, int questionId, string answer);
        bool RemoveAllByUsername(string username);
        bool IsValidCombination(string username, int questionId, string answer);
    }
}
