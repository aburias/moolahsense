﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Interfaces
{
    public interface IAuditLogDA
    {
        IEnumerable<tbl_AuditLog> GetBorrowerActionLogsByUsername(string username, DateTime? from = null, DateTime? to = null);
        IEnumerable<tbl_AuditLog> GetUserWarnings();
        DateTime? GetLastUpdateTimeStamp(int category, int loanRequestId);
        void InsertAuditRecords(object objectToAudit, DbContext db, string msg, string username,long? requestId, long? userId);
    }
}
