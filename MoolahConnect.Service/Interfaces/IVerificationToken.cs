﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Services.Interfaces
{
    public interface IVerificationToken
    {
        bool IsValidToken(string token, bool isExpire);
        bool Save(Guid userId, Guid token);
    }
}
