﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Services.Interfaces
{
    public interface IFileUploadDA
    {
        bool InsertUploadedFile(string filename,FileType fileType,Guid userId,long? requestId = null,int? offerId = null, bool multi = true);
        IEnumerable<tbl_Files> GetFilesFromUserIdAndTypeAndRequestId(Guid userid, FileType type,long? requestId = null);
        bool DeleteFile(int fileId);
        void UpdateUploadedFileComments(List<string> IdsAndComments);
        tbl_Files GetCoverPhotoFile(Guid userId, long requestId);
        IEnumerable<tbl_Files> GetFilesByRequestId(long requestId);
    }
}
