﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Services.Interfaces
{
    public interface IAccountDA
    {
        IEnumerable<tbl_AccountDetails> Get();

        IEnumerable<tbl_AccountDetails> Get(List<long> idList);

        tbl_Users GetUser(string name);
    }
}
