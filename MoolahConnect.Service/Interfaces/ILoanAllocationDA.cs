﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Models;

namespace MoolahConnect.Services.Interfaces
{
    public interface ILoanAllocationDA
    {
        tbl_LoanRequests GetLoanRequestByRequestId(long requestId);
        IEnumerable<tbl_LoanRequests> GetAllPublishedLoanRequests();
        IEnumerable<tbl_Loanoffers> GetLoanOffersByrequestId(long requestId);
        bool UpdateLoanOffer(int offerId, double acceptedAmount, double? acceptedRate, LoanOfferStatus status, string contractIdentifier, string reasonToReject = null);
        bool UpdateLoanRequestStatusByRequestId(long requestId, LoanRequestStatus status);
        bool UpdateLoanRequestOnApproval(long requestId, double acceptedAmount, decimal acceptedRate);
        bool InsertLoanAmortization(tbl_LoanAmortization ammortization);

        IEnumerable<tbl_Loanoffers> GetLoanOffersByInvestor(string username, DateTime? _from = null, DateTime? _to = null);
        IEnumerable<LoanInvestmentEntity> GetLoanInvestmentsByInvestor(string username, DateTime? _from = null, DateTime? _to = null);
        IEnumerable<LoanInvestmentEntity> GetIndividualLoanInvestmentsByInvestor(string username, DateTime? _from = null, DateTime? _to = null);

        IEnumerable<tbl_LoanRequests> GetWatchList(long investorId);

        void AddToWatchList(long userId, long requestId);
        void RemoveFromWatchList(long userId, long requestId);
        bool CanWithdrawLoanOffer(long loanRequestId, int offerId);

        BorrowerLoanContractModel GetBorrowerLoanContractModel(long requestId);
        InvestorLoanContractModel GetInvestorLoanContractModel(int offerId);

        bool AcceptPendingTransfer(long id, DateTime transferDate, decimal moolahFee, string adminUser);
        IEnumerable<UserTemp> RejectPendingTransfer(long id, string reson, string adminUser);
    }
}
