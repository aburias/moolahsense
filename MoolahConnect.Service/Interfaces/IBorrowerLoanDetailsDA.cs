﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Services.Interfaces
{
    public interface IBorrowerLoanDetailsDA
    {
        long InsertInitialLoanRequest(decimal amount, string terms, decimal rate,string username);
        IEnumerable<LoanEntity> GetLoansByUser(string username, List<LoanRequestStatus> statuses);
        tbl_LoanRequests GetLoanDraftByUser(string username);
        IEnumerable<tbl_LoanAmortization> Get3MonthsOutstandingAnlAllLatePaymentsByUser(string username);
        decimal? GetTotalMatchedAndMaturedLoan(string username);
        int GetTotalUniqueInvestor(string username);
        bool RemoveLoanRequestFromId(long requestId);
        bool PublishLoan(long requestId);
        bool DeleteLoan(long requestId);
        decimal? OutstandingLoanAmount(decimal requestId);
        decimal? GetMaxLoanAmount(string username);
        decimal? GetMaxLoanInvestmentAmount(string username);

        bool AddYouTubeLink(string link, string username, long requestId);
    }
}
