﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MoolahConnect.Services.Infrastructure
{
   public class EntityRelationshipName
   {
       public const string LoanPurposes = "LoanPurposes";
       public const string PersonalGuaranteeInfos = "PersonalGuaranteeInfos";
       public const string MoolahCores = "MoolahCores";
       public const string MoolahCoreVerifications = "MoolahCoreVerifications";
       public const string MoolahPeris = "MoolahPeris";
       public const string MoolahPeriVerifications = "MoolahPeriVerifications";
       public const string RepaymentMessages = "RepaymentMessages";
   }
}
