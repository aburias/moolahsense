﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.util.collections;
using Microsoft.Office.Interop.Excel;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.FinancialCalculations;
using MoolahConnect.Util.Logging;
using Org.BouncyCastle.Ocsp;

namespace MoolahConnect.Services
{
    public interface IIssuerService
    {
        string BorrowerIndexLoad();
        LoanRequest GetTermsheetDetails(long loanId);
        AccountDetail FillBorrowerDashboard();
        AccountDetail GetCompayProfile();
        AccountDetail GetmodelforgeneratePdf();
        AccountDetail CreateLoad();
        LoanRequest LoanrequestLoad();
      string CreateBorrower(AccountDetail accountDetail);
       void UpdateLoadLoanRequestDescription(LoanRequest loanRequest);
        void UpdateLoadLoanRequestMoolahcore(LoanRequest loanRequest);
        void UpdateLoadLoanRequestMoolahperi(LoanRequest loanRequest);
        string SaveLoanrequest(LoanRequest loanRequest);
        string SaveLoanrequestmoolahcore(MoolahCore moolahCore);
        string SaveLoanrequestmoolahPeri(LoanRequest loanRequest);

        string SaveLoanrequest(decimal Amount, string Terms, decimal Rate, decimal EIR, bool Ispersonalguarantee,
            int LoanpurposeID, string XMLquestionswithanswers, string Loanimages, string LoanDocs,
            string NameasinNRICPassport, string Designation, string NRICPassport, string Reasons,
            string Residentialaddress, string Telephone, string Email, string Postalcode, string VideoDiscription,
            List<int> DeleteDocument, string DetailedCompanyPro, string Logoimage);

        string Withdrawloan(long LoanRequestID, out string statusCategory);
        string ChangeLoanRateByRequestID(long RequestID, string Password, decimal? NewRate);
        void AcceptorRejectLoanOffer(string LoanOfferIds, bool Flag);
        bool SetLoanStatustoApproved(int LoanRequestID);

        IList<LoanRequest> GetCurrentRequests();

        IEnumerable<LoanRequest> GetLoanRequestbyUser();

    }
    public class IssuerService : IIssuerService
    {
        private readonly IAccountDetailService _iAccountDetailService;
        private readonly ICurrentUser _currentUser;
        private readonly ISessionService _iSessionService;
        private readonly ILoanRequestService _loanRequestService;
        private readonly IMoolahCoreService _moolahCoreService;

        public IssuerService(IAccountDetailService iAccountDetailService,ICurrentUser currentUser,ISessionService iSessionService,
            ILoanRequestService loanRequestService,IMoolahCoreService moolahCoreService)
        {
            _iAccountDetailService = iAccountDetailService;
            _currentUser = currentUser;
            _iSessionService = iSessionService;
            _loanRequestService = loanRequestService;
            _moolahCoreService = moolahCoreService;
        }

        public string BorrowerIndexLoad()
        {
            string controller = string.Empty;
            try
            {
               if (_currentUser.ApplicationUser.AdminVerification != "Approved")
                {
                    if (_currentUser.ApplicationUser.IsSubmitted != null && (bool)_currentUser.ApplicationUser.IsSubmitted)
                    {
                        controller = "SubmittedBorrower";
                    }
                    else
                    {
                      _iSessionService.Set("step", 1);
                        controller = "Borrower";
                    }
                }

                else
                {
                    controller = "";

                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                controller = string.Empty;
            }

            return controller;
        }

        public LoanRequest GetTermsheetDetails(long loanId)
        {
            throw new NotImplementedException();
        }

        public AccountDetail FillBorrowerDashboard()
        {
            return _iAccountDetailService.GetByUser(_currentUser.ApplicationUser.UserID);
        }

        public AccountDetail GetCompayProfile()
        {
            return _iAccountDetailService.GetByUser(_currentUser.ApplicationUser.UserID);
        }

        public AccountDetail GetmodelforgeneratePdf()
        {
            throw new NotImplementedException();
        }

        public AccountDetail CreateLoad()
        {
            throw new NotImplementedException();
        }

        public LoanRequest LoanrequestLoad()
        {
            throw new NotImplementedException();
        }

        public string CreateBorrower(AccountDetail accountDetail)
        {
            throw new NotImplementedException();
        }

        public void UpdateLoadLoanRequestDescription(LoanRequest loanRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateLoadLoanRequestMoolahcore(LoanRequest loanRequest)
        {
            throw new NotImplementedException();
        }

        public void UpdateLoadLoanRequestMoolahperi(LoanRequest loanRequest)
        {
            throw new NotImplementedException();
        }

        public string SaveLoanrequest(LoanRequest loanRequest)
        {
            
            loanRequest.EIR = (decimal)LoanRequestCalc.CalculateTargetEIR((double)loanRequest.Amount.Value, loanRequest.Terms, (double)loanRequest.Rate);
          _loanRequestService.Update(loanRequest,a=>a.Headline,a=> a.Amount,a=> a.Rate,a=> a.Terms,
                a=> a.EIR,a=> a.IsPersonalGuarantee,a=> a.LoanPurpose_Id,a=> a.LoanPurposeDescription,
                a=> a.SummaryCompanyPro,a=> a.DetailedCompanyPro,a=> a.NumberOfEmployees
            );
            return "Success";
        }

       

        public string SaveLoanrequestmoolahcore(MoolahCore moolahCore)
        {
          
            var request_id = _iSessionService.Get<long>("LoanrequestId");
            moolahCore.LoanRequest_Id = request_id;
            _moolahCoreService.CreateOrUpdate(moolahCore);
            
 

               

            return "id" + "Â" + "Success";
        }

        public string SaveLoanrequestmoolahPeri(LoanRequest loanRequest)
        {
            throw new NotImplementedException();
        }

        public string SaveLoanrequest(decimal Amount, string Terms, decimal Rate, decimal EIR, bool Ispersonalguarantee,
            int LoanpurposeID, string XMLquestionswithanswers, string Loanimages, string LoanDocs, string NameasinNRICPassport,
            string Designation, string NRICPassport, string Reasons, string Residentialaddress, string Telephone, string Email,
            string Postalcode, string VideoDiscription, List<int> DeleteDocument, string DetailedCompanyPro, string Logoimage)
        {
            throw new NotImplementedException();
        }

        public string Withdrawloan(long LoanRequestID, out string statusCategory)
        {
            throw new NotImplementedException();
        }

        public string ChangeLoanRateByRequestID(long RequestID, string Password, decimal? NewRate)
        {
            throw new NotImplementedException();
        }

        public void AcceptorRejectLoanOffer(string LoanOfferIds, bool Flag)
        {
            throw new NotImplementedException();
        }

        public bool SetLoanStatustoApproved(int LoanRequestID)
        {
            throw new NotImplementedException();
        }

        public IList<LoanRequest> GetCurrentRequests()
        {
            throw new NotImplementedException();
        }

        public IEnumerable<LoanRequest> GetLoanRequestbyUser()
        {
          return  _loanRequestService.GetLoanRequestbyUserID(_currentUser.ApplicationUser.UserID)
                .OrderByDescending(l => l.RequestId);

        }
    }
}
