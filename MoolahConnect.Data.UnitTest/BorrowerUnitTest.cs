﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using MoolahConnect.Data.Databases;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Models.BusinessModels;
using Moq;
using FluentAssertions;

namespace MoolahConnect.Data.UnitTest
{
    [TestClass]
    public class BorrowerUnitTest
    {
        [TestMethod]
        public void CreateLoanRequest_saves_a_loanRequest_via_context()
        {
            var data = new List<LoanRequest>();

            var mockLoanRequestSet = new Mock<DbSet<LoanRequest>>();
            var mockContext = new Mock<MoolahConnectDbContext>();
            mockContext.Setup(a => a.LoanRequests).Returns(mockLoanRequestSet.Object);

            var mockDbFactory = new Mock<IDatabaseFactory>();
            mockDbFactory.Setup(m => m.GetContext()).Returns(mockContext.Object);
            var mockLoanRequestRepository = new Mock<LoanRequestRepository>(mockDbFactory.Object);
            mockLoanRequestRepository.Setup(a => a.Create(It.IsAny<LoanRequest>())).Callback((LoanRequest req) => data.Add(req));
            var loanrequest = new LoanRequest();
            loanrequest.Amount = 10000;
            loanrequest.Rate = 5.2M;
            loanrequest.Terms = "Test term";
            var repository = mockLoanRequestRepository.Object;
            repository.Create(loanrequest);

            data.Count.Should().Be(1);
            var dataitem = data.First();
            dataitem.Amount.Should().Be(loanrequest.Amount);
            dataitem.Terms.Should().Be(loanrequest.Terms);
            dataitem.Rate.Should().Be(loanrequest.Rate);




        }

        [TestMethod]
        public void UpdateLoanRequest_saves_a_loanRequest_via_context()
        {
            var data = new List<LoanRequest>
            {
                new LoanRequest {RequestId = 1, Amount = 2000, Terms = "Anually", Rate = 5.3M}
            };

            var mockLoanRequestSet = new Mock<DbSet<LoanRequest>>();
            var mockContext = new Mock<MoolahConnectDbContext>();
            mockContext.Setup(a => a.LoanRequests).Returns(mockLoanRequestSet.Object);

            var mockDbFactory = new Mock<IDatabaseFactory>();
            mockDbFactory.Setup(m => m.GetContext()).Returns(mockContext.Object);
            var mockLoanRequestRepository = new Mock<LoanRequestRepository>(mockDbFactory.Object);

            mockLoanRequestRepository.Setup(a => a.Update(It.IsAny<LoanRequest>())).Callback((LoanRequest req,object obj) =>
            {
                var finditem = data.First(a => a.RequestId == req.RequestId);
                if (finditem != null)
                {
                    finditem.Amount = req.Amount;
                    finditem.Terms = req.Terms;
                    finditem.Rate = req.Rate;
                }
            });

            var loanrequest = new LoanRequest();
            loanrequest.RequestId = 1;
            loanrequest.Amount = 10000;
            loanrequest.Rate = 5.2M;
            loanrequest.Terms = "Test term";
            var repository = mockLoanRequestRepository.Object;
            repository.Update(loanrequest);
           
            var dataitem = data.First();
            dataitem.Amount.Should().Be(loanrequest.Amount);
            dataitem.Terms.Should().Be(loanrequest.Terms);
            dataitem.Rate.Should().Be(loanrequest.Rate);




        }

    }
}
