﻿using System;
using System.Collections.Generic;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Tasks.Interfaces
{
    public interface ILoanPaymentsTask
    {
        IEnumerable<RepaymentEntity> GetRepaymentSchedule(long? requestID, double amount, double rate, string Tenor, DateTime startDate, bool IsInvestor = false);
        double GetMonthlyInstallment(double amount, double rate, string tenor);
        bool AddLoanPayment(long requestId, int repaymentId, double amount, DateTime paidDate, bool repaymentPaid, bool accruedPaid, bool lateFeesPaid,
            double? accrued = null, double? lateFees = null);
        IEnumerable<tbl_LoanPayments> GetLoanPayments(int timePeriod);

        IEnumerable<tbl_AccountAdjustment> GetAccountAdjustments(int timePeriod);

        IEnumerable<tbl_RepaymentMessages> GetRepaymentMessageses(long requestId);
    }
}
