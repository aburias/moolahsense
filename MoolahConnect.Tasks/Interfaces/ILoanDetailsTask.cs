﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Tasks.Interfaces
{
    public interface ILoanDetailsTask
    {
        IEnumerable<LoanEntity> GetLoans(int timePeriod,LoanRequestStatus loanRequestStatus);
        IEnumerable<LoanRepaymentEntity> GetLoanRepayments(int timePeriod);
        IEnumerable<LoanRepaymentEntity> GetLoanRepaymentsByStatus(int timePeriod,RepaymentPayStatus status);
        IEnumerable<LoanRepaymentEntity> GetLateLoanRepayments(int timePeriod);
        IEnumerable<LoanInvestorEntity> GetLoanInvestors(long requestId,long repId);

        void NotifyApprovedInvestorsAndBorrowerOnTargetRateChanged(long requestID, double oldRate, double newRate);
        void NotifyApprovedInvestorsAndBorrowerOnLoanPublished(long requestID);
        void SendEmailsToBorrowerAndInvestedInvestorsOnAccpted(long requestID);

        bool ExpireLoanRequests(long requestID);
    }
}
