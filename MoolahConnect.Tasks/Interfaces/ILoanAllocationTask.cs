﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MoolahConnect.Tasks.Interfaces
{
    public interface ILoanAllocationTask
    {
        bool Allocate(long loanRequestId, out string status, out string statusCategory, out decimal outFinalRate, bool isPreview = false);
        int[] TimeLetfToExpireLoanRequest(DateTime approvedDate);
        int[] TimeLetfToExpireLoanDraft(DateTime approvedDate);
        bool AcceptLoanByAdmin(long requestID, DateTime transferDate, decimal moolahFee, string adminUser);
    }
}
