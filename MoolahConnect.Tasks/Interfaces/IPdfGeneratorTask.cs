﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MoolahConnect.Tasks.Interfaces
{
    public interface IPdfGeneratorTask
    {
        MemoryStream GenerateBorrowerLoanContract(long loanRequestId);
        MemoryStream GenerateInvestorLoanContract(long loanRequestId, int offerId, string contractIdentifier, int acceptedOffersCount);
    }
}
