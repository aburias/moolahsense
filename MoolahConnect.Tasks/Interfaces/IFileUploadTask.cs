﻿using Microsoft.WindowsAzure.Storage.Blob;
using MoolahConnect.Tasks.Implementation;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MoolahConnect.Tasks.Interfaces
{
    public interface IFileUploadTask
    {
        bool Upload(string userId, string type, string fileName, Stream fileStream,string requestId = null);
        bool Upload(string userId, string type, string fileName, byte[] fileStreamArray, int index, int length, string requestId = null, string offerId = null);
        bool Delete(string userId, string type, string fileName,string requestId = null);
        DownloadedFile Download(string userId, string type, string fileName, string requestId = null, string offerId = null);
        DownloadedFile Download(string filePath);
        string GetFileUrl(string userId, string type, string fileName, string requestId = null);
        string GetImageViewUrl(string userId, string type, string fileName, string requestId = null);
    }
}
