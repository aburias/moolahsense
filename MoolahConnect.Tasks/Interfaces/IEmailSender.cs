﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using MoolahConnect.Util.Enums;
using MoolahConnect.Services.Entities;

namespace MoolahConnect.Tasks.Interfaces
{
    public interface IEmailSender
    {
        bool OnAcceptedLoanRequestToBorrower(string emailAddress,long loanReference);
        bool OnAcceptedLoanOfferToInvetor(string emailAddress,long requestId,int offerId,string contractIdentifier, int acceptedOffersCount);

        bool OnRejectedLoanOfferToInvetor(string emailAddress,  string loanReference,double amount,
            double rate,DateTime createdDate, string businessName, string reason);

        bool OnUserVerificationChanged(string emailAddress, string status, bool isBorrower);
        bool OnAccountUnlocked(string emailAddress);
        bool OnApprovedLoanRequest(string emailAddress);
        bool OnRejectedLoanRequest(string emailAddress, string comments);
        bool EmailConfirmation(string Username, string url,UserRole role);
        bool EmailUpdateConfirmation(string username, string url, UserRole role);
        bool OnLoanCompletionToBorrower(string LoanReference, string UserName);
        bool OnWithdrawalRejectionToInvestor(string Email, string comments);
        bool OnResetPassword(string Emailid, string url);
        bool OnResetSecurityQuestion(string Emailid, string url);
        bool OnLoanRequestWithdrawn(string email, string loanRequestId);
        bool SendRejectFundTransferEmail(IEnumerable<UserTemp> users, long loanId, string reason);

        bool onNoteAcceptedByAdminToBorrower(string emailAddress, long loanReference);
        bool onNoteAcceptedByAdminToInvestor(string emailAddress, long loanReference);

        

        bool FundTransferIn(string email, double amount, double balance);
        bool FundTransferOut(string email, double amount, double balance);

        bool InvestorOfferMade(string email, string companyName, double amount, double rate);
        bool BorrowerRateChanged(string email, double preRate, double newRate);
        bool InvestorRateChanged(string email, string companyName, double preRate, double newRate);

        bool OnLoanListingExpired(string email, string loanRequestId);
        bool onLoanOfferCancelledDueToLoanRequestExpire(string email, string loanRequestId);

        bool InvestorOfferWithdrawn(string email, string companyName, double amount, double rate);

        bool BorrowerLoanPublished(string email, double amount, double rate);
        bool InvestorLoanPublished(string email,string name, double amount, double rate);

        #region Admin

        bool OnUserSubmittedRegistration(string emailAddress, string accountID);
        bool AdminUserCreatedFundTransferRequest(string emailAddress, string accountID, double amount, double blanace);

        #endregion

        bool UserSubmittedRegistration(string email);

        bool BorrowerRepaymentStatusChanged(string email, RepaymentPayStatus status, DateTime date, double amount, int monthsTillMat);
        bool InvestorRepaymentStatusChanged(string email, string issuerName, RepaymentPayStatus status, DateTime date, double amount, int monthsTillMat);
    }
}
