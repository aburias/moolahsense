﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualBasic;
using MoolahConnect.Services.Entities;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Util;
using MoolahConnect.Util.Logging;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Util.Enums;

namespace MoolahConnect.Tasks.Implementation
{
    public class LoanPaymentsTask : ILoanPaymentsTask
    {
        private ITransactionsDA _ITransactionsDA;
        private ILoanDetailsDA _ILoanDetailsDA;

        public LoanPaymentsTask()
        {
            _ITransactionsDA = new TransactionsDA();
            _ILoanDetailsDA = new LoanDetailsDA();
        }

        public IEnumerable<RepaymentEntity> GetRepaymentSchedule(long? requestID, double amount, double rate, string Tenor,
                                                                 DateTime startDate, bool IsInvestor = false)
        {
            try
            {
                var repaymentSchedule = new List<RepaymentEntity>();
                bool noRepayments = false;
                tbl_LoanRequests loanRequest = null;

                if (requestID.HasValue)
                {
                    loanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestID.Value);

                    if (loanRequest.tbl_LoanAmortization.Count <= 0)
                    {
                        noRepayments = true;
                    }
                }
                else
                {
                    noRepayments = true;
                }

                //if (!noRepayments && loanRequest != null && loanRequest.tbl_LoanAmortization.Count() > 0)
                if (IsInvestor || noRepayments || loanRequest == null)
                {

                    double monthlyRepayment = 0;
                    double periodicInterestRate = 0;
                    int tenorinYears = 0;
                    int totalNoOfPayments = 0;

                    double preEndBalance = 0;


                    tenorinYears = int.Parse(Tenor) / 12;
                    totalNoOfPayments = tenorinYears * Settings.PaymentsPerYear;
                    periodicInterestRate = rate / Settings.PaymentsPerYear;

                    monthlyRepayment = Financial.Pmt(periodicInterestRate / 100, totalNoOfPayments, amount * -1);

                    preEndBalance = amount;
                    for (int i = 1; i <= totalNoOfPayments; i++)
                    {
                        double interest = preEndBalance * ((rate / 100) / Settings.PaymentsPerYear);
                        double principal = monthlyRepayment - interest;
                        double endBalance = preEndBalance - principal;
                        DateTime date = startDate.AddMonths(i);

                        if ((int)endBalance <= 0)
                            endBalance = 0;

                        repaymentSchedule.Add(
                            new RepaymentEntity()
                                {
                                    DueDate = date,
                                    StartBalance = Math.Round(preEndBalance, 2),
                                    Interest = Math.Round(interest, 2),
                                    Payment = monthlyRepayment,
                                    Principal = Math.Round(principal, 2),
                                    EndBalance = Math.Round(endBalance, 2),
                                    Status = (int)RepaymentPayStatus.Pending,
                                });

                        preEndBalance = endBalance;
                    }
                }
                else
                {
                    repaymentSchedule = _ILoanDetailsDA.GetRepaymentsByRequestID(requestID.Value).ToList();
                }

                if (IsInvestor)
                {
                    var tempSchedule = _ILoanDetailsDA.GetRepaymentsByRequestID(requestID.Value).ToList();

                    for (int i = 0; i < tempSchedule.Count; i++)
                    {
                        repaymentSchedule[i].Status = tempSchedule[i].Status;
                    }
                }


                return repaymentSchedule;

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<RepaymentEntity>();
            }
        }

        public double GetMonthlyInstallment(double amount, double rate, string tenor)
        {
            var tenorinYears = int.Parse(tenor) / 12;
            var totalNoOfPayments = tenorinYears * Settings.PaymentsPerYear;
            var periodicInterestRate = rate / Settings.PaymentsPerYear;
            var monthlyRepayment = Financial.Pmt(periodicInterestRate / 100, totalNoOfPayments, amount * -1);
            return monthlyRepayment;
        }


        public bool AddLoanPayment(long requestId, int repaymentId, double amount, DateTime paidDate, bool repaymentPaid, bool accruedPaid, bool lateFeesPaid,
            double? accrued = null, double? lateFees = null)
        {
            if (!_ITransactionsDA.UpdatePaidAmount(requestId, repaymentId, amount, paidDate, repaymentPaid, accruedPaid, lateFeesPaid, accrued, lateFees))
            {
                return false;
            }

            if (!_ITransactionsDA.AddLoanPayment(requestId, repaymentId, amount, paidDate, lateFeesPaid ? lateFees : null))
            {
                return false;
            }

            var repaymentAmount = amount;
            var loanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestId);

            if (lateFeesPaid)
            {
                _ITransactionsDA.LoanTransactionDebit((int)loanRequest.tbl_Users.UserID,
                            (decimal)lateFees, requestId, "Interent", "Late Fees for Late Monthly Repayment");
                _ITransactionsDA.InternalTransactionCredit(Settings.MsenseAccountBalanceID, (decimal)lateFees, requestId, "Interent",
                    "Late Fees for Late Monthly Repayment", (int)loanRequest.tbl_Users.UserID);
                repaymentAmount -= lateFees.Value;
            }

            if (accruedPaid)
            {
                _ITransactionsDA.LoanTransactionDebit((int)loanRequest.tbl_Users.UserID,
                            (decimal)accrued, requestId, "Interent", "Accured Interest From Late Monthly Repayment");
                repaymentAmount -= accrued.Value;
            }

            double loanRepAmount = loanRequest.tbl_LoanAmortization.First() != null ? (double)loanRequest.tbl_LoanAmortization.First().EmiAmount.Value : 0;

            string transactionRef = repaymentPaid && amount == loanRepAmount ? "Full Monthly Repayment" : "Partial Monthly Repayment";

            _ITransactionsDA.LoanTransactionDebit((int)loanRequest.tbl_Users.UserID,
                        (decimal)repaymentAmount, requestId, "Interent", transactionRef);

            var repayment = _ILoanDetailsDA.GetRepaymentByID(repaymentId);
            return true;
        }


        public IEnumerable<tbl_LoanPayments> GetLoanPayments(int timePeriod)
        {
            try
            {
                DateTime? to = null;
                DateTime? from = null;

                DateTime thisMonthFirstDate = DateTime.Now.AddDays((-1) * DateTime.Now.Day);
                to = DateTime.Now;

                switch ((TimePeriodFilterEnum)timePeriod)
                {
                    case TimePeriodFilterEnum.All:
                        to = null;
                        break;
                    case TimePeriodFilterEnum.Current:
                        from = thisMonthFirstDate;
                        break;
                    case TimePeriodFilterEnum.LAST3:
                        from = DateTime.Now.AddMonths(-3);
                        break;
                    case TimePeriodFilterEnum.LAST6:
                        from = DateTime.Now.AddMonths(-6);
                        break;
                    case TimePeriodFilterEnum.LAST9:
                        from = DateTime.Now.AddMonths(-9);
                        break;
                    case TimePeriodFilterEnum.LAST12:
                        from = DateTime.Now.AddMonths(-12);
                        break;
                    default:
                        break;
                }

                return _ITransactionsDA.GetLoanPayments(from, to);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_LoanPayments>();
            }
        }


        public IEnumerable<tbl_AccountAdjustment> GetAccountAdjustments(int timePeriod)
        {
            try
            {
                DateTime? to = null;
                DateTime? from = null;

                DateTime thisMonthFirstDate = DateTime.Now.AddDays((-1) * DateTime.Now.Day);
                to = DateTime.Now;

                switch ((TimePeriodFilterEnum)timePeriod)
                {
                    case TimePeriodFilterEnum.All:
                        to = null;
                        break;
                    case TimePeriodFilterEnum.Current:
                        from = thisMonthFirstDate;
                        break;
                    case TimePeriodFilterEnum.LAST3:
                        from = DateTime.Now.AddMonths(-3);
                        break;
                    case TimePeriodFilterEnum.LAST6:
                        from = DateTime.Now.AddMonths(-6);
                        break;
                    case TimePeriodFilterEnum.LAST9:
                        from = DateTime.Now.AddMonths(-9);
                        break;
                    case TimePeriodFilterEnum.LAST12:
                        from = DateTime.Now.AddMonths(-12);
                        break;
                    default:
                        break;
                }

                return _ITransactionsDA.GetAccountAdjustments(from, to);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<tbl_AccountAdjustment>();
            }
        }

        public IEnumerable<tbl_RepaymentMessages> GetRepaymentMessageses(long requestId)
        {
            return _ILoanDetailsDA.GetRepaymentMessageses(requestId);
        }
    }
}