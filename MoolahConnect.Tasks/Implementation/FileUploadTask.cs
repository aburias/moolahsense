﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Tasks.Interfaces;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Blob;
using System.Configuration;
using MoolahConnect.Util;
using MoolahConnect.Util.Logging;
using System.IO;
using System.Web;

namespace MoolahConnect.Tasks.Implementation
{
    public class FileUploadTask : IFileUploadTask
    {
        static CloudStorageAccount storageAccount = CloudStorageAccount.Parse(Settings.AzureStorageConnectionString);
        static CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();



        public bool Upload(string userId, string type, string fileName, System.IO.Stream fileStream, string requestId = null)
        {
            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(userId);
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("{0}{1}/{2}",
                    type,
                    string.IsNullOrEmpty(requestId) ? string.Empty : "/" + requestId,
                    fileName));

                blockBlob.UploadFromStream(fileStream);

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool Upload(string userId, string type, string fileName, byte[] fileStreamArray,int index,int length, string requestId = null, string offerId = null)
        {
            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(userId);
                container.CreateIfNotExists();
                container.SetPermissions(new BlobContainerPermissions { PublicAccess = BlobContainerPublicAccessType.Blob });

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("{0}{1}{2}/{3}",
                    type,
                    string.IsNullOrEmpty(requestId) ? string.Empty : "/" + requestId,
                    string.IsNullOrEmpty(offerId) ? string.Empty : "/" + offerId,
                    fileName));

                blockBlob.UploadFromByteArray(fileStreamArray, index, length);


                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public bool Delete(string userId, string type, string fileName, string requestId = null)
        {
            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(userId);
                container.CreateIfNotExists();

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("{0}{1}/{2}",
                    type,
                    string.IsNullOrEmpty(requestId) ? string.Empty : "/" + requestId,
                    fileName));

                blockBlob.Delete();

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public DownloadedFile Download(string userId, string type, string fileName, string requestId = null, string offerId = null)
        {
            var file = new DownloadedFile();

            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(userId);
                container.CreateIfNotExists();

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("{0}{1}{2}/{3}",
                    type,
                    string.IsNullOrEmpty(requestId) ? string.Empty : "/" + requestId,
                    string.IsNullOrEmpty(offerId) ? string.Empty : "/" + offerId,
                    fileName));

                var stream = new MemoryStream();

                blockBlob.DownloadToStream(stream);
                file.FileStream = stream.ToArray();

                file.ContentType = blockBlob.Properties.ContentType;

                return file;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public string GetFileUrl(string userId, string type, string fileName, string requestId = null)
        {
            var url = string.Empty;

            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(userId);

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("{0}{1}/{2}",
                    type,
                    string.IsNullOrEmpty(requestId) ? string.Empty : "/" + requestId,
                    fileName));

                url = String.Format("http://{0}{1}", blockBlob.Uri.DnsSafeHost, blockBlob.Uri.AbsolutePath);

                return url;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }

        public string GetImageViewUrl(string userId, string type, string fileName, string requestId = null)
        {
            var url = string.Empty;

            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference(userId);

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(string.Format("{0}{1}/{2}",
                    type,
                    string.IsNullOrEmpty(requestId) ? string.Empty : "/" + requestId,
                    fileName));

                //url = String.Format("http://{0}{1}", blockBlob.Uri.DnsSafeHost, blockBlob.Uri.AbsolutePath);
                url = blockBlob.Uri.AbsoluteUri;

                return url;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }


        public DownloadedFile Download(string filePath)
        {
            var file = new DownloadedFile();

            try
            {
                CloudBlobContainer container = blobClient.GetContainerReference("moolahsense");
                container.CreateIfNotExists();

                CloudBlockBlob blockBlob = container.GetBlockBlobReference(filePath);

                var stream = new MemoryStream();

                blockBlob.DownloadToStream(stream);
                file.FileStream = stream.ToArray();

                file.ContentType = blockBlob.Properties.ContentType;

                return file;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }
    }

    public class DownloadedFile
    {
        public byte[] FileStream { get; set; }
        public string ContentType { get; set; }
    }
}
