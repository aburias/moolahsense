﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util;
using iTextSharp.text.pdf.draw;
using MoolahConnect.Util.PdfGenerations;
using System.Web;

namespace MoolahConnect.Tasks.Implementation
{
    public class PdfGeneratorTask : IPdfGeneratorTask
    {
        private IFileUploadDA _IFileUploadDA;
        private IFileUploadTask _IFileUploadTask;
        private ILoanAllocationDA _ILoanAllocationDA;
        private ILoanPaymentsTask _ILoanPaymentsTask;
        private Font tableFonttd = FontFactory.GetFont("Calibri", 11, Font.NORMAL);
        private Font tableFontth = FontFactory.GetFont("Calibri", 11, Font.BOLD);

        public PdfGeneratorTask()
        {
            _IFileUploadTask = new FileUploadTask();
            _ILoanAllocationDA = new LoanAllocationDA();
            _ILoanPaymentsTask = new LoanPaymentsTask();
            _IFileUploadDA = new FileUploadDA();
        }

        public MemoryStream GenerateBorrowerLoanContract(long loanRequestId)
        {

            try
            {
                var titleP = new Paragraph("THIS IS AN IMPORTANT LEGAL DOCUMENT \n",
                    FontFactory.GetFont("Calibri", 11, Font.BOLD, BaseColor.LIGHT_GRAY));
                titleP.Alignment = Element.ALIGN_CENTER;

                var titleP2 = new Paragraph("Please read this Note carefully.  You are advised to fully understand your obligations and risks under this Note.  If you have any concerns with this Note, consult a lawyer before signing this Note. \n",
                    FontFactory.GetFont("Calibri", 10, Font.BOLDITALIC, BaseColor.LIGHT_GRAY));
                titleP2.Alignment = Element.ALIGN_CENTER;

                var detailsP = new Paragraph("For value received, the Issuer promises to pay to each of the Payees, in Singapore dollars, the relevant Principal Subscription Amount and Interest as specified in this Promissory Note, on such Repayment Date and in accordance with the schedule specified in this Promissory Note. \n\n",
                    FontFactory.GetFont("Calibri", 10, Font.BOLD));
                detailsP.Alignment = Element.ALIGN_CENTER;

                var borrowerLoanContract = _ILoanAllocationDA.GetBorrowerLoanContractModel(loanRequestId);
                var repaymentSchedule = _ILoanPaymentsTask.GetRepaymentSchedule(loanRequestId, (double)borrowerLoanContract.Amount, (double)borrowerLoanContract.AcceptedRate,
                    borrowerLoanContract.Tenure, borrowerLoanContract.AgreementDate);

                PdfPTable table = new PdfPTable(2);
                float[] tableWiths = new float[2] { 60, 40 };
                table.SetWidthPercentage(tableWiths, new Rectangle(100, 100));

                table.AddCell(new Phrase("Promissory Note Request ID", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.LoanRequestId.ToString(), tableFonttd));
                table.AddCell(new Phrase("Name of Issuer", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.Name, tableFonttd));
                table.AddCell(new Phrase("Registration Number of Issuer", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.RegistrationNumber, tableFonttd));
                table.AddCell(new Phrase("Date of Promissory Note", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.AgreementDate.ToDate(), tableFonttd));
                table.AddCell(new Phrase("Aggregate Principal Subscription Amount ", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.Amount.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Maturity Period (not exceeding 12 months)", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.Tenure, tableFonttd));
                table.AddCell(new Phrase("Number of Accepted Offers", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.NumberOfAcceptedOffers.ToString(), tableFonttd));
                table.AddCell(new Phrase("Accepted Rate (p.a.)", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.AcceptedRate.ToString("G29") + " %", tableFonttd));
                //table.AddCell(new Phrase("Effective Rate (p.a.)", tableFonttd));
                //table.AddCell(new Phrase(borrowerLoanContract.EffectiveRate.ToString("G29") + " %", tableFonttd));
                table.AddCell(new Phrase("Total Interests Payable", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.TotalInterestPayable.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Repayment Amount at each Repayment Date", tableFonttd));
                table.AddCell(new Phrase(repaymentSchedule.First().Payment.ToCurrency(), tableFonttd));

                var notice = new Paragraph("\nPlease note that your actual interests paid can appear to be off by a few cents when compared to a manual calculation of interests. This is because we round off the monthly repayment to the nearest 2 decimals which sometimes contain a factor of a cent. \n",
                    FontFactory.GetFont("Calibri", 10, Font.BOLD));
                notice.Alignment = Element.ALIGN_LEFT;

                Phrase titleLendersPhrase = new Phrase();
                titleLendersPhrase.Add(new Chunk(new LineSeparator(1f, 9.5f, BaseColor.BLACK, Element.ALIGN_CENTER, -20)));
                titleLendersPhrase.Add(new Chunk("\nPayee(s)\n\n", FontFactory.GetFont("Calibri", 11, Font.BOLD)));

                var titleLenders = new Paragraph(titleLendersPhrase);
                titleLenders.Alignment = Element.ALIGN_CENTER;

                PdfPTable tableLenders = new PdfPTable(6);
                float[] tableLendersWiths = new float[6] { 15, 22, 19, 15, 14, 15 };
                tableLenders.SetWidthPercentage(tableLendersWiths, new Rectangle(100, 100));

                tableLenders.AddCell(new PdfPCell(new Phrase("Payee Reference No", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Name of Payee", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Identification Number", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Principal Subscription Amount", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Repayment Dates", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Amount Payable By Issuer on Each Repayment Date", tableFontth)));

                int index = 0;
                foreach (Lender lender in borrowerLoanContract.AcceptedOffers)
                {
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.ContractIdentifier, tableFonttd)));
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.Name, tableFonttd)));
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.IdNumber, tableFonttd)));
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.Amount.ToCurrency(), tableFonttd)));

                    if (index == 0)
                    {
                        PdfPCell repCell = new PdfPCell(
                            new Phrase(String.Join(", ", repaymentSchedule.Select(x => x.DueDate.ToDate())), FontFactory.GetFont("Calibri", 11, Font.NORMAL)));
                        repCell.Rowspan = borrowerLoanContract.NumberOfAcceptedOffers;
                        tableLenders.AddCell(repCell);
                    }

                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.RepaymentAmount.ToCurrency(), tableFonttd)));

                    index++;
                }

                var line = new Paragraph("\n\n This Note is issued subject to the “Standard Terms and Conditions To Promissory Note”. ",
                    FontFactory.GetFont("Calibri", 11, Font.BOLD));
                line.Alignment = Element.ALIGN_CENTER;

                Phrase line2Phrase = new Phrase();
                line2Phrase.Add(new Chunk(new LineSeparator(1f, 26f, BaseColor.BLACK, Element.ALIGN_LEFT, -20)));
                line2Phrase.Add(new Chunk("\n Issued and Endorsed by: \n\n", FontFactory.GetFont("Calibri", 11, Font.BOLD)));

                var line2 = new Paragraph(line2Phrase);
                line2.Alignment = Element.ALIGN_LEFT;

                PdfPTable table2 = new PdfPTable(3);
                float[] table2Withs = new float[3] { 25, 5, 70 };
                table2.SetWidthPercentage(table2Withs, new Rectangle(100, 100));
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.DefaultCell.FixedHeight = 30;
                table2.AddCell(new Phrase("Name of Issuer", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));
                table2.AddCell(new Phrase(borrowerLoanContract.Name.ToString(), tableFonttd));
                table2.AddCell(new Phrase("Registration Number", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));
                table2.AddCell(new Phrase(borrowerLoanContract.RegistrationNumber.ToString(), tableFonttd));
                table2.AddCell(new Phrase("Registered Address", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));
                table2.AddCell(new Phrase(
                    (borrowerLoanContract.RegisteredAddress != null ? borrowerLoanContract.RegisteredAddress.ToString() : string.Empty)
                    , tableFonttd));
                table2.AddCell(new Phrase("Name of Director/Partner", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));
                table2.AddCell(new Phrase(
                    (borrowerLoanContract.DirectorPartnetName != null ? borrowerLoanContract.DirectorPartnetName.ToString() : string.Empty)
                    , tableFonttd));
                table2.AddCell(new Phrase("Signature", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));

                var line3 = new Paragraph("\n\n For and on behalf of",
                    FontFactory.GetFont("Calibri", 11, Font.NORMAL));
                line3.Alignment = Element.ALIGN_LEFT;

                var line4 = new Paragraph("\n The Issuer",
                    FontFactory.GetFont("Calibri", 11, Font.BOLD));
                line4.Alignment = Element.ALIGN_LEFT;

                PdfReader template = new PdfReader(
                   HttpContext.Current.Server.MapPath("/Content/Template/Pdf/TermsConditions.pdf"));
                Rectangle size = template.GetPageSizeWithRotation(1);
                Document myDocument = new Document(size);

                var myMemoryStream = new MemoryStream();

                PdfWriter myPDFWriter = PdfWriter.GetInstance(myDocument, myMemoryStream);
                //PdfWriter myPDFWriter = PdfWriter.GetInstance(myDocument,
                //    new FileStream("C://Moolah//PDF//BorrowerContract.pdf", FileMode.Create));

                myDocument.Open();
                myPDFWriter.PageEvent = new PdfPageEvents();

                PdfContentByte cb = myPDFWriter.DirectContent;

                myDocument.Add(titleP);
                myDocument.Add(titleP2);
                myDocument.Add(detailsP);
                myDocument.Add(table);
                myDocument.Add(notice);
                myDocument.Add(titleLenders);
                myDocument.Add(tableLenders);
                myDocument.Add(line);
                myDocument.NewPage();
                myDocument.Add(line2);
                myDocument.Add(table2);
                myDocument.Add(line3);
                myDocument.Add(line4);

                myDocument.NewPage();

                // create the new page and add it to the pdf
                PdfImportedPage page1 = myPDFWriter.GetImportedPage(template, 1);
                cb.AddTemplate(page1, 0, 0);

                myDocument.NewPage();

                PdfImportedPage page2 = myPDFWriter.GetImportedPage(template, 2);
                cb.AddTemplate(page2, 0, 0);

                myDocument.NewPage();

                PdfImportedPage page3 = myPDFWriter.GetImportedPage(template, 3);
                cb.AddTemplate(page3, 0, 0);


                myPDFWriter.CloseStream = false;
                myDocument.Close();
                byte[] content = myMemoryStream.ToArray();

                _IFileUploadDA.InsertUploadedFile("Note Contract.pdf", FileType.IssuerLoanContract, borrowerLoanContract.UserId, loanRequestId);


                _IFileUploadTask.Upload(borrowerLoanContract.UserId.ToString(),
                FileType.IssuerLoanContract.ToString(),
                "Note Contract.pdf", content, 0, (int)content.Length, loanRequestId.ToString());

                myMemoryStream.Position = 0;

                //Generate Gurantor Contract
                GenerateGurantorContract(borrowerLoanContract, repaymentSchedule);

                return myMemoryStream;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new MemoryStream();
            }

        }

        private void GenerateGurantorContract(BorrowerLoanContractModel borrowerLoanContract, IEnumerable<RepaymentEntity> repaymentSchedule)
        {

            try
            {

                var detailsP = new Paragraph("The Guarantor (named below) grants in favour of the Payees (named below) this deed of guarantee (“Guarantee”) to guarantee the repayment of all sums which are due and payable by the Issuer to one or more Payees pursuant to the promissory note (“Promissory Note”) issued by the Issuer to one or more Payees (each named below), on the terms (including the attached Standard Terms and Conditions) set out in this Guarantee. \n\n",
                    FontFactory.GetFont("Calibri", 10, Font.BOLD));
                detailsP.Alignment = Element.ALIGN_CENTER;

                Phrase line2Phrase = new Phrase();
                line2Phrase.Add(new Chunk(new LineSeparator(1f, 16f, BaseColor.BLACK, Element.ALIGN_LEFT, -20)));
                line2Phrase.Add(new Chunk("\n The Guarantor \n\n", FontFactory.GetFont("Calibri", 11, Font.BOLD)));

                var line2 = new Paragraph(line2Phrase);
                line2.Alignment = Element.ALIGN_LEFT;

                PdfPTable table2 = new PdfPTable(2);
                table2.DefaultCell.Border = Rectangle.NO_BORDER;
                table2.DefaultCell.FixedHeight = 30;
                table2.AddCell(new Phrase("Name of Guarantor", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));
                table2.AddCell(new Phrase("Identification number", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));
                table2.AddCell(new Phrase("Relationship to Issuer", tableFonttd));
                table2.AddCell(new Phrase(" : ", tableFonttd));

                Phrase table2Phrase = new Phrase("SIGNED, SEALED AND DELIVERED by ", tableFonttd);
                table2Phrase.Add(new Chunk("\n The Guarantor", FontFactory.GetFont("Calibri", 11, Font.BOLD)));
                table2.AddCell(table2Phrase);
                table2.AddCell(new Phrase(" ) ______________________", tableFonttd));

                Phrase titleLendersPhrase = new Phrase();
                titleLendersPhrase.Add(new Chunk(new LineSeparator(1f, 9.5f, BaseColor.BLACK, Element.ALIGN_CENTER, -20)));
                titleLendersPhrase.Add(new Chunk("\nPayee(s)\n\n", FontFactory.GetFont("Calibri", 11, Font.BOLD)));

                var titleLenders = new Paragraph(titleLendersPhrase);
                titleLenders.Alignment = Element.ALIGN_CENTER;

                PdfPTable tableLenders = new PdfPTable(6);
                float[] tableLendersWiths = new float[6] { 15, 22, 19, 15, 14, 15 };
                tableLenders.SetWidthPercentage(tableLendersWiths, new Rectangle(100, 100));

                tableLenders.AddCell(new PdfPCell(new Phrase("Payee Reference No", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Name of Payee", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Identification Number", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Principal Subscription Amount", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Repayment Dates", tableFontth)));
                tableLenders.AddCell(new PdfPCell(new Phrase("Amount Payable By Issuer on Each Repayment Date", tableFontth)));

                int index = 0;
                foreach (Lender lender in borrowerLoanContract.AcceptedOffers)
                {
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.ContractIdentifier, tableFonttd)));
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.Name, tableFonttd)));
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.IdNumber, tableFonttd)));
                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.Amount.ToCurrency(), tableFonttd)));

                    if (index == 0)
                    {
                        PdfPCell repCell = new PdfPCell(
                            new Phrase(String.Join(", ", repaymentSchedule.Select(x => x.DueDate.ToDate())), FontFactory.GetFont("Calibri", 11, Font.NORMAL)));
                        repCell.Rowspan = borrowerLoanContract.NumberOfAcceptedOffers;
                        tableLenders.AddCell(repCell);
                    }

                    tableLenders.AddCell(new PdfPCell(new Phrase(lender.RepaymentAmount.ToCurrency(), tableFonttd)));

                    index++;
                }

                Phrase linePhrase = new Phrase();
                linePhrase.Add(new Chunk(new LineSeparator(1f, 52f, BaseColor.BLACK, Element.ALIGN_LEFT, -20)));
                linePhrase.Add(new Chunk("\n The terms of the Promissory Note are set out below: \n\n", FontFactory.GetFont("Calibri", 11, Font.BOLD)));

                var line = new Paragraph(linePhrase);
                line.Alignment = Element.ALIGN_LEFT;

                PdfPTable table = new PdfPTable(2);
                float[] tableWiths = new float[2] { 60, 40 };

                table.SetWidthPercentage(tableWiths, new Rectangle(100, 100));

                table.AddCell(new Phrase("Promissory Note Request ID", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.LoanRequestId.ToString(), tableFonttd));
                table.AddCell(new Phrase("Name of Issuer", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.Name, tableFonttd));
                table.AddCell(new Phrase("Registration Number of Issuer", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.RegistrationNumber, tableFonttd));
                table.AddCell(new Phrase("Date of Promissory Note", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.AgreementDate.ToDate(), tableFonttd));
                table.AddCell(new Phrase("Aggregate Principal Subscription Amount ", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.Amount.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Maturity Period (not exceeding 12 months)", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.Tenure, tableFonttd));
                table.AddCell(new Phrase("Number of Accepted Offers", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.NumberOfAcceptedOffers.ToString(), tableFonttd));
                table.AddCell(new Phrase("Accepted Rate (p.a.)", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.AcceptedRate.ToString("G29") + " %", tableFonttd));
                //table.AddCell(new Phrase("Effective Rate (p.a.)", tableFonttd));
                //table.AddCell(new Phrase(borrowerLoanContract.EffectiveRate.ToString("G29") + " %", tableFonttd));
                table.AddCell(new Phrase("Total Interests Payable", tableFonttd));
                table.AddCell(new Phrase(borrowerLoanContract.TotalInterestPayable.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Repayment Amount at each Repayment Date", tableFonttd));
                table.AddCell(new Phrase(repaymentSchedule.First().Payment.ToCurrency(), tableFonttd));

                var myMemoryStream = new MemoryStream();

                PdfReader template = new PdfReader(
                   HttpContext.Current.Server.MapPath("/Content/Template/Pdf/GuarantorTermsConditions.pdf"));
                Rectangle size = template.GetPageSizeWithRotation(1);
                Document myDocument = new Document(size,30,30,80,30);


                PdfWriter myPDFWriter = PdfWriter.GetInstance(myDocument, myMemoryStream);
                //PdfWriter myPDFWriter = PdfWriter.GetInstance(myDocument,
                //    new FileStream("C://Moolah//PDF//GurantorContract12.pdf", FileMode.Create));

                myDocument.Open();
                myPDFWriter.PageEvent = new PdfPageEventGuarantor();

                PdfContentByte cb = myPDFWriter.DirectContent;

                myDocument.Add(detailsP);
                myDocument.Add(line2);
                myDocument.Add(table2);
                myDocument.Add(titleLenders);
                myDocument.Add(tableLenders);
                myDocument.NewPage();
                myDocument.Add(line);
                myDocument.Add(table);

                myDocument.NewPage();

                // create the new page and add it to the pdf
                PdfImportedPage page1 = myPDFWriter.GetImportedPage(template, 1);
                cb.AddTemplate(page1, 0, 0);

                myDocument.NewPage();

                PdfImportedPage page2 = myPDFWriter.GetImportedPage(template, 2);
                cb.AddTemplate(page2, 0, 0);

                myPDFWriter.CloseStream = false;
                myDocument.Close();
                byte[] content = myMemoryStream.ToArray();

                _IFileUploadDA.InsertUploadedFile("Guarantor Contract.pdf", FileType.GuarantorContract, borrowerLoanContract.UserId, borrowerLoanContract.LoanRequestId);


                _IFileUploadTask.Upload(borrowerLoanContract.UserId.ToString(),
                FileType.GuarantorContract.ToString(),
                "Guarantor Contract.pdf", content, 0, (int)content.Length, borrowerLoanContract.LoanRequestId.ToString());

                myMemoryStream.Position = 0;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

        }

        public MemoryStream GenerateInvestorLoanContract(long loanRequestId, int offerId, string contractIdentifier, int acceptedOffersCount)
        {
            try
            {
                var titleP = new Paragraph("THIS IS AN IMPORTANT LEGAL DOCUMENT \n",
                    FontFactory.GetFont("Calibri", 11, Font.BOLD, BaseColor.LIGHT_GRAY));
                titleP.Alignment = Element.ALIGN_CENTER;

                var titleP2 = new Paragraph("Please read this Note carefully.  You are advised to fully understand your obligations and risks under this Note.  If you have any concerns with this Note, consult a lawyer before signing this Note. \n",
                    FontFactory.GetFont("Calibri", 10, Font.BOLDITALIC, BaseColor.LIGHT_GRAY));
                titleP2.Alignment = Element.ALIGN_CENTER;

                var investorLoanContract = _ILoanAllocationDA.GetInvestorLoanContractModel(offerId);
                var repaymentSchedule = _ILoanPaymentsTask.GetRepaymentSchedule(loanRequestId, (double)investorLoanContract.LoanAmount, (double)investorLoanContract.LoanAcceptedRate,
                    investorLoanContract.Tenure, investorLoanContract.AgreementDate);

                var detailsP = new Paragraph(string.Format("For value received, the Issuer promises to pay to the Payee in Singapore dollars, the relevant Principal Subscription Amount and Interest as specified in the Promissory Note {0}, on such Repayment Date and in accordance with the schedule specified in the Promissory Note {0} \n\n",
                    investorLoanContract.ContractRef),
                    FontFactory.GetFont("Calibri", 10, Font.BOLD));
                detailsP.Alignment = Element.ALIGN_CENTER;

                PdfPTable table = new PdfPTable(2);
                float[] tableWiths = new float[2] { 60, 40 };

                table.SetWidthPercentage(tableWiths, new Rectangle(100, 100));

                table.AddCell(new Phrase("Promissory Note Request ID", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.ContractRef.ToString(), tableFonttd));
                table.AddCell(new Phrase("Payee Reference No", tableFonttd));
                table.AddCell(new Phrase(contractIdentifier, tableFonttd));
                table.AddCell(new Phrase("Name of Payee", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.Name, tableFonttd));
                table.AddCell(new Phrase("Identification Number of Payee", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.LenderIDNumber, tableFonttd));
                table.AddCell(new Phrase("Name of Issuer", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.BorrowerName, tableFonttd));
                table.AddCell(new Phrase("Registration Number of Issuer", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.BorrowerRegNumber, tableFonttd));
                table.AddCell(new Phrase("Date of Promissory Note", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.AgreementDate.ToDate(), tableFonttd));
                table.AddCell(new Phrase("Payee’s Principal Subscription Amount", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.PrincipalAmount.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Aggregate Principal Subscription Amount ", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.LoanAmount.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Maturity Period (not exceeding 12 months)", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.Tenure, tableFonttd));
                table.AddCell(new Phrase("Number of Accepted Offers", tableFonttd));
                table.AddCell(new Phrase(acceptedOffersCount.ToString(), tableFonttd));
                table.AddCell(new Phrase("Accepted Rate (p.a.)", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.LoanAcceptedRate.ToString("G29") + " %", tableFonttd));
                //table.AddCell(new Phrase("Effective Rate (p.a.)", tableFonttd));
                //table.AddCell(new Phrase(borrowerLoanContract.EffectiveRate.ToString("G29") + " %", tableFonttd));
                table.AddCell(new Phrase("Total Interests Payable to Payee", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.TotalInterestReceivable.ToCurrency(), tableFonttd));
                table.AddCell(new Phrase("Repayment Amount at each Repayment Date to Payee", tableFonttd));
                table.AddCell(new Phrase(investorLoanContract.MonthlyRepaymentAmount.ToCurrency(), tableFonttd));

                var notice = new Paragraph("\nPlease note that your actual interests received can appear to be off by a few cents when compared to a manual calculation of interests. This is because we round off the monthly repayment to the nearest 2 decimals which sometimes contain a factor of a cent. \n",
                    FontFactory.GetFont("Calibri", 10, Font.BOLD));
                notice.Alignment = Element.ALIGN_LEFT;

                var titleSchedules = new Paragraph("\n\n Scheduled Dates \n\n", FontFactory.GetFont("Calibri", 11, Font.BOLD));
                titleSchedules.Alignment = Element.ALIGN_CENTER;

                PdfPTable tableSchedules = new PdfPTable(6);
                foreach (RepaymentEntity repayment in repaymentSchedule)
                {
                    tableSchedules.AddCell(new PdfPCell(new Phrase(repayment.DueDate.ToDate(), tableFonttd)));
                }

                var footer = new Paragraph("\n\n This Note is issued subject to the “Standard Terms and Conditions To Promissory Note”. ",
                    FontFactory.GetFont("Calibri", 11, Font.BOLD));
                footer.Alignment = Element.ALIGN_CENTER;

                var myMemoryStream = new MemoryStream();

                PdfReader template = new PdfReader(
                   HttpContext.Current.Server.MapPath("/Content/Template/Pdf/TermsConditions.pdf"));
                Rectangle size = template.GetPageSizeWithRotation(1);
                Document myDocument = new Document(size);

                PdfWriter myPDFWriter = PdfWriter.GetInstance(myDocument, myMemoryStream);
                //PdfWriter myPDFWriter = PdfWriter.GetInstance(myDocument,
                //    new FileStream("C://Moolah//PDF//InvestorContract.pdf", FileMode.Create));

                myDocument.Open();

                PdfContentByte cb = myPDFWriter.DirectContent;

                myDocument.Add(titleP);
                myDocument.Add(titleP2);
                myDocument.Add(detailsP);
                myDocument.Add(table);
                myDocument.Add(notice);
                myDocument.Add(titleSchedules);
                myDocument.Add(tableSchedules);
                myDocument.Add(footer);

                myDocument.NewPage();

                // create the new page and add it to the pdf
                PdfImportedPage page1 = myPDFWriter.GetImportedPage(template, 1);
                cb.AddTemplate(page1, 0, 0);

                myDocument.NewPage();

                PdfImportedPage page2 = myPDFWriter.GetImportedPage(template, 2);
                cb.AddTemplate(page2, 0, 0);

                myDocument.NewPage();

                PdfImportedPage page3 = myPDFWriter.GetImportedPage(template, 3);
                cb.AddTemplate(page3, 0, 0);

                myPDFWriter.CloseStream = false;
                myDocument.Close();
                byte[] content = myMemoryStream.ToArray();

                _IFileUploadDA.InsertUploadedFile("Note Contract.pdf", FileType.PayeeLoanContract, investorLoanContract.UserId,
                    loanRequestId, offerId);

                _IFileUploadTask.Upload(investorLoanContract.UserId.ToString(),
                    FileType.PayeeLoanContract.ToString(),
                    "Note Contract.pdf", content, 0, (int)content.Length, loanRequestId.ToString(), offerId.ToString());

                myMemoryStream.Position = 0;
                return myMemoryStream;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new MemoryStream();
            }
        }
    }
}
