﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;
using Microsoft.VisualBasic;
using MoolahConnect.Util.Email;

namespace MoolahConnect.Tasks.Implementation
{
    public class LoanAllocationTask : ILoanAllocationTask
    {
        ILoanAllocationDA _ILoanAllocationDA;
        IEmailSender _IEmailSender;
        ITransactionsDA _ITransactionDA;
        ILoanDetailsDA _ILoanDetailsDA;

        public LoanAllocationTask()
        {
            _ILoanAllocationDA = new LoanAllocationDA();
            _IEmailSender = new EmailSender();
            _ITransactionDA = new TransactionsDA();
            _ILoanDetailsDA = new LoanDetailsDA();
        }

        public bool Allocate(long loanRequestId, out string status, out string statusCategory, out decimal outFinalRate, bool isPreview = false)
        {
            var statusMessage = string.Empty;
            double totalOffersAmount = 0;
            double loanAmount = 0;
            double minThresholdAmount = 0;
            var loanOffersBelowOrEqualToRate = new List<tbl_Loanoffers>();
            decimal finalRate = 0;

            status = string.Empty;
            statusCategory = string.Empty;

            try
            {
                var pendingLoanOffers = _ILoanAllocationDA.GetLoanOffersByrequestId(loanRequestId)
                    .Where(x => x.OfferStatus == (int)LoanOfferStatus.Pending).ToList<tbl_Loanoffers>();
                var loanRequest = _ILoanAllocationDA.GetLoanRequestByRequestId(loanRequestId);
                outFinalRate = loanRequest.Rate.HasValue ? loanRequest.Rate.Value : 0;

                if (loanRequest.LoanStatus > 2)
                {
                    statusCategory = StatusMessages.LOAN_ALREADY_MATCHED;
                    status = "Your listing is already " + loanRequest.LoanStatus.ToLoanStatusDisplayName();
                    return false;
                }

                loanAmount = loanRequest.Amount.HasValue ? (double)loanRequest.Amount.Value : 0;

                minThresholdAmount = loanAmount * Settings.MinThreshold;

                //Get loan offer below or equal to the borrower's target rate
                loanOffersBelowOrEqualToRate = pendingLoanOffers.Where(x => x.OfferedRate <= loanRequest.Rate).ToList();

                //Get the sum of selected loan offers by target rate.
                var totalOffersAmountTemp = loanOffersBelowOrEqualToRate.Sum(x => x.OfferedAmount);
                totalOffersAmount = (double)totalOffersAmountTemp;

                if (totalOffersAmount < Settings.MinQuantum)
                {
                    statusCategory = StatusMessages.MIN_LOAN_ACCEPTANCE_NOTMET;
                    status = "Investors' total offers cannot satisfy the minimum quantum of " + Settings.MinQuantum.ToCurrency();
                    return false;
                }

                if (totalOffersAmount < minThresholdAmount)
                {
                    statusCategory = StatusMessages.MIN_LOAN_ACCEPTANCE_NOTMET;
                    status = string.Format("Investors' total offers cannot satisfy the minimum {0}% threshold of {1}", Settings.MinThreshold * 100, minThresholdAmount.ToCurrency());
                    return false;
                }

                //Set loan status to matched in the begining to avoid any offer addition or alterations during the process.
                if (!isPreview)
                {
                    _ILoanDetailsDA.ChangeRequestStatusByID(loanRequestId, LoanRequestStatus.Matched);
                }

                //Success if the offers amount is less than or equal to loan amount after initial checkings.
                if (totalOffersAmount <= loanAmount)
                {
                    finalRate = loanOffersBelowOrEqualToRate.Max(x => x.OfferedRate);

                    if (isPreview)
                    {
                        outFinalRate = finalRate;
                        return true;
                    }

                    statusCategory = StatusMessages.SUCCESS;
                    status = "Note Matched Provisonally";
                    OnAllocationSuccess(loanOffersBelowOrEqualToRate, loanRequest, totalOffersAmount);


                    RejectedOffersDueToRate(pendingLoanOffers, loanRequest, finalRate);
                    return true;
                }

                //Arrange allocations with extra loan offers.
                var sortedloanOffers = new List<tbl_Loanoffers>();
                sortedloanOffers = loanOffersBelowOrEqualToRate.OrderBy(x => x.OfferedRate).ThenBy(x => x.DateCreated).ToList();

                var approvedLoanOffers = new List<tbl_Loanoffers>();
                var rejectedLoanOffersDueToDemand = new List<tbl_Loanoffers>();
                double totalTemp = 0;

                for (int i = 0; i < sortedloanOffers.Count; i++)
                {

                    double approvedAmount = 0;
                    try
                    {
                        double offerAmount = (double)sortedloanOffers[i].OfferedAmount;

                        totalTemp += offerAmount;


                        if (totalTemp < loanAmount)
                        {
                            //No alterations until the offer sum pass the loan amount.
                            approvedAmount = offerAmount;
                        }
                        else
                        {
                            //Decrease the spare amount to match the loan amount and accept the cut off mark investor.
                            var spareAmount = totalTemp - loanAmount;
                            approvedAmount = offerAmount - spareAmount;

                            sortedloanOffers[i].AcceptedAmount = (decimal)approvedAmount;
                            approvedLoanOffers.Add(sortedloanOffers[i]);
                            var offer = sortedloanOffers[i];

                            finalRate = approvedLoanOffers.Max(x => x.OfferedRate);

                            if (isPreview)
                            {
                                outFinalRate = finalRate;
                                return true;
                            }

                            if (spareAmount > 0)
                            {
                                _ITransactionDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID, (decimal)spareAmount,
                                    loanRequestId, "Internet", AuditMessages.LOAN_OFFER_PARTIALLY_ALLOCATED, (int)offer.tbl_Users.UserID);
                                _ITransactionDA.LoanTransactionCredit((int)offer.tbl_Users.UserID, (decimal)spareAmount,
                                    loanRequestId, "Internet", AuditMessages.LOAN_OFFER_PARTIALLY_ALLOCATED);
                            }

                            //Get remaining offers and add them to the rejected offers due to demand.
                            var remainingOffers = sortedloanOffers.Skip(i + 1);
                            rejectedLoanOffersDueToDemand.AddRange(remainingOffers);

                            break;
                        }

                        //Add to the approved offers here mainly to keep the refined offer amount.
                        sortedloanOffers[i].AcceptedAmount = (decimal)approvedAmount;
                        approvedLoanOffers.Add(sortedloanOffers[i]);
                    }
                    catch (Exception ex)
                    {
                        MoolahLogManager.LogException(ex);
                    }
                }

                OnAllocationSuccess(approvedLoanOffers, loanRequest, loanAmount);

                RejectedOffersDueToDemand(rejectedLoanOffersDueToDemand, loanRequest, finalRate);
                RejectedOffersDueToRate(pendingLoanOffers, loanRequest, finalRate);

                statusCategory = StatusMessages.SUCCESS;
                status = "Note Matched Provisonally";
                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                _ILoanDetailsDA.ChangeRequestStatusByID(loanRequestId, LoanRequestStatus.InProgress);
                statusCategory = StatusMessages.SYSTEM_ERROR;
                status = "Your request can not be processed due to system error. Please try again";
                outFinalRate = 0;
                return false;
            }
        }

        private void OnAllocationSuccess(List<tbl_Loanoffers> allocatedLoanOffers, tbl_LoanRequests loanRequest, double acceptedAmount)
        {
            var accountDetails = loanRequest.tbl_Users.tbl_AccountDetails.SingleOrDefault();
            double fundedPrecentage = 0;
            double moolahFees = 0;
            decimal loanRate = allocatedLoanOffers.Max(x => x.OfferedRate);

            if (loanRequest.Amount.HasValue)
            {
                fundedPrecentage = (acceptedAmount / (double)loanRequest.Amount.Value) * 100;
                moolahFees = acceptedAmount * Settings.MoolahFees * loanRequest.Terms.ToYear();
            }

            _ILoanAllocationDA.UpdateLoanRequestOnApproval(loanRequest.RequestId, acceptedAmount, loanRate);
            MoolahLogManager.Log(AuditMessages.LOAN_MATCHED, LogType.Info, loanRequest.tbl_Users.UserName, loanRequest.RequestId);
            MoolahLogManager.Log(string.Format("Note Matched RequestID : {0}",
                         loanRequest.RequestId.ToString()),
                        LogType.Warn,
                        loanRequest.tbl_Users.UserName);
            _ITransactionDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID, (decimal)acceptedAmount, loanRequest.RequestId,
                "Internet", AuditMessages.LOAN_MATCHED_INV,
                (int)loanRequest.tbl_Users.UserID);

            _ITransactionDA.LoanTransactionCredit((int)loanRequest.tbl_Users.UserID, (decimal)acceptedAmount, loanRequest.RequestId,
                "Internet", AuditMessages.LOAN_MATCHED);
            _ITransactionDA.LoanTransactionDebit((int)loanRequest.tbl_Users.UserID, (decimal)moolahFees, loanRequest.RequestId,
                "Internet", AuditMessages.MOOLAHSENSE_FEES);

            _ITransactionDA.InternalTransactionCredit(Settings.MsenseAccountBalanceID, (decimal)moolahFees, loanRequest.RequestId,
                "Internet", AuditMessages.MOOLAHSENSE_FEES,
                (int)loanRequest.tbl_Users.UserID);

            LoanAmotrization(acceptedAmount, (double)loanRate, loanRequest.Terms, DateTime.UtcNow, loanRequest.RequestId);

            int index = 1;
            foreach (tbl_Loanoffers offer in allocatedLoanOffers)
            {
                double acceptedAmountTemp = offer.AcceptedAmount.HasValue ? (double)offer.AcceptedAmount.Value : (double)offer.OfferedAmount;

                var investorAccountDetails = offer.tbl_Users.tbl_AccountDetails.SingleOrDefault();
                var investorDisplayName = string.Empty;
                var companyName = string.Empty;

                if (accountDetails != null)
                {
                    companyName = accountDetails.AccountName;
                }

                if (investorAccountDetails != null)
                {
                    investorDisplayName = investorAccountDetails.DisplayName;
                }

                string contractIdentifier = string.Format("{0}-{1}", loanRequest.RequestId, index++.ToString("D4"));
                _ITransactionDA.LoanTransaction((int)offer.tbl_Users.UserID, loanRequest.RequestId, "Internet", AuditMessages.LOAN_MATCHED_INV, TransactionType.Info);
                _ILoanAllocationDA.UpdateLoanOffer(offer.OfferId, acceptedAmountTemp, (double)loanRate, Util.Enums.LoanOfferStatus.Accepted, contractIdentifier);

                //Send accepted offers count from here since the offers are updating in DB.
                //_IEmailSender.OnAcceptedLoanOfferToInvetor(offer.tbl_Users.UserName, loanRequest.RequestId, offer.OfferId,
                    ///contractIdentifier, allocatedLoanOffers.Count);
            }

            //_IEmailSender.OnAcceptedLoanRequestToBorrower(loanRequest.tbl_Users.UserName, loanRequest.RequestId);
        }

        private void RejectedOffersDueToRate(List<tbl_Loanoffers> allLoanOffers, tbl_LoanRequests loanRequest, decimal finalRate)
        {
            var rejectedOffers = allLoanOffers.Where(x => x.OfferedRate > loanRequest.Rate).ToList();

            if (rejectedOffers.Count > 0)
            {
                var accountDetails = loanRequest.tbl_Users.tbl_AccountDetails.SingleOrDefault();
                var companyName = string.Empty;

                if (accountDetails != null)
                    companyName = accountDetails.AccountName;

                foreach (tbl_Loanoffers offer in rejectedOffers)
                {
                    var investorAccountDetails = offer.tbl_Users.tbl_AccountDetails.SingleOrDefault();
                    var investorDisplayName = string.Empty;

                    if (investorAccountDetails != null)
                    {
                        investorDisplayName = investorAccountDetails.DisplayName;
                    }

                    _ILoanAllocationDA.UpdateLoanOffer(offer.OfferId, 0, null, Util.Enums.LoanOfferStatus.Rejected, null, ReasonForRejectedOffers.DUE_TO_DEMAND);

                    _ITransactionDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID, offer.OfferedAmount, loanRequest.RequestId,
                        "Internet", AuditMessages.LOAN_OFFER_REJECTED, (int)offer.tbl_Users.UserID);

                    _ITransactionDA.LoanTransactionCredit((int)offer.tbl_Users.UserID,
                        offer.OfferedAmount,
                        loanRequest.RequestId,
                        "Internet",
                        AuditMessages.LOAN_OFFER_REJECTED);
                    //_IEmailSender.OnRejectedLoanOfferToInvetor(offer.tbl_Users.UserName, loanRequest.RequestId.ToString(),
                    //    (double)offer.OfferedAmount,
                    //(double)offer.OfferedRate,
                    //offer.DateCreated,
                    //borrowerDetails != null ? borrowerDetails.BusinessName : string.Empty,
                    //     ReasonForRejectedOffers.DUE_TO_HIGH_RATE);
                }
            }
        }

        private void RejectedOffersDueToDemand(List<tbl_Loanoffers> rejectedOffers, tbl_LoanRequests loanRequest, decimal finalRate)
        {
            var accountDetails = loanRequest.tbl_Users.tbl_AccountDetails.SingleOrDefault();
            var companyName = string.Empty;

            if (accountDetails != null)
                companyName = accountDetails.AccountName;

            foreach (tbl_Loanoffers offer in rejectedOffers)
            {
                var investorAccountDetails = offer.tbl_Users.tbl_AccountDetails.SingleOrDefault();
                var investorDisplayName = string.Empty;

                if (investorAccountDetails != null)
                {
                    investorDisplayName = investorAccountDetails.DisplayName;
                }

                _ILoanAllocationDA.UpdateLoanOffer(offer.OfferId, 0, null, Util.Enums.LoanOfferStatus.Rejected, null, ReasonForRejectedOffers.DUE_TO_DEMAND);

                _ITransactionDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID, offer.OfferedAmount,
                        loanRequest.RequestId,
                        "Internet",
                        AuditMessages.LOAN_OFFER_REJECTED, (int)offer.tbl_Users.UserID);

                _ITransactionDA.LoanTransactionCredit((int)offer.tbl_Users.UserID,
                        offer.OfferedAmount,
                        loanRequest.RequestId,
                        "Internet",
                        AuditMessages.LOAN_OFFER_REJECTED);
                //_IEmailSender.OnRejectedLoanOfferToInvetor(offer.tbl_Users.UserName, loanRequest.RequestId.ToString(),
                //    (double)offer.OfferedAmount,
                //    (double)offer.OfferedRate,
                //    offer.DateCreated,
                //    borrowerDetails != null ? borrowerDetails.BusinessName : string.Empty,
                //    ReasonForRejectedOffers.DUE_TO_DEMAND);
            }
        }

        public void LoanAmotrization(double amount, double rate, string Tenor, DateTime startDate,
            long LoanRequestID)
        {
            var repaymentSchedule = new List<RepaymentEntity>();
            double monthlyRepayment = 0;
            double periodicInterestRate = 0;
            int tenorinYears = 0;
            int totalNoOfPayments = 0;

            double preEndBalance = 0;

            try
            {
                tenorinYears = int.Parse(Tenor) / 12;
                totalNoOfPayments = tenorinYears * Settings.PaymentsPerYear;
                periodicInterestRate = rate / Settings.PaymentsPerYear;

                monthlyRepayment = Financial.Pmt(periodicInterestRate / 100, totalNoOfPayments, amount * -1);

                preEndBalance = amount;
                for (int i = 1; i <= totalNoOfPayments; i++)
                {
                    double interest = preEndBalance * ((rate / 100) / Settings.PaymentsPerYear);
                    double principal = monthlyRepayment - interest;
                    double endBalance = preEndBalance - principal;
                    DateTime date = startDate.AddMonths(i);

                    if ((int)endBalance <= 0)
                        endBalance = 0;

                    tbl_LoanAmortization objLoanAmortizatin = new tbl_LoanAmortization();
                    objLoanAmortizatin.Balance = (decimal)Math.Round(endBalance, 2);
                    objLoanAmortizatin.EmiAmount = (decimal)Math.Round(monthlyRepayment, 2);
                    objLoanAmortizatin.EmiDate = date;
                    objLoanAmortizatin.Interest = (decimal)interest;
                    objLoanAmortizatin.PayStatus = (int)RepaymentPayStatus.Pending;
                    objLoanAmortizatin.AmountTotal = (decimal)Math.Round(monthlyRepayment, 2);
                    objLoanAmortizatin.LoanRequest_ID = LoanRequestID;
                    objLoanAmortizatin.Principal = (decimal)principal;

                    _ILoanAllocationDA.InsertLoanAmortization(objLoanAmortizatin);

                    preEndBalance = endBalance;
                }

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }


        }

        public decimal calculateEMI(decimal LoanAmount, decimal Rate, decimal period)
        {
            decimal loanAmt = 0;
            decimal interestRate = 0;
            decimal tenure = 0;
            try
            {
                loanAmt = LoanAmount;
                tenure = period;
                interestRate = Convert.ToDecimal(Rate);
                if (Convert.ToDecimal(interestRate) != 0)
                {
                    var intr = Rate / 1200;
                    double x = Convert.ToDouble(1 / (1 + intr));
                    double y = Convert.ToDouble(period);

                    decimal z = Convert.ToDecimal(Math.Pow(x, y));
                    var final = Convert.ToDecimal(LoanAmount * intr / (1 - (z)));

                    return Math.Round(final, 2);


                }
                else
                {
                    var emi = loanAmt / tenure;
                    var emiPerLakh = Math.Round(emi, 0);
                    return emiPerLakh;
                }
            }
            catch (Exception ex)
            {

                return 0;
            }
        }

        public int[] TimeLetfToExpireLoanRequest(DateTime approvedDate)
        {
            int[] timeLeft = new int[3];

            try
            {
                if (DateTime.UtcNow.Subtract(approvedDate).TotalMinutes >= Settings.LoanRequestExpiryDays * 24 * 60
                    || DateTime.UtcNow.Subtract(approvedDate).TotalMinutes < 0)
                    return new int[0];

                double timeLeftInMinutes = (Settings.LoanRequestExpiryDays * 24 * 60) - DateTime.UtcNow.Subtract(approvedDate).TotalMinutes;

                timeLeft[0] = (int)(timeLeftInMinutes / (24 * 60));
                timeLeft[1] = (int)((timeLeftInMinutes % (24 * 60)) / 60);
                timeLeft[2] = (int)((timeLeftInMinutes % (24 * 60)) % 60);

                return timeLeft;
            }
            catch (Exception ex)
            {
                return new int[0];
                throw;
            }
        }


        public int[] TimeLetfToExpireLoanDraft(DateTime approvedDate)
        {
            int[] timeLeft = new int[3];

            try
            {
                if (DateTime.UtcNow.Subtract(approvedDate).TotalMinutes >= Settings.LoanDraftExpiryDays * 24 * 60 || DateTime.UtcNow.Subtract(approvedDate).TotalMinutes < 0)
                    return new int[0];

                double timeLeftInMinutes = (Settings.LoanDraftExpiryDays * 24 * 60) - DateTime.UtcNow.Subtract(approvedDate).TotalMinutes;

                timeLeft[0] = (int)(timeLeftInMinutes / (24 * 60));
                timeLeft[1] = (int)((timeLeftInMinutes % (24 * 60)) / 60);
                timeLeft[2] = (int)((timeLeftInMinutes % (24 * 60)) % 60);

                return timeLeft;
            }
            catch (Exception ex)
            {
                return new int[0];
                throw;
            }
        }


        public bool AcceptLoanByAdmin(long requestID, DateTime transferDate, decimal moolahFee, string adminUser)
        {
            try
            {
                _ILoanAllocationDA.AcceptPendingTransfer(requestID, transferDate, (decimal)moolahFee, adminUser);

                var allocationDetails = _ILoanAllocationDA.GetBorrowerLoanContractModel(requestID);

                _ITransactionDA.LoanTransactionCredit((int)allocationDetails.UserIDInt, 0,
                                    requestID, "Internet", AuditMessages.NOTE_EXECUTED_BY_ISSUER, TransactionType.Info);
                _ITransactionDA.InternalTransactionDebit(Settings.MsenseAccountBalanceID, moolahFee, requestID, "Internet",
                    "Fees transferred to bank", allocationDetails.UserIDInt);

                _IEmailSender.onNoteAcceptedByAdminToBorrower(allocationDetails.Username, allocationDetails.LoanRequestId);

                foreach (Lender lender in allocationDetails.AcceptedOffers)
                {
                    _IEmailSender.onNoteAcceptedByAdminToInvestor(lender.Username, allocationDetails.LoanRequestId);
                    _ITransactionDA.LoanTransactionCredit((int)lender.UserID, 0,
                                    requestID, "Internet", AuditMessages.NOTE_EXECUTED_BY_ISSUER, TransactionType.Info);
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}
