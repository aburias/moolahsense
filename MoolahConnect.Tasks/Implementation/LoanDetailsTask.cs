﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Util.Email;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;

namespace MoolahConnect.Tasks.Implementation
{
    public class LoanDetailsTask : ILoanDetailsTask
    {
        private ILoanDetailsDA _ILoanDetailsDA;
        private IUser _IUser;
        private IEmailSender _IEmailSender;
        private IPdfGeneratorTask _IPdfGeneratorTask;

        public LoanDetailsTask()
        {
            _ILoanDetailsDA = new LoanDetailsDA();
            _IUser = new User();
            _IEmailSender = new EmailSender();
            _IPdfGeneratorTask = new PdfGeneratorTask();
        }

        public IEnumerable<LoanEntity> GetLoans(int timePeriod, LoanRequestStatus loanRequestStatus)
        {
            try
            {
                DateTime? to = null;
                DateTime? from = null;

                DateTime thisMonthFirstDate = DateTime.Now.AddDays((-1) * DateTime.Now.Day);
                to = DateTime.Now;

                switch ((TimePeriodFilterEnum)timePeriod)
                {
                    case TimePeriodFilterEnum.All:
                        to = null;
                        break;
                    case TimePeriodFilterEnum.Current:
                        from = thisMonthFirstDate;
                        break;
                    case TimePeriodFilterEnum.LAST3:
                        from = DateTime.Now.AddMonths(-3);
                        break;
                    case TimePeriodFilterEnum.LAST6:
                        from = DateTime.Now.AddMonths(-6);
                        break;
                    case TimePeriodFilterEnum.LAST9:
                        from = DateTime.Now.AddMonths(-9);
                        break;
                    case TimePeriodFilterEnum.LAST12:
                        from = DateTime.Now.AddMonths(-12);
                        break;
                    default:
                        break;
                }

                return _ILoanDetailsDA.GetLoans(from, to, loanRequestStatus);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanEntity>();
            }
        }


        public IEnumerable<LoanRepaymentEntity> GetLoanRepayments(int timePeriod)
        {
            try
            {
                DateTime? to = null;
                DateTime? from = null;

                DateTime thisMonthFirstDate = DateTime.Now.AddDays((-1) * DateTime.Now.Day);
                to = DateTime.Now;

                switch ((TimePeriodFilterEnum)timePeriod)
                {
                    case TimePeriodFilterEnum.All:
                        to = null;
                        break;
                    case TimePeriodFilterEnum.Current:
                        from = thisMonthFirstDate;
                        break;
                    case TimePeriodFilterEnum.LAST3:
                        from = DateTime.Now.AddMonths(-3);
                        break;
                    case TimePeriodFilterEnum.LAST6:
                        from = DateTime.Now.AddMonths(-6);
                        break;
                    case TimePeriodFilterEnum.LAST9:
                        from = DateTime.Now.AddMonths(-9);
                        break;
                    case TimePeriodFilterEnum.LAST12:
                        from = DateTime.Now.AddMonths(-12);
                        break;
                    default:
                        break;
                }

                return _ILoanDetailsDA.GetRepayments(from, to);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanRepaymentEntity>();
            }
        }

        public IEnumerable<LoanRepaymentEntity> GetLoanRepaymentsByStatus(int timePeriod, RepaymentPayStatus status)
        {
            try
            {
                DateTime? to = null;
                DateTime? from = null;

                DateTime thisMonthFirstDate = DateTime.Now.AddDays((-1) * DateTime.Now.Day);
                to = DateTime.Now;

                switch ((TimePeriodFilterEnum)timePeriod)
                {
                    case TimePeriodFilterEnum.All:
                        to = null;
                        break;
                    case TimePeriodFilterEnum.Current:
                        from = thisMonthFirstDate;
                        break;
                    case TimePeriodFilterEnum.LAST3:
                        from = DateTime.Now.AddMonths(-3);
                        break;
                    case TimePeriodFilterEnum.LAST6:
                        from = DateTime.Now.AddMonths(-6);
                        break;
                    case TimePeriodFilterEnum.LAST9:
                        from = DateTime.Now.AddMonths(-9);
                        break;
                    case TimePeriodFilterEnum.LAST12:
                        from = DateTime.Now.AddMonths(-12);
                        break;
                    default:
                        break;
                }

                return _ILoanDetailsDA.GetRepaymentsByStats(from, to, status);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanRepaymentEntity>();
            }
        }

        public IEnumerable<LoanRepaymentEntity> GetLateLoanRepayments(int timePeriod)
        {
            try
            {
                DateTime? to = null;
                DateTime? from = null;

                DateTime thisMonthFirstDate = DateTime.Now.AddDays((-1) * DateTime.Now.Day);
                to = DateTime.Now;

                switch ((TimePeriodFilterEnum)timePeriod)
                {
                    case TimePeriodFilterEnum.All:
                        to = null;
                        break;
                    case TimePeriodFilterEnum.Current:
                        from = thisMonthFirstDate;
                        break;
                    case TimePeriodFilterEnum.LAST3:
                        from = DateTime.Now.AddMonths(-3);
                        break;
                    case TimePeriodFilterEnum.LAST6:
                        from = DateTime.Now.AddMonths(-6);
                        break;
                    case TimePeriodFilterEnum.LAST9:
                        from = DateTime.Now.AddMonths(-9);
                        break;
                    case TimePeriodFilterEnum.LAST12:
                        from = DateTime.Now.AddMonths(-12);
                        break;
                    default:
                        break;
                }

                return _ILoanDetailsDA.GetLateRepayments(from, to);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new List<LoanRepaymentEntity>();
            }
        }


        public IEnumerable<LoanInvestorEntity> GetLoanInvestors(long requestId, long repId)
        {
            return _ILoanDetailsDA.GetLoanInvestors(requestId, repId);
        }


        public void NotifyApprovedInvestorsAndBorrowerOnTargetRateChanged(long requestID, double oldRate, double newRate)
        {
            try
            {
                var loanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestID);

                if (loanRequest == null)
                    return;

                var borrower = _IUser.GetUserFromId(loanRequest.User_ID.HasValue ? loanRequest.User_ID.Value : 0);
                var accountDetails = _IUser.GetAccountDetailsByID(loanRequest.User_ID.HasValue ? loanRequest.User_ID.Value : 0);

                if (borrower == null || accountDetails == null)
                    return;

                _IEmailSender.BorrowerRateChanged(borrower.UserName, oldRate, newRate);

                var approvedInvestors = _IUser.GetApprovedInvestors();

                foreach (tbl_Users approvedInv in approvedInvestors)
                {
                    _IEmailSender.InvestorRateChanged(approvedInv.UserName, accountDetails.BusinessName, oldRate, newRate);
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public bool ExpireLoanRequests(long requestID)
        {
            try
            {
                var offers = _ILoanDetailsDA.GetLoanOffersByRequestID(requestID,
                    LoanOfferStatus.Pending);

                var request = _ILoanDetailsDA.ExpireLoanRequests(requestID);

                if (request == null)
                    return false;

                _IEmailSender.OnLoanListingExpired(request.tbl_Users.UserName, request.RequestId.ToString());

                foreach (tbl_Loanoffers offer in offers)
                {
                    _IEmailSender.onLoanOfferCancelledDueToLoanRequestExpire(offer.tbl_Users.UserName,
                        request.RequestId.ToString());
                }


                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }


        public void NotifyApprovedInvestorsAndBorrowerOnLoanPublished(long requestID)
        {
            try
            {
                var loanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestID);

                if (loanRequest == null)
                    return;

                var borrower = _IUser.GetUserFromId(loanRequest.User_ID.HasValue ? loanRequest.User_ID.Value : 0);
                var accountDetails = _IUser.GetAccountDetailsByID(loanRequest.User_ID.HasValue ? loanRequest.User_ID.Value : 0);

                if (borrower == null || accountDetails == null)
                    return;

                _IEmailSender.BorrowerLoanPublished(borrower.UserName,
                    loanRequest.Amount.HasValue ? (double)loanRequest.Amount.Value : 0,
                    loanRequest.Rate.HasValue ? (double)loanRequest.Rate.Value : 0);

                var approvedInvestors = _IUser.GetApprovedInvestors();

                foreach (tbl_Users approvedInv in approvedInvestors)
                {
                    _IEmailSender.InvestorLoanPublished(approvedInv.UserName, accountDetails.BusinessName,
                        loanRequest.Amount.HasValue ? (double)loanRequest.Amount.Value : 0,
                    loanRequest.Rate.HasValue ? (double)loanRequest.Rate.Value : 0);
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }


        public void SendEmailsToBorrowerAndInvestedInvestorsOnAccpted(long requestID)
        {
            try
            {
                var acceptedLoanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestID);
                var acceptedOffers = _ILoanDetailsDA.GetLoanOffersByRequestID(requestID, LoanOfferStatus.Accepted);
                var rejectedOffers = _ILoanDetailsDA.GetLoanOffersByRequestID(requestID, LoanOfferStatus.Rejected);

                var borrowerDetails = acceptedLoanRequest.tbl_Users.tbl_AccountDetails.FirstOrDefault();

                _IEmailSender.OnAcceptedLoanRequestToBorrower(acceptedLoanRequest.tbl_Users.UserName, acceptedLoanRequest.RequestId);
                //Generate the provisional contract and upload to file store
                _IPdfGeneratorTask.GenerateBorrowerLoanContract(acceptedLoanRequest.RequestId);

                foreach (tbl_Loanoffers offer in acceptedOffers)
                {
                    _IEmailSender.OnAcceptedLoanOfferToInvetor(offer.tbl_Users.UserName, acceptedLoanRequest.RequestId,
                        offer.OfferId,
                    offer.ContractId, acceptedOffers.Count());
                    ////Generate the provisional contract and upload to file store
                    _IPdfGeneratorTask.GenerateInvestorLoanContract(requestID, offer.OfferId,
                    offer.ContractId, acceptedOffers.Count());
                }

                foreach (tbl_Loanoffers offer in rejectedOffers)
                {
                    _IEmailSender.OnRejectedLoanOfferToInvetor(offer.tbl_Users.UserName, acceptedLoanRequest.RequestId.ToString(),
                        (double)offer.OfferedAmount,
                    (double)offer.OfferedRate,
                    offer.DateCreated,
                    borrowerDetails != null ? borrowerDetails.BusinessName : string.Empty,
                         offer.ReasonToReject);
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                throw;
            }
        }
    }
}
