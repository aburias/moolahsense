﻿//Function for validate image type
//By Sourabh Sachdeva
//27 Nov 2009

function ValidateFileUpload(fuc) {
    var fuData = document.getElementById(fuc);
    var FileUploadPath = fuData.value;

    if (FileUploadPath == '') {
        return false;
    }
    else {
        var Extension = FileUploadPath.substring(FileUploadPath.lastIndexOf('.') + 1).toLowerCase();
        if (Extension == "JPG" || Extension == "jpg" || Extension == "GIF" || Extension == "gif" || Extension == "PNG" || Extension == "png" || Extension == "JPEG" || Extension == "jpeg") {

            return true;
        }
        else {
            fuData.value = "";
            alert("You can upload only JPG,GIF,PNG,JPEG files");
            return false;

        }
    }
}


//Function for Select All and deSelectall option of in any databoundcontrol
//By Sourabh Sachdeva
//4 Dec 2009

function ChkSelectAll(CheckBoxControl,GrdID) {		
    if (CheckBoxControl.checked == true) {
       var i;
        for (i=0; i < document.forms[0].elements.length; i++) {
            if ((document.forms[0].elements[i].type == 'checkbox') && (document.forms[0].elements[i].name.indexOf(GrdID) > -1)) {					
				if (document.forms[0].elements[i].disabled==false)
				{
	                document.forms[0].elements[i].checked = true;
				}
            }
        }
    } 
    
    else {
        
        var i;
        
        for (i=0; i < document.forms[0].elements.length; i++) {

            if ((document.forms[0].elements[i].type == 'checkbox') && (document.forms[0].elements[i].name.indexOf(GrdID) > -1)) {
                
                document.forms[0].elements[i].checked = false;
            }
        }
    }
}

//Function for check the selected checkbox before deletion.
//By Sourabh Sachdeva
//4 Dec 2009


function CheckboxSelection() {

    var LIntCtr;
    var LIntSelectedCheckBoxes = 0;

    for (LIntCtr = 0; LIntCtr < document.forms[0].elements.length; LIntCtr++) {
        if ((document.forms[0].elements[LIntCtr].type == 'checkbox') && (document.forms[0].elements[LIntCtr].name.indexOf('chkinner') > -1)) {
            if (document.forms[0].elements[LIntCtr].checked == true) {
                LIntSelectedCheckBoxes = parseInt(LIntSelectedCheckBoxes) + 1;
            }
        }
    }
    if (parseInt(LIntSelectedCheckBoxes) == 0) {
        alert('Record(s) Must Be Selected For Deletion !');
        return false;
    }
    else {
        return window.confirm('Do You Really Want To Delete The Selected Record(s) !');
    }
}
