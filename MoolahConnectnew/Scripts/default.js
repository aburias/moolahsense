﻿//Log the error on application OnError method.
window.onerror = function ErrorHandler(error, file, lineNumber) {
    $.ajax({
        type: "POST",
        url: "/App/Log",
        data: "{'url' : '" + window.location.href +
            "','host' : '" + window.location.host +
            "','error' : '" + error + "','file' : '" + file + "','lineNo' : '" + lineNumber + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            //alert('Unhandled client side error has been logged.');
        }
    });
}