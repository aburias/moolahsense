﻿function LogOut(returnUrl) {
    window.location = "/User/IdleLogOff";
}

function SetIdleLogOutTimer(timeLeftInMinutes, returnUrl) {
    var timeLeftInMiliSeconds = parseInt(timeLeftInMinutes) * 60 * 1000;
    var idle = setTimeout(function () {
        LogOut(returnUrl);
    }, timeLeftInMiliSeconds);
    
    $(document).bind("click dblclick mousemove mousedown mouseenter mouseleave mouseout mouseover mouseup keypress", function () {
        clearTimeout(idle);
        idle = setTimeout(function(){
            LogOut(returnUrl);
        }, timeLeftInMiliSeconds);
    });
}