﻿
// strip comma before validating
$.validator.methods.range_old = $.validator.methods.range;
$.validator.methods.number_old = $.validator.methods.number;
$.validator.methods.range = function (value, element, param) {
    var strippedValue = value.replace(",", "");
    return this.optional(element) || (strippedValue >= param[0] && strippedValue <= param[1]);
};
$.validator.methods.number = function (value, element, param) {
    var strippedValue = value.replace(/,/g, "");
    return this.optional(element) || /^-?(?:\d+|\d{1,3}(?:,\d{3})+)(?:\.\d+)?$/.test(strippedValue);
};

// format is wordLimit: <limit>
$.validator.addMethod("wordLimit", function (value, element, param) {
    var stringInput = value;
    stringInput = stringInput.replace(/(^\s*)|(\s*$)/gi, "");
    stringInput = stringInput.replace(/[ ]{2,}/gi, " ");
    stringInput = stringInput.replace(/\n /, "\n");
    return this.optional(element) || stringInput.split(' ').length <= param;
}, "* Word Count Limit is {0}");

// param { target, default }
$.validator.addMethod("requiredIf", function (value, element, param) {
    var targetElementValue = $(param.target).val();
    var defaultValue = param.default;
    
    //console.log(targetElementValue === defaultValue ? true : value.trim() !== "");
    
    return targetElementValue === defaultValue ? true : value.trim() !== "";

}, "* This Value is Required");
