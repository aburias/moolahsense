﻿
///////////////////////////////////// Function for login at pressing enter button in password textbox ///////////////////////////////////


function LoginatKeyPress(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode == 13) {
        LoginUser();
    }
}

///////////////////////////////////// Function for redirecting to Home page ///////////////////////////////////////////////////
function gotoHome() {
    window.location.href = "moolahconnect.tpsoftwaresolutions";
}

////////////////////////////////// Function for show hide functionality of tabs in Borrower and Investor section /////////////////////////
function ShowhideborrowerAccountInfoSteps(step, partialview, Model, Location) {

    $("ul.tabs li").removeClass("active"); //Remove any "active" class
    $(".tab_content").hide(); //Hide all content
    if (step == 1) {

        $("ul.tabs li:first").addClass("active").show(); //Activate first tab
        $(".tab_content:first").show(); //Show first tab content

    }
    else if (step == 2) {

        $("ul.tabs li:first").next().addClass("active").show(); //Activate first tab
        $(".tab_content:first").next().show(); //Show first tab content
    }
    else if (step == 3) {

        $("ul.tabs li:first").next().next().addClass("active").show(); //Activate first tab
        $(".tab_content:first").next().next().show(); //Show first tab content
    }
    else if (step == 4) {

        $("ul.tabs li:last").addClass("active").show(); //Activate first tab
        $(".tab_content:last").show(); //Show first tab content
    }

    $.ajax({
        url: "/" + Location + "/ShowInvestorAccountDetailsModel",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{Partialview:'" + partialview + "',objaccount:'" + Model + "'}",
        success: function (msg) {

        }
    });

}

///////////////////////////////////////////// function for displaying div for upload documents /////////////////////////////////////////////////////////////

function ShowFileuploadDiv(documentID) {
    $('#hdndocumentid').val(documentID);
    $('#divfileupload').show();
}


function ShowDiv(Divtoshow) {
    $('#' + Divtoshow).show();
}

function Closepopupdiv(divtoHide) {
    $('#' + divtoHide).hide();
}

//////////////////////////////////////////////////////////////  function for closing any div ///////////////////////////////////////////////////////////////////////////////

function Closediv(divtoHide) {
    $('#' + divtoHide).hide();
    $('input:text').val("");
    $('textarea').val("");
}


////////////////////////////////////////////// Function for saving the knowledge assessment details for the user //////////////////////////////////////////////////////////

function SaveKnowledgeDetailsofInvestor() {
    $('#mask').showLoading();

    var Xmldocs = "<Info>";
    var subtext = "";
    var mainCount = 0;
    var spCount = 0;

    $('input.KA').each(function () {
        var idCol = parseInt($(this).val(), 10);
        var inRange = false;
        if ((idCol >= 6 && idCol <= 12) || (idCol >= 20 && idCol <= 26)) {
            inRange = true;
        }
        if ($(this).is(":checked")) {
            subtext = subtext + "<Details OptionId =\"" + $(this).val() + "\"  OptionValue=\"true\" />";
            if (inRange) {
                spCount = spCount + 1;
            }
        } else {
            if (!inRange) {
                mainCount = mainCount + 1;
            }
        }
    });

    if (mainCount > 0 || spCount <= 0) {
        bootbox.alert("Please tick all checkboxes. All are mandatory");
        $('#mask').hideLoading();
        return false;
    }

    $('select.KA').each(function () {
        subtext = subtext + "<Details OptionId =\"" + $(this).val() + "\"  OptionValue=\"selected\" ></Details>"
    });

    var empStatus;
    $('input.emp').each(function () {
        if ($(this).is(':checked')) {
            subtext = subtext + "<Details OptionId =\"" + $(this).val() + "\"  OptionValue=\"checked\" ></Details>"
            empStatus = $(this).val();
        }
    });

    if (empStatus == 32 && $('select.emp').val() == 0) {
        bootbox.alert("Please select your occupation.");
        $('#mask').hideLoading();
        return false;
    }

    var hasOtherDetailsNotProvided;

    $('select.emp').each(function () {

        subtext = subtext + "<Details OptionId =\"" + $(this).val() + "\"  OptionValue=\"selected\" ></Details>"

        var txtValue = $('#txt52').val();

        if (empStatus == 32 && $(this).val() == 51 && (txtValue == undefined || txtValue == '')) {
            hasOtherDetailsNotProvided = true;
        }
    });

    if (hasOtherDetailsNotProvided) {
        bootbox.alert("Please provide details about your occupation");
        $('#mask').hideLoading();
        return false;
    }

    subtext = subtext + "<Details OptionId =\"52\"  OptionValue=\"" + $('#txt52').val() + "\" ></Details>"


    Xmldocs = Xmldocs + subtext + "</Info>";

    $.ajax({
        url: "/Investor/SaveKnowledgeassessmentForInvestor",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{Xmldocs:'" + Xmldocs + "'}",
        success: function (msg) {
            if (msg == "Success") {
                //ShowhideborrowerAccountInfoSteps(3);
                window.location = 'ConfirmRegistration';
            }
            else {
                $('#mask').hideLoading();

                if (msg != null) {
                    bootbox.alert(msg);
                }
                else {
                    bootbox.alert("Some error occured while saving details.");
                }
            }

        }
    });
    return false;


}

/////////////////////////////////////// function for getting quick indications for monthly payments for an amout at some rate for some time /////////////////////////

function Getquickindicationsforpayment() {
    var $amt = parseFloat($('#Amount').val().replace(/,/g, ''));
    var $i = parseFloat($('#Rate').val().replace(/,/g, ''));
    var $term = GetTenor();
    if ($amt != "" && $i != "" && $term != "") {
        if ($amt >= gBorrowerMinAmount && $amt <= gBorrowerMaxAmount) {
            var flatmcDiscoutrate = 3;
            var mfee = 0;
            var yfee = 0;
            var intr = $i / 1200;
            var calc = $amt * intr / (1 - (Math.pow(1 / (1 + intr), $term)));
            var exm = Math.round(calc * 100) / 100;
            var exy = Math.round((calc * $term - yfee - $amt) * 100) / 100;
            var totalexm = Math.round(calc * $term * 100) / 100;

            $('#monthlypayment').html(exm);
            $('#totalpayment').html(totalexm);
            $('#MCFees').html(round((flatmcDiscoutrate / 100) * $amt * ($term / 12), 2));
            $('#totalInt').html(exy);

            $('#monthlypayment').formatCurrency();
            $('#totalpayment').formatCurrency();
            $('#MCFees').formatCurrency();
            $('#totalInt').formatCurrency();

            $('#quickpaymentindication').show();

            $.getJSON("/Borrower/GetTargetEIR", { amount: $amt, targetRate: $i, term: $term }, function (data) {
                $('#txtTargetEIR').val(data.targetEIR)
            });
        }
        else {
            $('#quickpaymentindication').hide();
        }
    }
    else {
        $('#quickpaymentindication').hide();
    }

}

function addPeriod(nStr) {
    nStr += '';
    x = nStr.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';
    var rgx = /(\d+)(\d{3})/;
    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }
    return x1 + x2;
}

function GetTenor() {
    var tenor = '';

    if ($('#radioTerm12').is(':checked'))
        tenor = '12';
    else if (($('#radioTerm24').is(':checked')))
        tenor = '24';
    else if (($('#radioTerm36').is(':checked')))
        tenor = '36';

    return tenor;
}


function round(value, places) {
    var multi, chars;
    if (places == 0) chars = 0;
    if (places == 1) chars = 2;
    if (places == 2) chars = 3;
    var str = "" + value;


    for (var i = 0; (str.substring(i, i + 1) != "." && i < str.length) ; ++i);
    if (i == 0) return str;
    return str.substring(0, i + chars);
}

//////////////////////////////////////// Function for getting questions and answers list for some Loan purpose /////////////////////////////////////////////////

function GetQuestionsrelatedtoLoanPurpose(callback) {
    var Loanpurposeid = $('#LoanPurposeId').val();
    if ($('#LoanPurpose_Id').find("option[value=" + Loanpurposeid + "]").text() == "Others") {
        //  $('#txtloanpurpose').show();
    }
    else {
        $.ajax({
            url: "/Borrower/GetQuestionsforLoanRequest",
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            data: "{LoanpurposeId:'" + Loanpurposeid + "'}",
            success: function (msg) {
                if (msg != "") {
                    $('#divquestions').html(msg);
                    callback && callback();
                }
                else {
                    alert("Some error occured while getting questions.");
                }

            }
        });
    }
    return false;
}


//////////////////////////////////////////////////// Function for saving Loan request //////////////////////////////////////////////

function SaveLoanRequestdescription2(isNext,successCallBack) {

    // init dto
    var data = {
        RequestId: $('#hdnRequestId').val(),
        Amount: $('#Amount').val().replace(/,/g, ""),
        Term: GetTenor(),
        Rate: $('#Rate').val().split(' ')[0],
        Ispersonalguarantee: $('#IspersonalGuarantee').val(),
        LoanpurposeId: $('#LoanPurposeId').val(),
        LoanPurposeDescription: $('#LoanPurposeDescription').val(),
        LoanImages: $('#hdnloanimages').val(),
        LoanDocs: "",
        NameasinNRICPassport: '',
        NricNumber: '',
        PassportNumber: '',
        Reasons: '',
        Residentialaddress1: '',
        Residentialaddress2: '',
        Telephone: '',
        Email: '',
        Postalcode: '',
        Nationality: '',
        DetailedCompanyPro: $('#DetailedCompanyPro').val(),
        SummaryCompanyPro: $('#SummaryCompanyPro').val(),
        Headline: $('#Headline').val(),
        LoanLogo: $('#hdnlogoimage').val(),
        NumberOfEmployees: $('#NumberOfEmployees').val(),
        PersonGuarantees: PersonGuarantees
        
};

    //if (data.Ispersonalguarantee === 'true') {
        //data.NameasinNRICPassport = $('#NameasinNRICPassport').val();
        //data.NricNumber = $('#NricNumber').val();
        //data.PassportNumber = $('#PassportNumber').val();
        //data.Reasons = $('#Reasons').val();
        //data.Residentialaddress = $('#Residentialaddress').val();
        //data.Telephone = $('#Telephone').val();
        //data.Email = $('#Email').val();
        //data.Postalcode = $('#Postalcode').val();
        //data.Residentialaddress1 = $('#Residentialaddress1').val();
        //data.Residentialaddress2 = $('#Residentialaddress2').val();
        //data.Nationality = $('#nationality').val();
    //}

    data.comments = GetFileComments('vid,phot,doc1,cover,doc2,gurantor,YouTubeLinks');
    $.ajax({
        url: "/Borrower/SaveLoanRequestdescription",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: JSON.stringify(data),
        success: function (msg) {
            if (msg == "Success") {
               if (successCallBack != null) {
                   successCallBack();
               }
                if (isNext) {
                    $("ul.tabs li").removeClass("active"); //Remove any "active" class
                    $(".tab_content").hide(); //Hide all content
                    $("ul.tabs li:first").next().addClass("active").show(); //Activate first tab
                    $(".tab_content:first").next().show("slow"); //Show first tab content
                }
                else {
                    bootbox.alert("Draft Saved Successfully.");
                }
            }
            else {
                bootbox.alert("System error! Please try again.");
            }
        }
    });

    $('#hdnloanimages').val('');
    $('#hdnloandocs').val('');
    return false;
}

function SaveLoanRequestdescription() {

    var Amount = ($('#Amount').val().replace(/,/g, ""));
    if ((Amount >= 100000 && Amount <= 300000)) {
        $('#amountcheck').html('');
        /////////////////////////////// XML docs for questions list ////////////////////////////////////////

        var Xmldocs = "<Info>";
        var subtext = "";
        $('#divquestions div.fl ').each(function () {

            var QuestionId = $(this).find("div").attr("id");
            var Answer = $(this).find("textarea").val();
            subtext = subtext + "<Details QuestionId =\"" + QuestionId + "\"  Answer=\"" + Answer + "\" />"

        });

        Xmldocs = Xmldocs + subtext + "</Info>";

        ////////////////////////////////// Other parameters for Loan request ///////////////////////////////////////////////////

        var Rate = $('#Rate').val().split(' ')[0];
        var Terms = GetTenor();
        var LoanpurposeId = $('#LoanPurposeId').val();
        var VideoDiscription = $('#VideoDiscription').val();
        var IsPersonalguarantee = $('#IspersonalGuarantee').val();
        var DetailedCompanyPro = $('#DetailedCompanyPro').val();
        var SummaryCompanyPro = $('#SummaryCompanyPro').val();

        var Name = "";
        var Designation = "";
        var NricNumber = "";
        var Residentialaddress = "";
        var Postalcode = "";
        var Telephone = "";
        var Email = "";
        var Reasons = "";
        var loanimages = $('#hdnloanimages').val();
        var loandocs = "";
        var logoimage = $('#hdnlogoimage').val();
        var passportNumber = $('#PassportNumber').val();

        //var videoData = $('#hdnloanimages').val();
        //var photoData = $('#hdnloandocs').val();
        //var docsData = $('#hdnlogoimage').val();


        //if (IsPersonalguarantee == "true") {
        Name = $('#NameasinNRICPassport').val();
        Designation = $('#Designation').val();
        NricNumber = $('#NricNumber').val();
        Residentialaddress = $('#Residentialaddress').val();
        Postalcode = $('#Postalcode').val();
        Telephone = $('#Telephone').val();
        Email = $('#Email').val();
        Reasons = $('#Reasons').val();
        loandocs = $('#hdnloandocs').val();
        //}
        var iserror = "";
        if (Amount == "") {
            iserror += "* Amount is required";
        }
        if (Rate == "") {
            iserror += "* EIR is required";
        }
        //if (VideoDiscription == "") {
        //    iserror += "* Video description required";
        //}



        if ($('#NameasinNRICPassport').attr("disabled") == undefined) {
            if (Name == "") {
                iserror += "* Name is required";
            }
        }
        //if ($('#Designation').attr("disabled") == undefined) {
        //    if (Designation == "") {
        //        iserror += "* Designation 12312134";
        //    }
        //}
        if ($('#Residentialaddress').attr("disabled") == undefined) {
            if (Residentialaddress == "") {
                iserror += "* Residential Address is required";
            }
        }
        if ($('#Postalcode').attr("disabled") == undefined) {
            if (Postalcode == "") {
                iserror += "* Postalcode is required";
            }
        }


        if ($('#Telephone').attr("disabled") == undefined) {
            if (Telephone == "") {
                iserror += "* Telephone number is required";
            }
        }

        if (!(loanimages || loandocs || logoimage)) {
            iserror += "*Upload at least a Video or Photo";
        }


        if (iserror.length > 0) {
            return true;
        }
        else {
            //comment for my branch
            var data = {
                Amount: Amount,
                Term: Terms,
                Rate: Rate,
                Ispersonalguarantee: IsPersonalguarantee,
                LoanpurposeId: LoanpurposeId,
                XMLquestionswithanswers: Xmldocs,
                LoanImages: loanimages,
                LoanDocs: loandocs,
                NameasinNRICPassport: Name,
                NricNumber: NricNumber,
                PassportNumber: passportNumber,
                Reasons: Reasons,
                Residentialaddress: Residentialaddress,
                Telephone: Telephone,
                Email: Email,
                Postalcode: Postalcode,
                DetailedCompanyPro: DetailedCompanyPro,
                SummaryCompanyPro: SummaryCompanyPro,
                LoanLogo: logoimage
            };

            //"{Amount:'" + Amount + "', Terms:'" + Terms + "', Rate:'" + Rate + "', Ispersonalguarantee:'" + IsPersonalguarantee + "', LoanpurposeID:'" 
            //                    + LoanpurposeId + "', XMLquestionswithanswers:'" + Xmldocs + "', Loanimages:'" + loanimages 
            //                    + "', LoanDocs:'" + loandocs + "',NameasinNRICPassport:'" + Name + "',Designation:'" + Designation
            //                    + "', NRICPassport:'" + NRIC_Passport + "', Reasons:'" + Reasons + "', Residentialaddress:'" + Residentialaddress
            //                    + "', Telephone:'" + Telephone + "', Email:'" + Email + "', Postalcode:'" + Postalcode
            //                    + "',VideoDiscription:'" + VideoDiscription + "',DetailedCompanyPro:'" + DetailedCompanyPro
            //                    + "',Logoimage:'" + logoimage + "'}",
            $.ajax({
                url: "/Borrower/SaveLoanRequestdescription",
                type: "POST",
                dataType: "json",
                contentType: 'application/json',
                data: JSON.stringify(data),
                success: function (msg) {

                    if (msg == "Success") {
                        $("ul.tabs li").removeClass("active"); //Remove any "active" class
                        $(".tab_content").hide(); //Hide all content
                        $("ul.tabs li:first").next().addClass("active").show(); //Activate first tab
                        $(".tab_content:first").next().show("slow"); //Show first tab content
                        //   window.location = '/Borrower/LoanRequest';
                    }



                }
            });

            $('#hdnloanimages').val('');
            $('#hdnloandocs').val('');
            return false;
        }
    }
    else {
        $('#amountcheck').html('100k-300k');
    }
}


function ShowHidePersonalguaranteediv() {

    //if ($('#IspersonalGuarantee').val() == "true") {
    $('#personalguaranteeinfo').show();
    $('#NameasinNRICPassport').removeAttr("disabled");
    $('#Designation').removeAttr("disabled");
    $('#Residentialaddress').removeAttr("disabled");
    $('#Postalcode').removeAttr("disabled");
    $('#Telephone').removeAttr("disabled");
    /*
    }
    else {
        $('#personalguaranteeinfo').hide();
        $('#NameasinNRICPassport').attr("disabled", "disabled");
        $('#Designation').attr("disabled", "disabled");
        $('#Residentialaddress').attr("disabled", "disabled");
        $('#Postalcode').attr("disabled", "disabled");
        $('#Telephone').attr("disabled", "disabled");
    }
    */

}

// methos for hide and show for custom moolah perk /////

function getcustomperkTextbox() {

    var moolahperkid = $('#MoolahPerkset').val();
    if (moolahperkid == "Custom") {

        $('#divcustomeperk').fadeIn(0001);
    }
    else {
        $('#divcustomeperk').hide();
    }
}


// methos for hide and show for Letigationdiv /////

function getLetigationdivHide() {


    if ($('#drpIslegitatins').val() == "true") {

        $('#Letigationdiv').fadeIn('slow');

    }
    else {
        $('#Letigationdiv').fadeOut('slow');

        $('#dvtblOverDraft').fadeOut('slow');
        $('#dvtblLoan').fadeOut('slow');
        $('#dvtblTrade').fadeOut('slow');
    }
}

//// Method for calculatiing current ration //////////
function GetCurrentratio(txt1, txt2, resulttxt) {
    var txt1Val = $('#' + txt1).val().split(",").join("");
    var txt2Val = $('#' + txt2).val().split(",").join("");

    if (txt1Val != "" && txt2Val != "" && txt1Val != "0" && txt2Val != "0") {
        $('#' + resulttxt).val((txt1Val / txt2Val).toFixed(2));
    }
}

//// Method for calculatiing current ration //////////
function GetDebtEquity(txt1, txt2, resulttxt) {
    var txt1Val = $('#' + txt1).val().split(",").join("");
    var txt2Val = $('#' + txt2).val().split(",").join("");
    var ratio = (txt1Val / txt2Val).toFixed(2);

    if (txt1Val != "" && txt2Val != "" && txt1Val != "0" && txt2Val != "0") {
        $('#' + resulttxt).val(ratio);
    }
}

function TotalDebtLYOnChange() {
    GetDebtEquity('TotalDebt_LY', 'TotalShareholdersequity_LY', 'Debt_Equity_LY', 'Debt_Equity_LY_Hdn');
    GetDebtEquity('TotalDebt_LY', 'EBITDA_LY', 'DebtEBITDARatio_LY', 'DebtEBITDARatio_LY_Hdn');
}

function TotalDebtPYOnChange() {
    GetDebtEquity('TotalDebt_PY', 'TotalShareholdersequity_PY', 'Debt_Equity_PY', 'Debt_Equity_PY_Hdn');
    GetDebtEquity('TotalDebt_PY', 'EBITDA_PY', 'DebtEBITDARatio_PY', 'DebtEBITDARatio_PY_Hdn');
}

function GetDebtEquity(txt1, txt2, resulttxt, resulttxtHdn) {
    var txt1Val = $('#' + txt1).val().split(",").join("");
    var txt2Val = $('#' + txt2).val().split(",").join("");
    var ratio = (txt1Val / txt2Val).toFixed(2);

    if (txt1Val != "" && txt2Val != "" && txt1Val != "0" && txt2Val != "0") {
        $('#' + resulttxtHdn).val(ratio);
        $('#' + resulttxt).val(ratio);
        /*if (ratio >= 0) {
            $('#' + resulttxt).val(ratio);
        } else {
            $('#' + resulttxt).val('Negative Debt/Equity Ratio');
        }*/
    }
}



////////////////////// popup function //////////////


/************** start: functions. **************/

var popupStatus = 0; // set value

function loadPopup(divid, divtohopecity) {


    if (popupStatus == 0) { // if value is 0, show popup
        $("#" + divid).fadeIn(0500); // fadein popup div
        if (divtohopecity == "") {
            $("#backgroundPopup").css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("#backgroundPopup").fadeIn(0001);

        }
        else {
            $("." + divtohopecity).css("opacity", "0.7"); // css opacity, supports IE7, IE8
            $("." + divtohopecity).fadeIn(0001);
        }
        popupStatus = 1; // and set value to 1
    }
}

function disablePopup(divid, divtohopecity, redirecturl) {
    if (popupStatus == 1) {
        // if value is 1, close popup
        $("#" + divid).fadeOut("normal");

        if (divtohopecity == "") {

            $("#backgroundPopup").fadeOut("normal");
        }
        else {
            $("." + divtohopecity).fadeOut("normal");
            if (redirecturl != "" && redirecturl != undefined) {

                document.location.href = redirecturl;

            }
        }
        popupStatus = 0;  // and set value to 0
    }


}

///////////////////////////////////Check dropdwon value and display fields accordingly/////////////////////
function Changefields(ddlValue) {

    switch (ddlValue) {
        case 'Factoring Discount':
            $("#LitigationType option[value='" + ddlValue + "']").attr("selected", "selected");
            $('#hHeading').html('Factoring/Invoicing Discount');
            $('#litigationitem_loan_Div').fadeOut('normal');
            $('#litigationitem_tradecredeitosDiv').fadeOut('normal');;
            $('#litigationitemDiv').fadeIn(0001);

            break;
        case 'Overdraft Facility':
            $("#LitigationType option[value='" + ddlValue + "']").attr("selected", "selected");
            $('#hHeading').html('Overdraft Facility');
            $('#litigationitem_loan_Div').fadeOut('normal');
            $('#litigationitem_tradecredeitosDiv').fadeOut('normal');;
            $('#litigationitemDiv').fadeIn(0001);
            break;
        case 'Unsecured Loan':
            $(".LitigationTypeLoan option[value='" + ddlValue + "']").attr("selected", "selected");
            $('#dvAsset').fadeOut('normal');
            $('#hHeadingLoans').html('Unsecured Loans');
            $('#litigationitemDiv').fadeOut('normal');
            $('#litigationitem_tradecredeitosDiv').fadeOut('normal');
            $('#litigationitem_loan_Div').fadeIn(0001);
            $('#tt').show();
            break;
        case 'Secured Loan':
            $(".LitigationTypeLoan option[value='" + ddlValue + "']").attr("selected", "selected");
            $('#litigationitemDiv').fadeOut('normal');
            $('#litigationitem_tradecredeitosDiv').fadeOut('normal');
            $('#dvAsset').fadeIn(0001);
            $('#hHeadingLoans').html('Secured Loans');
            $('#litigationitem_loan_Div').fadeIn(0001);
            $('#tt').show();
            break;
        case 'Trade Creditors':

            $(".LitigationTypeTrade option[value='" + ddlValue + "']").attr("selected", "selected");
            $('#tt').hide();
            $('#litigationitem_loan_Div').hide();
            $('#litigationitemDiv').fadeOut('normal');
            $('#litigationitem_tradecredeitosDiv').fadeIn(0001);


            break;
    }

}


/************** end: functions. **************/


//////////////////////////////////////////////////// Function for saving Loan request moolahcore //////////////////////////////////////////////


function SaveLoanRequestMoolahcoreJson(isNext) {
    if (!$('#Loanrequestmoolahcoreedit').valid()) {
        return false;
    }

    var data = $("#Loanrequestmoolahcoreedit").serialize();
    data = data.split("%2C").join("");

    $.ajax({
        url: "/Borrower/SaveLoanRequestMoolahCore",
        type: "POST",
        data: data,
        success: function (msg) {

            if (msg == "Success") {
                if (isNext) {
                    $("ul.tabs li").removeClass("active"); //Remove any "active" class
                    $(".tab_content").hide(); //Hide all content
                    $("ul.tabs li:first").next().next().addClass("active").show(); //Activate first tab
                    $(".tab_content:first").next().next().show("slow"); //Show first tab content
                }
                else {
                    bootbox.alert("Draft Saved Successfully.");
                }

            }
        }
    });
    return false;
}


/////////////////////////////////////////Make collection of over draft facility or Factoring Discount/////////////////////////////

function SaveOverDraftFacilityorFactoring(LitigationID) {

    var litigationtype = '';
    var OutstandingAmount = null;
    var CreditLimit = null;
    var RenewalDate = null;
    var RepaymentAmount = null;
    var _RepaymentPeriodSet = '';
    var DateofLastPayment = null;
    var Description = '';
    var PaymentTerms = null;
    var UsageinLastMonth = null;
    switch (LitigationID) {
        case '0':
            litigationtype = $('#LitigationType').val();
            OutstandingAmount = $('.AmountOutstandingdraft').val();
            CreditLimit = $('#CreditLimit').val();
            UsageinLastMonth = $('#UsageinLastMonth').val();
            RenewalDate = $('#RenewalDate').val();
            ClearTextboxValues();
            break;

        case '1':
            litigationtype = $('.LitigationTypeLoan').val();
            OutstandingAmount = $('.AmountOutstandingLoan').val();
            RepaymentAmount = $('.RepaymentAmountLoan').val();
            _RepaymentPeriodSet = $('#_RepaymentPeriodSet').val();
            DateofLastPayment = $('#DateofLastPayment').val();
            if (litigationtype == 'Secured Loan') {

                Description = $('#Description').val();
            }
            ClearTextboxValues();
            break;

        case '2':
            litigationtype = $('.LitigationTypeTrade').val();
            OutstandingAmount = $('.AmountOutstandingTrade').val();
            PaymentTerms = $('#PaymentTerms').val();
            ClearTextboxValues();
            break;
    }
    $.ajax({
        url: "/Borrower/Makecollection",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{LitigationType:'" + litigationtype + "', AmountOutstanding:'" + parseFloat(OutstandingAmount) + "', CreditLimit:'" + parseFloat(CreditLimit) + "', UsageinLastMonth:'" + parseFloat(UsageinLastMonth) + "', RenewalDate:'" + RenewalDate + "', RepaymentAmount:'" + parseFloat(RepaymentAmount) + "', RepaymentPeriod:'" + _RepaymentPeriodSet + "', DateofLastPayment:'" + DateofLastPayment + "', Description:'" + Description + "', PaymentTerms:'" + parseInt(PaymentTerms) + "'}",
        success: function (msg) {
            AppendRecord(litigationtype, OutstandingAmount, CreditLimit, UsageinLastMonth, RenewalDate, RepaymentAmount, _RepaymentPeriodSet, DateofLastPayment, Description, PaymentTerms, msg);
        }
    });
}

////clear outstanding litgation textboxes
function ClearTextboxValues() {
    $('.clrtxt').val('');
}



////////////////////////////////////Append Record to table for overdrafting or discounting litigation////////////////////////

function AppendRecord(litigationtype, OutstandingAmount, CreditLimit, UsageinLastMonth, RenewalDate, RepaymentAmount, _RepaymentPeriodSet, DateofLastPayment, Description, PaymentTerms, DeleteID) {
    switch (litigationtype) {
        case 'Overdraft Facility':
            var newrow = "<tr id=" + DeleteID + " class=bg_col1><td class=bdr_left><a>" + OutstandingAmount + "</a></td><td>" + CreditLimit + " </td><td>" + UsageinLastMonth + "</td><td>" + RenewalDate + "  </td><td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + DeleteID + ",1)' /> </td></tr>";
            $('#tblOverDraft').append(newrow);
            $('#dvtblOverDraft').fadeIn(0001);
            break;
        case 'Factoring Discount':
            var newrow = "<tr id=" + DeleteID + " class=bg_col1><td class=bdr_left><a>" + OutstandingAmount + "</a></td><td>" + CreditLimit + " </td><td>" + UsageinLastMonth + "</td><td>" + RenewalDate + "  </td><td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + DeleteID + ",2)' /> </td></tr>";
            $('#tblFactoring').append(newrow);
            $('#dvtblFactoring').fadeIn(0001);
            break;
        case 'Unsecured Loan':
            var newrow = "<tr id=" + DeleteID + " class=bg_col1><td class=bdr_left><a>" + OutstandingAmount + "</a></td><td>" + RepaymentAmount + " </td><td>" + _RepaymentPeriodSet + "</td><td>" + DateofLastPayment + "  </td><td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + DeleteID + ",3)' /> </td></tr>";
            $('#tblLoan').append(newrow);
            $('#dvtblLoan').fadeIn(0001);
            break;
        case 'Secured Loan':
            var newrow = "<tr id=" + DeleteID + " class=bg_col1><td class=bdr_left><a>" + OutstandingAmount + "</a></td><td>" + RepaymentAmount + " </td><td>" + _RepaymentPeriodSet + "</td><td>" + DateofLastPayment + "  </td><td> " + Description + "</td><td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + DeleteID + ",4)' /> </td></tr>";
            $('#tblLoanSe').append(newrow);
            $('#dvtblLoanSe').fadeIn(0001);
            break;
        case 'Trade Creditors':
            var newrow = "<tr id=" + DeleteID + " class=bg_col1><td class=bdr_left><a>" + OutstandingAmount + "</a></td><td>" + PaymentTerms + " </td><td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + DeleteID + ",5)' /> </td></tr>";
            $('#tblTrade').append(newrow);
            $('#dvtblTrade').fadeIn(0001);
            break;
    }
    return newrow;
}
///////////////////////////Function to delete from collection and hide from table.This function will be used with every collection for saving litigation///////////////
function DeleteRecordFromCollection(DeleteIndex, type) {

    $.ajax({
        url: "/Borrower/DeleteRecordFromCollection",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{DeleteIndex:'" + parseInt(DeleteIndex) + "'}",
        success: function (msg) {
            $('#' + DeleteIndex).remove();
        }
    });




    if ($('#tblOverDraft >tbody >tr').length <= 2 && type == '1') {

        $('#dvtblOverDraft').hide();
    }


    if ($('#tblLoanSe >tbody >tr').length <= 2 && type == '4') {
        $('#dvtblLoanSe').hide();
    }

    if ($('#tblLoan >tbody >tr').length <= 2 && type == '3') {
        $('#dvtblLoan').hide();
    }
    if ($('#tblTrade >tbody >tr').length <= 2 && type == '5') {
        $('#dvtblTrade').hide();
    }

    if ($('#tblFactoring >tbody >tr').length <= 2 && type == '2') {
        $('#dvtblFactoring').hide();
    }
}


//////////////////////////////////////////////////// Function for saving Loan request moolah peri //////////////////////////////////////////////


function SaveLoanRequestMoolahperi() {




    var data = $("#Loanrequestmoolahperi").serialize();

    $.ajax({
        url: "/Borrower/SaveLoanRequestMoolahPeri",
        type: "POST",
        data: data,
        success: function (msg) {

            if (msg == "Success") {
                bootbox.alert('Your request is pending for admin review.', function () {
                    window.location.href = '/Issuer/Index';
                });

            }
        }
    });

    return false;
}


function DeleteDocument(DocumentID) {
    var DocumentName = $('#documentname_' + DocumentID).html();
    $.ajax({
        url: "/Borrower/DeleteDocument",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{DocumentID:'" + parseInt(DocumentID) + "',FileName:'" + DocumentName + "'}",
        success: function (msg) {
            $('#' + DocumentID).fadeOut('normal');
        }
    });
}


//////////////////////////////////////////////////// Function for saving Loan request //////////////////////////////////////////////


function SaveLoanRequestdescriptionEdit() {

    /////////////////////////////// XML docs for questions list ////////////////////////////////////////

    var Xmldocs = "<Info>";
    var subtext = "";
    $('#divquestions div.fl ').each(function () {

        var QuestionId = $(this).find("div").attr("id");
        var Answer = $(this).find("textarea").val();
        subtext = subtext + "<Details QuestionId =\"" + QuestionId + "\"  Answer=\"" + Answer + "\" />"

    });

    Xmldocs = Xmldocs + subtext + "</Info>";

    ////////////////////////////////// Other parameters for Loan request ///////////////////////////////////////////////////

    var Amount = ($('#Amount').val().replace(/,/g, ""));
    var Rate = $('#Rate').val().split(' ')[0];;
    var Terms = $('#Term:checked').val();
    var LoanpurposeId = $('#LoanPurpose_Id').val();
    var DetailedCompanyPro = $('#DetailedCompanyPro').val();


    var IsPersonalguarantee = $('#IspersonalGuarantee').val();

    var Name = "";
    var Designation = "";
    var NRIC_Passport = "";
    var Residentialaddress = "";
    var Postalcode = "";
    var Telephone = "";
    var Email = "";
    var Reasons = "";
    var loanimages = $('#hdnloanimages').val();
    var loandocs = "";

    //if (IsPersonalguarantee == "true") {
    Name = $('#NameasinNRICPassport').val();
    Designation = $('#Designation').val();
    NRIC_Passport = $('#NRICPassport').val();
    Residentialaddress = $('#Residentialaddress').val();
    Postalcode = $('#Postalcode').val();
    Telephone = $('#Telephone').val();
    Email = $('#Email').val();
    Reasons = $('#Reasons').val();
    loandocs = $('#hdnloandocs').val();
    //}
    var iserror = "";
    if (Amount == "") {
        iserror += "* Amount is required";
    }
    if (Rate == "") {
        iserror += "* Rate is required";
    }


    if ($('#NameasinNRICPassport').attr("disabled") == undefined) {
        if (Name == "") {
            iserror += "* Name is required";
        }
    }
    if ($('#Designation').attr("disabled") == undefined) {
        if (Designation == "") {
            iserror += "* Designation 12312134";
        }
    }
    if ($('#Residentialaddress').attr("disabled") == undefined) {
        if (Residentialaddress == "") {
            iserror += "* Residential Address is required";
        }
    }
    if ($('#Postalcode').attr("disabled") == undefined) {
        if (Postalcode == "") {
            iserror += "* Postalcode is required";
        }
    }
    if ($('#Telephone').attr("disabled") == undefined) {
        if (Telephone == "") {
            iserror += "* Telephone number is required";
        }
    }


    if (iserror.length > 0) {
        return true;
    }
    else {
        $.ajax({
            url: "/Borrower/SaveLoanRequestdescriptionEdit",
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            data: "{Amount:'" + Amount + "', Terms:'" + Terms + "', Rate:'" + Rate + "', Ispersonalguarantee:'" + IsPersonalguarantee + "', LoanpurposeID:'" + LoanpurposeId + "', XMLquestionswithanswers:'" + Xmldocs + "', Loanimages:'" + loanimages + "', LoanDocs:'" + loandocs + "',NameasinNRICPassport:'" + Name + "',Designation:'" + Designation + "', NRICPassport:'" + NRIC_Passport + "', Reasons:'" + Reasons + "', Residentialaddress:'" + Residentialaddress + "', Telephone:'" + Telephone + "', Email:'" + Email + "', Postalcode:'" + Postalcode + "', DetailedCompanyPro:'" + DetailedCompanyPro + "'}",
            success: function (msg) {


                if (msg != "") {
                    $('#tab2').html(msg.html);
                    $("ul.tabs li").removeClass("active"); //Remove any "active" class
                    $(".tab_content").hide(); //Hide all content
                    $("ul.tabs li:first").next().addClass("active").show(); //Activate first tab
                    $(".tab_content:first").next().show("slow"); //Show first tab content
                    //   window.location = '/Borrower/LoanRequest';
                }

                //else if (msg == jresult) {

                //}

            }
        });
        return false;
    }


}




//////////////////////////////////////////////////// Function for updating Loan request moolahcore //////////////////////////////////////////////


function UpdateLoanRequestMoolahCore() {



    var data = $("#UpdateLoanRequestMoolahCore").serialize();

    $.ajax({
        url: "/Borrower/UpdateLoanRequestMoolahCore",
        type: "POST",
        data: data,
        success: function (msg) {


            if (msg != "") {
                $('#tab3').html(msg.html);
                $("ul.tabs li").removeClass("active"); //Remove any "active" class
                $(".tab_content").hide(); //Hide all content
                $("ul.tabs li:first").next().next().addClass("active").show(); //Activate first tab
                $(".tab_content:first").next().next().show("slow"); //Show first tab content
                //   window.location = '/Borrower/LoanRequest';
            }


            //else if (msg == jresult) {

            //}

        }
    });
    return false;
}
////////////////////////////////  Show litigation records in case of edit ////////////////////////

function Showlitigationtable() {

    if ($('#drpIslegitatins').val() == "true") {

        if ($('#tblOverDraft tr').length > 1) {
            $('#dvtblOverDraft').show();
        }

        if ($('#tblLoan tr').length > 1) {
            $('#dvtblLoan').show();
        }
        if ($('#tblTrade tr').length > 1) {
            $('#dvtblTrade').show();
        }
    }

}

///////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////// Function for amortization of loan ///////////////////////////
//function LoanAmortization(LoanAmount, period, installementMonth, installmentyear, Rate) 
function LoanAmortization(LoanAmount, period, Rate) {

    if (LoanAmount != "" && Rate != "") {
        var monthValue = new Date().getMonth();

        var yearValue = new Date().getYear();
        if (LoanAmount !== "" && period !== "" && LoanAmount != 0 && period != 0 && Rate !== "" && monthValue !== "" && monthValue >= 0 && yearValue !== "" && yearValue != 0) {

            var loanAmt = LoanAmount;

            var emi = calculateEMI(loanAmt, Rate, 12);
            var interestRate = Rate;
            var interestRateForMonth = interestRate / 12; // (Monthly Rate of Interest in %)
            var interestRateForMonthFraction = interestRateForMonth / 100; // (Monthly Interest Rate expressed as a fraction)

            var loanOustanding = loanAmt;
            var totalPayment = 0;
            var totalInterestPortion = 0;
            var totalPrincipal = 0;
            var month = 0;
            var strData = "<table border='0' cellspacing='1'   bgcolor='#d5cfb1' cellpadding='1'><tr><td width='14%' align='center' bgcolor='#88a943' class='tblwt_txt' height='22'>Installment No</td><td width='14%'  align='center' bgcolor='#88a943' class='tblwt_txt' height='22'>Installment Date</td><td width='14%'  align='center' bgcolor='#88a943' class='tblwt_txt' height='22'>Opening Balance</td><td width='14%'  align='center' bgcolor='#88a943' class='tblwt_txt' height='22'>EMI</td><td width='14%'  align='center' bgcolor='#88a943' class='tblwt_txt' height='22'><strong>Loan Outstanding</strong></td><td width='14%'  align='center' bgcolor='#88a943' class='tblwt_txt' height='22'>Interest</td><td width='14%'  align='center' bgcolor='#88a943' class='tblwt_txt' height='22'>Principal</td></tr>";
            for (i = 1; i <= period; i++) {

                if (monthValue != 0)
                    month = parseInt(monthValue) + parseInt(i) - 1;
                else
                    month = parseInt(month) + 1;
                if (month > 12) {
                    year = parseInt(yearValue) + 1;
                    yearValue = year;
                    monthValue = 0;
                    month = parseInt(monthValue) + 1;
                } else {
                    year = yearValue;
                }

                if (month < 10) {
                    var installmentDate = '0' + month + '/' + year;
                } else {
                    var installmentDate = month + '/' + year;
                }


                if (loanOustanding == loanAmt) {
                    loanOustanding = loanAmt;
                    strData += '<tr><td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowInstallmentNo' + i + '" ><strong>' + i + '</strong></td>';
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowInstallmentDate' + i + '"><strong>' + installmentDate + '</strong></td>';
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowOpeningBalance' + i + '"><strong>' + getFormattedNumber(loanOustanding) + '</strong></td>';
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowPayment' + i + '"><strong>' + getFormattedNumber(emi) + '</strong></td>';
                    totalPayment = parseFloat(totalPayment) + parseFloat(emi);
                    interestPortion = loanOustanding * interestRateForMonthFraction;
                    interestPortion = roundDecimals(interestPortion, 0);

                } else {
                    strData += '<tr><td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowInstallmentNo' + i + '">' + i + '</td>';
                    strData += '<td class="tbl_txt" bgcolor="#FFFFFF" id="txtRowInstallmentDate' + i + '">' + installmentDate + '</td>';
                    strData += '<td class="tbl_txt" bgcolor="#FFFFFF" id="txtRowOpeningBalance' + i + '">' + getFormattedNumber(loanOustanding) + '</td>';
                    strData += '<td class="tbl_txt" bgcolor="#FFFFFF" id="txtRowPayment' + i + '">' + getFormattedNumber(emi) + '</td>';
                    totalPayment = parseFloat(totalPayment) + parseFloat(emi);
                    interestPortion = loanOustanding * interestRateForMonthFraction;
                    interestPortion = roundDecimals(interestPortion, 0);

                }

                loanOustanding = parseFloat(loanOustanding) + parseFloat(interestPortion) - parseFloat(emi);
                loanOustanding = roundDecimals(loanOustanding, 0);
                if (i == 1) {
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowloanOustanding' + i + '"><strong>' + getFormattedNumber(loanOustanding) + '</strong></td>';
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowintrestPortion' + i + '"><strong>' + getFormattedNumber(interestPortion) + '</strong></td>';
                } else {
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowloanOustanding' + i + '"><strong>' + getFormattedNumber(loanOustanding) + '</strong></td>';
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowintrestPortion' + i + '">' + getFormattedNumber(interestPortion) + '</td>';
                }
                totalInterestPortion = parseFloat(totalInterestPortion) + parseFloat(interestPortion);
                principal = roundDecimals(emi - interestPortion, 0);
                if (i == 1) {
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowprincipalPortion' + i + '"><strong>' + getFormattedNumber(principal) + '</strong></td></tr>';
                } else {
                    strData += '<td  class="tbl_txt" bgcolor="#FFFFFF" id="txtRowprincipalPortion' + i + '">' + getFormattedNumber(principal) + '</td></tr>';
                }
                totalPrincipal = parseFloat(totalPrincipal) + parseFloat(principal);

            }
            strData += "<tr><td  class='tbl_txt' bgcolor='#FFFFFF' >Total:</td><td bgcolor='#FFFFFF'></td><td  bgcolor='#FFFFFF' class='tbl_txt'></td><td bgcolor='#FFFFFF' class='tbl_txt'>" + getFormattedNumber(totalPayment) + "</td><td bgcolor='#FFFFFF' class='tbl_txt'></td><td bgcolor='#FFFFFF' class='tbl_txt'>" + getFormattedNumber(totalInterestPortion) + "</td><td bgcolor='#FFFFFF' class='tbl_txt'>" + getFormattedNumber(totalPrincipal) + "</td></tr></table>";
            //if (parseInt(formObj.installment.value) > parseInt(formObj.tenure.value) || parseInt(formObj.installment.value) == 0) {
            //    document.getElementById('tblinstallmentDetails').style.display = "none";
            //    alert('The Installment must be less than or equal to the Tenure')
            //}

            document.getElementById('tblpaymentsDetails').innerHTML = strData;
            loadPopup('dvRepayment', '');
            //for (i = 1; i <= formObj.tenure.value; i++) {
            //    if (i == formObj.installment.value) {
            //        document.getElementById('tblinstallmentDetails').style.display = "block";
            //        document.getElementById('openingBalance').innerHTML = document.getElementById('txtRowOpeningBalance' + i).innerHTML;
            //        document.getElementById('payment').innerHTML = document.getElementById('txtRowPayment' + i).innerHTML;
            //        document.getElementById('loanOustanding').innerHTML = document.getElementById('txtRowloanOustanding' + i).innerHTML;
            //        document.getElementById('intrestPortion').innerHTML = document.getElementById('txtRowintrestPortion' + i).innerHTML;
            //        document.getElementById('principalPortion').innerHTML = document.getElementById('txtRowprincipalPortion' + i).innerHTML;
            //        document.getElementById('installmentNumber').innerHTML = document.getElementById('txtRowInstallmentNo' + i).innerHTML;
            //        document.getElementById('installmentDate').innerHTML = document.getElementById('txtRowInstallmentDate' + i).innerHTML;

            //    }
            //}
        }
    }
    else {
        alert('Please enter offer amount and rate');

    }



}


/////////////////////// function for emi calculation /////////////////////////
function calculateEMI(LoanAmount, Rate, period) {
    var loanAmt, interestRate, tenure;
    loanAmt = LoanAmount;
    tenure = period;
    interestRate = Rate;
    if (parseFloat(interestRate) != 0) {
        var interestRateForMonth = interestRate / 12; // (Monthly Rate of Interest in %)
        var interestRateForMonthFraction = interestRateForMonth / 100; // (Monthly Interest Rate expressed as a fraction)
        var emi = 1 / Math.pow((1 + interestRateForMonthFraction), tenure);
        var emiPerLakh = (loanAmt * interestRateForMonthFraction) / (1 - emi) // (EMI per lakh borrowed)
        emiPerLakh = roundDecimals(emiPerLakh, 0);
        return emiPerLakh;
    } else {
        var emi = loanAmt / tenure;
        var emiPerLakh = roundDecimals(emi, 0);
        return emiPerLakh;
    }
}
/************* EMI Calculations End********************/


///////////////////////// function for decimal rounding ////////////////////
function roundDecimals(original_number, decimals) {

    var result1 = original_number * Math.pow(10, decimals)

    var result2 = Math.round(result1)

    var result3 = result2 / Math.pow(10, decimals)

    return (result3)

}

///////////////////////// get number to framated asin comma ////////////////////
function getFormattedNumber(fieldvalue) {
    //var fieldvalue = document.getElementById(field).value;
    fieldvalue = fieldvalue;

    //number = fieldvalue.substr(0,fieldvalue.length-3);
    number = fieldvalue
    if (number.length > 3 && number.length <= 12) {
        var last3Digits = number.substring(number.length - 3, number.length);
        var numExceptLastDigits = number.substring(0, number.length - 3)
        var formatted = numExceptLastDigits;
        //var formatted = makeComma(numExceptLastDigits);
        formatted = "$. " + formatted + "," + last3Digits;
        return formatted;
    }
    return "$. " + number;


}

////////////////////////////////////Show Investor Type////////////////////////////
function ShowInverstorType() {

    var value = $("input:radio[name=Userrole]:checked").val();

    if (value == "Borrower") {
        $('#dvInvstorType').fadeOut('normal');
    }
    else if (value == "Investor") {
        $('#dvInvstorType').fadeIn('normal');
    }
}
function HideInverstorType() {
    $('#dvInvstorType').fadeOut('normal');
}
///////////////////////////////////end///////////////////////////

//////////////////////////////////Clear Textboxes of loan offer on loan summary//////////////////////////////////
function ClearTextBoxes() {
    $('.graph_input_small').val('');
    $('#dvStatement').fadeOut();
}

var flagdeci = true;
function CheckDecimal(inputtxt) {
    var decimal = /^\d{1,6}(\.\d{1,2})?$/;
    var valueoftxt = $('#txtRate').val();
    if (valueoftxt.match(decimal)) {
        flagdeci = true;
        return true;
    }
    else {
        alert('Decimal allowed upto two decimal place');
        flagdeci = false;
        return false;
    }
}
function SaveLoanOffer(amt, rate, password) {
    if (flagdeci == true) {
        disablePopupLoan('SubmitconfirmationDiv', 'molahcoreperipopup');
        var chkMultipleofThousand = parseInt(amt) % 1000;

        if (chkMultipleofThousand == 0) {
            var tempamt = parseInt(amt);
            var temprate = parseFloat(rate);

            var minamt = parseInt($('#hdnMinimumOffer').val());

            var minrate = parseFloat($('#hdnMinimumRate').val());

            if (tempamt >= minamt && temprate >= minrate) {
                $('#mask').showLoading();
                $.ajax({
                    url: "/Loan/SaveLoanOffer",
                    type: "POST",
                    dataType: "json",
                    contentType: 'application/json',
                    data: "{Amount:'" + amt + "', Rate:'" + rate + "', Password:'" + password + "',LoanRequest:'" + $('#hdnLoanRequest').val() + "',Term:'" + $('#hdnTerm').val() + "'}",
                    success: function (msg) {

                        $('#dvAggrement').hide();
                        $('#dvHideSubmitAandClear').show();
                        if (msg == "Success") {
                            $('#mask').hideLoading();
                            bootbox.alert('The offer is successful.', function () {
                                location.reload();
                            });
                        }
                        else if (msg == "Locked") {
                            $('#mask').hideLoading();
                            bootbox.alert("Your account has been locked. Please contact Admin.", function () {
                                window.location.href = "/User/Index";
                            })
                        }
                        else {
                            $('#mask').hideLoading();
                            bootbox.alert(msg);
                        }

                    },
                    failure: function (msg) {
                        bootbox.alert('Error! Please try later.');
                    }
                });
            }
            else {
                //loadPopup('dvMsg', '');

                //$('#dvMsgTxt').html('Minimum offer Amount should be "' + minamt + '" and minimum offer rate should be "' + minrate + '"');
                //$('#dvAggrement').hide();
                //$('#dvHideSubmitAandClear').show();
                bootbox.alert('Minimum offer Amount should be "' + minamt + '" and minimum offer rate should be "' + minrate + '"');
            }
        }
        else {
            bootbox.alert('Your offer amount should be multiple of thousands');
            //loadPopup('dvMsg', '');

            //$('#dvMsgTxt').html('Your offer amount should be multiple of hundereds');
            //$('#dvAggrement').hide();
            //$('#dvHideSubmitAandClear').show();
        }
    }
    else
        bootbox.alert('Decimal allowed up to two decimal places');
}





//////////////////////////////////Generate Statememetn for investor while offering loan/////////////////
function GenerateStatement() {

    if ($('#txtAmount').val() != '' && $('#txtRate').val() != '' && $('#txtPassword').val() != '') {

        $('#spBorrowerName').html($('#txtAmount').val() + ' in  ' + $('#hdnBussinessName').val() + ' ');
        $('#spTerm').html(+$('#hdnTerm').val() + ' ');
        $('#spRate').html($('#txtRate').val() + ' % ');

        // loadPopup('SubmitconfirmationDiv', 'molahcoreperipopup');
        $('#dvAggrement').show();
        $('#dvHideSubmitAandClear').hide();

    }
    else {
        alert('Please enter amount, rate and your password');

    }


}




////////////////hide Aggreement  and Display Clear and Submit buton///////////
function HideAgrrementdiv() {
    $('#dvAggrement').hide();
    $('#dvHideSubmitAandClear').show();


}



/////////////////////////////////////////////Change check boxes status Loan offer borrower dashboar/////////////////////////////////
function ChangeCheckStatus() {
    var status = $('#chkTop').is(":checked");
    if (status == true) {
        $('.chkboxes').attr('checked', 'checked');
    }
    else {
        $('.chkboxes').removeAttr('checked');
    }

}

/////////////////////////////////////////////Approve Loan offer by LoanOfferID///////////////////////////
function AprroveLoanOffer(flag) {
    //////////////////////////////Flage is use to check whether the user has accepted or rejeected the loan offers if flag=1(accpeted) if flag=0(rejected)
    var ConfirmFlag = true;
    if (flag == true) {
        ConfirmFlag = confirm('Are you sure with ACCEPTING selected loan offers(s)? This Action will not be rollbacked. System will choose the best of offers for you.');
    }
    else if (flag == false) {
        ConfirmFlag = confirm('Are you sure with WITHDRAWING selected loan offers(s)? This Action will not be rollbacked');
    }

    if (ConfirmFlag == true) {

        var offerid = '';
        $("#tblOfferes tr td input:checkbox").each(function () {
            if ($(this).prop('checked')) {
                var OfferID = $(this).attr('data-val')
                {
                    if (OfferID != 'undefined' || OfferID != '') {
                        offerid += OfferID + ',';
                        $('#' + OfferID).fadeOut('normal');
                    }
                }
            }
        });

        if (offerid != '') {
            $.ajax({
                url: "/Borrower/AcceptorRejectLoanOffer",
                type: "POST",
                dataType: "json",
                contentType: 'application/json',
                data: "{OfferIds:'" + offerid + "', Flag:'" + flag + "'}",
                success: function (msg) {

                    if (msg == "Success") {
                        alert('Request submitted');
                    }
                    else {
                        alert('Please try latter');
                    }

                }
            });
        }
        else {
            alert('Please select loan offers');
        }



    }
}

//////////////////////////////////////////hide popup and clear boxes for loan summarry after disagrreing the terms on investor loan summary////////////////////////////
function disablePopupLoan(divid, divtohopecity) {
    $('.graph_input_small').val('');
    if (popupStatus == 1) { // if value is 1, close popup
        $("#" + divid).fadeOut("normal");
        if (divtohopecity == "") {
            $("#backgroundPopup").fadeOut("normal");
        }
        else {
            $("." + divtohopecity).fadeOut("normal");

        }
        popupStatus = 0;  // and set value to 0
    }
    return false;
}

////////////////////////////////Show hide investor listing////////////////////////////////////
function ShowHideInvestorListing(val) {
    if (val == 0) {
        $('#live').show();
        $('#funded').show();
        $('#Matured').show();

    }
    else if (val == 1) {
        $('#funded').hide();
        $('#Matured').hide();
        $('#live').show();
    }
    else if (val == 2) {
        $('#funded').show();
        $('#Matured').hide();
        $('#live').hide();
    }
    else {
        $('#funded').hide();
        $('#Matured').show();
        $('#live').hide();

    }

}

///////////////////////////////////////////Show Loan Accept Popup on borrower dashboard/////////
var DashLoanRequestID = '';
function ShowLoanAcceptPopup(Loanrequestid) {
    loadPopup("SubmitconfirmationDiv", "molahcoreperipopup");
    DashLoanRequestID = Loanrequestid;

}
//////////////////////////// On accepting the loan update the loan status to approved//////////////
function UpdateLoanOfferStatus() {
    if (DashLoanRequestID != '') {
        $.ajax({
            url: "/Borrower/ChangeLoanStatustoApproved",
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            data: "{RequestID:'" + parseInt(DashLoanRequestID) + "'}",
            success: function (msg) {
                if (msg == true) {
                    disablePopup('SubmitconfirmationDiv', 'molahcoreperipopup', '');
                    $('#dvTxt').html('Loan Accepted Successfully');
                    loadPopup('ConfirmationPopup', 'molahcoreperipopup');
                    $('#Funded_' + DashLoanRequestID).fadeIn();

                }
                else {
                    disablePopup('SubmitconfirmationDiv', 'molahcoreperipopup', '');
                    $('#dvTxt').html('Minimum number of unique investor should be 5.');
                    loadPopup('ConfirmationPopup', 'molahcoreperipopup');
                }

            }
        });
        DashLoanRequestID = '';
    }
}



/////////////////////////////////////Get Cookie Value by Cookie Name///////////////////////////

function getCookie(c_name) {
    var c_value = document.cookie;
    var c_start = c_value.indexOf(" " + c_name + "=");
    if (c_start == -1) {
        c_start = c_value.indexOf(c_name + "=");
    }
    if (c_start == -1) {
        c_value = null;
    }
    else {
        c_start = c_value.indexOf("=", c_start) + 1;
        var c_end = c_value.indexOf(";", c_start);
        if (c_end == -1) {
            c_end = c_value.length;
        }
        c_value = unescape(c_value.substring(c_start, c_end));
    }
    return c_value;
}

/////////////////////////////////////////end///////////////////////////////////////////////////

//$(document).ready(function () {
//    $(".formatCurrency").formatCurrency();
//});
var chngRateRequestID = '';
function ShowRateChangePopup(RequestId, rate) {

    $('#oldRate').val(rate);
    $('#divEditRateError').empty();
    $('#divEditRateError').hide();
    $('#txtRates').val('');
    $('#txtPasswords').val('');

    $('#modelRateChange').modal();

    chngRateRequestID = RequestId;

}


//for geting message history by messageID for investor
var MgsReplyRequestID = ''
var MessageTo = '';
var ChildOf = '';
var LoanRequestID = '';
function ShowMgsReplyPopup(MessageID, MgsTo, ChildID, LoanRequestid, imageid) {
    $("#" + imageid).attr("src", "");
    $("#" + imageid).attr("src", "/Content/images/MgsOpen.png");
    loadPopup('dvEditRate', '');
    MgsReplyRequestID = MessageID;
    MessageTo = MgsTo;
    ChildOf = ChildID;
    if (LoanRequestid != 0)
        LoanRequestID = LoanRequestid;
    else
        LoanRequestID = null;


    $.ajax({
        url: "/Investor/GetMessageHistoryByMessageID",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{MessageID:'" + parseInt(MessageID) + "'}",
        success: function (msg) {

            $('#txtMessage').html(msg.html);

        }
    });

}
//Message Replied By investor
var chilofid = '';
function MgsRepliedByInvestor(Message) {

    disablePopupLoan('dvEditRate', 'molahcoreperipopup');
    $.ajax({
        url: "/Investor/MgsRepliedByInvestor",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{MessageTo:'" + parseInt(MessageTo) + "',ChildOf:'" + parseInt(ChildOf) + "',Message:'" + Message + "',LoanRequestID:'" + parseInt(LoanRequestID) + "'}",
        success: function (msg) {

            alert(msg);

        }
    });
}
//for geting message history by messageID for Borrower
function ShowMgsReplyPopupForBorrower(MessageID, MgsTo, ChildID, LoanRequestid, imageid) {
    $("#" + imageid).attr("src", "");
    $("#" + imageid).attr("src", "/Content/images/MgsOpen.png");
    loadPopup('dvEditRate', '');
    MgsReplyRequestID = MessageID;
    MessageTo = MgsTo;
    ChildOf = ChildID;
    if (LoanRequestid != 0)
        LoanRequestID = LoanRequestid;
    else
        LoanRequestID = null;


    $.ajax({
        url: "/Borrower/GetMessageHistoryByMessageID",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{MessageID:'" + parseInt(MessageID) + "'}",
        success: function (msg) {

            $('#txtMessage').html(msg.html);

        }
    });

}
//Message Replied By Borrower
function MgsRepliedByBorrower(Message) {
    disablePopupLoan('dvEditRate', 'molahcoreperipopup');
    $.ajax({
        url: "/Borrower/MgsRepliedByInvestor",
        type: "POST",
        dataType: "json",
        contentType: 'application/json',
        data: "{MessageTo:'" + parseInt(MessageTo) + "',ChildOf:'" + parseInt(ChildOf) + "',Message:'" + Message + "',LoanRequestID:'" + parseInt(LoanRequestID) + "'}",
        success: function (msg) {

            alert(msg);

        }
    });
}

/////////////////////this function is use to update the rate of loan request by borrower, from borrower dashboard
function EditLoanRate(rate, password, oldRate) {

    if (rate != '' && password != '') {
        $('#mask').showLoading();
        $.ajax({
            async: false,
            url: "/Borrower/EditLoanRateByRequestID",
            type: "POST",
            dataType: "json",
            contentType: 'application/json',
            data: "{RequestID:'" + parseInt(chngRateRequestID) + "',Password:'" + password + "',NewRate:'" + parseFloat(rate) + "'}",
            success: function (msg) {
                if (msg == "Success") {
                    RefreshCurrentRequestsTable();
                    $('#divEditRateError').empty();
                    $('#divEditRateError').hide();
                    $('#mask').hideLoading();
                    $('#modelRateChange').modal('hide');
                    bootbox.alert("The rate is successfully changed.",
                        EditRateCallBack(chngRateRequestID,oldRate,rate));
                }
                else if (msg == "Failure") {
                    $('#divEditRateError').html('Wrong password');
                    $('#divEditRateError').show();
                    $('#mask').hideLoading();
                    return false;
                }
                else {
                    $('#divEditRateError').html(msg);
                    $('#divEditRateError').show();
                    $('#mask').hideLoading();
                    return false;
                }

            }
        });
    }
    else {

        $('#divEditRateError').html('Please enter rate and password');
        $('#divEditRateError').show();
        return false;
    }
}

function EditRateCallBack(requestId, oldRate, newRate) {
    $.getJSON("/Shared/SendEmailsToBorrowerAndApprovedInvestorsOnRateChange",
                       { requestID: requestId, oldRate: oldRate, newRate: newRate },
                   function (data) { });
    //window.location.href = '/Issuer/index';
}

//////////////check only one checkbox at borrower dashboar for accepting and withdrawing a loan
function CheckOnlycurrentbox(chkboxid) {
    $("#tbAllocateLoan tr td input:checkbox").each(function () {
        var chkFlag = $(this).attr('checked');
        if ($(this).attr('data-val') == chkboxid && chkFlag == "checked") {
            $(this).attr('checked', 'checked');
        }
        else {
            $(this).removeAttr('checked');
        }

    });
    ///for geting message history

}

//////////////Process registration submit request
function ClickSubmit(controller, tableNames) {

    var cbControls = $('input.borRules');

    for (var i = 0; i < cbControls.length; i++) {
        if ($('#' + cbControls[i].id).is(':checked') == false) {
            bootbox.alert("Please read and understand the required documents and tick all checkboxes before submitting.");
            return false;
        }
    }

    bootbox.confirm("Please ensure that you have provided all the necessary information.", function (result) {
        if (result == true) {
            $('#mask').showLoading();
            $.ajax({
                url: "/" + controller + "/SubmitUser",
                type: "POST",
                dataType: "json",
                contentType: 'application/json',
                data: '{\'comments\':' + JSON.stringify(GetFileComments(tableNames)) + '}',
                success: function (msg) {
                    if (msg == "Success") {
                        window.location = 'ConfirmRegistration';
                    } else {
                        $('#mask').hideLoading();
                        if (msg != null) {
                            bootbox.alert(msg);
                        }
                        else {
                            bootbox.alert("Some error occured while saving details.");
                        }
                    }

                }
            });
        }
    });
}

function GetFileComments(tableName) {
    var inputControls = [];
    var arrTableNames = tableName.split(',');

    for (var i = 0; i < arrTableNames.length; i++) {
        $.merge(inputControls, $('#fileList' + arrTableNames[i] + ' input:text'));
    }

    var comments = [];
    for (var i = 0; i < inputControls.length; i++) {
        comments[i] = inputControls[i].name + ',' + inputControls[i].value;
    }

    return comments;
}

function GetFileCommentsAsString(tableName) {

    var inputControls = [];
    var arrTableNames = tableName.split(',');

    for (var i = 0; i < arrTableNames.length; i++) {
        $.merge(inputControls, $('#fileList' + arrTableNames[i] + ' input:text'));
    }

    var comments = [];
    for (var i = 0; i < inputControls.length; i++) {
        comments[i] = inputControls[i].name + ':' + inputControls[i].value;
    }

    return comments;
}


function RedirectToLogin() {
    window.location.href = "/User/Login";
}

//Add string list

function LoadList(contextPath) {
    var context = "#" + contextPath + "Div .";
    var fieldName = "#" + contextPath;
    var exvalue = $(fieldName).val();
    if (exvalue != '') {
        var list = exvalue.split(',');
        for (var i = 0; i < list.length; i++) {
            var val = list[i];
            var txtVal = replaceAll(" ", "&nbsp;", val);
            $(context + "control-list ul").append('<li id="' + val + '">' +
                "<div class=\"dyc-div\"><input class=\"dyc-textbox\" type=\"text\" value=" + txtVal + " />&nbsp;" +
                '<img class=\"dyc-img\" src="/Content/images/close_btn.png" onclick="RemoveAchievement(\'' +
                contextPath + '\',\'' + val + '\')"\></div></li>');
            $(".dyc-textbox").attr('disabled', 'disabled');
        }
    }
}

function AddAchievement(contextPath) {
    var context = "#" + contextPath + "Div .";
    var val = $(context + "des_in_left").val();
    if (val != '') {
        var exp = /[+,]/;
        if (val.match(exp)) {
            bootbox.alert("Your input has some invalied charactors, Please check.");
        } else {
            var fieldName = "#" + contextPath;
            var exvalue = $(fieldName).val();
            var list = exvalue.split(',');
            var hasVal = false;
            for (var i = 0; i < list.length; i++) {
                if (list[i] == val) {
                    hasVal = true;
                }
            }
            if (!hasVal) {
                var txtVal = replaceAll(" ", "&nbsp;", val);
                $(context + "control-list ul").append('<li id="' + val + '">' +
                    "<div class=\"dyc-div\"><input class=\"dyc-textbox\" type=\"text\" value=" + txtVal + " />&nbsp;" +
                    '<img class=\"dyc-img\" src="/Content/images/close_btn.png" onclick="RemoveAchievement(\'' +
                    contextPath + '\',\'' + val + '\')"\></div></li>');

                $(".dyc-textbox").attr('disabled', 'disabled');

                $(context + "des_in_left").val('');
                if (exvalue != '') {
                    var newValue = exvalue + ',' + val;
                    $(fieldName).val(newValue);
                } else {
                    $(fieldName).val(val);
                }
            }
        }
    }
}

function replaceAll(find, replace, str) {
    while (str.indexOf(find) > -1) {
        str = str.replace(find, replace);
    }
    return str;
}

function RemoveAchievement(context, name) {
    bootbox.confirm("Are you sure you want to remove this item?",
        function (result) {
            if (result) {
                var fieldName = "#" + context;
                var exvalue = $(fieldName).val();
                var list = exvalue.split(',');
                var newList = "";
                for (var i = 0; i < list.length; i++) {
                    if (list[i] != name) {
                        if (newList != "") {
                            newList = newList + ",";
                        }
                        newList = newList + list[i];
                    }
                }
                $(fieldName).val(newList);
                $("li[id='" + name + "']").remove();
            }
        });
}

//

function ToQueryStringValue(val) {
    return val.replace().replace('+', '%2B');
}

function CheckAllOptions(cls, val) {
    $('input.' + cls).each(function () {
        $(this).prop('checked', val);
    });
}