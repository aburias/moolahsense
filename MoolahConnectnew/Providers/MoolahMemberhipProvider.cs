﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;

namespace MoolahConnectnew.Providers
{
    public static class MoolahMemberhipProvider
    {
        public static string GetPassword(string username)
        {
            try
            {
                return Membership.GetUser(username).GetPassword(GlobalConstansValues.SEC_QUES_ANSWER);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return string.Empty;
            }
        }

        public static void MakeLogoff()
        {
            string username = HttpContext.Current.User.Identity.Name;

            //WebSecurity.Logout();
            FormsAuthentication.SignOut();
            HttpContext.Current.Session.Abandon();
            // clear authentication cookie
            HttpCookie cookie1 = new HttpCookie(FormsAuthentication.FormsCookieName, "");
            cookie1.Expires = DateTime.UtcNow.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie1);

            // clear session cookie (not necessary for your current problem but i would recommend you do it anyway)
            HttpCookie cookie2 = new HttpCookie("ASP.NET_SessionId", "");
            cookie2.Expires = DateTime.UtcNow.AddYears(-1);
            HttpContext.Current.Response.Cookies.Add(cookie2);

            // Invalidate the Cache on the Client Side
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();

            HttpCookie cookie;

            cookie = HttpContext.Current.Request.Cookies.Get("_UsersRole_");
            if (cookie != null)
            {
                cookie = new HttpCookie("_UsersRole_");
                cookie.Expires = DateTime.UtcNow.AddDays(-1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }

            MoolahLogManager.Log(StatusMessages.SUCCESSFULL_LOGOUT, LogType.Warn, username);
        }
    }
}