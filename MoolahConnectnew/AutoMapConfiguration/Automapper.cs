﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting;
using System.Web;
using System.Web.Http.Routing.Constraints;
using AutoMapper;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Util;
using MoolahConnectClassLibrary.Models.ViewModels;
using LoanRequest = MoolahConnect.Models.BusinessModels.LoanRequest;
using  AccountDetail = MoolahConnect.Models.BusinessModels.AccountDetail;
using LoanRequestVM = MoolahConnectClassLibrary.Models.ViewModels.LoanRequest;
using OutstandingLitigationVM = MoolahConnectClassLibrary.Models.ViewModels.OutstandingLitigation;
using MoolahCore = MoolahConnect.Models.BusinessModels.MoolahCore;
using MoolahPeri = MoolahConnect.Models.BusinessModels.MoolahPeri;
using OutStandingLitigation = MoolahConnect.Models.BusinessModels.OutStandingLitigation;
namespace MoolahConnectnew.AutoMapConfiguration
{
    public class Automapper
    {
        
        public static TermsheetViewModel MapTermsheetViewModel(LoanRequest request)
        {
            return Mapper.Map<LoanRequest, TermsheetViewModel>(request);
        }
        public static BorrowerDashboard MapBorrowerDashboard(AccountDetail accountDetail)
        {
            //return Mapper.CreateMap<AccountDetail, BorrowerDashboard>()
            //    .ForMember(a => a.CommonOperations, m => m.MapFrom(b => new Common()
            //    {
            //        AccountName = b.AccountName,
            //    }));
          
            BorrowerDashboard dashboard = new BorrowerDashboard();
            return null;
        }
        public static CompanyProfile MapCompanyProfile(AccountDetail accountDetail)
        {
            Mapper.CreateMap<AccountDetail, CompanyProfile>()
                .ForMember(a => a.AccountName, c => c.MapFrom(opt => opt.AccountName))
                .ForMember(a => a.CompanyName, c => c.MapFrom(opt => opt.BusinessName))
                .ForMember(a => a.CompanyRegNumber, c => c.MapFrom(opt => opt.IC_CompanyRegistrationNumber))
                .ForMember(a => a.TypeofCompany, c => c.MapFrom(opt => opt.BusinessOrganisation))
                .ForMember(a => a.RegisteredAddress1,
                    c => c.MapFrom(opt => CommonMethods.LoadAddressField(opt.RegisteredAddress)[0]))
                .ForMember(a => a.RegisteredAddress2,
                    c => c.MapFrom(opt => CommonMethods.LoadAddressField(opt.RegisteredAddress)[1]))
                .ForMember(a => a.RegisteredAddressPostalCode,
                    c => c.MapFrom(opt => CommonMethods.LoadAddressField(opt.RegisteredAddress)[2]))
                .ForMember(a => a.BusinessMailingAddress1,
                    c => c.MapFrom(opt => CommonMethods.LoadAddressField(opt.BusinessMailingAddress)[0]))
                .ForMember(a => a.BusinessMailingAddress2,
                    c => c.MapFrom(opt => CommonMethods.LoadAddressField(opt.BusinessMailingAddress)[1]))
                .ForMember(a => a.BusinessMailingPostalCode,
                    c => c.MapFrom(opt => CommonMethods.LoadAddressField(opt.BusinessMailingAddress)[2]))
                .ForMember(a => a.TelephoneOutOfficeHours, c => c.MapFrom(opt => opt.Main_OfficeContactnumber))
                .ForMember(a => a.MobileOutofOfficeHours, c => c.MapFrom(opt => opt.Mobilenumber))
                .ForMember(a => a.MobileOutofOfficeHours, c => c.MapFrom(opt => opt.User.UserName))
                .ForMember(a => a.BankAccount, c => c.MapFrom(opt => opt.AccountNumber))
                .ForMember(a => a.NricNumber, c => c.MapFrom(opt => opt.NRIC_Number))
                .ForMember(a => a.PassportNumber, c => c.MapFrom(opt => opt.PassportNumber))
                .ForMember(a => a.Role, c => c.MapFrom(opt => opt.RoleJobTitle));
                

            return Mapper.Map<AccountDetail, CompanyProfile>(accountDetail);

        }
        public static BorrowerAccount MapBorrowerAccount(AccountDetail accountDetail)
        {
            return Mapper.Map<AccountDetail, BorrowerAccount>(accountDetail);
        }
        public static AccountDetail MapAccountDetail(BorrowerAccountDetailsModel accountDetail)
        {
            return Mapper.Map<BorrowerAccountDetailsModel,AccountDetail>(accountDetail);
        }

        public static LoanRequestVM MapLoanRequest(LoanRequest loanRequest)
        {
            return Mapper.Map<LoanRequest, LoanRequestVM>(loanRequest);
        }
        public static LoanRequest MapLoanRequest(LoanRequestDescription loanRequestDescription)
        {
            return Mapper.Map<LoanRequestDescription, LoanRequest>(loanRequestDescription);
        }
        public static LoanRequest MapLoanRequest(LoanRequestMoolahCore loanRequestMoolahCore)
        {
            return Mapper.Map<LoanRequestMoolahCore, LoanRequest>(loanRequestMoolahCore);
        }
        public static LoanRequest MapLoanRequest(LoanRequestMoolahPeri loanRequestMoolahPeri)
        {
            return Mapper.Map<LoanRequestMoolahPeri, LoanRequest>(loanRequestMoolahPeri);
        }
        public static LoanRequestMoolahCore MapLoanRequestMoolahCore(MoolahCore moolahCore)
        {
            return Mapper.Map<MoolahCore, LoanRequestMoolahCore>(moolahCore);
        }
        public static MoolahCore MapLoanRequestMoolahCore(LoanRequestMoolahCore moolahCore)
        {
            return Mapper.Map<LoanRequestMoolahCore, MoolahCore>(moolahCore);
        }
        
        public static LoanRequestMoolahPeri MapLoanRequestMoolahPeri(MoolahPeri moolahPeri)
        {
            return Mapper.Map<MoolahPeri, LoanRequestMoolahPeri>(moolahPeri);
        }
        public static MoolahPeri MapLoanRequestMoolahPeri(LoanRequestMoolahPeri loanRequestMoolahPeri)
        {
            return Mapper.Map<LoanRequestMoolahPeri, MoolahPeri>(loanRequestMoolahPeri);
        }
        public static OutStandingLitigation MapOutstandingLitigation(OutstandingLitigationVM outstandingLitigation)
        {
            return Mapper.Map<OutstandingLitigationVM, OutStandingLitigation>(outstandingLitigation);
        }

        public static IList<LoanRequestDashboard> MapLoanRequestDashboardList(IList<LoanRequest> list)
        {
            return Mapper.Map<IList<LoanRequest>, IList<LoanRequestDashboard>>(list);
        }

        public static PersonalGuaranteeInfo MaPPersonalGuaranteeInfo(PersonGuarantee personGuarantee )
        {
            return Mapper.Map<PersonGuarantee,PersonalGuaranteeInfo>(personGuarantee);
        }
        public static IList<PersonGuarantee> MaPPersonalGuaranteeInfo(IList<PersonalGuaranteeInfo> personalGuaranteeInfos)
        {
            return Mapper.Map<IList<PersonalGuaranteeInfo>, IList<PersonGuarantee>>(personalGuaranteeInfos);
        }
        
    }
}