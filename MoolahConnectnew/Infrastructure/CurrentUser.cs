﻿using System.Web;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnectnew.Infrastructure
{
    public class CurrentUser : ICurrentUser
    {
        private readonly IUserService _iuserService;

        private User _user;

        public CurrentUser(IUserService iuserService)
        {
            _iuserService = iuserService;
        }

        public User ApplicationUser
        {
            get { return _user ?? (_user = _iuserService.GetUser(HttpContext.Current.User.Identity.Name.Trim())); }
        }
      
    }
}