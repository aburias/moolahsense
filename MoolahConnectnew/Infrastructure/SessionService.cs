﻿using System.Web;
using System.Web.SessionState;
using MoolahConnect.Services.Interfaces1;

namespace MoolahConnectnew.Infrastructure
{
    public class SessionService : ISessionService
    {
        private readonly HttpSessionState httpSession;

        public SessionService()
        {
            httpSession = HttpContext.Current.Session;
        }

        public T Get<T>(string key)
        {
            return (T) (httpSession[key] ?? default(T));
        }

        public void Set<T>(string key, T t)
        {
            httpSession[key] = t;
        }

        public void Remove(string key)
        {
            httpSession.Remove(key);
        }
    }
}