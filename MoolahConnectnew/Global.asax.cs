﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;
using MoolahConnect.Data.Migrations;
using MoolahConnect.Models.BusinessModels;
using MoolahConnectClassLibrary.Models;
using NLog.Config;
using NLog.Common;
using MoolahConnectnew.App_Start;
using System.Data.Entity;
using MoolahConnect.Services.Entities;
using MoolahConnect.Data.Databases;
 




namespace MoolahConnectnew
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    public class MvcApplication : System.Web.HttpApplication
    {
        
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas(); 

            WebApiConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            AutoMapperConfig.Configure();
            Bootstrapper.Run();

            //Database.SetInitializer(new MigrateDatabaseToLatestVersion<MoolahConnectDbContext, MoolahConnectMigrationConfig>());
        }

    }
}