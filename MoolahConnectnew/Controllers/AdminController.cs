﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using MoolahConnect.Services;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Excel;
using MoolahConnectnew.Filters;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectClassLibrary.DAL;
using MoolahConnectClassLibrary.Models.ViewModels.Admin;
using System.IO;
using MoolahConnect.Services.Entities;
using MoolahConnectnew.ViewModels;
using MoolahConnectnew.ViewModels.Investor;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Tasks.Interfaces;
using ExcelLibrary.SpreadSheet;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util;
using MoolahConnect.Util.Logging;
using MoolahConnectnew.ViewModels.Admin;
using MoolahConnectnew.ViewModels.Borrower;
using MoolahConnectnew.Controllers.Helpers;
using System.Globalization;
using MoolahConnect.Util.SealedClasses;
using Newtonsoft.Json;
using MoolahConnect.Util.Email;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Models.CustomModels;
using MoolahConnectnew.ViewModels.Loan;
using MoolahConnect.Models.BusinessModels;

namespace MoolahConnectnew.Controllers
{
    [authorize.AuthorizeRole(Roles = "Admin")]
    public class AdminController : Controller
    {
        AdminOperations objAdminOperation;
        private readonly ILoanAllocationDA _laDa;
        private readonly IGlobalVariableDA _gvDa;
        InvestorOperations objInvestorOperations;
        ILoanDetailsDA _ILoanDetailsDA;
        ILoanDetailsTask _ILoanDetailsTask;
        ILoanAllocationTask _ILoanAllocationTask;
        IFileUploadDA _IFIleUploadDA;
        IAuditLogDA _IAuditLogDA;
        ITransactionsDA _ITransactionsDA;
        IUser userOpr;
        ILoanPaymentsTask _ILoanPaymentTask;
        IEmailSender _IEmailSender;
        IUser _IUser;
        const int PageSize = 15;


        private IUserService _IUserService;
        private ILoanRequestService _ILoanRequestService;
        private IRepaymentMessageService _IRepaymentMessageService;

        public AdminController(IUserService iUserService,ILoanRequestService iLoanRequestService,
            IRepaymentMessageService iRepaymentMessageService)
        {
            objAdminOperation = new AdminOperations();
            _laDa = new LoanAllocationDA();
            _gvDa = new GlobalVariableDA();
            objInvestorOperations = new InvestorOperations();
            _ILoanDetailsDA = new LoanDetailsDA();
            _ILoanAllocationTask = new LoanAllocationTask();
            _IFIleUploadDA = new FileUploadDA();
            _IAuditLogDA = new AuditLogDA();
            _ITransactionsDA = new TransactionsDA();
            _ILoanDetailsTask = new LoanDetailsTask();
            userOpr = new MoolahConnect.Services.Implementation.User();
            _ILoanPaymentTask = new LoanPaymentsTask();
            _IEmailSender = new EmailSender();
            _IUser = new MoolahConnect.Services.Implementation.User();

            _IUserService = iUserService;
            _ILoanRequestService = iLoanRequestService;
            _IRepaymentMessageService = iRepaymentMessageService;
        }

        public ActionResult Index()
        {
            var model = Mapper.Map<UsersSummaryDTO,UsersSummaryViewModel>(_IUserService.GetUsersSummary());
            return View(model);
        }

        /// <summary>
        /// for fetching user detail by userID
        /// </summary>
        /// <param name="UserID"></param>
        /// <returns></returns>
        public ActionResult UserVerification(string UserID)
        {
            var objborrower = objAdminOperation.GetAllUserDetailsByUserId(UserID);
            if (objAdminOperation == null)
            {
                return View();
            }
            else
            {
                return View(objborrower);
            }
        }

        /// <summary>
        /// save user's final verification status and  all verification by admin
        /// </summary>
        /// <param name="obj"></param>
        [HttpPost]
        public ActionResult SaveUserVerificationDetailbyUserID(AdminAccountDetail obj)
        {
            if (!string.IsNullOrEmpty(obj.comments))
            {
                var idComments = new List<string>();

                var items = obj.comments.Split(',');

                foreach (string item in items)
                {
                    idComments.Add(item.Replace(":", ","));
                }

                _IFIleUploadDA.UpdateUploadedFileComments(idComments);
            }

            objAdminOperation.SaveUserVerificationDetailbyUserID(obj);
            if (obj.InvestorType == @"Individual")
            {
                System.Web.HttpContext.Current.Session["step"] = 2;
                return RedirectToAction("UserVerification", "Admin", new { UserID = obj.UseriD });
            }
            System.Web.HttpContext.Current.Session["step"] = 1;
            return RedirectToAction("UserList", "Admin", new { role = "All" });
        }

        /// <summary>
        /// Method for saving knowledge assessment details for investor using XML
        /// </summary>       
        /// <returns></returns>
        public JsonResult SaveKnowledgeassessmentForInvestor(string xmldocs, string userId)
        {
            if (ModelState.IsValid)
            {
                var result = objInvestorOperations.Saveknowledgeassesmentotions(xmldocs, long.Parse(userId));
                if (result == "Success")
                {
                    System.Web.HttpContext.Current.Session["step"] = 1;
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else if (result == "Failure")
                {
                    return Json("Some error occured during saving Knowledgeassessmentoptions", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("Please fill data properly", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                return Json("Please fill data properly", JsonRequestBehavior.AllowGet);
            }
        }

        /// <summary>
        /// Method for getting knoweldge assessment options for specific question for knowledge assessment partial view
        /// </summary>        
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult GetKnowledgeassessmentoptions(int parentid, long userid)
        {
            String result = objInvestorOperations.GetKnowledgeassessmentoptions(parentid, userid);

            if (result != "Failure")
            {
                return Content(result, MediaTypeNames.Text.Html);
            }
            else
            {
                return RedirectToAction("UserVerification", "Admin");
                //.WithFlash(new { notice = "Some error occured during getting Knowledgeassessmentoptions" });
            }
        }

        public ActionResult WithdrawInformation()
        {
            var obj = objAdminOperation.GetWithdrawInformation();
            return View(obj);

        }
        /// <summary>
        /// Get all useres List by user role
        /// </summary>
        /// <returns></returns>
        public ActionResult UserList(string role)
        {
            if (role != "")
            {
                System.Web.HttpContext.Current.Session["step"] = 1;
                var obj = objAdminOperation.GetUserListbyUserRole(role, 0, PageSize,new UserSearchCreteria());
                ViewBag.PageSize = PageSize;
                ViewBag.TotalPages = obj.Item2;
                return View(obj.Item1);
            }
            else
                return View("Index");
        }
        public JsonResult GetUserList(string role, int currentPage,string userName = "",string fullName = "", string displayName="",string bussinessName="",string nric="")
        {
            if (role == "") return Json(new AdminUsersList(), JsonRequestBehavior.AllowGet);
            System.Web.HttpContext.Current.Session["step"] = 1;
            var results = objAdminOperation.GetUserListbyUserRole(role, currentPage, PageSize,new UserSearchCreteria
            {
                DisplayName = displayName,
                UserName =  userName,
                BusinessName =  bussinessName,
                NRIC = nric,
                FullName = fullName
            });
            var userResult = results.Item1.UserListAdmin
                .Select(a => new
                {
                    CreatedDate = a.CreatedDate.ToDateTime(),
                    UserName = a.UserName,
                    FullName = a.FullName,
                    DisplayName = a.DisplayName,
                    NRIC = a.NRIC,
                    BusinessName = a.BusinessName,
                    UserRole = a.UserRole,
                    UserType = a.UserType,
                    AccountNo = a.AccountNo,
                    AdminVerification = a.AdminVerification,
                    IsLocked = a.IsLocked,
                    IsSubmitted = a.IsSubmitted,
                    AvailableBalance = a.AvailableBalance,
                    UserID = a.UserID,
                    DigitalSignature = a.DigitalSignature,
                    RecordCount = results.Item2
                });
            
            return Json(userResult, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Get Loan detail for admin verification by request Id
        /// </summary>
        /// <param name="loanRequestID"></param>
        /// <returns></returns>
        public ActionResult LoanDetail(int loanRequestID)
        {


            Session["LoanrequestId"] = loanRequestID;
            //LoanRequestAdmin loanrequest = objAdminOperation.LoanrequestLoad(loanRequestID);

            var loanRequest = _ILoanRequestService.GetLoanRequestbyRequestID(loanRequestID);

            var model = new LoanDetailsViewModel();

            model.MoolahCore.MoolahCore = Mapper.Map<MoolahConnect.Models.BusinessModels.MoolahCore, MoolahCoreViewModel>(
                loanRequest.MoolahCores.FirstOrDefault() != null ? loanRequest.MoolahCores.FirstOrDefault() : new MoolahConnect.Models.BusinessModels.MoolahCore());
            model.MoolahCore.MoolahCoreVerify = Mapper.Map<MoolahConnect.Models.BusinessModels.MoolahCoreVerification, MoolahCoreVerifyViewModel>(
                loanRequest.MoolahCoreVerifications.FirstOrDefault() != null ? loanRequest.MoolahCoreVerifications.FirstOrDefault() : new MoolahConnect.Models.BusinessModels.MoolahCoreVerification());

            model.MoolahPeri.MoolahPeri = Mapper.Map<MoolahConnect.Models.BusinessModels.MoolahPeri, MoolahPeriViewModel>(
                loanRequest.MoolahPeris.FirstOrDefault() != null ? loanRequest.MoolahPeris.FirstOrDefault() : new MoolahConnect.Models.BusinessModels.MoolahPeri());
            model.MoolahPeri.MoolahPeriVerify = Mapper.Map<MoolahConnect.Models.BusinessModels.MoolahPeriVerification, MoolahPeriVerifyViewModel>(
                loanRequest.MoolahPeriVerifications.FirstOrDefault() != null ? loanRequest.MoolahPeriVerifications.FirstOrDefault() : new MoolahConnect.Models.BusinessModels.MoolahPeriVerification());

            model.RepaymentMessages = Mapper.Map<IEnumerable<MoolahConnect.Models.BusinessModels.RepaymentMessage>,IEnumerable<RepaymentMessageViewModel>>(
                loanRequest.RepaymentMessages.AsEnumerable());

            return View(model);
            //return View(objAdminOperation.GetLoanDetailbyRequestID(Convert.ToInt64(LoanRequestID));

        }

        public JsonResult SaveRepaymentMessage(RepaymentMessageViewModel repaymentMessage)
        {
            try
            {
                    repaymentMessage.LoanRequestId = long.Parse(Session["LoanrequestId"].ToString());
                    //objAdminOperation.SaveRepaymentMessage(repaymentMessage);

                    switch (repaymentMessage.State)
                    {
                        case EntityState.New:
                            var message = new RepaymentMessage() { 
                                LoanRequestId = repaymentMessage.LoanRequestId,
                                Message = repaymentMessage.Message,
                                CreatedDate = DateTime.Now
                            };
                            _IRepaymentMessageService.Create(message);
                            break;
                        case EntityState.Update:
                            var messageToUpdate = _IRepaymentMessageService.GetByID(repaymentMessage.Id);
                            messageToUpdate.Message = repaymentMessage.Message;
                            _IRepaymentMessageService.Update(messageToUpdate);
                            break;
                        case EntityState.Delete:
                            _IRepaymentMessageService.Delete(repaymentMessage.Id);
                            break;
                        default:
                            break;
                    }

                    return Json(new { Status = "Succeed", RepaymentMessage = repaymentMessage }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {

                return Json(new { Status = "fail" }, JsonRequestBehavior.AllowGet);
               
            }
           
        }

        /// <summary>
        /// Save the Guratee verification and docuemtn verfication done by admin on loan description i.e step 1 of loan creating process
        /// </summary>
        /// <returns></returns>
        public ActionResult SavePersonlaGuranteeVerifcation(LoanRequestDescriptionAdmin obj)
        {
            if (Session["LoanrequestId"] != null)
            {
              obj.PersonGuarantees =  JsonConvert.DeserializeObject<IList<PersonGuarantee>>(this.Request.Form["PersonGuarantees"]);
                if (!string.IsNullOrEmpty(obj.DocumentComments))
                {
                    var idComments = new List<string>();

                    var items = obj.DocumentComments.Split(',');

                    foreach (string item in items)
                    {
                        idComments.Add(item.Replace(":", ","));
                    }

                    _IFIleUploadDA.UpdateUploadedFileComments(idComments);
                }

                obj.RequestId = Convert.ToInt64(Session["LoanrequestId"]);
                objAdminOperation.SavePersonlaGuranteeVerifcation(obj);
            }
            return RedirectToAction("LoanDetail", "Admin", new { LoanRequestID = obj.RequestId });

        }
        /// <summary>
        /// Save MoolahPeri Verification detail and final loan approval by admin and send email to user with admin comments on loan request
        /// </summary>
        /// <param name="obj"></param>
        public ActionResult SaveMoolahPeriVerification(LoanRequestMoolahPeriAdmin obj)
        {
            if (Session["LoanrequestId"] != null)
            {
                obj.Request_ID = Convert.ToInt64(Session["LoanrequestId"]);

                objAdminOperation.SaveMoolahPeriVerification(obj);
            }
            return RedirectToAction("LoanDetail", "Admin", new { LoanRequestID = obj.Request_ID });
        }
        public ActionResult SaveMoolahCoreVerfication(LoanRequestMoolahCoreAdmin obj)
        {
            if (Session["LoanrequestId"] != null)
            {
                obj.RequestId = Convert.ToInt64(Session["LoanrequestId"]);
                objAdminOperation.SaveMoolahCoreVerfication(obj);
            }
            return RedirectToAction("LoanDetail", "Admin", new { LoanRequestID = obj.RequestId });
        }
        /// <summary>
        /// Get All Loan request by borrowers and check if the request is meets all the condition for approval
        /// </summary>
        /// <returns></returns>
        /// 
        public ActionResult GetAllLoanList()
        {
            return View(objAdminOperation.GetAllLoanRequestForAdmin());
        }
        //get approved loan offers by loanRequesID

        public JsonResult AddBalanceByUserID(int userID, decimal? amount, string adminUsername, string adminPassword)
        {
            if (!amount.HasValue)
            {
                return Json(new { Status = "Failed", Error = "Amount can not be empty." }, JsonRequestBehavior.AllowGet);
            }

            if (!userOpr.ValidateAdminUser(adminUsername, adminPassword))
            {
                return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
            }

            if (objAdminOperation.AddBalanceByUserID(userID, amount.Value) != "Success")
            {
                return Json(new { Status = "Failed", Error = "Unexpected Error. Please try again" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Distribute the amount of EmI between investors , by the highest rate offer approved by the loan allocation process
        /// </summary>
        /// <returns></returns>

        public ActionResult EMIAllocation()
        {
            return View();
        }
        /// <summary>
        ///  get EMI by Loan reference for Distributing the amount of EmI between investors
        /// </summary>
        /// <param name="LoanReferenceID"></param>
        /// <returns></returns>
        public JsonResult GetEMIbyLoanReference(string LoanReference)
        {
            return Json(objAdminOperation.GetEMIbyLoanReference(LoanReference), JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Set the emi status to true to show that emi has been paid for a loan
        /// </summary>
        /// <returns></returns>
        public JsonResult SaveRepaymentDetail(string LoanReference, string PayedOn)
        {
            return Json(objAdminOperation.SaveRepaymentDetail(LoanReference, PayedOn), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Update Withdraw Status
        /// </summary>
        /// <param name="WithdrawID"></param>
        /// <returns></returns>
        public JsonResult UpdateWithdrawStatus(long WithdrawID, string Comments, string Status, string adminUsername, string adminPassword)
        {
            if (!userOpr.ValidateAdminUser(adminUsername, adminPassword))
            {
                return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
            }

            if (objAdminOperation.UpdateWithdrawStatus(WithdrawID, Comments, Status, adminUsername) != "Success")
            {
                return Json(new { Status = "Failed", Error = "Unexpected Error. Please try again" }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult MoolahBoard()
        {
            var model = new LoanRequestBoardViewModel();
            var loans = _laDa.GetAllPublishedLoanRequests().ToList();
            var completionDays = _gvDa.Get().FirstOrDefault();
            foreach (var loan in loans)
            {
                if (completionDays != null)
                {
                    var finalDate = Convert.ToDateTime(loan.DateCreated).AddDays(Convert.ToDouble(completionDays.RequestCompletionDays));
                    if (DateTime.UtcNow > finalDate) continue;
                }
                var viewmodel = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(loan);
                var loanCalculations = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(loan.RequestId, loan.Amount);
                viewmodel.Funded = loanCalculations[0];
                viewmodel.AverageRate = Convert.ToDecimal(loan.Rate);
                if (completionDays != null)
                {
                    var timeRemaining = loan.PublishedDate.HasValue
                        ? _ILoanAllocationTask.TimeLetfToExpireLoanRequest(loan.PublishedDate.Value)
                                : new int[0];
                    if (timeRemaining.Count() > 0)
                    {
                        viewmodel.Days = timeRemaining[0];
                        viewmodel.Hours = timeRemaining[1];
                        viewmodel.Minutes = timeRemaining[2];
                    }
                }
                if (viewmodel.Minutes > 0)
                    model.LoanRequestViewModels.Add(viewmodel);
            }

            return View(model);
        }

        public ActionResult LoanSummary(long LoanRequestID)
        {
            var loan = new LoanRequestViewModel();
            var model = new InvestorLoanOffer();
            var account = new AccountViewModel();

            ViewBag.LoanRequestId = LoanRequestID;
            model = objInvestorOperations.GetLoanandBorrowerBriefDetailRequestID(LoanRequestID);

            var _loanDetails = _ILoanDetailsDA.GetLoanRequestFromId(LoanRequestID);
            if (_loanDetails != null)
                loan = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(_loanDetails);

            var _accountDetails = _ILoanDetailsDA.GetAccountDetailsFromUserId(loan.UserId);

            if (_accountDetails != null)
                account = Mapper.Map<tbl_AccountDetails, AccountViewModel>(_accountDetails);

            ViewData["Loan"] = loan;
            ViewData["Account"] = account;


            return View(model);
        }

        public ActionResult AcceptedLoans()
        {
            var history = objAdminOperation.MatchedLoans();
            var loans = new AcceptedLoanWrapper { Loans = history };
            return View(loans);
        }

        public ActionResult AuditLogs()
        {
            var obj = objAdminOperation.GetAllAuditLogs();
            return View(obj);
        }

        [HttpPost]
        public ActionResult AcceptedLoansHistory(int term)
        {
            var history = objAdminOperation.AcceptedLoansHistory(term);
            var loans = new AcceptedLoanWrapper { Loans = history };
            return View(loans);
        }

        public PartialViewResult OutstandingLoans(int timePeriod)
        {
            var model = Mapper.Map<IEnumerable<LoanEntity>, IEnumerable<LoanViewModel>>(
                _ILoanDetailsTask.GetLoans(timePeriod, LoanRequestStatus.FundApproved));

            ViewData["TimePeriod"] = timePeriod;

            return PartialView(model);
        }

        public PartialViewResult OutstandingRepayments(int timePeriod)
        {
            var model = AdvanceMappingHelper
                .MapLateLoanRepayments(_ILoanDetailsTask.GetLoanRepayments(timePeriod));

            ViewData["TimePeriod"] = timePeriod;

            return PartialView(model);
        }

        public PartialViewResult OutstandingRepaymentDetails(long requestId, long repaymentId)
        {
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            ViewBag.RequestId = requestId;
            ViewBag.RepaymentId = repaymentId;
            return PartialView();
        }

        public PartialViewResult _LoanPayment(long requestId, long repaymentId, DateTime repaymentDate, double repaymentAmout,
            double paidAmount, double accrued, double LateFees, double repaymentOutstanding)
        {
            ViewBag.RepaymentAmount = repaymentAmout;
            ViewBag.RepaymentDate = repaymentDate.ToDate();
            ViewBag.PaidAmount = paidAmount;
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            ViewBag.Accrued = accrued;
            ViewBag.LateFees = LateFees;
            ViewBag.RepaymentsOustanding = repaymentOutstanding;

            ViewBag.RepOutstandingAmount = (repaymentAmout - paidAmount);
            var outstandingTemp = (repaymentOutstanding + accrued + LateFees - paidAmount);
            ViewBag.OutstandingAmount = outstandingTemp < -0 ? 0 : outstandingTemp;

            ViewBag.RequestId = requestId;
            ViewBag.RepaymentId = repaymentId;

            var model = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
                _ILoanDetailsDA.GetLoanInvestors(requestId, repaymentId));

            return PartialView(model);
        }

        public JsonResult AddLoanPayment(long requestId, int repaymentId, string date, double repaymentAmount, double? amount,
            string adminUsername, string adminPassword, bool repaymentPaid, bool accruedPaid, bool lateFeesPaid, double? accrued, double? latefees, double? paidAmount)
        {
            try
            {
                if (date == null || !amount.HasValue)
                {
                    return Json(new { Status = "Failed", Error = "Amount or date can not be empty." }, JsonRequestBehavior.AllowGet);
                }

                if (!userOpr.ValidateAdminUser(adminUsername, adminPassword))
                {
                    return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
                }

                DateTime dateToPass = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);


                if (!_ILoanPaymentTask.AddLoanPayment(requestId, repaymentId, amount.Value, dateToPass, repaymentPaid, accruedPaid, lateFeesPaid, accrued, latefees))
                {
                    return Json(new { Status = "Failed", Error = "Unexpected Error. Please try again" }, JsonRequestBehavior.AllowGet);
                }

                var loanDetails = _ILoanDetailsDA.GetLoanRequestFromId(requestId);
                var repayment = _ILoanDetailsDA.GetRepaymentByID(repaymentId);

                _IEmailSender.BorrowerRepaymentStatusChanged(loanDetails.tbl_Users.UserName,
                    repaymentPaid ? RepaymentPayStatus.Paid : RepaymentPayStatus.PartiallyPaid,
                    repayment.EmiDate.HasValue ? repayment.EmiDate.Value : DateTime.MinValue,
                    amount.HasValue ? amount.Value : 0,
                    _ILoanDetailsDA.MonthsTillMaturity(requestId));

                #region Investor Deposits

                var loanInvestorsForRepayment = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
                        _ILoanDetailsDA.GetLoanInvestorsForDepLatePayments(requestId, repaymentAmount));

                if (!repaymentPaid)
                {

                    foreach (LoanInvestorViewModel item in loanInvestorsForRepayment)
                    {
                        _ITransactionsDA.LoanTransactionCredit(item.UserId, (decimal)item.RepaymentAmount, item.RequestId, "Interent", "Accured Interest From Late Monthly Repayment");
                        _IEmailSender.InvestorRepaymentStatusChanged(item.Username,
                            loanDetails.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName,
                            RepaymentPayStatus.PartiallyPaid,
                            repayment.EmiDate.HasValue ? repayment.EmiDate.Value : DateTime.MinValue,
                            item.RepaymentAmount,
                            _ILoanDetailsDA.MonthsTillMaturity(requestId));

                    }

                }
                else
                {
                    var loanInvestors = new List<LoanInvestorViewModel>();
                    

                    if (paidAmount.HasValue && paidAmount != 0)
                    {
                        var loanInvestorsForPaidAmount = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
                        _ILoanDetailsDA.GetLoanInvestorsForDepLatePayments(requestId, paidAmount.Value));

                        foreach (LoanInvestorViewModel investor in loanInvestorsForPaidAmount)
                        {
                            investor.Amount = Math.Round(loanInvestorsForRepayment.SingleOrDefault(x => x.OfferId == investor.OfferId).RepaymentAmount
                                - investor.Amount, 2);

                            loanInvestors.Add(investor);
                        }
                    }
                    else
                    {
                        loanInvestors = loanInvestorsForRepayment.ToList();
                    }

                    foreach (LoanInvestorViewModel item in loanInvestors)
                    {
                        _ITransactionsDA.LoanTransactionCredit(item.UserId, (decimal)item.RepaymentAmount, item.RequestId, "Interent", "Monthly Repayment");
                        _IEmailSender.InvestorRepaymentStatusChanged(item.Username,
                            loanDetails.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName,
                            RepaymentPayStatus.Paid,
                            repayment.EmiDate.HasValue ? repayment.EmiDate.Value : DateTime.MinValue,
                            item.RepaymentAmount,
                            _ILoanDetailsDA.MonthsTillMaturity(requestId));
                    }

                    if (accruedPaid)
                    {
                        var loanInvestorsForAccrued = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
                        _ILoanDetailsDA.GetLoanInvestorsForDepLatePayments(requestId, accrued.Value));


                        foreach (LoanInvestorViewModel item in loanInvestorsForAccrued)
                        {
                            _ITransactionsDA.LoanTransactionCredit(item.UserId, (decimal)item.RepaymentAmount, item.RequestId, "Interent", "Accured Interest From Late Monthly Repayment");
                        }
                    }

                    var investorsPaidSum = Math.Round(loanInvestorsForRepayment.Sum(x => x.RepaymentAmount), 2);

                    if (investorsPaidSum != repaymentAmount)
                    {
                        _ITransactionsDA.AddAccountAdjustment(repaymentId, investorsPaidSum, repaymentAmount - investorsPaidSum);
                    }
                }

                #endregion

                return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return Json(new { Status = "Failed", Error = "Unexpected error. Please try again" }, JsonRequestBehavior.AllowGet);
            }
        }

        public PartialViewResult LateRepaymentsDetails(long requestId, double latePayment)
        {
            var model = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
                           _ILoanDetailsDA.GetLoanInvestorsForDepLatePayments(requestId, latePayment));
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            ViewBag.LatePayment = latePayment;
            ViewBag.RequestId = requestId;

            return PartialView(model);
        }

        public PartialViewResult LateRepayments(int timePeriod)
        {
            //var model = AdvanceMappingHelper.MapLateLoanRepayments(_ILoanDetailsTask.GetLateLoanRepayments(timePeriod));
            var model = AdvanceMappingHelper.MapLateLoanRepayments(_ILoanDetailsTask.GetLateLoanRepayments(timePeriod));

            ViewData["TimePeriod"] = timePeriod;

            return PartialView(model);
        }

        public JsonResult UpdateRepaymentDetails(long requestId, long repaymentId, int status,
            string adminUsername, string adminPassword, DateTime? date)
        {
            if (!userOpr.ValidateAdminUser(adminUsername, adminPassword))
            {
                return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
            }

            //if (status == (int)RepaymentPayStatus.Paid)
            //{
            //    var loanInvestors = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
            //            _ILoanDetailsDA.GetLoanInvestors(requestId, repaymentId));

            //    foreach (LoanInvestorViewModel item in loanInvestors)
            //    {
            //        _ITransactionsDA.LoanTransactionCredit(item.UserId, (decimal)item.RepaymentAmount, item.RequestId, "Monthly Repayment", "Monthly Repayment");
            //    }


            //    var loanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestId);

            //    if (loanRequest != null)
            //    {
            //        var repaymentAmount = loanRequest.tbl_LoanAmortization.FirstOrDefault().EmiAmount;

            //        _ITransactionsDA.LoanTransactionDebit((int)loanRequest.tbl_Users.UserID,
            //            repaymentAmount.HasValue ? repaymentAmount.Value : 0,
            //            requestId, "Internet", "Monthly Repayment");
            //    }
            //}

            _ITransactionsDA.ChangeRepaymentStatus(repaymentId, (RepaymentPayStatus)status, date);

            return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult UpdateLateRepaymentDetails(long requestId, int status,
            string adminUsername, string adminPassword, string comment, double latePayment, DateTime? date,
            bool moolahFees)
        {
            if (!userOpr.ValidateAdminUser(adminUsername, adminPassword))
            {
                return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
            }

            if (status == (int)RepaymentPayStatus.Paid)
            {
                var loanInvestors = Mapper.Map<IEnumerable<LoanInvestorEntity>, IEnumerable<LoanInvestorViewModel>>(
                        _ILoanDetailsDA.GetLoanInvestorsForDepLatePayments(requestId, latePayment));

                foreach (LoanInvestorViewModel item in loanInvestors)
                {
                    _ITransactionsDA.LoanTransactionCredit(item.UserId, (decimal)item.RepaymentAmount, item.RequestId, "Interent", "Accured Interest From Late Monthly Repayment");
                }

                if (loanInvestors.Count() > 0)
                {
                    var loanRequest = _ILoanDetailsDA.GetLoanRequestFromId(requestId);

                    if (loanRequest != null)
                    {
                        _ITransactionsDA.LoanTransactionDebit((int)loanRequest.tbl_Users.UserID,
                            (decimal)latePayment, requestId, "Interent", "Accured Interest From Late Monthly Repayment");
                    }
                }
            }
            _ITransactionsDA.ChangeLateRepaymentsStatus(requestId, latePayment, (RepaymentPayStatus)status, date);

            return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
        }


        public ActionResult AuditWarnings()
        {
            var model = Mapper.Map<IEnumerable<tbl_AuditLog>, IEnumerable<AuditLogViewModel>>(_IAuditLogDA.GetUserWarnings());
            return View(model);
        }

        public ActionResult ReconcileAccount()
        {
            return View();
        }

        public FileResult DownloadInternalAccountStatement(string from, string to)
        {
            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null).AddHours(24);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            var trns = _ILoanDetailsDA.GetAllLoanTransactionsByDateRange(_from, _to).ToList();
            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet("Details");

            for (int i = 0; i < 100; i++)
            {
                worksheet.Cells[i, 0] = new Cell("");
            }

            worksheet.Cells[0, 0] = new Cell("Transaction ID");
            worksheet.Cells[0, 1] = new Cell("Note ID");
            worksheet.Cells[0, 2] = new Cell("Payment Terms");
            worksheet.Cells[0, 3] = new Cell("User Number");
            worksheet.Cells[0, 4] = new Cell("Username");
            worksheet.Cells[0, 5] = new Cell("Display Name");
            worksheet.Cells[0, 6] = new Cell("Full Name");
            worksheet.Cells[0, 7] = new Cell("Account Number");
            worksheet.Cells[0, 8] = new Cell("Date Created");
            worksheet.Cells[0, 9] = new Cell("Initial Balance");
            worksheet.Cells[0, 10] = new Cell("Debited");
            worksheet.Cells[0, 11] = new Cell("Credited");
            worksheet.Cells[0, 12] = new Cell("Current Balance");
            worksheet.Cells[0, 13] = new Cell("Reference");

            for (int i = 0; i < trns.Count; i++)
            {
                worksheet.Cells[i + 1, 0] = new Cell(trns[i].TransactionId.ToString());
                worksheet.Cells[i + 1, 1] = new Cell(trns[i].Request_Id.ToString());
                worksheet.Cells[i + 1, 2] = new Cell(trns[i].PaymentTerms);

                if (trns[i].tbl_Users1 != null)
                {
                    worksheet.Cells[i + 1, 3] = new Cell(CommonMethods.GetAccountNumber(trns[i].tbl_Users1.UserID, trns[i].tbl_Users1.UserRole));
                    worksheet.Cells[i + 1, 4] = new Cell(trns[i].tbl_Users1.UserName);

                    var accountDetails = trns[i].tbl_Users1.tbl_AccountDetails.FirstOrDefault();

                    if (accountDetails != null)
                    {
                        worksheet.Cells[i + 1, 5] = new Cell(accountDetails.DisplayName);
                        worksheet.Cells[i + 1, 6] = new Cell(
                            trns[i].tbl_Users1.UserRole == "Borrower"
                            ? accountDetails.BusinessName
                             : string.Format("{0} {1}", accountDetails.FirstName, accountDetails.LastName)
                            );
                    }

                    string accountName = string.Empty;

                    int balanceID = (int)trns[i].BalanceID.Value;

                    if (balanceID == Settings.EarmarkAccountBalanceID)
                    {
                        accountName = "Earmark";
                    }
                    else if (balanceID == Settings.MsenseAccountBalanceID)
                    {
                        accountName = "MSense";
                    }
                    else if (balanceID == Settings.FundTransferAccountBalanceID)
                    {
                        accountName = "FundTransfer";
                    }

                    worksheet.Cells[i + 1, 7] = new Cell(accountName); ;
                }
                else if (trns[i].tbl_Users != null)
                {
                    worksheet.Cells[i + 1, 3] = new Cell(CommonMethods.GetAccountNumber(trns[i].tbl_Users.UserID, trns[i].tbl_Users.UserRole));
                    worksheet.Cells[i + 1, 4] = new Cell(trns[i].tbl_Users.UserName);

                    var accountDetails = trns[i].tbl_Users.tbl_AccountDetails.FirstOrDefault();

                    if (accountDetails != null)
                    {
                        worksheet.Cells[i + 1, 5] = new Cell(accountDetails.DisplayName);
                        worksheet.Cells[i + 1, 6] = new Cell(
                            trns[i].tbl_Users.UserRole == "Borrower"
                            ? accountDetails.BusinessName
                            : string.Format("{0} {1}", accountDetails.FirstName, accountDetails.LastName)
                            );
                    }

                    worksheet.Cells[i + 1, 7] = new Cell(CommonMethods.GetAccountNumber(trns[i].tbl_Users.UserID, trns[i].tbl_Users.UserRole));
                }

                worksheet.Cells[i + 1, 8] = new Cell(trns[i].DateCreated.ToDateTime());
                worksheet.Cells[i + 1, 9] = new Cell(trns[i].Amount);
                worksheet.Cells[i + 1, 10] = new Cell(trns[i].Debted);
                worksheet.Cells[i + 1, 11] = new Cell(trns[i].Credited);
                worksheet.Cells[i + 1, 12] = new Cell(trns[i].Balance);
                worksheet.Cells[i + 1, 13] = new Cell(trns[i].Reference);
            }

            workbook.Worksheets.Add(worksheet);
            MemoryStream fileStream = new MemoryStream();
            workbook.SaveToStream(fileStream);

            return File(fileStream.ToArray(), "application/vnd.ms-excel", "Internal Account Details.xls");
        }

        public FileResult DownloadFile(string from, string to)
        {
            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null).AddHours(24);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            ILoanDetailsDA tmp = new LoanDetailsDA();
            var trns = tmp.GetUserLoanTransactionsByDateRange(_from, _to).ToList();
            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet("Details");

            for (int i = 0; i < 100; i++)
            {
                worksheet.Cells[i, 0] = new Cell("");
            }

            worksheet.Cells[0, 0] = new Cell("Transaction ID");
            worksheet.Cells[0, 1] = new Cell("Note ID");
            worksheet.Cells[0, 2] = new Cell("Payment Terms");
            worksheet.Cells[0, 3] = new Cell("Username");
            worksheet.Cells[0, 4] = new Cell("Display Name");
            worksheet.Cells[0, 5] = new Cell("Full Name");
            worksheet.Cells[0, 6] = new Cell("Account Number");
            worksheet.Cells[0, 7] = new Cell("Date Created");
            worksheet.Cells[0, 8] = new Cell("Initial Balance");
            worksheet.Cells[0, 9] = new Cell("Debited");
            worksheet.Cells[0, 10] = new Cell("Credited");
            worksheet.Cells[0, 11] = new Cell("Current Balance");
            worksheet.Cells[0, 12] = new Cell("Reference");

            for (int i = 0; i < trns.Count; i++)
            {
                worksheet.Cells[i + 1, 0] = new Cell(trns[i].TransactionId.ToString());
                worksheet.Cells[i + 1, 1] = new Cell(trns[i].Request_Id.ToString());
                worksheet.Cells[i + 1, 2] = new Cell(trns[i].PaymentTerms);

                if (trns[i].tbl_Users != null)
                {
                    worksheet.Cells[i + 1, 3] = new Cell(trns[i].tbl_Users.UserName);

                    if (trns[i].tbl_Users.tbl_AccountDetails != null && trns[i].tbl_Users.tbl_AccountDetails.Count > 0)
                    {
                        var accDetails = trns[i].tbl_Users.tbl_AccountDetails.FirstOrDefault();
                        worksheet.Cells[i + 1, 4] = new Cell(accDetails.DisplayName);
                        worksheet.Cells[i + 1, 5] = new Cell(string.Format("{0} {1}", accDetails.FirstName, accDetails.LastName));

                    }

                    worksheet.Cells[i + 1, 6] = new Cell(CommonMethods.GetAccountNumber(trns[i].tbl_Users.UserID, trns[i].tbl_Users.UserRole));
                }

                worksheet.Cells[i + 1, 7] = new Cell(trns[i].DateCreated.ToDateTime());
                worksheet.Cells[i + 1, 8] = new Cell(trns[i].Amount);
                worksheet.Cells[i + 1, 9] = new Cell(trns[i].Debted);
                worksheet.Cells[i + 1, 10] = new Cell(trns[i].Credited);
                worksheet.Cells[i + 1, 11] = new Cell(trns[i].Balance);
                worksheet.Cells[i + 1, 12] = new Cell(trns[i].Reference);
            }

            workbook.Worksheets.Add(worksheet);
            MemoryStream fileStream = new MemoryStream();
            workbook.SaveToStream(fileStream);

            return File(fileStream.ToArray(), "application/vnd.ms-excel", "Reconsile Account Details.xls");
        }

        public ActionResult ManuallyTransferFunds()
        {
            return View();
        }

        
        public JsonResult ManuallyTransferFundsSubmit(FundsTransferViewModel model)
        {
            string error = string.Empty;

            if (!model.Amount.HasValue)
            {
                return Json(new { Status = "Failed", Error = "Amount can not be empty." }, JsonRequestBehavior.AllowGet);
            }

            if (!userOpr.ValidateAdminUser(model.AdminUsername, model.AdminPassword))
            {
                return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
            }

            if (!ModelState.IsValid || !_ITransactionsDA.TransferFundsByUsername(model.UserID, model.IsDebit, model.Amount.Value, "Manual", model.Description, out error))
            {
                return Json(new { Status = "Error", Error = error },JsonRequestBehavior.AllowGet);
            }

            return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult TransactionSucceeded()
        {
            return View();
        }

        public ActionResult AcceptedLoansPendingTransfer(long id, DateTime transferDate, double moolahFee, string status)
        {
            var loan = objAdminOperation.MatchedLoansById(id);
            loan.FundTransferDate = transferDate;
            loan.MoolahFees = moolahFee;
            loan.Status = status == "Approve"
                              ? LoanRequestStatus.FundApproved.ToString()
                              : LoanRequestStatus.FundRejected.ToString();
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            return View("AcceptedLoanDialog", loan);
        }

        [HttpPost]
        public ActionResult AcceptedLoansPendingTransferSubmit(long id, DateTime transferDate, double moolahFee,
                                                               string status, string reson, string adminUser)
        {
            // Process the loan
            ILoanAllocationDA da = new LoanAllocationDA();

            var request = _ILoanDetailsDA.GetLoanRequestFromId(id);

            switch (status)
            {
                case "Approve":
                    if (_ILoanAllocationTask.AcceptLoanByAdmin(id, transferDate, (decimal)moolahFee, adminUser))
                    {

                    }
                    break;
                case "Reject":
                    {

                        _ITransactionsDA.InternalTransactionDebit(Settings.MsenseAccountBalanceID, request.MoolahFees.Value, id, "Internet",
                            "Matched Note Cancelled by Admin.Refund Moolah Fees", request.User_ID.Value);
                        _ITransactionsDA.InternalTransactionCredit(Settings.EarmarkAccountBalanceID, request.FinalAmount.Value, id, "Internet",
                            "Matched Note Cancelled by Admin,Funds back from Borrower", request.User_ID.Value);
                        _ITransactionsDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID, request.FinalAmount.Value, id, "Internet",
                            "Matched Note Cancelled by Admin, Refund investors", request.User_ID.Value);

                        var userList = da.RejectPendingTransfer(id, reson, adminUser);

                        IEmailSender tmp = new EmailSender();
                        tmp.SendRejectFundTransferEmail(userList, id, reson);
                    }

                    break;
            }

            ModelState.Clear();
            return RedirectToAction("AcceptedLoans");
        }

      
        public JsonResult ValidateAdminAccount(string username, string password)
        {

            return Json(userOpr.ValidateAdminUser(username, password), JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _LoanPayments(int timePeriod)
        {
            var model = Mapper.Map<IEnumerable<tbl_LoanPayments>, IEnumerable<LoanPaymentViewModel>>
                (_ILoanPaymentTask.GetLoanPayments(timePeriod));

            ViewData["TimePeriod"] = timePeriod;
            return PartialView(model);
        }

        public PartialViewResult _AccountAdjustment(int timePeriod)
        {
            var adjustments = _ILoanPaymentTask.GetAccountAdjustments(timePeriod);

            var model = Mapper.Map<IEnumerable<tbl_AccountAdjustment>, IEnumerable<AccountAdjustmentViewModel>>(
                adjustments);

            ViewData["TimePeriod"] = timePeriod;
            return PartialView(model);
        }

        public JsonResult CalculateLateFeesByDate(long repaymentID, DateTime date, double paidAmount, double repaymentsOutstanding)
        {
            double[] accruedInterestAndLateFees = _ITransactionsDA.GetAccruedInterestAndLateFees(repaymentID, date);

            double outstanding = Math.Round(repaymentsOutstanding + accruedInterestAndLateFees[0] + accruedInterestAndLateFees[1] - paidAmount, 2);

            return Json(new
            {
                Accrued = accruedInterestAndLateFees[0],
                LateFees = accruedInterestAndLateFees[1],
                Outstanding = outstanding
            }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _LoanDocuments(long requestID, string username)
        {
            ViewBag.RequestId = requestID;
            ViewBag.Username = username;

            return PartialView();
        }

        public PartialViewResult _AddBalance(long UserID)
        {
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            ViewBag.UserID = UserID;

            var model = Mapper.Map<UserAccountEntity, UserAccountViewModel>(_IUser.GetUserAccountFromUserID(UserID));

            return PartialView(model == null ? new UserAccountViewModel() : model);
        }

        public PartialViewResult _FundTransfer(long UserID)
        {
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            ViewBag.UserID = UserID;

            var model = Mapper.Map<UserAccountEntity, UserAccountViewModel>(_IUser.GetUserAccountFromUserID(UserID));

            return PartialView(model == null ? new UserAccountViewModel() : model);
        }

        public PartialViewResult _UpdateWithdraw(long withdrawID, string status)
        {
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();
            ViewBag.WithdrawID = withdrawID;
            ViewBag.Status = status;

            return PartialView();
        }

        public PartialViewResult _ExpireRequest(long requestID)
        {
            ViewBag.RequestID = requestID;
            ViewBag.AdminUsers = userOpr.GetAllAdminUsers();

            return PartialView();
        }

        public JsonResult ExpireRequest(long requestID, string adminUsername, string adminPassword)
        {
            if (!userOpr.ValidateAdminUser(adminUsername, adminPassword))
            {
                return Json(new { Status = "Failed", Error = "Admin authentication failed." }, JsonRequestBehavior.AllowGet);
            }

            return Json(new
            {
                Status = _ILoanDetailsTask.ExpireLoanRequests(requestID) ? StatusMessages.SUCCESS : StatusMessages.FAILED
            }, JsonRequestBehavior.AllowGet);
        }

        public FileContentResult DownloadInvestors(bool hasBalance)
        {
            var result = _ILoanDetailsDA.GetInvestorList(hasBalance);
            var memoryStream = new MemoryStream();
            CreateInvestorWorksheetHeader(result, a => WorkBookBuilder.CreateWorkBook(a).SaveToStream(memoryStream));
            return File(memoryStream.ToArray(), "application/vnd.ms-excel", "Investors.xls");
        }

        private void CreateInvestorWorksheetHeader(IEnumerable<MoolahConnect.Services.Entities.InvestorDTO> investorlist, Action<Worksheet> workbuilder)
        {
            var worksheet = new WorksheetBuilder("Investor");
            worksheet.CreateHeader("UserID", "UserName", "UserRole", "Isactive", "DateCreated", "InvestorType", "AdminVerification", "IsSubmitted",
                                   "IsEmailVerified", "FirstName", "LastName", "DisplayName", "AccountType", "Nationality", "DateofBirth", "Gender", "Main_OfficeContactnumber","Current Balance");
            worksheet.CreateBody(a =>
            {
                for (int i = 1; i < 100; i++)
                {
                    a.Cells[i, 0] = new Cell("");
                }

                var rowindex = 1;
                foreach (var investorDto in investorlist)
                {
                    a.Cells[rowindex, 0] = new Cell(investorDto.UserID.ToString());
                    a.Cells[rowindex, 1] = new Cell(investorDto.UserName);
                    a.Cells[rowindex, 2] = new Cell(investorDto.UserRole);
                    a.Cells[rowindex, 3] = new Cell(investorDto.IsActive);
                    a.Cells[rowindex, 4] = new Cell(investorDto.DateCreated.ToDate("dd/MM/yyyy"));
                    a.Cells[rowindex, 5] = new Cell(investorDto.InvestorType);
                    a.Cells[rowindex, 6] = new Cell(investorDto.AdminVerification);
                    a.Cells[rowindex, 7] = new Cell(investorDto.IsSubmitted == true ? 1 : 0);
                    a.Cells[rowindex, 8] = new Cell(investorDto.IsEmailVerified == true ? 1 : 0);
                    a.Cells[rowindex, 9] = new Cell(investorDto.FirstName);
                    a.Cells[rowindex, 10] = new Cell(investorDto.LastName);
                    a.Cells[rowindex, 11] = new Cell(investorDto.DisplayName);
                    a.Cells[rowindex, 12] = new Cell(investorDto.AccountType);
                    a.Cells[rowindex, 13] = new Cell(investorDto.Nationality);
                    a.Cells[rowindex, 14] = new Cell(investorDto.DateofBirth.ToDate("dd/MM/yyyy"));
                    a.Cells[rowindex, 15] = new Cell(investorDto.Gender);
                    a.Cells[rowindex, 16] = new Cell(investorDto.MainOfficeContactnumber);
                    a.Cells[rowindex, 17] = new Cell(investorDto.CurrentBalance, new CellFormat(CellFormatType.Number, "#,###.00"));
                    rowindex++;
                }
                workbuilder(a);
            });


        }

        public FileContentResult GenerateLoanInvestorsWithKADetails(long requestID)
        {
            var result = _IUser.GetInvestorKnowledgeAssesmentDetails(requestID);
            var memoryStream = new MemoryStream();
            CreateLoanInvestorsWithKADetailsHeaders(result, a => WorkBookBuilder.CreateWorkBook(a).SaveToStream(memoryStream));
            return File(memoryStream.ToArray(), "application/vnd.ms-excel", "InvestorsWithKnowledgeAssesmentDetails.xls");
        }

        private void CreateLoanInvestorsWithKADetailsHeaders(IEnumerable<InvestorKnowledgeAssesmentDetails> investorlist, Action<Worksheet> workbuilder)
        {
            var worksheet = new WorksheetBuilder("Investor");
            worksheet.CreateHeader("Userid","OfferAmount","OfferedRate","AcceptedAmount","AcceptedRate",
                "OfferStatus","AccountNumber","Username","DisplayName","Fullname",
                "Education","InvestedFinancialProductsInd","InvestedFinancialProductsCop","Employment",
                "Occupation","Other");
            worksheet.CreateBody(a =>
            {
                for (int i = 1; i < 100; i++)
                {
                    a.Cells[i, 0] = new Cell("");
                }

                var rowindex = 1;
                foreach (var investorDto in investorlist)
                {
                    a.Cells[rowindex, 0] = new Cell(investorDto.Userid.ToString());
                    a.Cells[rowindex, 1] = new Cell(investorDto.OfferAmount, new CellFormat(CellFormatType.Number, "#,###.00"));
                    a.Cells[rowindex, 2] = new Cell(investorDto.OfferedRate);
                    a.Cells[rowindex, 3] = new Cell(investorDto.AcceptedAmount, new CellFormat(CellFormatType.Number, "#,###.00"));
                    a.Cells[rowindex, 4] = new Cell(investorDto.AcceptedRate);
                    a.Cells[rowindex, 5] = new Cell(investorDto.OfferStatus);
                    a.Cells[rowindex, 6] = new Cell(investorDto.AccountNumber);
                    a.Cells[rowindex, 7] = new Cell(investorDto.Username);
                    a.Cells[rowindex, 8] = new Cell(investorDto.DisplayName);
                    a.Cells[rowindex, 9] = new Cell(investorDto.Fullname);
                    a.Cells[rowindex, 10] = new Cell(investorDto.Education);
                    a.Cells[rowindex,11] = new Cell(investorDto.InvestedFinancialProductsInd);
                    a.Cells[rowindex, 12] = new Cell(investorDto.InvestedFinancialProductsCop);
                    a.Cells[rowindex, 13] = new Cell(investorDto.Employment);
                    a.Cells[rowindex, 14] = new Cell(investorDto.Occupation);
                    a.Cells[rowindex, 15] = new Cell(investorDto.Other);
                    rowindex++;
                }
                workbuilder(a);
            });


        }

        public FileContentResult DownloadAcceptedOffers(long requestID)
        {
            var result = _ILoanDetailsDA.GetAcceptedOfferDetails(requestID);
            var memoryStream = new MemoryStream();
            CreateAcceptedOffersWorksheetHeader(result, a => WorkBookBuilder.CreateWorkBook(a).SaveToStream(memoryStream));
            return File(memoryStream.ToArray(), "application/vnd.ms-excel", "AcceptedOffers.xls");
        }

        private void CreateAcceptedOffersWorksheetHeader(IEnumerable<InvestorOfferDTO> investorlist, Action<Worksheet> workbuilder)
        {
            var worksheet = new WorksheetBuilder("AcceptedOffer");
            worksheet.CreateHeader("DisplayName", "NameAsInNRIC", "NRICOrPassportNumber","OfferedAmount", "InterestRateOffered",
                "AcceptedAmount","AcceptedRate",
                "RepaymentAmountPortion", "TotalInterestsPayabletoPayee");

            worksheet.CreateBody(a =>
            {
                var rowindex = 1;

                for (int i = 1; i < 100; i++)
                {
                    a.Cells[i, 0] = new Cell("");
                }

                foreach (var investorDto in investorlist)
                {
                    a.Cells[rowindex, 0] = new Cell(investorDto.DisplayName);
                    a.Cells[rowindex, 1] = new Cell(investorDto.NRICName);
                    a.Cells[rowindex, 2] = new Cell(investorDto.NRICOrPassportNumber);
                    a.Cells[rowindex, 3] = new Cell(investorDto.OfferedAmount);
                    a.Cells[rowindex, 4] = new Cell(investorDto.OfferedRate);
                    a.Cells[rowindex, 5] = new Cell(investorDto.AcceptedAmount);
                    a.Cells[rowindex, 6] = new Cell(investorDto.AcceptedRate);
                    a.Cells[rowindex, 7] = new Cell(investorDto.RepaymentAmountPortion);
                    a.Cells[rowindex, 8] = new Cell(investorDto.TotalInterest);
                    rowindex++;
                }
                workbuilder(a);
            });


        }

        public FileContentResult DownloadRejectedOffers(long requestID)
        {
            var result = _ILoanDetailsDA.GetRejectedOfferDetails(requestID);
            var memoryStream = new MemoryStream();
            CreateRejectedOffersWorksheetHeader(result, a => WorkBookBuilder.CreateWorkBook(a).SaveToStream(memoryStream));
            return File(memoryStream.ToArray(), "application/vnd.ms-excel", "RejectedOffers.xls");
        }
        public FileContentResult DownloadOffersAndWithdrawal(long requestID)
        {
            var result = _ILoanDetailsDA.GetOfferAndWithdrawal(requestID);
            var memoryStream = new MemoryStream();
            CreateOffersAndWithdrawalListWorksheetHeader(result, a => WorkBookBuilder.CreateWorkBook(a).SaveToStream(memoryStream));
            return File(memoryStream.ToArray(), "application/vnd.ms-excel", "Revised Offer and Withdrawal list.xls");
        }
        private void CreateRejectedOffersWorksheetHeader(IEnumerable<InvestorOfferDTO> investorlist, Action<Worksheet> workbuilder)
        {
            var worksheet = new WorksheetBuilder("RejectedOffers");
            worksheet.CreateHeader("DisplayName", "NameAsInNRIC", "NRICPassportNumber", "AmountRefunded", "InterestRateOffered");
            worksheet.CreateBody(a =>
            {
                for (int i = 1; i < 100; i++)
                {
                    a.Cells[i, 0] = new Cell("");
                }

                var rowindex = 1;
                foreach (var investorDto in investorlist)
                {
                    a.Cells[rowindex, 0] = new Cell(investorDto.DisplayName);
                    a.Cells[rowindex, 1] = new Cell(investorDto.NRICName);
                    a.Cells[rowindex, 2] = new Cell(investorDto.NRICOrPassportNumber);
                    a.Cells[rowindex, 3] = new Cell(investorDto.AmountRefunded);
                    a.Cells[rowindex, 4] = new Cell(investorDto.OfferedRate);
                    rowindex++;
                }
                workbuilder(a);
            });


        }
        private void CreateOffersAndWithdrawalListWorksheetHeader(IEnumerable<InvestorOfferDTO> investorlist, Action<Worksheet> workbuilder)
        {
            var worksheet = new WorksheetBuilder("Revised Offer and Withdrawal list");
            worksheet.CreateHeader("User Name", "Display Name", "Full Name", "Account Number", "Offer ID", "Date Created", "Offered Amount", "Offered Rate", "Offer Status", "Withdrawal Date");
            worksheet.CreateBody(a =>
            {
                for (int i = 1; i < 100; i++)
                {
                    a.Cells[i, 0] = new Cell("");
                }

                var rowindex = 1;
                foreach (var investorDto in investorlist)
                {
                    a.Cells[rowindex, 0] = new Cell(investorDto.UserName);
                    a.Cells[rowindex, 1] = new Cell(investorDto.DisplayName);
                    a.Cells[rowindex, 2] = new Cell(investorDto.FullName);
                    a.Cells[rowindex, 3] = new Cell(investorDto.AccountNumber);
                    a.Cells[rowindex, 4] = new Cell(investorDto.OfferID);
                    a.Cells[rowindex, 5] =
                        new Cell(investorDto.DateCreated.ToDateTime());
                    a.Cells[rowindex, 6] = new Cell(investorDto.OfferedAmountDecimal);
                    a.Cells[rowindex, 7] = new Cell(investorDto.OfferedRatedDecimal);
                    a.Cells[rowindex, 8] = new Cell(investorDto.OfferStatus);
                    if (investorDto.WithdrawalDate.HasValue)
                    {
                        a.Cells[rowindex, 9] = new Cell(investorDto.WithdrawalDate.ToDateTime());
                    }
                    rowindex++;
                }
                workbuilder(a);
            });


        }
        public JsonResult ChangeAccountAdjustment(long id, bool adjusted)
        {
            _ITransactionsDA.ChangeAccountAdjustment(id, adjusted);

            return Json(new { Status = "Succeed" }, JsonRequestBehavior.AllowGet);
        }

    }
}
