﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MoolahConnect.Util.Logging;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using System.IO;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.Email;

namespace MoolahConnectnew.Controllers
{
    public class AppController : Controller
    {
        IPdfGeneratorTask _IPdfGeneratorTask;

        public AppController()
        {
            _IPdfGeneratorTask = new PdfGeneratorTask();
        }

        public ActionResult Index()
        {
            return View();
        }

        public void Log(string url = "", string host = "", string error = "", string file = "", string lineNo = "")
        {
            var username = HttpContext.User.Identity != null ? HttpContext.User.Identity.Name : string.Empty;

            MoolahLogManager.LogClientSideException(error, file, lineNo, username);
        }

        public ActionResult Test()
        {
            //MemoryStream stream = _IPdfGeneratorTask.GenerateBorrowerLoanContract(2022);
            //MemoryStream stream2 = _IPdfGeneratorTask.GenerateInvestorLoanContract(1053, 136, "0001", 46);

            //stream.WriteTo(new FileStream("C://Moolah//PDF//MySamplePDF2.pdf", FileMode.Create));
            //stream.Close();

            IEmailSender emailSender = new EmailSender();
            //emailSender.BorrowerLoanPublished("kkhatd@gmail.com", 2000, 10);
            //emailSender.InvestorLoanPublished("kkhatd@gmail.com","Dan Com", 2000, 10);
            //emailSender.FundTransferOut("kkhatd@gmail.com", 2000, 8000);
            //emailSender.OnUserVerificationChanged("kkhatd@gmail.com", "approved", false);

            //emailSender.OnRejectedLoanOfferToInvetor("nevikduran@gmail.com", "2012", 1000, 12, DateTime.UtcNow, "Dan ABC",
            //    ReasonForRejectedOffers.DUE_TO_DEMAND);

            emailSender.OnUserVerificationChanged("kkhatd@gmail.com", "Approved", false);

            return View();
        }

    }
}
