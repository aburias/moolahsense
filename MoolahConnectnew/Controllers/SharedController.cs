﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using AutoMapper;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;
using MoolahConnectClassLibrary.DAL;
using MoolahConnectnew.ViewModels;
using MoolahConnectnew.Providers;
using MoolahConnect.Util;

namespace MoolahConnectnew.Controllers
{
    public class SharedController : Controller
    {
        private ILoanPaymentsTask _ILoanPaymentsTask;
        private IFileUploadDA _IFileUploadDA;
        private IFileUploadTask _IFileUploadTask;
        ITransactionsDA _ITransactionDA;
        private IBorrowerLoanDetailsDA _IBorrowerLoanDetailsDA;
        private ILoanAllocationDA _ILoanAllocationDA;
        private ILoanDetailsDA _ILoanDetailsDA;
        ILoanDetailsTask _ILoanDetailsTask;

        public SharedController()
        {
            _ILoanPaymentsTask = new LoanPaymentsTask();
            _IFileUploadDA = new FileUploadDA();
            _IFileUploadTask = new FileUploadTask();
            _ITransactionDA = new TransactionsDA();
            _IBorrowerLoanDetailsDA = new BorrowerLoanDetailsDA();
            _ILoanAllocationDA = new LoanAllocationDA();
            _ILoanDetailsDA = new LoanDetailsDA();
            _ILoanDetailsTask = new LoanDetailsTask();
        }

        public ActionResult Index()
        {
            return View();
        }

        public PartialViewResult _RepaymentSchedule(long requestID, double amount, double rate, string term, DateTime startDate)
        {
            var model = Mapper.Map<IEnumerable<RepaymentEntity>, IEnumerable<RepaymentViewModel>>(
                _ILoanPaymentsTask.GetRepaymentSchedule(requestID, amount, rate, term, startDate)).ToList();
            ViewBag.RepaymentMessages = _ILoanPaymentsTask.GetRepaymentMessageses(requestID);
            return PartialView(model);
        }

        public PartialViewResult _InvestorRepaymentScheduleFromRequestID(long requestID, double amount, double rate, string term, DateTime startDate)
        {
            var model = Mapper.Map<IEnumerable<RepaymentEntity>, IEnumerable<RepaymentViewModel>>(
                _ILoanPaymentsTask.GetRepaymentSchedule(requestID, amount, rate, term, startDate, true)).ToList();
            ViewBag.RepaymentMessages = _ILoanPaymentsTask.GetRepaymentMessageses(requestID);
            return PartialView("_RepaymentScheduleInv", model);
        }

        public PartialViewResult _RepaymentScheduleFromRequestID(long requestID)
        {
            var model = Mapper.Map<IEnumerable<RepaymentEntity>, IEnumerable<RepaymentViewModel>>(
                _ILoanDetailsDA.GetRepaymentsByRequestID(requestID)).ToList();

            return PartialView("_RepaymentSchedule", model);
        }

        /// <summary>
        /// Method for checking business registration number availability in registered users list
        /// </summary>
        /// <param name="BusinessRegistrationnumber"></param>
        /// <returns></returns>
        public JsonResult CheckBusinessRegNumberAvailbilty(string BusinessRegistrationnumber)
        {
            return Json(ContollerExtensions.CheckBusinessRegNumberAvailbilty(BusinessRegistrationnumber));
        }

        public string Upload(HttpPostedFileBase FileData, FileType type, string username, long? requestId = null, bool multi = true)
        {
            if (string.IsNullOrEmpty(username))
            {
                ViewData["error"] = StatusMessages.SYSTEM_ERROR;
                return StatusMessages.FAILED;
            }

            var user = Membership.GetUser(username);

            if (user == null)
            {
                ViewData["error"] = StatusMessages.SYSTEM_ERROR;
                return StatusMessages.FAILED;
            }

            Guid userId = (Guid)user.ProviderUserKey;

            if (_IFileUploadTask.Upload(userId.ToString(), type.ToString(),
                FileData.FileName, FileData.InputStream,
                requestId.HasValue ? requestId.ToString() : null))
            {


                if (user == null ||
                    !_IFileUploadDA.InsertUploadedFile(FileData.FileName, type,
                    userId, requestId, null, multi))
                {
                    ViewData["error"] = StatusMessages.SYSTEM_ERROR;
                    return StatusMessages.FAILED;
                }

                return StatusMessages.SUCCESS;
            }
            else
            {
                ViewData["error"] = StatusMessages.FILE_UPLOAD_FAILED;
                return StatusMessages.FAILED;
            }
        }

        public ActionResult DownloadFile(Guid userId, FileType type, string fileName, long? requestId = null, int? offerId = null)
        {
            var blob = _IFileUploadTask.Download(userId.ToString(), type.ToString(), fileName,
                requestId.HasValue ? requestId.ToString() : null,
                offerId.HasValue ? offerId.ToString() : null);

            if (blob != null)
            {
                return File(blob.FileStream, blob.ContentType, fileName);
            }
            else return null;
        }

        public ActionResult DownloadFileFromFileName(string filePath)
        {
            var blob = _IFileUploadTask.Download(filePath);

            if (blob != null)
            {
                return File(blob.FileStream, blob.ContentType, filePath);
            }
            else return null;
        }

        public JsonResult DeleteFile(int fileId, string fileName, FileType type, Guid userId, List<string> comments, long? requestId = null)
        {
            if (!_IFileUploadTask.Delete(userId.ToString(), type.ToString(), fileName,
                requestId.HasValue ? requestId.ToString() : null))
                return Json(new { Status = StatusMessages.FAILED }, JsonRequestBehavior.AllowGet);

            _IFileUploadDA.DeleteFile(fileId);
            _IFileUploadDA.UpdateUploadedFileComments(comments);
            return Json(new { Status = StatusMessages.SUCCESS }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _FilesList(string username, FileType type, string uniqueId, long? requestId = null, string comments = null)
        {
            if (!string.IsNullOrEmpty(comments))
            {

                var idComments = new List<string>();

                var items = comments.Split(',');

                foreach (string item in items)
                {
                    idComments.Add(item.Replace(":", ","));
                }

                _IFileUploadDA.UpdateUploadedFileComments(idComments);
            }

            var model = new List<FileModel>();

            var user = Membership.GetUser(username);

            if (user != null)
            {
                model = Mapper.Map<IEnumerable<tbl_Files>, IEnumerable<FileModel>>(
                    _IFileUploadDA.GetFilesFromUserIdAndTypeAndRequestId((Guid)user.ProviderUserKey, type, requestId)).ToList();
            }

            ViewBag.UniqueId = uniqueId;
            return PartialView(model);
        }

        public PartialViewResult _YouTubeLinksList(long requestId, string comments = null)
        {
            if (!string.IsNullOrEmpty(comments))
            {

                var idComments = new List<string>();

                var items = comments.Split(',');

                foreach (string item in items)
                {
                    idComments.Add(item.Replace(":", ","));
                }

                _IFileUploadDA.UpdateUploadedFileComments(idComments);
            }

            var model = new List<FileModel>();

            model = Mapper.Map<IEnumerable<tbl_Files>, IEnumerable<FileModel>>(
                _IFileUploadDA.GetFilesByRequestId(requestId)).Where(x => x.Type == FileType.YouTubeLink).ToList();

            return PartialView(model);
        }

        public JsonResult AddYouTubeLink(string link, string username, long requestId)
        {
            if (_IBorrowerLoanDetailsDA.AddYouTubeLink(link, username, requestId))
            {
                return Json(new { Status = StatusMessages.SUCCESS }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = StatusMessages.FAILED }, JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult DeleteYouTubeLink(int id, List<string> comments)
        {
            if (_IFileUploadDA.DeleteFile(id))
            {
                _IFileUploadDA.UpdateUploadedFileComments(comments);
                return Json(new { Status = StatusMessages.SUCCESS }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Status = StatusMessages.FAILED }, JsonRequestBehavior.AllowGet);
            }
        }

        #region AllLoanTransactinofInvestor
        public ActionResult LoanTransactions()
        {
            var model = new List<TransactionViewModel>();

            model = Mapper.Map<IEnumerable<tbl_LoanTransactions>, IEnumerable<TransactionViewModel>>(
                _ITransactionDA.GetTransactionbyUser(User.Identity != null ? User.Identity.Name : string.Empty))
                .ToList();

            return View(model);
        }

        public PartialViewResult _Transactions(string from, string to)
        {
            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            var model = new List<TransactionViewModel>();

            model = Mapper.Map<IEnumerable<tbl_LoanTransactions>, IEnumerable<TransactionViewModel>>(
                _ITransactionDA.GetTransactionbyUser(User.Identity != null ? User.Identity.Name : string.Empty, _from, _to))
                .ToList();

            return PartialView(model);
        }

        #endregion

        public JsonResult ValidatePassword(string password, string purpose)
        {
            string _status = string.Empty;

            if (Membership.ValidateUser(HttpContext.User.Identity.Name, password))
            {
                MoolahLogManager.Log(string.Format("{0} : {1}", purpose, StatusMessages.SUCCESSFULL_PASSWORD_VALIDATION), LogType.Warn, HttpContext.User.Identity.Name);
                _status = StatusMessages.SUCCESS;
            }
            else if (Membership.GetUser(HttpContext.User.Identity.Name).IsLockedOut)
            {
                MoolahLogManager.Log(string.Format("{0} : {1}", purpose, ErrorMessages.INVALID_PASSWORD), LogType.Warn, HttpContext.User.Identity.Name);
                MoolahLogManager.Log(string.Format("{0} : {1}", purpose, ErrorMessages.ACCOUNT_LOCKED), LogType.Warn, HttpContext.User.Identity.Name);
                MoolahMemberhipProvider.MakeLogoff();
                _status = StatusMessages.ACCOUNT_LOCKED;
            }
            else
            {
                MoolahLogManager.Log(string.Format("{0} : {1}", purpose, ErrorMessages.INVALID_PASSWORD), LogType.Warn, HttpContext.User.Identity.Name);
                _status = StatusMessages.FAILED;
            }

            return Json(new { status = _status }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MoolahBoard()
        {
            var model = new LoanRequestBoardViewModel();
            var loans = _ILoanAllocationDA.GetAllPublishedLoanRequests().ToList();
            foreach (var loan in loans)
            {
                var viewmodel = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(loan);
                var loanCalculations = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(loan.RequestId, loan.Amount);
                viewmodel.Funded = loanCalculations[0];
                var timeRemaining = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(loan.PublishedDate),
                    Convert.ToInt32(Settings.LoanRequestExpiryDays));
                viewmodel.Days = timeRemaining[0];
                viewmodel.Hours = timeRemaining[1];
                viewmodel.Minutes = timeRemaining[2];

                if (viewmodel.Minutes > 0)
                    model.LoanRequestViewModels.Add(viewmodel);
            }

            return View(model);
        }

        public JsonResult SendEmailsToBorrowerAndApprovedInvestorsOnPublished(long requestID)
        {
            _ILoanDetailsTask.NotifyApprovedInvestorsAndBorrowerOnLoanPublished(requestID);
            return Json(new {  }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendEmailsToBorrowerAndApprovedInvestorsOnRateChange(long requestID,double oldRate, double newRate)
        {
            _ILoanDetailsTask.NotifyApprovedInvestorsAndBorrowerOnTargetRateChanged(requestID,oldRate,newRate);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SendEmailsToBorrowerAndInvestedInvestorsOnAccpted(long requestID)
        {
            _ILoanDetailsTask.SendEmailsToBorrowerAndInvestedInvestorsOnAccpted(requestID);
            return Json(new { }, JsonRequestBehavior.AllowGet);
        }
    }
}
