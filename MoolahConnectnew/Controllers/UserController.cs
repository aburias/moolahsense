﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using System.Web.Security;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectClassLibrary.DAL;
using System.Collections;
using NLog.LayoutRenderers;
using NLog.Config;
using NLog;
using MoolahConnect.Util.Logging;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using AutoMapper;
using MoolahConnect.Services.Entities;
using MoolahConnectnew.ViewModels;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.Enums;
using MoolahConnectnew.Providers;
using MoolahConnect.Util;
using Recaptcha;
using MoolahConnect.Util.Email;


namespace MoolahConnectnew.Controllers
{
    [System.Web.Mvc.OutputCache(NoStore = true, Duration = 0, VaryByParam = "*")]
    public class UserController : Controller
    {
        private UserCommonOperations objusercommon;
        private IEmailSender _IEmailSender;
        private ISecurityQuestions _ISecurityQuestions;
        private IUser _IUser;
        private IVerificationToken _IVerificationToken;
        private IPdfGeneratorTask _IPdfGeneratorTask;

        public UserController()
        {
            objusercommon = new UserCommonOperations();
            _IEmailSender = new EmailSender();
            _ISecurityQuestions = new SecurityQuestions();
            _IUser = new User();
            _IVerificationToken = new VerificationToken();
            _IPdfGeneratorTask = new PdfGeneratorTask();
        }

        public ActionResult Index(string ReturnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                string role = objusercommon.GetUserRole();

                if (role == UserRole.Borrower.ToString())
                    return RedirectToAction("Index", "Borrower");
                else if (role == UserRole.Investor.ToString())
                    return RedirectToAction("Index", "Investor");
            }
            else
            {
                Response.Redirect(Settings.HomePageUrl);
            }

            HomeLoanRequests objLoanRequest = new HomeLoanRequests();
            try
            {
                /////////////////////////////////Get All approved Loans which are   ///////////////////////////
                objLoanRequest = objusercommon.GetAllAprrovedLoansList();

                var prviders = Membership.Providers;
                var users = Membership.GetAllUsers();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            return View(objLoanRequest);
        }

        public ActionResult Login(string message = null)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Index");

            var model = new LoginViewModel();
            //model.SecurityQuestions = Mapper.Map<IEnumerable<tbl_SecurityQuestions>, IEnumerable<SelectListItem>>(_ISecurityQuestions.Get()).ToList();

            if (message != null)
            {
                ViewData["Error"] = message;
            }

            return View(model);
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            var userRole = string.Empty;
            var user = Membership.GetUser(model.Username);

            if (user == null)
            {
                ViewData["Error"] = ErrorMessages.USERNAME_DOES_NOT_EXIST;
                return View(model);
            }

            if (user.IsLockedOut)
            {
                ViewData["Error"] = ErrorMessages.ACCOUNT_LOCKED;
                model.isLocked = true;
                return View(model);
            }

            if (!Membership.ValidateUser(model.Username, model.Password))
            {
                string errorMessage = ErrorMessages.INVALID_PASSWORD;
                MoolahLogManager.Log(ErrorMessages.INVALID_PASSWORD, LogType.Warn, model.Username);
                int attempts = _IUser.GetFailedPasswordAttemptsCount((Guid)user.ProviderUserKey);

                if (attempts == Membership.MaxInvalidPasswordAttempts - 1)
                    ViewData["Warning"] = string.Format(ErrorMessages.ACCOUNT_LOCKOUT_W, Membership.MaxInvalidPasswordAttempts);
                else if (attempts == Membership.MaxInvalidPasswordAttempts)
                {
                    model.isLocked = true;
                    errorMessage = errorMessage + " " + ErrorMessages.ACCOUNT_LOCKED;
                    MoolahLogManager.Log(ErrorMessages.ACCOUNT_LOCKED, LogType.Warn, model.Username);
                }

                ViewData["Error"] = errorMessage;
                return View(model);
            }

            var dbUser = _IUser.GetUserFromAspNetId((Guid)user.ProviderUserKey);

            if (dbUser.IsEmailVerified == null || dbUser.IsEmailVerified.Value == false)
            {
                return RedirectToAction("Message", new { message = ErrorMessages.ACCOUNT_NOT_VERIFIED });
            }

            if (dbUser.AdminVerification == "Rejected")
            {
                ViewData["Error"] = ErrorMessages.LOGIN_ACCOUNT_REJECTED_ERROR;
                return View(model);
            }

            MoolahLogManager.Log(StatusMessages.SUCCESSFULL_LOGIN, LogType.Warn, model.Username);
            FormsAuthentication.SetAuthCookie(model.Username, true);

            #region Redirect user based on user role

            userRole = _IUser.GetUserRoleByUsername(model.Username);

            if (userRole == UserRole.Borrower.ToString())
            {
                Response.Cookies["_UsersRole_"].Value = UserRole.Borrower.ToString();
                CreateUserCookie(model.Username, UserRole.Borrower.ToString());
                return RedirectToAction("Index", "Issuer");
            }
            else if (userRole == "Investor")
            {
                Response.Cookies["_UsersRole_"].Value = UserRole.Investor.ToString();
                CreateUserCookie(model.Username, UserRole.Investor.ToString());
                return RedirectToAction("Index", "Investor");
            }
            else
            {
                ViewData["Error"] = ErrorMessages.ROLE_CONSTRAINT;
                return View(model);
            }

            #endregion
        }

        private void CreateUserCookie(string username, string userRole)
        {
            FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, username, DateTime.UtcNow, DateTime.UtcNow.AddDays(2), false, userRole);
            String d1 = FormsAuthentication.Encrypt(tkt);
            HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, d1);
            Response.Cookies.Add(ck);

            if (!HttpContext.Response.Cookies.AllKeys.Contains("_UsersRole_"))
            {
                HttpCookie cookieUserRole = new HttpCookie("_UsersRole_");
                Response.Cookies["_UsersRole_"].Value = userRole;
                cookieUserRole.Value = userRole;
                Response.Cookies.Add(cookieUserRole);
            }
        }

        public ActionResult ResetCredentialStep1()
        {
            return View();
        }

        [HttpPost]
        public ActionResult ResetCredentialStep1(LoginViewModel model)
        {
            var user = Membership.GetUser(model.Username);

            if (user == null)
            {
                ViewData["Error"] = ErrorMessages.USERNAME_DOES_NOT_EXIST;
                return View();
            }
            else if (user.IsLockedOut)
            {
                ViewData["Error"] = ErrorMessages.ACCOUNT_LOCKED;
                return View();
            }

            var tblUser = _IUser.GetUserFromAspNetId((Guid)user.ProviderUserKey);

            if (tblUser == null)
            {
                ViewData["Error"] = ErrorMessages.UNEXPECTED_ERROR;
                return View();
            }

            if (tblUser.IsEmailVerified == null || !tblUser.IsEmailVerified.Value)
            {
                ViewData["Error"] = ErrorMessages.ACCOUNT_NOT_VERIFIED;
                return View();
            }

            return RedirectToAction("ResetCredentialStep2", new { username = model.Username });

        }

        public ActionResult ResetCredentialStep2(string username)
        {
            var model = new LoginViewModel();
            model.Username = username;
            model.SecurityQuestions = Mapper.Map<IEnumerable<tbl_SecurityQuestions>, IEnumerable<SelectListItem>>(_ISecurityQuestions.GetByUserName(model.Username)).ToList();
            return View(model);
        }


        public JsonResult ResetPassword(LoginViewModel model)
        {
            if (_ISecurityQuestions.IsValidCombination(model.Username, model.SecurityQuestion, model.SecurityQuestionAnswer))
            {
                try
                {
                    Guid tokenGuid = Guid.NewGuid();
                    _IVerificationToken.Save((Guid)Membership.GetUser(model.Username).ProviderUserKey, tokenGuid);

                    var requestUrl = Request.Url.AbsoluteUri;
                    var requestRawUrl = Request.Url.PathAndQuery;
                    var rootUrl = requestUrl.Replace(requestRawUrl, string.Empty);

                    string url = string.Format("{0}/User/UserResetPassword?token={1}&Username={2}", rootUrl, tokenGuid, model.Username);
                    _IEmailSender.OnResetPassword(model.Username, url);

                    return Json(new { status = StatusMessages.SUCCESS, message = StatusMessages.PASSWORD_RESET_SUCCESS }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    MoolahLogManager.LogException(ex);
                    return Json(new { status = StatusMessages.FAILED, message = StatusMessages.SYSTEM_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = StatusMessages.FAILED, message = ErrorMessages.INVALID_SECURITY_QUEST }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ResetSecurityQuestion(LoginViewModel model)
        {
            if (Membership.ValidateUser(model.Username, model.Password))
            {

                try
                {
                    Guid tokenGuid = Guid.NewGuid();
                    _IVerificationToken.Save((Guid)Membership.GetUser(model.Username).ProviderUserKey, tokenGuid);

                    var requestUrl = Request.Url.AbsoluteUri;
                    var requestRawUrl = Request.Url.PathAndQuery;
                    var rootUrl = requestUrl.Replace(requestRawUrl, string.Empty);

                    string url = string.Format("{0}/User/UserResetSecurityQuestions?token={1}&Username={2}", rootUrl, tokenGuid, model.Username);
                    _IEmailSender.OnResetSecurityQuestion(model.Username, url);

                    return Json(new { status = StatusMessages.SUCCESS, message = StatusMessages.SEC_QUEST_RESET_SUCCESS }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    MoolahLogManager.LogException(ex);
                    return Json(new { status = StatusMessages.FAILED, message = StatusMessages.SYSTEM_ERROR }, JsonRequestBehavior.AllowGet);
                }
            }

            return Json(new { status = StatusMessages.FAILED, message = ErrorMessages.INVALID_PASSWORD }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ResetCredentialSuccess(string message)
        {
            ViewBag.message = message;
            return View();
        }

        public ActionResult UserResetPassword(string token, string Username)
        {
            if (_IVerificationToken.IsValidToken(token, true))
            {
                return View();
            }
            else
            {
                return RedirectToAction("Message", new { message = ErrorMessages.TOKEN_EXPIRED });
            }

        }

        [HttpPost]
        public ActionResult UserResetPassword(LoginViewModel model)
        {
            var user = Membership.GetUser(model.Username);
            var password = user.GetPassword(GlobalConstansValues.SEC_QUES_ANSWER);

            if (password == model.Password)
            {
                ViewData["Error"] = ErrorMessages.PASSWORDRESET_SAME;
                return View();
            }

            user.ChangePassword(password, model.Password);
            return RedirectToAction("UserResetPasswordSuccess");
        }

        public ActionResult UserResetPasswordSuccess()
        {
            return View();
        }

        public ActionResult UserResetSecurityQuestions(string token, string Username)
        {
            if (_IVerificationToken.IsValidToken(token, true))
            {
                ViewBag.SecurityQuestions = Mapper.Map<IEnumerable<tbl_SecurityQuestions>, IEnumerable<SelectListItem>>(_ISecurityQuestions.Get()).ToList();
                return View();
            }
            else
            {
                return RedirectToAction("Message", new { message = ErrorMessages.TOKEN_EXPIRED });
            }
        }

        [HttpPost]
        public ActionResult UserResetSecurityQuestions(RegisterViewModel model)
        {
            var user = Membership.GetUser(model.Username);
            _ISecurityQuestions.RemoveAllByUsername(model.Username);
            _ISecurityQuestions.Add(model.Username, model.SecurityQuestion1, model.SecurityQuestionAnswer1);
            _ISecurityQuestions.Add(model.Username, model.SecurityQuestion2, model.SecurityQuestionAnswer2);
            _ISecurityQuestions.Add(model.Username, model.SecurityQuestion3, model.SecurityQuestionAnswer3);

            return RedirectToAction("UserResetSecurityQuestionsSuccess");
        }

        public ActionResult UserResetSecurityQuestionsSuccess()
        {
            return View();
        }


        /// <summary>
        /// Action to be called at join page load
        /// </summary>       
        /// <returns></returns>
        public ActionResult RegisterUser(string forwhat)
        {
            if (HttpContext.User.Identity.IsAuthenticated)
                return RedirectToAction("Index");

            string Role = string.Empty;
            ViewBag.SecurityQuestions = Mapper.Map<IEnumerable<tbl_SecurityQuestions>, IEnumerable<SelectListItem>>(_ISecurityQuestions.Get()).ToList();

            if (forwhat == null)
            {
                return View();
            }
            else
            {
                if (forwhat.ToLower() == "i")
                {
                    Role = "Investor";
                }
                else if (forwhat.ToLower() == "b")
                {
                    Role = "Borrower";
                }
                else
                {
                    Role = "";
                }

                RegisterUserModel objregister = new RegisterUserModel();
                string Userrole = objusercommon.GetUserRole();

                if (Userrole == Role)
                {
                    return RedirectToAction("Create", Role);
                }
                else
                {
                    if (Role != "")
                    {
                        objregister.Userrole = Role;
                        objregister.Isagree = true;
                        objregister.SubscribeforUpdate = true;
                        return View(objregister);
                    }
                    else
                    {
                        return View();
                    }
                }

            }
        }

        /// <summary>
        /// Method for user's registration process
        /// </summary>   
        /// <returns></returns>
        [HttpPost]
        public ActionResult RegisterUser(RegisterUserModel objregister)
        {
            if (ModelState.IsValid)
            {
                string error = string.Empty;
                if (objusercommon.RegisterUser(objregister, out error))
                {
                    /////////create a cooke for user role so that we can check it for dashboard link on layout page, to avoid the error of converting genericidentity(as it allow anonymous user cant access form authenticcation) to formsidentity
                    if (!HttpContext.Response.Cookies.AllKeys.Contains("_UsersRole_"))
                    {
                        HttpCookie cookieUserRole = new HttpCookie("_UsersRole_");
                        Response.Cookies["_UsersRole_"].Value = objregister.Userrole;
                        cookieUserRole.Value = objregister.Userrole;
                        Response.Cookies.Add(cookieUserRole);
                    }

                    return RedirectToAction("PendingEmailVerification");
                }
                else
                {
                    ViewBag.SecurityQuestions = Mapper.Map<IEnumerable<tbl_SecurityQuestions>, IEnumerable<SelectListItem>>(_ISecurityQuestions.Get()).ToList();
                    ModelState.AddModelError("", error);
                    return View();
                }
            }
            else
            {
                ViewBag.SecurityQuestions = Mapper.Map<IEnumerable<tbl_SecurityQuestions>, IEnumerable<SelectListItem>>(_ISecurityQuestions.Get()).ToList();
                ModelState.AddModelError("", "Some validation error occured. Please check.");
                return View();
            }
        }

        public ActionResult PendingEmailVerification()
        {
            return View();
        }


        public ActionResult Information()
        {
            return View();
        }

        /// <summary>
        /// Method for checking email id availability in registeres users list
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        //public JsonResult CheckEmailavailability(string EmailAddress)
        //{
        //    return objusercommon.GetCurrentUserEmail() == EmailAddress
        //               ? Json(true)
        //               : Json(Membership.GetUser(EmailAddress) == null ? true : false);
        //}

        public JsonResult CheckEmailavailability(string EmailAddress, long? UseriD = null)
        {

            if (UseriD.HasValue)
            {
                return Json(objusercommon.IsValidEmail(EmailAddress, UseriD.Value));
            }

            return objusercommon.GetCurrentUserEmail() == EmailAddress
                       ? Json(true)
                       : Json(Membership.GetUser(EmailAddress) == null ? true : false);
        }

        /// <summary>
        /// Method for checking email id and password text are same or not  while Join process
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        public JsonResult CheckEmailPasswordMatchcase(string Password, string EmailAddress)
        {
            string message = string.Empty;
            if (EmailAddress == Password)
            {
                message = "";
            }
            else
            {
                message = null;
            }
            return Json(message == null);
        }

        /// <summary>
        /// Method for logout functionality
        /// </summary>
        /// <returns></returns>
        public ActionResult LogOff()
        {
            MoolahMemberhipProvider.MakeLogoff();
            return Redirect(Settings.HomePageUrl);
        }

        public ActionResult IdleLogOff()
        {

            MoolahMemberhipProvider.MakeLogoff();
            return RedirectToAction("Login", new { message = ErrorMessages.IDLE_LOGOFF });
        }

        public ActionResult LoggoffWithUsernameChange()
        {
            MoolahMemberhipProvider.MakeLogoff();
            return RedirectToAction("Message", "user",
                                    new
                                        {
                                            message =
                                        "You have successfully changed the email address. We have sent you an email to verify your new email address. Please check."
                                        });
        }

        public ActionResult EmailVerification(string token, string email)
        {

            if (_IVerificationToken.IsValidToken(token, false))
            {
                _IUser.UpdateEmailVerification(email, true);
                return View();
            }
            else
            {
                return RedirectToAction("Message", new { message = ErrorMessages.TOKEN_EXPIRED });
            }
        }

        public ActionResult Message(string message)
        {
            ViewBag.Message = message;
            return View();
        }

        /// <summary>
        /// /////////////////////////////////Login action for admin//////////
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [AllowAnonymous]
        [RecaptchaControlMvc.CaptchaValidator]
        public ActionResult AdminLogin(AdminLogin objAdminLogin, bool captchaValid, string captchaErrorMessage)
        {
            if (ModelState.IsValid && captchaValid)
            {
                string IsAdmin = objusercommon.AdminLogin(objAdminLogin);
                if (IsAdmin == "Admin")
                    // RedirectToAction("Index", "Admin");
                    return RedirectToActionPermanent("Index", "Admin");
                else
                    return RedirectToAction("angchungyong", "User");
            }
            else
            {
                return RedirectToAction("angchungyong", "User");
            }

        }




        /// <summary>
        /// //////////////////////////////// Redirect to admin login page
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        public ActionResult angchungyong()
        {
            return View("AdminLogin");
        }

        [AllowAnonymous]
        public ActionResult InvestorBoard()
        {
            return View();
        }

        [AllowAnonymous]
        public ActionResult FAQ()
        {
            return View();
        }

    }
}