﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;

namespace MoolahConnectnew.Controllers.Helpers
{
    public static class RegistrationHelper
    {
        public static bool HasAccountDetailsExist(out string error)
        {
            try
            {
                IUser _IUser = new User();

                error = string.Empty;

                var memUser = Membership.GetUser();

                if (memUser == null)
                {
                    error = ErrorMessages.UNEXPECTED_ERROR;
                    return false;
                }

                var user = _IUser.GetUserFromAspNetId((Guid)memUser.ProviderUserKey);

                if (user == null)
                {
                    error = ErrorMessages.UNEXPECTED_ERROR;
                    return false;
                }

                if (_IUser.GetAccountDetailsByID(user.UserID) == null)
                {
                    error = ErrorMessages.REG_ACCOUNTDETAILS_MISSING_ERROR;
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                error = string.Empty;
                return false;
            }
        }
    }
}