﻿using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.FinancialCalculations;
using MoolahConnectnew.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace MoolahConnectnew.Controllers.Helpers
{
    public static class AdvanceMappingHelper
    {
        public static IEnumerable<LoanRepaymentViewModel> MapLateLoanRepayments(IEnumerable<LoanRepaymentEntity> loanRepayEntities)
        {
            ITransactionsDA _ITranDA = new TransactionsDA();
            var loanRepayModels = new List<LoanRepaymentViewModel>();

            //loanRepayEntities = loanRepayEntities.OrderBy(x => x.RepaymentDate).OrderBy(x => x.RequestId);

            //long currentRequestId = 0;
            double lastInterest = 0;
            double lateFees = 0;
            double totalOut = 0;
            int lateDays = 0;
            DateTime sinNowDate = DateTimeExtenstion.SingaporeTime(DateTime.Now).Date;

            foreach (LoanRepaymentEntity entity in loanRepayEntities)
            {
                lastInterest = 0;
                lateFees = 0;
                lateDays = 0;
                totalOut = 0;

                var dueDate = DateTimeExtenstion.SingaporeTime(entity.RepaymentDate.Value).AddDays(1).Date;
                if (entity.LateRepaymentPaidDate.HasValue)
                {
                    lastInterest = entity.LateInterest.HasValue ? (double)entity.LateInterest : 0;
                    lateFees = entity.LateFees.HasValue ? (double)entity.LateFees.Value : 0;

                    var latePayementPaidSinDate = DateTimeExtenstion.SingaporeTime(entity.LateRepaymentPaidDate.Value).Date;
                    lateDays = latePayementPaidSinDate.Subtract(dueDate).Days + 1;
                }
                else if (entity.Status == RepaymentPayStatus.Late ||
                    (entity.Status == RepaymentPayStatus.Paid && entity.LateRepaymentPayStatus != (int)RepaymentPayStatus.Paid))
                {
                    double[] accruedInterestAndLateFees = _ITranDA.GetAccruedInterestAndLateFees(entity.RepaymentId);
                    lastInterest = accruedInterestAndLateFees[0];
                    lateFees = accruedInterestAndLateFees[1];
                    lateDays = sinNowDate.Subtract(dueDate).Days + 1;
                    totalOut = accruedInterestAndLateFees[2];
                    
                }

                lateDays = lateDays < 0 ? 0 : lateDays;

                loanRepayModels.Add(new LoanRepaymentViewModel()
                {
                    AccountNumber = String.Format("BRW{0}", entity.AccountNumber),
                    AccuredInterest = lastInterest,
                    Amount = entity.Amount.HasValue ? (double)entity.Amount.Value : 0,
                    Comment = entity.Comment,
                    CompanyName = entity.CompanyName,
                    EndDate = GetMaturityDate(entity.AcceptedDate, entity.Terms),
                    LastUnpaidDueDate = entity.RepaymentDate.HasValue ? entity.RepaymentDate.Value : DateTime.MinValue,
                    LoanId = entity.RequestId,
                    MonthlyRepayment = entity.Repayment.HasValue ? (double)entity.Repayment.Value : 0,
                    MonthsTillMaturity = MonthsTillMaturity(entity.RequestId),
                    Rate = entity.Rate.HasValue ? (double)entity.Rate.Value : 0,
                    RepaymentDate = entity.RepaymentDate.HasValue ? entity.RepaymentDate.Value : DateTime.MinValue,
                    RepaymentId = entity.RepaymentId,
                    StartDate = entity.AcceptedDate.HasValue ? entity.AcceptedDate.Value : DateTime.MinValue,
                    Status = entity.Status.ToString(),

                    AccuredInterestPaidDate = entity.LateRepaymentPaidDate,
                    AccuredInterestPaid = (RepaymentPayStatus)entity.LateRepaymentPayStatus == RepaymentPayStatus.Paid ? true : false,
                    LateDaysCount = lateDays,
                    TotalOutstandingRepayments = totalOut,

                    Paidamount = entity.PaidAmount.HasValue ? (double)entity.PaidAmount.Value : 0,
                    LateFees = lateFees,
                    LateFeesPaid = entity.LateFees.HasValue ? true : false
                });
                //currentRequestId = entity.RequestId;
            }

            return loanRepayModels;
        }

        private static DateTime GetMaturityDate(DateTime? approvedDate, string terms)
        {
            if (!approvedDate.HasValue)
                return DateTime.MinValue;

            int termsInMonths = 0;
            int.TryParse(terms, out termsInMonths);

            return approvedDate.Value.AddMonths(termsInMonths);
        }

        private static int MonthsTillMaturity(long requestId)
        {
            ILoanDetailsDA loan = new LoanDetailsDA();
            return loan.MonthsTillMaturity(requestId);
        }
    }
}