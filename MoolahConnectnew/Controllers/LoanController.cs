﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MoolahConnectClassLibrary.DAL;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectnew.Filters;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Entities;
using MoolahConnectnew.ViewModels;
using AutoMapper;
using System.Web.Security;
using MoolahConnect.Util.SealedClasses;
using MoolahConnectnew.Providers;
using MoolahConnect.Util.Logging;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;

namespace MoolahConnectnew.Controllers
{
    [Authorize]
    public class LoanController : Controller
    {
        InvestorOperations objInvestorOperations;
        IInvestorLoanSummaryDA _IInvestorLoanSummaryDA;
        ILoanDetailsDA _ILoanDetailsDA;
        ILoanAllocationDA _ILoanAllocationDA;
        private readonly IAccountDA _accountDa;
        private IFileUploadTask _IFileUploadTask;
        private IFileUploadDA _IFileUploadDA;
        private ILoanPaymentsTask _ILoanPayments;

        public LoanController()
        {
            objInvestorOperations = new InvestorOperations();
            _IInvestorLoanSummaryDA = new InvestorLoanSummaryDA();
            _ILoanDetailsDA = new LoanDetailsDA();
            _ILoanAllocationDA = new LoanAllocationDA();
            _accountDa = new AccountDA();
            _IFileUploadTask = new FileUploadTask();
            _IFileUploadDA = new FileUploadDA();
            _ILoanPayments = new LoanPaymentsTask();
        }

        public JsonResult SaveLoanOffer(string Amount, string Rate, string Password, string LoanRequest, string Term)
        {
            string message = string.Empty;

            if (Membership.ValidateUser(HttpContext.User.Identity.Name, Password))
            {
                MoolahLogManager.Log(string.Format("Save Offer : {0}", StatusMessages.SUCCESSFULL_PASSWORD_VALIDATION), LogType.Warn, HttpContext.User.Identity.Name);
                message = objInvestorOperations.SaveLoanOffer(Amount, Rate, LoanRequest, Term);
            }
            else if (Membership.GetUser(HttpContext.User.Identity.Name).IsLockedOut)
            {
                MoolahLogManager.Log(string.Format("Save Offer : {0}", ErrorMessages.INVALID_PASSWORD), LogType.Warn, HttpContext.User.Identity.Name);
                MoolahLogManager.Log(string.Format("Save Offer : {0}", ErrorMessages.ACCOUNT_LOCKED), LogType.Warn, HttpContext.User.Identity.Name);
                MoolahMemberhipProvider.MakeLogoff();
                message = StatusMessages.ACCOUNT_LOCKED;
            }
            else
            {
                MoolahLogManager.Log(string.Format("Save Offer : {0}", ErrorMessages.INVALID_PASSWORD), LogType.Warn, HttpContext.User.Identity.Name);
                message = "The password is incorrect.";
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMoolahCoreIndicatorData(int requestId)
        {
            var model = new MoolahCoreViewModel();
            var modelVerify = new MoolahCoreVerifyViewModel();
            var mcIndicators = _IInvestorLoanSummaryDA.GetLoanRequestMoolahCoreForLoanRequest(requestId);
            var mcIndicatorsVerifications = _IInvestorLoanSummaryDA.GetMoolahCoreVerificationForLoanRequest(requestId);

            if (mcIndicators != null)
                model = Mapper.Map<tbl_MoolahCore, MoolahCoreViewModel>(mcIndicators);

            if (mcIndicatorsVerifications != null)
                modelVerify = Mapper.Map<tbl_MoolahCoreVerification, MoolahCoreVerifyViewModel>(mcIndicatorsVerifications);


            var userAccount = _IInvestorLoanSummaryDA.GetAccountDetailsFromRequestId(requestId);
            if (userAccount != null)
                model.MoreThanOTEqual2YearsInBusiness = userAccount.DateofIncorporation.HasValue &&
                    DateTime.UtcNow.Subtract(userAccount.DateofIncorporation.Value).Days >= 365 * 2 ? true : false;

            return Json(new
            {
                status = "success",
                data = model == null ? new MoolahCoreViewModel() : model,
                dataVerify = modelVerify == null ? new MoolahCoreVerifyViewModel() : modelVerify,
            }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMoolahPeriIndicatorData(int requestId)
        {
            var model = new MoolahPeriViewModel();
            var modelVerify = new MoolahPeriVerifyViewModel();

            var mpIndicators = _IInvestorLoanSummaryDA.GetMoolahPeriForLoanRequest(requestId);
            var mpIndicatirsVerifications = _IInvestorLoanSummaryDA.GetMoolahPeriVerificationForLoanRequest(requestId);

            if (mpIndicators != null)
                model = Mapper.Map<tbl_MoolahPeri, MoolahPeriViewModel>(mpIndicators);

            if (mpIndicatirsVerifications != null)
                modelVerify = Mapper.Map<tbl_MoolahPeriVerification, MoolahPeriVerifyViewModel>(mpIndicatirsVerifications);

            return Json(new
            {
                status = "success",
                data = model == null ? new MoolahPeriViewModel() : model,
                dataVerify = modelVerify == null ? new MoolahPeriVerifyViewModel() : modelVerify
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult MatchedLoanDetailsAsPreview(long requestId)
        {
            if (requestId == -999)
            {
                return Json("Session has been expired", JsonRequestBehavior.AllowGet);
            }

            var obj = GetLoanDetails(requestId, true);
            obj.loanSummary.IsPreview = true;
            ViewBag.IsBorrower = true;
            return View("MatchedLoanDetails", obj);
        }

        public ActionResult BorrowerLoanDetails(long noteRequestID)
        {
            var obj = GetLoanDetails(noteRequestID, false);
            ViewBag.IsBorrower = true;
            return View("MatchedLoanDetails", obj);
        }

        public ActionResult MatchedLoanDetails(long noteRequestID)
        {
            var obj = GetLoanDetails(noteRequestID, false);
            obj.loanSummary.IsPreview = false;
            ViewBag.IsBorrower = false;
            return View("MatchedLoanDetails", obj);
        }

        private LoanDetailsViewModel GetLoanDetails(long LoanRequestID, bool isPreview)
        {
            try
            {
                var loanDetails = new LoanDetailsViewModel();

                ViewBag.LoanRequestId = LoanRequestID;
                loanDetails.loanSummary = isPreview ? objInvestorOperations.GetLoanandBorrowerBriefDetailForPreviewRequestID(LoanRequestID)
                    : objInvestorOperations.GetLoanandBorrowerBriefDetailRequestID(LoanRequestID);

                if (loanDetails.loanSummary != null)
                {
                    var user = Membership.GetUser(loanDetails.loanSummary.AccountDetails.EmailAddress == null ? string.Empty :
                        loanDetails.loanSummary.AccountDetails.EmailAddress);
                    if (user != null)
                    {
                        var file = _IFileUploadDA.GetCoverPhotoFile((Guid)user.ProviderUserKey, LoanRequestID);

                        if (file != null)
                        {
                            loanDetails.loanSummary.LogoImage = _IFileUploadTask.GetFileUrl(user.ProviderUserKey.ToString(),
                                FileType.LoanCover.ToString(),
                                file.FileName,
                                LoanRequestID.ToString());
                            loanDetails.loanSummary.LogoImageName = file.Comments;
                        }
                    }
                }

                var _loanDetails = _ILoanDetailsDA.GetLoanRequestFromId(LoanRequestID);
                if (_loanDetails != null)
                    loanDetails.Loan = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(_loanDetails);

                if (loanDetails.Loan != null)
                {
                    var _accountDetails = _ILoanDetailsDA.GetAccountDetailsFromUserId(loanDetails.Loan.UserId);

                    if (_accountDetails != null)
                        loanDetails.Account = Mapper.Map<tbl_AccountDetails, AccountViewModel>(_accountDetails);
                }

                var _moolahCore = _IInvestorLoanSummaryDA.GetLoanRequestMoolahCoreForLoanRequest(LoanRequestID);
                if (_moolahCore != null)
                {
                    loanDetails.MoolahCore.MoolahCore = Mapper.Map<tbl_MoolahCore, MoolahCoreViewModel>(_moolahCore);

                    if (_moolahCore.NetprofitafterTax_LY.HasValue)
                    {
                        loanDetails.MoolahCore.MoolahCore.Profitability = _moolahCore.NetprofitafterTax_LY;
                    }
                    else
                    {
                        loanDetails.MoolahCore.MoolahCore.Profitability = null;
                    }

                    loanDetails.MoolahCore.MoolahCore.ProfitabilityPy = _moolahCore.NetprofitafterTax_PY;
                }

                var _moolahCoreVerification = _IInvestorLoanSummaryDA.GetMoolahCoreVerificationForLoanRequest(LoanRequestID);
                if (_moolahCoreVerification != null)
                    loanDetails.MoolahCore.MoolahCoreVerify = Mapper.Map<tbl_MoolahCoreVerification, MoolahCoreVerifyViewModel>
                        (_moolahCoreVerification);

                var _moolahPeri = _IInvestorLoanSummaryDA.GetMoolahPeriForLoanRequest(LoanRequestID);
                if (_moolahPeri != null)
                {
                    loanDetails.MoolahPeri.MoolahPeri = Mapper.Map<tbl_MoolahPeri, MoolahPeriViewModel>(_moolahPeri);

                    if (string.IsNullOrEmpty(loanDetails.MoolahPeri.MoolahPeri.MoolahPerk))
                        loanDetails.MoolahPeri.MoolahPeri.MoolahPerk = _ILoanDetailsDA.GetMoolahPerkFromId(loanDetails.MoolahPeri.MoolahPeri.MoolahPerkId);
                }

                var _moolahPeriVerificaion = _IInvestorLoanSummaryDA.GetMoolahPeriVerificationForLoanRequest(LoanRequestID);
                if (_moolahPeriVerificaion != null)
                    loanDetails.MoolahPeri.MoolahPeriVerify = Mapper.Map<tbl_MoolahPeriVerification, MoolahPeriVerifyViewModel>(_moolahPeriVerificaion);

                var l = GetWatchlist();

                if (l != null)
                {
                    ViewBag.Addable = !l.Contains(LoanRequestID);
                }

                loanDetails.Account.DetailedCompanyProfile = loanDetails.Loan.DetailedCompanyProfile;

                var model = new LoanDocumentsViewModel();

                var files = Mapper.Map<IEnumerable<tbl_Files>, IEnumerable<FileModel>>(
                    _IFileUploadDA.GetFilesByRequestId(LoanRequestID));

                model.Videos = files.Where(x => x.Type == FileType.LoanVideo).ToList();
                model.Documents = files.Where(x => x.Type == FileType.LoanDocuments).ToList();
                model.YouTube = files.Where(x => x.Type == FileType.YouTubeLink).ToList();
                model.PhotoUrls = files.Where(x => x.Type == FileType.LoanPhoto).ToList();

                loanDetails.LoanDocuments = model;

                return loanDetails;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return new LoanDetailsViewModel();
            }
        }

        private IEnumerable<long> GetWatchlist()
        {
            var u = _accountDa.GetUser(User.Identity.Name);

            if (u == null)
            {
                return null;
            }

            return _ILoanAllocationDA.GetWatchList(u.UserID).Select(e => e.RequestId);
        }

        public PartialViewResult _DocumentsViewer(LoanDocumentsViewModel model)
        {
            return PartialView(model);
        }

        /// <summary>
        /// Get the last admin update timestamp
        /// </summary>
        /// <param name="cat">
        /// 1. Summary
        /// 2. Profile
        /// 3. Description
        /// 4. Moolah Core
        /// 5. Moolah Peri
        /// </param>
        /// <param name="reqId"></param>
        /// <returns>Timestamp of last update</returns>
        public JsonResult GetLastUpdateTimeStamp(int cat, int reqId)
        {
            IAuditLogDA da = new AuditLogDA();
            var lastUpdateTimeStamp = da.GetLastUpdateTimeStamp(cat, reqId);
            if (lastUpdateTimeStamp != null)
            {
                var timeStamp = (DateTime)lastUpdateTimeStamp;
                return Json(timeStamp.ToDateTime());
            }
            return Json("NA");
        }

        public PartialViewResult PhotoViewer(List<string> urls)
        {
            return PartialView(urls);
        }

        public PartialViewResult YouTubeVideoViewer(string url, string name)
        {
            url = url.Replace("watch?v=", "embed/");
            url = url.Replace("youtu.be", "www.youtube.com/embed/");

            var model = new FileModel();

            model.Comments = name == null ? string.Empty : name;
            model.Url = url;

            return PartialView(model);
        }

        public PartialViewResult VideoViewer(string url, string name)
        {
            var model = new FileModel();

            model.Comments = name == null ? string.Empty : name;
            model.Url = url;

            return PartialView(model);
        }

        public PartialViewResult _RepaymentsDetailsForNewOffer(double amount, double rate, string tenor)
        {
            ViewBag.Amount = amount;
            ViewBag.Rate = rate;
            ViewBag.Tenor = tenor;

            var repayments = Mapper.Map<IEnumerable<RepaymentEntity>, IEnumerable<RepaymentViewModel>>(
                _ILoanPayments.GetRepaymentSchedule(null, amount, rate, tenor, DateTime.Today)).ToList();

            return PartialView(repayments);
        }
    }
}
