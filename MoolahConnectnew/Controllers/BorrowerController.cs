﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectnew.AutoMapConfiguration;
using MoolahConnectnew.Filters;
using MoolahConnectClassLibrary.DAL;
using MvcPaging;
using System.Web.UI;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.FinancialCalculations;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnectnew.ViewModels.Borrower;
using AutoMapper;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Util.Enums;
using MoolahConnectnew.ViewModels;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;
using System.Web.Security;
using System.Threading;
using MoolahConnectnew.Providers;
using MoolahConnect.Util;
using MoolahConnectnew.Controllers.Helpers;
using WebGrease;
using LoanRequest = MoolahConnectClassLibrary.Models.ViewModels.LoanRequest;


namespace MoolahConnectnew.Controllers
{

    [authorize.AuthorizeRole(Roles = "Borrower")]

    public class BorrowerController : Controller
    {
        public static List<OutstandingLitigation> obj = new List<OutstandingLitigation>();
        public static List<int> ListofIndex = new List<int>();  ////////////////////This collection is use for deleting doucment while editing loan request it will contain doucment id

        //private BorrowerOperations objborroweropr;
        private ILoanAllocationTask _ILoanAllocationTask;
        private readonly IIssuerService _issuerService;
        private readonly IPersonGuarantorService _iPersonGuaranteeService;
        private readonly IMoolahCoreService _imMoolahCoreService;
        private readonly IMoolahPeriService _imoolahPeriService;
        private readonly IOutStandingLitigationService _iOutStandingLitigationService;
        private readonly IAccountDetailService _iAccountDetailService;
        private readonly ICurrentUser _iCurrentUser;
        private readonly ILoanPurposeService _loanPurposeService;
        private readonly IMoolahPerkLenderService _moolahPerkLenderService;
        private readonly ISessionService _sessionService;
        private readonly IUnitOfWork _unitOfWork;
        private readonly ILoanRequestService _loanRequestService;
        private ILoanAllocationDA _ILoanAllocationDA;
        private IBorrowerLoanDetailsDA _IBorrowerLoanDetailsDA;
        private ITransactionsDA _ITransactionsDA;
        private IAuditLogDA _IAuditLogDA;
        private IFileUploadDA _IFileUploadDA;
        private ILoanPaymentsTask _ILoanPaymentsTask;

        private UserCommonOperations objusercommon;


        public BorrowerController(ILoanAllocationTask loaLoanAllocationTask, IIssuerService issuerService,
            IPersonGuarantorService iPersonGuaranteeService, IMoolahCoreService imMoolahCoreService,
            IMoolahPeriService imoolahPeriService, IOutStandingLitigationService iOutStandingLitigationService,
            IAccountDetailService iAccountDetailService, ICurrentUser iCurrentUser, ILoanPurposeService loanPurposeService,
            IMoolahPerkLenderService moolahPerkLenderService, ISessionService sessionService, IUnitOfWork unitOfWork, ILoanRequestService loanRequestService)
        {
           // objborroweropr = new BorrowerOperations();
            _ILoanAllocationTask = loaLoanAllocationTask; // new LoanAllocationTask();
            _issuerService = issuerService;
            _iPersonGuaranteeService = iPersonGuaranteeService;
            _imMoolahCoreService = imMoolahCoreService;
            _imoolahPeriService = imoolahPeriService;
            _iOutStandingLitigationService = iOutStandingLitigationService;
            _iAccountDetailService = iAccountDetailService;
            _iCurrentUser = iCurrentUser;
            _loanPurposeService = loanPurposeService;
            _moolahPerkLenderService = moolahPerkLenderService;
            _sessionService = sessionService;
            _unitOfWork = unitOfWork;
            _loanRequestService = loanRequestService;
            _IBorrowerLoanDetailsDA = new BorrowerLoanDetailsDA();
            _ITransactionsDA = new TransactionsDA();
            _IAuditLogDA = new AuditLogDA();
            _IFileUploadDA = new FileUploadDA();
            _ILoanPaymentsTask = new LoanPaymentsTask();
            objusercommon = new UserCommonOperations();
            _ILoanAllocationDA = new LoanAllocationDA();
        }

        /// <summary>
        /// Method for Index load
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (User.Identity.Name != null)
            {
                var result = _issuerService.BorrowerIndexLoad();

                if (result == "SubmittedBorrower")
                {
                    return RedirectToAction("ConfirmRegistration", "Issuer");
                }
                if (result == "Borrower")
                {
                    return RedirectToAction("Create", "Issuer");
                }
                if (result == "User")
                {
                    return RedirectToAction("Index", "User");
                }

                #region Borrower Dashboard

                var outstandingLoans = new List<LoanViewModel>();
                var completedLoans = new List<LoanViewModel>();
                var outstandingPayments = new List<PaymentViewModel>();
                var loanDraft = new LoanRequestViewModel();

                loanDraft = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(
                _IBorrowerLoanDetailsDA.GetLoanDraftByUser(User.Identity.Name));

                outstandingLoans = Mapper.Map<IEnumerable<LoanEntity>, IEnumerable<LoanViewModel>>(
                _IBorrowerLoanDetailsDA.GetLoansByUser(User.Identity.Name,
                                                           new List<LoanRequestStatus>() { LoanRequestStatus.Matured, LoanRequestStatus.Matched, LoanRequestStatus.FundApproved, LoanRequestStatus.FundRejected })).ToList();
                            decimal osl = outstandingLoans.Sum(outstandingLoan => _IBorrowerLoanDetailsDA.OutstandingLoanAmount(outstandingLoan.RequestId) ?? 0);

                BorrowerDashboard objDashboard = GetBorrowerDashboard(osl, outstandingLoans.Count);
                ViewBag.LoanDraft = loanDraft;
                ViewBag.OutStandingLoans = outstandingLoans
                    .Where(a => a.Status != LoanRequestStatus.Matured).ToList();
                ViewBag.CompletedLoans = outstandingLoans.Where(a => a.Status == LoanRequestStatus.Matured).ToList();
               

                #endregion

                return View(objDashboard);
                //return RedirectToAction("Index", "User");
            }
            else
            {
                return RedirectToAction("Index", "User");
            }

        }

        public ActionResult GetBoardView()
        {
            var outstandingLoans = Mapper.Map<IEnumerable<LoanEntity>, IEnumerable<LoanViewModel>>(
                    _IBorrowerLoanDetailsDA.GetLoansByUser(User.Identity.Name,
                                                           new List<LoanRequestStatus>() { LoanRequestStatus.Matured, LoanRequestStatus.Matched, LoanRequestStatus.FundApproved, LoanRequestStatus.FundRejected })).ToList();
             decimal osl = outstandingLoans.Sum(outstandingLoan => _IBorrowerLoanDetailsDA.OutstandingLoanAmount(outstandingLoan.RequestId) ?? 0);
            BorrowerDashboard objDashboard = GetBorrowerDashboard(osl, outstandingLoans.Count);
              ViewBag.OutStandingLoans = outstandingLoans.Where(a => a.Status != LoanRequestStatus.Matured).ToList();

              ViewBag.CompletedLoans = outstandingLoans.Where(a => a.Status == LoanRequestStatus.Matured).ToList();
              return PartialView("_Dashboard", objDashboard.Dashboard);
        }

        public ActionResult GetCompanyProfileView()
        {
                    return PartialView("_CompanyProfile", Automapper.MapCompanyProfile(_issuerService.GetCompayProfile()));
        }
        public ActionResult GetRepaymentView()  
        {
            return PartialView("_LoanRepayment");
        }
        private BorrowerDashboard GetBorrowerDashboard(decimal totaloutStanding, int outStandingCount)
        {
           // BorrowerDashboard objDashboard = Automapper.MapBorrowerDashboard(_issuerService.FillBorrowerDashboard());
            var objBorrowerDashboard = new BorrowerDashboard();
            var objDashboard = new Dashboard();
            var accountDetails = _iAccountDetailService.GetByUser(_iCurrentUser.ApplicationUser.UserID);

            var objCommon = new Common();
            objCommon.AccountName = accountDetails.AccountName;
            objCommon.BusinessName = accountDetails.BusinessName;
            objCommon.AccountNumber = CommonMethods.GetAccountNumber(_iCurrentUser.ApplicationUser.UserID, _iCurrentUser.ApplicationUser.UserRole);
            objBorrowerDashboard.CommonOperations = objCommon;

            var objtblLoanRequests = _issuerService.GetLoanRequestbyUser().Where(a => a.LoanStatus != (int)LoanRequestStatus.InProgress);
           
            if (objtblLoanRequests != null)
            {
                var objLoanRequestList = new List<LoanRequestDashboard>();

                var loanLIst =
               _issuerService.GetLoanRequestbyUser()
                   .Where(
                       a =>
                           a.IsApproved.HasValue && a.IsApproved.Value &&
                           (a.LoanStatus == (int)LoanRequestStatus.Pending
                            || a.LoanStatus == (int)LoanRequestStatus.InProgress
                            || a.LoanStatus == (int)LoanRequestStatus.Cancelled)
                           && a.PreliminaryLoanStatus > 1);
                foreach (var item in loanLIst)
                {
                    var objDashboardProps = new LoanRequestDashboard();
                    objDashboardProps.Reference = Convert.ToString(item.RequestId);
                    objDashboardProps.Amount = item.Amount;
                    objDashboardProps.LoanRequestID = item.RequestId;
                    objDashboardProps.Purpose = item.LoanPurposes.PurposeName;
                    objDashboardProps.Tenor = item.Terms;
                    objDashboardProps.PaymentGrade = "Fair static"; ///its static for now it will be set by admin
                    objDashboardProps.Rate = Convert.ToDecimal(item.Rate);
                    objDashboardProps.OfferStatus = item.LoanStatus.ToLoanStatusDisplayName();
                    objDashboardProps.TotalInterestPayable = CommonMethods.GetTotalInterestPayable(item.Terms,
                        item.Amount.HasValue ? (double)item.Amount : 0, (double)item.Rate);
                    objDashboardProps.LoanStatus = item.LoanStatus;
                    objDashboardProps.PreliminaryStatus = item.PreliminaryLoanStatus;
                    objDashboardProps.Status =
                        CommonMethods.GetLoanStatus((PreliminaryLoanRequestStatus)item.PreliminaryLoanStatus,
                            (LoanRequestStatus)item.LoanStatus);
                    decimal?[] arr = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(item.RequestId,
                        item.Amount);
                    objDashboardProps.AvgRate = Math.Round(Convert.ToDecimal(arr[1]), 2);
                    objDashboardProps.AvgFunded = Math.Round(Convert.ToDecimal(arr[0]), 2);
                    objDashboardProps.AcceptedDate = item.ApproveDate.HasValue
                        ? (DateTime)item.ApproveDate
                        : DateTime.MinValue;
                    objLoanRequestList.Add(objDashboardProps);
                }
                objDashboard.LoanRequestList = objLoanRequestList;

            }
            objBorrowerDashboard.Dashboard = objDashboard;
            //var loanlist =
            //    _issuerService.GetLoanRequestbyUser().Where(a => a.LoanStatus == (int)LoanRequestStatus.Matched);
            var objGraphList = new List<string>();

            objBorrowerDashboard.Dashboard.GraphOutstandingLoan = objGraphList;
            //DateTime? MinDate = loanlist.FirstOrDefault().LoanAmortizations.FirstOrDefault().EmiDate;
            //var MaxDate = loanlist.FirstOrDefault().LoanAmortizations.OrderByDescending(a => a.EmiDate).FirstOrDefault().EmiDate;
            

            if (objBorrowerDashboard.Dashboard != null)
            {
                objBorrowerDashboard.Dashboard.TotalOutstandingLoan = (decimal?)totaloutStanding;
                objBorrowerDashboard.Dashboard.NumberofOutstandingLoan = outStandingCount;
                objBorrowerDashboard.Dashboard.TotalNumberofUniqueInvestor =
                    _IBorrowerLoanDetailsDA.GetTotalUniqueInvestor(User.Identity.Name);
                var tml = _IBorrowerLoanDetailsDA.GetTotalMatchedAndMaturedLoan(User.Identity.Name);
                if (objBorrowerDashboard.Dashboard.TotalNumberofUniqueInvestor > 0)
                    objBorrowerDashboard.Dashboard.AverageAmount = tml / objBorrowerDashboard.Dashboard.TotalNumberofUniqueInvestor;
                objBorrowerDashboard.Dashboard.HighestAmount =
                    _IBorrowerLoanDetailsDA.GetMaxLoanInvestmentAmount(User.Identity.Name);
            }
            else
                objDashboard = null;
            return objBorrowerDashboard;

        }
        public JsonResult IsLoanAcceptable(long requestId)
        {
            var statusTemp = string.Empty;
            var statusCategoryTemp = string.Empty;

            //This value is only to call the mthod. This value may or may not assigned.
            decimal finalRate = 0;

            var _status = _ILoanAllocationTask.Allocate(requestId, out statusTemp, out statusCategoryTemp, out finalRate, true);

            return Json(new
            {
                status = _status ? StatusMessages.SUCCESS : StatusMessages.FAILED,
                statusMessage = statusTemp,
                finalRate = finalRate
            }, JsonRequestBehavior.AllowGet);
        }

        public PartialViewResult _TermsheetPartial(long requestId, double finalRate)
        {
            var obj = Automapper.MapTermsheetViewModel(_issuerService.GetTermsheetDetails(requestId));

            obj.RepaymentAmount = _ILoanPaymentsTask.GetMonthlyInstallment((double)obj.PrincipalAmount,
                                                                           finalRate, obj.Tenure);
            ViewBag.Repaymemts = Mapper.Map<IEnumerable<RepaymentEntity>, IEnumerable<RepaymentViewModel>>(
                _ILoanPaymentsTask.GetRepaymentSchedule(requestId, (double)obj.PrincipalAmount, finalRate,
                                                        obj.Tenure, DateTime.Now)).ToList();
            obj.AcceptedRate = (decimal)finalRate;
            obj.EffectiveRate = (decimal)LoanRequestCalc.CalculateTargetEIR((double)obj.PrincipalAmount, obj.Tenure, finalRate);
            obj.TotalInterest = CommonMethods.GetTotalInterestPayable(obj.Tenure, (double)obj.PrincipalAmount,
                                                                      finalRate);

            return PartialView("_TermsheetPartial", obj);
        }

        /// <summary>
        /// Method for getting details for registrationform PDF
        /// </summary>
        /// <returns></returns>
        public ActionResult GeneratePDFforRegistrationform()
        {
            BorrowerAccount objborrower = Automapper.MapBorrowerAccount(_issuerService.GetmodelforgeneratePdf());
            ControllerPDF.RenderPartialViewToString(this, "GeneratePDFforRegistrationform", objborrower, "RegistrationForm");
            return View();

        }

        /// <summary>
        /// Method for getting details of user logged in and displaying form for saving account details of user
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            var objborrower = Automapper.MapBorrowerAccount(_issuerService.CreateLoad());
            return View(objborrower);
        }


        //
        // POST: /Borrower/Create

        [HttpPost]
        public ActionResult Create(BorrowerAccountDetailsModel objborrower)
        {
            var result = _issuerService.CreateBorrower(Automapper.MapAccountDetail(objborrower));
            if (result == "Success")
            {
                var proxy = new UserCommonOperations();
                var curEmail = proxy.GetCurrentUserEmail();
                if (curEmail != objborrower.EmailAddress)
                {
                    // Update email process
                    string errorMsg;
                    if (proxy.UpdateUserEmail(objborrower.EmailAddress, out errorMsg))
                    {
                        return RedirectToAction("LoggoffWithUsernameChange", "User");
                    }
                    return
                        RedirectToAction("Create", "Issuer")
                            .WithFlash(new { notice = errorMsg });
                }
                return RedirectToAction("Create", "Issuer");

            }
            return
                RedirectToAction("Create", "Issuer")
                    .WithFlash(new { notice = "Some error occured while saving details" });
        }

        /// <summary>
        /// Method for binding details while Loan request is to be made
        /// </summary>
        /// <returns></returns>
       
        /// <summary>
        /// Method for saving loan request basic details 
        /// </summary>       
        /// <returns></returns>
        //public JsonResult SaveLoanRequestdescription(decimal Amount, string Terms, decimal Rate, bool Ispersonalguarantee, int LoanpurposeID, string XMLquestionswithanswers, string Loanimages, string LoanDocs,
        //     string NameasinNRICPassport, string Designation, string NRICPassport, string Reasons, string Residentialaddress, string Telephone, string Email, string Postalcode, string VideoDiscription, string DetailedCompanyPro, string Logoimage)
        public JsonResult SaveLoanRequestdescription(LoanRequestDescription objLoanDescription, List<string> comments)
        {
            if (ModelState.IsValid)
            {
                _IFileUploadDA.UpdateUploadedFileComments(comments);
                var loanobject = Automapper.MapLoanRequest(objLoanDescription);

                loanobject.PersonalGuaranteeInfos.Clear();
                var message = _issuerService.SaveLoanrequest(loanobject);

                foreach (var personGuarantee in objLoanDescription.PersonGuarantees)
                {
                  
                    switch (personGuarantee.State)
                    {
                        case EntityState.Delete:
                            var model = Automapper.MaPPersonalGuaranteeInfo(personGuarantee);
                            model.Request_Id = loanobject.RequestId;
                            _iPersonGuaranteeService.Delete(model);
                            break;
                        case EntityState.New:
                             var modelnew = Automapper.MaPPersonalGuaranteeInfo(personGuarantee);
                             modelnew.Request_Id = loanobject.RequestId;
                            _iPersonGuaranteeService.Save(modelnew);
                            break;

                        case EntityState.Update:
                            var modelupdate = Automapper.MaPPersonalGuaranteeInfo(personGuarantee);
                            modelupdate.Request_Id = loanobject.RequestId;
                            _iPersonGuaranteeService.Update(modelupdate);
                            break;
                    }
                }
              
                _unitOfWork.Commit();
                if (message == "Success")
                {
                    Response.Cookies["Loansteps"].Value = "2";
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {

                    Response.Cookies["Loansteps"].Value = "1";
                    return Json("Failure", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                Response.Cookies["Loansteps"].Value = "1";
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }
        public JsonResult LoadLoanGuarantors(long requestId)
        {
            var personals = Automapper.MaPPersonalGuaranteeInfo(_iPersonGuaranteeService.Get(requestId).ToList());
            foreach (var personGuarantee in personals)
            {
                personGuarantee.State = EntityState.Update;
            }
            return Json(personals, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Method for getting list of questions for loan purpose id
        /// </summary>        
        /// <returns></returns>
        public JsonResult GetQuestionsforLoanRequest(int LoanpurposeId)
        {
            string result = GetQuestionslistforLoanPurpose(LoanpurposeId);
            return Json(result, JsonRequestBehavior.AllowGet);
        }
        private string GetQuestionslistforLoanPurpose(int LoanpurposeId)
        {
            StringBuilder strdiv = new StringBuilder();
            try
            {
                //var question =
                //    db.tbl_QuestionsForSelectedLoanpurpose.FirstOrDefault(p => p.Loanpurpose_Id == LoanpurposeId);
                strdiv.Append("<div class='fl bot_mar width100'><div class='wish_txt_des' id='" + LoanpurposeId +
                              "'> Please provide more details on the purpose </div><textarea  class='des_tarea' placeholder='Details'/></div>");
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
            return strdiv.ToString();
        }
        /// <summary>
        /// Method for saving loan request basic details 
        /// </summary>       
        /// <returns></returns>
        public JsonResult SaveLoanRequestMoolahCore(LoanRequestMoolahCore objmoolahcore)
        {

            string message = "";
            var moolahCore = Automapper.MapLoanRequestMoolahCore(objmoolahcore);

            message = _issuerService.SaveLoanrequestmoolahcore(moolahCore);// objborroweropr.SaveLoanrequestmoolahcore(objmoolahcore);
            _unitOfWork.Commit();
            if (message.Contains('Â'))
            {
                var msg = message.Split('Â');
                Session["MoolahCoreID"] = msg[0];

                if (msg[1] == "Success")
                {
                    Response.Cookies["Loansteps"].Value = "3";
                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ListofIndex.Clear();
                    Response.Cookies["Loansteps"].Value = "2";
                    return Json("Failure", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                Response.Cookies["Loansteps"].Value = "2";
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
            //}
            //else
            //{
            //    // Session["Loanstep"] = 2;
            //    Response.Cookies["Loansteps"].Value = "2";
            //    return Json("Failure", JsonRequestBehavior.AllowGet);
            //}
        }



        /// <summary>
        /// Method for saving loan request basic details 
        /// </summary>       
        /// <returns></returns>
        public ActionResult SaveLoanRequestMoolahPeri(LoanRequestMoolahPeri objmoolahPeri)
        {
            if (ModelState.IsValid)
            {
                string message = "";
                message = _imoolahPeriService.Save(Automapper.MapLoanRequestMoolahPeri(objmoolahPeri), false);// objborroweropr.SaveLoanrequestmoolahPeri(objmoolahPeri, false);

                if (message == "Success" && Session["MoolahCoreID"] != null)
                {
                    if (ListofIndex != null)
                    {
                        foreach (int i in ListofIndex)
                            obj.RemoveAt(i);
                    }
                    if (obj.Count > 0)
                    {
                        foreach (OutstandingLitigation objLitigation in obj)
                        {
                            objLitigation.MoolahCore_Id = Convert.ToInt32(Session["MoolahCoreID"]);
                            _iOutStandingLitigationService.Save(Automapper.MapOutstandingLitigation(objLitigation)); //objborroweropr.SaveLoanRequestLitigations(objLitigation);
                        }
                    }
                    ListofIndex.Clear();//clearing list for further use, and avoiding duplicacy
                    obj.Clear();
                    //clearing list for further use, and avoiding duplicacy

                    Session["LoanrequestId"] = null;
                    Session["MoolahCoreID"] = null;
                    Response.Cookies["Loansteps"].Value = "4";

                    return Json("Success", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    //Session["Loanstep"] = 3;
                    Response.Cookies["Loansteps"].Value = "3";
                    return Json("Failure", JsonRequestBehavior.AllowGet);
                }
            }
            else
            {
                //Session["Loanstep"] = 3;
                Response.Cookies["Loansteps"].Value = "3";
                return Json("Failure", JsonRequestBehavior.AllowGet);
            }
        }

        public JsonResult SaveLoanRequestMoolahPeriForPreview(LoanRequestMoolahPeri objmoolahPeri)
        {
            ModelState.Remove("MoolahPerk_Id");
            if (ModelState.IsValid)
            {
                _issuerService.SaveLoanrequestmoolahPeri(Automapper.MapLoanRequest(objmoolahPeri));// objborroweropr.SaveLoanrequestmoolahPeri(objmoolahPeri, true);
                return Json("Success", JsonRequestBehavior.AllowGet);
            }
            return Json("Failure", JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method for saving loan request basic details 
        /// </summary>       
        /// <returns></returns>
        public int Makecollection(string LitigationType, decimal? AmountOutstanding, decimal? CreditLimit,
                               decimal? UsageinLastMonth, DateTime? RenewalDate, decimal? RepaymentAmount,
                               string RepaymentPeriod, DateTime? DateofLastPayment, string Description,
                               int? PaymentTerms)
        {
            OutstandingLitigation objoverdraft = new OutstandingLitigation();
            objoverdraft.LitigationType = LitigationType;
            objoverdraft.AmountOutstanding = AmountOutstanding;

            if (LitigationType == "Overdraft Facility" || LitigationType == "Factoring Discount")
            {
                objoverdraft.CreditLimit = CreditLimit;
                objoverdraft.UsageinLastMonth = UsageinLastMonth;
                objoverdraft.RenewalDate = RenewalDate;
            }
            if (LitigationType == "Unsecured Loan" || LitigationType == "Secured Loan")
            {
                objoverdraft.RepaymentAmount = RepaymentAmount;
                objoverdraft._RepaymentPeriodSet = RepaymentPeriod;
                objoverdraft.DateofLastPayment = DateofLastPayment;
            }
            if (LitigationType == "Secured Loan")
                objoverdraft.Description = Description;
            if (LitigationType == "Trade Creditors")
                objoverdraft.PaymentTerms = PaymentTerms;
            //objoverdraft.MoolahCore_Id=                        
            obj.Add(objoverdraft);

            return obj.IndexOf(objoverdraft);
        }

        /// <summary>
        /// ///////////////////////////////Make collection of index from where we have to delete the items from oustandinglitigation items
        /// </summary>
        /// <param name="DeleteIndex"></param>
        public void DeleteRecordFromCollection(int DeleteIndex)
        {
            ListofIndex.Add(DeleteIndex);

        }
        public ActionResult ReviewandSubmit()
        {
            return View("_ReviewandSubmit");
        }


        ///////////////////////Loan Request Edit Section. This section will contain function for updation ofl loan request/////////////////////////////////////////////////////
        #region LoanRequestEdit

        /////////////// Delete uploaded documents for loan request  //////////////////////////
        public void DeleteDocument(int DocumentID, string FileName)
        {
            DeleteFile(Path.Combine(Server.MapPath("~/Content/Uploads/Userdocuments"), FileName));
            ListofIndex.Add(DocumentID);///////added to collection//////////////            

        }

        private void DeleteFile(String fileToDelete)
        {
            try
            {
                FileInfo f = new FileInfo(fileToDelete);
                if (f.Exists)
                {
                    f.Delete();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);

            }
        }


        ////////////////////////// Action method for edit loan request description and get moolah core detial ///////////////////////////

        /// <summary>
        /// Method for saving loan request basic details 
        /// </summary>       
        /// <returns></returns>
        public JsonResult SaveLoanRequestdescriptionEdit(decimal Amount, string Terms, decimal Rate, bool Ispersonalguarantee, int LoanpurposeID, string XMLquestionswithanswers, string Loanimages, string LoanDocs,
             string NameasinNRICPassport, string Designation, string NRICPassport, string Reasons, string Residentialaddress, string Telephone, string Email, string Postalcode, string VideoDiscription, string DetailedCompanyPro)
        {

            LoanRequestMoolahCore loanrequestmoolahcore = null;
            if (ModelState.IsValid)
            {
                decimal EIR = (decimal)LoanRequestCalc.CalculateTargetEIR((double)Amount, Terms, (double)Rate);
                string message = _issuerService.SaveLoanrequest(Amount, Terms, Rate, EIR, Ispersonalguarantee, LoanpurposeID, XMLquestionswithanswers, Loanimages, LoanDocs,
               NameasinNRICPassport, Designation, NRICPassport, Reasons, Residentialaddress, Telephone, Email, Postalcode, VideoDiscription, ListofIndex, DetailedCompanyPro, "");
                ListofIndex.Clear();//////clear list , so it can be used for outstanding litigations delting
                if (message == "Success")
                {
                    if (Session["LoanrequestId"] != null)
                        loanrequestmoolahcore = Automapper.MapLoanRequestMoolahCore(_imMoolahCoreService.Get(Convert.ToInt64(Session["LoanrequestId"])));
                    //collection litigation ids which are already in database so if user will delete while updating, the coresponding record will be deliting from collection which is used while updating 
                    if (loanrequestmoolahcore != null)
                    {
                        foreach (OutstandingLitigation o in loanrequestmoolahcore.obj)
                        {
                            OutstandingLitigation objlitigation = new OutstandingLitigation();
                            objlitigation.MoolahCore_Id = o.MoolahCore_Id;
                            objlitigation.LitigationType = o.LitigationType;
                            objlitigation.AmountOutstanding = o.AmountOutstanding;
                            objlitigation.CreditLimit = o.CreditLimit;
                            objlitigation.UsageinLastMonth = o.UsageinLastMonth;
                            objlitigation.RenewalDate = o.RenewalDate;
                            objlitigation.RepaymentAmount = o.RepaymentAmount;
                            objlitigation._RepaymentPeriodSet = o._RepaymentPeriodSet;
                            objlitigation.DateofLastPayment = o.DateofLastPayment;
                            objlitigation.Description = o.Description;
                            objlitigation.PaymentTerms = o.PaymentTerms;
                            obj.Add(objlitigation);
                        }
                    }


                    return Json(
                        new
                        {
                            html = this.RenderPartialViewToString("_LoanRequestMoolahCoreEdit", loanrequestmoolahcore)
                        });
                }

                else
                {
                    return Json(
                      new
                      {
                          html = this.RenderPartialViewToString("_LoanRequestMoolahCoreEdit", loanrequestmoolahcore)
                      });
                }
            }
            else
            {
                return Json(
                              new
                              {
                                  html = "Failure"
                              });

            }
        }




        /// <summary>
        /// update moolahcore and get moolha peri deatil
        /// </summary>       
        /// <returns></returns>
        public JsonResult UpdateLoanRequestMoolahCore(LoanRequestMoolahCore objmoolahcore)
        {
            LoanRequestMoolahPeri objmoolahperi = null;
            if (ModelState.IsValid)
            {
                string message = "";
                message = _issuerService.SaveLoanrequestmoolahcore(Automapper.MapLoanRequestMoolahCore(objmoolahcore));// objborroweropr.SaveLoanrequestmoolahcore(objmoolahcore);
               _unitOfWork.Commit();
                if (message.Contains('Â'))
                {
                    var msg = message.Split('Â');
                    Session["MoolahCoreID"] = msg[0];

                    if (msg[1] == "Success")
                    {
                        if (Session["LoanrequestId"] != null)
                            objmoolahperi =
                                Automapper.MapLoanRequestMoolahPeri(
                                    _imoolahPeriService.Get(Convert.ToInt32(Session["LoanrequestId"])));// objborroweropr.GetMoolahPeriDetailbyLoanRequestID(Convert.ToInt32(Session["LoanrequestId"]));
                        return Json(
                            new
                            {
                                html = this.RenderPartialViewToString("_LoanRequestmoolahPeriEdit", objmoolahperi)
                            });
                    }
                    else
                    {
                        return Json(
                                      new
                                      {
                                          html = "Failure"
                                      });

                    }
                }
                else
                {
                    return Json(
                                  new
                                  {
                                      html = "Failure"
                                  });

                }
            }
            else
            {
                return Json(
                              new
                              {
                                  html = "Failure"
                              });

            }
        }

        #endregion
        #region Dashboard
        public void AcceptorRejectLoanOffer(string OfferIds, string Flag)
        {

            _issuerService.AcceptorRejectLoanOffer(OfferIds, Convert.ToBoolean(Flag)); //objborroweropr.AcceptorRejectLoanOffer(OfferIds, Convert.ToBoolean(Flag));
        }

        //public ActionResult AjaxPage(int? page)
        //{
        //    int currentPageIndex = page.HasValue ? page.Value - 1 : 0;
        //    LoanRequestPaging products = objborroweropr.test(currentPageIndex, 10);
        //    return PartialView("_DashboardLoanRequestPaging", products);

        //}
        /// <summary>
        /// Set Loan status to approved by request id
        /// </summary>
        public bool ChangeLoanStatustoApproved(int RequestID)
        {
            return _issuerService.SetLoanStatustoApproved(RequestID);// objborroweropr.SetLoanStatustoApproved(RequestID);

        }
        #endregion

        /// <summary>
        /// This function will check various condition and allcate loan, and return appropriate message to user
        /// If flag is true (it will allocate the loan ) else will withdraw loan and credite the user balance
        /// </summary>
        /// <returns>string</returns>
        public JsonResult AllocateLoan(long loanReqestId, bool Flag)
        {
            var statusTemp = string.Empty;
            var statusCategoryTemp = string.Empty;

            //This value is only to call the mthod. This value may or may not assigned.
            decimal finalRate = 0;

            if (Flag == true)
                _ILoanAllocationTask.Allocate(loanReqestId, out statusTemp, out statusCategoryTemp, out finalRate);
            else
            {
                statusTemp = _issuerService.Withdrawloan(loanReqestId, out statusCategoryTemp);
            }

            return Json(new { status = statusTemp, statusCategory = statusCategoryTemp }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method will allow to change the rate for his loan request
        /// </summary>
        /// <param name="RequestID"></param>
        /// <param name="Password"></param>
        /// <param name="NewRate"></param>
        /// <returns></returns>
        public JsonResult EditLoanRateByRequestID(int RequestID, string Password, decimal? NewRate)
        {
            return Json(_issuerService.ChangeLoanRateByRequestID(RequestID, Password, NewRate), JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public JsonResult GetTargetEIR(double amount, double targetRate, string term)
        {
            double targetEIRTemp = LoanRequestCalc.CalculateTargetEIR(amount, term, targetRate);

            return Json(new { targetEIR = Double.IsNaN(targetEIRTemp) ? 0 : targetEIRTemp }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetMoolahFees(double amount, string term)
        {
            double moolahFeesTemp = LoanRequestCalc.CalculateMoolahFees(amount, term);

            return Json(new { moolahFees = string.Format("{0:C}", Double.IsNaN(moolahFeesTemp) ? 0 : moolahFeesTemp).Replace("$", "") }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ConfirmRegistration()
        {
            return View();
        }

        /// <summary>
        /// Method for submit user
        /// </summary>      
        /// <returns></returns>
        public JsonResult SubmitUser(List<string> comments)
        {
            string error = string.Empty;
            if (!RegistrationHelper.HasAccountDetailsExist(out error))
            {
                return Json(error, JsonRequestBehavior.AllowGet);
            }

            _IFileUploadDA.UpdateUploadedFileComments(comments);
            var opr = new UserCommonOperations();
            var isSuccess = opr.SubmitUser();
            return Json(isSuccess ? "Success" : "Failure", JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoanTransactions()
        {
            var model = new List<TransactionViewModel>();

            model = Mapper.Map<IEnumerable<tbl_LoanTransactions>, IEnumerable<TransactionViewModel>>(
                _ITransactionsDA.GetTransactionbyUser(HttpContext.User.Identity != null ? HttpContext.User.Identity.Name : string.Empty))
                .ToList();

            return View(model);
        }

        public PartialViewResult _Transactions(string from, string to)
        {
            var model = new List<TransactionViewModel>();

            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            model = Mapper.Map<IEnumerable<tbl_LoanTransactions>, IEnumerable<TransactionViewModel>>(
                _ITransactionsDA.GetTransactionbyUser(HttpContext.User.Identity != null ? HttpContext.User.Identity.Name : string.Empty, _from, _to))
                .ToList();

            return PartialView(model);
        }

        public ActionResult ActionLogs()
        {
            var model = new List<AuditLogViewModel>();
            model = Mapper.Map<IEnumerable<tbl_AuditLog>, IEnumerable<AuditLogViewModel>>(
                _IAuditLogDA.GetBorrowerActionLogsByUsername(HttpContext.User.Identity != null ? HttpContext.User.Identity.Name : string.Empty)).ToList();

            return View(model);
        }

        public PartialViewResult _Logs(string from, string to)
        {
            var model = new List<AuditLogViewModel>();

            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            model = Mapper.Map<IEnumerable<tbl_AuditLog>, IEnumerable<AuditLogViewModel>>(
                _IAuditLogDA.GetBorrowerActionLogsByUsername(HttpContext.User.Identity != null ? HttpContext.User.Identity.Name : string.Empty,
                _from, _to)).ToList();
            return PartialView(model);
        }

        public JsonResult GetCurrentRequestId()
        {
            if (System.Web.HttpContext.Current.Session["LoanrequestId"] != null)
            {
                return Json(System.Web.HttpContext.Current.Session["LoanrequestId"]);
            }
            return Json("-999");
        }

        public ActionResult RemoveLoanRequesTDraft(long requestId)
        {
            if (!_IBorrowerLoanDetailsDA.RemoveLoanRequestFromId(requestId))
                ViewData["Error"] = ErrorMessages.FAILED_LOAN_REQUEST_DELETE;

            return RedirectToAction("Index");
        }

        public JsonResult PublishLoan(long requestId)
        {
            bool _status = _IBorrowerLoanDetailsDA.PublishLoan(requestId);

            return Json(new { status = _status ? StatusMessages.SUCCESS : StatusMessages.FAILED }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult DeleteLoan(long requestId)
        {
            bool _status = _IBorrowerLoanDetailsDA.DeleteLoan(requestId);

            return Json(new { status = _status ? StatusMessages.SUCCESS : StatusMessages.FAILED }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CreateInitialLoanRequest(decimal amount, string terms, decimal rate)
        {
            var loanRequest = _IBorrowerLoanDetailsDA.InsertInitialLoanRequest(amount, terms, rate,
                User.Identity.Name);

            return Json(new { requestId = loanRequest }, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public PartialViewResult loanCalculator()
        {
            ViewData["VerificationCheck"] = objusercommon.VerificationCheck();
            return PartialView(ViewData["VerificationCheck"]);
        }

        public PartialViewResult _ProvisionalContract(long requestID)
        {
            var borrowerLoanContract = _ILoanAllocationDA.GetBorrowerLoanContractModel(requestID);
            ViewBag.Repayments = _ILoanPaymentsTask.GetRepaymentSchedule(requestID, (double)borrowerLoanContract.Amount, (double)borrowerLoanContract.AcceptedRate,
                    borrowerLoanContract.Tenure, borrowerLoanContract.AgreementDate);
            return PartialView(borrowerLoanContract);
        }

        public PartialViewResult _CurrentRequests()
        {
            return PartialView(Automapper.MapLoanRequestDashboardList(_issuerService.GetCurrentRequests()));// objborroweropr.GetCurrentRequests());
        }

        public PartialViewResult _OutstandingPayments()
        {
            var outstandingPayments = Mapper.Map<IEnumerable<tbl_LoanAmortization>, IEnumerable<PaymentViewModel>>(
                _IBorrowerLoanDetailsDA.Get3MonthsOutstandingAnlAllLatePaymentsByUser(User.Identity.Name)).ToList();

            return PartialView(outstandingPayments);
        }

        public PartialViewResult _OutstandingNoteDetails()
        {
            var outstandingLoans = Mapper.Map<IEnumerable<LoanEntity>, IEnumerable<LoanViewModel>>(
                    _IBorrowerLoanDetailsDA.GetLoansByUser(User.Identity.Name,
                    new List<LoanRequestStatus>() { LoanRequestStatus.Matched, LoanRequestStatus.FundApproved, LoanRequestStatus.FundRejected })).ToList();

            return PartialView(outstandingLoans);
        }
        public ActionResult LoanRequest(long? requestId = null)
        {
            //  LoanRequest objloanrequest = Automapper.MapLoanRequest(_issuerService.LoanrequestLoad());
            LoanRequest objloanrequest;
            var objloanrequestdesc = new LoanRequestDescription();
            var objloanrequestmoolahcore = new LoanRequestMoolahCore();
            var objloanrequestmoolahPeri = new LoanRequestMoolahPeri();
            _sessionService.Set<long>("LoanrequestId",requestId.Value);
            objloanrequestdesc.Accountnumber = CommonMethods.GetAccountNumber(_iCurrentUser.ApplicationUser.UserID, _iCurrentUser.ApplicationUser.UserRole);
            objloanrequestdesc.LoanpurposeList = _loanPurposeService.Get().Where(p => p.Isactive == true).OrderBy(p => p.PurposeName).ToList();
            objloanrequestdesc._IspersonalGuarantee = new SelectList(IsPersonalguarantee(), "Value", "Text", "No");
            objloanrequestdesc._Nationality = new SelectList(new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = "Singapore Citizen",
                                Value = "Singapore Citizen",
                                Selected = true
                            },
                        new SelectListItem {Text = "Singapore PR", Value = "Singapore PR"},
                        new SelectListItem {Text = "Other Nationalities", Value = "Other Nationalities"}
                    }, "Value", "Text", "Singapore");
            objloanrequestmoolahcore.Accountnumber = objloanrequestdesc.Accountnumber;
            objloanrequestmoolahcore._Litigation = new SelectList(OutstandingLitigation(), "Value", "Text");
            objloanrequestmoolahPeri.Accountnumber = objloanrequestdesc.Accountnumber;

            objloanrequestmoolahPeri.MoolahPerk = AddCoustomValueinEnd();
            /////////////////////////////////
            OutstandingLitigation objlitigation = new OutstandingLitigation();
            objlitigation._RepaymentPeriod = new SelectList(MoolaCoreTime(), "Value", "Text");
            objloanrequest = new LoanRequest();
            objloanrequest.LoanRequestDescription = objloanrequestdesc;
            objloanrequest.LoanRequestmoolahCore = objloanrequestmoolahcore;
            objloanrequest.LoanRequestmoolahPeri = objloanrequestmoolahPeri;
            objloanrequest.LoanRequestmoolahCore.Litigation = objlitigation;

            if (_sessionService.Get<int>("Loanstep") == 0)
            {
                _sessionService.Set<int>("Loanstep", 1);
            }
            if (objloanrequest != null)
            {
                // Update the order of the Loanpurpose list
                var oldIndex = -1;
                var purposeList = objloanrequest.LoanRequestDescription.LoanpurposeList.ToList();
                for (var i = 0; i < purposeList.Count(); i++)
                {
                    if (purposeList[i].PurposeName != @"Others") continue;
                    oldIndex = i;
                    break;
                }
                var item = purposeList[oldIndex];
                purposeList.RemoveAt(oldIndex);
                var newIndex = purposeList.Count;
                purposeList.Insert(newIndex, item);
                objloanrequest.LoanRequestDescription.LoanpurposeList = purposeList;

                // Update the order of the MoolahPerk list
                oldIndex = -1;
                var perkList = objloanrequest.LoanRequestmoolahPeri.MoolahPerk.ToList();
                for (var i = 0; i < perkList.Count(); i++)
                {
                    if (perkList[i].Text != @"NA") continue;
                    oldIndex = i;
                    break;
                }
                var perkItem = perkList[oldIndex];
                perkItem.Selected = true;
                perkList.RemoveAt(oldIndex);
                perkList.Insert(0, perkItem);
                objloanrequest.LoanRequestmoolahPeri.MoolahPerk = perkList;

                HttpCookie cookie = new HttpCookie("Loansteps");
                Response.Cookies["Loansteps"].Value = "1";
                // Response.Cookies.Add(["Loansteps"].;
                if (!string.IsNullOrEmpty(Request.QueryString["a"]) && !string.IsNullOrEmpty(Request.QueryString["t"]) && !string.IsNullOrEmpty(Request.QueryString["r"]))
                {
                    objloanrequest.LoanRequestDescription.Amount = Convert.ToDecimal(Request.QueryString["a"]);
                    objloanrequest.LoanRequestDescription.Term = Request.QueryString["t"];
                    objloanrequest.LoanRequestDescription.Rate = Convert.ToDecimal(Request.QueryString["r"]);
                    objloanrequest.LoanRequestDescription.RequestId = requestId.HasValue ? requestId.Value : 0;

                    var loanDraft = new LoanRequestViewModel();

                    loanDraft = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(
                        _IBorrowerLoanDetailsDA.GetLoanDraftByUser(User.Identity.Name));
                    if (loanDraft != null)
                    {
                        UpdateLoadLoanRequestDescription(objloanrequest.LoanRequestDescription);//  objborroweropr.UpdateLoadLoanRequestDescription(objloanrequest.LoanRequestDescription);
                        objloanrequest.LoanRequestmoolahCore.LoanRequest_Id = objloanrequest.LoanRequestDescription.RequestId;
                        UpdateLoadLoanRequestMoolahcore(objloanrequest.LoanRequestmoolahCore);//  objborroweropr.UpdateLoadLoanRequestMoolahcore(objloanrequest.LoanRequestmoolahCore);
                        objloanrequest.LoanRequestmoolahPeri.Request_ID = objloanrequest.LoanRequestDescription.RequestId;
                        UpdateLoadLoanRequestMoolahperi(objloanrequest.LoanRequestmoolahPeri);//  objborroweropr.UpdateLoadLoanRequestMoolahperi(objloanrequest.LoanRequestmoolahPeri);
                    }
                }
                else
                    return RedirectToAction("MakeLoanRequest", "User");


                return View(objloanrequest);
            }
            else
            {
                return RedirectToAction("Information", "User");

            }
        }
        private void UpdateLoadLoanRequestDescription(LoanRequestDescription loanRequest)
        {
            var request = _loanRequestService.GetLoanRequestbyRequestID(loanRequest.RequestId);
            if (request != null)
            {
                loanRequest.Amount = request.Amount;
                loanRequest.Term = request.Terms;
                loanRequest.Rate = request.Rate ?? 0;
                loanRequest.SummaryCompanyPro = request.SummaryCompanyPro;
                loanRequest.DetailedCompanyPro = request.DetailedCompanyPro;
                loanRequest.NumberOfEmployees = request.NumberOfEmployees ?? 0;
                loanRequest.LoanPurposeId = request.LoanPurpose_Id;
                loanRequest.LoanPurposeDescription = request.LoanPurposeDescription;
                loanRequest.IspersonalGuarantee = request.IsPersonalGuarantee;
                loanRequest.Headline = request.Headline;

                if (loanRequest.IspersonalGuarantee != null && (bool)loanRequest.IspersonalGuarantee)
                {
                    var info = request.PersonalGuaranteeInfos.ToList().Select(a => new PersonGuarantee()
                    {
                        id = a.InfoId,
                        Email = a.Email,
                        NameInNRIC = a.NameAsinNRIC,
                        Nationality = a.Nationality,
                        NRICNo = a.NRIC_Passport,
                        PassportNo = a.PassportNumber,
                        PostalCode = a.Postalcode,
                        Reason = a.Reasons,
                        AddressSpilt = a.ResidentialAddress,
                        Telephone = a.Telephone,
                        State = EntityState.Update

                    });
                    loanRequest.PersonGuarantees = info.ToList();

                }
            }
        }
        private void UpdateLoadLoanRequestMoolahcore(LoanRequestMoolahCore loanRequest)
        {
            var objMoolahcore = _loanRequestService.GetLoanRequestbyRequestID(loanRequest.LoanRequest_Id.Value).MoolahCores.FirstOrDefault();
            if (objMoolahcore == null) return;
            loanRequest.ID = objMoolahcore.MoolahCoreId;
            loanRequest.LatestYear = objMoolahcore.LatestYear;
            loanRequest.PreviousYear = objMoolahcore.PreiviousYear;
            loanRequest.TotalDebt_LY = objMoolahcore.TotalDebt_LY;
            loanRequest.TotalDebt_PY = objMoolahcore.TotalDebt_PY;
            loanRequest.Turnovers_LY = objMoolahcore.Turnovers_LY;
            loanRequest.Turnovers_PY = objMoolahcore.Turnovers_PY;

            loanRequest.ProfitBeforIntAndTax_LY = objMoolahcore.ProfitBeforIntAndTax_LY;
            loanRequest.ProfitBeforIntAndTax_PY = objMoolahcore.ProfitBeforIntAndTax_PY;

            loanRequest.InterestExpense_LY = objMoolahcore.InterestExpense_LY;
            loanRequest.InterestExpense_PY = objMoolahcore.InterestExpense_PY;

            loanRequest.InterestCoverageRatio_LY = objMoolahcore.InterestCoverageRatio_LY;
            loanRequest.InterestCoverageRatio_PY = objMoolahcore.InterestCoverageRatio_PY;

            //loanRequest.EBITDA_LY = objMoolahcore.EBITDA_LY;
            //loanRequest.EBITDA_PY = objMoolahcore.EBITDA_PY;

            //loanRequest.DebtEBITDARatio_LY = objMoolahcore.DebtEBITDARatio_LY;
            //loanRequest.DebtEBITDARatio_PY = objMoolahcore.DebtEBITDARatio_PY;

            loanRequest.TotalShareholdersequity_LY = objMoolahcore.TotalShareholdersequity_LY;
            loanRequest.TotalShareholdersequity_PY = objMoolahcore.TotalShareholdersequity_PY;
            loanRequest.CashFlowfromOperations_LY = objMoolahcore.CashFlowfromOperations_LY;
            loanRequest.CashFlowfromOperations_PY = objMoolahcore.CashFlowfromOperations_PY;
            loanRequest.CurrentAssests_LY = objMoolahcore.CurrentAssests_LY;
            loanRequest.CurrentAssests_PY = objMoolahcore.CurrentAssests_PY;
            loanRequest.CurrentLiabilities_LY = objMoolahcore.CurrentLiabilities_LY;
            loanRequest.CurrentLiabilities_PY = objMoolahcore.CurrentLiabilities_PY;
            loanRequest.CurrentRatio_LY = objMoolahcore.CurrentRatio_LY;
            loanRequest.CurrentRatio_PY = objMoolahcore.CurrentRatio_PY;
            loanRequest.Debt_Equity_LY = objMoolahcore.Debt_Equity_LY;
            loanRequest.Debt_Equity_PY = objMoolahcore.Debt_Equity_PY;
            loanRequest.IsoutstandingLitigation = objMoolahcore.IsoutstandingLitigation;
            loanRequest.NetprofitafterTax_LY = objMoolahcore.NetprofitafterTax_LY.HasValue ? objMoolahcore.NetprofitafterTax_LY.Value : 0;
            loanRequest.NetprofitafterTax_PY = objMoolahcore.NetprofitafterTax_PY;
            loanRequest.AvgThreeMntCashBalBankAcc_LY = objMoolahcore.AvgThreeMntCashBalBankAcc_LY;
            loanRequest.AvgThreeMntCashBalBankAcc_PY = objMoolahcore.AvgThreeMntCashBalBankAcc_PY;
            loanRequest.Profitable = objMoolahcore.Profitable;
        }

        private void UpdateLoadLoanRequestMoolahperi(LoanRequestMoolahPeri loanRequest)
        {
            var moolahPeri = _loanRequestService.GetLoanRequestbyRequestID(loanRequest.Request_ID.Value).MoolahPeris.FirstOrDefault();
            if (moolahPeri != null)
            {
                loanRequest.Accreditation = moolahPeri.Accreditation;
                loanRequest.BiodataOfDS = moolahPeri.BiodataOfDS;
                loanRequest.BusinessAwards = moolahPeri.BusinessAwards;
                loanRequest.CommunityInitiative = moolahPeri.CommunityInitiative;
                loanRequest.DigitalFootPrint = moolahPeri.DigitalFootPrint;
                loanRequest.MoolahPerk_Custom = moolahPeri.MoolahPerk_Custom;

                loanRequest.MoolahPerk_Id = moolahPeri.MoolahPerk_Id;
                if (moolahPeri.MoolahPerk_Custom != null)
                {
                    moolahPeri.MoolahPerk_Custom = moolahPeri.MoolahPerk_Custom;
                }
                else
                {
                    moolahPeri.MoolahPerk_Id = moolahPeri.MoolahPerk_Id;
                }

                loanRequest.MTA = moolahPeri.MTA;
                loanRequest.DateCreated = moolahPeri.DateCreated;
            }
        }
        private List<SelectListItem> MoolaCoreTime()
        {
            var Time = new List<SelectListItem>()
            {
                new SelectListItem(){ Value = "Weekly", Text = "Weekly" }  ,     
                new SelectListItem(){ Value = "Monthly", Text = "Monthly" }, 
            };
            return Time;
        }
        private List<SelectListItem> AddCoustomValueinEnd()
        {
            var AddinListItem = _moolahPerkLenderService.GetAll().Where(p => p.Isactive == true).OrderBy(p => p.MoolahPerk).ToList();
            var FinalList = new List<SelectListItem>();
            foreach (var moolahperk in AddinListItem)
            {
                FinalList.Add(new SelectListItem() { Value = moolahperk.MoolahPerkId.ToString(), Text = moolahperk.MoolahPerk.ToString() });
            }

            return FinalList;

        }
        private List<SelectListItem> OutstandingLitigation()
        {
            var Litigation = new List<SelectListItem>()
                           {
                                new SelectListItem(){ Value = "Overdraft Facility", Text = "Overdraft Facility" }  ,     
                                new SelectListItem(){ Value = "Unsecured Loan", Text = "Unsecured Loan" }, 
                                new SelectListItem(){ Value = "Trade Creditors", Text = "Trade Creditors" },
                                new SelectListItem(){ Value = "Secured Loan", Text = "Secured Loan" },  
                                new SelectListItem(){ Value = "Factoring Discount", Text = "Factoring Discount" }
                            };
            return Litigation;
        }
        private List<SelectListItem> IsPersonalguarantee()
        {
            var Titles = new List<SelectListItem>()
                           {
                                new SelectListItem(){ Text = "No", Value= "false", Selected = false}  ,     
                               new SelectListItem(){ Text = "Yes", Value= "true", Selected = false} 
                                                    
                            };
            return Titles;
        }
    }
}








