﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Net.Mime;
using System.Text;
using System.Web;
using System.Web.Mvc;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Services.Implemantation1;
using MoolahConnect.Services.Interfaces1;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectnew.Filters;
using MoolahConnectClassLibrary.DAL;
using MoolahConnectnew.ViewModels;
using AutoMapper;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Entities;
using MoolahConnectnew.ViewModels.Investor;
using MoolahConnect.Util.SealedClasses;
using System.Web.Security;
using MoolahConnectnew.Providers;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Util.Enums;
using MoolahConnectnew.Controllers.Helpers;

namespace MoolahConnectnew.Controllers
{
    [authorize.AuthorizeRole(Roles = "Investor")]
    public class InvestorController : Controller
    {
        ILoanAllocationDA _ILoanAllocationDA;
        private readonly IAccountDA _accountDa;
        private readonly IGlobalVariableDA _gvDa;
        private UserCommonOperations objusercommon;
        private readonly ILoanPaymentsTask _ILoanPaymentTask;
        private readonly ILoanDetailsDA _ILoanDetailsDA;

        // New Services...
        private IInvestorService _investorService;

        public InvestorController(IInvestorService investorService)
        {
            _ILoanAllocationDA = new LoanAllocationDA();
            _accountDa = new AccountDA();
            _gvDa = new GlobalVariableDA();
            objusercommon = new UserCommonOperations();
            _ILoanPaymentTask = new LoanPaymentsTask();
            _ILoanDetailsDA = new LoanDetailsDA();

            // New Services...
            _investorService = investorService;
        }

        InvestorOperations objInvestorOperations = new InvestorOperations();
        /// <summary>
        /// Method for index load 
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            if (User.Identity.Name == null) return RedirectToAction("Index", "User");
            //var result = objInvestorOperations.IndexLoad();
            var result = _investorService.IndexLoad();
            if (result == "SubmittedCorporate")
            {
                return RedirectToAction("ConfirmRegistration", "Investor");
            }
            if (result == "SubmittedInvestor")
            {
                return RedirectToAction("ConfirmRegistration", "Investor");
            }
            if (result == "Corporate")
            {
                return RedirectToAction("Corporate", "Investor");
            }
            if (result == "Investor")
            {
                return RedirectToAction("Create", "Investor");
            }
            if (result == "User")
            {
                return RedirectToAction("Index", "User");
            }
            ViewBag.Tab = TempData["Tab"];
            #region Dashboard
            InvestorDashboard obj = objInvestorOperations.LoadDashboardInformation();

            List<LoanOfferViewModel> loanOffers = Mapper.Map<IEnumerable<tbl_Loanoffers>, IEnumerable<LoanOfferViewModel>>(_ILoanAllocationDA.GetLoanOffersByInvestor(User.Identity.Name)).ToList();
            ViewBag.LoanOffers = loanOffers;

            List<LoanInvestmentViewModel> loanInvestments =
                Mapper.Map<IEnumerable<LoanInvestmentEntity>, IEnumerable<LoanInvestmentViewModel>>(
                _ILoanAllocationDA.GetIndividualLoanInvestmentsByInvestor(User.Identity.Name)).ToList();
            ViewBag.LoanInvestments = loanInvestments;
            //ViewBag.WatchList = GetWatchlist();
           // ViewBag.MoolahBoard = GetMoolahBoard();
            return View(obj);
            #endregion
        }

        public ActionResult GetMoolahBoardView()
        {
            ViewBag.MoolahBoard = GetMoolahBoard();
            return PartialView("_MoolahBoardDashboard");
        }
        public ActionResult GetMoolahBoardWatchListView()
        {
            ViewBag.WatchList = GetWatchlist();
            return PartialView("WatchList");
        }
        public ActionResult GetTransferMoneyDashboardView()
        {
           InvestorDashboard obj = objInvestorOperations.LoadDashboardInformation();   
            return PartialView("_TransferMoneyDashboard",  obj.TransferMoney);
        }

        public ActionResult SummaryView()
        {
            InvestorDashboard obj = objInvestorOperations.LoadDashboardInformation();

            List<LoanOfferViewModel> loanOffers = Mapper.Map<IEnumerable<tbl_Loanoffers>, IEnumerable<LoanOfferViewModel>>(_ILoanAllocationDA.GetLoanOffersByInvestor(User.Identity.Name)).ToList();
            ViewBag.LoanOffers = loanOffers;

            List<LoanInvestmentViewModel> loanInvestments =
                Mapper.Map<IEnumerable<LoanInvestmentEntity>, IEnumerable<LoanInvestmentViewModel>>(
                _ILoanAllocationDA.GetIndividualLoanInvestmentsByInvestor(User.Identity.Name)).ToList();
            ViewBag.LoanInvestments = loanInvestments;
            return PartialView("_SummaryDashboard",obj.Summary);
        }
        private LoanRequestBoardViewModel GetWatchlist()
        {
            var u = _accountDa.GetUser(User.Identity.Name);
            var model = new LoanRequestBoardViewModel();
            var loans = _ILoanAllocationDA.GetWatchList(u.UserID);
            foreach (var loan in loans)
            {
                var viewmodel = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(loan);
                var loanCalculations = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(loan.RequestId,
                    loan.Amount);
                viewmodel.Funded = loanCalculations[0];
                viewmodel.AverageRate = Convert.ToDecimal(loan.Rate);

                var timeRemaining = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(loan.PublishedDate),
                    Settings.LoanRequestExpiryDays);
                if (timeRemaining.Count() > 0)
                {
                    viewmodel.Days = timeRemaining[0];
                    viewmodel.Hours = timeRemaining[1];
                    viewmodel.Minutes = timeRemaining[2];
                }

                if (viewmodel.Minutes > 0)
                    model.LoanRequestViewModels.Add(viewmodel);
            }
            return model;
        }

        private LoanRequestBoardViewModel GetMoolahBoard()
        {
            var model = new LoanRequestBoardViewModel();
            var loans = _ILoanAllocationDA.GetAllPublishedLoanRequests().ToList();
            var completionDays = _gvDa.Get().FirstOrDefault();
            foreach (var loan in loans)
            {
                var viewmodel = Mapper.Map<tbl_LoanRequests, LoanRequestViewModel>(loan);
                var loanCalculations = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(loan.RequestId, loan.Amount);
                viewmodel.Funded = loanCalculations[0];
                viewmodel.AverageRate = Convert.ToDecimal(loan.Rate);
                if (completionDays != null)
                {
                    var timeRemaining = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(loan.PublishedDate),
                        Settings.LoanRequestExpiryDays);
                    viewmodel.Days = timeRemaining[0];
                    viewmodel.Hours = timeRemaining[1];
                    viewmodel.Minutes = timeRemaining[2];
                }
                if (viewmodel.Minutes > 0)
                    model.LoanRequestViewModels.Add(viewmodel);
            }
            return model;
        }

        public JsonResult AddToWatchList(long requestId)
        {
            var u = _accountDa.GetUser(User.Identity.Name);
            _ILoanAllocationDA.AddToWatchList(u.UserID, requestId);
            //TempData["Tab"] = 3;
            //return RedirectToRoute(new { controller = "Investor", action = "Index" });
            return Json(new { id = 1 }, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RemoveFromWatchList(long requestId)
        {
            var u = _accountDa.GetUser(User.Identity.Name);
            _ILoanAllocationDA.RemoveFromWatchList(u.UserID, requestId);
            //TempData["Tab"] = 3;
            return Json(new { id = 1 }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// Method for generating PDF of registration form for investor (A view is passed to the controllerPDF for creating PDF)
        /// </summary>
        /// <returns></returns>
        public ActionResult GeneratePDFforRegistrationform()
        {
            var objinvestor = _investorService.GetRegistrationDetailsForPDF();
            //InvestorAccount objinvestor = objInvestorOperations.GetregistrationdetailsforPDF();
            ControllerPDF.RenderPartialViewToString(this, "GeneratePDFforRegistrationform", objinvestor, "RegistrationForm");
            return View();

        }


        //
        // GET: /Investor/Create 
        // Method to be called on view load (Get information of investor logged in, if any stored for binding with fields)

        public ActionResult Create()
        {

            var objInvestorAccount = _investorService.GetInvestorAccountInfo();//objInvestorOperations.GetinvestorAccountInfo();
            //InvestorAccount objInvestorAccount = objInvestorOperations.GetinvestorAccountInfo();
            return View(objInvestorAccount);
        }

        //
        // POST: /Investor/Create
        // Method for saving account basic details of investor

        [HttpPost]
        public ActionResult Create(InvestorAccountDetailsModel objinvestor)
        {
            String Message = String.Empty;

            Message = objInvestorOperations.CreateInvestorAccount(objinvestor);

            if (Message == "Success")
            {
                return RedirectToAction("Create", "Investor");
            }
            else
            {
                return RedirectToAction("Create", "Investor").WithFlash(new { notice = "Unfortunately we have encountered an unexpected error. Please contact customer support." });
            }


        }

        /// <summary>
        /// Method for getting knoweldge assessment options for specific question for knowledge assessment partial view
        /// </summary>        
        /// <returns></returns>
        [ChildActionOnly]
        public ActionResult GetKnowledgeassessmentoptions(int Parentid)
        {
            String result = objInvestorOperations.GetKnowledgeassessmentoptions(Parentid);

            if (result != "Failure")
            {
                return Content(result, MediaTypeNames.Text.Html);
            }
            else
            {
                return RedirectToAction("Create", "Investor").WithFlash(new { notice = "Some error occured during getting Knowledgeassessmentoptions" });
            }


        }


        /// <summary>
        /// Method for saving knowledge assessment details for investor using XML
        /// </summary>       
        /// <returns></returns>
        public JsonResult SaveKnowledgeassessmentForInvestor(string XMLdocs)
        {

            string result = string.Empty;

            string error = string.Empty;
            if (!RegistrationHelper.HasAccountDetailsExist(out error))
            {
                return Json(error, JsonRequestBehavior.AllowGet);
            }

            if (ModelState.IsValid)
            {
                result = objInvestorOperations.Saveknowledgeassesmentotions(XMLdocs);
                if (result == "Success")
                {
                    var opr = new UserCommonOperations();
                    var isSuccess = opr.SubmitUser();
                    return Json(isSuccess ? "Success" : "Failure", JsonRequestBehavior.AllowGet);
                }
                else if (result == "Failure")
                {
                    return Json("Some error occured during saving Knowledgeassessmentoptions", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json("User not found with this username", JsonRequestBehavior.AllowGet);
                }

            }
            else
            {
                return Json("Please fill data properly", JsonRequestBehavior.AllowGet);
            }
        }
        #region BorroweBriefDetail
        /// <summary>
        /// Save Loan offer, two checks for that, investor should have sufficient balance and the loan should be in offer period
        /// </summary>
        /// <param name="Amount"></param>
        /// <param name="Rate"></param>
        /// <param name="Password"></param>
        /// <param name="LoanRequest"></param>
        /// <param name="Term"></param>
        /// <returns>string</returns>

        #endregion

        #region LoanListing Live
        ////////It will include thos loans of investor which are not 100% funded with in offer period/////////
        public ActionResult Listings()
        {
            LiveLoanListings objLiveLoanListing = objInvestorOperations.GetLiveLoanListings();
            return View(objLiveLoanListing);
        }
        #endregion

        #region DashboardProfile
        ///////it will return a partial view for the profile tab on investor dashboard 
        public PartialViewResult GetUserProfileDetailbyUserID()
        {
            var result = Mapper.Map<tbl_AccountDetails,
                                    InvestorProfile>(objInvestorOperations.GetUserProfileDetailbyUserID());
            result.EmailAddress = HttpContext.User.Identity.Name;
            return PartialView("_ProfileDashboard", result);
        }
        #endregion

        /// <summary>
        /// check if user current balance is lesss then equals to the amount he is withdrawing from his account
        /// </summary>
        /// <returns></returns>
        public JsonResult CheckCurrentBalance(decimal WithdrawAmount, string Password)
        {
            string message = string.Empty;
            if (HttpContext.User.Identity.Name == null
                || !Membership.ValidateUser(HttpContext.User.Identity.Name, Password))
            {
                message = ErrorMessages.INVALID_PASSWORD;
            }
            else
            {
                message = objInvestorOperations.CheckCurrentBalance(WithdrawAmount);
            }

            return Json(message, JsonRequestBehavior.AllowGet);
        }
        /// <summary>
        /// Withdraw offer by offerid, and credit the balance in balance table
        /// </summary>
        /// <param name="OfferID"></param>
        /// <returns></returns>
        public JsonResult WithDrawOffer(int OfferID, long RequestId)
        {
            string message = string.Empty;

            var request = _ILoanAllocationDA.GetLoanRequestByRequestId(RequestId);

            if (request != null && request.LoanStatus == (int)LoanRequestStatus.Matched)
            {
                message = ErrorMessages.LOCK_OFFER_WITHDRAW_PROCESSING;   
            }
            else if (!_ILoanAllocationDA.CanWithdrawLoanOffer(RequestId, OfferID))
            {
                message = ErrorMessages.LOAN_OFFER_CANNOT_WITHDRAW_INSUFFICIENT_FUNDS;
            }
            else
            {
                message = objInvestorOperations.WithDrawOfferbyOfferID(OfferID);

                if (message == ContollerExtensions.ReturnSuccess())
                {
                    _ILoanDetailsDA.AddOfferLog(LoanOfferStatus.Withdrawn, OfferID);
                }
            }

            return Json(message, JsonRequestBehavior.AllowGet);

        }

        /// <summary>
        ///investor further refferences to loan request, by adding it in his watch list
        /// </summary>
        public JsonResult SaveAddToWatchList(int LoanRequestID)
        {
            return Json(objInvestorOperations.AddToWatchList(LoanRequestID), JsonRequestBehavior.AllowGet);
        }


        /// <summary>
        /// Method for getting details of user logged in and displaying form for saving account details of corporate user
        /// </summary>
        /// <returns></returns>
        public ActionResult Corporate()
        {
            BorrowerAccount objborrower = objInvestorOperations.CorporateUserLoad();
            if (objborrower == null)
            {
                return View();
            }
            else
            {
                return View(objborrower);
            }
        }
        //
        // POST: /Corporate Investor create

        [HttpPost]
        public ActionResult Corporate(BorrowerAccountDetailsModel objborrower)
        {
            try
            {
                string result = objInvestorOperations.CreateCorporateInestor(objborrower);
                if (result == "Success")
                {

                    var proxy = new UserCommonOperations();
                    var curEmail = proxy.GetCurrentUserEmail();
                    if (curEmail != objborrower.EmailAddress)
                    {
                        // Update email process
                        string errorMsg;
                        if (proxy.UpdateUserEmail(objborrower.EmailAddress, out errorMsg))
                        {
                            return RedirectToAction("LoggoffWithUsernameChange", "User");
                        }
                        return
                            RedirectToAction("Corporate", "Investor")
                                .WithFlash(new { notice = errorMsg });
                    }

                    return RedirectToAction("Corporate", "Investor");

                }
                else
                {
                    return RedirectToAction("Corporate", "Investor").WithFlash(new { notice = "Some error occured while saving details" });
                }
            }
            catch (Exception ex)
            {

                return RedirectToAction("Corporate", "Investor").WithFlash(new { notice = "Some error occured while saving details" });
            }
        }

        /// <summary>
        /// Method for checking dispaly name  availability in registeres investors list
        /// </summary>
        /// <param name="EmailAddress"></param>
        /// <returns></returns>
        [AllowAnonymous]
        public JsonResult CheckDisplayNameAvailbilty(string DisplayName, string Emailaddress)
        {
            return Json(objInvestorOperations.CheckDisplayNameAvailbilty(DisplayName, Emailaddress));

        }
        [AllowAnonymous]
        public JsonResult IsValidNRICFIN(string ICNumber, string DisplayName)
        {

            return Json(objusercommon.IsValidNRICFIN(ICNumber, DisplayName));

        }

        [AllowAnonymous]
        public JsonResult IsValidNRIIN(string NricNumber, string DisplayName)
        {
            return IsValidNRICFIN(NricNumber, DisplayName);
        }

        public PartialViewResult _LoanOffers(string from, string to)
        {
            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            List<LoanOfferViewModel> loanOffers = Mapper.Map<IEnumerable<tbl_Loanoffers>, IEnumerable<LoanOfferViewModel>>(
                _ILoanAllocationDA.GetLoanOffersByInvestor(User.Identity != null ? User.Identity.Name : string.Empty, _from, _to)).ToList();
            ViewBag.LoanOffers = loanOffers;
            return PartialView();
        }

        public PartialViewResult _LoanInvestments(string from, string to)
        {
            DateTime? _from = null;
            DateTime? _to = null;

            try
            {
                _from = DateTime.ParseExact(from, "dd/MM/yyyy", null);
                _to = DateTime.ParseExact(to, "dd/MM/yyyy", null);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }

            List<LoanInvestmentViewModel> loanInvestments = Mapper.Map<IEnumerable<LoanInvestmentEntity>, IEnumerable<LoanInvestmentViewModel>>(
                        _ILoanAllocationDA.GetLoanInvestmentsByInvestor(User.Identity != null ? User.Identity.Name : string.Empty, _from, _to)).ToList();
            ViewBag.LoanInvestments = loanInvestments;
            return PartialView();
        }

        public ActionResult ConfirmRegistration()
        {
            return View();
        }

        /// <summary>
        /// Method for submit user
        /// </summary>      
        /// <returns></returns>
        public JsonResult SubmitUser()
        {
            string error = string.Empty;
            if (!RegistrationHelper.HasAccountDetailsExist(out error))
            {
                return Json(error, JsonRequestBehavior.AllowGet);
            }

            var opr = new UserCommonOperations();
            var isSuccess = opr.SubmitUser();
            return Json(isSuccess ? "Success" : "Failure", JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public PartialViewResult ReturnCalculator()
        {
            ViewData["VerificationCheck"] = objusercommon.VerificationCheck();
            return PartialView(ViewData["VerificationCheck"]);
        }

        public PartialViewResult _ProvisionalContract(int offerID)
        {
            var investorLoanContract = _ILoanAllocationDA.GetInvestorLoanContractModel(offerID);
            var repaymentSchedule = _ILoanPaymentTask.GetRepaymentSchedule(null, (double)investorLoanContract.LoanAmount, (double)investorLoanContract.LoanAcceptedRate,
                investorLoanContract.Tenure, investorLoanContract.AgreementDate);

            ViewBag.Repayments = repaymentSchedule;

            return PartialView(investorLoanContract);
        }
    }

}
