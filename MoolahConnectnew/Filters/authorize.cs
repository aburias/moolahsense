﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Principal;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using MoolahConnectClassLibrary;

namespace MoolahConnectnew.Filters
{
    public class authorize
    {

        public class BooleanRequired : RequiredAttribute, IClientValidatable
        {

            public BooleanRequired()
            {

            }

            public override bool IsValid(object value)
            {
                return value != null && (bool)value == true;
            }

            public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
            {
                return new ModelClientValidationRule[] { new ModelClientValidationRule() { ValidationType = "brequired", ErrorMessage = this.ErrorMessage } };
            }
        }


        public class AuthorizeRoleAttribute : AuthorizeAttribute
        {
            protected override bool AuthorizeCore(HttpContextBase httpContext)
            {
                if (httpContext.User.Identity.IsAuthenticated)
                {
                    var authCookie = httpContext.Request.Cookies[FormsAuthentication.FormsCookieName];
                    if (authCookie != null)
                    {
                        var ticket = FormsAuthentication.Decrypt(authCookie.Value);
                        var identity = new GenericIdentity(httpContext.User.Identity.Name);
                        var roles = (ticket.UserData ?? string.Empty).Split('|');
                        httpContext.User = new GenericPrincipal(identity, roles);

                    }
                }

                return base.AuthorizeCore(httpContext);
            }
        }


      


    }
}