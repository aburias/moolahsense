﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
namespace MoolahConnectnew
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");
            routes.IgnoreRoute("{resource}.ashx/{*pathInfo}");

            routes.MapRoute(
                "LoanToNote",
                "Note/MatchedNoteDetails/{id}",
                new { controller = "Loan", action = "MatchedLoanDetails", id = UrlParameter.Optional });

            routes.MapRoute(
                "LoanToNote2",
                "Note/BorrowerNoteDetails/{id}",
                new { controller = "Loan", action = "BorrowerLoanDetails", id = UrlParameter.Optional });

            routes.MapRoute(
                "LoanToNote3",
                "Shared/NoteTransactions/{id}",
                new { controller = "Shared", action = "LoanTransactions", id = UrlParameter.Optional });

            routes.MapRoute(
                "Custom",
                "Issuer/InterestCalculator",
                new { controller = "Borrower", action = "LoanCalculator" });

            routes.MapRoute(
                "BorToIssuer",
                "Issuer/Request/{id}",
                new { controller = "Borrower", action = "LoanRequest", id = UrlParameter.Optional });

            routes.MapRoute(
                "BorToIssuer2",
                "Issuer/{action}/{id}",
                new { controller = "Borrower", action = "Index", id = UrlParameter.Optional });

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "User", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}