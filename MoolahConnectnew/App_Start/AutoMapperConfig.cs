﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;
using Microsoft.Ajax.Utilities;
using MoolahConnect.Models.BusinessModels;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectnew.ViewModels;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;
using MoolahConnectnew.ViewModels.Borrower;
using MoolahConnect.Util.SealedClasses;
using MoolahConnectnew.ViewModels.Investor;
using System.Web.Mvc;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util;
using Microsoft.VisualBasic;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnectnew.ViewModels.Admin;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.FinancialCalculations;
using LoanRequest = MoolahConnect.Models.BusinessModels.LoanRequest;
using MoolahConnect.Models.CustomModels;
using MoolahConnectnew.ViewModels.Loan;

namespace MoolahConnectnew.App_Start
{
    public static class AutoMapperConfig
    {
        public static void Configure()
        {
            ConfigureMoolahCoreIndicatorMappings();
            ConfigureMoolahPeriIndicatorMappings();

            ConfigureMoolahCoreIndicatorMappings1();
            ConfigureMoolahPeriIndicatorMappings1();
            ConfigureRepaymentMessage();

            ConfigureLoanMappings();
            ConfigurePaymentMappings();
            ConfigureLoanOfferMappings();
            ConfigureLoanInvestmentMappings();
            ConfigureLoanRequestMapping();
            ConfigureAccountDetailsMapping();
            ConfigureTransactionMapping();
            ConfigureAuditLogMappings();
            ConfigRepaymentMappings();
            ConfigSecurityQuestionMapping();
            ConfigInvestorProfileMapping();
            ConfigUploadedFileMappings();
            ConfigureLoanRepayment();
            ConfigureLoanInvestor();
            ConfigureLoanPayments();
            ConfigureAccountAdjustments();
            ConfigureUserAccounts();
            ConfigLoanRequestMapping();
            ConfigureUsersSummary();
        }

        private static void ConfigureUserAccounts()
        {
            Mapper.CreateMap<UserAccountEntity, UserAccountViewModel>()
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => CommonMethods.GetAccountNumber(src.UserID, src.UserRole)))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => CommonMethods.GetAccountFullName(src.Title, src.FirstName, src.LastName)));
        }

        private static void ConfigureUsersSummary()
        {
            Mapper.CreateMap<UsersSummaryDTO, UsersSummaryViewModel>();
        }



        private static void ConfigureAccountAdjustments()
        {
            Mapper.CreateMap<tbl_AccountAdjustment, AccountAdjustmentViewModel>()
                .ForMember(dest => dest.RequestID, opt => opt.MapFrom(src => src.tbl_LoanAmortization.tbl_LoanRequests.RequestId))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.tbl_LoanAmortization.tbl_LoanRequests.AcceptedDate))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => GetMaturityDate(src.tbl_LoanAmortization.tbl_LoanRequests.AcceptedDate, src.tbl_LoanAmortization.tbl_LoanRequests.Terms)))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.tbl_LoanAmortization.tbl_LoanRequests.FinalAmount))
                .ForMember(dest => dest.Rate, opt => opt.MapFrom(src => src.tbl_LoanAmortization.tbl_LoanRequests.Rate))
                .ForMember(dest => dest.RepaymentAmount, opt => opt.MapFrom(src => src.tbl_LoanAmortization.EmiAmount))
                .ForMember(dest => dest.RepaymentDate, opt => opt.MapFrom(src => src.tbl_LoanAmortization.EmiDate))
                .ForMember(dest => dest.ReceivedAmount, opt => opt.MapFrom(src => (double)src.TotalReceivable))
                .ForMember(dest => dest.Difference, opt => opt.MapFrom(src => (double)src.Difference))
                .ForMember(dest => dest.Adjusted, opt => opt.MapFrom(src => src.Adjusted))
                .ForMember(dest => dest.AdjustedDate, opt => opt.MapFrom(src => src.AdjustedDate))
                ;
        }

        private static void ConfigureLoanPayments()
        {
            Mapper.CreateMap<tbl_LoanPayments, LoanPaymentViewModel>()
                           .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.LoanRequestID))
                           .ForMember(dest => dest.RepaymentId, opt => opt.MapFrom(src => src.RepaymentID))
                           .ForMember(dest => dest.RepaymentDate, opt => opt.MapFrom(src => src.tbl_LoanAmortization.EmiDate))
                           .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                           .ForMember(dest => dest.PaidDate, opt => opt.MapFrom(src => src.PaidDate));
        }

        private static void ConfigureLoanInvestor()
        {
            Mapper.CreateMap<LoanInvestorEntity, LoanInvestorViewModel>()
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => String.Format("INV{0}", src.UserId)))
                .ForMember(dest => dest.OfferId, opt => opt.MapFrom(src => src.OfferId))
                .ForMember(dest => dest.RepaymentId, opt => opt.MapFrom(src => src.RepaymentId))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.DisplayName))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => CommonMethods.GetAccountFullName(src.Title,src.FirstName,src.LastName)))
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.LoanId))
                .ForMember(dest => dest.RepaymentAmount, opt => opt.MapFrom(src => GetRepaymentAllocation(src.Amount, src.Rate, src.Tenor)))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount.HasValue ? src.Amount.Value : 0))
                ;
        }

        //private static double GetRepaymentAllocation(decimal? acceptedAmount, decimal? loanAmount, decimal? repayment)
        private static double GetRepaymentAllocation(decimal? amount, decimal? rate, string Tenor)
        {
            if (!amount.HasValue || !rate.HasValue)
            {
                return 0;
            }

            return LoanRequestCalc.RepaymentAmount((double)amount, (double)rate, Tenor);
        }

        private static void ConfigureLoanRepayment()
        {
            Mapper.CreateMap<LoanRepaymentEntity, LoanRepaymentViewModel>()
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => String.Format("BRW{0}", src.AccountNumber)))
                .ForMember(dest => dest.RepaymentId, opt => opt.MapFrom(src => src.RepaymentId))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount.HasValue ? src.Amount.Value : 0))
                .ForMember(dest => dest.Comment, opt => opt.MapFrom(src => src.Comment))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.LoanId, opt => opt.MapFrom(src => src.RequestId))
                .ForMember(dest => dest.StatusRef, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.MonthlyRepayment, opt => opt.MapFrom(src => src.Repayment.HasValue ? src.Repayment.Value : 0))
                .ForMember(dest => dest.EndDate, opt => opt.MapFrom(src => GetMaturityDate(src.AcceptedDate, src.Terms)))
                .ForMember(dest => dest.MonthsTillMaturity, opt => opt.MapFrom(src => src.MonthsTillMaturity))
                .ForMember(dest => dest.Rate, opt => opt.MapFrom(src => src.Rate.HasValue ? src.Rate : 0))
                .ForMember(dest => dest.RepaymentDate, opt => opt.MapFrom(src => src.RepaymentDate.HasValue ? src.RepaymentDate : DateTime.MinValue))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.AcceptedDate.HasValue ? src.AcceptedDate.Value : DateTime.MinValue))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => CommonMethods.RepaymentDisplayName(src.Status)))
                .ForMember(dest => dest.LateDaysCount, opt => opt.MapFrom(src => GetLateRepaymentDaysCount(
                    src.RepaymentDate, src.LateRepaymentPaidDate)))
                ;
        }

        private static int GetLateRepaymentDaysCount(DateTime? due, DateTime? paidDate)
        {
            int lateDays = 0;

            if (!due.HasValue)
            {
                return 0;
            }

            DateTime sinNowDate = DateTimeExtenstion.SingaporeTime(DateTime.Now).Date;
            var dueDate = DateTimeExtenstion.SingaporeTime(due.Value).AddDays(1).Date;
            if (paidDate.HasValue)
            {
                var latePayementPaidSinDate = DateTimeExtenstion.SingaporeTime(paidDate.Value).Date;
                lateDays = latePayementPaidSinDate.Subtract(dueDate).Days + 1;
            }
            else if (dueDate < sinNowDate)
            {
                lateDays = sinNowDate.Subtract(dueDate).Days + 1;
            }

            return lateDays;
        }

        private static void ConfigUploadedFileMappings()
        {
            IFileUploadTask fileUploadTask = new FileUploadTask();

            Mapper.CreateMap<tbl_Files, FileModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Id))
                .ForMember(dest => dest.FileName, opt => opt.MapFrom(src => src.FileName))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type))
                .ForMember(dest => dest.userId, opt => opt.MapFrom(src => src.UserId))
                .ForMember(dest => dest.Comments, opt => opt.MapFrom(src => src.Comments))
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.RequestId))
                .ForMember(dest => dest.CreatedDate, opt => opt.MapFrom(src => src.CreatedDate))
                .ForMember(dest => dest.Url, opt => opt.MapFrom(src => fileUploadTask.GetFileUrl(src.UserId.ToString(),
                    src.Type,
                    src.FileName,
                    src.RequestId.HasValue ? src.RequestId.ToString() : null)));
        }

        private static void ConfigSecurityQuestionMapping()
        {
            Mapper.CreateMap<tbl_SecurityQuestions, SelectListItem>()
                .ForMember(dest => dest.Text, opt => opt.MapFrom(src => src.QuestionName))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.QuestionId));
        }

        private static void ConfigRepaymentMappings()
        {
            Mapper.CreateMap<RepaymentEntity, RepaymentViewModel>()
                .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src => src.DueDate))
                .ForMember(dest => dest.EndBalance, opt => opt.MapFrom(src => src.EndBalance))
                .ForMember(dest => dest.Interest, opt => opt.MapFrom(src => src.Interest))
                .ForMember(dest => dest.Payment, opt => opt.MapFrom(src => src.Payment))
                .ForMember(dest => dest.Principal, opt => opt.MapFrom(src => src.Principal))
                .ForMember(dest => dest.StartBalance, opt => opt.MapFrom(src => src.StartBalance))
                .ForMember(dest => dest.status, opt => opt.MapFrom(src => CommonMethods.RepaymentDisplayName((RepaymentPayStatus)src.Status)));
        }

        private static void ConfigureAuditLogMappings()
        {
            Mapper.CreateMap<tbl_AuditLog, AuditLogViewModel>()
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.LoanRequestId))
                .ForMember(dest => dest.Message, opt => opt.MapFrom(src => src.Message))
                .ForMember(dest => dest.TimeStamp, opt => opt.MapFrom(src => src.Timestamp))
                .ForMember(dest => dest.Reference, opt => opt.MapFrom(src => src.Reference))
                .ForMember(dest => dest.Username, opt => opt.MapFrom(src => src.UserName));
        }

        private static void ConfigureTransactionMapping()
        {
            Mapper.CreateMap<tbl_LoanTransactions, TransactionViewModel>()
                .ForMember(dest => dest.Balance, opt => opt.MapFrom(src => src.Balance.HasValue ? src.Balance.Value : 0))
                .ForMember(dest => dest.CreditedAmount, opt => opt.MapFrom(src => src.Credited.HasValue ? src.Credited.Value : 0))
                .ForMember(dest => dest.DebitedAmount, opt => opt.MapFrom(src => src.Debted.HasValue ? src.Debted.Value : 0))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.DateCreated))
                .ForMember(dest => dest.Description, opt => opt.MapFrom(src => src.PaymentTerms))
                .ForMember(dest => dest.TransactionId, opt => opt.MapFrom(src => src.TransactionId))
                .ForMember(dest => dest.TransactionReference, opt => opt.MapFrom(src => src.Reference))
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.Request_Id))
                ;
        }

        private static void ConfigureAccountDetailsMapping()
        {
            Mapper.CreateMap<tbl_AccountDetails, AccountViewModel>()
                  .ForMember(dest => dest.BusinessName, opt => opt.MapFrom(src => src.BusinessName))
                  .ForMember(dest => dest.BusinessOrganisation, opt => opt.MapFrom(src => src.BusinessOrganisation))
                  .ForMember(dest => dest.BusinessRegNumber, opt => opt.MapFrom(src => src.IC_CompanyRegistrationNumber))
                  .ForMember(dest => dest.DateOfIncorporation, opt => opt.MapFrom(src => src.DateofIncorporation))
                  .ForMember(dest => dest.SiccCode, opt => opt.MapFrom(src => src.SiccCode))
                  .ForMember(dest => dest.NatureOfBusiness,
                             opt => opt.MapFrom(src => src.tbl_NatureofBusinessList != null
                                                           ? src.tbl_NatureofBusinessList.NatureofBusinessName
                                                           : string.Empty))
                  .ForMember(dest => dest.ResgiteredAddress1,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.RegisteredAddress)[0]))
                  .ForMember(dest => dest.ResgiteredAddress2,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.RegisteredAddress)[1]))
                  .ForMember(dest => dest.ResgiteredAddressPostalCode,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.RegisteredAddress)[2]))
                  .ForMember(dest => dest.BusinessMailAddress1,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.BusinessMailingAddress)[0]))
                  .ForMember(dest => dest.BusinessMailAddress2,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.BusinessMailingAddress)[1]))
                  .ForMember(dest => dest.BusinessMailAddressPostalCode,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.BusinessMailingAddress)[2]));
        }

        private static void ConfigureLoanRequestMapping()
        {
            Mapper.CreateMap<tbl_LoanRequests, LoanRequestViewModel>()
                .ForMember(dest => dest.EIR, opt => opt.MapFrom(src => src.EIR.HasValue ? src.EIR.Value : 0))
                .ForMember(dest => dest.Rate, opt => opt.MapFrom(src => src.Rate.HasValue ? src.Rate.Value : 0))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount.HasValue ? src.Amount.Value : 0))
                .ForMember(dest => dest.LoanStatus, opt => opt.MapFrom(src => src.LoanStatus))
                .ForMember(dest => dest.PreliminaryStatus, opt => opt.MapFrom(src => src.PreliminaryLoanStatus))
                .ForMember(dest => dest.Headline, opt => opt.MapFrom(src => src.Headline))
                .ForMember(dest => dest.LoanPurposeAnswer, opt => opt.MapFrom(src => src.LoanPurposeDescription))
                .ForMember(dest => dest.AcceptedAmount,
                    opt => opt.MapFrom(src => src.FinalAmount.HasValue ? src.FinalAmount.Value : 0))
                .ForMember(dest => dest.SummaryCompanyPro,
                    opt => opt.MapFrom(src => src.SummaryCompanyPro))
                .ForMember(dest => dest.LoanPurpose, opt => opt.MapFrom(src => src.tbl_LoanPurposesList != null
                    ? src.tbl_LoanPurposesList.PurposeName
                    : string.Empty))
                .ForMember(dest => dest.PersonalGurantee,
                    opt => opt.MapFrom(src => src.IsPersonalGuarantee.HasValue ? src.IsPersonalGuarantee.Value : false))
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.RequestId))
                .ForMember(dest => dest.Term, opt => opt.MapFrom(src => src.Terms))
                .ForMember(dest => dest.VideoDescription, opt => opt.MapFrom(src => src.VideoDescription))
                .ForMember(dest => dest.DetailedCompanyProfile, opt => opt.MapFrom(src => src.DetailedCompanyPro))
                .ForMember(dest => dest.UserId, opt => opt.MapFrom(src => src.User_ID))
                .ForMember(dest => dest.LoanPurposeId, opt => opt.MapFrom(src => src.LoanPurpose_Id))
                .ForMember(dest => dest.NoOfEmployees, opt => opt.MapFrom(src => src.NumberOfEmployees))
                .ForMember(dest => dest.CompanyName,
                    opt =>
                        opt.MapFrom(
                            src =>
                                src.tbl_Users != null && src.tbl_Users.tbl_AccountDetails != null &&
                                src.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                                    ? src.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName
                                    : string.Empty));

        }

        private static void ConfigureLoanInvestmentMappings()
        {
            Mapper.CreateMap<LoanInvestmentEntity, LoanInvestmentViewModel>()
                .ForMember(dest => dest.LoanAmount, opt => opt.MapFrom(src => src.LoanAmount.HasValue ? src.LoanAmount : 0))
                .ForMember(dest => dest.Rate, opt => opt.MapFrom(src => src.Rate.HasValue ? src.Rate : 0))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.InterestRecieved, opt => opt.MapFrom(src => src.InterestReceived.HasValue ? src.InterestReceived : 0))
                .ForMember(dest => dest.LoanRequestId, opt => opt.MapFrom(src => src.LoanRequestId))
                .ForMember(dest => dest.MaturityDate, opt => opt.MapFrom(src => src.LoanStartDate.HasValue ? GetMaturityDate(src.LoanStartDate, src.Tenor) : src.LoanStartDate))
                .ForMember(dest => dest.MonthsTillMaturity, opt => opt.MapFrom(src => MonthsTillMaturity(src.LoanRequestId)))
                .ForMember(dest => dest.PrincipalRecieved, opt => opt.MapFrom(src => src.PrincipalRecevived.HasValue ? src.PrincipalRecevived : 0))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.LoanStartDate.HasValue ? src.LoanStartDate : DateTime.MinValue))
                .ForMember(dest => dest.Tenor, opt => opt.MapFrom(src => src.Tenor))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.LoanRequestStatus))
                .ForMember(dest => dest.offerIds, opt => opt.MapFrom(src => src.offerIds))
                .ForMember(dest => dest.LatestStatus, opt => opt.MapFrom(src => src.LatestRepaymentStatus));
        }

        private static void ConfigureLoanOfferMappings()
        {
            ILoanAllocationDA loanAllocation = new LoanAllocationDA();

            Mapper.CreateMap<tbl_Loanoffers, LoanOfferViewModel>()
                .ForMember(dest => dest.OfferId, opt => opt.MapFrom(src => src.OfferId))
                .ForMember(dest => dest.AmountAccepted, opt => opt.MapFrom(src => src.AcceptedAmount.HasValue ? src.AcceptedAmount : 0))
                .ForMember(dest => dest.AmountOfferd, opt => opt.MapFrom(src => src.OfferedAmount))
                .ForMember(dest => dest.RateAccepted, opt => opt.MapFrom(src => src.AcceptedRate.HasValue ? src.AcceptedRate : 0))
                .ForMember(dest => dest.RateOffered, opt => opt.MapFrom(src => src.OfferedRate))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.tbl_LoanRequests != null && src.tbl_LoanRequests.tbl_Users != null
                    && src.tbl_LoanRequests.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null ?
                    src.tbl_LoanRequests.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName : string.Empty))
                .ForMember(dest => dest.DateOfOffer, opt => opt.MapFrom(src => src.DateCreated))
                .ForMember(dest => dest.LoanRequestId, opt => opt.MapFrom(src => src.LoanRequest_Id))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => (LoanOfferStatus)src.OfferStatus))
                .ForMember(dest => dest.Tenor, opt => opt.MapFrom(src => src.tbl_LoanRequests != null ? src.tbl_LoanRequests.Terms : string.Empty))
            .ForMember(dest => dest.ReasonToReject, opt => opt.MapFrom(src => src.ReasonToReject))
            .ForMember(dest => dest.LoanRequestStatus, opt => opt.MapFrom(src => src.tbl_LoanRequests != null ? (LoanRequestStatus)src.tbl_LoanRequests.LoanStatus : LoanRequestStatus.Pending))
            .ForMember(dest => dest.LoanRate, opt => opt.MapFrom(src => src.tbl_LoanRequests != null ? src.tbl_LoanRequests.Rate : 0))
            .ForMember(dest => dest.RequestedLoanRate, opt => opt.MapFrom(src => 
                src.tbl_LoanRequests != null
                ? (src.tbl_LoanRequests.RequestedRate == null || src.tbl_LoanRequests.RequestedRate == 0 
                    ? src.tbl_LoanRequests.Rate : src.tbl_LoanRequests.RequestedRate )
                : 0))
            .ForMember(dest => dest.CanWithdraw, opt => opt.MapFrom(src => loanAllocation.CanWithdrawLoanOffer(src.LoanRequest_Id, src.OfferId)));
        }

        #region Need to be removed once db layer refactoring is done

        private static void ConfigureMoolahCoreIndicatorMappings()
        {

            Mapper.CreateMap<tbl_MoolahCore, MoolahCoreViewModel>()
                .ForMember(dest => dest.Profitability, opt => opt.MapFrom(src => src.NetprofitafterTax_LY))
                .ForMember(dest => dest.ProfitabilityPy, opt => opt.MapFrom(src => src.NetprofitafterTax_PY))
                .ForMember(dest => dest.CashFlowPositive, opt => opt.MapFrom(src => src.CashFlowfromOperations_LY))
                .ForMember(dest => dest.CashFlowPositivePy, opt => opt.MapFrom(src => src.CashFlowfromOperations_PY))
                .ForMember(dest => dest.DebtEquity, opt => opt.MapFrom(src => (double?)src.Debt_Equity_LY))
                .ForMember(dest => dest.InterestCoverageRatio, opt => opt.MapFrom(src => (double?)src.InterestCoverageRatio_LY))
                .ForMember(dest => dest.InterestCoverageRatioPY, opt => opt.MapFrom(src => (double?)src.InterestCoverageRatio_PY))
                .ForMember(dest => dest.DebtEquityPy, opt => opt.MapFrom(src => (double?)src.Debt_Equity_PY))
                .ForMember(dest => dest.CurrentRatio, opt => opt.MapFrom(src => (double?)src.CurrentRatio_LY))
                .ForMember(dest => dest.CurrentRatioPy, opt => opt.MapFrom(src => (double?)src.CurrentRatio_PY))
                .ForMember(dest => dest.DPPaymentGrade, opt => opt.MapFrom(src => src.DPCreditPaymentGrade))
                .ForMember(dest => dest.AnnualTurnOver, opt => opt.MapFrom(src => (double?)src.Turnovers_LY))
                .ForMember(dest => dest.AnnualTurnOverPy, opt => opt.MapFrom(src => (double?)src.Turnovers_PY))
                .ForMember(dest => dest.AverageCashBalance, opt => opt.MapFrom(src => (double?)src.AvgThreeMntCashBalBankAcc_LY))
                .ForMember(dest => dest.AverageCashBalancePy, opt => opt.MapFrom(src => (double?)src.AvgThreeMntCashBalBankAcc_PY))
                .ForMember(dest => dest.MoreThanOTEqual2YearsInBusiness, opt => opt.Ignore())
                .ForMember(dest => dest.LatestYear, opt => opt.MapFrom(src => src.LatestYear))
                .ForMember(dest => dest.PreviousYear, opt => opt.MapFrom(src => src.PreiviousYear))
                .ForMember(dest => dest.IsOutstandingLitigation, opt => opt.MapFrom(src => src.IsoutstandingLitigation.HasValue ? src.IsoutstandingLitigation.Value : false));

            Mapper.CreateMap<tbl_MoolahCoreVerification, MoolahCoreVerifyViewModel>()
                .ForMember(dest => dest.ProfitabilityVerified, opt => opt.MapFrom(src => src.NetProfitAfterTax_LY_Verification))
                .ForMember(dest => dest.Profitability_PY_Verify, opt => opt.MapFrom(src => src.NetProfitAfterTax_PY_Verification))
                .ForMember(dest => dest.CashFlowPositive_LY_Verified, opt => opt.MapFrom(src => src.CashFlowFromOperations_LY_Verification.HasValue && src.CashFlowFromOperations_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CashFlowPositive_PY_Verified, opt => opt.MapFrom(src => src.CashFlowFromOperations_PY_Verification.HasValue && src.CashFlowFromOperations_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.InterestCoverageRatio_LY_Verified, opt => opt.MapFrom(src => src.InterestCoverageRatio_LY_Verification.HasValue && src.InterestCoverageRatio_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.InterestCoverageRatio_PY_Verified, opt => opt.MapFrom(src => src.InterestCoverageRatio_PY_Verification.HasValue && src.InterestCoverageRatio_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.DebtEquity_LY_Verified, opt => opt.MapFrom(src => src.Debt_Equity_LY_Verification.HasValue && src.Debt_Equity_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.DebtEquity_PY_Verified, opt => opt.MapFrom(src => src.Debt_Equity_PY_Verification.HasValue && src.Debt_Equity_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CurrentRatio_LY_Verified, opt => opt.MapFrom(src => src.CurrentRatio_LY_Verification.HasValue && src.CurrentRatio_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CurrentRatio_PY_Verified, opt => opt.MapFrom(src => src.CurrentRatio_PY_Verification.HasValue && src.CurrentRatio_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AnnualTurnOver_LY_Verified, opt => opt.MapFrom(src => src.Turnover_LY_Verification.HasValue && src.Turnover_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AnnualTurnOver_PY_Verified, opt => opt.MapFrom(src => src.Turnover_PY_Verification.HasValue && src.Turnover_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.DPPaymentGradeVerified, opt => opt.MapFrom(src => src.DPCreditPaymentGrade_Verification.HasValue && src.DPCreditPaymentGrade_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.MoreThanOREqual2YearsInBusinessVerified, opt => opt.UseValue(true))
                .ForMember(dest => dest.FinancialStatementSubmitted, opt => opt.MapFrom(src => src.FinancialStatementSubmitted.HasValue ? src.FinancialStatementSubmitted.Value : false))
                .ForMember(dest => dest.FinancialStatementSubmitted_PY_Verified, opt => opt.MapFrom(src => src.FinancialStatementSubmitted_PY_Verification.HasValue ? src.FinancialStatementSubmitted_PY_Verification.Value : false))
                .ForMember(dest => dest.Audited, opt => opt.MapFrom(src => src.Audited))
                .ForMember(dest => dest.Audited_PY_Verified, opt => opt.MapFrom(src => src.Audited_PY_Verification))
                .ForMember(dest => dest.NoOfCreditEnquiries, opt => opt.MapFrom(src => src.NumberOfCreditEnquires))
                .ForMember(dest => dest.OutstandingLitigationVerified, opt => opt.MapFrom(src => src.OutstandingLitigation_Verification_.HasValue && src.OutstandingLitigation_Verification_ == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.NoOfCreditEnquiriesVerified, opt => opt.MapFrom(src => src.NumberOfCreditEnquires_Verification.HasValue && src.NumberOfCreditEnquires_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AverageCachBalance_LY_Verified, opt => opt.MapFrom(src => src.AvgThreeMntCashBalBankAcc_LY_Verification.HasValue && src.AvgThreeMntCashBalBankAcc_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AverageCachBalance_PY_Verified, opt => opt.MapFrom(src => src.AvgThreeMntCashBalBankAcc_PY_Verification.HasValue && src.AvgThreeMntCashBalBankAcc_PY_Verification == (int)VerificationStatus.Verified ? true : false));

        }

        private static void ConfigureMoolahPeriIndicatorMappings()
        {
            Mapper.CreateMap<tbl_MoolahPeri, MoolahPeriViewModel>()
                .ForMember(dest => dest.BusinessAwards, opt => opt.MapFrom(src => src.BusinessAwards))
                .ForMember(dest => dest.Accreditation, opt => opt.MapFrom(src => src.Accreditation))
                .ForMember(dest => dest.TradeAssociation, opt => opt.MapFrom(src => src.MTA))
                .ForMember(dest => dest.CommunityInitiatives, opt => opt.MapFrom(src => src.CommunityInitiative))
                .ForMember(dest => dest.SocialMediaFootprint, opt => opt.MapFrom(src => src.DigitalFootPrint))
                .ForMember(dest => dest.BiodataOfKeyPersonnel, opt => opt.MapFrom(src => src.BiodataOfDS))
                .ForMember(dest => dest.MoolahPerkId, opt => opt.MapFrom(src => src.MoolahPerk_Id))
                .ForMember(dest => dest.MoolahPerk, opt => opt.MapFrom(src => src.MoolahPerk_Custom))
                .ForMember(dest => dest.UpdateMoolahPost, opt => opt.MapFrom(src => src.UpdateMoolahPost))
                .ForMember(dest => dest.ResponseToQuestions, opt => opt.Ignore());

            Mapper.CreateMap<tbl_MoolahPeriVerification, MoolahPeriVerifyViewModel>()
                .ForMember(dest => dest.BusinessAwardsVerified, opt => opt.MapFrom(src => src.BussinessAwardsVerification.HasValue && src.BussinessAwardsVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AccreditationVerified, opt => opt.MapFrom(src => src.AcrediationVerification.HasValue && src.AcrediationVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.TradeAssociationVerified, opt => opt.MapFrom(src => src.MTAVerificationVerification.HasValue && src.MTAVerificationVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CommunityInitiativesVerified, opt => opt.MapFrom(src => src.CommunityVerification.HasValue && src.CommunityVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.SocialMediaFootprintVerified, opt => opt.MapFrom(src => src.DigitalFootPrintVerification.HasValue && src.DigitalFootPrintVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.BiodataOfKeyPersonnelVerified, opt => opt.MapFrom(src => src.BiodataofDSVerification.HasValue && src.BiodataofDSVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.MoolahPerkVerified, opt => opt.MapFrom(src => src.MoolahPerkVerification.HasValue && src.MoolahPerkVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.ResponseToQuestionsVerified, opt => opt.UseValue(true));
        }

        #endregion

        private static void ConfigureMoolahCoreIndicatorMappings1()
        {

            Mapper.CreateMap<MoolahConnect.Models.BusinessModels.MoolahCore, MoolahCoreViewModel>()
                .ForMember(dest => dest.Profitability, opt => opt.MapFrom(src => src.NetprofitafterTax_LY))
                .ForMember(dest => dest.ProfitabilityPy, opt => opt.MapFrom(src => src.NetprofitafterTax_PY))
                .ForMember(dest => dest.CashFlowPositive, opt => opt.MapFrom(src => src.CashFlowfromOperations_LY))
                .ForMember(dest => dest.CashFlowPositivePy, opt => opt.MapFrom(src => src.CashFlowfromOperations_PY))
                .ForMember(dest => dest.DebtEquity, opt => opt.MapFrom(src => (double?)src.Debt_Equity_LY))
                .ForMember(dest => dest.InterestCoverageRatio, opt => opt.MapFrom(src => (double?)src.InterestCoverageRatio_LY))
                .ForMember(dest => dest.InterestCoverageRatioPY, opt => opt.MapFrom(src => (double?)src.InterestCoverageRatio_PY))
                .ForMember(dest => dest.DebtEquityPy, opt => opt.MapFrom(src => (double?)src.Debt_Equity_PY))
                .ForMember(dest => dest.CurrentRatio, opt => opt.MapFrom(src => (double?)src.CurrentRatio_LY))
                .ForMember(dest => dest.CurrentRatioPy, opt => opt.MapFrom(src => (double?)src.CurrentRatio_PY))
                .ForMember(dest => dest.DPPaymentGrade, opt => opt.MapFrom(src => src.DPCreditPaymentGrade))
                .ForMember(dest => dest.AnnualTurnOver, opt => opt.MapFrom(src => (double?)src.Turnovers_LY))
                .ForMember(dest => dest.AnnualTurnOverPy, opt => opt.MapFrom(src => (double?)src.Turnovers_PY))
                .ForMember(dest => dest.AverageCashBalance, opt => opt.MapFrom(src => (double?)src.AvgThreeMntCashBalBankAcc_LY))
                .ForMember(dest => dest.AverageCashBalancePy, opt => opt.MapFrom(src => (double?)src.AvgThreeMntCashBalBankAcc_PY))
                .ForMember(dest => dest.MoreThanOTEqual2YearsInBusiness, opt => opt.Ignore())
                .ForMember(dest => dest.LatestYear, opt => opt.MapFrom(src => src.LatestYear))
                .ForMember(dest => dest.PreviousYear, opt => opt.MapFrom(src => src.PreiviousYear))
                .ForMember(dest => dest.IsOutstandingLitigation, opt => opt.MapFrom(src => src.IsoutstandingLitigation.HasValue ? src.IsoutstandingLitigation.Value : false));

            Mapper.CreateMap<MoolahCoreVerification, MoolahCoreVerifyViewModel>()
                .ForMember(dest => dest.ProfitabilityVerified, opt => opt.MapFrom(src => src.NetProfitAfterTax_LY_Verification))
                .ForMember(dest => dest.Profitability_PY_Verify, opt => opt.MapFrom(src => src.NetProfitAfterTax_PY_Verification))
                .ForMember(dest => dest.CashFlowPositive_LY_Verified, opt => opt.MapFrom(src => src.CashFlowFromOperations_LY_Verification.HasValue && src.CashFlowFromOperations_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CashFlowPositive_PY_Verified, opt => opt.MapFrom(src => src.CashFlowFromOperations_PY_Verification.HasValue && src.CashFlowFromOperations_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.InterestCoverageRatio_LY_Verified, opt => opt.MapFrom(src => src.InterestCoverageRatio_LY_Verification.HasValue && src.InterestCoverageRatio_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.InterestCoverageRatio_PY_Verified, opt => opt.MapFrom(src => src.InterestCoverageRatio_PY_Verification.HasValue && src.InterestCoverageRatio_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.DebtEquity_LY_Verified, opt => opt.MapFrom(src => src.Debt_Equity_LY_Verification.HasValue && src.Debt_Equity_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.DebtEquity_PY_Verified, opt => opt.MapFrom(src => src.Debt_Equity_PY_Verification.HasValue && src.Debt_Equity_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CurrentRatio_LY_Verified, opt => opt.MapFrom(src => src.CurrentRatio_LY_Verification.HasValue && src.CurrentRatio_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CurrentRatio_PY_Verified, opt => opt.MapFrom(src => src.CurrentRatio_PY_Verification.HasValue && src.CurrentRatio_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AnnualTurnOver_LY_Verified, opt => opt.MapFrom(src => src.Turnover_LY_Verification.HasValue && src.Turnover_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AnnualTurnOver_PY_Verified, opt => opt.MapFrom(src => src.Turnover_PY_Verification.HasValue && src.Turnover_PY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.DPPaymentGradeVerified, opt => opt.MapFrom(src => src.DPCreditPaymentGrade_Verification.HasValue && src.DPCreditPaymentGrade_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.MoreThanOREqual2YearsInBusinessVerified, opt => opt.UseValue(true))
                .ForMember(dest => dest.FinancialStatementSubmitted, opt => opt.MapFrom(src => src.FinancialStatementSubmitted.HasValue ? src.FinancialStatementSubmitted.Value : false))
                .ForMember(dest => dest.FinancialStatementSubmitted_PY_Verified, opt => opt.MapFrom(src => src.FinancialStatementSubmitted_PY_Verification.HasValue ? src.FinancialStatementSubmitted_PY_Verification.Value : false))
                .ForMember(dest => dest.Audited, opt => opt.MapFrom(src => src.Audited))
                .ForMember(dest => dest.Audited_PY_Verified, opt => opt.MapFrom(src => src.Audited_PY_Verification))
                .ForMember(dest => dest.NoOfCreditEnquiries, opt => opt.MapFrom(src => src.NumberOfCreditEnquires))
                .ForMember(dest => dest.OutstandingLitigationVerified, opt => opt.MapFrom(src => src.OutstandingLitigation_Verification_.HasValue && src.OutstandingLitigation_Verification_ == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.NoOfCreditEnquiriesVerified, opt => opt.MapFrom(src => src.NumberOfCreditEnquires_Verification.HasValue && src.NumberOfCreditEnquires_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AverageCachBalance_LY_Verified, opt => opt.MapFrom(src => src.AvgThreeMntCashBalBankAcc_LY_Verification.HasValue && src.AvgThreeMntCashBalBankAcc_LY_Verification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AverageCachBalance_PY_Verified, opt => opt.MapFrom(src => src.AvgThreeMntCashBalBankAcc_PY_Verification.HasValue && src.AvgThreeMntCashBalBankAcc_PY_Verification == (int)VerificationStatus.Verified ? true : false));

        }

        private static void ConfigureMoolahPeriIndicatorMappings1()
        {
            Mapper.CreateMap<MoolahPeri, MoolahPeriViewModel>()
                .ForMember(dest => dest.BusinessAwards, opt => opt.MapFrom(src => src.BusinessAwards))
                .ForMember(dest => dest.Accreditation, opt => opt.MapFrom(src => src.Accreditation))
                .ForMember(dest => dest.TradeAssociation, opt => opt.MapFrom(src => src.MTA))
                .ForMember(dest => dest.CommunityInitiatives, opt => opt.MapFrom(src => src.CommunityInitiative))
                .ForMember(dest => dest.SocialMediaFootprint, opt => opt.MapFrom(src => src.DigitalFootPrint))
                .ForMember(dest => dest.BiodataOfKeyPersonnel, opt => opt.MapFrom(src => src.BiodataOfDS))
                .ForMember(dest => dest.MoolahPerkId, opt => opt.MapFrom(src => src.MoolahPerk_Id))
                .ForMember(dest => dest.MoolahPerk, opt => opt.MapFrom(src => src.MoolahPerk_Custom))
                .ForMember(dest => dest.UpdateMoolahPost, opt => opt.MapFrom(src => src.UpdateMoolahPost))
                .ForMember(dest => dest.ResponseToQuestions, opt => opt.Ignore());

            Mapper.CreateMap<MoolahPeriVerification, MoolahPeriVerifyViewModel>()
                .ForMember(dest => dest.BusinessAwardsVerified, opt => opt.MapFrom(src => src.BussinessAwardsVerification.HasValue && src.BussinessAwardsVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.AccreditationVerified, opt => opt.MapFrom(src => src.AcrediationVerification.HasValue && src.AcrediationVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.TradeAssociationVerified, opt => opt.MapFrom(src => src.MTAVerificationVerification.HasValue && src.MTAVerificationVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.CommunityInitiativesVerified, opt => opt.MapFrom(src => src.CommunityVerification.HasValue && src.CommunityVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.SocialMediaFootprintVerified, opt => opt.MapFrom(src => src.DigitalFootPrintVerification.HasValue && src.DigitalFootPrintVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.BiodataOfKeyPersonnelVerified, opt => opt.MapFrom(src => src.BiodataofDSVerification.HasValue && src.BiodataofDSVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.MoolahPerkVerified, opt => opt.MapFrom(src => src.MoolahPerkVerification.HasValue && src.MoolahPerkVerification == (int)VerificationStatus.Verified ? true : false))
                .ForMember(dest => dest.ResponseToQuestionsVerified, opt => opt.UseValue(true));
        }

        private static void ConfigureRepaymentMessage()
        {
            Mapper.CreateMap<MoolahConnect.Models.BusinessModels.RepaymentMessage, RepaymentMessageViewModel>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src=>src.ID))
                .ForMember(dest => dest.CreatedDateTime, opt => opt.MapFrom(src => src.CreatedDate.ToDateTime()));
        }

        #region Loan mappings
        private static void ConfigureLoanMappings()
        {
            Mapper.CreateMap<LoanEntity, LoanViewModel>()
                .ForMember(dest => dest.RequestId, opt => opt.MapFrom(src => src.RequestId))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount.HasValue ? (double)src.Amount.Value : 0))
                .ForMember(dest => dest.EIR, opt => opt.MapFrom(src => src.EIR.HasValue ? (double)src.EIR.Value : 0))
                .ForMember(dest => dest.FinalAmount, opt => opt.MapFrom(src => src.FinalAmount.HasValue ? (double)src.FinalAmount.Value : 0))
                .ForMember(dest => dest.InterestPaidTillDate, opt => opt.MapFrom(src => src.InterestPaidTillDate.HasValue ? (double)src.InterestPaidTillDate.Value : 0))
                .ForMember(dest => dest.MaturityDate, opt => opt.MapFrom(src => GetMaturityDate(src.AcceptedDate, src.Terms)))
                .ForMember(dest => dest.MonthsTillMaturity, opt => opt.MapFrom(src => MonthsTillMaturity(src.RequestId)))
                .ForMember(dest => dest.MoolahFees, opt => opt.MapFrom(src => src.MoolahFees))
                .ForMember(dest => dest.PrincipalPaidTillDate, opt => opt.MapFrom(src => src.PrincipalPaidTillDate.HasValue ? (double)src.PrincipalPaidTillDate.Value : 0))
                .ForMember(dest => dest.Rate, opt => opt.MapFrom(src => src.Rate.HasValue ? (double)src.Rate.Value : 0))
                .ForMember(dest => dest.StartDate, opt => opt.MapFrom(src => src.AcceptedDate))
                .ForMember(dest => dest.Tenor, opt => opt.MapFrom(src => src.Terms))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.LoanStatus))
                .ForMember(dest => dest.AccountNumber, opt => opt.MapFrom(src => String.Format("BRW{0}", src.AccountNumber)))
                .ForMember(dest => dest.CompanyName, opt => opt.MapFrom(src => src.CompanyName))
                .ForMember(dest => dest.AmountPaidTillDate, opt => opt.MapFrom(src => src.AmountPaidTillDate.HasValue ? (double)src.AmountPaidTillDate.Value : 0))
                .ForMember(dest => dest.TotalInterestPayable, opt => opt.MapFrom(src => CommonMethods.GetTotalInterestPayable(src.Terms,
                    src.FinalAmount.HasValue ? (double)src.FinalAmount : 0, src.Rate.HasValue ? (double)src.Rate : 0)));
        }

        private static int MonthsTillMaturity(long requestId)
        {
            ILoanDetailsDA loan = new LoanDetailsDA();
            return loan.MonthsTillMaturity(requestId);
        }

        private static DateTime GetMaturityDate(DateTime? approvedDate, string terms)
        {
            if (!approvedDate.HasValue)
                return DateTime.MinValue;

            int termsInMonths = 0;
            int.TryParse(terms, out termsInMonths);

            return approvedDate.Value.AddMonths(termsInMonths);
        }
        #endregion

        private static void ConfigurePaymentMappings()
        {
            Mapper.CreateMap<tbl_LoanAmortization, PaymentViewModel>()
                .ForMember(dest => dest.LoanRequestId, opt => opt.MapFrom(src => src.LoanRequest_ID))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.EmiAmount.HasValue ? (double)src.EmiAmount.Value : 0))
                .ForMember(dest => dest.DueDate, opt => opt.MapFrom(src => src.EmiDate))
                .ForMember(dest => dest.Note, opt => opt.MapFrom(src => GetPaymentNote(src.EmiDate, src.PayStatus)))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => GetPaymentStatus(src.EmiDate, src.PayStatus, src.PaidAmount)));
        }

        private static string GetPaymentStatus(DateTime? dueDate, int? payStatus, decimal? paidAmount)
        {
            string status = string.Empty;

            if (payStatus.HasValue && (RepaymentPayStatus)payStatus.Value == RepaymentPayStatus.Late && paidAmount.HasValue && paidAmount.Value > 0)
            {
                return "Partially Paid";
            }

            if (payStatus.HasValue && (RepaymentPayStatus)payStatus.Value != RepaymentPayStatus.Pending)
                return ((RepaymentPayStatus)payStatus.Value).ToString();

            if (!dueDate.HasValue)
                return string.Empty;

            if (dueDate.Value.Month == DateTime.UtcNow.Month)
                return PaymentStatus.CURRENT;

            if (dueDate.Value > DateTime.Today)
                return PaymentStatus.COMING_DUE;

            return PaymentStatus.LATE;
        }

        private static string GetPaymentNote(DateTime? dueDate, int? payStatus)
        {
            string status = string.Empty;

            if (payStatus.HasValue && ((RepaymentPayStatus)payStatus.Value) == RepaymentPayStatus.Paid)
                return PaymentNote.PAID;

            if (payStatus.HasValue && ((RepaymentPayStatus)payStatus.Value) == RepaymentPayStatus.Late)
                return PaymentNote.LATE;

            if (payStatus.HasValue && ((RepaymentPayStatus)payStatus.Value) == RepaymentPayStatus.Default)
                return String.Empty;

            if (!dueDate.HasValue)
                return string.Empty;

            if (dueDate.Value.Month == DateTime.UtcNow.Month)
                return PaymentNote.CURRENT;

            if (dueDate.Value > DateTime.Today)
                return PaymentNote.COMING_DUE;

            return PaymentNote.LATE;
        }

        private static void ConfigInvestorProfileMapping()
        {
            Mapper.CreateMap<tbl_AccountDetails, MoolahConnectClassLibrary.Models.ViewModels.InvestorProfile>()
                  .ForMember(dest => dest.InvestorType, opt => opt.MapFrom(src => src.tbl_Users.InvestorType))
                  .ForMember(dest => dest.BusinessRegNumber, opt => opt.MapFrom(src => src.IC_CompanyRegistrationNumber))
                  .ForMember(dest => dest.OfficialCorrespondenceEmail,
                             opt => opt.MapFrom(src => src.OptionalContactnumber))
                  .ForMember(dest => dest.DirectLine, opt => opt.MapFrom(src => src.Main_OfficeContactnumber))
                  .ForMember(dest => dest.Bank, opt => opt.MapFrom(src => src.tbl_BanksList.BankName))
                  .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.FirstName))
                  .ForMember(dest => dest.NricNumber, opt => opt.MapFrom(src => src.NRIC_Number))
                  .ForMember(dest => dest.PositionHeld, opt => opt.MapFrom(src => src.RoleJobTitle))
                  .ForMember(dest => dest.MainContactNumber, opt => opt.MapFrom(src => src.Main_OfficeContactnumber))

                  .ForMember(dest => dest.RegisteredAddress1,
                             opt => opt.MapFrom(src => CommonMethods.LoadAddressField(src.RegisteredAddress)[0]))
                  .ForMember(dest => dest.RegisteredAddress2,
                             opt => opt.MapFrom(src => CommonMethods.LoadAddressField(src.RegisteredAddress)[1]))
                  .ForMember(dest => dest.RegisteredAddressPostalCode,
                             opt => opt.MapFrom(src => CommonMethods.LoadAddressField(src.RegisteredAddress)[2]))

                  .ForMember(dest => dest.BusinessMailingAddress1,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.BusinessMailingAddress)[0]))
                  .ForMember(dest => dest.BusinessMailingAddress2,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.BusinessMailingAddress)[1]))
                  .ForMember(dest => dest.BusinessMailingAddressPostalCode,
                             opt =>
                             opt.MapFrom(src => CommonMethods.LoadAddressField(src.BusinessMailingAddress)[2]))

                  .ForMember(dest => dest.ResidentialAddress1,
                             opt => opt.MapFrom(src => CommonMethods.LoadAddressField(src.ResidentialAddress)[0]))
                  .ForMember(dest => dest.ResidentialAddress2,
                             opt => opt.MapFrom(src => CommonMethods.LoadAddressField(src.ResidentialAddress)[1]))
                  .ForMember(dest => dest.ResidentialAddressPostalCode,
                             opt => opt.MapFrom(src => CommonMethods.LoadAddressField(src.ResidentialAddress)[2]));

        }
        private static void ConfigLoanRequestMapping()
        {
            Mapper.CreateMap<LoanRequestDescription, LoanRequest>()
             .ForMember(a => a.Amount, m => m.MapFrom(opt => opt.Amount))
             .ForMember(a => a.Terms, m => m.MapFrom(opt => opt.Term))
             .ForMember(a => a.Rate, m => m.MapFrom(opt => opt.Rate))
             .ForMember(a => a.IsPersonalGuarantee, m => m.MapFrom(opt => opt.IspersonalGuarantee))
             .ForMember(a => a.LoanPurposeDescription, m => m.MapFrom(opt => opt.LoanPurposeDescription))
             .ForMember(a => a.LoanPurposeDescription, m => m.MapFrom(opt => opt.LoanPurposeDescription))
             .ForMember(a => a.DetailedCompanyPro, m => m.MapFrom(opt => opt.DetailedCompanyPro))
             .ForMember(a => a.SummaryCompanyPro, m => m.MapFrom(opt => opt.SummaryCompanyPro))
             .ForMember(a => a.NumberOfEmployees, m => m.MapFrom(opt => opt.NumberOfEmployees))
             .ForMember(a => a.Headline, m => m.MapFrom(opt => opt.Headline))
             .ForMember(a => a.RequestId, m => m.MapFrom(opt => opt.RequestId))
             .ForMember(a => a.PersonalGuaranteeInfos, m => m.MapFrom(opt => opt.PersonGuarantees));

          //  Mapper.CreateMap<LoanRequest, LoanRequestDescription>()
          // .ForMember(a => a.Term, m => m.MapFrom(opt => opt.Terms))
          // .ForMember(a => a.IspersonalGuarantee, m => m.MapFrom(opt => opt.IsPersonalGuarantee))
          //.ForMember(a => a.PersonGuarantees, m => m.MapFrom(opt => opt.PersonalGuaranteeInfos));

            Mapper.CreateMap<PersonGuarantee, PersonalGuaranteeInfo>()
                .ForMember(a => a.Email, m => m.MapFrom(opt => opt.Email))
                .ForMember(a => a.NRIC_Passport, m => m.MapFrom(opt => opt.NRICNo))
                .ForMember(a => a.NameAsinNRIC, m => m.MapFrom(opt => opt.NameInNRIC))
                .ForMember(a => a.Nationality, m => m.MapFrom(opt => opt.Nationality))
                .ForMember(a => a.PassportNumber, m => m.MapFrom(opt => opt.PassportNo))
                .ForMember(a => a.Postalcode, m => m.MapFrom(opt => opt.PostalCode))
                .ForMember(a => a.Reasons, m => m.MapFrom(opt => opt.Reason))
                .ForMember(a => a.InfoId, m => m.MapFrom(opt => opt.id))
                .ForMember(a => a.ResidentialAddress,
                    m => m.MapFrom(opt => ContollerExtensions.AppendAddressField(opt.ResidentialAddress1,
                        opt.ResidentialAddress2, "")))
                .ForMember(a => a.Telephone, m => m.MapFrom(opt => opt.Telephone));

            Mapper.CreateMap<PersonalGuaranteeInfo, PersonGuarantee>()
              .ForMember(a => a.NRICNo, m => m.MapFrom(opt => opt.NRIC_Passport))
              .ForMember(a => a.NameInNRIC, m => m.MapFrom(opt => opt.NameAsinNRIC))
              .ForMember(a => a.PassportNo, m => m.MapFrom(opt => opt.PassportNumber))
              .ForMember(a => a.PostalCode, m => m.MapFrom(opt => opt.Postalcode))
              .ForMember(a => a.Reason, m => m.MapFrom(opt => opt.Reasons))
              .ForMember(a => a.id, m => m.MapFrom(opt => opt.InfoId))
              .ForMember(a => a.ResidentialAddress1,m => m.MapFrom(opt => CommonMethods.LoadAddressField(opt.ResidentialAddress)[0]))
              .ForMember(a => a.ResidentialAddress2,m => m.MapFrom(opt => CommonMethods.LoadAddressField(opt.ResidentialAddress)[1]))
              .ForMember(a => a.Telephone, m => m.MapFrom(opt => opt.Telephone));

            Mapper.CreateMap<LoanRequestMoolahCore, MoolahConnect.Models.BusinessModels.MoolahCore>()
                .ForMember(a => a.AvgThreeMntCashBalBankAcc_LY, m => m.MapFrom(opt => opt.AvgThreeMntCashBalBankAcc_LY))
                .ForMember(a => a.AvgThreeMntCashBalBankAcc_PY, m => m.MapFrom(opt => opt.AvgThreeMntCashBalBankAcc_PY))
                .ForMember(a => a.CashFlowfromOperations_LY, m => m.MapFrom(opt => opt.CashFlowfromOperations_LY))
                .ForMember(a => a.CashFlowfromOperations_PY, m => m.MapFrom(opt => opt.CashFlowfromOperations_PY))
                .ForMember(a => a.CurrentAssests_LY, m => m.MapFrom(opt => opt.CurrentAssests_LY))
                .ForMember(a => a.CurrentAssests_PY, m => m.MapFrom(opt => opt.CurrentAssests_PY))
                .ForMember(a => a.CurrentLiabilities_LY, m => m.MapFrom(opt => opt.CurrentLiabilities_LY))
                .ForMember(a => a.CurrentLiabilities_PY, m => m.MapFrom(opt => opt.CurrentLiabilities_PY))
                .ForMember(a => a.CurrentRatio_LY, m => m.MapFrom(opt => opt.CurrentRatio_LY))
                .ForMember(a => a.CurrentRatio_PY, m => m.MapFrom(opt => opt.CurrentRatio_PY))
                .ForMember(a => a.DPCreditPaymentGrade, m => m.MapFrom(opt => opt.DPCreditPaymentGrade))
                .ForMember(a => a.Debt_Equity_LY, m => m.MapFrom(opt => opt.Debt_Equity_LY))
                .ForMember(a => a.Debt_Equity_PY, m => m.MapFrom(opt => opt.Debt_Equity_PY))
                .ForMember(a => a.InterestCoverageRatio_LY, m => m.MapFrom(opt => opt.InterestCoverageRatio_LY))
                .ForMember(a => a.InterestCoverageRatio_PY, m => m.MapFrom(opt => opt.InterestCoverageRatio_PY))
                .ForMember(a => a.InterestExpense_LY, m => m.MapFrom(opt => opt.InterestExpense_LY))
                .ForMember(a => a.InterestExpense_PY, m => m.MapFrom(opt => opt.InterestExpense_PY))
                .ForMember(a => a.IsoutstandingLitigation, m => m.MapFrom(opt => opt.IsoutstandingLitigation))
                .ForMember(a => a.LoanRequest_Id, m => m.MapFrom(opt => opt.LoanRequest_Id))
                .ForMember(a => a.MoolahCoreId, m => m.MapFrom(opt => opt.ID))
                .ForMember(a => a.LatestYear, m => m.MapFrom(opt => opt.LatestYear));



        }

    }
}