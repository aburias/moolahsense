﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using Autofac;
using Autofac.Integration.Mvc;
using MoolahConnect.Data.Infrastructure;
using MoolahConnect.Data.Interfaces;
using MoolahConnect.Data.Repositories;
using MoolahConnect.Services;
using MoolahConnect.Services.Implemantation1;
using MoolahConnect.Services.Interfaces1;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnectnew.Infrastructure;

namespace MoolahConnectnew.App_Start
{
    public class Bootstrapper
    {
        public static void Run()
        {
            SetAutofacContainer();
            //Configure AutoMapper
          
        }
        private static void SetAutofacContainer()
        {
            var builder = new ContainerBuilder();
            builder.RegisterControllers(Assembly.GetExecutingAssembly());
            builder.RegisterType<UnitOfWork>().As<IUnitOfWork>().InstancePerHttpRequest();
            builder.RegisterType<DatabaseFactory>().As<IDatabaseFactory>().InstancePerHttpRequest();
            builder.RegisterType<LoanAllocationTask>().As<ILoanAllocationTask>().InstancePerHttpRequest();
            builder.RegisterType<IssuerService>().As<IIssuerService>().InstancePerHttpRequest();
            builder.RegisterType<PersonGuarantorService>().As<IPersonGuarantorService>().InstancePerHttpRequest();
            builder.RegisterType<MoolahCoreService>().As<IMoolahCoreService>().InstancePerHttpRequest();
            builder.RegisterType<MoolahPeriService>().As<IMoolahPeriService>().InstancePerHttpRequest();
            builder.RegisterType<OutStandingLitigationService>().As<IOutStandingLitigationService>().InstancePerHttpRequest();
            builder.RegisterType<AccountDetailService>().As<IAccountDetailService>().InstancePerHttpRequest();
            builder.RegisterType<AccountDetailRepository>().As<IAccountDetailRepository>().InstancePerHttpRequest();
            builder.RegisterType<CurrentUser>().As<ICurrentUser>().InstancePerHttpRequest();
            builder.RegisterType<UserService>().As<IUserService>().InstancePerHttpRequest();
            builder.RegisterType<SessionService>().As<ISessionService>().InstancePerHttpRequest();
            builder.RegisterType<UserRepository>().As<IUserRepository>().InstancePerHttpRequest();
            builder.RegisterType<LoanRequestService>().As<ILoanRequestService>().InstancePerHttpRequest();
            builder.RegisterType<LoanRequestRepository>().As<ILoanRequestRepository>().InstancePerHttpRequest();
            builder.RegisterType<LoanPurposeRepository>().As<ILoanPurposeRepository>().InstancePerHttpRequest();
            builder.RegisterType<LoanPurposeService>().As<ILoanPurposeService>().InstancePerHttpRequest();
            builder.RegisterType<MoolahPerkLenderRepository>().As<IMoolahPerkLenderRepository>().InstancePerHttpRequest();
            builder.RegisterType<MoolahPerkLenderService>().As<IMoolahPerkLenderService>().InstancePerHttpRequest();
            builder.RegisterType<PersonGuarantorService>().As<IPersonGuarantorService>().InstancePerHttpRequest();
            builder.RegisterType<RepaymentMessageService>().As<IRepaymentMessageService>().InstancePerHttpRequest();
            builder.RegisterType<PersonalGuarantorRepository>().As<IPersonalGuarantorRepository>().InstancePerHttpRequest();

            builder.RegisterType<InvestorService>().As<IInvestorService>().InstancePerHttpRequest();
            builder.RegisterType<AccountDetailRepository>().As<IAccountDetailRepository>().InstancePerHttpRequest();
            builder.RegisterType<BankRepository>().As<IBankRepository>().InstancePerHttpRequest();
            builder.RegisterType<UserSecurityQuestionRepository>().As<IUserSecurityQuestion>().InstancePerHttpRequest();
            builder.RegisterType<MoolahCoreRepository>().As<IMoolahCoreRepository>().InstancePerHttpRequest();
            builder.RegisterType<MoolahCoreService>().As<IMoolahCoreService>().InstancePerHttpRequest();
            builder.RegisterType<RepaymentMessageRepository>().As<IRepaymentMessageRepository>().InstancePerHttpRequest();
            builder.RegisterType<SecurityQuestionRepository>()
                .As<ISecurityQuestionRepository>()
                .InstancePerHttpRequest();
            builder.RegisterType<KnowledgeAssesmentParentRepository>()
                .As<IKnowledgeAssesmentParentRepository>()
                .InstancePerHttpRequest();
      
            builder.RegisterFilterProvider();
            IContainer container = builder.Build();
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}