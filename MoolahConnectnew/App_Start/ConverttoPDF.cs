﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.Web.UI.HtmlControls;


public static class ControllerPDF
{
    #region Render Partial to String
    //public static string RenderPartialViewToString(this Controller controller)
    //{
    //    return RenderPartialViewToString(null, null);
    //}


    public static void RenderPartialViewToString(this Controller controller, string viewName,string Filename)
    {
        RenderPartialViewToString(controller, viewName, null, Filename);
    }


    //public static string RenderPartialViewToString(this Controller controller, object model)
    //{
    //    return RenderPartialViewToString(controller, null, model);
    //}


    public static void RenderPartialViewToString(this Controller controller, string viewName, object model, string Filename)
    {
        if (string.IsNullOrEmpty(viewName))
            viewName = controller.ControllerContext.RouteData.GetRequiredString("action");


        controller.ViewData.Model = model;


        using (StringWriter sw = new StringWriter())
        {
            ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
            ViewContext viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
            viewResult.View.Render(viewContext, sw);

            madepdfattacment(sw.GetStringBuilder().ToString(), Filename);
            //return sw.GetStringBuilder().ToString();
        }
    }
    #endregion


    #region Render View to String






    public static void madepdfattacment(String PdfContents,string Filename)
    {
        //HttpContext.Current.Response.ContentType = "application/pdf";
        //HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=LeadsDetail.pdf");
        //HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        ////HtmlTextWriter hw = new HtmlTextWriter(sw);
        //Document pdfDoc = new Document(PageSize.A1, 7f, 7f, 7f, 0f);

        //HTMLWorker htmlparser = new HTMLWorker(pdfDoc);

        //PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);
        //StringReader sr = new StringReader(PdfContents);


        //pdfDoc.Open();
        //HttpContext.Current.Response.Flush();
        //htmlparser.Parse(sr);
        //pdfDoc.Close();
        //HttpContext.Current.Response.Write(pdfDoc);
        //HttpContext.Current.Response.End();


        var percentageofcolumns = PdfContents.Substring(PdfContents.IndexOf('Å') + 1, PdfContents.LastIndexOf("Å"));
        string[] colmnswidth = percentageofcolumns.Split(',');


        // var test = PdfContents.StartsWith("width=");
        HttpContext.Current.Response.ContentType = "application/pdf";
        HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=" + Filename + ".pdf");
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        //HtmlTextWriter hw = new HtmlTextWriter(sw);
        Document pdfDoc = new Document(PageSize.A4, 10.0F, 10.0F, 100.0F, 0.0F);

        //iTextSharp.text.html.simpleparser.StyleSheet styles = new iTextSharp.text.html.simpleparser.StyleSheet();
        //////////////////varun/////////////////////
        iTextSharp.text.Rectangle rect = PageSize.LETTER;
        float pagewidth = rect.Width;


        float[] xy = new float[colmnswidth.Length];
        for (int i = 0; i < colmnswidth.Length; i++)
        {
            if (colmnswidth[i].Contains('Å'))
            {
                xy[i] = (float)(Convert.ToInt32(colmnswidth[i].Substring(0, colmnswidth[i].IndexOf("Å"))));
            }
            else
            {
                xy[i] = (float)((Convert.ToInt32(colmnswidth[i])));
            }
        }
        //styles.LoadTagStyle("table", "width", "500px");
        PdfWriter.GetInstance(pdfDoc, HttpContext.Current.Response.OutputStream);
        pdfDoc.Open();
        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);



        List<IElement> objects = HTMLWorker.ParseToList(new StringReader(PdfContents), null);

        foreach (IElement element in objects)
        {
            PdfPTable tbl = element as PdfPTable;

            if (tbl != null)
            {
                tbl.SetWidthPercentage(xy, rect);                  
            }
            pdfDoc.Add(element);
            /////////////////end varun/////////////////
        }

        StringReader sr = new StringReader(PdfContents);
        HttpContext.Current.Response.Flush();
        // htmlparser.Parse(sr);
        pdfDoc.Close();
        HttpContext.Current.Response.Write(pdfDoc);
        HttpContext.Current.Response.End();


    }

    #endregion
}

