﻿using System.Web;
using System.Web.Optimization;

namespace MoolahConnectnew
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/default").Include(
                "~/Scripts/default.js"));
            bundles.Add(new ScriptBundle("~/bundles/default").Include(
                "~/Scripts/config.js"));
            bundles.Add(new ScriptBundle("~/bundles/jqueryEdited").Include(
                        "~/Scripts/Customjs/jquery-1.7.1*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryValidate").Include(
                       "~/Scripts/jquery.validate.js",
                       "~/Scripts/jquery.validate.unobtrusive.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*",
                        "~/Scripts/jquery.validate*", "~/Content/js/jquery.uploadify.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/loading").Include(
                "~/Scripts/jquery.showLoading*"));

            bundles.Add(new ScriptBundle("~/bundles/qtip").Include(
                "~/Scripts/qTip/jquery.qtip*"));

            bundles.Add(new ScriptBundle("~/bundles/uploadify").Include(
                "~/Scripts/jquery.uploadifive*"));

            bundles.Add(new ScriptBundle("~/bundles/idleUser").Include("~/Scripts/IdleUser/idleUser.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootbox").Include("~/Scripts/bootbox*"));
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include("~/Scripts/bootstrap*"));
            bundles.Add(new ScriptBundle("~/bundles/autoNumeric").Include("~/Scripts/autoNumeric.js"));
            bundles.Add(new ScriptBundle("~/bundles/passstrength").Include("~/Scripts/jquery.passstrength*"));
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/css/JqueryUI").Include("~/Content/css/jquery-ui-{version}.custom.css"));
            bundles.Add(new StyleBundle("~/Content/css/Smooth/JqueryUI").Include("~/Content/Admin/Css/jquery-ui-smooth.css"));
            bundles.Add(new StyleBundle("~/Content/css/Site").Include("~/Content/css/Site.css"));
            bundles.Add(new StyleBundle("~/Content/css/Uploadify").Include("~/Content/css/uploadifive.css"));
        }
    }
}