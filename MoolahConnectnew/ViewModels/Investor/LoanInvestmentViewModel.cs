﻿using MoolahConnect.Util.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Investor
{
    public class LoanInvestmentViewModel
    {
        public LoanInvestmentViewModel()
        {
            offerIds = new List<int>();
        }

        public int OfferID { get; set; }
        public long LoanRequestId { get; set; }
        public string CompanyName { get; set; }
        public string Tenor { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? MaturityDate { get; set; }
        public decimal LoanAmount { get; set; }
        public decimal Rate { get; set; }
        public int MonthsTillMaturity { get; set; }
        public decimal PrincipalRecieved { get; set; }
        public decimal InterestRecieved { get; set; }
        public LoanRequestStatus Status { get; set; }
        public RepaymentPayStatus LatestStatus { get; set; }

        public List<int> offerIds { get; set; }
    }
}