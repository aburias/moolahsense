﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoolahConnect.Util.Enums;

namespace MoolahConnectnew.ViewModels.Investor
{
    public class LoanOfferViewModel
    {
        public int OfferId { get; set; }
        public long LoanRequestId { get; set; }
        public string CompanyName { get; set; }
        public DateTime DateOfOffer { get; set; }
        public string Tenor { get; set; }
        public decimal RateOffered { get; set; }
        public decimal RateAccepted { get; set; }
        public decimal AmountOfferd { get; set; }
        public decimal AmountAccepted { get; set; }
        public string Status { get; set; }
        public string ReasonToReject { get; set; }
        public decimal LoanRate { get; set; }
        public decimal RequestedLoanRate { get; set; }
        public LoanRequestStatus LoanRequestStatus { get; set; }
        public bool CanWithdraw { get; set; }
    }
}