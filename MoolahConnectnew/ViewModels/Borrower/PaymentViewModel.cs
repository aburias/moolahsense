﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Borrower
{
    public class PaymentViewModel
    {
        public long LoanRequestId { get; set; }
        public double Amount { get; set; }
        public DateTime? DueDate { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }
    }
}