﻿using MoolahConnect.Util.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Borrower
{
    public class LoanViewModel
    {
        public long RequestId { get; set; }
        public double Amount { get; set; }
        public double FinalAmount { get; set; }
        public string Tenor { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime MaturityDate { get; set; }
        public int MonthsTillMaturity { get; set; }
        public double MoolahFees { get; set; }
        public double Rate { get; set; }
        public double EIR { get; set; }
        public double PrincipalPaidTillDate { get; set; }
        public double InterestPaidTillDate { get; set; }
        public double TotalInterestPayable { get; set; }
        public Guid UserId { get; set; }
        public LoanRequestStatus Status { get; set; }

        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public double AmountPaidTillDate { get; set; }

        public string FinalIssuerContractName { get; set; }
        public string FinalGuaranorContractName { get; set; }
    }
}