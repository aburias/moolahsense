﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class UserAccountViewModel
    {
        public string AccountNumber { get; set; }
        public string NRIC { get; set; }
        public string BusinessRegNumber { get; set; }
        public string FullName { get; set; }
    }
}