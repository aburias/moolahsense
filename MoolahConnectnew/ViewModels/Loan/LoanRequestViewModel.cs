﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class LoanRequestViewModel
    {
        public LoanRequestViewModel()
        {
            
        }

        public long RequestId { get; set; }
        public int LoanPurposeId { get; set; }

        public decimal Amount { get; set; }

        [DisplayName("Amount")]
        public decimal AcceptedAmount { get; set; }

        [DisplayName("Tenor (Months)")]
        public string Term { get; set; }

        [DisplayName("Target EIR")]
        public decimal EIR { get; set; }

        [DisplayName("Target Rate (% p.a.)")]
        public decimal Rate { get; set; }

        [DisplayName("Video Description")]
        public string VideoDescription { get; set; }

        [DisplayName("Detailed Company Profile")]
        public string DetailedCompanyProfile { get; set; }

        [DisplayName("Headline")]
        public string Headline { get; set; }

        [DisplayName("Summary Company Profile")]
        public string SummaryCompanyPro { get; set; }

        [DisplayName("Purpose")]
        public string LoanPurpose { get; set; }

        
        public string PurchaseValue { get; set; }

        [DisplayName("Personal Guarantee")]
        public bool PersonalGurantee { get; set; }

        [DisplayName("Moolah Sense Comments")]
        public string MoolahSenseComments { get; set; }

        public int UserId { get; set; }

        public string CompanyName { get; set; }
        public string Reference { get; set; }
        public string RegisteredAddress { get; set; }
        public decimal? AverageRate { get; set; }
        public int Days { get; set; }
        public int Hours { get; set; }
        public int Minutes { get; set; }
        public decimal? Funded { get; set; }

        public string LoanPurposeAnswer { get; set; }

        [DisplayName("Number of Employees")]
        public int NoOfEmployees { get; set; }

        public int LoanStatus { get; set; }
        public int PreliminaryStatus { get; set; }

        public DateTime? PublishedDate { get; set; }
    }
}