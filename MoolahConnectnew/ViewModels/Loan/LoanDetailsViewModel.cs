﻿using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectnew.ViewModels.Borrower;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoolahConnectnew.ViewModels.Loan;

namespace MoolahConnectnew.ViewModels
{
    public class LoanDetailsViewModel
    {
        public LoanDetailsViewModel()
        {
            Account = new AccountViewModel();
            Loan = new LoanRequestViewModel();
            MoolahCore = new LoanDetailsMoolahCore();
            MoolahPeri = new LoanDetailsMoolahPeri();
            loanSummary = new InvestorLoanOffer();
            LoanDocuments = new LoanDocumentsViewModel();
            RepaymentMessages = new List<RepaymentMessageViewModel>();
            
        }

        public InvestorLoanOffer loanSummary { get; set; }
        public AccountViewModel Account { get; set; }
        public LoanRequestViewModel Loan { get; set; }
        public LoanDetailsMoolahCore MoolahCore {get;set;}
        public LoanDetailsMoolahPeri MoolahPeri { get; set; }
        public LoanDocumentsViewModel LoanDocuments { get; set; }
        public IEnumerable<RepaymentMessageViewModel> RepaymentMessages {get;set;}
    }

    public class LoanDetailsMoolahCore
    {
        public LoanDetailsMoolahCore()
        {
            MoolahCore = new MoolahCoreViewModel();
            MoolahCoreVerify = new MoolahCoreVerifyViewModel();
        }

        public MoolahCoreViewModel MoolahCore { get; set; }
        public MoolahCoreVerifyViewModel MoolahCoreVerify { get; set; }
    }

    public class LoanDetailsMoolahPeri
    {
        public LoanDetailsMoolahPeri()
        {
            MoolahPeri = new MoolahPeriViewModel();
            MoolahPeriVerify = new MoolahPeriVerifyViewModel();
        }

        public MoolahPeriViewModel MoolahPeri { get; set; }
        public MoolahPeriVerifyViewModel MoolahPeriVerify { get; set; }
    }
}