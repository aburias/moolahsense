﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoolahConnectClassLibrary.Models.ViewModels;

namespace MoolahConnectnew.ViewModels.Loan
{
    public class RepaymentMessageViewModel
    {
        public RepaymentMessageViewModel()
        {
            State = EntityState.Update;
        }

        public long Id { set; get; }
        public string Message { get; set; }
        public EntityState State { get; set; }
        public long LoanRequestId { get; set; }
        public string CreatedDateTime { get; set; }

    }
}