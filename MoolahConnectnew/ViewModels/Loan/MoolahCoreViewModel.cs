﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class MoolahCoreViewModel
    {
        [DisplayName("Profitable")]
        public decimal? Profitability { get; set; }

        [DisplayName("DP Credit Rating")]
        public int DPPaymentGrade { get; set; }

        [DisplayName("Cash Flow From Operations")]
        public decimal? CashFlowPositive { get; set; }

        [DisplayName("Debt/Equity Ratio")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double? DebtEquity { get; set; }

        [DisplayName("Interest Coverage Ratio")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double? InterestCoverageRatio { get; set; }

        [DisplayName("Current Ratio")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double? CurrentRatio { get; set; }

        [DisplayName("Turnover")]
        public double? AnnualTurnOver { get; set; }

        public bool MoreThanOTEqual2YearsInBusiness { get; set; }

        [DisplayName("Financial Year")]
        public string LatestYear { get; set; }

        [DisplayName("Outstanding Litigation")]
        public bool IsOutstandingLitigation { get; set; }

        [DisplayName("Average Cash Balance (3 Months)"), DisplayFormat(DataFormatString = "S {0:C}", ApplyFormatInEditMode = true)]
        public double? AverageCashBalance { get; set; }

        [DisplayName("Debt/EBITDA Ratio")]
        public decimal? DebtEBITDARatio_LY { get; set; }

        //previous year


        [DisplayName("Profitable")]
        public decimal? ProfitabilityPy { get; set; }

        [DisplayName("Cash Flow From Operations")]
        public decimal? CashFlowPositivePy { get; set; }

        [DisplayName("Debt/Equity Ratio")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double? DebtEquityPy { get; set; }

        [DisplayName("Interest Coverage Ratio")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double? InterestCoverageRatioPY { get; set; }

        [DisplayName("Current Ratio")]
        [DisplayFormat(DataFormatString = "{0:n2}", ApplyFormatInEditMode = true)]
        public double? CurrentRatioPy { get; set; }

        [DisplayName("Turnover")]
        public double? AnnualTurnOverPy { get; set; }

        [DisplayName("Average Cash Balance (3 Months)"), DisplayFormat(DataFormatString = "S {0:C}", ApplyFormatInEditMode = true)]
        public double? AverageCashBalancePy { get; set; }

        [DisplayName("Financial Year")]
        public string PreviousYear { get; set; }

        [DisplayName("Debt/EBITDA Ratio")]
        public decimal? DebtEBITDARatio_PY { get; set; }
    }
}