﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class LoanPaymentViewModel
    {
        public long RequestId { get; set; }
        public long? RepaymentId { get; set; }
        public DateTime RepaymentDate { get; set; }
        public double Amount { get; set; }
        public DateTime PaidDate { get; set; }
    }
}