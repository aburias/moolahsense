﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class LoanRequestBoardViewModel
    {
        public List<LoanRequestViewModel> LoanRequestViewModels { get; set; }

        public LoanRequestBoardViewModel()
        {
            LoanRequestViewModels = new List<LoanRequestViewModel>();
        }
    }
}