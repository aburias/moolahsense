﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class RepaymentViewModel
    {
        public DateTime DueDate { get; set; }
        public double StartBalance { get; set; }
        public double Principal { get; set; }
        public double Interest { get; set; }
        public double Payment { get; set; }
        public double EndBalance { get; set; }
        public string status { get; set; }
    }
}