﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class LoanRepaymentViewModel
    {
        public long LoanId { get; set; }
        public long RepaymentId { get; set; }
        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Amount { get; set; }
        public double Rate { get; set; }
        public int MonthsTillMaturity { get; set; }
        public double MonthlyRepayment { get; set; }
        public DateTime RepaymentDate { get; set; }
        public string Status { get; set; }
        public int StatusRef { get; set; }
        public string Comment { get; set; }

        public DateTime LastUnpaidDueDate { get; set; }
        public double AccuredInterest { get; set; }
        public bool AccuredInterestPaid { get; set; }
        public DateTime? AccuredInterestPaidDate { get; set; }
        public int LateDaysCount { get; set; }

        public double TotalOutstandingRepayments { get; set; }
        public double LateFees { get; set; }
        public bool LateFeesPaid { get; set; }
        public double Paidamount { get; set; }
    }
}