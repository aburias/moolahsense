﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class MoolahPeriViewModel
    {
         
            [DisplayName("Business Awards")]
            public string BusinessAwards {get;set;}

            [DisplayName("Accreditation or Industry Certification")]
            public string Accreditation {get;set;}

            [DisplayName("Member of Trade Association")]
            public string TradeAssociation {get;set;}

            [DisplayName("Community Initiatives")]
            public string CommunityInitiatives {get;set;}

            [DisplayName("Social Media Footprint")]
            public string SocialMediaFootprint {get;set;}

            [DisplayName("Biodata of Directors/ Partners")]
            public string BiodataOfKeyPersonnel {get;set;}

            
            public string ResponseToQuestions { get; set; }

            [DisplayName("MoolahPerks for Investors")]
            public string MoolahPerk { get; set; }

            public int MoolahPerkId { get; set; }

        [DisplayName("Update MoolahPost")]
            public bool UpdateMoolahPost { get; set; }
    }
}