﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class MoolahPeriIndicatorViewModel
    {
        public bool Profitability { get; set; }
        public bool CashFlowPositive { get; set; }
        public bool DebtEquity { get; set; }
        public bool CurrentRatio { get; set; }
        public bool AnnualTurnOver { get; set; }
        public bool MoreThanOREqual2YearsInBusiness { get; set; }
    }
}