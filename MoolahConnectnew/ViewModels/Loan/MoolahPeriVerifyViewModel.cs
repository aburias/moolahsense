﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class MoolahPeriVerifyViewModel
    {
        public bool BusinessAwardsVerified { get; set; }
        public bool AccreditationVerified { get; set; }
        public bool TradeAssociationVerified { get; set; }
        public bool CommunityInitiativesVerified { get; set; }
        public bool SocialMediaFootprintVerified { get; set; }
        public bool BiodataOfKeyPersonnelVerified { get; set; }
        public bool ResponseToQuestionsVerified { get; set; }
        public bool MoolahPerkVerified { get; set; }
    }
}