﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class MoolahCoreVerifyViewModel
    {
        public bool Profitability { get; set; }
        public bool Profitability_PY_Verify { get; set; }

        public bool ProfitabilityVerified { get; set; }
        public bool CashFlowPositive_LY_Verified { get; set; }
        public bool CashFlowPositive_PY_Verified { get; set; }
        public bool DebtEquity_LY_Verified { get; set; }
        public bool DebtEquity_PY_Verified { get; set; }

        public bool DebtEBITDARatio_LY_Verification { get; set; }
        public bool DebtEBITDARatio_PY_Verification { get; set; }

        

        public bool InterestCoverageRatio_LY_Verified { get; set; }
        public bool InterestCoverageRatio_PY_Verified { get; set; }

        public bool CurrentRatio_LY_Verified { get; set; }
        public bool CurrentRatio_PY_Verified { get; set; }
        public bool AnnualTurnOver_LY_Verified { get; set; }
        public bool AnnualTurnOver_PY_Verified { get; set; }
        public bool DPPaymentGradeVerified { get; set; }
        public bool MoreThanOREqual2YearsInBusinessVerified { get; set; }
        public bool OutstandingLitigationVerified { get; set; }
        public bool NoOfCreditEnquiriesVerified { get; set; }
        public bool AverageCachBalance_LY_Verified { get; set; }
        public bool AverageCachBalance_PY_Verified { get; set; }

        [DisplayName("Financial Statement Submitted")]
        public bool FinancialStatementSubmitted { get; set; }

        [DisplayName("Financial Statement Submitted")]
        public bool? FinancialStatementSubmitted_PY_Verified { get; set; }

        [DisplayName("Audited")]
        public bool Audited { get; set; }

        [DisplayName("Audited")]
        public bool? Audited_PY_Verified { get; set; }

        [DisplayName("Financial Related Search Count in The Last 6 Months")]
        public string NoOfCreditEnquiries { get; set; }
    }
}