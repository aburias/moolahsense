﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class LoanDocumentsViewModel
    {
        public List<FileModel> Videos { get; set; }
        public List<FileModel> Documents { get; set; }
        public List<FileModel> YouTube { get; set; }
        public List<FileModel> PhotoUrls { get; set; }
    }
}