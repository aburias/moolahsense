﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoolahConnect.Util.Enums;

namespace MoolahConnectnew.ViewModels
{
    public class FileModel
    {
        public int Id { get; set; }
        public string FileName { get; set; }
        public FileType Type { get; set; }
        public DateTime CreatedDate {get;set;}
        public Guid userId { get; set; }
        public string Comments { get; set; }
        public long? RequestId { get; set; }
        public string Url { get; set; }
    }
}