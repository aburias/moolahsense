﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class AuditLogViewModel
    {
        public long? RequestId { get; set; }
        public string Message { get; set; }
        public DateTime TimeStamp { get; set; }
        public string Reference { get; set; }
        public string Username { get; set; }
    }
}