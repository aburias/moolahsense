﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class AccountViewModel
    {
        [DisplayName("Business Name")]
        public string BusinessName { get; set; }

        [DisplayName("Business Registration Number")]
        public string BusinessRegNumber { get; set; }

        [DisplayName("Business Organization")]
        public string BusinessOrganisation { get; set; }

        [DisplayName("Nature of Business")]
        public string NatureOfBusiness { get; set; }

        [DisplayName("Date of Incorporation")]
        public DateTime? DateOfIncorporation { get; set; }

        [DisplayName("Registered Address 1")]
        public string ResgiteredAddress1 { get; set; }

        [DisplayName("Registered Address 2")]
        public string ResgiteredAddress2 { get; set; }

        [DisplayName("Registered Address Postal Code")]
        public string ResgiteredAddressPostalCode { get; set; }

        [DisplayName("Business (Mailing) Address 1")]
        public string BusinessMailAddress1 { get; set; }

        [DisplayName("Business (Mailing) Address 2")]
        public string BusinessMailAddress2 { get; set; }

        [DisplayName("Business (Mailing) Address Postal Code")]
        public string BusinessMailAddressPostalCode { get; set; }

        [DisplayName("Paid Up Capital")]
        public float PaidUpCapital { get; set; }

        [DisplayName("Detailed Company Profile")]
        public string DetailedCompanyProfile { get; set; }

        [DisplayName("SICC Code")]
        public string SiccCode { get; set; }

    }
}