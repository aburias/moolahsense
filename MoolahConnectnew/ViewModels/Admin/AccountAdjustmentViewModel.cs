﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Admin
{
    public class AccountAdjustmentViewModel
    {
        public long ID { get; set; }
        public long RequestID { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public double Amount { get; set; }
        public double Rate { get; set; }
        public DateTime RepaymentDate { get; set; }
        public double RepaymentAmount { get; set; }
        public double ReceivedAmount { get; set; }
        public double Difference { get; set; }
        public bool Adjusted { get; set; }
        public DateTime? AdjustedDate { get; set; }
    }
}