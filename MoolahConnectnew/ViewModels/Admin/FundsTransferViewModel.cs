﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Admin
{
    public class FundsTransferViewModel
    {
        [DisplayName("UserID")]
        [Required]
        public int UserID { get; set; }

        [DisplayName("Description")]
        [Required]
        public string Description { get; set; }

        [DisplayName("Debit/Credit")]
        public bool IsDebit { get; set; }

        [DisplayName("Amount")]
        [Required]
        public decimal? Amount { get; set; }

        public string AdminUsername { get; set; }
        public string AdminPassword { get; set; }
    }
}