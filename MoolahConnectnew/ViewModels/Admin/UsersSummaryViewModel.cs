﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Admin
{
    public class UsersSummaryViewModel
    {
        public int TotalUsers { get; set; }
        public int TotalBorrower { get; set; }
        public int TotalInvestors { get; set; }
    }
}