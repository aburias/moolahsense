﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels.Admin
{
    public class LoanInvestorViewModel
    {
        public int OfferId { get; set; }
        public long RequestId { get; set; }

        public int UserId { get; set; }
        public string Username { get; set; }

        public string AccountId { get; set; }
        public string DisplayName { get; set; }
        public string FullName { get; set; }
        public double Amount { get; set; }
        public double RepaymentAmount { get; set; }
        public double LatePayment { get; set; }
        public long RepaymentId { get; set; }
    }
}
