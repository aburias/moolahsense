﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectnew.ViewModels
{
    public class TransactionViewModel
    {
        public DateTime? Date { get; set; }
        public long TransactionId { get; set; }
        public string TransactionReference { get; set; }
        public string Description { get; set; }
        public decimal DebitedAmount { get; set; }
        public decimal CreditedAmount { get; set; }
        public decimal Balance { get; set; }
        public long? RequestId { get; set; }
    }
}