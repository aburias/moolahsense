﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.WebPages.Html;

namespace MoolahConnectnew.ViewModels
{
    public class RegisterViewModel
    {
        public string Username { get; set; }

        public IEnumerable<SelectListItem> SecurityQuestions { get; set; }

        public int SecurityQuestion1 { get; set; }

        [Required(ErrorMessage = "* Question Answer is required.")]
        public string SecurityQuestionAnswer1 { get; set; }

        public int SecurityQuestion2 { get; set; }

        [Required(ErrorMessage = "* Question Answer is required.")]
        public string SecurityQuestionAnswer2 { get; set; }

        public int SecurityQuestion3 { get; set; }

        [Required(ErrorMessage = "* Question Answer is required.")]
        public string SecurityQuestionAnswer3 { get; set; }
    }
}