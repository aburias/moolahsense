﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MoolahConnectnew.ViewModels
{
    public class LoginViewModel
    {
        [DisplayName("Email")]
        [Required(ErrorMessage = "Please provide email address.")]
        public string Username { get; set; }

        [DisplayName("Password")]
        [Required(ErrorMessage = "Please provide password.")]
        public string Password { get; set; }

        [DisplayName("Confirm Password")]
        [Required(ErrorMessage = "Please provide password.")]
        [System.ComponentModel.DataAnnotations.Compare("Password",ErrorMessage="Please enter the same password.")]
        public string ConfirmPassword { get; set; }

        [Required()]
        public int SecurityQuestion { get; set; }

        [Required(ErrorMessage = "Please provide security answer.")]
        public string SecurityQuestionAnswer { get; set; }

        public IEnumerable<SelectListItem> SecurityQuestions { get; set; }

        public bool isLocked { get; set; }

        public string CaptchaCode { get; set; }
    }
}