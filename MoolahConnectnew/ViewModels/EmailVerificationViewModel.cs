﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoolahConnect.Util.Enums;

namespace MoolahConnectnew.ViewModels
{
    public class EmailVerificationViewModel
    {
        public bool IsVerified { get; set; }
        public string RedirectAddress { get; set; }
    }
}