﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MoolahConnectClassLibrary.Models.ViewModels.Attribute
{
    class CurrencyRequiredAttribute : ValidationAttribute, IClientValidatable
{
    public bool AllowEmptyStrings { get; set; }

    public CurrencyRequiredAttribute()
        : base("The field is required. Test")
    {
        AllowEmptyStrings = false;
    }

    public override bool IsValid(object value)
    {
        if (value == null)
            return false;

        if (value is string && !this.AllowEmptyStrings)
        {
            return !string.IsNullOrWhiteSpace(value as string);
        }

        return true;
    }

    public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
    {
        var modelClientValidationRule = new ModelClientValidationRequiredRule(FormatErrorMessage(metadata.DisplayName));
        yield return modelClientValidationRule;
    }
}
}
