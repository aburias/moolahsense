﻿using System.Web.UI;
using MoolahConnect.Services.Entities;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using Org.BouncyCastle.Bcpg.OpenPgp;

namespace MoolahConnectClassLibrary.Models.ViewModels
{

    public class LoanRequestDescriptionAdmin
    {
        
        public long RequestId { get; set; }
        public decimal? Amount { get; set; }
        public string videodescription { get; set; }
        public string MoolahSenseComments { get; set; }
        public string Term { get; set; }
        public decimal EIR { get; set; }
        public int? LoanPurpose_Id { get; set; }
        public IEnumerable<tbl_LoanPurposesList> LoanpurposeList { get; set; }
        public string Questions { get; set; }
        public Nullable<bool> IsotherLoantype { get; set; }
        public Nullable<bool> IspersonalGuarantee { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _IspersonalGuarantee { get; set; }
        public string RequestStatus { get; set; }
        public string VideoDiscription { get; set; }
        public string NameasinNRICPassport { get; set; }
        public string Designation { get; set; }
        public string NRICPassport { get; set; }
        public string Residentialaddress1 { get; set; }
        public string Residentialaddress2 { get; set; }
        public string Postalcode { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Reasons { get; set; }
        public string Accountnumber { get; set; }
        public string Loanimages { get; set; }
        public string LoanDocs { get; set; }
        ///////////////question and answer property for update///////////////
        public string QA { get; set; }
        public string DocumentsList { get; set; }
        ///verification
       public string AmountVerification { get; set; }
        public string TermVerification { get; set; }
        public string EIRVerification { get; set; }
        public string LoanPurpose_IdVerification { get; set; }
        public string LoanPurposeDocumentIDs { get; set; }
        public string PersonelGuranteeVerification { get; set; }
        public string VideoDiscriptionVerification { get; set; }
        public string Headline { get; set; }

        //personel gurantee verfication properties
        public string NameasinNRICPassportVerification { get; set; }
        public string DesignationVerification { get; set; }
        public string NRICPassportVerification { get; set; }
        public string ResidentialaddressVerification1 { get; set; }
        public string ResidentialaddressVerification2 { get; set; }
        public string PostalcodeVerification { get; set; }
        public string TelephoneVerificaiion { get; set; }
        public string EmailVerificaiion { get; set; }
        public string ReasonsVerificaiion { get; set; }
        public string NationalityVerification { get; set; }
        public string PassportNumberVerification { get; set; }

        public string DetailedCompanyPro { get; set; }

        public string SummaryCompanyPro { get; set; }
        public string NumberOfEmployees { get; set; }
        public string Nationality { get; set; }
        public string PassportNumber { get; set; }
        public string Username { get; set; }
        
        public string DocumentComments { get; set; }

        public IList<PersonGuarantee> PersonGuarantees { get; set; } 
    }

    public class LoanRequestAdmin
    {
        //public LoanRequestAdmin()
        //{
        //    RepaymentMessages =new List<RepaymentMessage>();
        //}
        //public LoanRequest(LoanRequestDescription LoanRequestDescription)
        //{
        //    this.LoanRequestDescription = LoanRequestDescription;
        //}
        public LoanRequestDescriptionAdmin LoanRequestDescription { get; set; }
        public LoanRequestMoolahCoreAdmin LoanRequestmoolahCore { get; set; }
        public LoanRequestMoolahPeriAdmin LoanRequestmoolahPeri { get; set; }

        //public IList<RepaymentMessage> RepaymentMessages { get; set; } 
        
        //public OutstandingLitigation Ou

    }
    
    public class LoanRequestMoolahCoreAdmin
    {
        public long RequestId { get; set; }
        public Nullable<long> LoanRequest_Id { get; set; }
        public Nullable<long> User_ID { get; set; }
        public string LatestYear { get; set; }
        public string PreviousYear { get; set; }
        public decimal? Turnovers_LY { get; set; }
        public decimal? NetprofitafterTax_LY { get; set; }
        public decimal? CurrentAssests_LY { get; set; }
        public decimal? CurrentLiabilities_LY { get; set; }
        public decimal? CurrentRatio_LY { get; set; }
        public decimal? TotalDebt_LY { get; set; }
        public decimal? TotalShareholdersequity_LY { get; set; }
        public decimal? Debt_Equity_LY { get; set; }
        public decimal? CashFlowfromOperations_LY { get; set; }
        public decimal? Turnovers_PY { get; set; }
        public decimal? NetprofitafterTax_PY { get; set; }
        public decimal? CurrentAssests_PY { get; set; }
        public decimal? CurrentLiabilities_PY { get; set; }
        public decimal? CurrentRatio_PY { get; set; }
        public decimal? TotalDebt_PY { get; set; }
        public decimal? TotalShareholdersequity_PY { get; set; }
        public decimal? Debt_Equity_PY { get; set; }
        public decimal? CashFlowfromOperations_PY { get; set; }
        public bool? IsoutstandingLitigation { get; set; }
        public string Accountnumber { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _Litigation { get; set; }
        public string LitigationType { get; set; }

        public decimal? ProfitBeforIntAndTax_LY { get; set; }
        public decimal? InterestExpense_LY { get; set; }
        public decimal? InterestCoverageRatio_LY { get; set; }
        public decimal? EBITDA_LY { get; set; }
        public decimal? DebtEBITDARatio_LY { get; set; }

        public decimal? ProfitBeforIntAndTax_PY { get; set; }
        public decimal? InterestExpense_PY { get; set; }
        public decimal? InterestCoverageRatio_PY { get; set; }
        public decimal? EBITDA_PY { get; set; }
        public decimal? DebtEBITDARatio_PY { get; set; }

        public int? ProfitBeforIntAndTax_LY_Verification { get; set; }
        public int? InterestExpense_LY_Verification { get; set; }
        public int? InterestCoverageRatio_LY_Verification { get; set; }
        public int? EBITDA_LY_Verification { get; set; }
        public int? DebtEBITDARatio_LY_Verification { get; set; }

        public int? ProfitBeforIntAndTax_PY_Verification { get; set; }
        public int? InterestExpense_PY_Verification { get; set; }
        public int? InterestCoverageRatio_PY_Verification { get; set; }
        public int? EBITDA_PY_Verification { get; set; }
        public int? DebtEBITDARatio_PY_Verification { get; set; }



        public OutstandingLitigation Litigation { get; set; }
        public long MoolahCoreId { get; set; }
        public bool? FinancialStatementSubmitted { get; set; }
        public bool? FinancialStatementSubmitted_PY { get; set; }
        public bool? Audited { get; set; }
        public bool? Audited_PY_Verification { get; set; }
        public double? AnnualRevenue { get; set; }
        public bool? Profitable { get; set; }
        public bool? Profitable_PY { get; set; }
        public decimal? Gearing { get; set; }
        public string NumberOfCreditEnquires { get; set; }
        public string TotalLoanDueNext12Months { get; set; }
        public int? DPCreditPaymentGrade { get; set; }
        /////////////////properties use while updating, make string of outsanding litigation to show in a table
        public List<tbl_OutStandingLitigation> OverdraftandFactoring { get; set; }
        public List<tbl_OutStandingLitigation> Loans { get; set; }
        public List<tbl_OutStandingLitigation> Trade { get; set; }
        /////////properties for verification of values
        public int? Turnovers_LY_Verification { get; set; }
        public int? NetprofitafterTax_LY_Verification { get; set; }

        public int? PreviousYearVerification { get; set; }
        public int? LatestYearVerification { get; set; }

        public int? CurrentAssests_LY_Verification { get; set; }
        public int? CurrentLiabilities_LY_Verification { get; set; }
        public int? CurrentRatio_LY_Verification { get; set; }
        public int? TotalDebt_LY_Verification { get; set; }
        public int? TotalShareholdersequity_LY_Verification { get; set; }
        public int? Debt_Equity_LY_Verification { get; set; }
        public int? CashFlowfromOperations_LY_Verification { get; set; }
        public int? Turnovers_PY_Verification { get; set; }
        public int? NetprofitafterTax_PY_Verification { get; set; }
        public int? CurrentAssests_PY_Verification { get; set; }
        public int? CurrentLiabilities_PY_Verification { get; set; }
        public int? CurrentRatio_PY_Verification { get; set; }
        public int? TotalDebt_PY_Verification { get; set; }
        public int? TotalShareholdersequity_PY_Verification { get; set; }
        public int? Debt_Equity_PY_Verification { get; set; }
        public int? CashFlowfromOperations_PY_Verification { get; set; }
        public int? IsoutstandingLitigation_Verification { get; set; }
        public int? Accountnumber_Verification { get; set; }
        public int? Profitable_Verification { get; set; }
        public int? Profitable_PY_Verification { get; set; }
        public int? DPCreditPayment_Verification { get; set; }
        public decimal? AvgThreeMntCashBalBankAcc_LY { get; set; }
        public int? AvgThreeMntCashBalBankAcc_LY_Verification { get; set; }
        public decimal? AvgThreeMntCashBalBankAcc_PY { get; set; }
        public int? AvgThreeMntCashBalBankAcc_PY_Verification { get; set; }
        public int? NumberOfCreditEnquires_Verification { get; set; }
    }
//    public class  RepaymentMessage

//{
//        public long Id { set; get; }
//        public string Message { get; set; }
//        public EntityState State { get; set; }
//        public long LoanRequestId { get; set; }
//        public string CreatedDateTime { get; set; }
//}

    public class OutstandingLitigationAdmin
    {
        public long LitigationId { get; set; }
        public Decimal? AmountOutstanding { get; set; }
        public Decimal? CreditLimit { get; set; }

        public Decimal? UsageinLastMonth { get; set; }

        [DataType(DataType.Date, ErrorMessage = "* Only dates are allowed")]
        public DateTime? RenewalDate { get; set; }

        public Decimal? RepaymentAmount { get; set; }

        [DataType(DataType.Date, ErrorMessage = "* Only dates are allowed")]
        public DateTime? DateofLastPayment { get; set; }

        [StringLength(4000)]
        public string Description { get; set; }
        [StringLength(32)]
        public string LitigationType { get; set; }

        public int? PaymentTerms { get; set; }

        public long? MoolahCore_Id { get; set; }
        public int? User_ID { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _RepaymentPeriod { get; set; }

        public string _RepaymentPeriodSet { get; set; }
    } 
  
    public class LoanRequestMoolahPeriAdmin
    {
        [StringLength(500)]
        public string BusinessAwards { get; set; }
        [StringLength(500)]
        public string Accreditation { get; set; }
        [StringLength(500)]
        public string MTA { get; set; }
        [StringLength(500)]
        public string DigitalFootPrint { get; set; }
        [StringLength(500)]
        public string CommunityInitiative { get; set; }
        [StringLength(4000)]
        public string BiodataOfDS { get; set; }
        public Nullable<long> MoolahPerk_Id { get; set; }
        public string MoolahPerk_ddlCustom { get; set; }
        public Nullable<long> Request_ID { get; set; }
        public Nullable<long> User_ID { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public String Accountnumber { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> MoolahPerk { get; set; }
        public string MoolahPerkset { get; set; }
        [StringLength(64)]
        public string MoolahPerk_Custom { get; set; }

        public bool UpdateMoolahPost { get; set; }

        //public IEnumerable<tbl_PersonalGuaranteeInfo> LoanpurposeList { get; set; }
        /////////verifiation properties for moolahperi

        public int? BussinesAwardsVerification { get; set; }
        public int? AcredationVerification { get; set; }
        public int? MemberofAssociationVerifcation { get; set; }
        public int?   DigitalVerification { get; set; }
        public int? CommunityVerification { get; set; }
        public int? BioDataofDirectorsVerification { get; set; }
        public int? MoolahPerkforLendersVerifcation { get; set; }
        public bool? FinalApprovalofLoanRequest { get; set; }
        public string CommentofAdminforMoolahPeri { get; set; }

        public string RequestStatus { get; set; }
        public int LoanStatus { get; set; }
        public int PreliminaryStatus { get; set; }

    }

    
    
    


}