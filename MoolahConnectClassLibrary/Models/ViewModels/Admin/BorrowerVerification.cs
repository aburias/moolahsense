﻿using MoolahConnect.Services.Entities;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels.Admin
{
    public class BorrowerAccounts
    {
        public AdminAccountDetail AccountDetails { get; set; }
        public KnowledgeassessmentWrapper Knowledgeassessment { get; set; }
    }
    
    public class BorrowerAccountDetailsModels
    {
        public string Titles { get; set; }
        public long? UseriD { get; set; }
        [Required(ErrorMessage = "* Full Name is required")]
        [Display(Name = "Full Name")]
        [StringLength(50)] 
        [DataType(DataType.Text)]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        public string FirstNames { get; set; }
        public string AccountType { get; set; }

        [Display(Name = "LastName")]
        [StringLength(50)]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        public string LastNames { get; set; }


        [Display(Name = "DisplayName")]
        [StringLength(100)]
        public string DisplayNames { get; set; }

        [Required(ErrorMessage = "* Position Held is required")]
        [Display(Name = "Position Held")]
        [StringLength(100)]
        public string RoleJobTitles { get; set; }

        [Required(ErrorMessage = "* Business Name is required")]
        [Display(Name = "BusinessName")]
        [StringLength(500)]
        public string BusinessNames { get; set; }

        [Required(ErrorMessage = "* Business Registration Number is required")]
        [Display(Name = "BusinessRegistrationnumber")]
        [StringLength(20)]
        public string BusinessRegistrationnumbers { get; set; }


        [Display(Name = "BusinessOrganisation")]
        public string BusinessOrganisations { get; set; }


        public int NatureofBusinessIds { get; set; }
        public string NatureofBusinessNames { get; set; }

        public IEnumerable<tbl_NatureofBusinessList> NatureofBusinessLists { get; set; }


        [DataType(DataType.Date, ErrorMessage = "* Only Date formate is allowed")]
        [Display(Name = "DateofIncorporation")]
        public DateTime? DateofIncorporations { get; set; }
        public string DateofIncorporationsString { get; set; }

        [Required(ErrorMessage = "* Registered Address is required")]
        [Display(Name = "RegisteredAddress")]
        [StringLength(500)]
        public string RegisteredAddresss { get; set; }



        public string InvestorType { get; set; }
        public string BusinessMailingaddresss { get; set; }
        public double PaidUpCapital { get; set; }
        public int NumOfEmployees { get; set; }

        [Required(ErrorMessage = "* Direct Line is required")]
        [Display(Name = "DirectLine")]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber, ErrorMessage = "* No characters are allowed")]
        public string DirectLines { get; set; }

        [Display(Name = "Mobilenumber")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits")]
        public string Mobilenumbers { get; set; }

        [Display(Name = "Official Correspondence Email")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid e-mail adress")]
        [StringLength(20)]
        public string Othercontactnumbers { get; set; }

        public int BankIds { get; set; }

        public string BankNames { get; set; }

        public IEnumerable<tbl_BanksList> BanksLists { get; set; }

        [Required(ErrorMessage = "* Bank Account Number is required")]
        [Display(Name = "BankAccountNumber")]
        [StringLength(20)]
        public string BankAccountNumbers { get; set; }

        [Required(ErrorMessage = "* Bank Account Name is required")]
        [Display(Name = "BankAccountName")]
        [StringLength(100)]
        public string BankAccountNames { get; set; }

        [Display(Name = "SICC Code")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "* No characters are allowed")]
        public string SiccCode { get; set; }

        [RegularExpression(@"^[a-z|A-Z]{1}\d{7}[a-z|A-Z]{1}$", ErrorMessage = "* NRIC / FIN number must be in format A1234567B ")]
        public string NricNumber { get; set; }

        public string PassportNumber { get; set; }
        
        public string ICNumber { get; set; }
        public string Nationality { get; set; }
        public bool? PRStatus { get; set; }
        public DateTime? DateofBirth { get; set; }
        public string Gender { get; set; }
        public string ResidentialAddress { get; set; }
        public string PostalCode { get; set; }
        public string Emailaddress { get; set; }
        public string Occupation { get; set; }
        public string IncomeRange { get; set; }


        public IEnumerable<tbl_SecurityQuestions> SecurityQuestionList { get; set; }

        public int? SecurityQuestion1 { get; set; }
        public int? SecurityQuestion2 { get; set; }
        public int? SecurityQuestion3 { get; set; }

        public string SecurityQuestion1Name { get; set; }
        public string SecurityQuestion2name { get; set; }
        public string SecurityQuestion3name { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer1")]
        [StringLength(200)]
        public string SecurityAnswer1 { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer2")]
        [StringLength(200)]
        public string SecurityAnswer2 { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer3")]
        [StringLength(200)]
        public string SecurityAnswer3 { get; set; }


        public IEnumerable<System.Web.Mvc.SelectListItem> _Gender { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _IncomeRange { get; set; }

        public IEnumerable<System.Web.Mvc.SelectListItem> _Nationality { get; set; }
  

        public IEnumerable<System.Web.Mvc.SelectListItem> _Titless { get; set; }
        public IEnumerable<BorrowerDocuments> CommentsLists { get; set; }
        public IEnumerable<DocumentDetailList> DocumentList { get; set; }
 
        public string TitleVerification { get; set; }
        public string FirstNameVerification { get; set; }
        public string LastNameVerification { get; set; }
        public string DisplayNameVerification { get; set; }
        public string RoleOrJobTitleVerification { get; set; }
        public string BussinessNameVerification { get; set; }
        public string BussinessRegistrationNumberVerification { get; set; }
        public string BussinessOrganisationVerification { get; set; }
        public string NatureofBussinessVerification { get; set; }
        public string DateofIncorporationVerification { get; set; }
        public string RegisteredAddressVerification { get; set; }
        public string BussinessMailingAddressVerification { get; set; }
        public string DirectLineVerification { get; set; }
        public string MobileVerification { get; set; }
        public string OptinalContactVerification { get; set; }
        public string BankVerification { get; set; }
        public string BankAccountNumberVerification { get; set; }
        public string BankAccountNameVerification { get; set; }
        public string PaidUpCapitalVerification { get; set; }
        public string NumOfEmployeesVerification { get; set; }

        public string VerifiedDocumentIDs { get; set; }
        public string AdminCommentOnUserData { get; set; }
        public string AdminCommentonDocuments { get; set; }
        public string AdminVerification { get; set; }
        public string AdminPersonelComments { get; set; }
        public string ICNumberVer { get; set; }
        public string NationalityVer { get; set; }
        public string  PRStatusVer { get; set; }
        public string GenderVer { get; set; }
        public string ResidentialAddressVer { get; set; }
        public string PostalCodeVer { get; set; }
        public string OcupationVer { get; set; }
        public string IncomeRaneVer { get; set; }
        public string EmailVer { get; set; }
        public string DOBVer { get; set; }
    }

    public class DocumentDetailList
    {
        public long? DocumentIds { get; set; }
        public string DocumentNames { get; set; }
   
        public string Comments { get; set; }
       
        public string DocName { get; set; }
        public string IsVerified { get; set; }
        public Int64 DocID { get; set; }
        public bool? DocStatus { get; set; }
    }
    public class BorrowerDocumentss
    {
        public Int64 DocumentIds { get; set; }
        public string DocumentNames { get; set; }
        public string DocumentPaths { get; set; }
        public Nullable<bool> Documentstatuss { get; set; }
        public string Documenttypes { get; set; }
        public int Userids { get; set; }
        public string Comments { get; set; }
        public IList<commentlists> CommentsLists { get; set; }
        public string DocName { get; set; }
        public string IsVerified { get; set; }
        public Int64 DocID { get; set; }
    }
    public class commentlists
    {
        public string Comments { get; set; }
        public string Usernames { get; set; }
    }


  

}