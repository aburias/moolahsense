﻿using MoolahConnect.Services.Entities;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels.Admin
{
    public class AuditTrial
    {
        tbl_AuditTrial audit { get; set; }
        tbl_Users user { get; set; }
    }
}