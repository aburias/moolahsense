﻿using MoolahConnect.Services.Entities;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels.Admin
{
    public class Withdrawal
    {
        public List<WithdrawList> ModelWithdrawalList {get;set;}
        public List<tbl_Users> UserList { get; set; }
        public long AdminID { get; set; } 
    }
    public class WithdrawList
    {
        public long WitdrawID { get; set; }
        public DateTime? WithdrawDate {get;set;}
        public string FullName { get; set; }
        
        public string TransactionReference { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }

        public string BankAccountNUmber { get; set; }
        
        public decimal? CurrentBalance { get; set; }
        public decimal? AmounttoWithdraw { get; set; }
        public decimal? BalanceAfterWithdrawl { get; set; }
        
        public string Status { get; set; }
        public string Comments { get; set; }
        public long? AdminID { get; set; }
        public string AdminName { get; set; }
        public DateTime? AdminActionDate { get; set; }



    }


}