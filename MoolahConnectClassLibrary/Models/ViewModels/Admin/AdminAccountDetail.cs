﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
namespace MoolahConnectClassLibrary.Models.ViewModels.Admin
{
    public class AdminAccountDetail : AccountDetailsBase
    {
        public string BankVerification { get; set; }
        public string BankNumberVerification { get; set; }
        public string BranchNumberVerification { get; set; }
        public string BranchNameVerification { get; set; }
        public string BankAccountNumberVerification { get; set; }
        public string BankAccountNameVerification { get; set; }
        public string FirstNameVerification { get; set; }
        public string NationalityVerification { get; set; }
        public string NricNumberVerification { get; set; }
        public string PassportNumberVerification { get; set; }
        public string BussinessNameVerification { get; set; }
        public string BussinessRegistrationNumberVerification { get; set; }
        public string BussinessOrganisationVerification { get; set; }
        public string SiccCodeVerification { get; set; }
        public string DateofIncorporationVerification { get; set; }
        public string RegisteredAddress1Verification { get; set; }
        public string RegisteredAddress2Verification { get; set; }
        public string RegisteredAddressPostVerification { get; set; }
        public string BussinessMailingAddress1Verification { get; set; }
        public string BussinessMailingAddress2Verification { get; set; }
        public string BussinessMailingAddressPostVerification { get; set; }
        public string DirectLineVerification { get; set; }
        public string MobileVerification { get; set; }
        public string OfficialCorrespondenceEmailVerification { get; set; }
        public string PositionHeldVerification { get; set; }
        public string DisplayNameVerification { get; set; }
        public string PaidUpCapitalVerification { get; set; }
        public string DoBVerification { get; set; }
        public string GenderVerification { get; set; }
        public string ResidentialAddress1Verification { get; set; }
        public string ResidentialAddress2Verification { get; set; }
        public string ResidentialAddressPostVerification { get; set; }
        public string PostalCodeVerification { get; set; }
        public string MainContactNumberVerification { get; set; }
        public string EmailVerification { get; set; }
        public string AdminVerification { get; set; }
        public bool isSubmitted { get; set; }
        
    }
}