﻿using System;
using System.Collections.Generic;

namespace MoolahConnectClassLibrary.Models.ViewModels.Admin
{
    public class AcceptedLoanWrapper
    {
        public int SearchTerm { get; set; }
        public List<AcceptedLoan> Loans { get; set; }
    }

    public class AcceptedLoan
    {
        public long LoanRefNumber { get; set; }
        public string Username { get; set; }
        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public string Tenor { get; set; }
        public decimal Rate { get; set; }
        public decimal LoanAmount { get; set; }
        public double MoolahFees { get; set; }
        public double AmountTransfered { get; set; }
        public string BankName { get; set; }
        public string BankAccountNumber { get; set; }
        public DateTime? FundTransferDate { get; set; }
        public DateTime AcceptedDate { get; set; }
        public string Status { get; set; }
        public string Comments { get; set; }
        public string AdminId { get; set; }
    }
}