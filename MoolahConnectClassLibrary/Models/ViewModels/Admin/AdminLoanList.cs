﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels.Admin
{
    public class AdminLoanList
    {
        public List<LoanListForAdmin> LoanListForAdmin { get; set; }

    }
    public class LoanListForAdmin
    {
        public LoanListForAdmin()
        {
            TimeLeft = new int[3];
        }

        public string UserName { get; set; }
        public long RequestID { get; set; }
        public decimal? Amount { get; set; }
        public int PreliminaryStatus { get; set; }
        public int Status { get; set; }
        public string IsApproved { get; set; }
        public DateTime? DateCreated { get; set; }
        public string AccountNumber { get; set; }
        public string CompanyName { get; set; }
        public int[] TimeLeft { get; set; }
        public string Submitted { get; set; }
    }
}