﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class WatchList
    {
        public List<WatchListData> WatchListData { get; set; }
    }

    public class WatchListData
    {
        public long? Request_ID { get; set; }
        public long? UserID { get; set; }
        public decimal? Amount { get; set; }
        public string Terms { get; set; }

    }
}