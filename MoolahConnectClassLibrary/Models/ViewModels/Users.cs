﻿using MoolahConnect.Services.Entities;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;



namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class Users
    {

    }

    #region User Common
    public class ForgotPasswordModel
    {
        [Required(ErrorMessage = "* E-mail Id is required")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address*")]
        public string Emailid { get; set; }

        [Required(ErrorMessage = "* Security question is required")]
        public string SecurityQuestion { get; set; }

        public IEnumerable<tbl_SecurityQuestions> SecurityQuestionList { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [StringLength(200)]
        public string Answer { get; set; }

        [Required(ErrorMessage = "* Account number is required")]
        public string Accountnumber { get; set; }


    }

    public class ForgotAccountNumberModel
    {
        [Required(ErrorMessage = "* E-mail Id is required")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email address*")]
        public string Emailid { get; set; }

        [Required(ErrorMessage = "* Security question is required")]
        public string SecurityQuestion { get; set; }

        public IEnumerable<tbl_SecurityQuestions> SecurityQuestionList { get; set; }

        [Required(ErrorMessage = "* Answer is required")]
        [StringLength(200)]
        public string Answer { get; set; }

        [Required(ErrorMessage = "* Identity card/ Comapany Registration number  is required")]
        public string IdentityCardnumber_CompanyRegistrationnumber { get; set; }
    }

    public class ResetPasswordAccountnumberModel
    {
        public ResetPasswordAccountnumberModel(ForgotPasswordModel ForgetPassword, ForgotAccountNumberModel ForgetAccountNumber)
        {
            this.Forgotpassword = ForgetPassword;
            this.ForgotAccountnumber = ForgetAccountNumber;
        }
        public ForgotPasswordModel Forgotpassword { get; set; }
        public ForgotAccountNumberModel ForgotAccountnumber { get; set; }
    }


    public class RegisterUserModel
    {
        //////added by varun/////
        public string InverstorType { get; set; }
        [Required(ErrorMessage = "* E-mail Id is required")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "* Please enter a valid e-mail adress")]
        [Display(Name = "Email address")]
        [System.Web.Mvc.Remote("CheckEmailavailability", "User", HttpMethod = "POST", ErrorMessage = "* Email already registered. Please enter a different email.")]
        public string EmailAddress { get; set; }

        [Required(ErrorMessage = "* Confirmation E-mail Id is required")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "* Please enter a valid e-mail adress")]
        [Display(Name = "Confirm Email address")]
        [System.ComponentModel.DataAnnotations.Compare("EmailAddress", ErrorMessage = "* The email and confirmation email do not match.")]
        public string ConfirmEmailAddress { get; set; }


        [Required(ErrorMessage = "* Please select \"Find Moolah\" or \"Invest Moolah\"")]
        public string Userrole { get; set; }

        [Required(ErrorMessage = "* Password is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        [System.Web.Mvc.Remote("CheckEmailPasswordMatchcase", "User", HttpMethod = "POST", ErrorMessage = "* Email and Password must not be same", AdditionalFields = "EmailAddress")]
        public string Password { get; set; }

        [Required(ErrorMessage = "* Confirmation Password is required")]
        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "* The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public bool SubscribeforUpdate { get; set; }

        //[Required]
        //[BooleanMustBeTrue(ErrorMessage = "You must agree to the terms and conditions")]
        public bool Isagree { get; set; }

        public int SecurityQuestion1 { get; set; }

        [Required(ErrorMessage = "* Question Answer is required.")]
        public string SecurityQuestionAnswer1 { get; set; }

        public int SecurityQuestion2 { get; set; }

        [Required(ErrorMessage = "* Question Answer is required.")]
        public string SecurityQuestionAnswer2 { get; set; }

        public int SecurityQuestion3 { get; set; }

        [Required(ErrorMessage = "* Question Answer is required.")]
        public string SecurityQuestionAnswer3 { get; set; }

        //public class BooleanMustBeTrueAttribute : ValidationAttribute
        //{
        //    public override bool IsValid(object propertyValue)
        //    {
        //        return propertyValue != null
        //            && propertyValue is bool
        //            && (bool)propertyValue;
        //    }
        //}



    }


    public class AdminLogin
    {
        [Required(ErrorMessage = "* E-mail Id is required")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        public string UserName { get; set; }
        [Required(ErrorMessage = "* Password is requiered")]
        public string Password { get; set; }
    }

    #endregion




}