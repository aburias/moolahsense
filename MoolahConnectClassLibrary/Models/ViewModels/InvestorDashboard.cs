﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class InvestorDashboard
    {
        public Summary_InvestorDashboard Summary { get; set; }
        public MoolahBoard_InvestorDashboard MoolahBoard { get; set; }
        public TransferMoney TransferMoney { get; set; }

    }

    public class InvestorDetails
    {
        public DateTime MemberSince { get; set; }
        public int LoanOffersCount { get; set; }
        public double CummulativeAmountOfLoanOffers { get; set; }
        public int MatachedLoanOffersCount { get; set; }
        public double MatachedLoanOffersAmount { get; set; }
        public double WeightedAverageNominalYeidl { get; set; }
    }

    public class InvestorProfile
    {
        public string InvestorType { get; set; }

        public string BusinessName { get; set; }
        public string BusinessRegNumber { get; set; }
        public string BusinessOrganisation { get; set; }
        public string SiccCode { get; set; }
        public DateTime? DateofIncorporation { get; set; }
        public string RegisteredAddress { get; set; }
        public string BusinessMailingAddress { get; set; }
        public string DirectLine { get; set; }
        public string Mobilenumber { get; set; }
        public string OfficialCorrespondenceEmail { get; set; }
        public string Bank { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string FullName { get; set; }
        public string DisplayName{get;set;}
        public string Nationality { get; set; }
        public string NricNumber { get; set; }
        public string PassportNumber { get; set; }
        public string PositionHeld { get; set; }

        public string BankNumber { get; set; }
        public string BranchName { get; set; }
        public string BranchNumber { get; set; }

        public DateTime? DateofBirth { get; set; }
        public string Gender { get; set; }
        public string ResidentialAddress { get; set; }
        public string MainContactNumber { get; set; }
        public string EmailAddress { get; set; }

        public string RegisteredAddress1 { get; set; }
        public string RegisteredAddress2 { get; set; }
        public string RegisteredAddressPostalCode { get; set; }

        public string BusinessMailingAddress1 { get; set; }
        public string BusinessMailingAddress2 { get; set; }
        public string BusinessMailingAddressPostalCode { get; set; }

        public string ResidentialAddress1 { get; set; }
        public string ResidentialAddress2 { get; set; }
        public string ResidentialAddressPostalCode { get; set; }
    }

    public class InvestorProfile_Dashboard
    {
        public List<InvestorProfile> InverstorProfiles { get; set; }
    }

    public class TransferMoney
    {
        public decimal? CurrentBlance { get; set; }
        public decimal WithDrawMoney { get; set; }
        public int? MinimumBalancetoWithdraw { get; set; }
        public string AccountNumber { get; set; }
        public string BankName { get; set; }
    }
    public class Summary_InvestorDashboard
    {
        public decimal? TootalMoolah { get; set; }
        public decimal? AvailFunds { get; set; }
        public decimal? OutstandingLoans { get; set; }
        public decimal? LoanInvestment { get; set; }
        public decimal? AccruedInterest { get; set; }
        public decimal?  CurrentBids { get; set; }
        public List<EarningSummary> EarningSummary { get; set; }
        public decimal? GrossYield { get; set; }
        public List<InvestorLoanRequest> InvestorLoanRequest { get; set; }
        public List<InvestorLoanOfferes> InvestorAllLoanOffers { get; set; }
        public string AccountName { get; set; }
        public string AccountNumber { get; set; }

        public InvestorDetails Details { get; set; }
    }
    public class HowItWorks_InvestorDashboard
    {

    }
    public class TransferMoney_InvestorDashboard
    {
    }

    public class MoolahBoard_InvestorDashboard
    {
        public List<MoolahBoardRequest> MoolahBoard { get; set; }
    }
    
}

public class EarningSummary
{
    public string Reference { get; set; }
    public long? LoanRequestID { get; set; }
    public decimal? YourShare { get; set; }
    public string DateRecieved { get; set; }    
}

public class InvestorLoanRequest
{
    
    public long? LoanRequestLoanRequestID { get; set; }
    public decimal? LoanRequestAmount { get; set; }
    public string LoanRequestTerm { get; set; }
    public string LoanRequestReference { get; set; }
    public DateTime? LoanCreationDate { get; set; }  
}
public class InvestorLoanOfferes
{
    public long OfferID { get; set; }
    public string Reference { get; set; }
    public long? LoanRequestID { get; set; }
    public decimal? OfferAmount { get; set; }
    public string DateCreated { get; set; }
    public decimal? RateOffered { get; set; }
    public string status { get; set; }

}
public class MoolahBoardRequest
{
    public decimal? Amount { get; set; }
    public string RegisteredAddress { get; set; }
    public DateTime? DateCreated { get; set; }
    public string Term { get; set; }
    public decimal? AverageRate { get; set; }
    public string TimeLeft { get; set; }
    public int days { get; set; }
    public int Hours { get; set; }
    public int minutest { get; set; }
    public long LoanRequestID { get; set; }
    public string Reference { get; set; }
    public string Purpose { get; set; }
    public string CompanyName { get; set; }
    public decimal? Funded { get; set; }
}
 




