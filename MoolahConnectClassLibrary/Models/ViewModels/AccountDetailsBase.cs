﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using MoolahConnect.Services.Entities;
using MoolahConnectClassLibrary.Models.ViewModels.Admin;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class AccountDetailsBase
    {
        #region Base Fields

        // Base fields
        public long? UseriD { get; set; }
        public string AccountType { get; set; }
        public string InvestorType { get; set; }
        public string UserRole { get; set; }
        public string comments { get; set; }

        #endregion

        #region Common Fields

        // Common fields
        public int BankId { get; set; }

        ////[Required(ErrorMessage = "* Bank Code is required")]
        [Display(Name = "Bank Code")]
        [StringLength(100)]
        public string BankNumber { get; set; }

        //[Required(ErrorMessage = "* Branch Name is required")]
        [Display(Name = "Branch Name")]
        [StringLength(100)]
        public string BranchName { get; set; }

        //[Required(ErrorMessage = "* Branch Code is required")]
        [Display(Name = "Branch Code")]
        [StringLength(100)]
        public string BranchNumber { get; set; }

        //[Required(ErrorMessage = "* Bank Account Number is required")]
        [Display(Name = "BankAccountNumber")]
        [StringLength(20)]
        public string BankAccountNumber { get; set; }

        //[Required(ErrorMessage = "* Bank Account Name is required")]
        [Display(Name = "Bank Account Name")]
        [StringLength(100)]
        public string BankAccountName { get; set; }

        //[Required(ErrorMessage = "* Full Name is required")]
        [Display(Name = "Full Name")]
        [StringLength(50)]
        [DataType(DataType.Text)]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        public string FirstName { get; set; }

        public string Nationality { get; set; }

        [Display(Name = "NRIC / FIN Number")]
        [RegularExpression(@"^[a-z|A-Z]{1}\d{7}[a-z|A-Z]{1}$",
            ErrorMessage = "* NRIC / FIN number must be in format A1234567B ")]
        [Remote("IsValidNRIIN", "Investor", AdditionalFields = "DisplayName", HttpMethod = "POST", ErrorMessage = "* NRIC / FIN number already registered. Please check your number.")]

        public string NricNumber { get; set; }

        public string PassportNumber { get; set; }

        [Display(Name = "Email")]
        //[Required(ErrorMessage = "* Email Address is required")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [DataType(DataType.EmailAddress, ErrorMessage = "Please enter a valid e-mail adress")]
        [Remote("CheckEmailavailability", "User",AdditionalFields="UseriD", HttpMethod = "POST", ErrorMessage = "* Email already registered. Please enter a different email.")]
        public string Emailaddress { get; set; }

        #endregion

        #region Common to borrower & corporate

        // Common to borrower & corporate
        //[Required(ErrorMessage = "* Business Name is required")]
        [Display(Name = "Business Name")]
        [StringLength(500)]
        public string BusinessName { get; set; }

        //[Required(ErrorMessage = "* Business Registration Number is required")]
        [Display(Name = "Business Registration Number")]
        [StringLength(20)]
        public string BusinessRegistrationnumber { get; set; }

        [Display(Name = "Business Organisation")]
        public string BusinessOrganisation { get; set; }

        [Display(Name = "SICC Code")]
        [RegularExpression(@"[0-9]*", ErrorMessage = "* No characters are allowed")]
        public string SiccCode { get; set; }

        [DataType(DataType.Date, ErrorMessage = "* Only Date formate is allowed")]
        [Display(Name = "Date of Incorporation")]
        public DateTime? DateofIncorporation { get; set; }

        public string DateofIncorporationString { get; set; }

        //[Required(ErrorMessage = "* Registered Address 1 is required")]
        [Display(Name = "Registered Address 1")]
        [StringLength(240)]
        public string RegisteredAddress1 { get; set; }

        //[Required(ErrorMessage = "* Registered Address 2 is required")]
        [Display(Name = "Registered Address 2")]
        [StringLength(240)]
        public string RegisteredAddress2 { get; set; }

        //[Required(ErrorMessage = "* Registered Address Postal Code is required")]
        [Display(Name = "Registered Address Postal Code")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "* No characters are allowed or number must contain 6 digits")]
        [RegularExpression(@"[0-9]+", ErrorMessage = "No characters are allowed")]
        public string RegisteredPostalCode { get; set; }

        //[Required(ErrorMessage = "* Business Mailing Address 1 is required")]
        [Display(Name = "Mailing Address 1")]
        [StringLength(240)]
        public string BusinessMailingAddress1 { get; set; }

        //[Required(ErrorMessage = "* Business Mailing Address 2 is required")]
        [Display(Name = "Mailing Address 2")]
        [StringLength(240)]
        public string BusinessMailingAddress2 { get; set; }

        //[Required(ErrorMessage = "* Business Mailing Postal Code is required")]
        [Display(Name = "Mailing Postal Code")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "* No characters are allowed or number must contain 6 digits")]
        public string BusinessMailingPostalCode { get; set; }

        //[Required(ErrorMessage = "* Direct Line is required")]
        [Display(Name = "Direct Line")]
        [StringLength(20)]
        [DataType(DataType.PhoneNumber, ErrorMessage = "* No characters are allowed")]
        public string DirectLine { get; set; }

        [Display(Name = "Mobile Number")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits"
            )]
        public string Mobilenumber { get; set; }
        
        //[Required(ErrorMessage = "* Position Held is required")]
        [Display(Name = "Position Held")]
        [StringLength(100)]
        public string PositionHeld { get; set; }

        #endregion

        #region Common to individual & corporate

        // Common to individual & corporate
        [Display(Name = "DisplayName")]
        [StringLength(100)]
        [Remote("CheckDisplayNameAvailbilty", "Investor", AdditionalFields = "Emailaddress", HttpMethod = "POST", ErrorMessage = "* This name is already in use by other investor.")]

        public string DisplayName { get; set; }

        #endregion

        #region Specific Fields

        // Borrower specific
        public double PaidUpCapital { get; set; }

        // Individual investor specific
        public DateTime? DateofBirth { get; set; }
        public string Gender { get; set; }

        //[Required(ErrorMessage = "* Residential Address 1 is required")]
        [Display(Name = "Residential Address 1")]
        [StringLength(240)]
        public string ResidentialAddress1 { get; set; }

        //[Required(ErrorMessage = "* Residential Address 2 is required")]
        [Display(Name = "Residential Address 2")]
        [StringLength(240)]
        public string ResidentialAddress2 { get; set; }

        //[Required(ErrorMessage = "* Residential Address Postal Code is required")]
        [Display(Name = "Residential Address Postal Code")]
        [StringLength(6, MinimumLength = 6, ErrorMessage = "* No characters are allowed or number must contain 6 digits")]
        public string ResidentialPostalCode { get; set; }

        //[Required(ErrorMessage = "* Main Contact number is required")]
        [Display(Name = "Main Contact number")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "* No characters are allowed")]
        public string MainContactNumber { get; set; }

        #endregion

        #region Security questions fields

        // Security questions fields
        public int? SecurityQuestion1 { get; set; }
        public int? SecurityQuestion2 { get; set; }
        public int? SecurityQuestion3 { get; set; }

        public string SecurityQuestion1Name { get; set; }
        public string SecurityQuestion2Name { get; set; }
        public string SecurityQuestion3Name { get; set; }

        //[Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer1")]
        [StringLength(200)]
        public string SecurityAnswer1 { get; set; }

        //[Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer2")]
        [StringLength(200)]
        public string SecurityAnswer2 { get; set; }

        //[Required(ErrorMessage = "* Answer is required")]
        [Display(Name = "SecurityAnswer3")]
        [StringLength(200)]
        public string SecurityAnswer3 { get; set; }

        #endregion

        #region Admin Fields

        // Admin fields
        public string VerifiedDocumentIDs { get; set; }
        public string AdminCommentOnUserData { get; set; }
        public string AdminCommentonDocuments { get; set; }
        public string AdminPersonelComments { get; set; }

        #endregion

        // Helper fields
        public IEnumerable<tbl_BanksList> BanksList { get; set; }
        public IEnumerable<tbl_SecurityQuestions> SecurityQuestionList { get; set; }
        public IEnumerable<SelectListItem> Genders { get; set; }
        public IEnumerable<SelectListItem> Nationalities { get; set; }

        public bool IsLocked { get; set; }
        public bool? IsEmailVerified { get; set; }
    }
}