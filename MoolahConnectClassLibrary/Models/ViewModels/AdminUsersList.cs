﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class AdminUsersList
    {
        public AdminUsersList()
        {
            UserListAdmin = new List<UserListAdmin>();
        }

        public List<UserListAdmin> UserListAdmin { get; set; }
    }
    public class UserListAdmin
    {
        public long UserID { get; set; }
        public string UserName { get; set; }
        public string FullName { get; set; }
        public string UserRole { get; set; }
        public string UserType { get; set; }
        public string AccountNo { get; set; }
        public string AdminVerification { get; set; }
        public string IsSubmitted { get; set; }
        public string AdminPersonelComments { get; set; }
        public decimal? AvailableBalance { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string DisplayName { get; set; }
        public string BusinessName { get; set; }
        public bool IsLocked { get; set; }
        public string NRIC { get; set; }
        public string DigitalSignature { get; set; }
            
    }
}