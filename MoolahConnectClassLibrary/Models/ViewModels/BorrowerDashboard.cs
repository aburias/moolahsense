﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcPaging;

namespace MoolahConnectClassLibrary.Models.ViewModels
{

    public class BorrowerDashboard
    {
        public Dashboard Dashboard { get; set; }
        public CompanyProfile CompanyProfile { get; set; }
        public Common CommonOperations { get; set; }

        public ProfileDetail ProfileDetail { get; set; }
       
    }

    public class Dashboard
    {

        public List<LoanRequestDashboard> LoanRequestList { get; set; }
        public LoanRequestDashboard LoanRequestprops { get; set; }
        //public List<LoanOfferesDashboard> LoanOfferesDashboard { get; set; }
        public List<OutstandingPayments> OutstandingPayments { get; set; }
        public List<FullyFundedLoans> FullyFundedLoans { get; set; }
        public List<string> GraphOutstandingLoan { get; set; }
        public decimal? TotalOutstandingLoan { get; set; }
        public int NumberofOutstandingLoan { get; set; }

        public int TotalNumberofUniqueInvestor { get; set; }
        public decimal? HighestAmount { get; set; }
        public decimal? AverageAmount { get; set; }


    }

    public class CompanyProfile
    {
        public string CompanyName { get; set; }
        public string CompanyRegNumber { get; set; }
        public string TypeofCompany { get; set; }
        public string SiccCode { get; set; }
        public DateTime? DateofIncorporation { get; set; }
        public string RegisteredAddress1 { get; set; }
        public string RegisteredAddress2 { get; set; }
        public string RegisteredAddressPostalCode { get; set; }
        public string BusinessMailingAddress1 { get; set; }
        public string BusinessMailingAddress2 { get; set; }
        public string BusinessMailingPostalCode { get; set; }
        public double PaidUpCapital { get; set; }
        public string TelephoneofficeHours { get; set; }
        public string MobileOutofOfficeHours { get; set; }
        public string TelephoneOutOfficeHours { get; set; }
        public string Bank { get; set; }
        public string BankAccount { get; set; }
        public string AccountName { get; set; }
        public string FirstName { get; set; }
        public string Nationality { get; set; }
        public string NricNumber { get; set; }
        public string PassportNumber { get; set; }
        public string Role { get; set; }

        public string BankNumber { get; set; }
        public string BranchName { get; set; }
        public string BranchNumber { get; set; }
        
        public string OtherTelephone { get; set; }
        public string NatureofBussiness { get; set; }
        public string YearsinBssiness { get; set; }
        public string EmailAddress { get; set; }
        public string CreditInformation { get; set; }
        public string CreditPaymentGrade { get; set; }
        public int NumOfEmployees { get; set; }
        public string BankBranch { get; set; }
        public string Title { get; set; }
        public string LastName { get; set; }
    }

    public class ProfileDetail
    {
        public string Title  {get;set;}
        public string FirstName {get;set;}
        public string LastName {get;set;}
        public string DisplayName {get;set;}
        public string RoleJobTitle {get;set;} 
        public string [] DocumentUploaded{get;set;} 
    }

}
public class OutstandingPayments
{
    public string ReferenceID { get; set; }
    public decimal? EmiAmount { get; set; }
    public DateTime? DueDate { get; set; }
    public string Status { get; set; }
    public string Note { get; set; }


}
public class LoanRequestDashboard
{
    public string Reference { get; set; }
    public long LoanRequestID { get; set; }
    public string Purpose { get; set; }
    public string Tenor { get; set; }//time period in months
    public decimal? Rate { get; set; }
    public decimal? Amount { get; set; }
    public string PaymentGrade { get; set; }////////set by amin        
    public string OfferStatus { get; set; }
    public decimal? AvgFunded { get; set; }
    public decimal? AvgRate { get; set; }

    public double TotalInterestPayable { get; set; }
    public string Status { get; set; }
    public int LoanStatus { get; set; }
    public int PreliminaryStatus { get; set; }

    public DateTime AcceptedDate { get; set; }
}



public class BalancesDashboard
{
    public decimal? TotalAmountOutstanding { get; set; }
    public int NumberofOutstandingLoan { get; set; }
    public decimal AverageCost { get; set; }
    public int TotalUniqueInvestory { get; set; }
    public decimal HighestAmountbyUniqueInvestor { get; set; }
    public decimal AverageAmountbyInvestors { get; set; }

}

public class Common
{
    public string AccountName { get; set; }
    public string AccountNumber { get; set; }
    public string BusinessName { get; set; }
}

public class OutstandingLoans
{
    public long LoanReqestID { get; set; }
    public string LoanReference { get; set; }
    public string Tenor { get; set; }
    public decimal? Interest { get; set; }
    public decimal? LoanAmount { get; set; }
    public int MonthsTillMaturity { get; set; }
    public decimal? PrincipalPaid { get; set; }
    public decimal? InterestPadi { get; set; }
    public long RepaymentReference { get; set; }
    public decimal? EIR { get; set; }


}


public class MaturedLoans
{
    public long LoanReqestID { get; set; }
    public string  Purpose { get; set; }
    public string Tenor { get; set; }    
    public decimal? LoanAmount { get; set; }
    public string PaymentGrade { get; set; }
    public string PG { get; set; }
    public decimal? Interest { get; set; }
    public string MaturityDate { get; set; }
    public int NumberofLenders { get; set; }
    public string Referece { get; set; }
    public int LendersNUmber { get; set; }
}

public class FullyFundedLoans
{
    public string Reference { get; set; }
    public long LoanRequestID { get; set; }
    public string Purpose { get; set; }
    public string Tenor { get; set; }//time period in months
    public string Rate { get; set; }
    public decimal? Amount { get; set; }
    public string PaymentGrade { get; set; }////////set by amin        
    public string OfferStatus { get; set; }
}

public class GraphOustandingLoan
{
    public string Month { get; set; }
    public decimal? AggOutstandingAmount { get; set; }
    public decimal? CurrentRepayment { get; set; }
    public decimal? AggRepaymentForLiveLoans { get; set; }


}



