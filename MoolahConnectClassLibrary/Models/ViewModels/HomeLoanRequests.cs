﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class HomeLoanRequests
    {
        public List<HomeApprovedLoanRequests> ApprovedLoanList { get; set; }
    }
    public class HomeApprovedLoanRequests
    {
        public decimal? Amount {get;set;}
        public string RegisteredAddress {get;set;}
        public DateTime? DateCreated {get;set;}
        public string Term {get;set;}
        public decimal? AverageRate {get;set;}        
        public string TimeLeft {get;set;}
        public int days {get;set;}
        public int Hours {get;set;}
        public int minutest {get;set;}
        public long LoanRequestID { get; set; }
        public string Reference { get; set; }
        public string Purpose { get; set; }
        public decimal? Funded { get; set; }
        public int PreliminaryStatus { get; set; }
    }
}