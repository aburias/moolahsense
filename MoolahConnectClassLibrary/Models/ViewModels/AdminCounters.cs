﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class AdminCounters
    {
        public int TotalUsers { get; set; }
        public int TotalBorrower { get; set; }
        public int TotalInvestors { get; set; }
    }
}