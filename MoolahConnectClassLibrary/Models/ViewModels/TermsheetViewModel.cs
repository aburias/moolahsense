﻿using System;
using System.Collections.Generic;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class TermsheetViewModel
    {
        public long LoanId { get; set; }
        public DateTime? LoanAgreementDt { get; set; }
        public decimal PrincipalAmount { get; set; }
        public string Tenure { get; set; }
        public decimal AcceptedRate { get; set; }
        public decimal EffectiveRate { get; set; }
        public double TotalInterest { get; set; }
        public double RepaymentAmount { get; set; }
    }
}
