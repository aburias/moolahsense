﻿using MoolahConnect.Models.BusinessModels;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util;
using MoolahConnectClassLibrary.Infrastructure;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{

    public class LoanRequestDescription
    {
        public long RequestId { get; set; }

        [Required(ErrorMessage = "* Amount is required")]
        [Range(GlobalVar.gBorrowerMinAmount, GlobalVar.gBorrowerMaxAmount, ErrorMessage = GlobalVar.gBorrowerTextErrorString)]
        [DataType(DataType.Currency)]
        public decimal? Amount { get; set; }
        
        public string DisplayName { get; set; }
        public string Term { get; set; }

        [Required(ErrorMessage = "* Rate is required")]
        [Display(Name = "Rate")]
        public decimal Rate { get; set; }

        public decimal EIR { get; set; }

        public int? LoanPurposeId { get; set; }
        public IEnumerable<LoanPurpose> LoanpurposeList { get; set; }

        public string Questions { get; set; }
        public bool? IsotherLoantype { get; set; }


        public bool? IspersonalGuarantee { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _IspersonalGuarantee { get; set; }

        public string RequestStatus { get; set; }

        [Required(ErrorMessage = "* Detailed Company Profile is required")]
        public string DetailedCompanyPro { get; set; }

        [Required(ErrorMessage = "* Summary Company Profile is required")]
        public string SummaryCompanyPro { get; set; }
       
        public string Headline {get;set;}
      

        public int NumberOfEmployees { get; set; }


        // *******************************************
        // Required only if IspersonalGuaratee == TRUE
        //
       // public string NameasinNRICPassport { get; set; }
        
        //public string Residentialaddress1 { get; set; }
       // public string Residentialaddress2 { get; set; }
        
       // public string Postalcode { get; set; }

        
       // public string Telephone { get; set; }

        
       // public string Email { get; set; }

       // public string Reasons { get; set; }

        public string Accountnumber { get; set; }

        public string LoanImages { get; set; }

        public string LoanDocs { get; set; }

        public string LoanLogo { get; set; }

        ///////////////question and answer property for update///////////////
        public string QA { get; set; }
        public string DocumentsList { get; set; }

        
       // public string NricNumber { get; set; }

       // public string PassportNumber { get; set; }

        //public string Nationality { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _Nationality { get; set; }

        [Required(ErrorMessage = "* Loan purpose description is required")]
        public string LoanPurposeDescription { get; set; }

        public IList<PersonGuarantee> PersonGuarantees { get; set; }
      

    }

    public class PersonGuarantee
    {
        public PersonGuarantee()
        {
            this.ResidentialAddress1 = string.Empty;
            this.ResidentialAddress2 = string.Empty;
            this.NRICNo = string.Empty;
        }
        public int id { get; set; }
        [StringLength(50,ErrorMessage = "Too many characters")]
        [Required(ErrorMessage = "Required Information")]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        public string NameInNRIC { get; set; }
        [StringLength(50, ErrorMessage = "Too many characters")]
        [RegularExpression(@"[A-Za-z ]+", ErrorMessage = "* Only alphabets are allowed ")]
        [RequiredIf("Nationalityddl", conditionValue: "Other Nationalities", propertyNameInServerSide: "Nationality", ErrorMessage = "Required Information")]
        public string Nationality { get; set; }
        [StringLength(20, ErrorMessage = "Too many characters")]
        [Required(ErrorMessage = "Required Information")]
        [RegularExpression(@"^[a-z|A-Z]{1}\d{7}[a-z|A-Z]{1}$", ErrorMessage = "* NRIC / FIN number must be in format A1234567B ")]
        public string NRICNo { get; set; }
         [StringLength(20, ErrorMessage = "Too many characters")]
         [RequiredIf("Nationalityddl", conditionValue: "Singapore Citizen,Singapore PR", propertyNameInServerSide: "Nationality", conditionMode: RequiredIfConditionMode.NotEqual, ErrorMessage = "Required Information")]
        public string PassportNo { get; set; }
        [StringLength(200, ErrorMessage = "Too many characters")]
        [Required(ErrorMessage = "Required Information")]
        public string ResidentialAddress1 { get; set; }
        [StringLength(200, ErrorMessage = "Too many characters")]
        public string ResidentialAddress2 { get; set; }
        [Required(ErrorMessage = "Required Information")]
        [RegularExpression("^[0-9]{6}$", ErrorMessage = "Please enter a valid Postal Code")]
        public string PostalCode { get; set; }
        [DataType(DataType.PhoneNumber, ErrorMessage = "* No characters are allowed")]
        [Required(ErrorMessage = "Required Information")]
        [StringLength(8, MinimumLength = 8, ErrorMessage = "* No characters are allowed or number must contain 8 digits")] 
        public string Telephone { get; set; }
        [Required(ErrorMessage = "Required Information")]
        public string Reason { get; set; }
        [Required(ErrorMessage = "Required Information")]
        [RegularExpression(@"[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}", ErrorMessage = "Please enter a valid e-mail adress")]
        [StringLength(50)]
        public string Email { get; set; }
        public EntityState State { get; set; }

        public string AddressSpilt
        {
            set
            {
                var address = CommonMethods.LoadAddressField(value);
                this.ResidentialAddress1 = address[0];
                this.ResidentialAddress2 = address[2];
            }
        }
    }

    public enum EntityState
    {
        New,
        Update,
        Delete
    }
    
    public class LoanRequest
    {
        //public LoanRequest(LoanRequestDescription LoanRequestDescription)
        //{
        //    this.LoanRequestDescription = LoanRequestDescription;
        //}
        public LoanRequestDescription LoanRequestDescription { get; set; }
        public LoanRequestMoolahCore LoanRequestmoolahCore { get; set; }
        public LoanRequestMoolahPeri LoanRequestmoolahPeri { get; set; }
        public LoanRequestProfile LoanRequestProfile { get; set; }
        //public OutstandingLitigation Ou

    }
    public class LoanRequestProfile
    {
        public long AccountId { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string DisplayName { get; set; }
        public string AccountType { get; set; }
        public Nullable<long> User_ID { get; set; }
        public string Nationality { get; set; }
        public Nullable<bool> PRstatus { get; set; }
        public Nullable<System.DateTime> DateofBirth { get; set; }
        public string Gender { get; set; }
        public string ResidentialAddress { get; set; }
        public string PostalCode { get; set; }
        public string OptionalContactnumber { get; set; }
        public string EmailAddress { get; set; }
        public string Occupation { get; set; }
        public string IncomeRange { get; set; }
        public string BankBranch { get; set; }
        public string AccountNumber { get; set; }
        public string AccountName { get; set; }
        public string LevelofEducation { get; set; }
        public string FinancialInvestmentProducts { get; set; }
        public string MoolahConnectInvestment { get; set; }
        public string InvestmentObjectives { get; set; }
        public string ReactionOndefaulted { get; set; }
        public string RoleJobTitle { get; set; }
        public string BusinessName { get; set; }
        public string BusinessOrganisation { get; set; }
        public Nullable<int> Natureofbusiness_Id { get; set; }
        public Nullable<System.DateTime> DateofIncorporation { get; set; }
        public string RegisteredAddress { get; set; }
        public string BusinessMailingAddress { get; set; }
        public Nullable<int> Bank_Id { get; set; }
        public string Main_OfficeContactnumber { get; set; }
        public string Mobilenumber { get; set; }
        public string IC_CompanyRegistrationNumber { get; set; }
        public string NatureofBusiness { get; set; }

    }
    public class LoanRequestMoolahCore
    {
        public long ID { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public string LatestYear { get; set; }
        public string PreviousYear { get; set; }
        public Nullable<long> LoanRequest_Id { get; set; }
        public Nullable<long> User_ID { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? Turnovers_LY { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? NetprofitafterTax_LY { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? CurrentAssests_LY { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? CurrentLiabilities_LY { get; set; }
        public decimal? CurrentRatio_LY { get; set; }
        public decimal? CurrentRatio_PY { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? TotalDebt_LY { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? TotalShareholdersequity_LY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? ProfitBeforIntAndTax_LY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? InterestExpense_LY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? InterestCoverageRatio_LY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? EBITDA_LY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? DebtEBITDARatio_LY { get; set; }
        
        public decimal? Debt_Equity_LY { get; set; }
        
        public decimal? Debt_Equity_PY { get; set; }
        [Required(ErrorMessage = "The field is required.")]
        public decimal? CashFlowfromOperations_LY { get; set; }
        public decimal? Turnovers_PY { get; set; }
        public decimal? Turnovers_Value { get; set; }
        public decimal? NetprofitafterTax_PY { get; set; }
        public decimal? CurrentAssests_PY { get; set; }
        public decimal? CurrentLiabilities_PY { get; set; }
        public decimal? TotalDebt_PY { get; set; }
        public decimal? TotalShareholdersequity_PY { get; set; }
        public decimal? CashFlowfromOperations_PY { get; set; }
        public bool? IsoutstandingLitigation { get; set; }
        public string Accountnumber { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _Litigation { get; set; }
        public string LitigationType { get; set; }

        
        public decimal? ProfitBeforIntAndTax_PY { get; set; }
        public decimal? InterestExpense_PY { get; set; }
        public decimal? InterestCoverageRatio_PY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? EBITDA_PY { get; set; }

        [Required(ErrorMessage = "The field is required.")]
        public decimal? DebtEBITDARatio_PY { get; set; }
   
        public OutstandingLitigation Litigation { get; set; }
        public long MoolahCoreId { get; set; }

        
        public bool? FinancialStatementSubmitted { get; set; }
        public bool? Audited { get; set; }
        public double? AnnualRevenue { get; set; }
        public bool? Profitable { get; set; }
        public decimal? Gearing { get; set; }
        public string NumberOfCreditEnquires { get; set; }
        public string TotalLoanDueNext12Months { get; set; }
        public int? DPCreditPaymentGrade { get; set; }
        /////////////////properties use while updating, make string of outsanding litigation to show in a table
        public string OverdraftandFactoring { get; set; }
        public string Loans { get; set; }
        public string Trade { get; set; }
       public   List<OutstandingLitigation> obj { get; set; }///list to store the ids of litigation detail to assign on controller

        //properties for moolahconnect trackrecord
       public int LoanRequested { get; set; }
       public int LoanFunded { get; set; }
       public Decimal? LoanAmount { get; set; }
       public string Status { get; set; }
       public int LatePayment { get; set; }
       public int OutstandingAmount { get; set; }

       public string Turnovers_verification { get; set; }
       public string Debt_Equity_verification { get; set; }
       public string CurrentRatio_verification { get; set; }
       public string CashFlowfromOperations_verification { get; set; }

       public string Turnovers_Check { get; set; }
       public string CurrentRatio_Check { get; set; }
       public string CashFlowfromOperations_Check { get; set; }
       public string OutstandingLitigation_Check { get; set; }
       public string MorethenTwoYers { get; set; }
       [Required(ErrorMessage = "The field is required.")]
       public decimal? AvgThreeMntCashBalBankAcc_LY { get; set; }
       public decimal? AvgThreeMntCashBalBankAcc_PY { get; set; }
       public string AvgThreeMntCashBalBankAcc_Check { get; set; }
    }

    public class OutstandingLitigation
    {
        public long LitigationId { get; set; }
        public Decimal? AmountOutstanding { get; set; }
        public Decimal? CreditLimit { get; set; }

        public Decimal? UsageinLastMonth { get; set; }

        [DataType(DataType.Date, ErrorMessage = "* Only dates are allowed")]
        public DateTime? RenewalDate { get; set; }

        public Decimal? RepaymentAmount { get; set; }

        [DataType(DataType.Date, ErrorMessage = "* Only dates are allowed")]
        public DateTime? DateofLastPayment { get; set; }

        [StringLength(4000)]
        public string Description { get; set; }
        [StringLength(32)]
        public string LitigationType { get; set; }

        public int? PaymentTerms { get; set; }

        public long? MoolahCore_Id { get; set; }
        public int? User_ID { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> _RepaymentPeriod { get; set; }

        public string _RepaymentPeriodSet { get; set; }
    }
    public class LoanRequestMoolahPeri
    {
        
        [StringLength(500)]
        public string BusinessAwards { get; set; }
        [StringLength(500)]
        public string Accreditation { get; set; }
        [StringLength(500)]
        public string MTA { get; set; }
        [StringLength(500)]
        public string DigitalFootPrint { get; set; }
        [StringLength(500)]
        public string CommunityInitiative { get; set; }
        
        public string BiodataOfDS { get; set; }
        public Nullable<long> MoolahPerk_Id { get; set; }
        public string MoolahPerk_ddlCustom { get; set; }
        public Nullable<long> Request_ID { get; set; }
        public Nullable<long> User_ID { get; set; }
        public Nullable<System.DateTime> DateCreated { get; set; }
        public Nullable<System.DateTime> DateUpdated { get; set; }
        public String Accountnumber { get; set; }
        public IEnumerable<System.Web.Mvc.SelectListItem> MoolahPerk { get; set; }
        public string MoolahPerkset { get; set; }
        
        public string MoolahPerk_Custom { get; set; }

        public string BusinessQuardsIndicator { get; set; }
        public string AcreditationIndicator { get; set; }
        public string MTAIndicator { get; set; }
        public string DigitalFootPrintIndicator { get; set; }
        public string CommunityInitiativeIndicator { get; set; }
        public string BiodataOfDSIndicator { get; set; }
        public string MoolahPerkIndicator { get; set; }
        public string MoolahPerk_ddlCustomIndicator { get; set; }
        public long Request_IDIndicator { get; set; }
        public string AdminCommentsIndicator { get; set; }
        public string AccountnumberIndicator { get; set; }
        
        public string BusinessQuardsVerification { get; set; }
        public string AcreditationVerification { get; set; }
        public string MTAVerification { get; set; }
        public string DigitalFootPrintVerification { get; set; }
        public string CommunityInitiativeVerification { get; set; }
        public string BiodataOfDSVerification { get; set; }

        public string RespondToQuestion { get; set; }

        //public IEnumerable<tbl_PersonalGuaranteeInfo> LoanpurposeList { get; set; }
    }

    public class OverDraftCollection
    {
        [StringLength(10)]
        public string LitigationType { get; set; }
        public decimal AmountOutstanding { get; set; }
        public decimal CreditLimit { get; set; }

        public decimal UsageinLastMonth { get; set; }


        public DateTime RenewalDate { get; set; }
    }

    public class LoansCollection
    {
        public string LitigationType { get; set; }
        public decimal AmountOutstanding { get; set; }
        [StringLength(50)]
        public decimal RepaymentAmount { get; set; }
        public string _RepaymentPeriodSet { get; set; }
        public string DateofLastPayment { get; set; }
        public string Description { get; set; }
    }
    public class TradeCredeitorCollection
    {
        public string LitigationType { get; set; }
        public decimal AmountOutstanding { get; set; }
        public int PaymentTerms { get; set; }
    }


}