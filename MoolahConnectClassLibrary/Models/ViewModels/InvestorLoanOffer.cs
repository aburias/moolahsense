﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class InvestorLoanOffer
    {
        public LoanRequestInverstor LoanRequest { get; set; }
        public MoolahCore MoolahCore { get; set; }
        public InverstorLoanMoolahPeri MoolahPeri { get; set; }
        public AccountDetail AccountDetails { get; set; }
        public LoanRequestOffers LoanOffers { get; set; }
        public List<LoanOffersbyinvestors> offersbyInvesotr { get; set; }


        public InvestorLoanOffer()
        {
            MoolahCore = new MoolahCore();
            LoanRequest = new LoanRequestInverstor();
            MoolahPeri = new InverstorLoanMoolahPeri();
            AccountDetails = new AccountDetail();
            LoanOffers = new LoanRequestOffers();
        }
        public bool IsInvestor = false;
        public string LogoImage { get; set; }
        public string LogoImageName { get; set; }
        public bool IsPreview { get; set; }
    }
    public class MoolahCore
    {
        //////////////////////////////////Values to compare set by admin for moolah Core...This string will be set as yes or no after comparing it to value set by admin///////////
        public string NetProfit { get; set; }
        public string Cashflow { get; set; }
        public string Turnover { get; set; }
        public string Debt_Equity { get; set; }
        public string CurrentRation { get; set; }

        public string Turnovers_verification { get; set; }
        public string Debt_Equity_verification { get; set; }          
        public string CurrentRatio_verification { get; set; }     
        public string CashFlowfromOperations_verification { get; set; }
    }
    public class InverstorLoanMoolahPeri
    {
        /////////////////////////////////////Values to compare set by admin for moolah Core...This string will be set as yes or no after comparing it to value set by admin/////////////
        public bool? BussinessAwards { get; set; }
        public bool? Accridiation { get; set; }
        public bool? Trade { get; set; }///MTA
        public bool? Community { get; set; }
        public bool? DigitalFootPrint { get; set; }
        public bool? BiodataOfDS { get; set; }
        public string BusinessQuardsVerification { get; set; }
        public string AcreditationVerification { get; set; }
        public string MTAVerification { get; set; }
        public string DigitalFootPrintVerification { get; set; }
        public string CommunityInitiativeVerification { get; set; }
        public string BiodataOfDSVerification { get; set; }
        
        
    }


    public class LoanRequestInverstor
    {

        public decimal? Amount { get; set; }
        public string Term { get; set; }
        public decimal? EIR { get; set; }
        public string LoanPurpose { get; set; }
        public Nullable<bool> IspersonalGuarantee { get; set; }

        ////////////////////////////MoolahSense History//////////////////
        public int LoanRequested { get; set; }
        public decimal? LoanFunded { get; set; }
        public decimal? LoanAmount { get; set; }
        public string LatePayment { get; set; }
        public decimal? OutstandingAmount { get; set; }
        public decimal EMI { get; set; }
        public decimal? LoanFundedPercentage { get; set; }

        public bool HasExpiryDatePassed { get; set; }
        public string ExpiredMessage { get; set; }

        public DateTime? TransferedDate { get; set; }
        public DateTime? PublishedDate { get; set; }

        public DateTime? LastDay { get; set; }

    }
    public class AccountDetail
    {
       
        public string DisplayName { get; set; }
        public string EmailAddress { get; set; }
        public string Gender { get; set; }
        public string Nationality { get; set; }
        public bool? PRstatus { get; set; }
        public string ResidentialAddress { get; set; }
        public string MobileNumber { get; set; }
        public string AccountNumber { get; set; }
        public string BussinessName { get; set; }
        public string BussinessRegistrationNUmber { get; set; }
        public string BussinessOrganisation { get; set; }
        public string NatureofBussiness { get; set; }
        public string DateofIncorporation { get; set; }
    }
    public class LoanRequestOffers
    {
        public decimal LoanFunded { get; set; }
        public int NumberofOfferes { get; set; }
        public int UniqueBidders { get; set; }
        public decimal? Highestrate { get; set; }
        public decimal? Lowestrate { get; set; }
        public decimal? Averagerate { get; set; }
        public bool? IsApproved { get; set; }
        public bool Admisibility { get; set; }
        public int days { get; set; }
        public int Hours { get; set; }
        public int minutest { get; set; }
        public int? MiniMumOffer { get; set; }
        public decimal? MinimumRate { get; set; }
        public int LoanStatus { get; set; }
        public int PreliminaryStatus { get; set; }
        ///////////////////////////////////////loan offer by investor/////////////////////////////////
        [Required(ErrorMessage = "*")]
        [Display(Name = "Amount")]
        [StringLength(9)]
        [RegularExpression("^[0-9]{1,9}$", ErrorMessage = "*")]
        public decimal? Amount { get; set; }

        [Required(ErrorMessage = "*")]
        [Display(Name = "Rate")]
        [StringLength(5)]
        public decimal? Rate { get; set; }


        [Required(ErrorMessage = "*")]
        [Display(Name = "*")]                
        public string Password{ get; set; }

        public long LoanRequestID { get; set; }
        
    }




}
public class LoanOffersbyinvestors
{
    public decimal? OfferRate { get; set; }
    public DateTime? OfferDate { get; set; }
    public string UserName { get; set; }
    public decimal? Amount { get; set; }
    public decimal? AcceptedAmount { get; set; }
}