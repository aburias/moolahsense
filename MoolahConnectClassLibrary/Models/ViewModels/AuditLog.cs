﻿using System;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class AuditLog
    {
        public string Reference { get; set; }
        public string Message { get; set; }
        public string FieldName { get; set; }
        public string OldValue { get; set; }
        public string NewValue { get; set; }
        public string RequestId { get; set; }
        public string AccountId { get; set; }
        public string Username { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}