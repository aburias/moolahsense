﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MoolahConnectClassLibrary.Models.ViewModels
{
    public class FundingSummary
    {

        //declare for geting funding summary
        public decimal LoanFunded { get; set; }
        public int NumberofOfferes { get; set; }
        public int UniqueBidders { get; set; }
        public decimal? Highestrate { get; set; }
        public decimal? Lowestrate { get; set; }
        public decimal? Averagerate { get; set; }
        public bool Admisibility { get; set; }
        public int days { get; set; }
        public int Hours { get; set; }
        public int minutest { get; set; }
        public List<LoanOffersbyinvestors> offers { get; set; }
    }
}