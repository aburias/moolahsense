﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MoolahConnectClassLibrary.Infrastructure
{
    public enum RequiredIfConditionMode
    {
        Equal = 1,
        NotEqual
    }
    public class RequiredIfAttribute : ValidationAttribute, IClientValidatable
    {
        private String PropertyName { get; set; }
        private String PropertyNameInServerSide { get; set; }
        private string ConditionValue { get; set; }
        private Object DesiredValue { get; set; }
        private RequiredIfConditionMode ConditionMode { get; set; }

        public RequiredIfAttribute(String propertyName, string conditionValue = "", RequiredIfConditionMode conditionMode = RequiredIfConditionMode.Equal, string propertyNameInServerSide = "")
        {
            PropertyName = propertyName;
            this.ConditionValue = conditionValue == string.Empty ? "" : conditionValue;
            ConditionMode = conditionMode;
            this.PropertyNameInServerSide = propertyNameInServerSide;
            // DesiredValue = desiredvalue;
        }

        protected override ValidationResult IsValid(object value, ValidationContext context)
        {
            Object instance = context.ObjectInstance;
            Type type = instance.GetType();
            Object proprtyvalue = type.GetProperty(string.IsNullOrEmpty(PropertyNameInServerSide) ? PropertyName : PropertyNameInServerSide).GetValue(instance, null);
            if (ConditionMode == RequiredIfConditionMode.Equal)
            {
                if (ConditionValue.Split(',').Any(a => a == proprtyvalue.ToString()) && value == null)
                {
                    var errorMessage = FormatErrorMessage(context.DisplayName);
                    return new ValidationResult(errorMessage);
                }
            }
            else
            {
              
                if (!ConditionValue.Split(',').Any(a => a == proprtyvalue.ToString()) && value == null)
                {
                    var errorMessage = FormatErrorMessage(context.DisplayName);
                    return new ValidationResult(errorMessage);
                }
            }


            return ValidationResult.Success;
        }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule();
            rule.ErrorMessage = FormatErrorMessage(metadata.GetDisplayName());
            rule.ValidationParameters.Add("name", PropertyName);
            rule.ValidationParameters.Add("condition", ConditionValue);
            rule.ValidationParameters.Add("conditionmode", ConditionMode.ToString());
            rule.ValidationType = "ifrequired";
            yield return rule;
        }
    }
}
