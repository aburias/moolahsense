﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.UI;
using NLog;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web.Security;
using MoolahConnectClassLibrary.Models.ViewModels.Admin;
using System.Globalization;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using System.Data.Entity.Validation;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.GenerateReference;
using MoolahConnect.Util;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using EntityState = MoolahConnectClassLibrary.Models.ViewModels.EntityState;
using MoolahConnect.Util.Email;

namespace MoolahConnectClassLibrary.DAL
{
    public class AdminOperations
    {
        IEmailSender _IEmailSender;
        ILoanAllocationTask _ILoanAllocationTask;
        ITransactionsDA _ITransactionDA;

        public AdminOperations()
        {
            _IEmailSender = new EmailSender();
            _ILoanAllocationTask = new LoanAllocationTask();
            _ITransactionDA = new TransactionsDA();
        }

        dbMoolahConnectEntities db = new dbMoolahConnectEntities();
        #region Admin Home Page Counters
        /// <summary>
        /// This funcion will return the number of user borrower investors number of loan request
        /// </summary>
        /// <returns>Class AdminCounters</returns>
        //public AdminCounters GetStatisticsForAdmin()
        //{
        //    var AllUserRole = db.tbl_Users;
        //    AdminCounters obj = new AdminCounters();
        //    obj.TotalUsers = AllUserRole.Count();
        //    obj.TotalInvestors = AllUserRole.Where(l => l.UserRole == "Investor").Count();
        //    obj.TotalBorrower = AllUserRole.Where(l => l.UserRole == "Borrower").Count();

        //    return obj;


        //}

        public BorrowerAccounts GetAllUserDetailsByUserId(string userId)
        {
            var opr = new AccountOperations();
            var acc = new BorrowerAccounts
            {
                AccountDetails = (AdminAccountDetail)opr.GetUserDetailbyUserId(userId, true),
                Knowledgeassessment = new KnowledgeassessmentWrapper()
            };


            if (acc.AccountDetails != null && acc.AccountDetails.AccountType == @"Investor")
            {
                acc.Knowledgeassessment.Userid = userId;

                if (acc.AccountDetails.InvestorType == "Corporate")
                {
                    acc.Knowledgeassessment.Knowledgeassessment = new Knowledgeassessment
                        {
                            Parentlist =
                                db.tbl_Knowledgeassessment_Parentlist.Where(pr => pr.InvType == "C" || pr.InvType == "B")
                                  .OrderBy(p => p.Priority).ToList()
                        };
                }
                else
                {
                    acc.Knowledgeassessment.Knowledgeassessment = new Knowledgeassessment
                        {
                            Parentlist =
                                db.tbl_Knowledgeassessment_Parentlist.Where(pr => pr.InvType == "I" || pr.InvType == "B")
                                  .OrderBy(p => p.Priority)
                                  .ToList()
                        };
                }
            }

            return acc;
        }

        #endregion

        public void SaveMoolahCoreVerfication(LoanRequestMoolahCoreAdmin obj)
        {

            tbl_MoolahCoreVerification tbl_MoolahCoreVerificationObj = this.db.tbl_MoolahCoreVerification.SingleOrDefault<tbl_MoolahCoreVerification>(l => l.Request_ID == obj.RequestId);
            tbl_MoolahCore moolahcoreObj = this.db.tbl_MoolahCore.SingleOrDefault<tbl_MoolahCore>(l => l.LoanRequest_Id == obj.RequestId);

            if (moolahcoreObj == null)
            {
                var newMoolahCoreRow = new tbl_MoolahCore()
                {
                    LoanRequest_Id = obj.RequestId,
                    User_ID = obj.User_ID,
                };
                this.db.tbl_MoolahCore.Add(newMoolahCoreRow);
                this.db.SaveChanges();
                moolahcoreObj = this.db.tbl_MoolahCore.SingleOrDefault<tbl_MoolahCore>(l => l.LoanRequest_Id == obj.RequestId);
            }

            if (tbl_MoolahCoreVerificationObj != null)
            {

                tbl_MoolahCoreVerificationObj.LatestYearVerification = obj.LatestYearVerification;
                tbl_MoolahCoreVerificationObj.PreviousYearVerfication = obj.PreviousYearVerification;

                tbl_MoolahCoreVerificationObj.ProfitBeforIntAndTax_LY_Verification = obj.ProfitBeforIntAndTax_LY_Verification;
                tbl_MoolahCoreVerificationObj.ProfitBeforIntAndTax_PY_Verification = obj.ProfitBeforIntAndTax_PY_Verification;

                tbl_MoolahCoreVerificationObj.InterestExpense_LY_Verification = obj.InterestExpense_LY_Verification;
                tbl_MoolahCoreVerificationObj.InterestExpense_PY_Verification = obj.InterestExpense_PY_Verification;

                tbl_MoolahCoreVerificationObj.InterestCoverageRatio_LY_Verification = obj.InterestCoverageRatio_LY_Verification;
                tbl_MoolahCoreVerificationObj.InterestCoverageRatio_PY_Verification = obj.InterestCoverageRatio_PY_Verification;

                tbl_MoolahCoreVerificationObj.EBITDA_LY_Verification = obj.EBITDA_LY_Verification;
                tbl_MoolahCoreVerificationObj.EBITDA_PY_Verification = obj.EBITDA_PY_Verification;

                tbl_MoolahCoreVerificationObj.DebtEBITDARatio_LY_Verification = obj.DebtEBITDARatio_LY_Verification;
                tbl_MoolahCoreVerificationObj.DebtEBITDARatio_PY_Verification = obj.DebtEBITDARatio_PY_Verification;

                tbl_MoolahCoreVerificationObj.Turnover_LY_Verification = obj.Turnovers_LY_Verification;
                tbl_MoolahCoreVerificationObj.Turnover_PY_Verification = obj.Turnovers_PY_Verification;
                tbl_MoolahCoreVerificationObj.NetProfitAfterTax_LY_Verification = obj.NetprofitafterTax_LY_Verification;
                tbl_MoolahCoreVerificationObj.NetProfitAfterTax_PY_Verification = obj.NetprofitafterTax_PY_Verification;
                tbl_MoolahCoreVerificationObj.CurrentAssests_LY_Verification = obj.CurrentAssests_LY_Verification;
                tbl_MoolahCoreVerificationObj.CurrentAssests_PY_Verification = obj.CurrentAssests_PY_Verification;
                tbl_MoolahCoreVerificationObj.CurrentLiabilities_LY_Verification = obj.CurrentLiabilities_LY_Verification;
                tbl_MoolahCoreVerificationObj.CurrentLiabilities_PY_Verification = obj.CurrentLiabilities_PY_Verification;
                tbl_MoolahCoreVerificationObj.CurrentRatio_LY_Verification = obj.CurrentRatio_LY_Verification;
                tbl_MoolahCoreVerificationObj.CurrentRatio_PY_Verification = obj.CurrentRatio_PY_Verification;
                tbl_MoolahCoreVerificationObj.TotalShareholdersEquity_LY_Verification = obj.TotalShareholdersequity_LY_Verification;
                tbl_MoolahCoreVerificationObj.TotalShareholdersEquity_PY_Verification = obj.TotalShareholdersequity_PY_Verification;
                tbl_MoolahCoreVerificationObj.TotalDebt_LY_Verification = obj.TotalDebt_LY_Verification;
                tbl_MoolahCoreVerificationObj.TotalDebt_PY_Verification = obj.TotalDebt_PY_Verification;
                tbl_MoolahCoreVerificationObj.Debt_Equity_LY_Verification = obj.Debt_Equity_LY_Verification;
                tbl_MoolahCoreVerificationObj.Debt_Equity_PY_Verification = obj.Debt_Equity_PY_Verification;
                tbl_MoolahCoreVerificationObj.CashFlowFromOperations_LY_Verification = obj.CashFlowfromOperations_LY_Verification;
                tbl_MoolahCoreVerificationObj.CashFlowFromOperations_PY_Verification = obj.CashFlowfromOperations_PY_Verification;
                tbl_MoolahCoreVerificationObj.OutstandingLitigation_Verification_ = obj.IsoutstandingLitigation_Verification;
                tbl_MoolahCoreVerificationObj.DPCreditPaymentGrade_Verification = obj.DPCreditPayment_Verification;
                tbl_MoolahCoreVerificationObj.AvgThreeMntCashBalBankAcc_LY_Verification = obj.AvgThreeMntCashBalBankAcc_LY_Verification;
                tbl_MoolahCoreVerificationObj.AvgThreeMntCashBalBankAcc_PY_Verification = obj.AvgThreeMntCashBalBankAcc_PY_Verification;
                tbl_MoolahCoreVerificationObj.FinancialStatementSubmitted = obj.FinancialStatementSubmitted;
                tbl_MoolahCoreVerificationObj.FinancialStatementSubmitted_PY_Verification = obj.FinancialStatementSubmitted_PY;
                tbl_MoolahCoreVerificationObj.Audited = obj.Audited;
                tbl_MoolahCoreVerificationObj.Audited_PY_Verification = obj.Audited_PY_Verification;
                tbl_MoolahCoreVerificationObj.NumberOfCreditEnquires_Verification = obj.NumberOfCreditEnquires_Verification;
                if (obj.Turnovers_LY != null)
                {
                    tbl_MoolahCoreVerificationObj.AnnualRevenue = obj.Turnovers_LY;
                }

                if (obj.TotalDebt_LY.HasValue && obj.TotalShareholdersequity_LY.HasValue && obj.TotalShareholdersequity_LY.Value != 0)
                {
                    var gearing = obj.TotalDebt_LY.Value / obj.TotalShareholdersequity_LY;
                    moolahcoreObj.Gearing = gearing;
                }
                moolahcoreObj.DPCreditPaymentGrade = obj.DPCreditPaymentGrade;
                tbl_MoolahCoreVerificationObj.NumberOfCreditEnquires = obj.NumberOfCreditEnquires;
                moolahcoreObj.Profitable = obj.NetprofitafterTax_LY > 0;
                this.db.SaveChanges();
            }
            else
            {
                tbl_MoolahCoreVerification objverfi = new tbl_MoolahCoreVerification();

                objverfi.LatestYearVerification = obj.LatestYearVerification;
                objverfi.PreviousYearVerfication = obj.PreviousYearVerification;

                objverfi.Turnover_LY_Verification = obj.Turnovers_LY_Verification;
                objverfi.Turnover_PY_Verification = obj.Turnovers_PY_Verification;
                objverfi.NetProfitAfterTax_LY_Verification = obj.NetprofitafterTax_LY_Verification;
                objverfi.NetProfitAfterTax_PY_Verification = obj.NetprofitafterTax_PY_Verification;
                objverfi.CurrentAssests_LY_Verification = obj.CurrentAssests_LY_Verification;
                objverfi.CurrentAssests_PY_Verification = obj.CurrentAssests_PY_Verification;
                objverfi.CurrentLiabilities_LY_Verification = obj.CurrentLiabilities_LY_Verification;
                objverfi.CurrentLiabilities_PY_Verification = obj.CurrentLiabilities_PY_Verification;
                objverfi.CurrentRatio_LY_Verification = obj.CurrentRatio_LY_Verification;
                objverfi.CurrentRatio_PY_Verification = obj.CurrentRatio_PY_Verification;
                objverfi.TotalShareholdersEquity_LY_Verification = obj.TotalShareholdersequity_LY_Verification;
                objverfi.TotalShareholdersEquity_PY_Verification = obj.TotalShareholdersequity_PY_Verification;
                objverfi.TotalDebt_LY_Verification = obj.TotalDebt_LY_Verification;
                objverfi.TotalDebt_PY_Verification = obj.TotalDebt_PY_Verification;
                objverfi.Debt_Equity_LY_Verification = obj.Debt_Equity_LY_Verification;
                objverfi.Debt_Equity_PY_Verification = obj.Debt_Equity_PY_Verification;
                objverfi.CashFlowFromOperations_LY_Verification = obj.CashFlowfromOperations_LY_Verification;
                objverfi.CashFlowFromOperations_PY_Verification = obj.CashFlowfromOperations_PY_Verification;
                objverfi.OutstandingLitigation_Verification_ = obj.IsoutstandingLitigation_Verification;
                objverfi.DPCreditPaymentGrade_Verification = obj.DPCreditPayment_Verification;
                objverfi.AvgThreeMntCashBalBankAcc_LY_Verification = obj.AvgThreeMntCashBalBankAcc_LY_Verification;
                objverfi.AvgThreeMntCashBalBankAcc_PY_Verification = obj.AvgThreeMntCashBalBankAcc_PY_Verification;
                objverfi.NumberOfCreditEnquires_Verification = obj.NumberOfCreditEnquires_Verification;
                objverfi.Request_ID = obj.RequestId;

                objverfi.ProfitBeforIntAndTax_LY_Verification = obj.ProfitBeforIntAndTax_LY_Verification;
                objverfi.ProfitBeforIntAndTax_PY_Verification = obj.ProfitBeforIntAndTax_PY_Verification;

                objverfi.InterestExpense_LY_Verification = obj.InterestExpense_LY_Verification;
                objverfi.InterestExpense_PY_Verification = obj.InterestExpense_PY_Verification;

                objverfi.InterestCoverageRatio_LY_Verification = obj.InterestCoverageRatio_LY_Verification;
                objverfi.InterestCoverageRatio_PY_Verification = obj.InterestCoverageRatio_PY_Verification;

                objverfi.EBITDA_LY_Verification = obj.EBITDA_LY_Verification;
                objverfi.EBITDA_PY_Verification = obj.EBITDA_PY_Verification;

                objverfi.DebtEBITDARatio_LY_Verification = obj.DebtEBITDARatio_LY_Verification;
                objverfi.DebtEBITDARatio_PY_Verification = obj.DebtEBITDARatio_PY_Verification;

                objverfi.FinancialStatementSubmitted = obj.FinancialStatementSubmitted;
                objverfi.FinancialStatementSubmitted_PY_Verification = obj.FinancialStatementSubmitted_PY;
                objverfi.Audited = obj.Audited;
                objverfi.Audited_PY_Verification = obj.Audited_PY_Verification;
                if (obj.Turnovers_LY != null)
                {
                    objverfi.AnnualRevenue = obj.Turnovers_LY;
                }

                if (obj.TotalDebt_LY.HasValue && obj.TotalShareholdersequity_LY.HasValue && obj.TotalShareholdersequity_LY.Value != 0)
                {
                    var gearing = obj.TotalDebt_LY.Value / obj.TotalShareholdersequity_LY;
                    moolahcoreObj.Gearing = gearing;
                }
                objverfi.NumberOfCreditEnquires = obj.NumberOfCreditEnquires;
                moolahcoreObj.DPCreditPaymentGrade = obj.DPCreditPaymentGrade;
                moolahcoreObj.Profitable = obj.NetprofitafterTax_LY > 0;
                this.db.tbl_MoolahCoreVerification.Add(objverfi);
                this.db.SaveChanges();
            }

            moolahcoreObj = db.tbl_MoolahCore.SingleOrDefault(l => l.LoanRequest_Id == obj.RequestId);
            if (moolahcoreObj == null) return;

            moolahcoreObj.LatestYear = obj.LatestYear;
            moolahcoreObj.PreiviousYear = obj.PreviousYear;

            moolahcoreObj.ProfitBeforIntAndTax_LY = obj.ProfitBeforIntAndTax_LY;
            moolahcoreObj.ProfitBeforIntAndTax_PY = obj.ProfitBeforIntAndTax_PY;

            moolahcoreObj.InterestExpense_LY = obj.InterestExpense_LY;
            moolahcoreObj.InterestExpense_PY = obj.InterestExpense_PY;

            moolahcoreObj.InterestCoverageRatio_LY = obj.InterestCoverageRatio_LY;
            moolahcoreObj.InterestCoverageRatio_PY = obj.InterestCoverageRatio_PY;

            moolahcoreObj.EBITDA_LY = obj.EBITDA_LY;
            moolahcoreObj.EBITDA_PY = obj.EBITDA_PY;

            moolahcoreObj.DebtEBITDARatio_LY = obj.DebtEBITDARatio_LY;
            moolahcoreObj.DebtEBITDARatio_PY = obj.DebtEBITDARatio_PY;

            moolahcoreObj.Turnovers_LY = obj.Turnovers_LY;
            moolahcoreObj.Turnovers_PY = obj.Turnovers_PY;
            moolahcoreObj.NetprofitafterTax_LY = obj.NetprofitafterTax_LY;
            moolahcoreObj.NetprofitafterTax_PY = obj.NetprofitafterTax_PY;
            moolahcoreObj.CurrentAssests_LY = obj.CurrentAssests_LY;
            moolahcoreObj.CurrentAssests_PY = obj.CurrentAssests_PY;
            moolahcoreObj.CurrentLiabilities_LY = obj.CurrentLiabilities_LY;
            moolahcoreObj.CurrentLiabilities_PY = obj.CurrentLiabilities_PY;
            moolahcoreObj.CurrentRatio_LY = obj.CurrentRatio_LY;
            moolahcoreObj.CurrentRatio_PY = obj.CurrentRatio_PY;
            moolahcoreObj.TotalShareholdersequity_LY = obj.TotalShareholdersequity_LY;
            moolahcoreObj.TotalShareholdersequity_PY = obj.TotalShareholdersequity_PY;
            moolahcoreObj.TotalDebt_LY = obj.TotalDebt_LY;
            moolahcoreObj.TotalDebt_PY = obj.TotalDebt_PY;
            moolahcoreObj.Debt_Equity_LY = obj.Debt_Equity_LY;
            moolahcoreObj.Debt_Equity_PY = obj.Debt_Equity_PY;
            moolahcoreObj.CashFlowfromOperations_LY = obj.CashFlowfromOperations_LY;
            moolahcoreObj.CashFlowfromOperations_PY = obj.CashFlowfromOperations_PY;
            moolahcoreObj.AvgThreeMntCashBalBankAcc_LY = obj.AvgThreeMntCashBalBankAcc_LY;
            moolahcoreObj.AvgThreeMntCashBalBankAcc_PY = obj.AvgThreeMntCashBalBankAcc_PY;
            moolahcoreObj.IsoutstandingLitigation = obj.IsoutstandingLitigation;

            var adminUser = HttpContext.Current.User.Identity.Name;
            const string auditMsg = @"Update Note Request Moolah Core";
            var accDet = db.tbl_AccountDetails.SingleOrDefault(ac => ac.User_ID == moolahcoreObj.User_ID);
            long? accId = accDet != null ? accDet.AccountId : 0;

            ContollerExtensions.InsertAuditRecords(moolahcoreObj, db, auditMsg, adminUser, moolahcoreObj.LoanRequest_Id, accId);

            db.SaveChanges();
        }

        public void SaveMoolahPeriVerification(LoanRequestMoolahPeriAdmin obj)
        {
            try
            {
                var objloanrequest = db.tbl_LoanRequests.FirstOrDefault(l => l.RequestId == obj.Request_ID);

                if (objloanrequest == null) return;

                var str = (from p in db.tbl_Users
                           where p.UserID == objloanrequest.User_ID
                           select p.UserName).FirstOrDefault<string>();

                var adminUser = HttpContext.Current.User.Identity.Name;

                if (obj.FinalApprovalofLoanRequest == null)
                {
                    objloanrequest.IsApproved = null;
                    objloanrequest.LoanStatus = (int)LoanRequestStatus.Pending;
                    objloanrequest.ApproveDate = null;
                }
                else if (obj.FinalApprovalofLoanRequest.Value && (!objloanrequest.IsApproved.HasValue || !objloanrequest.IsApproved.Value))
                {
                    objloanrequest.IsApproved = true;
                    objloanrequest.PreliminaryLoanStatus = (int)PreliminaryLoanRequestStatus.DraftApproved;
                    objloanrequest.ApproveDate = DateTime.UtcNow;
                    MoolahLogManager.LogAdminAudit(AuditMessages.LOAN_LISTING_APPROVED_BY_ADMIN, HttpContext.Current.User.Identity.Name,
                        obj.Request_ID, objloanrequest.User_ID, "Approval", null, null);
                    _IEmailSender.OnApprovedLoanRequest(str);
                }
                else if (!obj.FinalApprovalofLoanRequest.Value && (objloanrequest.IsApproved.HasValue && objloanrequest.IsApproved.Value))
                {
                    objloanrequest.IsApproved = false;
                    objloanrequest.LoanStatus = (int)LoanRequestStatus.Rejected;
                    objloanrequest.PreliminaryLoanStatus = (int)PreliminaryLoanRequestStatus.DraftRejected;
                    objloanrequest.ApproveDate = null;
                    MoolahLogManager.LogAdminAudit(AuditMessages.LOAN_LISTING_REJECTED_BY_ADMIN, HttpContext.Current.User.Identity.Name,
                        obj.Request_ID, objloanrequest.User_ID, "Approval", null, null);
                    _IEmailSender.OnRejectedLoanRequest(str, obj.CommentofAdminforMoolahPeri);
                }

                var accDet = db.tbl_AccountDetails.SingleOrDefault(ac => ac.User_ID == objloanrequest.User_ID);
                long? accId = accDet != null ? accDet.AccountId : 0;
                // Create audit records
                ContollerExtensions.InsertAuditRecords(objloanrequest, db, "Update Loan Status", adminUser,
                                                       objloanrequest.RequestId, accId);

                db.SaveChanges();

                var verification = db.tbl_MoolahPeriVerification.FirstOrDefault(l => l.Request_ID == obj.Request_ID);
                if (verification != null)
                {
                    verification.BussinessAwardsVerification = obj.BussinesAwardsVerification;
                    verification.AcrediationVerification = obj.AcredationVerification;
                    verification.MTAVerificationVerification = obj.MemberofAssociationVerifcation;
                    verification.DigitalFootPrintVerification = obj.DigitalVerification;
                    verification.CommunityVerification = obj.CommunityVerification;
                    verification.BiodataofDSVerification = obj.BioDataofDirectorsVerification;
                    verification.MoolahPerkVerification = obj.MoolahPerkforLendersVerifcation;
                    verification.AdminComments = obj.CommentofAdminforMoolahPeri;
                    db.SaveChanges();
                }
                else
                {
                    var entity = new tbl_MoolahPeriVerification
                        {
                            BussinessAwardsVerification = obj.BussinesAwardsVerification,
                            AcrediationVerification = obj.AcredationVerification,
                            MTAVerificationVerification = obj.MemberofAssociationVerifcation,
                            DigitalFootPrintVerification = obj.DigitalVerification,
                            CommunityVerification = obj.CommunityVerification,
                            BiodataofDSVerification = obj.BioDataofDirectorsVerification,
                            MoolahPerkVerification = obj.MoolahPerkforLendersVerifcation,
                            Request_ID = obj.Request_ID,
                            DateCreated = DateTime.UtcNow,
                            AdminComments = obj.CommentofAdminforMoolahPeri
                        };
                    db.tbl_MoolahPeriVerification.Add(entity);
                    db.SaveChanges();
                }

                var moolahperiObj = db.tbl_MoolahPeri.SingleOrDefault(l => l.Request_ID == obj.Request_ID);
                if (moolahperiObj == null)
                {
                    moolahperiObj = new tbl_MoolahPeri();
                    objloanrequest.tbl_MoolahPeri.Add(moolahperiObj);
                }

                moolahperiObj.BusinessAwards = obj.BusinessAwards;
                moolahperiObj.Accreditation = obj.Accreditation;
                moolahperiObj.MTA = obj.MTA;
                moolahperiObj.DigitalFootPrint = obj.DigitalFootPrint;
                moolahperiObj.CommunityInitiative = obj.CommunityInitiative;
                moolahperiObj.BiodataOfDS = obj.BiodataOfDS;
                moolahperiObj.MoolahPerk_Id = long.Parse(obj.MoolahPerkset);
                moolahperiObj.MoolahPerk_Custom = obj.MoolahPerkset == "5" ? obj.MoolahPerk_Custom : null;
                moolahperiObj.UpdateMoolahPost = obj.UpdateMoolahPost;

                const string auditMsg = @"Update Note Request Moolah Peri";
                ContollerExtensions.InsertAuditRecords(moolahperiObj, db, auditMsg, adminUser, moolahperiObj.Request_ID, accId);

                db.SaveChanges();
            }
            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
            }
        }

        public void SavePersonlaGuranteeVerifcation(LoanRequestDescriptionAdmin obj)
        {
            try
            {
                string[] strArray;
                tbl_PersonalGuranteeVerification verification = this.db.tbl_PersonalGuranteeVerification.FirstOrDefault<tbl_PersonalGuranteeVerification>(l => l.Request_IdInfoVerification == obj.RequestId);
                if (verification != null)
                {
                    verification.NameAsinNRICVerification = obj.NameasinNRICPassportVerification;
                    verification.DesignationVerification = obj.DesignationVerification;
                    verification.NRIC_PassportVerification = obj.NRICPassportVerification;
                    verification.PassportNumber = obj.PassportNumberVerification;
                    var addVer = ContollerExtensions.AppendAddressField(AccountOperations.GetVerificationSymbol(obj.ResidentialaddressVerification1),
                                                           AccountOperations.GetVerificationSymbol(obj.ResidentialaddressVerification2), "");
                    verification.ResidentialAddressVerification = addVer;
                    verification.TelephoneVerification = obj.TelephoneVerificaiion;
                    verification.EmailVerification = obj.EmailVerificaiion;
                    verification.ReasonsVerification = obj.ReasonsVerificaiion;
                    verification.Request_IdInfoVerification = new long?(obj.RequestId);
                    verification.PostalcodeVerification = obj.PostalcodeVerification;
                    verification.NationalityVerification = obj.NationalityVerification;

                    this.db.SaveChanges();
                }
                else
                {
                    var addVer = ContollerExtensions.AppendAddressField(AccountOperations.GetVerificationSymbol(obj.ResidentialaddressVerification1),
                                                           AccountOperations.GetVerificationSymbol(obj.ResidentialaddressVerification2), "");
                    tbl_PersonalGuranteeVerification entity = new tbl_PersonalGuranteeVerification
                    {
                        NameAsinNRICVerification = obj.NameasinNRICPassportVerification,
                        DesignationVerification = obj.DesignationVerification,
                        NRIC_PassportVerification = obj.NRICPassportVerification,
                        PassportNumber = obj.PassportNumberVerification,

                        ResidentialAddressVerification = addVer,
                        TelephoneVerification = obj.TelephoneVerificaiion,
                        EmailVerification = obj.EmailVerificaiion,
                        ReasonsVerification = obj.ReasonsVerificaiion,
                        Request_IdInfoVerification = new long?(obj.RequestId),
                        PostalcodeVerification = obj.PostalcodeVerification,
                        NationalityVerification = obj.NationalityVerification
                    };
                    this.db.tbl_PersonalGuranteeVerification.Add(entity);
                    this.db.SaveChanges();
                }

                var loanRequest = db.tbl_LoanRequests.SingleOrDefault(r => r.RequestId == obj.RequestId);
                if (loanRequest == null) return;

                loanRequest.Amount = obj.Amount;
                loanRequest.Terms = obj.Term;
                loanRequest.Rate = obj.EIR;
                loanRequest.SummaryCompanyPro = obj.SummaryCompanyPro;
                loanRequest.DetailedCompanyPro = obj.DetailedCompanyPro;
                loanRequest.NumberOfEmployees = int.Parse(obj.NumberOfEmployees);
                loanRequest.LoanPurpose_Id = obj.LoanPurpose_Id;
                loanRequest.LoanPurposeDescription = obj.QA != null ? obj.QA : string.Empty;
                loanRequest.IsPersonalGuarantee = obj.IspersonalGuarantee;
                loanRequest.MoolahSenseComments = obj.MoolahSenseComments != null ? obj.MoolahSenseComments : string.Empty;
                loanRequest.Headline = obj.Headline;

                var adminUser = HttpContext.Current.User.Identity.Name;
                const string auditMsg = @"Update Note Request";
                var accDet = db.tbl_AccountDetails.SingleOrDefault(ac => ac.User_ID == loanRequest.User_ID);
                long? accId = accDet != null ? accDet.AccountId : 0;

                if (obj.IspersonalGuarantee != null && (bool)obj.IspersonalGuarantee)
                {
                    foreach (var item in obj.PersonGuarantees)
                    {


                        var info = db.tbl_PersonalGuaranteeInfo.SingleOrDefault(p => p.InfoId == item.id && p.Request_Id == obj.RequestId);
                        if (info == null)
                        {
                            info = new tbl_PersonalGuaranteeInfo();
                            loanRequest.tbl_PersonalGuaranteeInfo.Add(info);
                        }
                        if (item.State == EntityState.Delete)
                        {
                            
                            db.tbl_PersonalGuaranteeInfo.Remove(info);
                            db.SaveChanges();
                        }
                        else
                        {
                            info.NameAsinNRIC = item.NameInNRIC;
                            info.Nationality = item.Nationality;
                            info.NRIC_Passport = item.NRICNo;
                            info.PassportNumber = item.PassportNo;
                            var add = ContollerExtensions.AppendAddressField(item.ResidentialAddress1,
                                item.ResidentialAddress2, "");
                            info.ResidentialAddress = add;
                            info.Postalcode = item.PostalCode;
                            info.Telephone = item.Telephone;
                            info.Email = item.Email;
                            info.Reasons = item.Reason;
                        }
                        ContollerExtensions.InsertAuditRecords(info, db, auditMsg, adminUser, loanRequest.RequestId, accId);
                    }

                    
                }

                ContollerExtensions.InsertAuditRecords(loanRequest, db, auditMsg, adminUser, loanRequest.RequestId, accId);

                db.SaveChanges();
            }
            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
            }
        }

        public void SaveUserVerificationDetailbyUserID(AdminAccountDetail objAccount)
        {
            var opr = new AccountOperations();
            opr.SaveUserDetailbyUserId(objAccount);
        }

        public Tuple<AdminUsersList, int> GetUserListbyUserRole(string userRole, int currentPage, int pageSize,UserSearchCreteria creteria = null)
        {
            var totalPages = 0;
            try
            {
                if (userRole.ToLower() != "borrower" && userRole.ToLower() != "investor")
                {
                    userRole = string.Empty;
                }

                var queryable =
                    (from foo in this.db.tbl_Users.Where(u => string.IsNullOrEmpty(userRole) || u.UserRole == userRole)
                        join bar in this.db.tbl_AccountDetails on foo.UserID equals bar.User_ID into Bars
                        join per in this.db.tbl_UserVerification on foo.UserID equals per.UserID into Pers
                        from x in Pers.DefaultIfEmpty<tbl_UserVerification>()
                        from y in Bars.DefaultIfEmpty<tbl_AccountDetails>()

                        select new UserListAdmin()
                        {
                            UserID = foo.UserID,
                            UserName = foo.UserName,
                            CreatedDate = foo.DateCreated,
                            DisplayName = y.DisplayName ?? string.Empty,
                            BusinessName = y.BusinessName ?? string.Empty,
                            NRIC = y.NRIC_Number ?? string.Empty,
                            FullName = y.FirstName ?? string.Empty + " " + y.LastName ?? string.Empty,
                            UserType = foo.InvestorType ?? string.Empty,
                            AdminVerification = foo.AdminVerification,
                            IsSubmitted = foo.IsSubmitted != null && (bool) foo.IsSubmitted ? "Yes" : "No",
                            AdminPersonelComments = x.AdminPersonelComment,
                            DigitalSignature = foo.DigitalSignature,
                            UserRole = foo.UserRole ?? string.Empty,
                            AccountNo =  "dd"


                        });
              
                creteria = creteria ?? new UserSearchCreteria();
                
                ParameterExpression pe = Expression.Parameter(typeof(UserListAdmin));
                Expression resultExpression;
                resultExpression = GetExpressionStartWith(pe, "UserName", string.IsNullOrEmpty(creteria.UserName) ? string.Empty : creteria.UserName);

                if (!string.IsNullOrEmpty(creteria.BusinessName))
                {
                    resultExpression = Expression.AndAlso(resultExpression, GetExpressionStartWith(pe, "BusinessName", creteria.BusinessName));
                }
                if (!string.IsNullOrEmpty(creteria.FullName))
                {
                    resultExpression = Expression.AndAlso(resultExpression, GetExpressionStartWith(pe, "FullName", creteria.FullName));
                }
                if (!string.IsNullOrEmpty(creteria.DisplayName))
                {
                    resultExpression = Expression.AndAlso(resultExpression, GetExpressionStartWith(pe, "DisplayName", creteria.DisplayName));
                }
                if (!string.IsNullOrEmpty(creteria.NRIC))
                {
                    resultExpression = Expression.AndAlso(resultExpression, GetExpressionStartWith(pe, "NRIC", creteria.NRIC));
                }
          
            var wherecall = Expression.Call(typeof (Queryable), "Where", new Type[]{typeof(UserListAdmin)}, queryable.Expression,
                Expression.Lambda<Func<UserListAdmin, bool>>(resultExpression, new ParameterExpression[] { pe }));
            totalPages = queryable.Provider.CreateQuery<UserListAdmin>(wherecall).Count();
            var list = queryable.Provider.CreateQuery<UserListAdmin>(wherecall).OrderBy(a=> a.UserID)
                .Skip(currentPage* pageSize)
                .Take(pageSize).OrderBy(o => o.CreatedDate)
                .ToList();

                foreach (var item in list)
                {
                    item.AccountNo = CommonMethods.GetAccountNumber(item.UserID, item.UserRole);
                    item.AvailableBalance =
                        db.tbl_Balances.Where(l => l.User_ID == item.UserID)
                            .Select(l => l.LedgerAmount)
                            .FirstOrDefault();
                    item.IsLocked = Membership.GetUser(item.UserName) == null ||
                                    Membership.GetUser(item.UserName).IsLockedOut;

                }
              

                return
                    new Tuple<AdminUsersList, int>(
                        new AdminUsersList { UserListAdmin = list }, totalPages);

            }
            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
                return new Tuple<AdminUsersList, int>(new AdminUsersList(), 0);
            }
        }
        
        private static Expression GetExpressionStartWith(ParameterExpression param, string propertyName, string val)
        {
            MemberExpression member = Expression.Property(param, propertyName);
            var emptyString = Expression.Constant("");
            ConstantExpression constant = Expression.Constant(val);
            var restolower = Expression.Call(Expression.Coalesce(member, emptyString), typeof(string).GetMethod("ToLower", System.Type.EmptyTypes));
            return Expression.Call(restolower, typeof(string).GetMethod("Contains", new Type[] { typeof(string) }), constant);
           
        }
       
        //public LoanRequestAdmin LoanrequestLoad(long LoanrequestID)
        //{
        //    Func<tbl_MoolahPeri, bool> predicate = null;
        //    try
        //    {
        //        LoanRequestDescriptionAdmin admin2 = new LoanRequestDescriptionAdmin();
        //        LoanRequestMoolahCoreAdmin admin3 = new LoanRequestMoolahCoreAdmin();
        //        LoanRequestMoolahPeriAdmin admin4 = new LoanRequestMoolahPeriAdmin();

        //        admin2.LoanpurposeList = (from p in this.db.tbl_LoanPurposesList
        //                                  where p.Isactive == true
        //                                  orderby p.PurposeName
        //                                  select p).ToList<tbl_LoanPurposesList>();
        //        var type = (from l in this.db.tbl_LoanRequests
        //                    where l.RequestId == LoanrequestID
        //                    select new
        //                    {

        //                        RequestId = l.RequestId,
        //                        Amount = l.Amount,
        //                        Terms = l.Terms,
        //                        EIR = l.Rate,
        //                        DetailedCompanyPro = l.DetailedCompanyPro,
        //                        VideoDescription = l.VideoDescription,
        //                        Question = l.Question,
        //                        UserID = l.User_ID,
        //                        IsPersonalGuarantee = l.IsPersonalGuarantee,
        //                        PaymentTerms = l.PaymentTerms,
        //                        LoanStatus = l.LoanStatus,
        //                        LoanPurpose_Id = l.LoanPurpose_Id,
        //                        LoanPurposeDes = l.LoanPurposeDescription,
        //                        IsotherLoanType = l.IsotherLoanType,
        //                        //NameAsinNRIC = z.NameAsinNRIC,
        //                        //Designation = z.Designation,
        //                       // NRIC_Passport = z.NRIC_Passport,
        //                        //ResidentialAddress = z.ResidentialAddress,
        //                        //Postalcode = z.Postalcode,
        //                        //Telephone = z.Telephone,
        //                       // Email = z.Email,
        //                        //Reasons = z.Reasons,
        //                        IsApproved = l.IsApproved,
        //                        SummaryCompanyPro = l.SummaryCompanyPro,
        //                       // PassportNumber = z.PassportNumber,
        //                        NumberOfEmployees = l.NumberOfEmployees,
        //                        //Nationality = z.Nationality,
        //                        PreliminaryStatus = l.PreliminaryLoanStatus,
        //                        Username = l.tbl_Users != null ? l.tbl_Users.UserName : string.Empty,
        //                        MoolahSenseComments = l.MoolahSenseComments,
        //                        Headline = l.Headline
        //                    }).FirstOrDefault();
        //        admin2.RequestId = type.RequestId;
        //        admin2.Amount = type.Amount;
        //        admin2.DetailedCompanyPro = type.DetailedCompanyPro;
        //        admin2.videodescription = type.VideoDescription;
        //        admin2.Term = type.Terms;
        //        admin2.EIR = Convert.ToDecimal(type.EIR);
        //        admin2.IsotherLoantype = type.IsotherLoanType;
        //        admin2.Questions = type.Question;
        //        admin2.IspersonalGuarantee = type.IsPersonalGuarantee;
        //        admin2.RequestStatus = type.LoanStatus.ToLoanStatusDisplayName();
        //        admin2.SummaryCompanyPro = type.SummaryCompanyPro;
        //        admin2.NumberOfEmployees = type.NumberOfEmployees.HasValue ? type.NumberOfEmployees.Value.ToString() : "0";
        //        admin2.Username = type.Username;
        //        admin2.MoolahSenseComments = type.MoolahSenseComments;
        //        admin2.Headline = type.Headline;

        //        var user = db.tbl_Users.Where(x => x.UserID == type.UserID).SingleOrDefault();

        //        if (user != null)
        //            admin2.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);

        //        tbl_PersonalGuranteeVerification verification = this.db.tbl_PersonalGuranteeVerification.FirstOrDefault<tbl_PersonalGuranteeVerification>(l => l.Request_IdInfoVerification == LoanrequestID);
        //        if (verification != null)
        //        {
        //            admin2.NameasinNRICPassportVerification = verification.NameAsinNRICVerification;
        //            admin2.DesignationVerification = verification.DesignationVerification;
        //            admin2.NRICPassportVerification = verification.NRIC_PassportVerification;
        //            var addVer = CommonMethods.LoadAddressField(verification.ResidentialAddressVerification);
        //            admin2.ResidentialaddressVerification1 = AccountOperations.GetVerificationPhrase(addVer[0]);
        //            admin2.ResidentialaddressVerification2 = AccountOperations.GetVerificationPhrase(addVer[1]);
        //            admin2.PostalcodeVerification = verification.PostalcodeVerification;
        //            admin2.TelephoneVerificaiion = verification.TelephoneVerification;
        //            admin2.EmailVerificaiion = verification.EmailVerification;
        //            admin2.ReasonsVerificaiion = verification.ReasonsVerification;
        //            admin2.PassportNumberVerification = verification.PassportNumber;
        //            admin2.NationalityVerification = verification.NationalityVerification;
        //        }

        //        admin2.LoanPurpose_Id = type.LoanPurpose_Id;
        //        admin2.QA = type.LoanPurposeDes;
        //        admin2.IspersonalGuarantee = type.IsPersonalGuarantee;

        //        //var add = CommonMethods.LoadAddressField(type.ResidentialAddress);

        //        if (admin2.IspersonalGuarantee == true)
        //        {
        //            //admin2.NameasinNRICPassport = type.NameAsinNRIC;
        //            //admin2.Designation = type.Designation;
        //            //admin2.NRICPassport = type.NRIC_Passport;
        //            //admin2.Residentialaddress1 = add[0];
        //            //admin2.Residentialaddress2 = add[1];
        //            //admin2.Telephone = type.Telephone;
        //            //admin2.Email = type.Email;
        //            //admin2.Reasons = type.Reasons;
        //            //admin2.Postalcode = type.Postalcode;
        //            //admin2.Nationality = type.Nationality;
        //            //admin2.PassportNumber = type.PassportNumber;// TODO:

        //            admin2.PersonGuarantees =
        //                db.tbl_PersonalGuaranteeInfo.Where(a => a.Request_Id == LoanrequestID)
        //                .ToList()
        //                    .Select(a=> new PersonGuarantee()
        //                    {
        //                        id = a.InfoId,
        //                        Email = a.Email,
        //                        NameInNRIC = a.NameAsinNRIC,
        //                        Nationality = a.Nationality,
        //                        NRICNo = a.NRIC_Passport,
        //                        PassportNo = a.PassportNumber,
        //                        PostalCode = a.Postalcode,
        //                        Reason = a.Reasons,
        //                        AddressSpilt = a.ResidentialAddress,
        //                        Telephone = a.Telephone
                                
        //                    }).ToList();

        //        }
        //        LoanRequestMoolahCoreAdmin admin5 = new LoanRequestMoolahCoreAdmin();
        //        tbl_MoolahCore MoolahCoreDetail = this.db.tbl_MoolahCore.FirstOrDefault<tbl_MoolahCore>(p => p.LoanRequest_Id == LoanrequestID);
        //        OutstandingLitigationAdmin admin6 = new OutstandingLitigationAdmin
        //        {
        //            _RepaymentPeriod = new SelectList(this.MoolaCoreTime(), "Value", "Text")
        //        };
        //        if (MoolahCoreDetail != null)
        //        {
        //            admin5.MoolahCoreId = MoolahCoreDetail.MoolahCoreId;
        //            admin5.Turnovers_LY = MoolahCoreDetail.Turnovers_LY;



        //            admin5.LatestYear = MoolahCoreDetail.LatestYear;
        //            admin5.PreviousYear = MoolahCoreDetail.PreiviousYear;

        //            admin5.ProfitBeforIntAndTax_LY = MoolahCoreDetail.ProfitBeforIntAndTax_LY;
        //            admin5.ProfitBeforIntAndTax_PY = MoolahCoreDetail.ProfitBeforIntAndTax_PY;

        //            admin5.InterestExpense_LY = MoolahCoreDetail.InterestExpense_LY;
        //            admin5.InterestExpense_PY = MoolahCoreDetail.InterestExpense_PY;

        //            admin5.InterestCoverageRatio_LY = MoolahCoreDetail.InterestCoverageRatio_LY;
        //            admin5.InterestCoverageRatio_PY = MoolahCoreDetail.InterestCoverageRatio_PY;

        //            admin5.EBITDA_LY = MoolahCoreDetail.EBITDA_LY;
        //            admin5.EBITDA_PY = MoolahCoreDetail.EBITDA_PY;

        //            admin5.DebtEBITDARatio_LY = MoolahCoreDetail.DebtEBITDARatio_LY;
        //            admin5.DebtEBITDARatio_PY = MoolahCoreDetail.DebtEBITDARatio_PY;

        //            admin5.NetprofitafterTax_LY = MoolahCoreDetail.NetprofitafterTax_LY;
        //            admin5.CurrentAssests_LY = MoolahCoreDetail.CurrentAssests_LY;
        //            admin5.CurrentRatio_LY = MoolahCoreDetail.CurrentRatio_LY;
        //            admin5.TotalDebt_LY = MoolahCoreDetail.TotalDebt_LY;
        //            admin5.TotalShareholdersequity_LY = MoolahCoreDetail.TotalShareholdersequity_LY;
        //            admin5.Debt_Equity_LY = MoolahCoreDetail.Debt_Equity_LY;
        //            admin5.CashFlowfromOperations_LY = MoolahCoreDetail.CashFlowfromOperations_LY;
        //            admin5.CurrentAssests_PY = MoolahCoreDetail.CurrentAssests_PY;
        //            admin5.CurrentLiabilities_PY = MoolahCoreDetail.CurrentLiabilities_PY;
        //            admin5.TotalDebt_PY = MoolahCoreDetail.TotalDebt_PY;
        //            admin5.TotalShareholdersequity_PY = MoolahCoreDetail.TotalShareholdersequity_PY;
        //            admin5.Debt_Equity_PY = MoolahCoreDetail.Debt_Equity_PY;
        //            admin5.CashFlowfromOperations_PY = MoolahCoreDetail.CashFlowfromOperations_PY;
        //            admin5.IsoutstandingLitigation = MoolahCoreDetail.IsoutstandingLitigation;
        //            admin5.CurrentLiabilities_LY = MoolahCoreDetail.CurrentLiabilities_LY;
        //            admin5.Turnovers_PY = MoolahCoreDetail.Turnovers_PY;
        //            admin5.CurrentRatio_PY = MoolahCoreDetail.CurrentRatio_PY;
        //            admin5.NetprofitafterTax_PY = MoolahCoreDetail.NetprofitafterTax_PY;
        //            admin5.AvgThreeMntCashBalBankAcc_LY = MoolahCoreDetail.AvgThreeMntCashBalBankAcc_LY;
        //            admin5.AvgThreeMntCashBalBankAcc_PY = MoolahCoreDetail.AvgThreeMntCashBalBankAcc_PY;
        //            admin5.OverdraftandFactoring = (from o in this.db.tbl_OutStandingLitigation
        //                                            where (o.MoolahCore_Id == MoolahCoreDetail.MoolahCoreId) && ((o.LitigationType == "Factoring Discount") || (o.LitigationType == "Overdraft Facility"))
        //                                            select o).ToList<tbl_OutStandingLitigation>();
        //            admin5.Loans = (from o in this.db.tbl_OutStandingLitigation
        //                            where (o.MoolahCore_Id == MoolahCoreDetail.MoolahCoreId) && ((o.LitigationType == "Unsecured Loan") || (o.LitigationType == "Secured Loan"))
        //                            select o).ToList<tbl_OutStandingLitigation>();
        //            admin5.Trade = (from o in this.db.tbl_OutStandingLitigation
        //                            where (o.MoolahCore_Id == MoolahCoreDetail.MoolahCoreId) && ((o.LitigationType.Trim() == "Trade Creditors") || (o.LitigationType.Trim() == "Trade Creditors"))
        //                            select o).ToList<tbl_OutStandingLitigation>();
        //            admin5.LitigationType = string.Empty;


        //            if (user != null)
        //                admin5.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);
        //        }

        //        LoanRequestMoolahPeriAdmin admin7 = new LoanRequestMoolahPeriAdmin
        //        {
        //            Accountnumber = user != null ?
        //               admin2.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole) : string.Empty
        //        };
        //        if (predicate == null)
        //        {
        //            predicate = delegate(tbl_MoolahPeri p)
        //            {
        //                long? nullable = p.Request_ID;
        //                long loanrequestID = LoanrequestID;
        //                return (nullable.GetValueOrDefault() == loanrequestID) && nullable.HasValue;
        //            };
        //        }
        //        tbl_MoolahPeri peri = this.db.tbl_MoolahPeri.ToList<tbl_MoolahPeri>().FirstOrDefault<tbl_MoolahPeri>(predicate);
        //        admin7.MoolahPerk = this.AddCoustomValueinEnd();
        //        admin7.FinalApprovalofLoanRequest = type.IsApproved;
        //        admin7.PreliminaryStatus = type.PreliminaryStatus;
        //        admin7.LoanStatus = type.LoanStatus;
        //        if (peri != null)
        //        {
        //            admin7.BusinessAwards = peri.BusinessAwards;
        //            admin7.Accreditation = peri.Accreditation;
        //            admin7.MTA = peri.MTA;
        //            admin7.DigitalFootPrint = peri.DigitalFootPrint;
        //            admin7.CommunityInitiative = peri.CommunityInitiative;
        //            admin7.BiodataOfDS = peri.BiodataOfDS;
        //            admin7.MoolahPerk_Id = peri.MoolahPerk_Id;
        //            admin7.MoolahPerk_Custom = peri.MoolahPerk_Custom;
        //            admin7.UpdateMoolahPost = peri.UpdateMoolahPost.HasValue ? peri.UpdateMoolahPost.Value : false;

        //            if (peri.MoolahPerk_Id.HasValue)
        //            {
        //                admin7.MoolahPerkset = peri.MoolahPerk_Id.ToString();
        //            }
        //            else
        //            {
        //                admin7.MoolahPerkset = "Custom";
        //            }
        //        }
        //        tbl_MoolahPeriVerification verification2 = this.db.tbl_MoolahPeriVerification.FirstOrDefault<tbl_MoolahPeriVerification>(l => l.Request_ID == LoanrequestID);
        //        if (verification2 != null)
        //        {
        //            admin7.BussinesAwardsVerification = verification2.BussinessAwardsVerification;
        //            admin7.AcredationVerification = verification2.AcrediationVerification;
        //            admin7.MemberofAssociationVerifcation = verification2.MTAVerificationVerification;
        //            admin7.DigitalVerification = verification2.DigitalFootPrintVerification;
        //            admin7.CommunityVerification = verification2.CommunityVerification;
        //            admin7.BioDataofDirectorsVerification = verification2.BiodataofDSVerification;
        //            admin7.CommentofAdminforMoolahPeri = verification2.AdminComments;
        //            admin7.MoolahPerkforLendersVerifcation = verification2.MoolahPerkVerification;
        //            admin7.RequestStatus = type.LoanStatus.ToLoanStatusDisplayName();
        //        }

        //        tbl_MoolahCoreVerification CoreVerification = this.db.tbl_MoolahCoreVerification.FirstOrDefault<tbl_MoolahCoreVerification>(l => l.Request_ID == LoanrequestID);
        //        if (CoreVerification != null)
        //        {
        //            admin5.ProfitBeforIntAndTax_LY_Verification = CoreVerification.ProfitBeforIntAndTax_LY_Verification;
        //            admin5.ProfitBeforIntAndTax_PY_Verification = CoreVerification.ProfitBeforIntAndTax_PY_Verification;

        //            admin5.InterestExpense_LY_Verification = CoreVerification.InterestExpense_LY_Verification;
        //            admin5.InterestExpense_PY_Verification = CoreVerification.InterestExpense_PY_Verification;

        //            admin5.InterestCoverageRatio_LY_Verification = CoreVerification.InterestCoverageRatio_LY_Verification;
        //            admin5.InterestCoverageRatio_PY_Verification = CoreVerification.InterestCoverageRatio_PY_Verification;

        //            admin5.EBITDA_LY_Verification = CoreVerification.EBITDA_LY_Verification;
        //            admin5.EBITDA_PY_Verification = CoreVerification.EBITDA_PY_Verification;

        //            admin5.DebtEBITDARatio_LY_Verification = CoreVerification.DebtEBITDARatio_LY_Verification;
        //            admin5.DebtEBITDARatio_PY_Verification = CoreVerification.DebtEBITDARatio_PY_Verification;

        //            admin5.LatestYearVerification = CoreVerification.LatestYearVerification;
        //            admin5.PreviousYearVerification = CoreVerification.PreviousYearVerfication;
        //            admin5.Turnovers_LY_Verification = CoreVerification.Turnover_LY_Verification;
        //            admin5.Turnovers_PY_Verification = CoreVerification.Turnover_PY_Verification;
        //            admin5.DPCreditPaymentGrade = MoolahCoreDetail.DPCreditPaymentGrade;
        //            admin5.NetprofitafterTax_LY_Verification = CoreVerification.NetProfitAfterTax_LY_Verification;
        //            admin5.NetprofitafterTax_PY_Verification = CoreVerification.NetProfitAfterTax_PY_Verification;
        //            admin5.CurrentAssests_LY_Verification = CoreVerification.CurrentAssests_LY_Verification;
        //            admin5.CurrentAssests_PY_Verification = CoreVerification.CurrentAssests_PY_Verification;
        //            admin5.CurrentLiabilities_LY_Verification = CoreVerification.CurrentLiabilities_LY_Verification;
        //            admin5.CurrentLiabilities_PY_Verification = CoreVerification.CurrentLiabilities_PY_Verification;
        //            admin5.CurrentRatio_LY_Verification = CoreVerification.CurrentRatio_LY_Verification;
        //            admin5.CurrentRatio_PY_Verification = CoreVerification.CurrentRatio_PY_Verification;
        //            admin5.TotalShareholdersequity_LY_Verification = CoreVerification.TotalShareholdersEquity_LY_Verification;
        //            admin5.TotalShareholdersequity_PY_Verification = CoreVerification.TotalShareholdersEquity_PY_Verification;
        //            admin5.TotalDebt_LY_Verification = CoreVerification.TotalDebt_LY_Verification;
        //            admin5.TotalDebt_PY_Verification = CoreVerification.TotalDebt_PY_Verification;
        //            admin5.Debt_Equity_LY_Verification = CoreVerification.Debt_Equity_LY_Verification;
        //            admin5.Debt_Equity_PY_Verification = CoreVerification.Debt_Equity_PY_Verification;
        //            admin5.CashFlowfromOperations_LY_Verification = CoreVerification.CashFlowFromOperations_LY_Verification;
        //            admin5.CashFlowfromOperations_PY_Verification = CoreVerification.CashFlowFromOperations_PY_Verification;
        //            admin5.IsoutstandingLitigation_Verification = CoreVerification.OutstandingLitigation_Verification_;
        //            admin5.DPCreditPayment_Verification = CoreVerification.DPCreditPaymentGrade_Verification;
        //            admin5.AvgThreeMntCashBalBankAcc_LY_Verification = CoreVerification.AvgThreeMntCashBalBankAcc_LY_Verification;
        //            admin5.AvgThreeMntCashBalBankAcc_PY_Verification = CoreVerification.AvgThreeMntCashBalBankAcc_PY_Verification;
        //            admin5.FinancialStatementSubmitted = CoreVerification.FinancialStatementSubmitted;
        //            admin5.FinancialStatementSubmitted_PY = CoreVerification.FinancialStatementSubmitted_PY_Verification;
        //            admin5.Audited = CoreVerification.Audited;
        //            admin5.Audited_PY_Verification = CoreVerification.Audited_PY_Verification;
        //            admin5.AnnualRevenue = (double?)CoreVerification.AnnualRevenue;
        //            admin5.Profitable = MoolahCoreDetail.Profitable;
        //            admin5.Profitable_PY = MoolahCoreDetail.Profitable_PY;
        //            admin5.Gearing = MoolahCoreDetail.Gearing;
        //            admin5.NumberOfCreditEnquires = CoreVerification.NumberOfCreditEnquires;
        //            admin5.NumberOfCreditEnquires_Verification = CoreVerification.NumberOfCreditEnquires_Verification;
        //        }
        //        LoanRequestAdmin admin = new LoanRequestAdmin
        //        {
        //            LoanRequestDescription = admin2
        //        };
        //        admin2._IspersonalGuarantee = new SelectList(this.IsPersonalguarantee(), "Value", "Text", admin2.IspersonalGuarantee.ToString());
        //        admin4.MoolahPerk = this.AddCoustomValueinEnd();
        //        //var repaymentMessageList =
        //        //    db.tbl_RepaymentMessages
        //        //    .Where(a => a.LoanRequestId == LoanrequestID)
        //        //    .OrderByDescending(a=> a.CreatedDate).ToList()
        //        //    .Select(a=> new RepaymentMessage()
        //        //    {
        //        //        Id = a.ID,
        //        //        Message = a.Message,
        //        //        LoanRequestId = LoanrequestID,
        //        //        CreatedDateTime = a.CreatedDate.HasValue ? a.CreatedDate.Value.ToString("dd MMM yyyy HH:mm") : DateTime.Now.ToString("dd MMM yyyy HH:mm"),
        //        //        State = EntityState.Update,
                        
        //        //    })
        //        //    .ToList();
        //        return new LoanRequestAdmin { LoanRequestDescription = admin2, LoanRequestmoolahCore = admin5, LoanRequestmoolahPeri = admin7, RepaymentMessages = repaymentMessageList };
        //    }
        //    catch (Exception exception)
        //    {
        //        MoolahLogManager.LogException(exception);
        //        return null;
        //    }
        //}

        //public void SaveRepaymentMessage(RepaymentMessage message)
        //{
        //    var repaymentMessage = db.tbl_RepaymentMessages.Where(a => a.ID == message.Id).FirstOrDefault();
        //    tbl_RepaymentMessages newRepayMessage = null;
        //        switch (message.State)
        //        {
        //            case EntityState.Delete:
        //                db.tbl_RepaymentMessages.Remove(repaymentMessage);
        //                db.SaveChanges();
        //                break;
        //                case EntityState.New:
        //                 newRepayMessage = new tbl_RepaymentMessages()
        //                {
        //                    LoanRequestId = message.LoanRequestId,
        //                    Message = message.Message,
        //                    CreatedDate = DateTime.Now
        //                };
        //                db.tbl_RepaymentMessages.Add(newRepayMessage);
                       
        //                    break;
        //            case EntityState.Update:
        //                repaymentMessage.Message = message.Message;
        //                break;
        //        }
        //        db.SaveChanges();
        //    if (newRepayMessage != null)
        //    {
        //        message.CreatedDateTime = newRepayMessage.CreatedDate.Value.ToString("dd MMM yyyy HH:mm");
        //        message.Id = newRepayMessage.ID;
        //    }
        //}

        public AdminLoanList GetAllLoanRequestForAdmin()
        {
            try
            {
                var loanList = new List<LoanListForAdmin>();

                loanList = (from lr in db.tbl_LoanRequests.AsEnumerable()
                            select new LoanListForAdmin()
                            {
                                AccountNumber = lr.tbl_Users != null
                                ? CommonMethods.GetAccountNumber(lr.tbl_Users.UserID, lr.tbl_Users.UserRole) : string.Empty,
                                CompanyName = lr.tbl_Users != null && lr.tbl_Users.tbl_AccountDetails.FirstOrDefault() != null
                                ? lr.tbl_Users.tbl_AccountDetails.FirstOrDefault().AccountName : string.Empty,
                                Amount = lr.Amount,
                                IsApproved = lr.IsApproved.HasValue && lr.IsApproved.Value ? "Yes" : "No",
                                RequestID = lr.RequestId,
                                PreliminaryStatus = lr.PreliminaryLoanStatus,
                                Status = lr.LoanStatus,
                                UserName = lr.tbl_Users != null ? lr.tbl_Users.UserName : string.Empty,
                                DateCreated = lr.DateCreated,
                                TimeLeft = lr.PublishedDate.HasValue ? _ILoanAllocationTask.TimeLetfToExpireLoanRequest(lr.PublishedDate.Value)
                                : (lr.ApproveDate.HasValue ? _ILoanAllocationTask.TimeLetfToExpireLoanDraft(lr.ApproveDate.Value) : new int[0]),
                                Submitted = (lr.Submitted != null && (bool)lr.Submitted) ? "Yes" : "No"
                            }).ToList();

                return new AdminLoanList { LoanListForAdmin = loanList };
            }
            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
                return null;
            }
        }

        private List<SelectListItem> IsPersonalguarantee()
        {
            List<SelectListItem> list2 = new List<SelectListItem>();
            SelectListItem item = new SelectListItem
            {
                Text = "No",
                Value = "false",
                Selected = false
            };
            list2.Add(item);
            SelectListItem item2 = new SelectListItem
            {
                Text = "Yes",
                Value = "true",
                Selected = false
            };
            list2.Add(item2);
            return list2;
        }

        ////private List<SelectListItem> IncomerangeItems()
        ////{
        ////    List<SelectListItem> list2 = new List<SelectListItem>();
        ////    SelectListItem item = new SelectListItem
        ////    {
        ////        Text = "Under  S$30,000\x0012\x000e",
        ////        Value = "Under S$30,000\x0012\x000e",
        ////        Selected = false
        ////    };
        ////    list2.Add(item);
        ////    SelectListItem item2 = new SelectListItem
        ////    {
        ////        Text = "S$30,000 to S$60,000",
        ////        Value = "S$30,000 to S$60,000",
        ////        Selected = false
        ////    };
        ////    list2.Add(item2);
        ////    SelectListItem item3 = new SelectListItem
        ////    {
        ////        Text = "S$60,001 to S$100,000",
        ////        Value = "S$60,001 to S$100,000",
        ////        Selected = false
        ////    };
        ////    list2.Add(item3);
        ////    SelectListItem item4 = new SelectListItem
        ////    {
        ////        Text = "S$100,001 to S$160,000",
        ////        Value = "S$100,001 to S$160,000",
        ////        Selected = false
        ////    };
        ////    list2.Add(item4);
        ////    SelectListItem item5 = new SelectListItem
        ////    {
        ////        Text = "S$160,001 to S$240,000",
        ////        Value = "S$160,001 to S$240,000",
        ////        Selected = false
        ////    };
        ////    list2.Add(item5);
        ////    SelectListItem item6 = new SelectListItem
        ////    {
        ////        Text = "S$240,001 and Above",
        ////        Value = "S$240,001 and Above",
        ////        Selected = false
        ////    };
        ////    list2.Add(item6);
        ////    return list2;
        ////}

        private List<SelectListItem> MoolaCoreTime()
        {
            List<SelectListItem> list2 = new List<SelectListItem>();
            SelectListItem item = new SelectListItem
            {
                Value = "Weekly",
                Text = "Weekly"
            };
            list2.Add(item);
            SelectListItem item2 = new SelectListItem
            {
                Value = "Monthly",
                Text = "Monthly"
            };
            list2.Add(item2);
            return list2;
        }

        /// <summary>
        /// Method for generating list for dropdown list(select list) for Nationality
        /// </summary>
        /// <returns></returns>
        ////private List<SelectListItem> NationalityItems()
        ////{
        ////    var CountriesList = db.tbl_Countries.ToList();
        ////    var Nationality = new List<SelectListItem>();
        ////    if (CountriesList != null)
        ////    {
        ////        foreach (var CountryName in CountriesList)
        ////        {
        ////            {
        ////                if (CountryName.CountryName.Trim() == "Singapore")
        ////                    Nationality.Add(new SelectListItem() { Text = CountryName.CountryName.Trim(), Value = CountryName.CountryName.Trim(), Selected = true });

        ////                else
        ////                    Nationality.Add(new SelectListItem() { Text = CountryName.CountryName.Trim(), Value = CountryName.CountryName.Trim() });
        ////            };
        ////        }
        ////    }
        ////    else
        ////    {
        ////        Nationality.Add(new SelectListItem() { Text = "Try Latter", Value = "Try Latter", Selected = true });
        ////    }

        ////    return Nationality;
        ////}



        ////public string CheckCurrentStatusbyLoanID(long LoanRequestID)
        ////{

        ////    tbl_LoanRequests LoanRequestDetail = this.db.tbl_LoanRequests.FirstOrDefault<tbl_LoanRequests>(l => l.RequestId == LoanRequestID);
        ////    return LoanRequestDetail.LoanStatus.ToLoanStatusDisplayName();
        ////}

        private List<SelectListItem> AddCoustomValueinEnd()
        {
            List<tbl_MoolahPerksForLenders> list = (from p in this.db.tbl_MoolahPerksForLenders
                                                    where p.Isactive == true
                                                    orderby p.MoolahPerk
                                                    select p).ToList<tbl_MoolahPerksForLenders>();
            List<SelectListItem> list2 = new List<SelectListItem>();
            foreach (tbl_MoolahPerksForLenders lenders in list)
            {
                SelectListItem item = new SelectListItem
                {
                    Value = lenders.MoolahPerkId.ToString(),
                    Text = lenders.MoolahPerk.ToString()
                };
                list2.Add(item);
            }
            return list2;
        }

        ////private List<SelectListItem> GenderItems()
        ////{
        ////    List<SelectListItem> list2 = new List<SelectListItem>();
        ////    SelectListItem item = new SelectListItem
        ////    {
        ////        Text = "Male",
        ////        Value = "Male",
        ////        Selected = false
        ////    };
        ////    list2.Add(item);
        ////    SelectListItem item2 = new SelectListItem
        ////    {
        ////        Text = "Female",
        ////        Value = "Female",
        ////        Selected = false
        ////    };
        ////    list2.Add(item2);
        ////    return list2;
        ////}

        public Withdrawal GetWithdrawInformation()
        {
            try
            {

                var result =

                    from u in db.tbl_Users
                    join w in db.tbl_WithDrawMoney on u.UserID equals w.User_ID
                    join a in db.tbl_AccountDetails on u.UserID equals a.User_ID
                    //join t in db.tbl_LoanTransactions on w.WithDrawID equals t.Request_Id
                    join b in db.tbl_BanksList on a.Bank_Id equals b.BankId
                    join ba in db.tbl_Balances on a.User_ID equals ba.User_ID
                    join an in db.tbl_WithDrawAdminNotes
                     on w.WithDrawID equals an.WithDraw_ID into ljoin //Left Join
                    from an in ljoin.DefaultIfEmpty()
                    orderby w.DateCreated
                    select new { tbl_AccountDetails = a, tbl_WithDrawMoney = w, tbl_BanksList = b, tbl_Balances = ba, tbl_Users = u, tbl_WithDrawAdminNotes = an };
                List<WithdrawList> objlist = new List<WithdrawList>();
                foreach (var item in result)
                {
                    WithdrawList obbj = new WithdrawList();
                    obbj.WitdrawID = item.tbl_WithDrawMoney.WithDrawID;
                    obbj.WithdrawDate = item.tbl_WithDrawMoney.DateCreated;
                    obbj.TransactionReference = db.tbl_LoanTransactions.Where(l => l.Withdraw_ID == item.tbl_WithDrawMoney.WithDrawID && l.PaymentMode == "Withdrawl money").Select(l => l.Reference).FirstOrDefault();

                    var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower()
                        == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault();

                    if (user != null)
                        obbj.AccountNumber = CommonMethods.GetAccountNumber(item.tbl_Users.UserID, item.tbl_Users.UserRole);

                    if (item.tbl_AccountDetails != null)
                        obbj.FullName = CommonMethods.GetAccountFullName(item.tbl_AccountDetails.Title, item.tbl_AccountDetails.FirstName, item.tbl_AccountDetails.LastName);

                    obbj.BankName = item.tbl_BanksList.BankName;
                    obbj.BankAccountNUmber = item.tbl_AccountDetails.AccountNumber;

                    obbj.CurrentBalance = item.tbl_WithDrawMoney.CurrentBalance;
                    obbj.BalanceAfterWithdrawl = item.tbl_WithDrawMoney.AfterBalance;
                    obbj.AmounttoWithdraw = item.tbl_WithDrawMoney.WithDrawAmount;
                    obbj.Status = item.tbl_WithDrawMoney.Status;
                    obbj.AdminActionDate = item.tbl_WithDrawMoney.ActionbyAdmin;
                    if (item.tbl_WithDrawAdminNotes != null)
                        obbj.Comments = item.tbl_WithDrawAdminNotes.Note;
                    else
                        obbj.Comments = "NA";
                    obbj.AdminID = item.tbl_WithDrawMoney.AdminID;
                    obbj.AdminName = db.tbl_Users.Where(u => u.UserID == item.tbl_WithDrawMoney.AdminID).SingleOrDefault() != null
                        ? db.tbl_Users.Where(u => u.UserID == item.tbl_WithDrawMoney.AdminID).SingleOrDefault().UserName
                        : string.Empty;
                    objlist.Add(obbj);
                }
                Withdrawal obj = new Withdrawal();
                obj.UserList = db.tbl_Users.Where(l => l.UserRole == "Admin").ToList();
                obj.ModelWithdrawalList = objlist;
                return obj;


            }

            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
                return null;
            }




        }

        public string AddBalanceByUserID(int UserID, decimal Balance)
        {
            try
            {
                tbl_Balances objbalance = db.tbl_Balances.SingleOrDefault(p => p.User_ID == UserID);
                string username = string.Empty;
                var user = db.tbl_Users.Where(x => x.UserID == UserID).SingleOrDefault();

                if (user == null)
                {
                    return "Something wrong here";
                }

                username = user.UserName;
                var accDet = user.tbl_AccountDetails.SingleOrDefault(ac => ac.User_ID == user.UserID);
                long? accId = accDet != null ? accDet.AccountId : 0;

                if (objbalance != null)
                {
                    LoanTransactionCredit(objbalance.ActualAmount, Balance, null, "Updated balance by MoolahSense", AuditMessages.DEPOSIT_FUND, UserID, objbalance.ActualAmount + Balance);
                    objbalance.ActualAmount = Balance + objbalance.ActualAmount;
                    objbalance.LedgerAmount = Balance + objbalance.LedgerAmount;
                    objbalance.DateCreated = DateTime.UtcNow;
                    db.SaveChanges();
                    MoolahLogManager.LogAdminAudit(@"Add balance amount to investor account",
                                                       username, null, accId, "NA",
                                                       (objbalance.LedgerAmount - Balance).ToString(), objbalance.LedgerAmount.ToString());

                    _IEmailSender.FundTransferIn(username, (double)Balance, (double)objbalance.LedgerAmount);
                }
                else
                {
                    LoanTransactionCredit(0, Convert.ToDecimal(Balance), null, "Account opened", AuditMessages.DEPOSIT_FUND, Convert.ToInt64(UserID), Convert.ToDecimal(Balance));
                    tbl_Balances obj = new tbl_Balances();
                    obj.ActualAmount = obj.LedgerAmount = Balance;
                    obj.User_ID = UserID;
                    obj.DateCreated = DateTime.UtcNow;
                    db.tbl_Balances.Add(obj);
                    db.SaveChanges();
                    MoolahLogManager.LogAdminAudit(@"Add balance amount to investor account",
                                                       HttpContext.Current.User.Identity.Name, null, accId, "NA",
                                                       "0", Balance.ToString());

                    _IEmailSender.FundTransferIn(username, (double)Balance, (double)obj.LedgerAmount);
                }



                return ContollerExtensions.ReturnSuccess();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Something wrong here. Contact Admin";
            }
        }

        /// <summary>
        /// get EMI by Loan reference for Distributing the amount of EmI between investors
        /// </summary>
        /// <param name="LoanReferenceID"></param>
        /// <returns></returns>
        public string GetEMIbyLoanReference(string LoanReferenceID)
        {
            try
            {
                LoanReferenceID = LoanReferenceID.TrimStart('0').TrimEnd('#');
                long RequiestID = Convert.ToInt64(LoanReferenceID);
                var EMIAmount = db.tbl_LoanAmortization.FirstOrDefault(l => l.LoanRequest_ID == RequiestID);
                if (EMIAmount != null)
                    return EMIAmount.EmiAmount.ToString();
                else
                    return "No Reference found";

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Check format of referene, Something wrong here. Contact Admin";
            }
        }

        /// <summary>
        /// SEt emi as true to show that emi has been paid, credit the balance in user balance after calculating his share by his offer on total loan amount
        /// </summary>
        /// <returns></returns>
        public string SaveRepaymentDetail(string LoanReference, string Date)
        {
            try
            {
                LoanReference = LoanReference.TrimStart('0').TrimEnd('#');
                long RequiestID = Convert.ToInt64(LoanReference);



                string Check = Repayment(RequiestID, Convert.ToDateTime(Date));
                if (Check == "Added")
                {
                    MoolahLogManager.LogAdminAudit(@"Monthly repayment has been accepted",
                                                       HttpContext.Current.User.Identity.Name, RequiestID, null, "NA",
                                                       "NA", "NA");
                    return "Emi Paid Successfully";
                }
                else
                    return Check;

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Something wrong here. Contact Admin";
            }

        }

        private string Repayment(long RequiestID, DateTime PayedOn)
        {
            try
            {
                var DAteCheck = db.tbl_LoanAmortization.SingleOrDefault(l => l.PayedOn.Value.Month == PayedOn.Month && l.PayedOn.Value.Year == PayedOn.Year && l.LoanRequest_ID == RequiestID);

                //chek if the emi is already payed for that month and year for a particular loan
                if (DAteCheck == null)
                {

                    tbl_LoanAmortization EmiDetail = db.tbl_LoanAmortization.FirstOrDefault(l => l.LoanRequest_ID == RequiestID
                        && l.PayStatus == (int)RepaymentPayStatus.Pending);
                    var ApprovedOfferList = db.tbl_Loanoffers.Where(l => l.LoanRequest_Id == RequiestID && l.OfferStatus == (int)LoanOfferStatus.Accepted).ToList();
                    var Loandetail = db.tbl_LoanRequests.SingleOrDefault(l => l.RequestId == RequiestID && l.LoanStatus == (int)LoanRequestStatus.Matched);
                    ///distribute the emi amount propotinally as per the offer made by investors
                    ///
                    if (Loandetail != null)
                    {
                        foreach (var item in ApprovedOfferList)
                        {
                            //check percentage of total loan amount has been paid by a offer
                            decimal? OfferPercentage = (item.OfferedAmount / Loandetail.Amount) * 100;
                            ///get the amount from emi and credit it in the balance of user 
                            decimal? BalanceTobeCredited = (OfferPercentage * EmiDetail.EmiAmount) / 100;


                            ///credit the emi share in user account
                            var AmountBefore = db.tbl_Balances.FirstOrDefault(l => l.User_ID == item.Investor_Id);

                            ////update the status of emi in amortization table
                            EmiDetail.PayStatus = (int)RepaymentPayStatus.Paid;
                            EmiDetail.PayedOn = PayedOn;
                            db.SaveChanges();



                            LoanTransactionCredit(AmountBefore.ActualAmount, BalanceTobeCredited, RequiestID, "Repayment Share", "Investment return", item.Investor_Id, AmountBefore.ActualAmount + BalanceTobeCredited);
                        }
                        ////Check if All Emi is paid then change status of Loan in tbl_loanrequest to Completed and send email to user
                        int count = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == RequiestID
                            && l.PayStatus == (int)RepaymentPayStatus.Pending).Count();
                        if (count == 0)
                        {
                            tbl_LoanRequests obj = db.tbl_LoanRequests.SingleOrDefault(l => l.RequestId == RequiestID);
                            obj.LoanStatus = (int)LoanRequestStatus.Matured;
                            db.SaveChanges();
                            _IEmailSender.OnLoanCompletionToBorrower(obj.RequestId.ToString().PadLeft(7, '0') + "#", obj.tbl_Users.UserName);

                        }
                        return "Added";
                    }
                    else
                        return "Wrong Reference";
                }
                else
                    return "Emi for this month and year has already been paid";


            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Something wrong here. Contact Admin";
            }

        }

        /// <summary>
        /// make transaction
        /// </summary>
        /// <param name="AmountBefore"></param>
        /// <param name="AmountToCredited"></param>
        /// <param name="LoanRequestID"></param>
        /// <param name="PaymentMode"></param>
        /// <param name="PaymentTerms"></param>
        /// <param name="UserID"></param>
        /// <param name="BalanceAfter"></param>
        public void LoanTransactionCredit(decimal? AmountBefore, decimal? AmountToCredited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
            objTransaction.Amount = Convert.ToDecimal(AmountBefore);///its the the amount user have in tbl_blances  before the transaction has occured
            objTransaction.Credited = Convert.ToDecimal(AmountToCredited);//amount of offer that should be credit in user balace
            objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime
            if (PaymentMode != "Withdrawal rejected by admin")
                objTransaction.Request_Id = LoanRequestID;//loan request from which offer has withdrawn
            else
                objTransaction.Withdraw_ID = LoanRequestID;//loan request from which offer has withdrawn
            objTransaction.PaymentMode = PaymentMode;
            objTransaction.PaymentTerms = PaymentTerms;
            objTransaction.User_ID = UserID;
            objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
            objTransaction.Balance = Convert.ToDecimal(BalanceAfter);//balance of user after crediting the amount in his account
            dbMoolahConnectEntities db = new dbMoolahConnectEntities();
            db.tbl_LoanTransactions.Add(objTransaction);
            db.SaveChanges();

        }

        ////public void DeleteFile(String fileToDelete)
        ////{
        ////    try
        ////    {
        ////        FileInfo f = new FileInfo(fileToDelete);
        ////        if (f.Exists)
        ////        {
        ////            f.Delete();
        ////        }
        ////    }
        ////    catch (Exception ex)
        ////    {
        ////        MoolahLogManager.LogException(ex);

        ////    }
        ////}

        
        /// <summary>
        /// Get list of All users username whose values has been changed by admin
        /// </summary>
        //public IEnumerable<AuditTrial> GetAllUserListChangedbyAdin()
        //{
        //    try
        //    {
        //        var AuditTrialList=db.tbl_Users.
        //    }
        //    catch (Exception ex)
        //    {
        //        MoolahLogManager.LogException(ex);
        //    }
        //}

        /// <summary>
        /// Update Withdrawl status, send mail according to performa specied to user if rejected or approved by admin
        /// </summary>
        /// <param name="WithdrawID"></param>
        public string UpdateWithdrawStatus(long WithdrawID, string Comments, string Status, string adminName)
        {

            try
            {
                string Curentadminid = (from p in db.tbl_Users
                                        where p.UserName.Trim().ToLower() == adminName.Trim().ToLower()
                                        select p.UserID).FirstOrDefault().ToString();


                var obj = db.tbl_WithDrawMoney.SingleOrDefault(l => l.WithDrawID == WithdrawID);

                if (obj == null)
                {
                    return ContollerExtensions.ReturnFailure();
                }

                var user = db.tbl_Users.SingleOrDefault(u => u.UserID == obj.User_ID);

                if (user == null)
                {
                    return ContollerExtensions.ReturnFailure();
                }

                if (Status == "Rejected")
                {
                    obj.AfterBalance = obj.CurrentBalance;
                    obj.Status = "Rejected";
                    obj.ActionbyAdmin = DateTime.UtcNow;
                    obj.AdminID = Convert.ToInt64(Curentadminid);
                    db.SaveChanges();

                    tbl_WithDrawAdminNotes objadmin = new tbl_WithDrawAdminNotes();
                    objadmin.DateCreated = DateTime.UtcNow;
                    objadmin.Note = Comments;
                    objadmin.WithDraw_ID = WithdrawID;
                    db.tbl_WithDrawAdminNotes.Add(objadmin);
                    db.SaveChanges();


                    //if request rejected cr. the balance in suer account
                    decimal? debtideamount = db.tbl_LoanTransactions.Where(l => l.Withdraw_ID == WithdrawID && l.PaymentTerms == "Withdrawl money").Select(l => l.Debted).FirstOrDefault();

                    tbl_Balances objBal = new tbl_Balances();
                    objBal = db.tbl_Balances.SingleOrDefault(l => l.User_ID == user.UserID);
                    objBal.LedgerAmount = objBal.LedgerAmount + obj.WithDrawAmount;

                    var pendingRequests = db.tbl_WithDrawMoney.Where(w => w.User_ID == obj.User_ID && w.Status == "Pending"
                        && w.DateCreated.HasValue && w.DateCreated.Value > obj.DateCreated);

                    foreach (tbl_WithDrawMoney request in pendingRequests)
                    {
                        request.CurrentBalance += obj.WithDrawAmount;
                        request.AfterBalance += obj.WithDrawAmount;
                    }

                    db.SaveChanges();

                    LoanTransactionCredit(objBal.LedgerAmount - obj.WithDrawAmount, obj.WithDrawAmount, WithdrawID, "Fund Transfer Rejected", "Fund Transfer Rejected",
                        user.UserID, objBal.LedgerAmount);

                    MoolahLogManager.LogAdminAudit(@"Fund Transfer Rejected",
                                                   HttpContext.Current.User.Identity.Name, WithdrawID, obj.User_ID, "NA",
                                                   "NA", "NA");
                    _ITransactionDA.InternalTransactionDebit(Settings.FundTransferAccountBalanceID,
                        obj.WithDrawAmount.HasValue ? obj.WithDrawAmount.Value : 0,
                        null, "Withdrawl money", "Fund Transfer Rejected",
                        obj.User_ID.HasValue ? obj.User_ID.Value : 0);

                    _IEmailSender.OnWithdrawalRejectionToInvestor(user.UserName, Comments);
                }
                else
                {
                    obj.Status = "Approved";
                    obj.ActionbyAdmin = DateTime.UtcNow;
                    obj.AdminID = Convert.ToInt64(Curentadminid);

                    tbl_Balances objBal = new tbl_Balances();
                    objBal = db.tbl_Balances.SingleOrDefault(l => l.User_ID == user.UserID);
                    objBal.ActualAmount = objBal.ActualAmount - obj.WithDrawAmount;

                    db.SaveChanges();

                    MoolahLogManager.LogAdminAudit(@"Fund Transfer Approved",
                                                   HttpContext.Current.User.Identity.Name, WithdrawID, obj.User_ID, "NA",
                                                   "NA", "NA");
                    _ITransactionDA.InternalTransactionDebit(Settings.FundTransferAccountBalanceID,
                        obj.WithDrawAmount.HasValue ? obj.WithDrawAmount.Value : 0,
                        null, "Withdrawl money", "Fund Transfer Approved",
                        obj.User_ID.HasValue ? obj.User_ID.Value : 0);

                    _IEmailSender.FundTransferOut(user.UserName, (double)obj.WithDrawAmount, (double)(objBal.LedgerAmount.HasValue ? objBal.LedgerAmount : objBal.ActualAmount));
                }

                return ContollerExtensions.ReturnSuccess();
            }

            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }


        }

        public List<AuditLog> GetAllAuditLogs()
        {
            var logList = new List<AuditLog>();
            var tmp = db.tbl_AdminActionLogs.OrderByDescending(l => l.Timestamp).ToList();
            var sgZone = TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time");

            foreach (var log in tmp)
            {
                var tmpData = new AuditLog
                    {
                        Reference = log.Reference,
                        Message = log.Message,
                        FieldName = log.FieldName,
                        OldValue = log.OldValue.Replace("@#@", ","),
                        NewValue = log.NewValue.Replace("@#@", ","),
                        RequestId = log.LoanRequestId.ToString(),
                        Username = log.UserName,
                        TimeStamp = (DateTime)log.Timestamp
                    };

                tmpData.TimeStamp = TimeZoneInfo.ConvertTimeFromUtc(tmpData.TimeStamp, sgZone);

                var actionUser = db.tbl_AccountDetails.SingleOrDefault(o => o.AccountId == log.ActionUserId);
                if (actionUser != null)
                {
                    tmpData.AccountId = actionUser.tbl_Users.UserName;
                }

                logList.Add(tmpData);
            }
            return logList;
        }

        public AcceptedLoan MatchedLoansById(long requestId)
        {
            IEnumerable<tbl_LoanRequests> requests = db.tbl_LoanRequests.Where(
                l => l.RequestId == requestId);
            var loan = LoadLoanList(requests);
            return loan.Count > 0 ? loan.FirstOrDefault() : null;
        }

        public List<AcceptedLoan> MatchedLoans()
        {
            IEnumerable<tbl_LoanRequests> requests = db.tbl_LoanRequests
                .Include("tbl_Users")
                .Where(
                l => l.LoanStatus == (int)LoanRequestStatus.Matched).OrderBy(l => l.AcceptedDate);
            return LoadLoanList(requests);
        }

        public List<AcceptedLoan> AcceptedLoansHistory(int numOfPrevMonths)
        {
            IEnumerable<tbl_LoanRequests> requests;
            if (numOfPrevMonths != -1)
            {
                var frmDate = DateTime.Today.AddMonths(numOfPrevMonths * -1);
                var toDate = DateTime.Now;
                requests = db.tbl_LoanRequests.Where(
                    l =>
                    l.AcceptedDate > frmDate && l.AcceptedDate <= toDate &&
                    (l.LoanStatus == (int)LoanRequestStatus.FundApproved
                     || l.LoanStatus == (int)LoanRequestStatus.FundRejected)).OrderBy(l => l.AcceptedDate);
            }
            else
            {
                requests = db.tbl_LoanRequests.Where(
                    l => (l.LoanStatus == (int)LoanRequestStatus.FundApproved
                          || l.LoanStatus == (int)LoanRequestStatus.FundRejected)).OrderBy(l => l.AcceptedDate);
            }

            return LoadLoanList(requests);
        }

        private List<AcceptedLoan> LoadLoanList(IEnumerable<tbl_LoanRequests> requests)
        {
            var sgZone = TimeZoneInfo.FindSystemTimeZoneById("Singapore Standard Time");
            var requestList = new List<AcceptedLoan>();
            foreach (var request in requests)
            {
                var userAcc = db.tbl_AccountDetails.SingleOrDefault(u => u.User_ID == request.User_ID);
                var obj = new AcceptedLoan();
                obj.AcceptedDate = TimeZoneInfo.ConvertTimeFromUtc((DateTime)request.AcceptedDate, sgZone);
                obj.LoanRefNumber = request.RequestId;
                obj.AccountNumber = CommonMethods.GetAccountNumber(request.tbl_Users.UserID, request.tbl_Users.UserRole);
                obj.CompanyName = userAcc.BusinessName;
                obj.Tenor = request.Terms;
                obj.Rate = request.Rate.HasValue ? (decimal)request.Rate : 0;
                obj.LoanAmount = request.FinalAmount.HasValue ? (Decimal)request.FinalAmount : 0;
                obj.MoolahFees = request.MoolahFees.HasValue ? (double)request.MoolahFees : 0;
                obj.AmountTransfered = (double)obj.LoanAmount - obj.MoolahFees;
                obj.BankName = db.tbl_BanksList.SingleOrDefault(b => b.BankId == userAcc.Bank_Id).BankName;
                obj.BankAccountNumber = userAcc.AccountNumber;
                obj.FundTransferDate = request.TransferDate;
                var enumDisplayStatus = ((LoanRequestStatus)request.LoanStatus);
                obj.Status = enumDisplayStatus.ToString();
                obj.Comments = request.ResonReject;
                obj.Username = request.tbl_Users.UserName;

                try
                {
                    var tmp = db.tbl_AdminActionLogs.Where(
                        al =>
                        (al.Message == AuditMessages.PENDING_TRANSFER_ACCEPTED_BY_ADMIN
                         || al.Message == AuditMessages.PENDING_TRANSFER_REJECTED_BY_ADMIN) &&
                        al.LoanRequestId == obj.LoanRefNumber);
                    obj.AdminId = tmp.OrderByDescending(t => t.Timestamp).First().UserName;
                }
                catch (Exception)
                {
                    obj.AdminId = null;
                }

                requestList.Add(obj);
            }

            return requestList;
        }
    }
}