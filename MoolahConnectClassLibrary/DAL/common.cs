﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using MoolahConnect.Services.Entities;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;
using MoolahConnectClassLibrary.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;




public static class ContollerExtensions
{
    /// <summary>
    /// Render partial view to string
    /// </summary>
    /// <returns>Result</returns>
    public static string RenderPartialViewToString(this Controller controller)
    {
        return RenderPartialViewToString(controller, null, null);
    }
    /// <summary>
    /// Render partial view to string
    /// </summary>
    /// <param name="viewName">View name</param>
    /// <returns>Result</returns>
    public static string RenderPartialViewToString(this Controller controller, string viewName)
    {
        return RenderPartialViewToString(controller, viewName, null);
    }
    /// <summary>
    /// Render partial view to string
    /// </summary>
    /// <param name="model">Model</param>
    /// <returns>Result</returns>
    public static string RenderPartialViewToString(this Controller controller, object model)
    {
        return RenderPartialViewToString(controller, null, model);
    }
    /// <summary>
    /// Render partial view to string
    /// </summary>
    /// <param name="viewName">View name</param>
    /// <param name="model">Model</param>
    /// <returns>Result</returns>
    public static string RenderPartialViewToString(this Controller controller, string viewName, object model)
    {
        //Original source code: http://craftycodeblog.com/2010/05/15/asp-net-mvc-render-partial-view-to-string/
        if (string.IsNullOrEmpty(viewName))
            viewName = controller.ControllerContext.RouteData.GetRequiredString("action");

        controller.ViewData.Model = model;

        using (var sw = new StringWriter())
        {
            ViewEngineResult viewResult = System.Web.Mvc.ViewEngines.Engines.FindPartialView(controller.ControllerContext, viewName);
            var viewContext = new ViewContext(controller.ControllerContext, viewResult.View, controller.ViewData, controller.TempData, sw);
            viewResult.View.Render(viewContext, sw);

            return sw.GetStringBuilder().ToString();
        }
    }

    

    /// <summary>
    /// ////////////this function will calculate the loan funded percentage  , average interest on loan offers on a loan and unmber of lendrs for a loan
    /// </summary>
    public static decimal?[] CalculateAvgFundedAvgInterestandNumofLenders(long LoanRequestID, decimal? LoanAmount)
    {
        dbMoolahConnectEntities db = new dbMoolahConnectEntities();
        var londetail = db.tbl_LoanRequests.FirstOrDefault(l => l.RequestId == LoanRequestID);
        var objLoanOfferes = db.tbl_Loanoffers.Where(lo => lo.LoanRequest_Id == LoanRequestID && (lo.OfferStatus == (int)LoanOfferStatus.Pending || lo.OfferStatus == (int)LoanOfferStatus.Accepted) && lo.OfferedRate <= londetail.Rate).ToList();/////change in condition as per change in requierecment && lo.OfferStatus == true
        decimal?[] FundedandAverageInterest = new decimal?[3];
        if (objLoanOfferes != null)
        {

            FundedandAverageInterest[0] = Math.Round(Convert.ToDecimal((objLoanOfferes.Sum(l => l.OfferedAmount) / LoanAmount) * 100), 2);///average of loan funded
            if (objLoanOfferes.Count > 0)
            {///
                var LoanofferforRequestID = db.tbl_Loanoffers.Where(lo => lo.LoanRequest_Id == LoanRequestID && lo.OfferedRate <= londetail.Rate).ToList();/////change in condition as per change in requierecment && lo.OfferStatus == true
                var TotalofLoanOffers = LoanofferforRequestID.Sum(l => l.OfferedAmount);

                //foreach (var offere in LoanofferforRequestID)
                //    AvergageRate = AvergageRate + (offere.OfferAmount / TotalofLoanOffers) * offere.Rate;

                decimal? AvergageRate = 0;
                foreach (var offere in LoanofferforRequestID)
                    AvergageRate = AvergageRate + (offere.OfferedAmount / TotalofLoanOffers) * offere.OfferedRate;


                //Math.Round(Convert.ToDecimal(objLoanOfferes.Sum(l => l.Rate) / objLoanOfferes.Count), 2);//average rate of investor                
                FundedandAverageInterest[1] = AvergageRate;

            }
            else
                FundedandAverageInterest[1] = 0;
            FundedandAverageInterest[2] = objLoanOfferes.Count;
        }
        return FundedandAverageInterest;
    }
    /// <summary>
    /// Get all loan request provide true for second variable for fetching loans which are in offer time period , if set to false the it will get all loan request
    /// </summary>
    /// <param name="UserID"></param>
    /// <returns>List tbl_loanrequest </returns>
    public static List<tbl_LoanRequests> GetLoanRequestbyUserIDandInTimePeriod(long UserID, bool OfferPeriodIndicator)
    {
        dbMoolahConnectEntities db = new dbMoolahConnectEntities();
        if (OfferPeriodIndicator == false)
        {
            return db.tbl_LoanRequests.Where(e => e.User_ID == UserID && e.LoanStatus == (int)LoanRequestStatus.InProgress).OrderByDescending(l => l.RequestId).ToList();
        }
        else
        {
            int? RequestCompletedate = db.tbl_GlobalVariables.Select(l => l.RequestCompletionDays).FirstOrDefault();
            var FinalDate = DateTime.UtcNow.AddDays(Convert.ToInt64(RequestCompletedate));

            return db.tbl_LoanRequests.Where(e => e.User_ID == UserID && e.LoanStatus == (int)LoanRequestStatus.InProgress
                && e.DateCreated <= FinalDate).OrderByDescending(l => l.RequestId).ToList();
        }
    }
    /// <summary>
    /// Retrun success as string for json methods flag for success
    /// </summary>
    /// <returns>string</returns>
    public static string ReturnSuccess()
    {
        //if (HttpContext.Current.Request.Cookies["_jresult"] == null)
        //{
        //    HttpCookie objJsonsResultCooke = new HttpCookie("_jresult", "Success");
        //    objJsonsResultCooke.Value = "Success";
        //    HttpContext.Current.Response.Cookies.Add(objJsonsResultCooke);            
        //}
        //else
        //{
        //    HttpContext.Current.Response.Cookies["_jresult"].Value = "Success";
        //}

        return "Success";
    }

    public static string ReturnFailure()
    {
        //if (HttpContext.Current.Request.Cookies["_jresult"] == null)
        //{
        //    HttpCookie objJsonsResultCooke = new HttpCookie("_jresult", "Failure");
        //    objJsonsResultCooke.Value = "Failure";
        //    HttpContext.Current.Response.Cookies.Add(objJsonsResultCooke);
        //}
        //else
        //{
        //    HttpContext.Current.Response.Cookies["_jresult"].Value = "Failure";

        //}
        return "Failure";

    }

    public static string AppendAddressField(string address1, string address2, string postalCode)
    {
        try
        {
            string addressString;

            if (address1 == null)
            {
                addressString = "@#@";
            }
            else
            {
                addressString = address1 + "@#@";
            }

            if (address2 == null)
            {
                addressString += "@#@";
            }
            else
            {
                addressString += address2 + "@#@";
            }

            if (postalCode == null)
            {
                addressString += "";
            }
            else
            {
                addressString += postalCode + "";
            }

            return addressString == "@#@@#@" ? string.Empty : addressString;
        }
        catch (Exception ex)
        {
            MoolahLogManager.LogException(ex);
            return string.Empty;
        }
    }

    public static bool CheckBusinessRegNumberAvailbilty(string regNumber)
    {
        var db = new dbMoolahConnectEntities();
        var usage = db.tbl_AccountDetails.Count(a => a.IC_CompanyRegistrationNumber == regNumber);
        if (usage > 0)
        {
            if (usage == 1)
            {
                var acc = db.tbl_AccountDetails.SingleOrDefault(a => a.IC_CompanyRegistrationNumber == regNumber);
                var userAccount = db.tbl_Users.SingleOrDefault(u => u.UserID == acc.User_ID);
                return userAccount == null || !((bool)userAccount.IsSubmitted);
            }
            return false;
        }
        return usage == 0;
    }

    public static void InsertAuditRecords(object objectToAudit, DbContext db, string msg, string username, long? requestId, long? userId)
    {
        IAuditLogDA audit = new AuditLogDA();
        audit.InsertAuditRecords(objectToAudit, db, msg, username, requestId, userId);
    }
}

