﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Linq;
using System.Web;

using System.Web.Security;
using System.Text;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using MoolahConnect.Util.FinancialCalculations;
using NLog;
using MvcPaging;
using System.Web.Helpers;
using System.Globalization;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.GenerateReference;
using MoolahConnect.Util;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using System.Data.Entity.Validation;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using EntityState = MoolahConnectClassLibrary.Models.ViewModels.EntityState;
using MoolahConnect.Util.Email;


namespace MoolahConnectClassLibrary.DAL
{
    public class BorrowerOperations
    {
        dbMoolahConnectEntities db = new dbMoolahConnectEntities();
        IEmailSender _IEmailSender;
        ITransactionsDA _ITransactionsDA;
        ILoanDetailsTask _ILoanDetailsTask;

        public BorrowerOperations()
        {
            _IEmailSender = new EmailSender();
            _ITransactionsDA = new TransactionsDA();
            _ILoanDetailsTask = new LoanDetailsTask();
        }

        /// <summary>
        /// Method for Index load
        /// </summary>
        /// <returns></returns>
        public string BorrowerIndexLoad()
        {
            string controller = string.Empty;
            try
            {
                var LoggedinUser = db.tbl_Users.SingleOrDefault(p => p.UserName.Equals(HttpContext.Current.User.Identity.Name, StringComparison.OrdinalIgnoreCase));
                //tbl_AccountDetails objborrower = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == LoggedinUser.UserID);
                //tbl_AccountDocuments objdocs = db.tbl_AccountDocuments.FirstOrDefault(p => p.User_ID == LoggedinUser.UserID);
                //DocumentsList objDocumentsList = new DocumentsList();
                //objDocumentsList.DocumentsList1 = db.tbl_AccountDocuments.Where(p => p.User_ID == LoggedinUser.UserID &&  p.DocumentType == "Account Document").ToList();
               // var objUserVerification = (from p in db.tbl_UserVerification where p.UserID == LoggedinUser.UserID select p).ToList();


                if (LoggedinUser.AdminVerification != "Approved")
                {
                    if (LoggedinUser.IsSubmitted != null && (bool)LoggedinUser.IsSubmitted)
                    {
                        controller = "SubmittedBorrower";
                    }
                    else
                    {
                        HttpContext.Current.Session["step"] = 1;
                        controller = "Borrower";
                    }
                }

                else
                {
                    controller = "";

                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                controller = string.Empty;
            }

            return controller;


        }

        /// <summary>
        /// Nethod for getting registration form details for generating PDF
        /// </summary>
        /// <returns></returns>
        public BorrowerAccount GetmodelforgeneratePDF()
        {

            try
            {
                BorrowerAccountDetailsModel objaccount = new BorrowerAccountDetailsModel();
                BorrowerDocuments objdocuments = new BorrowerDocuments();
                BorrowerAccount objborrower;

                string id = (from p in db.tbl_Users
                             where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                             select p.UserID).FirstOrDefault().ToString();




                tbl_AccountDetails objuser = new tbl_AccountDetails();
                tbl_SecurityQuestionsForUsers objusersecurityquestions = new tbl_SecurityQuestionsForUsers();


                if (id != "0" && id != null)
                {
                    int Userid = Convert.ToInt32(id);

                    objdocuments.Userid = Userid;
                    objuser = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);


                    if (objuser != null)
                    {
                        objaccount.Title = objuser.Title;
                        objaccount.FirstName = objuser.FirstName;
                        objaccount.LastName = objuser.LastName;
                        objaccount.DisplayName = objuser.DisplayName;
                        objaccount.RoleJobTitle = objuser.RoleJobTitle;
                        objaccount.BusinessName = objuser.BusinessName;
                        objaccount.BusinessRegistrationnumber = objuser.IC_CompanyRegistrationNumber;
                        objaccount.BusinessOrganisation = objuser.BusinessOrganisation;
                        objaccount.NatureofBusinessName = (from p in db.tbl_NatureofBusinessList
                                                           where p.NatureofBusinessId == objuser.Natureofbusiness_Id
                                                           select p.NatureofBusinessName).First().ToString();
                        var regAddress = CommonMethods.LoadAddressField(objuser.RegisteredAddress);
                        objaccount.RegisteredAddress1 = regAddress[0];
                        objaccount.RegisteredAddress2 = regAddress[1];
                        objaccount.RegisteredPostalCode = regAddress[2];
                        var mailingAddress = CommonMethods.LoadAddressField(objuser.BusinessMailingAddress);
                        objaccount.BusinessMailingAddress1 = mailingAddress[0];
                        objaccount.BusinessMailingAddress2 = mailingAddress[1];
                        objaccount.BusinessMailingPostalCode = mailingAddress[2];
                        objaccount.DirectLine = objuser.Main_OfficeContactnumber;
                        objaccount.Mobilenumber = objuser.Mobilenumber;
                        objaccount.Othercontactnumber = objuser.OptionalContactnumber;
                        objaccount.BankName = (from p in db.tbl_BanksList
                                               where p.BankId == objuser.Bank_Id
                                               select p.BankName).First().ToString();
                        objaccount.BankAccountNumber = objuser.AccountNumber;
                        objaccount.BankAccountName = objuser.AccountName;



                        var userquest = db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == Userid).ToList();
                        int q = 0;
                        foreach (var ite in userquest)
                        {
                            if (q == 0)
                            {
                                objaccount.SecurityQuestion1 = ite.Question_Id;
                                objaccount.SecurityAnswer1 = ite.Answer;
                            }
                            else
                            {
                                objaccount.SecurityQuestion2 = ite.Question_Id;
                                objaccount.SecurityAnswer2 = ite.Answer;
                            }

                            q++;
                        }

                        objaccount.SecurityQuestion1Name = (from p in db.tbl_SecurityQuestions
                                                            where p.QuestionId == objaccount.SecurityQuestion1
                                                            select p.QuestionName).First().ToString();
                        objaccount.SecurityQuestion2name = (from p in db.tbl_SecurityQuestions
                                                            where p.QuestionId == objaccount.SecurityQuestion2
                                                            select p.QuestionName).First().ToString();

                    }

                }

                objborrower = new BorrowerAccount(objaccount, null, objdocuments);
                return objborrower;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);

                return null;
            }
        }

        /// <summary>
        /// Method for binding details of user at create action load
        /// </summary>
        /// <returns></returns>
        public BorrowerAccount CreateLoad()
        {
            try
            {
                var id = (from p in db.tbl_Users
                          where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                          select p.UserID).FirstOrDefault().ToString();

                var objaccount = new BorrowerAccountDetailsModel();
                var objdocuments = new BorrowerDocuments();

                //////////////////////////////////// Get list of Security Questions , Nature of buisness and banks list for binding with drop downs /////////////////////////
                objaccount.SecurityQuestionList = db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();
                objaccount.NatureofBusinessList = db.tbl_NatureofBusinessList.Where(p => p.Isactive == true).ToList();
                objaccount.BanksList = db.tbl_BanksList.Where(p => p.Isactive == true).ToList();
                objaccount._Nationality = new SelectList(new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = "Singapore Citizen",
                                Value = "Singapore Citizen",
                                Selected = true
                            },
                        new SelectListItem {Text = "Singapore PR", Value = "Singapore PR"},
                        new SelectListItem {Text = "Other Nationalities", Value = "Other Nationalities"}
                    }, "Value", "Text", "Singapore");

                var objuser = new tbl_AccountDetails();

                //////////////////////////////////// If user is logged in then get details of user from Userid for displaying in account details ///////////////////
                if (id != "0" && id != null)
                {
                    var commonUser = new UserCommonOperations();
                    var userid = Convert.ToInt32(id);
                    objdocuments.Userid = userid;
                    objuser = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == userid);

                    if (objuser != null)
                    {
                        objaccount._Titles = new SelectList(TitleItems(), "Value", "Text", objuser.Title);
                        objaccount.Title = objuser.Title;
                        objaccount.FirstName = objuser.FirstName;
                        objaccount.LastName = objuser.LastName;
                        objaccount.DisplayName = objuser.DisplayName;
                        objaccount.RoleJobTitle = objuser.RoleJobTitle;
                        objaccount.BusinessName = objuser.BusinessName;
                        objaccount.BusinessRegistrationnumber = objuser.IC_CompanyRegistrationNumber;
                        objaccount.BusinessOrganisation = objuser.BusinessOrganisation;
                        objaccount.NatureofBusinessId = objuser.Natureofbusiness_Id == null
                                                            ? 1
                                                            : Convert.ToInt32(objuser.Natureofbusiness_Id);
                        var regAddress = CommonMethods.LoadAddressField(objuser.RegisteredAddress);
                        objaccount.RegisteredAddress1 = regAddress[0];
                        objaccount.RegisteredAddress2 = regAddress[1];
                        objaccount.RegisteredPostalCode = regAddress[2];
                        var mailingAddress = CommonMethods.LoadAddressField(objuser.BusinessMailingAddress);
                        objaccount.BusinessMailingAddress1 = mailingAddress[0];
                        objaccount.BusinessMailingAddress2 = mailingAddress[1];
                        objaccount.BusinessMailingPostalCode = mailingAddress[2];
                        objaccount.DirectLine = objuser.Main_OfficeContactnumber;
                        objaccount.Mobilenumber = objuser.Mobilenumber;
                        objaccount.Othercontactnumber = objuser.OptionalContactnumber;
                        objaccount.BankId = Convert.ToInt32(objuser.Bank_Id);
                        objaccount.BankNumber = objuser.BankNumber;
                        objaccount.BranchName = objuser.BranchName;
                        objaccount.BranchNumber = objuser.BranchNumber;
                        objaccount.BankAccountNumber = objuser.AccountNumber;
                        objaccount.DateofIncorporation = objuser.DateofIncorporation;
                        objaccount.BankAccountName = objuser.AccountName;
                        objaccount.PaidUpCapital = Convert.ToDouble(objuser.PaidUpCapital);
                        objaccount.NumOfEmployees = Convert.ToInt32(objuser.NumOfEmployees);
                        objaccount.NricNumber = objuser.NRIC_Number;
                        objaccount.PassportNumber = objuser.PassportNumber;
                        objaccount.SiccCode = objuser.SiccCode;
                        objaccount.Nationality = objuser.Nationality;
                        objaccount.EmailAddress = commonUser.GetCurrentUserEmail();
                        objaccount.SecurityQuestion1 =
                            Convert.ToInt32(
                                db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == userid)
                                  .Select(p => p.Question_Id)
                                  .FirstOrDefault());
                        objaccount.SecurityQuestion2 =
                            Convert.ToInt32(
                                db.tbl_SecurityQuestionsForUsers.Where(
                                    p => p.Question_Id != objaccount.SecurityQuestion1 && p.User_ID == userid)
                                  .Select(p => p.Question_Id)
                                  .FirstOrDefault());

                        var securityanswer1 = (from p in db.tbl_SecurityQuestionsForUsers
                                               where
                                                   p.Question_Id == objaccount.SecurityQuestion1 && p.User_ID == userid
                                               select p.Answer).FirstOrDefault();

                        objaccount.SecurityAnswer1 = securityanswer1;
                        var securityanswer2 = (from p in db.tbl_SecurityQuestionsForUsers
                                               where
                                                   p.Question_Id == objaccount.SecurityQuestion2 && p.User_ID == userid
                                               select p.Answer).FirstOrDefault();
                        objaccount.SecurityAnswer2 = securityanswer2;
                    }
                    else
                    {
                        objaccount._Titles = new SelectList(TitleItems(), "Value", "Text", "Mr.");
                        objaccount.EmailAddress = commonUser.GetCurrentUserEmail();
                    }
                }
                else
                {
                    objaccount._Titles = new SelectList(TitleItems(), "Value", "Text");
                }

                ///////////////////////////// for setting steps value for enabling the tabs depending upon the current step //////////////////////////////////////////
                var objborrower = new BorrowerAccount(objaccount, null, objdocuments);
                if (HttpContext.Current.Session["step"] == null || HttpContext.Current.Session["step"].ToString() == "")
                {
                    if (objuser == null)
                    {
                        HttpContext.Current.Session["step"] = 1;
                    }
                    else
                    {
                        HttpContext.Current.Session["step"] = 3;
                    }
                }

                return objborrower;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }

        public string CreateBorrower(BorrowerAccountDetailsModel objborrower)
        {
            try
            {
                var userid = (from p in db.tbl_Users
                              where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                              select p.UserID).FirstOrDefault();
                var objaccounts = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == userid);

                if (objaccounts != null)
                {
                    ///////////////////////////////// for updating record in tbl_accountdetails table //////////////////////////////////
                    objaccounts.Title = objborrower.Title;
                    objaccounts.FirstName = objborrower.FirstName;
                    objaccounts.LastName = objborrower.LastName;
                    objaccounts.DisplayName = objborrower.DisplayName;
                    objaccounts.RoleJobTitle = objborrower.RoleJobTitle;
                    objaccounts.BusinessName = objborrower.BusinessName;
                    objaccounts.IC_CompanyRegistrationNumber = objborrower.BusinessRegistrationnumber;
                    objaccounts.BusinessOrganisation = objborrower.BusinessOrganisation;
                    var regAdd = ContollerExtensions.AppendAddressField(objborrower.RegisteredAddress1,
                                                           objborrower.RegisteredAddress2,
                                                           objborrower.RegisteredPostalCode);
                    objaccounts.RegisteredAddress = regAdd;
                    var mailingAdd = ContollerExtensions.AppendAddressField(objborrower.BusinessMailingAddress1,
                                                           objborrower.BusinessMailingAddress2,
                                                           objborrower.BusinessMailingPostalCode);
                    objaccounts.BusinessMailingAddress = mailingAdd;
                    objaccounts.Main_OfficeContactnumber = objborrower.DirectLine;
                    objaccounts.Mobilenumber = objborrower.Mobilenumber;
                    objaccounts.OptionalContactnumber = objborrower.Othercontactnumber;
                    objaccounts.Bank_Id = objborrower.BankId;
                    objaccounts.BankNumber = objborrower.BankNumber;
                    objaccounts.BranchName = objborrower.BranchName;
                    objaccounts.BranchNumber = objborrower.BranchNumber;
                    objaccounts.AccountNumber = objborrower.BankAccountNumber;
                    objaccounts.AccountName = objborrower.BankAccountName;
                    objaccounts.PaidUpCapital = objborrower.PaidUpCapital;
                    objaccounts.NumOfEmployees = objborrower.NumOfEmployees;
                    objaccounts.NRIC_Number = objborrower.NricNumber;
                    objaccounts.PassportNumber = objborrower.PassportNumber;
                    objaccounts.Nationality = objborrower.Nationality;
                    objaccounts.SiccCode = objborrower.SiccCode;
                    var dateString = string.Empty;
                    if (objborrower.DateofIncorporation != null)
                        dateString = objborrower.DateofIncorporation.Value.ToShortDateString();
                    var dt = Convert.ToDateTime(dateString);
                    var dr = dt;
                    objaccounts.DateofIncorporation = dr;
                    db.SaveChanges();

                }
                else
                {
                    ///////////////////////////////// for inserting new record in tbl_accountdetails table //////////////////////////////////

                    var regAdd = ContollerExtensions.AppendAddressField(objborrower.RegisteredAddress1,
                                                           objborrower.RegisteredAddress2,
                                                           objborrower.RegisteredPostalCode);
                    var mailingAdd = ContollerExtensions.AppendAddressField(objborrower.BusinessMailingAddress1,
                                                           objborrower.BusinessMailingAddress2,
                                                           objborrower.BusinessMailingPostalCode);

                    var objaccountsnewuser = new tbl_AccountDetails
                    {
                        Title = objborrower.Title,
                        FirstName = objborrower.FirstName,
                        LastName = objborrower.LastName,
                        DisplayName = objborrower.DisplayName,
                        RoleJobTitle = objborrower.RoleJobTitle,
                        BusinessName = objborrower.BusinessName,
                        IC_CompanyRegistrationNumber = objborrower.BusinessRegistrationnumber,
                        BusinessOrganisation = objborrower.BusinessOrganisation,
                        RegisteredAddress = regAdd,
                        BusinessMailingAddress = mailingAdd,
                        Main_OfficeContactnumber = objborrower.DirectLine,
                        Mobilenumber = objborrower.Mobilenumber,
                        OptionalContactnumber = objborrower.Othercontactnumber,
                        Bank_Id = objborrower.BankId,
                        BankNumber = objborrower.BankNumber,
                        BranchName = objborrower.BranchName,
                        BranchNumber = objborrower.BranchNumber,
                        AccountNumber = objborrower.BankAccountNumber,
                        AccountName = objborrower.BankAccountName,
                        PaidUpCapital = objborrower.PaidUpCapital,
                        NumOfEmployees = objborrower.NumOfEmployees,
                        NRIC_Number = objborrower.NricNumber,
                        PassportNumber = objborrower.PassportNumber,
                        Nationality = objborrower.Nationality,
                        SiccCode = objborrower.SiccCode,
                    };
                    if (objborrower.DateofIncorporation != null)
                    {
                        var dateString = objborrower.DateofIncorporation.Value.ToShortDateString();
                        var dt = Convert.ToDateTime(dateString);
                        DateTime? dr = dt;
                        objaccountsnewuser.DateofIncorporation = dr;
                    }
                    objaccountsnewuser.User_ID = userid;
                    objaccountsnewuser.AccountType = "Borrower";
                    db.tbl_AccountDetails.Add(objaccountsnewuser);
                    db.SaveChanges();
                }
                HttpContext.Current.Session["step"] = 2;
                return ContollerExtensions.ReturnSuccess();
            }
            catch (DbEntityValidationException ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }

        /// <summary>
        /// Method for binding details on Loanrequest load
        /// </summary>
        /// <returns></returns>
        /// 
        public LoanRequest LoanrequestLoad()
        {
            LoanRequest objloanrequest;
            LoanRequestDescription objloanrequestdesc = new LoanRequestDescription();
            LoanRequestMoolahCore objloanrequestmoolahcore = new LoanRequestMoolahCore();
            LoanRequestMoolahPeri objloanrequestmoolahPeri = new LoanRequestMoolahPeri();

            try
            {
                var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault();

                if (user != null)
                    objloanrequestdesc.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);
               // objloanrequestdesc.LoanpurposeList = db.tbl_LoanPurposesList.Where(p => p.Isactive == true).OrderBy(p => p.PurposeName);

                Int64 Userid = (from p in db.tbl_Users
                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                select p.UserID).FirstOrDefault();


                objloanrequestdesc._IspersonalGuarantee = new SelectList(IsPersonalguarantee(), "Value", "Text", "No");
                objloanrequestdesc._Nationality = new SelectList(new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = "Singapore Citizen",
                                Value = "Singapore Citizen",
                                Selected = true
                            },
                        new SelectListItem {Text = "Singapore PR", Value = "Singapore PR"},
                        new SelectListItem {Text = "Other Nationalities", Value = "Other Nationalities"}
                    }, "Value", "Text", "Singapore");

                /////setting default values in moolahcore model /////
                objloanrequestmoolahcore.Accountnumber = objloanrequestdesc.Accountnumber;
                objloanrequestmoolahcore._Litigation = new SelectList(OutstandingLitigation(), "Value", "Text");
                ////////////////////////////////////////////////////////////////////

                /////setting default values  in moolahPeri model /////
                objloanrequestmoolahPeri.Accountnumber = objloanrequestdesc.Accountnumber;

                objloanrequestmoolahPeri.MoolahPerk = AddCoustomValueinEnd();
                /////////////////////////////////
                OutstandingLitigation objlitigation = new OutstandingLitigation();
                objlitigation._RepaymentPeriod = new SelectList(MoolaCoreTime(), "Value", "Text");
                objloanrequest = new LoanRequest();
                objloanrequest.LoanRequestDescription = objloanrequestdesc;
                objloanrequest.LoanRequestmoolahCore = objloanrequestmoolahcore;
                objloanrequest.LoanRequestmoolahPeri = objloanrequestmoolahPeri;
                objloanrequest.LoanRequestmoolahCore.Litigation = objlitigation;
                if (HttpContext.Current.Session["Loanstep"] == null)
                {
                    HttpContext.Current.Session["Loanstep"] = 1;
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objloanrequest = null;
            }

            return objloanrequest;

        }
        public LoanRequest UpdateLoadLoanRequestDescription()
        {
            LoanRequest objloanrequest;
            LoanRequestDescription objloanrequestdesc = new LoanRequestDescription();
            LoanRequestMoolahCore objloanrequestmoolahcore = new LoanRequestMoolahCore();
            LoanRequestMoolahPeri objloanrequestmoolahPeri = new LoanRequestMoolahPeri();

            try
            {
                var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault();

                if (user != null)
                    objloanrequestdesc.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);
                //commited Damith
                //objloanrequestdesc.LoanpurposeList = db.tbl_LoanPurposesList.Where(p => p.Isactive == true).OrderBy(p => p.PurposeName).ToList();

                Int64 Userid = (from p in db.tbl_Users
                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                select p.UserID).FirstOrDefault();
               

                objloanrequestdesc._IspersonalGuarantee = new SelectList(IsPersonalguarantee(), "Value", "Text", "No");
                objloanrequestdesc._Nationality = new SelectList(new List<SelectListItem>
                    {
                        new SelectListItem
                            {
                                Text = "Singapore Citizen",
                                Value = "Singapore Citizen",
                                Selected = true
                            },
                        new SelectListItem {Text = "Singapore PR", Value = "Singapore PR"},
                        new SelectListItem {Text = "Other Nationalities", Value = "Other Nationalities"}
                    }, "Value", "Text", "Singapore");

                /////setting default values in moolahcore model /////
                objloanrequestmoolahcore.Accountnumber = objloanrequestdesc.Accountnumber;
                objloanrequestmoolahcore._Litigation = new SelectList(OutstandingLitigation(), "Value", "Text");
                ////////////////////////////////////////////////////////////////////

                /////setting default values  in moolahPeri model /////
                    objloanrequestmoolahPeri.Accountnumber = objloanrequestdesc.Accountnumber;

                    objloanrequestmoolahPeri.MoolahPerk = AddCoustomValueinEnd();
                    /////////////////////////////////
                    OutstandingLitigation objlitigation = new OutstandingLitigation();
                    objlitigation._RepaymentPeriod = new SelectList(MoolaCoreTime(), "Value", "Text");
                    objloanrequest = new LoanRequest();
                    objloanrequest.LoanRequestDescription = objloanrequestdesc;
                    objloanrequest.LoanRequestmoolahCore = objloanrequestmoolahcore;
                    objloanrequest.LoanRequestmoolahPeri = objloanrequestmoolahPeri;
                    objloanrequest.LoanRequestmoolahCore.Litigation = objlitigation;
                    if (HttpContext.Current.Session["Loanstep"] == null)
                    {
                        HttpContext.Current.Session["Loanstep"] = 1;
                    }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objloanrequest = null;
            }

            return objloanrequest;

        }

        /////////////////////save  litigations
        public void SaveLoanRequestLitigations(OutstandingLitigation objLitigationmodel)
        {
            try
            {
                Int64 Userid = Convert.ToInt64(db.tbl_Users.Where(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).Select(p => p.UserID).FirstOrDefault());
                if (Userid > 0)
                {

                    tbl_OutStandingLitigation objLitigation = new tbl_OutStandingLitigation();
                    objLitigation.AmountOutstanding = objLitigationmodel.AmountOutstanding;
                    objLitigation.CreditLimit = objLitigationmodel.CreditLimit;
                    objLitigation.UsageinLastMonth = objLitigationmodel.UsageinLastMonth;
                    objLitigation.RenewalDate = objLitigationmodel.RenewalDate;
                    objLitigation.RepaymentAmount = objLitigationmodel.RepaymentAmount;
                    objLitigation.RepaymentPeriod = objLitigationmodel._RepaymentPeriodSet;
                    objLitigation.DateofLastPayment = objLitigationmodel.DateofLastPayment;
                    objLitigation.Description = objLitigationmodel.Description;
                    objLitigation.LitigationType = objLitigationmodel.LitigationType;
                    objLitigation.PaymentTerms = objLitigationmodel.PaymentTerms;
                    objLitigation.MoolahCore_Id = objLitigationmodel.MoolahCore_Id;
                    objLitigation.User_ID = Userid;
                    objLitigation.RepaymentPeriod = objLitigationmodel._RepaymentPeriodSet;
                    objLitigation.DateCreated = DateTime.UtcNow;

                    db.tbl_OutStandingLitigation.Add(objLitigation);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objLitigationmodel = null;
            }


        }

        public void UpdateLoadLoanRequestDescription(LoanRequestDescription loanRequest)
        {
            var request = db.tbl_LoanRequests.SingleOrDefault(rq => rq.RequestId == loanRequest.RequestId);
            if (request != null)
            {
                loanRequest.Amount = request.Amount;
                loanRequest.Term = request.Terms;
                loanRequest.Rate = request.Rate ?? 0;
                loanRequest.SummaryCompanyPro = request.SummaryCompanyPro;
                loanRequest.DetailedCompanyPro = request.DetailedCompanyPro;
                loanRequest.NumberOfEmployees = request.NumberOfEmployees ?? 0;
                loanRequest.LoanPurposeId = request.LoanPurpose_Id;
                loanRequest.LoanPurposeDescription = request.LoanPurposeDescription;
                loanRequest.IspersonalGuarantee = request.IsPersonalGuarantee;
                loanRequest.Headline = request.Headline;

                if (loanRequest.IspersonalGuarantee != null && (bool)loanRequest.IspersonalGuarantee)
                {
                    var info = request.tbl_PersonalGuaranteeInfo.ToList().Select(a=> new PersonGuarantee()
                    {
                        id = a.InfoId,
                        Email = a.Email,
                        NameInNRIC = a.NameAsinNRIC,
                        Nationality = a.Nationality,
                        NRICNo = a.NRIC_Passport,
                        PassportNo = a.PassportNumber,
                        PostalCode =  a.Postalcode,
                        Reason =  a.Reasons,
                        AddressSpilt =  a.ResidentialAddress,
                        Telephone = a.Telephone

                    });
                    loanRequest.PersonGuarantees = info.ToList();
                    //if (info .Count > 0)
                    //{
                    //    loanRequest.NameasinNRICPassport = info.NameAsinNRIC;
                    //    loanRequest.Nationality = info.Nationality;
                    //    loanRequest.NricNumber = info.NRIC_Passport;
                    //    loanRequest.PassportNumber = info.PassportNumber;
                    //    var add = CommonMethods.LoadAddressField(info.ResidentialAddress);
                    //    loanRequest.Residentialaddress1 = add[0];
                    //    loanRequest.Residentialaddress2 = add[1];
                    //    loanRequest.Postalcode = info.Postalcode;
                    //    loanRequest.Telephone = info.Telephone;
                    //    loanRequest.Email = info.Email;
                    //    loanRequest.Reasons = info.Reasons;
                    //}
                }
            }
        }

       
        public IList<PersonGuarantee> LoadLoanGuarantors(long requestId)
        {
            return db.tbl_PersonalGuaranteeInfo.Where(a=> a.Request_Id == requestId).ToList().Select(a=> new PersonGuarantee()
                    {
                        id = a.InfoId,
                        Email = a.Email,
                        NameInNRIC = a.NameAsinNRIC,
                        Nationality = a.Nationality,
                        NRICNo = a.NRIC_Passport,
                        PassportNo = a.PassportNumber,
                        PostalCode =  a.Postalcode,
                        Reason =  a.Reasons,
                        AddressSpilt = a.ResidentialAddress,
                        Telephone = a.Telephone

                    }).ToList();
        }

        /// <summary>
        /// Method for saving loan request of borrower
        /// </summary>       
        /// <returns></returns>
        public string SaveLoanrequest(LoanRequestDescription objLoanDescription)
        {
            try
            {
                var eir = LoanRequestCalc.CalculateTargetEIR((double)objLoanDescription.Amount.Value, objLoanDescription.Term, (double)objLoanDescription.Rate);
                var user = db.tbl_Users.FirstOrDefault(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower());

                long userid = 0;
                if (user != null)
                    userid = user.UserID;

                long request_id = 0;

                if (objLoanDescription.RequestId != 0)
                    request_id = objLoanDescription.RequestId;
                else if (HttpContext.Current.Session["LoanrequestId"] != null)
                    request_id = (long)HttpContext.Current.Session["LoanrequestId"];

                //////////////////////////////////////// Save Loan request's basic details in tbl_LoanRequests  ////////////////////////////////
                var param = new SqlParameter[15];
                param[0] = new SqlParameter("@Amount", objLoanDescription.Amount);
                param[1] = new SqlParameter("@Terms", objLoanDescription.Term);
                param[2] = new SqlParameter("@Rate", objLoanDescription.Rate);
                param[3] = new SqlParameter("@Ispersonalguarantee", objLoanDescription.IspersonalGuarantee);
                param[4] = new SqlParameter("@LoanpurposeID", objLoanDescription.LoanPurposeId);
                param[5] = new SqlParameter("@LoanPurposeDescription", objLoanDescription.LoanPurposeDescription);
                //param[6] = new SqlParameter("@NameasinNRICPassport", (object)objLoanDescription.NameasinNRICPassport ?? DBNull.Value);
                //param[7] = new SqlParameter("@Designation", "");
                //param[8] = new SqlParameter("@NRICPassport", (object)objLoanDescription.NricNumber ?? DBNull.Value);
                //param[9] = new SqlParameter("@Reasons", (object)objLoanDescription.Reasons ?? DBNull.Value);
                //var add = ContollerExtensions.AppendAddressField(objLoanDescription.Residentialaddress1,
                //                                       objLoanDescription.Residentialaddress2, "");
                //param[10] = new SqlParameter("@Residentialaddress", add);
                //param[11] = new SqlParameter("@Telephone", (object)objLoanDescription.Telephone ?? DBNull.Value);
                //param[12] = new SqlParameter("@Email", (object)objLoanDescription.Email ?? DBNull.Value);
                //param[13] = new SqlParameter("@Postalcode", (object)objLoanDescription.Postalcode ?? DBNull.Value);
                param[6] = new SqlParameter("@User_Id", userid);
                param[7] = new SqlParameter("@requestID", 0) { Direction = ParameterDirection.Output };
                param[8] = new SqlParameter("@requestIDold", request_id);
                param[9] = new SqlParameter("@VideoDiscription", "");
                param[10] = new SqlParameter("@EIR", eir);
                param[11] = new SqlParameter("@DetailedCompanyPro", objLoanDescription.DetailedCompanyPro);
                param[12] = new SqlParameter("@SummaryCompanyPro", objLoanDescription.SummaryCompanyPro);
                //param[13] = new SqlParameter("@PassportNumber", (object)objLoanDescription.PassportNumber ?? DBNull.Value);
                param[13] = new SqlParameter("@NumberOfEmployees", (object)objLoanDescription.NumberOfEmployees ?? DBNull.Value);
                //param[15] = new SqlParameter("@Nationality", (object)objLoanDescription.Nationality ?? DBNull.Value);
                param[14] = new SqlParameter("@Headline", (object)objLoanDescription.Headline ?? DBNull.Value);
                db.Database.ExecuteSqlCommand("USP_tbl_LoanRequest_SaveLoanrequestdetails   @Amount ,@Terms ,@Rate ,@Ispersonalguarantee ,@LoanpurposeID ,@LoanPurposeDescription ,@User_Id,@requestID out,@requestIDold,@VideoDiscription,@EIR,@DetailedCompanyPro,@SummaryCompanyPro,@NumberOfEmployees,@Headline", param);
                request_id = Convert.ToInt64(param[7].Value);
                SavePersonalGuarantors(objLoanDescription.PersonGuarantees, request_id);
                HttpContext.Current.Session["LoanrequestId"] = request_id;

                return ContollerExtensions.ReturnSuccess();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }

            return "";
        }

        private void SavePersonalGuarantors(IList<PersonGuarantee> guarantees,long requestId)
        {
            foreach (var item in guarantees.Where(a=> a.State != EntityState.Delete))
            {
                  var param = new SqlParameter[12];

                  param[0] = new SqlParameter("@inforid", item.id);
                  param[1] = new SqlParameter("@requestid", requestId);
                  param[2] = new SqlParameter("@NameasinNRICPassport", item.NameInNRIC);
                  param[3] = new SqlParameter("@Designation", "");
                param[4] = new SqlParameter("@NRICPassport", (object)item.NRICNo ?? DBNull.Value);
                  param[5] = new SqlParameter("@Reasons", (object) item.Reason ??DBNull.Value);
                  var add = ContollerExtensions.AppendAddressField(item.ResidentialAddress1,
                                                         item.ResidentialAddress2, "");
                  param[6] = new SqlParameter("@Residentialaddress", add);
                  param[7] = new SqlParameter("@Telephone", (object)item.Telephone ?? DBNull.Value );
                  param[8] = new SqlParameter("@Email", (object)item.Email ?? DBNull.Value );
                  param[9] = new SqlParameter("@Postalcode", (object)item.PostalCode ?? DBNull.Value);
                  param[10] = new SqlParameter("@PassportNumber", (object)item.PassportNo ?? DBNull.Value);
                  param[11] = new SqlParameter("@Nationality", (object)item.Nationality ?? DBNull.Value);
                  db.Database.ExecuteSqlCommand("USP_tbl_PersonalGuaranteeInfo_Save   @inforid ,@requestid ,@NameasinNRICPassport ,@Designation ,@NRICPassport,"
                + "@Reasons ,@Residentialaddress,@Telephone,@Email,@Postalcode,@PassportNumber,@Nationality", param);


            }
            foreach (var item in guarantees.Where(a => a.State == EntityState.Delete))
            {
                var param = new SqlParameter[2];

                param[0] = new SqlParameter("@inforid", item.id);
                param[1] = new SqlParameter("@requestid", requestId);

                db.Database.ExecuteSqlCommand("USP_tbl_PersonalGuaranteeInfo_Delete   @inforid ,@requestid", param);
            }
        }

        public string SaveLoanrequest(decimal Amount, string Terms, decimal Rate, decimal EIR, bool Ispersonalguarantee, int LoanpurposeID, string XMLquestionswithanswers, string Loanimages, string LoanDocs,
             string NameasinNRICPassport, string Designation, string NRICPassport, string Reasons, string Residentialaddress, string Telephone, string Email, string Postalcode, string VideoDiscription, List<int> DeleteDocument, string DetailedCompanyPro, string Logoimage)
        {
            try
            {
                var user = db.tbl_Users.Where(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).FirstOrDefault();

                long Userid = 0;
                if (user != null)
                    Userid = user.UserID;

                long request_id = 0;

                if (HttpContext.Current.Session["LoanrequestId"] != null)
                {
                    request_id = Convert.ToInt64(HttpContext.Current.Session["LoanrequestId"]);
                }
                //////////////////////////////////////// Save Loan request's basic details in tbl_LoanRequests  ////////////////////////////////
                SqlParameter[] param = new SqlParameter[20];
                param[0] = new SqlParameter("@Amount", Amount);
                param[1] = new SqlParameter("@Terms", Terms);
                param[2] = new SqlParameter("@Rate", Rate);
                param[3] = new SqlParameter("@Ispersonalguarantee", Ispersonalguarantee);
                param[4] = new SqlParameter("@LoanpurposeID", LoanpurposeID);
                param[5] = new SqlParameter("@LoanPurposeDescription", XMLquestionswithanswers);
                param[6] = new SqlParameter("@NameasinNRICPassport", NameasinNRICPassport);
                param[7] = new SqlParameter("@Designation", Designation);
                param[8] = new SqlParameter("@NRICPassport", NRICPassport);
                param[9] = new SqlParameter("@Reasons", Reasons);
                param[10] = new SqlParameter("@Residentialaddress", Residentialaddress);
                param[11] = new SqlParameter("@Telephone", Telephone);
                param[12] = new SqlParameter("@Email", Email);
                param[13] = new SqlParameter("@Postalcode", Postalcode);
                param[14] = new SqlParameter("@User_Id", Userid);
                param[15] = new SqlParameter("@requestID", 0);
                param[15].Direction = ParameterDirection.Output;
                param[16] = new SqlParameter("@requestIDold", request_id);
                param[17] = new SqlParameter("@VideoDiscription", VideoDiscription);
                param[18] = new SqlParameter("@EIR", EIR);
                param[19] = new SqlParameter("@DetailedCompanyPro", DetailedCompanyPro);
                db.Database.ExecuteSqlCommand("USP_tbl_LoanRequest_SaveLoanrequestdetails   @Amount ,@Terms ,@Rate ,@Ispersonalguarantee ,@LoanpurposeID ,@XMLquestionswithanswers ,@NameasinNRICPassport ,@Designation ,@NRICPassport ,@Reasons ,@Residentialaddress ,@Telephone ,@Email ,@Postalcode ,@User_Id,@requestID out,@requestIDold,@VideoDiscription,@EIR,@DetailedCompanyPro ", param);
                request_id = Convert.ToInt64(param[15].Value);
                HttpContext.Current.Session["LoanrequestId"] = request_id;

                return ContollerExtensions.ReturnSuccess();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }

        /// <summary>
        /// Method for getting questions list for loan purpose
        /// </summary>       
        /// <returns></returns>
        public string GetQuestionslistforLoanPurpose(int LoanpurposeId)
        {
            StringBuilder strdiv = new StringBuilder();
            try
            {
                //var question =
                //    db.tbl_QuestionsForSelectedLoanpurpose.FirstOrDefault(p => p.Loanpurpose_Id == LoanpurposeId);
                strdiv.Append("<div class='fl bot_mar width100'><div class='wish_txt_des' id='" + LoanpurposeId +
                              "'> Please provide more details on the purpose </div><textarea  class='des_tarea' placeholder='Details'/></div>");
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
            return strdiv.ToString();
        }

        public void UpdateLoadLoanRequestMoolahcore(LoanRequestMoolahCore loanRequest)
        {
            var objMoolahcore = db.tbl_MoolahCore.SingleOrDefault(mc => mc.LoanRequest_Id == loanRequest.LoanRequest_Id);
            if (objMoolahcore == null) return;
            loanRequest.LatestYear = objMoolahcore.LatestYear;
            loanRequest.PreviousYear = objMoolahcore.PreiviousYear;
            loanRequest.TotalDebt_LY = objMoolahcore.TotalDebt_LY;
            loanRequest.TotalDebt_PY = objMoolahcore.TotalDebt_PY;
            loanRequest.Turnovers_LY = objMoolahcore.Turnovers_LY;
            loanRequest.Turnovers_PY = objMoolahcore.Turnovers_PY;

            loanRequest.ProfitBeforIntAndTax_LY = objMoolahcore.ProfitBeforIntAndTax_LY;
            loanRequest.ProfitBeforIntAndTax_PY = objMoolahcore.ProfitBeforIntAndTax_PY;

            loanRequest.InterestExpense_LY = objMoolahcore.InterestExpense_LY;
            loanRequest.InterestExpense_PY = objMoolahcore.InterestExpense_PY;

            loanRequest.InterestCoverageRatio_LY = objMoolahcore.InterestCoverageRatio_LY;
            loanRequest.InterestCoverageRatio_PY = objMoolahcore.InterestCoverageRatio_PY;

            loanRequest.EBITDA_LY = objMoolahcore.EBITDA_LY;
            loanRequest.EBITDA_PY = objMoolahcore.EBITDA_PY;

            loanRequest.DebtEBITDARatio_LY = objMoolahcore.DebtEBITDARatio_LY;
            loanRequest.DebtEBITDARatio_PY = objMoolahcore.DebtEBITDARatio_PY;

            loanRequest.TotalShareholdersequity_LY = objMoolahcore.TotalShareholdersequity_LY;
            loanRequest.TotalShareholdersequity_PY = objMoolahcore.TotalShareholdersequity_PY;
            loanRequest.CashFlowfromOperations_LY = objMoolahcore.CashFlowfromOperations_LY;
            loanRequest.CashFlowfromOperations_PY = objMoolahcore.CashFlowfromOperations_PY;
            loanRequest.CurrentAssests_LY = objMoolahcore.CurrentAssests_LY;
            loanRequest.CurrentAssests_PY = objMoolahcore.CurrentAssests_PY;
            loanRequest.CurrentLiabilities_LY = objMoolahcore.CurrentLiabilities_LY;
            loanRequest.CurrentLiabilities_PY = objMoolahcore.CurrentLiabilities_PY;
            loanRequest.CurrentRatio_LY = objMoolahcore.CurrentRatio_LY;
            loanRequest.CurrentRatio_PY = objMoolahcore.CurrentRatio_PY;
            loanRequest.Debt_Equity_LY = objMoolahcore.Debt_Equity_LY;
            loanRequest.Debt_Equity_PY = objMoolahcore.Debt_Equity_PY;
            loanRequest.IsoutstandingLitigation = objMoolahcore.IsoutstandingLitigation;
            loanRequest.NetprofitafterTax_LY = objMoolahcore.NetprofitafterTax_LY.HasValue ? objMoolahcore.NetprofitafterTax_LY.Value : 0;
            loanRequest.NetprofitafterTax_PY = objMoolahcore.NetprofitafterTax_PY;
            loanRequest.AvgThreeMntCashBalBankAcc_LY = objMoolahcore.AvgThreeMntCashBalBankAcc_LY;
            loanRequest.AvgThreeMntCashBalBankAcc_PY = objMoolahcore.AvgThreeMntCashBalBankAcc_PY;
            loanRequest.Profitable = objMoolahcore.Profitable;
        }

        /// <summary>
        /// Method for saving loan request moolahCore of borrower
        /// </summary>       
        /// <returns></returns>
        public string SaveLoanrequestmoolahcore(LoanRequestMoolahCore objmoolahcoreView)
        {
            try
            {
                long id = 0;
                Int64 Userid = Convert.ToInt64(db.tbl_Users.Where(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).Select(p => p.UserID).FirstOrDefault());
                Int64 request_id = 0;
                if (HttpContext.Current.Session["LoanrequestId"] != null)
                {
                    request_id = Convert.ToInt64(HttpContext.Current.Session["LoanrequestId"]);
                }

                var iquery = db.tbl_MoolahCore.ToList();
                var LoanRequestID = iquery.SingleOrDefault(m => m.LoanRequest_Id.Equals(request_id));
                if (LoanRequestID != null)
                {
                    LoanRequestID.LatestYear = objmoolahcoreView.LatestYear;
                    LoanRequestID.PreiviousYear = objmoolahcoreView.PreviousYear;
                    LoanRequestID.ProfitBeforIntAndTax_LY = objmoolahcoreView.ProfitBeforIntAndTax_LY;
                    LoanRequestID.ProfitBeforIntAndTax_PY = objmoolahcoreView.ProfitBeforIntAndTax_PY;
                    LoanRequestID.InterestExpense_LY = objmoolahcoreView.InterestExpense_LY;
                    LoanRequestID.InterestExpense_PY = objmoolahcoreView.InterestExpense_PY;

                    LoanRequestID.EBITDA_LY = objmoolahcoreView.EBITDA_LY;
                    LoanRequestID.EBITDA_PY = objmoolahcoreView.EBITDA_PY;

                    LoanRequestID.DebtEBITDARatio_LY = objmoolahcoreView.DebtEBITDARatio_LY;
                    LoanRequestID.DebtEBITDARatio_PY = objmoolahcoreView.DebtEBITDARatio_PY;

                    LoanRequestID.InterestCoverageRatio_LY = objmoolahcoreView.InterestCoverageRatio_LY;
                    LoanRequestID.InterestCoverageRatio_PY = objmoolahcoreView.InterestCoverageRatio_PY;

                    LoanRequestID.TotalDebt_LY = objmoolahcoreView.TotalDebt_LY;
                    LoanRequestID.TotalDebt_PY = objmoolahcoreView.TotalDebt_PY;
                    LoanRequestID.Turnovers_LY = objmoolahcoreView.Turnovers_LY;
                    LoanRequestID.Turnovers_PY = objmoolahcoreView.Turnovers_PY;
                    LoanRequestID.TotalShareholdersequity_LY = objmoolahcoreView.TotalShareholdersequity_LY;
                    LoanRequestID.TotalShareholdersequity_PY = objmoolahcoreView.TotalShareholdersequity_PY;
                    LoanRequestID.CashFlowfromOperations_LY = objmoolahcoreView.CashFlowfromOperations_LY;
                    LoanRequestID.CashFlowfromOperations_PY = objmoolahcoreView.CashFlowfromOperations_PY;
                    LoanRequestID.CurrentAssests_LY = objmoolahcoreView.CurrentAssests_LY;
                    LoanRequestID.CurrentAssests_PY = objmoolahcoreView.CurrentAssests_PY;
                    LoanRequestID.CurrentLiabilities_LY = objmoolahcoreView.CurrentLiabilities_LY;
                    LoanRequestID.CurrentLiabilities_PY = objmoolahcoreView.CurrentLiabilities_PY;
                    LoanRequestID.CurrentRatio_LY = objmoolahcoreView.CurrentRatio_LY;
                    LoanRequestID.CurrentRatio_PY = objmoolahcoreView.CurrentRatio_PY;
                    LoanRequestID.Debt_Equity_LY = objmoolahcoreView.Debt_Equity_LY;
                    LoanRequestID.Debt_Equity_PY = objmoolahcoreView.Debt_Equity_PY;
                    LoanRequestID.IsoutstandingLitigation = objmoolahcoreView.IsoutstandingLitigation;

                    LoanRequestID.NetprofitafterTax_LY = objmoolahcoreView.NetprofitafterTax_LY;
                    LoanRequestID.NetprofitafterTax_PY = objmoolahcoreView.NetprofitafterTax_PY;
                    LoanRequestID.AvgThreeMntCashBalBankAcc_LY = objmoolahcoreView.AvgThreeMntCashBalBankAcc_LY;
                    LoanRequestID.AvgThreeMntCashBalBankAcc_PY = objmoolahcoreView.AvgThreeMntCashBalBankAcc_PY;
                    LoanRequestID.LoanRequest_Id = request_id;
                    LoanRequestID.User_ID = Userid;
                    LoanRequestID.Profitable = objmoolahcoreView.NetprofitafterTax_LY > 0;
                    LoanRequestID.Profitable_PY = objmoolahcoreView.NetprofitafterTax_PY > 0;
                    db.SaveChanges();
                    id = LoanRequestID.MoolahCoreId;
                    if (objmoolahcoreView.IsoutstandingLitigation == false)
                    {
                        var Litigations = db.tbl_OutStandingLitigation.Where(e => e.MoolahCore_Id == id);
                        foreach (tbl_OutStandingLitigation i in Litigations)
                        {
                            db.tbl_OutStandingLitigation.Remove(i);

                        }
                        db.SaveChanges();
                    }

                }
                else
                {
                    tbl_MoolahCore objmoolahcore = new tbl_MoolahCore();
                    objmoolahcore.TotalDebt_LY = objmoolahcoreView.TotalDebt_LY;
                    objmoolahcore.TotalDebt_PY = objmoolahcoreView.TotalDebt_PY;
                    objmoolahcore.Turnovers_LY = objmoolahcoreView.Turnovers_LY;
                    objmoolahcore.Turnovers_PY = objmoolahcoreView.Turnovers_PY;
                    objmoolahcore.TotalShareholdersequity_LY = objmoolahcoreView.TotalShareholdersequity_LY;
                    objmoolahcore.TotalShareholdersequity_PY = objmoolahcoreView.TotalShareholdersequity_PY;
                    objmoolahcore.CashFlowfromOperations_LY = objmoolahcoreView.CashFlowfromOperations_LY;
                    objmoolahcore.CashFlowfromOperations_PY = objmoolahcoreView.CashFlowfromOperations_PY;
                    objmoolahcore.CurrentAssests_LY = objmoolahcoreView.CurrentAssests_LY;
                    objmoolahcore.CurrentAssests_PY = objmoolahcoreView.CurrentAssests_PY;
                    objmoolahcore.CurrentLiabilities_LY = objmoolahcoreView.CurrentLiabilities_LY;
                    objmoolahcore.CurrentLiabilities_PY = objmoolahcoreView.CurrentLiabilities_PY;
                    objmoolahcore.CurrentRatio_LY = objmoolahcoreView.CurrentRatio_LY;
                    objmoolahcore.CurrentRatio_PY = objmoolahcoreView.CurrentRatio_PY;
                    objmoolahcore.Debt_Equity_LY = objmoolahcoreView.Debt_Equity_LY;
                    objmoolahcore.Debt_Equity_PY = objmoolahcoreView.Debt_Equity_PY;
                    objmoolahcore.IsoutstandingLitigation = objmoolahcoreView.IsoutstandingLitigation;
                    objmoolahcore.LatestYear = objmoolahcoreView.LatestYear;
                    objmoolahcore.PreiviousYear = objmoolahcoreView.PreviousYear;
                    objmoolahcore.NetprofitafterTax_LY = objmoolahcoreView.NetprofitafterTax_LY;
                    objmoolahcore.NetprofitafterTax_PY = objmoolahcoreView.NetprofitafterTax_PY;
                    objmoolahcore.NetprofitafterTax_LY = objmoolahcoreView.NetprofitafterTax_LY;
                    objmoolahcore.NetprofitafterTax_PY = objmoolahcoreView.NetprofitafterTax_PY;
                    objmoolahcore.AvgThreeMntCashBalBankAcc_LY = objmoolahcoreView.AvgThreeMntCashBalBankAcc_LY;
                    objmoolahcore.AvgThreeMntCashBalBankAcc_PY = objmoolahcoreView.AvgThreeMntCashBalBankAcc_PY;
                    objmoolahcore.Profitable = objmoolahcoreView.NetprofitafterTax_LY > 0;
                    objmoolahcore.Profitable_PY = objmoolahcoreView.NetprofitafterTax_PY > 0;
                    objmoolahcore.LoanRequest_Id = request_id;
                    objmoolahcore.User_ID = Userid;
                    objmoolahcore.Datecreated = DateTime.UtcNow;

                    objmoolahcore.ProfitBeforIntAndTax_LY = objmoolahcoreView.ProfitBeforIntAndTax_LY;
                    objmoolahcore.ProfitBeforIntAndTax_PY = objmoolahcoreView.ProfitBeforIntAndTax_PY;
                    objmoolahcore.InterestExpense_LY = objmoolahcoreView.InterestExpense_LY;
                    objmoolahcore.InterestExpense_PY = objmoolahcoreView.InterestExpense_PY;

                    objmoolahcore.EBITDA_LY = objmoolahcoreView.EBITDA_LY;
                    objmoolahcore.EBITDA_PY = objmoolahcoreView.EBITDA_PY;

                    objmoolahcore.DebtEBITDARatio_LY = objmoolahcoreView.DebtEBITDARatio_LY;
                    objmoolahcore.DebtEBITDARatio_PY = objmoolahcoreView.DebtEBITDARatio_PY;

                    objmoolahcore.InterestCoverageRatio_LY = objmoolahcoreView.InterestCoverageRatio_LY;
                    objmoolahcore.InterestCoverageRatio_PY = objmoolahcoreView.InterestCoverageRatio_PY;

                    db.tbl_MoolahCore.Add(objmoolahcore);
                    db.SaveChanges();
                    id = objmoolahcore.MoolahCoreId;
                }

                return id + "Â" + ContollerExtensions.ReturnSuccess();
            }

            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }

        public void UpdateLoadLoanRequestMoolahperi(LoanRequestMoolahPeri loanRequest)
        {
            var moolahPeri = db.tbl_MoolahPeri.SingleOrDefault(mp => mp.Request_ID == loanRequest.Request_ID);
            if (moolahPeri != null)
            {
                loanRequest.Accreditation = moolahPeri.Accreditation;
                loanRequest.BiodataOfDS = moolahPeri.BiodataOfDS;
                loanRequest.BusinessAwards = moolahPeri.BusinessAwards;
                loanRequest.CommunityInitiative = moolahPeri.CommunityInitiative;
                loanRequest.DigitalFootPrint = moolahPeri.DigitalFootPrint;
                loanRequest.MoolahPerk_Custom = moolahPeri.MoolahPerk_Custom;

                loanRequest.MoolahPerk_Id = moolahPeri.MoolahPerk_Id;
                if (moolahPeri.MoolahPerk_Custom != null)
                {
                    moolahPeri.MoolahPerk_Custom = moolahPeri.MoolahPerk_Custom;
                }
                else
                {
                    moolahPeri.MoolahPerk_Id = moolahPeri.MoolahPerk_Id;
                }

                loanRequest.MTA = moolahPeri.MTA;
                loanRequest.DateCreated = moolahPeri.DateCreated;
            }
        }

        /// <summary>
        /// Method for saving loan request moolahCore of borrower
        /// </summary>       
        /// <returns></returns>
        public string SaveLoanrequestmoolahPeri(LoanRequestMoolahPeri objmoolahPeriView, bool isDraft)
        {
            try
            {
                Int64 Userid = Convert.ToInt64(db.tbl_Users.Where(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).Select(p => p.UserID).FirstOrDefault());
                Int64 request_id = 0;
                if (HttpContext.Current.Session["LoanrequestId"] != null)
                {
                    request_id = Convert.ToInt64(HttpContext.Current.Session["LoanrequestId"]);
                }
                var CheckLoanRequestExist = db.tbl_MoolahPeri.SingleOrDefault(p => p.Request_ID == request_id);

                if (!isDraft)
                {
                    // Update the submit status
                    var request = db.tbl_LoanRequests.SingleOrDefault(p => p.RequestId == request_id);
                    request.Submitted = true;
                    request.PreliminaryLoanStatus = (int)PreliminaryLoanRequestStatus.DraftSubmitted;

                    MoolahLogManager.Log(AuditMessages.RAISE_A_NEW_LOAN, LogType.Info,
                    HttpContext.Current.User.Identity != null ? HttpContext.Current.User.Identity.Name : string.Empty,
                    request_id);
                }

                if (CheckLoanRequestExist != null)
                {
                    CheckLoanRequestExist.Accreditation = objmoolahPeriView.Accreditation;
                    CheckLoanRequestExist.BiodataOfDS = objmoolahPeriView.BiodataOfDS;
                    CheckLoanRequestExist.BusinessAwards = objmoolahPeriView.BusinessAwards;
                    CheckLoanRequestExist.CommunityInitiative = objmoolahPeriView.CommunityInitiative;
                    CheckLoanRequestExist.DigitalFootPrint = objmoolahPeriView.DigitalFootPrint;
                    LoanRequestMoolahPeri obj = new LoanRequestMoolahPeri();
                    CheckLoanRequestExist.MoolahPerk_Id = objmoolahPeriView.MoolahPerk_Id;
                    if (objmoolahPeriView.MoolahPerk_Id != 5)
                    {
                        CheckLoanRequestExist.MoolahPerk_Custom = null;
                    }
                    else
                    {
                        CheckLoanRequestExist.MoolahPerk_Custom = objmoolahPeriView.MoolahPerk_Custom;
                    }

                    CheckLoanRequestExist.MTA = objmoolahPeriView.MTA;
                    CheckLoanRequestExist.Request_ID = request_id;
                    CheckLoanRequestExist.User_ID = Userid;
                    CheckLoanRequestExist.DateUpdated = DateTime.UtcNow;
                    db.SaveChanges();
                }
                else
                {
                    tbl_MoolahPeri objmoolahPeri = new tbl_MoolahPeri();
                    objmoolahPeri.Accreditation = objmoolahPeriView.Accreditation;
                    objmoolahPeri.BiodataOfDS = objmoolahPeriView.BiodataOfDS;
                    objmoolahPeri.BusinessAwards = objmoolahPeriView.BusinessAwards;
                    objmoolahPeri.CommunityInitiative = objmoolahPeriView.CommunityInitiative;
                    objmoolahPeri.DigitalFootPrint = objmoolahPeriView.DigitalFootPrint;
                    objmoolahPeri.MoolahPerk_Id = objmoolahPeriView.MoolahPerk_Id;
                    if (objmoolahPeriView.MoolahPerk_Id != 5)
                    {
                        objmoolahPeri.MoolahPerk_Custom = null;
                    }
                    else
                    {
                        objmoolahPeri.MoolahPerk_Custom = objmoolahPeriView.MoolahPerk_Custom;
                    }

                    objmoolahPeri.MTA = objmoolahPeriView.MTA;
                    objmoolahPeri.Request_ID = request_id;
                    objmoolahPeri.User_ID = Userid;
                    objmoolahPeri.DateCreated = DateTime.UtcNow;
                    db.tbl_MoolahPeri.Add(objmoolahPeri);
                    db.SaveChanges();
                }

                return ContollerExtensions.ReturnSuccess();
            }

            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }




        /// <summary>
        /// Method for getting list of titles for dropdown
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> TitleItems()
        {
            var Titles = new List<SelectListItem>()
                           {
                               new SelectListItem(){ Text = "Mr.", Value= "Mr."},
                               new SelectListItem(){ Text = "Miss", Value= "Miss"},
                               new SelectListItem(){ Text = "Mrs.", Value= "Mrs."},
                               new SelectListItem(){ Text = "Dr.", Value= "Dr."}
                            };
            return Titles;
        }

        /// <summary>
        /// Method for getting list of ispersonalguarantee List
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> IsPersonalguarantee()
        {
            var Titles = new List<SelectListItem>()
                           {
                                new SelectListItem(){ Text = "No", Value= "false", Selected = false}  ,     
                               new SelectListItem(){ Text = "Yes", Value= "true", Selected = false} 
                                                    
                            };
            return Titles;
        }



        /// <summary>
        /// Method for getting list of litigation List
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> OutstandingLitigation()
        {
            var Litigation = new List<SelectListItem>()
                           {
                                new SelectListItem(){ Value = "Overdraft Facility", Text = "Overdraft Facility" }  ,     
                                new SelectListItem(){ Value = "Unsecured Loan", Text = "Unsecured Loan" }, 
                                new SelectListItem(){ Value = "Trade Creditors", Text = "Trade Creditors" },
                                new SelectListItem(){ Value = "Secured Loan", Text = "Secured Loan" },  
                                new SelectListItem(){ Value = "Factoring Discount", Text = "Factoring Discount" }
                            };
            return Litigation;
        }

        private List<SelectListItem> MoolaCoreTime()
        {
            var Time = new List<SelectListItem>()
            {
                new SelectListItem(){ Value = "Weekly", Text = "Weekly" }  ,     
                new SelectListItem(){ Value = "Monthly", Text = "Monthly" }, 
            };
            return Time;
        }
        ///////////////////////Add a custom value for Moolah perks for lenders
        private List<SelectListItem> AddCoustomValueinEnd()
        {
            var AddinListItem = db.tbl_MoolahPerksForLenders.Where(p => p.Isactive == true).OrderBy(p => p.MoolahPerk).ToList();
            var FinalList = new List<SelectListItem>();
            foreach (var moolahperk in AddinListItem)
            {
                FinalList.Add(new SelectListItem() { Value = moolahperk.MoolahPerkId.ToString(), Text = moolahperk.MoolahPerk.ToString() });
            }

            return FinalList;

        }
        ///////////////////////////////////////////////////Loan request edit functions/////////////////////////////////////////
        #region LoanRequest

        public LoanRequestMoolahCore GetMoolahCoreDetailbyLoanRequestID(int LoanRequestID)
        {
            LoanRequestMoolahCore objloanmoolahcore = new LoanRequestMoolahCore();
            try
            {
                List<OutstandingLitigation> obj = new List<OutstandingLitigation>();

                var MoolahCoreDetail = db.tbl_MoolahCore.FirstOrDefault(p => p.LoanRequest_Id == LoanRequestID);

                OutstandingLitigation objlitigation = new OutstandingLitigation();
                objlitigation._RepaymentPeriod = new SelectList(MoolaCoreTime(), "Value", "Text");
                objloanmoolahcore._Litigation = new SelectList(OutstandingLitigation(), "Value", "Text");
                objloanmoolahcore.Litigation = objlitigation;
                if (MoolahCoreDetail != null)
                {


                    objloanmoolahcore.MoolahCoreId = MoolahCoreDetail.MoolahCoreId;

                    objloanmoolahcore.Turnovers_LY = MoolahCoreDetail.Turnovers_LY;

                    objloanmoolahcore.ProfitBeforIntAndTax_LY = MoolahCoreDetail.ProfitBeforIntAndTax_LY;
                    objloanmoolahcore.ProfitBeforIntAndTax_PY = MoolahCoreDetail.ProfitBeforIntAndTax_PY;

                    objloanmoolahcore.InterestExpense_LY = MoolahCoreDetail.InterestExpense_LY;
                    objloanmoolahcore.InterestExpense_PY = MoolahCoreDetail.InterestExpense_PY;

                    objloanmoolahcore.InterestCoverageRatio_LY = MoolahCoreDetail.InterestCoverageRatio_LY;
                    objloanmoolahcore.InterestCoverageRatio_PY = MoolahCoreDetail.InterestCoverageRatio_PY;

                    objloanmoolahcore.NetprofitafterTax_LY = MoolahCoreDetail.NetprofitafterTax_LY.HasValue ? MoolahCoreDetail.NetprofitafterTax_LY.Value : 0;
                    objloanmoolahcore.CurrentAssests_LY = MoolahCoreDetail.CurrentAssests_LY;
                    objloanmoolahcore.CurrentRatio_LY = MoolahCoreDetail.CurrentRatio_LY;
                    objloanmoolahcore.TotalDebt_LY = MoolahCoreDetail.TotalDebt_LY;
                    objloanmoolahcore.TotalShareholdersequity_LY = MoolahCoreDetail.TotalShareholdersequity_LY;
                    objloanmoolahcore.Debt_Equity_LY = MoolahCoreDetail.Debt_Equity_LY;
                    objloanmoolahcore.CashFlowfromOperations_LY = MoolahCoreDetail.CashFlowfromOperations_LY;
                    objloanmoolahcore.CurrentAssests_PY = MoolahCoreDetail.CurrentAssests_PY;
                    objloanmoolahcore.CurrentLiabilities_PY = MoolahCoreDetail.CurrentLiabilities_PY;
                    objloanmoolahcore.TotalDebt_PY = MoolahCoreDetail.TotalDebt_PY;
                    objloanmoolahcore.TotalShareholdersequity_PY = MoolahCoreDetail.TotalShareholdersequity_PY;
                    objloanmoolahcore.Debt_Equity_PY = MoolahCoreDetail.Debt_Equity_PY;
                    objloanmoolahcore.CashFlowfromOperations_PY = MoolahCoreDetail.CashFlowfromOperations_PY;
                    objloanmoolahcore.IsoutstandingLitigation = MoolahCoreDetail.IsoutstandingLitigation;
                    objloanmoolahcore.CurrentLiabilities_LY = MoolahCoreDetail.CurrentLiabilities_LY;
                    objloanmoolahcore.Turnovers_PY = MoolahCoreDetail.Turnovers_PY;
                    objloanmoolahcore.CurrentRatio_PY = MoolahCoreDetail.CurrentRatio_PY;
                    objloanmoolahcore.NetprofitafterTax_PY = MoolahCoreDetail.NetprofitafterTax_PY;
                    objloanmoolahcore.OverdraftandFactoring = MakeHtmlTableforOverDraft(MoolahCoreDetail.MoolahCoreId, obj);
                    objloanmoolahcore.Loans = MakeHtmlTableforLoans(MoolahCoreDetail.MoolahCoreId, obj);
                    objloanmoolahcore.Trade = MakeHtmlTableforTrade(MoolahCoreDetail.MoolahCoreId, obj);
                    objloanmoolahcore.obj = obj;
                    objloanmoolahcore.LitigationType = string.Empty;

                    var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault();

                    if (user != null)
                        objloanmoolahcore.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);


                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objloanmoolahcore = null;
            }

            return objloanmoolahcore;

        }


        public string MakeHtmlTableforOverDraft(long MoolahCoreID, List<OutstandingLitigation> obj)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                var LitigationDetail = db.tbl_OutStandingLitigation.Where(o => o.MoolahCore_Id == MoolahCoreID && (o.LitigationType == "Factoring Discount" || o.LitigationType == "Overdraft Facility")).ToList();
                if (LitigationDetail.Count > 0)
                {

                    foreach (tbl_OutStandingLitigation o in LitigationDetail)
                    {
                        OutstandingLitigation objoverdraft = new OutstandingLitigation();
                        objoverdraft.LitigationType = o.LitigationType;
                        objoverdraft.AmountOutstanding = o.AmountOutstanding;
                        objoverdraft.CreditLimit = o.CreditLimit;
                        objoverdraft.UsageinLastMonth = o.UsageinLastMonth;
                        objoverdraft.RenewalDate = o.RenewalDate;
                        objoverdraft.RepaymentAmount = o.RepaymentAmount;
                        objoverdraft._RepaymentPeriodSet = o.RepaymentPeriod;
                        objoverdraft.DateofLastPayment = o.DateofLastPayment;
                        objoverdraft.Description = o.Description;
                        objoverdraft.PaymentTerms = o.PaymentTerms;
                        //objoverdraft.MoolahCore_Id=                 
                        obj.Add(objoverdraft);
                        //////delete button is rmoved as client said that loan request should be view only <td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + obj.IndexOf(objoverdraft) + ")' /> </td>
                        sb.Append("<tr id=" + obj.IndexOf(objoverdraft) + " class=bg_col1><td class=bdr_left><a>" + o.AmountOutstanding + "</a></td><td>" + o.CreditLimit + " </td><td>" + o.UsageinLastMonth + "</td><td>" + Convert.ToDateTime(o.RenewalDate).ToString("dd-MMM-yyyy") + "  </td></tr>");
                    }
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                sb = null;
            }
            return sb.ToString();
        }
        public string MakeHtmlTableforLoans(long MoolahCoreID, List<OutstandingLitigation> obj)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                var LitigationDetail = db.tbl_OutStandingLitigation.Where(o => o.MoolahCore_Id == MoolahCoreID && (o.LitigationType == "Unsecured Loan" || o.LitigationType == "Secured Loan")).ToList();
                if (LitigationDetail.Count > 0)
                {
                    foreach (tbl_OutStandingLitigation o in LitigationDetail)
                    {
                        if (o.LitigationType.Trim() == "Unsecured Loan")
                        {
                            OutstandingLitigation objoverdraft = new OutstandingLitigation();
                            objoverdraft.MoolahCore_Id = o.MoolahCore_Id;
                            objoverdraft.LitigationType = o.LitigationType;
                            objoverdraft.AmountOutstanding = o.AmountOutstanding;
                            objoverdraft.CreditLimit = o.CreditLimit;
                            objoverdraft.UsageinLastMonth = o.UsageinLastMonth;
                            objoverdraft.RenewalDate = o.RenewalDate;
                            objoverdraft.RepaymentAmount = o.RepaymentAmount;
                            objoverdraft._RepaymentPeriodSet = o.RepaymentPeriod;
                            objoverdraft.DateofLastPayment = o.DateofLastPayment;
                            objoverdraft.Description = o.Description;
                            objoverdraft.PaymentTerms = o.PaymentTerms;
                            //objoverdraft.MoolahCore_Id=                 
                            obj.Add(objoverdraft);
                            /////////delete button removed for view   only
                            //<td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + obj.IndexOf(objoverdraft) + ")' /> </td>
                            sb.Append("<tr id=" + obj.IndexOf(objoverdraft) + " class=bg_col1><td class=bdr_left><a>" + o.AmountOutstanding + "</a></td><td>" + o.RepaymentAmount + " </td><td>" + o.RepaymentPeriod + "</td><td>" + Convert.ToDateTime(o.DateofLastPayment).ToString("dd-MM-yyyy") + "  </td><td>NA </td></tr>");
                        }
                        else if (o.LitigationType.Trim() == "Secured Loan")
                        {
                            OutstandingLitigation objoverdraft = new OutstandingLitigation();
                            objoverdraft.MoolahCore_Id = o.MoolahCore_Id;
                            objoverdraft.LitigationType = o.LitigationType;
                            objoverdraft.AmountOutstanding = o.AmountOutstanding;
                            objoverdraft.CreditLimit = o.CreditLimit;
                            objoverdraft.UsageinLastMonth = o.UsageinLastMonth;
                            objoverdraft.RenewalDate = o.RenewalDate;
                            objoverdraft.RepaymentAmount = o.RepaymentAmount;
                            objoverdraft._RepaymentPeriodSet = o.RepaymentPeriod;
                            objoverdraft.DateofLastPayment = o.DateofLastPayment;
                            objoverdraft.Description = o.Description;
                            objoverdraft.PaymentTerms = o.PaymentTerms;
                            //objoverdraft.MoolahCore_Id=                 
                            obj.Add(objoverdraft);
                            //delete button removed fro view only
                            //<td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + obj.IndexOf(objoverdraft) + ")' /> </td>
                            sb.Append("<tr id=" + obj.IndexOf(objoverdraft) + " class=bg_col1><td class=bdr_left><a>" + o.AmountOutstanding + "</a></td><td>" + o.RepaymentAmount + " </td><td>" + o.RepaymentPeriod + "</td><td>" + Convert.ToDateTime(o.DateofLastPayment).ToString("dd-MM-yyyy") + "  </td><td>" + o.Description + " </td></tr>");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                sb = null;
            }
            return sb.ToString();
        }
        public string MakeHtmlTableforTrade(long MoolahCoreID, List<OutstandingLitigation> obj)
        {
            StringBuilder sb = new StringBuilder();
            try
            {
                var LitigationDetail = db.tbl_OutStandingLitigation.Where(o => o.MoolahCore_Id == MoolahCoreID && (o.LitigationType.Trim() == "Trade Creditors" || o.LitigationType.Trim() == "Trade Creditors")).ToList();
                if (LitigationDetail.Count > 0)
                {
                    foreach (tbl_OutStandingLitigation o in LitigationDetail)
                    {
                        OutstandingLitigation objoverdraft = new OutstandingLitigation();
                        objoverdraft.MoolahCore_Id = o.MoolahCore_Id;
                        objoverdraft.LitigationType = o.LitigationType;
                        objoverdraft.AmountOutstanding = o.AmountOutstanding;
                        objoverdraft.CreditLimit = o.CreditLimit;
                        objoverdraft.UsageinLastMonth = o.UsageinLastMonth;
                        objoverdraft.RenewalDate = o.RenewalDate;
                        objoverdraft.RepaymentAmount = o.RepaymentAmount;
                        objoverdraft._RepaymentPeriodSet = o.RepaymentPeriod;
                        objoverdraft.DateofLastPayment = o.DateofLastPayment;
                        objoverdraft.Description = o.Description;
                        objoverdraft.PaymentTerms = o.PaymentTerms;
                        //objoverdraft.MoolahCore_Id=                 
                        obj.Add(objoverdraft);
                        ///delete button removed fro view only
                        ///<td><img src='../content/images/close_btn.png' style='cursor:pointer;' onclick='DeleteRecordFromCollection(" + obj.IndexOf(objoverdraft) + ")' /> </td>
                        sb.Append("<tr id=" + obj.IndexOf(objoverdraft) + " class=bg_col1><td class=bdr_left><a>" + o.AmountOutstanding + "</a></td><td>" + o.PaymentTerms + " </td></tr>");
                    }
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                sb = null;
            }
            return sb.ToString();
        }


        public LoanRequestMoolahPeri GetMoolahPeriDetailbyLoanRequestID(int LoanRequestID)
        {

            LoanRequestMoolahPeri objloanMollahperi = new LoanRequestMoolahPeri();
            try
            {
                var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault();

                if (user != null)
                    objloanMollahperi.Accountnumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);

                var ListofMoolahPeri = db.tbl_MoolahPeri.ToList();
                var MoolahCoreDetail = ListofMoolahPeri.FirstOrDefault(p => p.Request_ID == LoanRequestID);
                objloanMollahperi.MoolahPerk = AddCoustomValueinEnd();
                if (MoolahCoreDetail != null)
                {
                    objloanMollahperi.BusinessAwards = MoolahCoreDetail.BusinessAwards;
                    objloanMollahperi.Accreditation = MoolahCoreDetail.Accreditation;
                    objloanMollahperi.MTA = MoolahCoreDetail.MTA;
                    objloanMollahperi.DigitalFootPrint = MoolahCoreDetail.DigitalFootPrint;
                    objloanMollahperi.CommunityInitiative = MoolahCoreDetail.CommunityInitiative;
                    objloanMollahperi.BiodataOfDS = MoolahCoreDetail.BiodataOfDS;
                    objloanMollahperi.MoolahPerk_Id = MoolahCoreDetail.MoolahPerk_Id;
                    objloanMollahperi.MoolahPerk_Custom = MoolahCoreDetail.MoolahPerk_Custom;
                    if (MoolahCoreDetail.MoolahPerk_Id != null)
                    {
                        objloanMollahperi.MoolahPerkset = MoolahCoreDetail.MoolahPerk_Id.ToString();
                    }
                    else
                    {
                        objloanMollahperi.MoolahPerkset = "Custom";
                    }
                }
            }

            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objloanMollahperi = null;
            }
            return objloanMollahperi;
        }

        /// <summary>
        /// //////////////////////////////Delte all items from litgation before inserting new records
        /// </summary>
        /// <param name="fileToDelete"></param>
        /// 
        public void DelteAllLitigationRelatedTomoolahcoreid(long? MoolahCoreID)
        {
            //////////////////////It will be usefull while updating, delete existing litigation with moolachcoreid//////////////////////
            try
            {
                var GetAllrecords = db.tbl_OutStandingLitigation.Where(e => e.MoolahCore_Id == MoolahCoreID).ToList();
                foreach (var i in GetAllrecords)
                {
                    db.tbl_OutStandingLitigation.Remove(i);

                }
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);

            }
            //////////////////end////////////////////////////////////////////////
        }
        #endregion


        #region Dashboard
        public BorrowerDashboard FillBorrowerDashboard()
        {
            BorrowerDashboard objBorrowerDashboard = new BorrowerDashboard();
            try
            {
                Dashboard objDashboard = new Dashboard();
                var commonUser = new UserCommonOperations();

                #region Common

                long UserID = Convert.ToInt64((from p in db.tbl_Users
                    where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                    select p.UserID).FirstOrDefault());
                var AccountDetail = db.tbl_AccountDetails.FirstOrDefault(a => a.User_ID == UserID);
                    Common objCommon = new Common();
                    objCommon.AccountName = AccountDetail.AccountName;
                    objCommon.BusinessName = AccountDetail.BusinessName;

                var user =
                    db.tbl_Users.Where(
                        x => x.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower())
                        .SingleOrDefault();

                if (user != null)
                    objCommon.AccountNumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);

                #endregion

                List<tbl_LoanRequests> objtblLoanRequests =
                    ContollerExtensions.GetLoanRequestbyUserIDandInTimePeriod(UserID, false);
                IEnumerable<tbl_LoanRequests> objtblLoanRequest =
                    objtblLoanRequests.Where(l => l.LoanStatus != (int) LoanRequestStatus.InProgress);

                #region Loan Offeres

                //LoanOfferesDashboard objLoanOffersDashboard = new LoanOfferesDashboard();                
                var LoanOffers = (from p in objtblLoanRequest
                    join q in db.tbl_Loanoffers
                        on p.RequestId equals q.LoanRequest_Id
                    join r in db.tbl_AccountDetails
                        on q.Investor_Id equals r.User_ID
                        into leftJoin
                    from r in leftJoin.DefaultIfEmpty()
                    where
                        q.OfferStatus == (int) LoanOfferStatus.Pending ||
                        q.OfferStatus == (int) LoanOfferStatus.Accepted
                    select new
                    {
                        objtblLoanRequest = p,
                        tbl_AccountDetails = r,
                        tbl_Loanoffers = q
                    }).ToList();
                //var ApprovedLoanOffers =
                //    LoanOffers.Where(l => l.tbl_Loanoffers.OfferStatus == (int) LoanOfferStatus.Accepted).ToList();

                #endregion

                #region Loan Request

                if (objtblLoanRequest != null)
                {



                    //List<tbl_LoanPurposesList> objLoanPurpose = db.tbl_LoanPurposesList.ToList();
                    List<LoanRequestDashboard> objLoanRequestList = new List<LoanRequestDashboard>();
                    var loanLIst = db.tbl_LoanRequests.Where(
                        l =>
                            l.tbl_Users.UserName.Trim().ToLower() ==
                            HttpContext.Current.User.Identity.Name.Trim().ToLower()
                            && l.IsApproved.HasValue
                            && l.IsApproved.Value
                                && (l.LoanStatus == (int) LoanRequestStatus.Pending
                                    || l.LoanStatus == (int) LoanRequestStatus.InProgress
                                    || l.LoanStatus == (int) LoanRequestStatus.Cancelled)
                                && l.PreliminaryLoanStatus > 1);

                    foreach (tbl_LoanRequests i in loanLIst)
                    {
                        //var GetApprovedLoanOffers =
                        //    db.tbl_Loanoffers.Where(
                        //        l => l.LoanRequest_Id == i.RequestId && l.OfferStatus == (int) LoanOfferStatus.Accepted).ToList();

                        //decimal? TotalLoanOffersAmount = null;
                        //if (GetApprovedLoanOffers.Count() > 0)
                        //    TotalLoanOffersAmount = GetApprovedLoanOffers.Sum(l => l.OfferedAmount);

                        LoanRequestDashboard objDashboardProps = new LoanRequestDashboard();
                        objDashboardProps.Reference = Convert.ToString(i.RequestId);
                        objDashboardProps.Amount = i.Amount;
                        objDashboardProps.LoanRequestID = i.RequestId;
                        objDashboardProps.Purpose = i.tbl_LoanPurposesList.PurposeName;
                        objDashboardProps.Tenor = i.Terms;
                        objDashboardProps.PaymentGrade = "Fair static"; ///its static for now it will be set by admin
                        objDashboardProps.Rate = Convert.ToDecimal(i.Rate);
                        objDashboardProps.OfferStatus = i.LoanStatus.ToLoanStatusDisplayName();
                        objDashboardProps.TotalInterestPayable = CommonMethods.GetTotalInterestPayable(i.Terms,
                            i.Amount.HasValue ? (double) i.Amount : 0, (double) i.Rate);
                        objDashboardProps.LoanStatus = i.LoanStatus;
                        objDashboardProps.PreliminaryStatus = i.PreliminaryLoanStatus;
                        objDashboardProps.Status =
                            CommonMethods.GetLoanStatus((PreliminaryLoanRequestStatus) i.PreliminaryLoanStatus,
                                (LoanRequestStatus) i.LoanStatus);
                        decimal?[] arr = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(i.RequestId,
                            i.Amount);
                        objDashboardProps.AvgRate = Math.Round(Convert.ToDecimal(arr[1]), 2);
                        objDashboardProps.AvgFunded = Math.Round(Convert.ToDecimal(arr[0]), 2);
                        objDashboardProps.AcceptedDate = i.ApproveDate.HasValue
                            ? (DateTime) i.ApproveDate
                            : DateTime.MinValue;
                        objLoanRequestList.Add(objDashboardProps);
                    }
                    objDashboard.LoanRequestList = objLoanRequestList;

                    #region GraphGraphOustandingLoan

                    //1) Aggregate Outstanding Amount. - Total amount of loans outstanding for each month. The formula require you to add the total outstanding loans together. For example, for that month loan 1 total amount outstanding is 10,000 and loan 2 total amount outstanding is 30,000 and therefore aggregate outstanding for that month will be $40,000
                    //2) Current Repayment - Total Current monthly repayment. The formula require you to add the total current repayment. For example, for loan 1, for that month, the repayment iS$1000 and loan 2 repayment iS$500. The current repayment iS$1500. 
                    //3) Aggregate Repayment for live loans. - This mean the total repayment for live loans. For example, for this month, borrower repay $10,000 and have already repaid $40,000 for outstanding live loans, then aggregate repayment for lives loans will be $50,000. When any of the loan has been fully repaid, you will need to remove that amount from the aggregate repayment for live loans as it is no longer a live loan.


                    var EmiDetailForGraph = (from p in db.tbl_LoanRequests
                        join q in db.tbl_LoanAmortization
                            on p.RequestId equals q.LoanRequest_ID

                            into leftJoin
                        from q in leftJoin.DefaultIfEmpty()
                        where p.LoanStatus == (int) LoanRequestStatus.Matched && p.User_ID == UserID

                        select new
                        {
                            objtblLoanRequest = p,
                            tbl_LoanAmortization = q
                        }).ToList();

                    if (EmiDetailForGraph != null)
                    {
                        List<string> objGraphList = new List<string>();
                        if (EmiDetailForGraph.Count > 0 &&
                            EmiDetailForGraph.FirstOrDefault().tbl_LoanAmortization != null)
                        {

                            var MindateFirst = EmiDetailForGraph.FirstOrDefault();

                            DateTime? MinDate = null;
                            if (MindateFirst.tbl_LoanAmortization != null)
                                MinDate = MindateFirst.tbl_LoanAmortization.EmiDate;

                            var MaxDate =
                                EmiDetailForGraph.Where(x => x.tbl_LoanAmortization != null)
                                    .OrderByDescending(l => l.tbl_LoanAmortization.EmiDate)
                                    .Select(l => l.tbl_LoanAmortization.EmiDate)
                                    .FirstOrDefault();
                           
                            int StartMonthofLoan =
                                db.tbl_LoanRequests.Where(
                                    l => l.User_ID == UserID && l.LoanStatus == (int) LoanRequestStatus.Matched)
                                    .Select(l => l.DateCreated.Value.Month)
                                    .FirstOrDefault();
                            decimal? TotalLoan = 0;
                            int i = 0;
                            string GraphString = string.Empty;
                            foreach (var GraphGenration in EmiDetailForGraph)
                            {

                                if (i != 0)
                                {
                                    GraphOustandingLoan objGraph = new GraphOustandingLoan();

                                    decimal? TotalOfEmisOn =
                                        db.tbl_LoanAmortization.Where(
                                            l =>
                                                l.PayedOn.Value.Month == MinDate.Value.Month &&
                                                l.PayedOn.Value.Year == MinDate.Value.Year).Sum(l => l.EmiAmount);
                                    decimal? IntrestTotal =
                                        db.tbl_LoanAmortization.Where(
                                            l =>
                                                l.PayedOn.Value.Month == MinDate.Value.Month &&
                                                l.PayedOn.Value.Year == MinDate.Value.Year).Sum(l => l.Interest);
                                    if (TotalOfEmisOn != null && IntrestTotal != null)
                                    {
                                        TotalLoan = TotalLoan - (TotalOfEmisOn - IntrestTotal);
                                        objGraph.AggOutstandingAmount = TotalLoan;
                                        objGraph.Month = ConvertNumberintoMonth(MinDate.Value.Month) + " " +
                                                         MinDate.Value.Year.ToString();
                                        objGraph.CurrentRepayment =
                                            db.tbl_LoanAmortization.Where(
                                                l =>
                                                    l.PayedOn.Value.Month == MinDate.Value.Month &&
                                                    l.PayedOn.Value.Year == MinDate.Value.Year).Sum(l => l.EmiAmount);
                                        objGraph.CurrentRepayment = TotalOfEmisOn;
                                        objGraph.AggRepaymentForLiveLoans =
                                            db.tbl_LoanAmortization.Where(
                                                l =>
                                                    l.PayedOn.Value.Month <= MinDate.Value.Month &&
                                                    l.PayedOn.Value.Year <= MinDate.Value.Year).Sum(l => l.EmiAmount);

                                        MinDate = MinDate.Value.AddMonths(1);

                                        //  DateTime? testing = test.EmiDate;
                                        //    int tt = testing;
                                        GraphString = "['" + objGraph.Month + "'," +
                                                      Math.Round(Convert.ToDecimal(objGraph.AggOutstandingAmount), 2) +
                                                      "," + Math.Round(Convert.ToDecimal(objGraph.CurrentRepayment), 2) +
                                                      "," +
                                                      Math.Round(Convert.ToDecimal(objGraph.AggRepaymentForLiveLoans), 2) +
                                                      "],";
                                        objGraphList.Add(GraphString);
                                        if (MinDate.Value.Date > MaxDate.Value.Date)
                                            break;
                                    }
                                    else
                                    {

                                        GraphString = "['" + ConvertNumberintoMonth(MinDate.Value.Month) + " " +
                                                      MinDate.Value.Year + "'," +
                                                      Math.Round(Convert.ToDecimal(TotalLoan), 0) + "," + 0 + "," + 0 +
                                                      "],";
                                        objGraphList.Add(GraphString);
                                        MinDate = MinDate.Value.AddMonths(1);
                                    }

                                }
                                else
                                {

                                    GraphString = "['" + "Total" + "'," + Math.Round(Convert.ToDecimal(TotalLoan), 0) +
                                                  "," + 0 + "," + 0 + "],";
                                    objGraphList.Add(GraphString);
                                }
                                i++;

                            }

                        }
                        objDashboard.GraphOutstandingLoan = objGraphList;
                    }

                    #endregion

                }

                #endregion

                //second tab on dashboard
                objBorrowerDashboard.Dashboard = objDashboard;
                objBorrowerDashboard.CommonOperations = objCommon;


            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                //objBorrowerDashboard = null;
            }

            return objBorrowerDashboard;
        }

        public CompanyProfile GetCompayProfile()
        {
            long UserID = Convert.ToInt64((from p in db.tbl_Users
                                           where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                           select p.UserID).FirstOrDefault());
            var AccountDetail = db.tbl_AccountDetails.FirstOrDefault(a => a.User_ID == UserID);
            #region CompanyProfile

            CompanyProfile objCompanyProfile = new CompanyProfile();
            objCompanyProfile.AccountName = AccountDetail.AccountName;
            objCompanyProfile.Bank = AccountDetail.tbl_BanksList.BankName;
            objCompanyProfile.BankAccount = AccountDetail.AccountNumber;
            objCompanyProfile.BankBranch = AccountDetail.BankBranch;

            var mailingAddress = CommonMethods.LoadAddressField(AccountDetail.BusinessMailingAddress);
            objCompanyProfile.BusinessMailingAddress1 = mailingAddress[0];
            objCompanyProfile.BusinessMailingAddress2 = mailingAddress[1];
            objCompanyProfile.BusinessMailingPostalCode = mailingAddress[2];

            objCompanyProfile.CompanyName = AccountDetail.BusinessName;
            objCompanyProfile.CompanyRegNumber = AccountDetail.IC_CompanyRegistrationNumber;
            objCompanyProfile.CreditInformation = "static";
            objCompanyProfile.CreditPaymentGrade = "Fair static";
            objCompanyProfile.EmailAddress = new UserCommonOperations().GetCurrentUserEmail();
            objCompanyProfile.OtherTelephone = AccountDetail.OptionalContactnumber;

            var regAddress = CommonMethods.LoadAddressField(AccountDetail.RegisteredAddress);
            objCompanyProfile.RegisteredAddress1 = regAddress[0];
            objCompanyProfile.RegisteredAddress2 = regAddress[1];
            objCompanyProfile.RegisteredAddressPostalCode = regAddress[2];

            objCompanyProfile.TelephoneofficeHours = AccountDetail.Main_OfficeContactnumber;
            objCompanyProfile.TelephoneOutOfficeHours = AccountDetail.OptionalContactnumber;
            objCompanyProfile.MobileOutofOfficeHours = AccountDetail.Mobilenumber;
            objCompanyProfile.TypeofCompany = AccountDetail.BusinessOrganisation;
            objCompanyProfile.YearsinBssiness = "static";
            objCompanyProfile.DateofIncorporation = AccountDetail.DateofIncorporation;
            objCompanyProfile.Title = AccountDetail.Title;
            objCompanyProfile.FirstName = AccountDetail.FirstName;
            objCompanyProfile.LastName = AccountDetail.LastName;
            objCompanyProfile.Role = AccountDetail.RoleJobTitle;
            objCompanyProfile.SiccCode = AccountDetail.SiccCode;
            objCompanyProfile.Nationality = AccountDetail.Nationality;
            objCompanyProfile.NricNumber = AccountDetail.NRIC_Number;
            objCompanyProfile.PassportNumber = AccountDetail.PassportNumber;
            objCompanyProfile.PaidUpCapital = AccountDetail.PaidUpCapital == null
                ? 0.0
                : Convert.ToDouble(AccountDetail.PaidUpCapital);
            objCompanyProfile.NumOfEmployees = AccountDetail.NumOfEmployees == null
                ? 0
                : Convert.ToInt32(AccountDetail.NumOfEmployees);
            objCompanyProfile.BankNumber = AccountDetail.BankNumber;
            objCompanyProfile.BranchName = AccountDetail.BranchName;
            objCompanyProfile.BranchNumber = AccountDetail.BranchNumber;

            //objBorrowerDashboard.Dashboard = objDashboard;


            //objBorrowerDashboard.CommonOperations = objCommon;
            //objBorrowerDashboard.CompanyProfile = objCompanyProfile;

            ///Personel Profile 
            ProfileDetail objProfileDetail = new ProfileDetail();
            objProfileDetail.DisplayName = AccountDetail.DisplayName;
            objProfileDetail.Title = AccountDetail.Title;
            objProfileDetail.FirstName = AccountDetail.FirstName;
            objProfileDetail.LastName = AccountDetail.LastName;
            objProfileDetail.RoleJobTitle = AccountDetail.RoleJobTitle;

            //objBorrowerDashboard.ProfileDetail = objProfileDetail;

            #endregion

            return objCompanyProfile;
        }
        public List<LoanRequestDashboard> GetCurrentRequests()
        {
            List<LoanRequestDashboard> objLoanRequestList = new List<LoanRequestDashboard>();
            var loanLIst = db.tbl_LoanRequests.Where(
                l => l.tbl_Users.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                && l.IsApproved.HasValue
                && l.IsApproved.Value
                && (l.LoanStatus == (int)LoanRequestStatus.Pending
                || l.LoanStatus == (int)LoanRequestStatus.InProgress
                || l.LoanStatus == (int)LoanRequestStatus.Cancelled)
                && l.PreliminaryLoanStatus > 1);

            foreach (tbl_LoanRequests i in loanLIst)
            {
                var GetApprovedLoanOffers = db.tbl_Loanoffers.Where(l => l.LoanRequest_Id == i.RequestId && l.OfferStatus == (int)LoanOfferStatus.Accepted);

                decimal? TotalLoanOffersAmount = null;
                if (GetApprovedLoanOffers.Count() > 0)
                    TotalLoanOffersAmount = GetApprovedLoanOffers.Sum(l => l.OfferedAmount);

                LoanRequestDashboard objDashboardProps = new LoanRequestDashboard();
                objDashboardProps.Reference = Convert.ToString(i.RequestId);
                objDashboardProps.Amount = i.Amount;
                objDashboardProps.LoanRequestID = i.RequestId;
                objDashboardProps.Purpose = i.tbl_LoanPurposesList.PurposeName;
                objDashboardProps.Tenor = i.Terms;
                objDashboardProps.PaymentGrade = "Fair static";///its static for now it will be set by admin
                objDashboardProps.Rate = Convert.ToDecimal(i.Rate);
                objDashboardProps.OfferStatus = i.LoanStatus.ToLoanStatusDisplayName();
                objDashboardProps.TotalInterestPayable = CommonMethods.GetTotalInterestPayable(i.Terms,
                    i.Amount.HasValue ? (double)i.Amount : 0, (double)i.Rate);
                objDashboardProps.LoanStatus = i.LoanStatus;
                objDashboardProps.PreliminaryStatus = i.PreliminaryLoanStatus;
                objDashboardProps.Status = CommonMethods.GetLoanStatus((PreliminaryLoanRequestStatus)i.PreliminaryLoanStatus,
                    (LoanRequestStatus)i.LoanStatus);
                decimal?[] arr = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(i.RequestId, i.Amount);
                objDashboardProps.AvgRate = Math.Round(Convert.ToDecimal(arr[1]), 2);
                objDashboardProps.AvgFunded = Math.Round(Convert.ToDecimal(arr[0]), 2);
                objDashboardProps.AcceptedDate = i.ApproveDate.HasValue ? (DateTime)i.ApproveDate : DateTime.MinValue;
                objLoanRequestList.Add(objDashboardProps);
            }

            return objLoanRequestList;
        }

        public void AcceptorRejectLoanOffer(string LoanOfferIds, bool Flag)
        {
            try
            {

                //Get All unique Loan offers on wich user have accepted the offers, User can select the offers related to two or more loans.
                var LoanRequestID = db.tbl_LoanRequests.Select(l => l.LoanPurpose_Id).Distinct();
                //get all loan details 





                //string[] LoanofferIDs = LoanOfferIds.TrimEnd(',').Split(',');
                //foreach (string ID in LoanofferIDs)
                //{
                //    int i = Convert.ToInt32(ID);
                //    var tbl_LoanOffer = db.tbl_Loanoffers.FirstOrDefault(l => l.OfferId.Equals(i));
                //    tbl_LoanOffer.OfferStatus = Flag;
                //    db.SaveChanges();


                //}

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);

            }
        }

        /// <summary>    
        ///  if it return false means unique investors are less then 5, if true set loan status to appreoved and return true which shows unique investors are greater then or equls 5
        /// </summary>
        /// <param name="LoanRequestID"></param>
        /// <returns>bool</returns>
        public bool SetLoanStatustoApproved(int LoanRequestID)
        {
            int UniqueInvestor = db.tbl_Loanoffers.Where(l => l.LoanRequest_Id == LoanRequestID && l.OfferStatus == (int)LoanOfferStatus.Accepted).Select(l => l.Investor_Id).Distinct().Count();
            if (UniqueInvestor >= 5)
            {
                tbl_LoanRequests obj = new tbl_LoanRequests();
                obj = db.tbl_LoanRequests.Where(l => l.RequestId == LoanRequestID).FirstOrDefault();
                obj.IsApproved = true;
                db.SaveChanges();
                return true;
            }
            else
                return false;

        }

        #endregion

        public void DeleteFile(String fileToDelete)
        {
            try
            {
                FileInfo f = new FileInfo(fileToDelete);
                if (f.Exists)
                {
                    f.Delete();
                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);

            }
        }

        /// <summary>
        /// Use this function where you have the Distinct Investors for a particular loan. It will return the list of the first offer made by all distinct investors on a particular loan.
        /// </summary>
        /// <param name="InvestorID"></param>
        /// <param name="LoanRequestID"></param>
        private tbl_Loanoffers GetDistinctLoanOfferbyInvestor(long? InvestorID, long LoanRequestID)
        {
            return db.tbl_Loanoffers.FirstOrDefault(l => l.Investor_Id == InvestorID && l.LoanRequest_Id == LoanRequestID && l.OfferStatus == (int)LoanOfferStatus.Pending);

        }

        public void LoanAmotrization(decimal LoanAmount, int period, int installementMonth, int installmentyear, decimal? Rate, long LoanRequestID)
        {
            int monthValue = installementMonth;
            int yearValue = installmentyear;

            if (LoanAmount > 0 && period > 0 && monthValue > 0 && yearValue > 0 && Rate > 0)
            {

                var loanAmt = LoanAmount;
                var emi = calculateEMI(LoanAmount, Rate, period);
                var interestRate = Rate;
                var interestRateForMonth = interestRate / 12; // (Monthly Rate of Interest in %)
                var interestRateForMonthFraction = Convert.ToDecimal(interestRateForMonth) / 100; // (Monthly Interest Rate expressed as a fraction)

                var loanOustanding = loanAmt;
                decimal totalPayment = 0;
                decimal totalInterestPortion = 0;
                decimal totalPrincipal = 0;
                int month = 0;
                int year = 0;
                decimal interestPortion = 0;
                decimal principal = 0;
                for (int i = 1; i <= period; i++)
                {

                    if (monthValue != 0)
                        month = monthValue + i - 1;
                    else
                        month = month + 1;
                    if (month > 12)
                    {
                        year = yearValue + 1;
                        yearValue = year;
                        monthValue = 0;
                        month = monthValue + 1;
                    }
                    else
                    {
                        year = yearValue;
                    }


                    DateTime todaydate = DateTime.UtcNow;
                    DateTime installmentDate;


                    installmentDate = todaydate.AddMonths(i);


                    if (loanOustanding == loanAmt)
                    {
                        totalPayment = Convert.ToDecimal(totalPayment) + Convert.ToDecimal(emi);
                        interestPortion = Math.Round(loanOustanding * interestRateForMonthFraction, 0);


                    }
                    else
                    {
                        totalPayment = Convert.ToDecimal(totalPayment) + Convert.ToDecimal(emi);
                        interestPortion = loanOustanding * interestRateForMonthFraction;
                        interestPortion = Math.Round(interestPortion, 0);

                    }
                    loanOustanding = Convert.ToDecimal(loanOustanding) + Convert.ToDecimal(interestPortion) - Convert.ToDecimal(emi);
                    loanOustanding = Math.Round(loanOustanding, 0);

                    totalInterestPortion = Convert.ToDecimal(totalInterestPortion) + Convert.ToDecimal(interestPortion);
                    principal = Math.Round(Convert.ToDecimal(emi - interestPortion), 0);

                    totalPrincipal = Math.Round((Convert.ToDecimal(totalPrincipal) + Convert.ToDecimal(principal)), 0);

                    tbl_LoanAmortization objLoanAmortizatin = new tbl_LoanAmortization();
                    objLoanAmortizatin.Balance = loanOustanding;
                    objLoanAmortizatin.EmiAmount = emi;
                    objLoanAmortizatin.EmiDate = Convert.ToDateTime(installmentDate);
                    objLoanAmortizatin.Interest = interestPortion;
                    objLoanAmortizatin.PayStatus = (int)RepaymentPayStatus.Pending;
                    objLoanAmortizatin.AmountTotal = totalPayment;
                    objLoanAmortizatin.LoanRequest_ID = LoanRequestID;
                    db.tbl_LoanAmortization.Add(objLoanAmortizatin);
                    db.SaveChanges();
                }

            }


        }
        public decimal? calculateEMI(decimal LoanAmount, decimal? Rate, decimal period)
        {
            decimal loanAmt = 0;
            decimal interestRate = 0;
            decimal tenure = 0;
            loanAmt = LoanAmount;
            tenure = period;
            interestRate = Convert.ToDecimal(Rate);
            if (Convert.ToDecimal(interestRate) != 0)
            {
                var intr = Rate / 1200;
                double x = Convert.ToDouble(1 / (1 + intr));
                double y = Convert.ToDouble(period);

                decimal z = Convert.ToDecimal(Math.Pow(x, y));
                var final = Convert.ToDecimal(LoanAmount * intr / (1 - (z)));

                return Math.Round(final, 2);


            }
            else
            {
                var emi = loanAmt / tenure;
                var emiPerLakh = Math.Round(emi, 0);
                return emiPerLakh;
            }
        }
        /// <summary>
        /// set loan request to withdra and credite the offers money to user balance
        /// </summary>
        /// <param name="LoanRequestIDs"></param>
        public string WithDrawLoan(long LoanRequestID, out string statusCategory)
        {
            try
            {

                tbl_LoanRequests LoanRequest = new tbl_LoanRequests();
                LoanRequest = db.tbl_LoanRequests.FirstOrDefault(l => l.RequestId == LoanRequestID);
                if (LoanRequest.LoanStatus == (int)LoanRequestStatus.InProgress)
                {
                    long? LoanRequesID = Convert.ToInt64(LoanRequestID);
                    var LoanOffers = db.tbl_Loanoffers
                        .Where(p => p.LoanRequest_Id == LoanRequesID && p.OfferStatus == (int)LoanOfferStatus.Pending);//get pending loan offers

                    //make transaction and credit the balance in user account
                    foreach (tbl_Loanoffers m in LoanOffers.ToList())
                    {
                        tbl_Balances obj = new tbl_Balances();
                        obj = db.tbl_Balances.FirstOrDefault(l => l.User_ID == m.Investor_Id);

                        _ITransactionsDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID,
                            m.OfferedAmount, m.LoanRequest_Id, "Internet", "Listing Cancelled by Issuer", m.Investor_Id);
                        LoanTransactionCredit(obj.ActualAmount, m.OfferedAmount, m.LoanRequest_Id, "Internet", "Listing Cancelled by Issuer", m.Investor_Id, obj.ActualAmount + m.OfferedAmount);

                        obj.ActualAmount = obj.ActualAmount + m.OfferedAmount;
                        obj.LedgerAmount = obj.LedgerAmount + m.OfferedAmount;
                        db.SaveChanges();

                        m.OfferStatus = (int)LoanOfferStatus.Cancelled;
                        db.SaveChanges();

                        if (m.tbl_Users != null)
                            _IEmailSender.OnLoanRequestWithdrawn(m.tbl_Users.UserName, LoanRequestID.ToString());
                    }

                    //set status of loan request to withdrawn
                    long? Requesid = Convert.ToInt64(LoanRequesID);
                    tbl_LoanRequests LoanRequestDetail = new tbl_LoanRequests();
                    LoanRequestDetail = db.tbl_LoanRequests.FirstOrDefault(p => p.RequestId == Requesid);
                    LoanRequestDetail.LoanStatus = (int)LoanRequestStatus.Cancelled;  //set request status instead of loan status, cause loan status shows the admin action on the loan request

                    db.SaveChanges();

                    MoolahLogManager.Log(AuditMessages.CANCEL_LOAN_LISTING, LogType.Info,
                        HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : string.Empty,
                        LoanRequesID);
                    statusCategory = StatusMessages.SUCCESS;
                    return "Your listing is cancelled successfully";


                }
                else
                {
                    statusCategory = StatusMessages.DUPLICATE;
                    return "Your listing is already " + LoanRequest.LoanStatus.ToLoanStatusDisplayName();
                }

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                statusCategory = StatusMessages.FAILED;
                return "Something wrong here. Contact Admin";
            }

        }


        /// <summary>
        /// make transaction
        /// </summary>
        /// <param name="AmountBefore"></param>
        /// <param name="AmountToCredited"></param>
        /// <param name="LoanRequestID"></param>
        /// <param name="PaymentMode"></param>
        /// <param name="PaymentTerms"></param>
        /// <param name="UserID"></param>
        /// <param name="BalanceAfter"></param>
        public void LoanTransactionCredit(decimal? AmountBefore, decimal? AmountToCredited, long? LoanRequestID, string PaymentMode,
            string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
            objTransaction.Amount = Convert.ToDecimal(AmountBefore);///its the the amount user have in tbl_blances  before the transaction has occured
            objTransaction.Credited = Convert.ToDecimal(AmountToCredited);//amount of offer that should be credit in user balace
            objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime
            objTransaction.Request_Id = LoanRequestID;//loan request from which offer has withdrawn
            objTransaction.PaymentMode = PaymentMode;
            objTransaction.PaymentTerms = PaymentTerms;
            objTransaction.User_ID = UserID;
            objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
            objTransaction.Balance = Convert.ToDecimal(BalanceAfter);//balance of user after crediting the amount in his account

            db.tbl_LoanTransactions.Add(objTransaction);
            db.SaveChanges();

        }

        /// <summary>
        /// Withdrawing from MoolahSense
        /// </summary>
        /// <param name="AmountBefore"></param>
        /// <param name="AmountToCredited"></param>    
        /// <param name="PaymentMode"></param>
        /// <param name="PaymentTerms"></param>
        /// <param name="UserID"></param>
        /// <param name="BalanceAfter"></param>
        public void LoanTransactionDebit(decimal? AmountBefore, decimal? AmountTobeDebited, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
            objTransaction.Amount = AmountBefore;///its the the amount user have in tbl_blances  before the transaction has occured
            objTransaction.Debted = AmountTobeDebited;//amount of offer that should be debited in user balace
            objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime        
            objTransaction.PaymentMode = PaymentMode;
            objTransaction.PaymentTerms = PaymentTerms;
            objTransaction.User_ID = UserID;
            objTransaction.Balance = BalanceAfter;//balance of user after debting the amount from his account

            db.tbl_LoanTransactions.Add(objTransaction);
            db.SaveChanges();
        }

        public string ChangeLoanRateByRequestID(long RequestID, string Password, decimal? NewRate)
        {
            try
            {
                decimal? oldRate = 0;
                if (Membership.ValidateUser(HttpContext.Current.User.Identity.Name, Password))
                {
                    tbl_LoanRequests obj = new tbl_LoanRequests();
                    obj = db.tbl_LoanRequests.FirstOrDefault(l => l.RequestId == RequestID);
                    oldRate = obj.Rate;
                    obj.Rate = NewRate;
                    db.SaveChanges();

                    //_ILoanDetailsTask.NotifyApprovedInvestorsAndBorrowerOnTargetRateChanged(RequestID, (double)oldRate.Value, (double)NewRate.Value);

                    MoolahLogManager.Log(string.Format(AuditMessages.RATE_CHANGE,
                         oldRate.Value.ToString("0.##"),
                        NewRate.Value.ToString("0.##")), LogType.Info,
                        HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : string.Empty,
                        RequestID);

                    MoolahLogManager.Log(string.Format(AuditMessages.RATE_CHANGE,
                         oldRate.Value.ToString("0.##"),
                        NewRate.Value.ToString("0.##")),
                        LogType.Warn,
                        HttpContext.Current.User != null ? HttpContext.Current.User.Identity.Name : string.Empty);

                    return ContollerExtensions.ReturnSuccess();
                }
                else
                    return ContollerExtensions.ReturnFailure();


            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Something wrong here. Contact Admin";
            }
        }
        /// <summary>
        /// Function used for getting values of indicators and return correspondance image name
        /// </summary>
        /// <returns></returns>
        public string TestUserVerification(string Status)
        {
            switch (Status)
            {
                case "Verified":
                    return "tick.png";

                case "Unverified":
                    return "cross.png";

                default:
                    return "error.png";

            }
        }

        public bool CheckVerificationStatus(string ColumnValue)
        {
            switch (ColumnValue)
            {
                case "Verified":
                    return true;
                case "Unverified":
                    return false;
                default:
                    return true;
            }
        }
        //public string TestDocumentVerification(string status)
        //{
        //    switch (status)
        //    {
        //        case "True":
        //            return "tick.png";

        //        default:
        //            return "cross.png";

        //    }
        //}

        /// <summary>
        /// Convert Month Number into month abbrivative name for graph
        /// </summary>
        /// <param name="MonthValue"></param>
        /// <returns></returns>
        public string ConvertNumberintoMonth(int MonthValue)
        {
            switch (MonthValue)
            {
                case 1:
                    return "Jan";
                case 2:
                    return "Feb";
                case 3:
                    return "Mar";
                case 4:
                    return "Apr";
                case 5:
                    return "May";
                case 6:
                    return "Jun";
                case 7:
                    return "Jul";
                case 8:
                    return "Aug";
                case 9:
                    return "Sep";
                case 10:
                    return "Oct";
                case 11:
                    return "Nov";
                default:
                    return "Dec";


            }
        }

        public TermsheetViewModel GetTermsheetDetails(long loanId)
        {
            var req = db.tbl_LoanRequests.SingleOrDefault(l => l.RequestId == loanId);

            var obj = new TermsheetViewModel
            {
                LoanId = req.RequestId,
                LoanAgreementDt = DateTime.Now,
                PrincipalAmount = (decimal)req.Amount,
                Tenure = req.Terms,
            };

            return obj;
        }

    }
}
