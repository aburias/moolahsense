﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using System.Web.Security;
//using MoolahConnectnew.Filters;
using System.Web.Mvc;
using System.Text;
using System.Data.SqlClient;
using NLog;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Globalization;
using MoolahConnectClassLibrary.Models;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.ExtensionMethods;
using MoolahConnect.Util.Logging;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.GenerateReference;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Util;
using MoolahConnect.Util.Email;

namespace MoolahConnectClassLibrary.DAL
{

    public class InvestorOperations
    {
        private dbMoolahConnectEntities db = new dbMoolahConnectEntities();
        IInvestorLoanSummaryDA _IInvestorLoanSummaryDA;
        ILoanAllocationTask _ILoanAllocationTask;
        ILoanPaymentsTask _ILoanPaymentTask;
        IUser _IUser;
        ITransactionsDA _ITransactionDA;
        IEmailSender _IEmailSender;

        public InvestorOperations()
        {
            _IInvestorLoanSummaryDA = new InvestorLoanSummaryDA();
            _ILoanAllocationTask = new LoanAllocationTask();
            _ILoanPaymentTask = new LoanPaymentsTask();
            _IUser = new User();
            _ITransactionDA = new TransactionsDA();
            _IEmailSender = new EmailSender();
        }

        /// <summary>
        /// Method for Index load
        /// </summary>
        /// <returns></returns>
        /// 

        //public string IndexLoad()
        //{
        //    string Controller = string.Empty;
        //    try
        //    {


        //        var LoggedinUser = db.tbl_Users.SingleOrDefault(p => p.UserName.Equals(HttpContext.Current.User.Identity.Name, StringComparison.OrdinalIgnoreCase));

        //        if (LoggedinUser != null)
        //        {

        //            // tbl_AccountDetails objborrower = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == LoggedinUser.UserID);
        //            var objborrower = (from p in db.tbl_UserVerification where p.UserID == LoggedinUser.UserID select p).ToList();

        //            if (LoggedinUser.AdminVerification != "Approved" || LoggedinUser.AdminVerification == null)
        //            {
        //                HttpContext.Current.Session["step"] = 1;

        //                if (LoggedinUser.IsSubmitted != null && (bool)LoggedinUser.IsSubmitted)
        //                {
        //                    Controller = LoggedinUser.InvestorType == "Corporate" ? "SubmittedCorporate" : "SubmittedInvestor";
        //                }
        //                else
        //                {
        //                    HttpContext.Current.Session["step"] = 1;
        //                    Controller = LoggedinUser.InvestorType == "Corporate" ? "Corporate" : "Investor";
        //                }
        //            }
        //            else
        //            {
        //                Controller = "";
        //            }

        //            if (LoggedinUser.UserRole == "Borrower")
        //            {
        //                Controller = "User";
        //            }
        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        MoolahLogManager.LogException(ex);
        //        Controller = string.Empty;
        //    }

        //    return Controller;
        //}


        /// <summary>
        /// Method for generating pdf for registration form
        /// </summary>
        /// <returns></returns>
        //public InvestorAccount GetregistrationdetailsforPDF()
        //{
        //    InvestorAccount objinvestor;
        //    try
        //    {
        //        string id = (from p in db.tbl_Users
        //                     where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
        //                     select p.UserID).FirstOrDefault().ToString();


        //        InvestorAccountDetailsModel objaccount = new InvestorAccountDetailsModel();
        //        InvestorDocuments objdocuments = new InvestorDocuments();
        //        Knowledgeassessment onjknowledgeassessment = new Knowledgeassessment();



        //        tbl_AccountDetails objuser = new tbl_AccountDetails();

        //        //////////////////////////////////// If user is logged in then get details of user from Userid for displaying in account details ///////////////////

        //        if (id != "0" && id != null)
        //        {
        //            int Userid = Convert.ToInt32(id);

        //            objdocuments.Userid = Userid;
        //            objuser = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);

        //            if (objuser != null)
        //            {
        //                objaccount.Title = objuser.Title;
        //                objaccount.Nationality = objuser.Nationality;
        //                objaccount.Gender = objuser.Gender;
        //                objaccount.IncomeRange = objuser.IncomeRange;
                        
        //                objaccount.FullName = objuser.FirstName + " " + objuser.LastName;
        //                objaccount.ICNumber = objuser.IC_CompanyRegistrationNumber;
        //                objaccount.PRStatus = objuser.PRstatus;
        //                objaccount.DateofBirth = objuser.DateofBirth;

        //                var resAddress = CommonMethods.LoadAddressField(objuser.ResidentialAddress);
        //                objaccount.ResidentialAddress1 = resAddress[0];
        //                objaccount.ResidentialAddress2 = resAddress[1];
        //                objaccount.ResidentialPostalCode = resAddress[2];

        //                objaccount.MainContactNumber = objuser.Main_OfficeContactnumber;
        //                objaccount.Othercontactnumber = objuser.OptionalContactnumber;
        //                objaccount.Occupation = objuser.Occupation;
        //                objaccount.IncomeRange = objuser.IncomeRange;
        //                objaccount.BankAccountName = objuser.AccountName;
        //                objaccount.BankAccountNumber = objuser.AccountNumber;
        //                objaccount.BankId = Convert.ToInt32(objuser.Bank_Id);
        //                objaccount.BankName = db.tbl_BanksList.Where(p => p.BankId == objaccount.BankId).Select(p => p.BankName).FirstOrDefault();

        //                var userquest = db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == Userid).ToList();
        //                int q = 0;
        //                foreach (var ite in userquest)
        //                {
        //                    if (q == 0)
        //                    {
        //                        objaccount.SecurityQuestion1 = ite.Question_Id;
        //                        objaccount.SecurityAnswer1 = ite.Answer;
        //                    }
        //                    else
        //                    {
        //                        objaccount.SecurityQuestion2 = ite.Question_Id;
        //                        objaccount.SecurityAnswer2 = ite.Answer;
        //                    }

        //                    q++;
        //                }

        //            }
        //            else
        //            {

        //            }
        //        }
        //        else
        //        {

        //        }


        //        objinvestor = new InvestorAccount(objaccount, objdocuments, onjknowledgeassessment);
        //    }
        //    catch (Exception ex)
        //    {
        //        MoolahLogManager.LogException(ex);
        //        objinvestor = null;
        //    }

        //    return objinvestor;
        //}

        /// <summary>
        /// Method for fetching  investor info

        /// <returns></returns>
        //public InvestorAccount GetinvestorAccountInfo()
        //{

        //    var message = string.Empty;
        //    try
        //    {
        //        InvestorAccountDetailsModel objaccount = new InvestorAccountDetailsModel();
        //        //InvestorDocuments objdocuments = new InvestorDocuments();
        //        Knowledgeassessment objknowledgeassessment = new Knowledgeassessment();
        //        string id = (from p in db.tbl_Users
        //                     where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
        //                     select p.UserID).FirstOrDefault().ToString();

        //        //////////////////////////////////// Get list of Security Questions , Nature of buisness and banks list for binding with drop downs ///////////////////////// 

        //        objaccount.SecurityQuestionList = db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();
        //        objaccount.BanksList = db.tbl_BanksList.Where(p => p.Isactive == true).ToList();

        //        objknowledgeassessment.Parentlist =
        //            db.tbl_Knowledgeassessment_Parentlist.Where(k => k.InvType == "B" || k.InvType == "I")
        //              .OrderBy(p => p.Priority)
        //              .ToList();

        //        tbl_AccountDetails objuser = new tbl_AccountDetails();


        //        //////////////////////////////////// If user is logged in then get details of user from Userid for displaying in account details ///////////////////

        //        if (id != "0" && id != null)
        //        {
        //            int Userid = Convert.ToInt32(id);
        //            //objdocuments.Userid = Userid;
        //            objuser = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);

        //            var commonUser = new UserCommonOperations();

        //            if (objuser != null)
        //            {

        //                objaccount.UserVerificationStatus = "unverified";
        //                objaccount._Titles = new SelectList(TitleItems(), "Value", "Text", objuser.Title);

        //                objaccount.Title = objuser.Title;
        //                objaccount._Gender = new SelectList(GenderItems(), "Value", "Text", objuser.Gender);
        //                objaccount._Nationality = new SelectList(NationalityItems(), "Value", "Text", objuser.Nationality);
        //                objaccount._IncomeRange = new SelectList(IncomerangeItems(), "Value", "Text", objuser.IncomeRange);
        //                objaccount.IncomeRange = objuser.IncomeRange;

        //                objaccount.FullName = objuser.FirstName + "" + objuser.LastName;
        //                objaccount.ICNumber = objuser.NRIC_Number;
        //                objaccount.PassportNumber = objuser.PassportNumber;
        //                objaccount.PRStatus = objuser.PRstatus;
        //                //chnage format 01/02/2013
        //                objaccount.DateofBirth = objuser.DateofBirth;
        //                objaccount.DisplayName = objuser.DisplayName;
        //                objaccount.Nationality = objuser.Nationality;

        //                var resAddress = CommonMethods.LoadAddressField(objuser.ResidentialAddress);
        //                objaccount.ResidentialAddress1 = resAddress[0];
        //                objaccount.ResidentialAddress2 = resAddress[1];
        //                objaccount.ResidentialPostalCode = resAddress[2];

        //                objaccount.MainContactNumber = objuser.Main_OfficeContactnumber;
        //                objaccount.Othercontactnumber = objuser.OptionalContactnumber;
        //                objaccount.Emailaddress = commonUser.GetCurrentUserEmail();
        //                objaccount.Occupation = objuser.Occupation;
        //                objaccount.BankAccountName = objuser.AccountName;
        //                objaccount.BankAccountNumber = objuser.AccountNumber;
        //                objaccount.BankId = Convert.ToInt32(objuser.Bank_Id);
        //                objaccount.BankAccountNumber = objuser.AccountNumber;
        //                objaccount.BankAccountName = objuser.AccountName;
        //                objaccount.BranchName = objuser.BranchName;
        //                objaccount.BranchNumber = objuser.BranchNumber;
        //                objaccount.BankNumber = objuser.BankNumber;
        //                objaccount.SecurityQuestion1 = Convert.ToInt32(db.tbl_SecurityQuestionsForUsers.Where(p => p.User_ID == Userid).Select(p => p.Question_Id).FirstOrDefault());
        //                objaccount.SecurityQuestion2 = Convert.ToInt32(db.tbl_SecurityQuestionsForUsers.Where(p => p.Question_Id != objaccount.SecurityQuestion1 && p.User_ID == Userid).Select(p => p.Question_Id).FirstOrDefault());
        //                objaccount.SecurityAnswer1 = Convert.ToString((from p in db.tbl_SecurityQuestionsForUsers
        //                                                               where p.Question_Id == objaccount.SecurityQuestion1 && p.User_ID == Userid
        //                                                               select p.Answer).FirstOrDefault());

        //                objaccount.SecurityAnswer2 = Convert.ToString((from p in db.tbl_SecurityQuestionsForUsers
        //                                                               where p.Question_Id == objaccount.SecurityQuestion2 && p.User_ID == Userid
        //                                                               select p.Answer).FirstOrDefault());
        //            }
        //            else
        //            {
        //                objaccount._Titles = new SelectList(TitleItems(), "Value", "Text");
        //                objaccount._Gender = new SelectList(GenderItems(), "Value", "Text", "Male");
        //                objaccount._Nationality = new SelectList(NationalityItems(), "Value", "Text", "Singapore");
        //                objaccount._IncomeRange = new SelectList(IncomerangeItems(), "Value", "Text", "50,000-1,00,000");
        //                objaccount.Emailaddress = commonUser.GetCurrentUserEmail();
        //            }
        //        }
        //        else
        //        {
        //            objaccount._Titles = new SelectList(TitleItems(), "Value", "Text");
        //            objaccount._Gender = new SelectList(GenderItems(), "Value", "Text", "Male");
        //            objaccount._Nationality = new SelectList(NationalityItems(), "Value", "Text", "Singapore");
        //            objaccount._IncomeRange = new SelectList(IncomerangeItems(), "Value", "Text", "50,000-1,00,000");
        //        }

        //        ///////////////////////////// for setting steps value for enabling the tabs depending upon the current step //////////////////////////////////////////

        //        InvestorAccount objinvestor = new InvestorAccount(objaccount, objknowledgeassessment);
        //        if (HttpContext.Current.Session["step"] == null || HttpContext.Current.Session["step"].ToString() == "")
        //        {
        //            HttpContext.Current.Session["step"] = "1";
        //        }
        //        return objinvestor;

        //    }
        //    catch (Exception ex)
        //    {
        //        MoolahLogManager.LogException(ex);
        //        return null;

        //    }


        //}

        /// <summary>
        /// Method for creating investor Account
        ///<param name="InvestorAccountDetailsModel"
        /// <returns></returns>
        public string CreateInvestorAccount(InvestorAccountDetailsModel objinvestor)
        {
            try
            {
                Knowledgeassessment objknowledgeassessment = new Knowledgeassessment();

                InvestorAccount objmain = new InvestorAccount(objinvestor, objknowledgeassessment);

                Int64 Userid = (from p in db.tbl_Users
                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                select p.UserID).FirstOrDefault();

                tbl_AccountDetails objaccounts = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);
                if (objaccounts != null)
                {

                    ///////////////////////////////// for updating record in tbl_accountdetails table //////////////////////////////////
                    objaccounts.Title = objinvestor.Title;
                    objaccounts.FirstName = objinvestor.FullName;
                    objaccounts.DisplayName = objinvestor.DisplayName;
                    objaccounts.NRIC_Number = objinvestor.ICNumber;
                    objaccounts.PassportNumber = objinvestor.PassportNumber;
                    objaccounts.Nationality = objinvestor.Nationality;
                    objaccounts.PRstatus = objinvestor.PRStatus;
                    objaccounts.DateofBirth = objinvestor.DateofBirth;
                    objaccounts.Gender = objinvestor.Gender;

                    var resAddress = ContollerExtensions.AppendAddressField(objinvestor.ResidentialAddress1,
                        objinvestor.ResidentialAddress2, objinvestor.ResidentialPostalCode);
                    objaccounts.ResidentialAddress = resAddress;

                    objaccounts.Main_OfficeContactnumber = objinvestor.MainContactNumber;
                    objaccounts.OptionalContactnumber = objinvestor.Othercontactnumber;
                    objaccounts.Bank_Id = objinvestor.BankId;
                    objaccounts.BankNumber = objinvestor.BankNumber;
                    objaccounts.BranchName = objinvestor.BranchName;
                    objaccounts.BranchNumber = objinvestor.BranchNumber;
                    objaccounts.AccountNumber = objinvestor.BankAccountNumber;
                    objaccounts.AccountName = objinvestor.BankAccountName;
                    objaccounts.IncomeRange = objinvestor.IncomeRange;
                    objaccounts.Occupation = objinvestor.Occupation;
                    db.SaveChanges();

                }
                else
                {
                    ///////////////////////////////// for inserting new record in tbl_accountdetails table //////////////////////////////////
                    tbl_AccountDetails objaccountsnewuser = new tbl_AccountDetails();
                    objaccountsnewuser.Title = objinvestor.Title;
                    objaccountsnewuser.FirstName = objinvestor.FullName;
                    objaccountsnewuser.DisplayName = objinvestor.DisplayName;
                    objaccountsnewuser.NRIC_Number = objinvestor.ICNumber;
                    objaccountsnewuser.PassportNumber = objinvestor.PassportNumber;
                    objaccountsnewuser.Nationality = objinvestor.Nationality;
                    objaccountsnewuser.PRstatus = objinvestor.PRStatus;
                    objaccountsnewuser.DateofBirth = objinvestor.DateofBirth;
                    objaccountsnewuser.Gender = objinvestor.Gender;

                    var resAddress = ContollerExtensions.AppendAddressField(objinvestor.ResidentialAddress1,
                        objinvestor.ResidentialAddress2, objinvestor.ResidentialPostalCode);
                    objaccountsnewuser.ResidentialAddress = resAddress;

                    objaccountsnewuser.Main_OfficeContactnumber = objinvestor.MainContactNumber;
                    objaccountsnewuser.OptionalContactnumber = objinvestor.Othercontactnumber;
                    objaccountsnewuser.Bank_Id = objinvestor.BankId;
                    objaccountsnewuser.BankNumber = objinvestor.BankNumber;
                    objaccountsnewuser.BranchName = objinvestor.BranchName;
                    objaccountsnewuser.BranchNumber = objinvestor.BranchNumber;
                    objaccountsnewuser.AccountNumber = objinvestor.BankAccountNumber;
                    objaccountsnewuser.AccountName = objinvestor.BankAccountName;
                    objaccountsnewuser.User_ID = Userid;
                    objaccountsnewuser.Occupation = objinvestor.Occupation;
                    objaccountsnewuser.IncomeRange = objinvestor.IncomeRange;
                    objaccountsnewuser.DisplayName = objinvestor.DisplayName;
                    objaccountsnewuser.AccountType = "Investor";

                    db.tbl_AccountDetails.Add(objaccountsnewuser);
                    db.SaveChanges();
                }

                HttpContext.Current.Session["step"] = "2";
                return ContollerExtensions.ReturnSuccess();

            }
            catch (Exception ex)
            {
                LogDetailsOnError(objinvestor);
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }

        private void LogDetailsOnError(InvestorAccountDetailsModel investor)
        {
            try
            {
                var message = new StringBuilder();

                message.Append("Title : " + investor.Title);
                message.Append(", FirstName : " + investor.FullName);
                message.Append(", DisplayName : " + investor.DisplayName);
                message.Append(", NRIC_Number : " + investor.ICNumber);
                message.Append(", PassportNumber : " + investor.PassportNumber);
                message.Append(", Nationality : " + investor.Nationality);
                message.Append(", PRstatus : " + investor.PRStatus);
                message.Append(", DateofBirth : " + (investor.DateofBirth.HasValue ? investor.DateofBirth.Value.ToShortDateString() : string.Empty));
                message.Append(", Gender : " + investor.Gender);
                message.Append(", Res Address : " + ContollerExtensions.AppendAddressField(investor.ResidentialAddress1,
                            investor.ResidentialAddress2, investor.ResidentialPostalCode));
                message.Append(", MainContactNumber : " + investor.MainContactNumber);
                message.Append(", Othercontactnumber : " + investor.Othercontactnumber);
                message.Append(", BankId : " + investor.BankId);
                message.Append(", BankNumber : " + investor.BankNumber);
                message.Append(", BranchName : " + investor.BranchName);
                message.Append(", BranchNumber : " + investor.BranchNumber);
                message.Append(", BankAccountNumber : " + investor.BankAccountNumber);
                message.Append(", BankAccountName : " + investor.BankAccountName);
                message.Append(", Occupation : " + investor.Occupation);
                message.Append(", IncomeRange : " + investor.IncomeRange);
                message.Append(", DisplayName : " + investor.DisplayName);
                message.Append(", AccountType : " + "Investor");

                MoolahLogManager.LogExceptionEvent(message.ToString(), HttpContext.Current.User.Identity.Name);
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }

        /// <summary>
        /// Method for getting Knowledgeassessmentoptions by parent id
        /// </summary>
        ///<param name="parentid"></param>
        /// <returns></returns>
        public string GetKnowledgeassessmentoptions(int parentid)
        {
            var userid = (from p in db.tbl_Users
                          where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                          select p.UserID).FirstOrDefault();
            return GetKnowledgeassessmentoptions(parentid, userid);
        }

        /// <summary>
        /// Method for getting Knowledgeassessmentoptions by parent id and userid with selected marks
        /// </summary>
        ///<param name="parentid"/>
        ///<param name="userid"/>
        /// <returns>Html string</returns>
        public string GetKnowledgeassessmentoptions(int parentid, long userid)
        {
            try
            {
                var strselectstrt = string.Empty;
                var strselectend = string.Empty;
                var strinnercontent = new StringBuilder();

                var options = from p in db.tbl_Knowledgeassessment_OptionsList
                              join q in db.tbl_Knowledgeassessment_Parentlist
                                  on p.Parent_Id equals q.ParentId
                                  into j1
                              from j2 in j1.DefaultIfEmpty()
                              where p.Parent_Id == parentid
                              select new
                              {
                                  p.Optiontext,
                                  p.Optionid,
                                  p.Optiontype,
                                  p.url,
                                  p.tbl_Knowledgeassessment_Parentlist.Priority,
                                  p.tbl_Knowledgeassessment_Parentlist.InvType,
                                  p.Parent_Id
                              };
                var fullhtml = "<div style='width:100%;font-size:13px;margin:10px;float:left;'>";

                var priority = db.tbl_Knowledgeassessment_Parentlist.Where(k => k.ParentId == parentid).SingleOrDefault().Priority;

                if (priority == 7 || priority == 6)
                {
                    strinnercontent.Append(
                        "<input type='checkbox'  onclick='CheckAllOptions(\"" + priority + "\",this.checked)' style='float:left; margin-right:5px;'" +
                                "/><div style='float:left;'>Select All</div><div class='clear' ></div>");
                }

                if (priority == 3)
                {
                    strinnercontent.Append("<option Value='0' Selected='true'>Please select one</option>");
                }

                foreach (var m in options)
                {
                    var val = db.tbl_Knowledgeassessmentdetails_ForUser.SingleOrDefault(
                        a => a.Option_Id == m.Optionid && a.User_ID == userid);

                    if (m.Optiontype == "checkbox")
                    {
                        var option = val != null ? " checked='" + val.Option_Value + "' " : "";

                        if (m.Optiontext.Trim() == "None at all")
                        {
                            strinnercontent.Append(
                                "<input type='checkbox' class='KA NA'  onclick='disalbleotheroption()' value='" +
                                m.Optionid + "' style='float:left; margin-right:5px;'" + option +
                                "/><div style='float:left;'> " + m.Optiontext +
                                "  </div><div class='clear' ></div>");
                        }
                        else
                        {
                            if (m.Priority == 7)
                            {
                                strinnercontent.Append("<input type='checkbox' class='KA o 7'  value='" + m.Optionid +
                                                       "' style='float:left; margin-right:5px;'" + option +
                                                       "/><div style='float:left;'><a target=\"_blank\" href=\"" + m.url + "\" >" +
                                                       m.Optiontext + "</a></div><div class='clear' ></div>");
                            }
                            else if ((m.InvType == "I" && m.Priority == 5) || (m.InvType == "C" && m.Priority == 1))
                            {
                                strinnercontent.Append("<input type='checkbox' class='KA d'  value='" + m.Optionid +
                                                       "' style='float:left; margin-right:5px;'" + option +
                                                       "/><div style='float:left;'> " +
                                                       m.Optiontext + "  </div><div class='clear' ></div>");
                            }
                            else
                            {
                                strinnercontent.Append("<input type='checkbox' class='KA o " + priority + "'  value='" + m.Optionid +
                                                       "' style='float:left; margin-right:5px;'" + option +
                                                       "/><div style='float:left;'> " +
                                                       m.Optiontext + "  </div><div class='clear' ></div>");
                            }
                        }

                    }
                    else if (m.Optiontype == "radio")
                    {
                        if (m.Priority == 2)
                        {
                            string option = string.Empty;

                            if (m.Optionid == 32)
                            {
                                option = " checked='checked' ";
                            }
                            else
                            {
                                option = val != null ? " checked='" + val.Option_Value + "' " : "";
                            }

                            strinnercontent.Append("<input type='radio' class='emp' onChange='EmploymentSelectionChanged();' value='" + m.Optionid + "'  style='float:left; margin-right:5px;'" + option + "name='childof_" + parentid + "'  /><div style='float:left;'> " + m.Optiontext + "  </div><div class='clear' ></div>");
                        }
                        else
                        {
                            var option = val != null ? " checked='" + val.Option_Value + "' " : "";

                            strinnercontent.Append("<input type='radio' class='KA' value='" + m.Optionid + "'  style='float:left; margin-right:5px;'" + option + "name='childof_" + parentid + "'  /><div style='float:left;'> " + m.Optiontext + "  </div><div class='clear' ></div>");
                        }
                    }
                    else if (m.Optiontype == "option")
                    {
                        if (m.Parent_Id == 11)
                        {
                            string option = val != null ? " selected='" + val.Option_Value + "' " : "";

                            strselectstrt = "<select id='select" + m.Parent_Id + "' class='wish_list_in emp' onChange=OccupationSelectionChanged(); >";
                            strselectend = "</select>";
                            strinnercontent.Append("<option Value='" + m.Optionid + "'" + option + ">" + m.Optiontext +
                                                   "</option>");
                        }
                        else
                        {
                            var option = val != null ? " selected='" + val.Option_Value + "' " : "";
                            strselectstrt = "<select class='wish_list_in KA' >";
                            strselectend = "</select>";
                            strinnercontent.Append("<option Value='" + m.Optionid + "'" + option + ">" + m.Optiontext +
                                                   "</option>");
                        }
                    }
                    else if (m.Optiontype == "text")
                    {
                        string value = val != null ? val.Option_Value : string.Empty;

                        strinnercontent.Append(
                            "<input id='txt" + m.Optionid + "' type='text' style='float:left; margin-right:5px;'name='childof_" +
                            parentid + "' value='" + value + "'  /></div><div class='clear' >");
                    }
                }

                fullhtml = fullhtml + strselectstrt + strinnercontent + strselectend + "</div>";

                return fullhtml;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }

        public string Saveknowledgeassesmentotions(String XMLdocs)
        {
            Int64 Userid = (from p in db.tbl_Users
                            where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                            select p.UserID).FirstOrDefault();

            return Saveknowledgeassesmentotions(XMLdocs, Userid);
        }

        public string Saveknowledgeassesmentotions(String XMLdocs, Int64 Userid)
        {
            try
            {

                String Message = string.Empty;
                InvestorAccountDetailsModel objaccount = new InvestorAccountDetailsModel();
                InvestorDocuments objdocuments = new InvestorDocuments();
                Knowledgeassessment objknowledgeassessment = new Knowledgeassessment();

                InvestorAccount objinvestor = new InvestorAccount(objaccount, objdocuments, objknowledgeassessment);


                // TODO: Add insert logic here

                if (Userid != 0)
                {
                    SqlParameter[] objPram = new SqlParameter[2];
                    objPram[0] = new SqlParameter("@XmlText", XMLdocs);
                    objPram[1] = new SqlParameter("@userid", Userid);

                    db.Database.ExecuteSqlCommand("USP_tbl_Knowledgeassessment_ForUser_Savedetails @XmlText ,@Userid", objPram);

                    _IUser.SaveDigitalSignature(Userid);

                    HttpContext.Current.Session["step"] = "3";
                    Message = ContollerExtensions.ReturnSuccess();
                }
                else
                {
                    Message = "User not found";
                }
                return Message;

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }

        }

        /// <summary>
        /// Method for generating list for dropdown list(select list) for titles, for user 
        /// </summary>
        /// <returns></returns>
        /// 
        private List<SelectListItem> TitleItems()
        {

            var Titles = new List<SelectListItem>()    
                           {
                               new SelectListItem(){ Text = "Mr.", Value= "Mr."},
                               new SelectListItem(){ Text = "Miss", Value= "Miss"},
                               new SelectListItem(){ Text = "Mrs.", Value= "Mrs."},
                                new SelectListItem(){ Text = "Dr.", Value= "Dr."}
                            };


            return Titles;
        }


        /// <summary>
        /// Method for generating list for dropdown list(select list) for Gender
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> GenderItems()
        {
            var Gender = new List<SelectListItem>()
                           {
                               new SelectListItem(){ Text = "Male", Value= "Male", Selected = false},
                               new SelectListItem(){ Text = "Female", Value= "Female", Selected = false}                              
                            };
            return Gender;
        }

        /// <summary>
        /// Method for generating list for dropdown list(select list) for Nationality
        /// </summary>
        /// <returns></returns>
        private IEnumerable<SelectListItem> NationalityItems()
        {
            var nationality = new List<SelectListItem>
                {
                    new SelectListItem {Text = "Singapore Citizen", Value = "Singapore Citizen", Selected = true},
                    new SelectListItem {Text = "Singapore PR", Value = "Singapore PR"},
                    new SelectListItem {Text = "Other Nationalities", Value = "Other Nationalities"}
                };
            return nationality;
        }

        /// <summary>
        /// Method for generating list for dropdown list(select list) for Incomerange
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> IncomerangeItems()
        {
            var IncomeRange = new List<SelectListItem>()
                           {
                               new SelectListItem(){ Text = "Under  S$30,000\u0012\u000E", Value= "Under S$30,000\u0012\u000E", Selected = false},
                               new SelectListItem(){ Text = "S$30,000 to S$60,000", Value= "S$30,000 to S$60,000", Selected = false}  , 
                               new SelectListItem(){ Text = "S$60,001 to S$100,000", Value= "S$60,001 to S$100,000", Selected = false}   ,

                               new SelectListItem(){ Text = "S$100,001 to S$160,000", Value= "S$100,001 to S$160,000", Selected = false}   ,
                               new SelectListItem(){ Text = "S$160,001 to S$240,000", Value= "S$160,001 to S$240,000", Selected = false}   ,
                                new SelectListItem(){ Text = "S$240,001 and Above", Value= "S$240,001 and Above", Selected = false}   
                            };
            return IncomeRange;
        }
        //////////////////////////////////////////////////////Inverstor Loan Offers//////////////////////////////////
        #region InvestorLoanOffers
        //////////////////////////Get Loan and Borrower Detail by Loan request ID
        public InvestorLoanOffer GetLoanandBorrowerBriefDetailRequestID(long LoanRequestID)
        {
            InvestorLoanOffer objloanoffer = new InvestorLoanOffer();
            try
            {
                tbl_Users user = (from p in db.tbl_Users
                                  where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                  select p).FirstOrDefault();
                if (user == null)
                    return new InvestorLoanOffer();

                Int64 Userid = user.UserID;

                if (user.UserRole == "Investor")
                    objloanoffer.IsInvestor = true;

                var LoanRequest = (from l in db.tbl_LoanRequests
                                   join u in db.tbl_Users
                                    on l.User_ID equals u.UserID
                                   join lp in db.tbl_LoanPurposesList
                                   on l.LoanPurpose_Id equals lp.LoanPurposeId
                                   join a in db.tbl_AccountDetails
                                   on l.User_ID equals a.User_ID
                                   join mc in db.tbl_MoolahCore
                                     on l.RequestId equals mc.LoanRequest_Id
                                   where l.RequestId == LoanRequestID && l.IsApproved.HasValue && l.IsApproved.Value
                                   select new
                                   {
                                       l.Amount,
                                       l.Rate,////////////////rate of interest
                                       l.IsPersonalGuarantee,
                                       l.Terms,/////////////time in months 12,24,36
                                       l.DateCreated,
                                       l.User_ID,
                                       lp.PurposeName,/////////loan purpose
                                       l.IsApproved,
                                       l.ApproveDate,
                                       l.LoanStatus,
                                       l.PublishedDate,
                                       l.PreliminaryLoanStatus,
                                       l.TransferDate,

                                       ////////////////////////////////moolah core section////////////////////
                                       mc.NetprofitafterTax_LY,
                                       mc.CashFlowfromOperations_LY,
                                       mc.Turnovers_LY,
                                       mc.Debt_Equity_LY,
                                       mc.CurrentRatio_LY,

                                       ///////////////////////////Compay and user profile
                                       a.DisplayName,
                                       a.Mobilenumber,
                                       a.AccountNumber,
                                       a.Gender,
                                       a.Nationality,
                                       a.DateofIncorporation,
                                       a.PRstatus,
                                       a.ResidentialAddress,
                                       a.BusinessName,
                                       a.IC_CompanyRegistrationNumber,
                                       a.BusinessOrganisation,
                                       a.tbl_NatureofBusinessList.NatureofBusinessName,

                                       u.UserName

                                   }).SingleOrDefault();
                if (LoanRequest != null)
                {
                    AccountDetail objAccountDetail = new AccountDetail();
                    objAccountDetail.DisplayName = LoanRequest.DisplayName == null ? "NA" : LoanRequest.DisplayName;
                    objAccountDetail.Nationality = LoanRequest.Nationality;
                    objAccountDetail.ResidentialAddress = LoanRequest.ResidentialAddress;
                    objAccountDetail.MobileNumber = LoanRequest.Mobilenumber == null ? "Not Specified" : LoanRequest.Mobilenumber;
                    objAccountDetail.AccountNumber = LoanRequest.AccountNumber;
                    objAccountDetail.BussinessName = LoanRequest.BusinessName;
                    objAccountDetail.EmailAddress = LoanRequest.UserName;

                    objAccountDetail.BussinessRegistrationNUmber = LoanRequest.IC_CompanyRegistrationNumber;
                    objAccountDetail.BussinessOrganisation = LoanRequest.BusinessOrganisation;
                    objAccountDetail.DateofIncorporation = Convert.ToDateTime(LoanRequest.DateofIncorporation).ToString("dd MMM yyyy");

                    var GetGloabalVariables = db.tbl_GlobalVariables.SingleOrDefault();
                    MoolahCore objMoolahCore = new MoolahCore();

                    var MoolahCoreIndicator = this.db.tbl_MoolahCoreVerification.FirstOrDefault(l => l.Request_ID == LoanRequestID);
                    if (MoolahCoreIndicator != null)
                    {
                        if (MoolahCoreIndicator.Turnover_LY_Verification == (int)VerificationStatus.Verified && MoolahCoreIndicator.Turnover_PY_Verification == (int)VerificationStatus.Verified)
                        {
                            objMoolahCore.Turnovers_verification = "YES";

                        }
                        else
                        {
                            objMoolahCore.Turnovers_verification = "NO";

                        }
                        if (MoolahCoreIndicator.Debt_Equity_LY_Verification == (int)VerificationStatus.Verified && MoolahCoreIndicator.Debt_Equity_PY_Verification == (int)VerificationStatus.Verified)
                        {
                            objMoolahCore.Debt_Equity_verification = "YES";

                        }
                        else
                        {
                            objMoolahCore.Debt_Equity_verification = "NO";
                        }
                        if (MoolahCoreIndicator.CurrentRatio_LY_Verification == (int)VerificationStatus.Verified && MoolahCoreIndicator.CurrentRatio_PY_Verification == (int)VerificationStatus.Verified)
                        {
                            objMoolahCore.CurrentRatio_verification = "YES";

                        }
                        else
                        {
                            objMoolahCore.CurrentRatio_verification = "NO";

                        }
                        if (MoolahCoreIndicator.CashFlowFromOperations_LY_Verification == (int)VerificationStatus.Verified && MoolahCoreIndicator.CashFlowFromOperations_PY_Verification == (int)VerificationStatus.Verified)
                        {
                            objMoolahCore.CashFlowfromOperations_verification = "Yes";

                        }
                        else
                        {
                            objMoolahCore.CashFlowfromOperations_verification = "No";

                        }

                    }


                    InverstorLoanMoolahPeri objMoolahPeri = new InverstorLoanMoolahPeri();
                    var MoolahPeriID = db.tbl_MoolahPeri.FirstOrDefault(e => e.Request_ID == LoanRequestID);

                    var UserVerisicationStatus = (from p in db.tbl_MoolahPeriVerification where p.Request_ID == LoanRequestID select p).FirstOrDefault();
                    if (UserVerisicationStatus != null)
                    {
                        if (UserVerisicationStatus.BussinessAwardsVerification == (int)VerificationStatus.Verified)
                        {
                            objMoolahPeri.BusinessQuardsVerification = "YES";
                        }
                        else
                        {
                            objMoolahPeri.BusinessQuardsVerification = "NO";
                        }
                        if (UserVerisicationStatus.AcrediationVerification == (int)VerificationStatus.Verified)
                        {
                            objMoolahPeri.AcreditationVerification = "YES";
                        }
                        else
                        {
                            objMoolahPeri.AcreditationVerification = "NO";
                        }
                        if (UserVerisicationStatus.MTAVerificationVerification == (int)VerificationStatus.Verified)
                        {
                            objMoolahPeri.MTAVerification = "YES";
                        }
                        else
                        {
                            objMoolahPeri.MTAVerification = "NO";
                        }
                        if (UserVerisicationStatus.CommunityVerification == (int)VerificationStatus.Verified)
                        {
                            objMoolahPeri.CommunityInitiativeVerification = "YES";
                        }
                        else
                        {
                            objMoolahPeri.CommunityInitiativeVerification = "NO";
                        }
                        if (UserVerisicationStatus.DigitalFootPrintVerification == (int)VerificationStatus.Verified)
                        {
                            objMoolahPeri.DigitalFootPrintVerification = "YES";
                        }
                        else
                        {
                            objMoolahPeri.DigitalFootPrintVerification = "NO";
                        }
                        if (UserVerisicationStatus.BiodataofDSVerification == (int)VerificationStatus.Verified)
                        {
                            objMoolahPeri.BiodataOfDSVerification = "YES";
                        }
                        else
                        {
                            objMoolahPeri.BiodataOfDSVerification = "NO";
                        }

                    }





                    ////////////////////////end///////////////////////////////////
                    LoanRequestInverstor objInvestor = new LoanRequestInverstor();
                    objInvestor.Amount = LoanRequest.Amount;
                    objInvestor.Term = LoanRequest.Terms;
                    objInvestor.EIR = Convert.ToDecimal(LoanRequest.Rate);
                    objInvestor.TransferedDate = LoanRequest.TransferDate;

                    var time = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(LoanRequest.PublishedDate),
                        Settings.LoanRequestExpiryDays);

                    if (time.Length >= 3)
                    {
                        objInvestor.HasExpiryDatePassed = time[2] <= 0;
                        objInvestor.ExpiredMessage = "Pending Admin processing of expiry";
                    }

                    /////////////emi calculate/////////////////
                    var term = objInvestor.Term;
                    decimal EMI = 0;
                    var intr = Convert.ToDecimal(objInvestor.EIR) / 1200;
                    var one = 1 / (1 + intr);
                    var calc = Convert.ToDecimal(objInvestor.Amount * intr / (Convert.ToDecimal(1 - (Math.Pow(Convert.ToDouble(one), Convert.ToDouble(term))))));
                    EMI = Math.Round(calc, 2);
                    objInvestor.EMI = EMI;

                    objInvestor.LoanPurpose = LoanRequest.PurposeName;
                    objInvestor.IspersonalGuarantee = LoanRequest.IsPersonalGuarantee;

                    /////////////////Get All approved Loans Detail for moolah connect hisotry///////////////////////////

                    var FinalDates = Convert.ToDateTime(LoanRequest.DateCreated).AddDays(Convert.ToInt32(GetGloabalVariables.RequestCompletionDays));
                    var GetAllAprrovedLoanHisotry = db.tbl_LoanRequests.Where(e => e.RequestId.Equals(LoanRequestID) && e.IsApproved.HasValue && e.IsApproved.Value).ToList();


                    if (GetAllAprrovedLoanHisotry != null)
                    {
                        int count = 0;
                        decimal? TotalAmount = 0;
                        foreach (var itemloan in GetAllAprrovedLoanHisotry)
                        {
                            if (itemloan.DateCreated < FinalDates)
                            {
                                count++;
                                TotalAmount += itemloan.Amount;
                            }
                        }
                        objInvestor.LoanRequested = count;
                        objInvestor.LoanAmount = TotalAmount;
                        objInvestor.PublishedDate = LoanRequest.PublishedDate;
                        if (LoanRequest.PublishedDate.HasValue)
                        {
                            objInvestor.LastDay = LoanRequest.PublishedDate.Value.AddDays(Settings.LoanRequestExpiryDays);
                        }

                        var loanofferss = db.tbl_Loanoffers.ToList();
                        var queryLoanOfferss = loanofferss.Where(e => e.LoanRequest_Id.Equals(LoanRequestID)).ToList();
                        var OfferAmounts = queryLoanOfferss.Where(e => e.OfferStatus == (int)LoanOfferStatus.Accepted && e.OfferedRate <= LoanRequest.Rate).Sum(e => e.OfferedAmount);
                        objInvestor.LoanFunded = OfferAmounts;
                        if (OfferAmounts - LoanRequest.Amount >= 0)
                            objInvestor.LoanFunded = LoanRequest.Amount;
                        else
                            objInvestor.LoanFunded = OfferAmounts;


                        decimal? AllOutstandingLoan = db.tbl_LoanRequests.Where(l => l.User_ID == LoanRequest.User_ID && l.LoanStatus == (int)LoanRequestStatus.Matched).Sum(l => l.Amount);

                        var allloans = db.tbl_LoanRequests.Where(l => l.User_ID == LoanRequest.User_ID && l.LoanStatus == (int)LoanRequestStatus.Matched);
                        //decrement the amount paid by borrower
                        var outstandpayment = (from i in allloans
                                               join q in db.tbl_LoanAmortization
                                               on i.RequestId equals q.LoanRequest_ID
                                               into left
                                               from r in left.DefaultIfEmpty()
                                               where r.PayStatus == (int)RepaymentPayStatus.Pending
                                               select new
                                               {
                                                   tbl_LoanRequests = i,
                                                   tbl_LoanAmortization = r
                                               });

                        decimal? SumOfOutstanding = outstandpayment.Sum(l => l.tbl_LoanAmortization.EmiAmount);
                        objInvestor.OutstandingAmount = Math.Round(Convert.ToDecimal(SumOfOutstanding), 2);


                    }


                    ///////////////////////////////////////////end/////////////////////////////////////////////////////

                    ////////////////////////////////////////Get funding history of loan request from tbl_loanoffers///////////////
                    LoanRequestOffers objLoanOfferes = new LoanRequestOffers();
                    objLoanOfferes.MiniMumOffer = GetGloabalVariables.MinimumOfferAmount;
                    objLoanOfferes.MinimumRate = GetGloabalVariables.MinimumOfferRate;
                    objLoanOfferes.LoanRequestID = LoanRequestID;
                    objLoanOfferes.IsApproved = LoanRequest.IsApproved;
                    objLoanOfferes.LoanStatus = LoanRequest.LoanStatus;
                    objLoanOfferes.PreliminaryStatus = LoanRequest.PreliminaryLoanStatus;

                    var queryLoanOffers = db.tbl_Loanoffers
                        .Where(e => e.LoanRequest_Id.Equals(LoanRequestID)
                            && (e.OfferStatus == (int)LoanOfferStatus.Pending || e.OfferStatus == (int)LoanOfferStatus.Accepted
                            || e.OfferStatus == (int)LoanOfferStatus.Expired
                            || e.OfferStatus == (int)LoanOfferStatus.Cancelled)
                            && (e.OfferedRate <= LoanRequest.Rate))
                            .OrderByDescending(l => l.OfferedRate)
                        .OrderByDescending(l => l.DateCreated).ToList();

                    if (queryLoanOffers.Count > 0)
                    {
                        var uniqueuser = (from i in queryLoanOffers
                                          group new { i.Investor_Id } by i.Investor_Id into byNmGp
                                          select byNmGp.Distinct()).ToList();
                        var OfferAmount = queryLoanOffers.Sum(e => e.OfferedAmount);
                        objLoanOfferes.LoanFunded = Math.Round(Convert.ToDecimal((OfferAmount / LoanRequest.Amount) * 100), 2);

                        if (OfferAmount - LoanRequest.Amount >= 0)
                            objInvestor.LoanFunded = LoanRequest.Amount;
                        else
                            objInvestor.LoanFunded = OfferAmount;



                        objLoanOfferes.UniqueBidders = uniqueuser.Count;
                        var HighestRate = queryLoanOffers.OrderByDescending(e => e.OfferedRate).FirstOrDefault();
                        objLoanOfferes.Highestrate = HighestRate.OfferedRate;
                        var LowestRate = queryLoanOffers.OrderBy(e => e.OfferedRate).FirstOrDefault();
                        objLoanOfferes.Lowestrate = LowestRate.OfferedRate;

                        /*caluclation for weighted average rate*/
                        var LoanofferforRequestID = db.tbl_Loanoffers
                            .Where(l => l.LoanRequest_Id.Equals(LoanRequestID) && l.OfferedRate <= LoanRequest.Rate);
                        var TotalofLoanOffers = LoanofferforRequestID.Sum(l => l.OfferedAmount);
                        decimal? AvergageRate = 0;
                        foreach (var offere in LoanofferforRequestID)
                            AvergageRate = AvergageRate + (offere.OfferedAmount / TotalofLoanOffers) * offere.OfferedRate;
                        /*end average weighted rate*/


                        objLoanOfferes.Averagerate = Math.Round(Convert.ToDecimal(AvergageRate), 2);
                        ///wehighted average 
                        ///admisibility
                        ///days hours minutes to calcluate
                        ///

                        if (objInvestor.HasExpiryDatePassed)
                        {
                            if (OfferAmount < LoanRequest.Amount
                                                && (LoanRequest.Amount.Value < (decimal)Settings.MinQuantum
                                                || objLoanOfferes.LoanFunded < (decimal)Settings.MinThreshold * 100))
                            {
                                objInvestor.ExpiredMessage = "Pending Admin processing of expiry";
                            }
                            else
                            {
                                objInvestor.ExpiredMessage = "Pending Issuer acceptance of offer(s)";
                            }
                        }

                    }
                    int? NumberofCompletionday = db.tbl_GlobalVariables.Select(l => l.RequestCompletionDays).FirstOrDefault();



                    //int[] TimeRemaing = ContollerExtensions.GetDaysHoursandMinutes(Convert.ToDateTime(LoanRequest.PublishedDate), Convert.ToInt32(NumberofCompletionday));
                    int[] TimeRemaing = new int[3];

                    if (LoanRequest.PreliminaryLoanStatus == (int)PreliminaryLoanRequestStatus.DraftPublished)
                        TimeRemaing = _ILoanAllocationTask.TimeLetfToExpireLoanRequest(LoanRequest.PublishedDate.Value);
                    else if (LoanRequest.PreliminaryLoanStatus == (int)PreliminaryLoanRequestStatus.DraftApproved)
                        TimeRemaing = _ILoanAllocationTask.TimeLetfToExpireLoanDraft(LoanRequest.ApproveDate.Value);

                    if (TimeRemaing.Count() > 0)
                    {
                        objLoanOfferes.days = TimeRemaing[0];
                        objLoanOfferes.Hours = TimeRemaing[1];
                        objLoanOfferes.minutest = TimeRemaing[2];
                    }
                    //////////////////////////detail of offers//////////////////

                    var loanoffersbyinvestor = (from l in db.tbl_Loanoffers
                                                join u in db.tbl_AccountDetails
                                                on l.Investor_Id equals u.User_ID
                                                where l.LoanRequest_Id == LoanRequestID
                                                && (l.OfferStatus <= 1 || l.OfferStatus == (int)LoanOfferStatus.Expired
                                                || l.OfferStatus == (int)LoanOfferStatus.Cancelled)
                                                orderby l.OfferedRate, l.DateCreated
                                                select new
                                                {
                                                    l.DateCreated,
                                                    l.OfferedAmount,
                                                    u.DisplayName,
                                                    l.OfferedRate,
                                                    l.OfferStatus,
                                                    l.AcceptedAmount

                                                }).ToList();
                    List<LoanOffersbyinvestors> listloanoffer = new List<LoanOffersbyinvestors>();
                    foreach (var offer in loanoffersbyinvestor)
                    {
                        LoanOffersbyinvestors obj = new LoanOffersbyinvestors();
                        obj.Amount = offer.OfferedAmount;
                        obj.UserName = offer.DisplayName;
                        obj.OfferDate = offer.DateCreated;
                        obj.OfferRate = offer.OfferedRate;
                        obj.AcceptedAmount = offer.AcceptedAmount;
                        listloanoffer.Add(obj);
                    }
                    objloanoffer.offersbyInvesotr = listloanoffer;
                    objloanoffer.LoanRequest = objInvestor;
                    objloanoffer.MoolahCore = objMoolahCore;
                    objloanoffer.AccountDetails = objAccountDetail;
                    objloanoffer.MoolahPeri = objMoolahPeri;
                    objloanoffer.LoanOffers = objLoanOfferes;
                }



            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objloanoffer = new InvestorLoanOffer();
            }

            return objloanoffer;
        }

        //Get Loan details for preview
        public InvestorLoanOffer GetLoanandBorrowerBriefDetailForPreviewRequestID(long LoanRequestID)
        {
            InvestorLoanOffer objloanoffer = new InvestorLoanOffer();
            try
            {
                tbl_Users user = (from p in db.tbl_Users
                                  where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                  select p).FirstOrDefault();
                if (user == null)
                    return new InvestorLoanOffer();

                Int64 Userid = user.UserID;

                if (user.UserRole == "Investor")
                    objloanoffer.IsInvestor = true;

                var LoanRequest = (from l in db.tbl_LoanRequests
                                   join u in db.tbl_Users
                                    on l.User_ID equals u.UserID
                                   join lp in db.tbl_LoanPurposesList
                                   on l.LoanPurpose_Id equals lp.LoanPurposeId
                                   join a in db.tbl_AccountDetails
                                   on l.User_ID equals a.User_ID
                                   join mc in db.tbl_MoolahCore
                                     on l.RequestId equals mc.LoanRequest_Id
                                   where l.RequestId == LoanRequestID
                                   select new
                                   {
                                       l.Amount,
                                       l.Rate,////////////////rate of interest
                                       l.IsPersonalGuarantee,
                                       l.Terms,/////////////time in months 12,24,36
                                       l.DateCreated,
                                       l.User_ID,
                                       lp.PurposeName,/////////loan purpose
                                       l.IsApproved,
                                       l.ApproveDate,
                                       l.LoanStatus,


                                       ////////////////////////////////moolah core section////////////////////
                                       mc.NetprofitafterTax_LY,
                                       mc.CashFlowfromOperations_LY,
                                       mc.Turnovers_LY,
                                       mc.Debt_Equity_LY,
                                       mc.CurrentRatio_LY,

                                       ///////////////////////////Compay and user profile
                                       a.DisplayName,
                                       a.Mobilenumber,
                                       a.AccountNumber,
                                       a.Gender,
                                       a.Nationality,
                                       a.DateofIncorporation,
                                       a.PRstatus,
                                       a.ResidentialAddress,
                                       a.BusinessName,
                                       a.IC_CompanyRegistrationNumber,
                                       a.BusinessOrganisation,
                                       a.tbl_NatureofBusinessList.NatureofBusinessName,

                                       u.UserName

                                   }).SingleOrDefault();
                if (LoanRequest != null)
                {
                    AccountDetail objAccountDetail = new AccountDetail();
                    objAccountDetail.DisplayName = LoanRequest.DisplayName == null ? "NA" : LoanRequest.DisplayName;
                    objAccountDetail.Nationality = LoanRequest.Nationality;
                    objAccountDetail.ResidentialAddress = LoanRequest.ResidentialAddress;
                    objAccountDetail.MobileNumber = LoanRequest.Mobilenumber == null ? "Not Specified" : LoanRequest.Mobilenumber;
                    objAccountDetail.AccountNumber = LoanRequest.AccountNumber;
                    objAccountDetail.BussinessName = LoanRequest.BusinessName;
                    objAccountDetail.EmailAddress = LoanRequest.UserName;

                    objAccountDetail.BussinessRegistrationNUmber = LoanRequest.IC_CompanyRegistrationNumber;
                    objAccountDetail.BussinessOrganisation = LoanRequest.BusinessOrganisation;
                    objAccountDetail.DateofIncorporation = Convert.ToDateTime(LoanRequest.DateofIncorporation).ToString("dd MMM yyyy");

                    var GetGloabalVariables = db.tbl_GlobalVariables.SingleOrDefault();

                    LoanRequestInverstor objInvestor = new LoanRequestInverstor();
                    objInvestor.Amount = LoanRequest.Amount;
                    objInvestor.Term = LoanRequest.Terms;
                    objInvestor.EIR = Convert.ToDecimal(LoanRequest.Rate);
                    /////////////emi calculate/////////////////
                    var term = objInvestor.Term;
                    decimal EMI = 0;
                    var intr = Convert.ToDecimal(objInvestor.EIR) / 1200;
                    var one = 1 / (1 + intr);
                    var calc = Convert.ToDecimal(objInvestor.Amount * intr / (Convert.ToDecimal(1 - (Math.Pow(Convert.ToDouble(one), Convert.ToDouble(term))))));
                    EMI = Math.Round(calc, 2);
                    objInvestor.EMI = EMI;

                    objInvestor.LoanPurpose = LoanRequest.PurposeName;
                    objInvestor.IspersonalGuarantee = LoanRequest.IsPersonalGuarantee;

                    objloanoffer.LoanRequest = objInvestor;
                    objloanoffer.AccountDetails = objAccountDetail;
                }



            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objloanoffer = new InvestorLoanOffer();
            }

            return objloanoffer;
        }

        /// <summary>
        /// Save Loan offer, two checks for that, investor should have sufficient balance and the loan should be in offer period
        /// </summary>
        /// <param name="Amount"></param>
        /// <param name="Rate"></param>
        /// <param name="Password"></param>
        /// <param name="LoanRequestID"></param>
        /// <param name="Term"></param>
        /// <returns>string</returns>
        public string SaveLoanOffer(string Amount, string Rate, string LoanRequestID, string Term)
        {
            try
            {
                var amount = Convert.ToDecimal(Amount);
                var rate = Convert.ToDecimal(Rate);

                long? id = (from p in db.tbl_Users
                            where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                            select p.UserID).FirstOrDefault();
                //////check if investor have that much of balance in his account from tbl_balances////////////////////
                tbl_Balances Balancecheck = db.tbl_Balances.FirstOrDefault(b => b.User_ID == id);
                long RequestID = Convert.ToInt64(LoanRequestID);
                var LoanDetail = db.tbl_LoanRequests.FirstOrDefault(l => l.RequestId == RequestID);

                if (LoanDetail.LoanStatus == (int)LoanRequestStatus.Matched)
                {
                    return ErrorMessages.LOCK_OFFER_CREATE_PROCESSING;
                }

                var borrowerAccountDetails = LoanDetail.tbl_Users.tbl_AccountDetails.FirstOrDefault();
                DateTime LoanCreatedDate = Convert.ToDateTime(LoanDetail.DateCreated);
                Double Numberofdays = Convert.ToDouble(db.tbl_GlobalVariables.Select(l => l.RequestCompletionDays).FirstOrDefault());
                if (Balancecheck != null && Balancecheck.LedgerAmount >= Convert.ToDecimal(Amount))
                {
                    tbl_Loanoffers objtblLoanOffers = new tbl_Loanoffers();
                    objtblLoanOffers.LoanRequest_Id = Convert.ToInt64(LoanRequestID);
                    objtblLoanOffers.Investor_Id = (long)id;
                    objtblLoanOffers.OfferedAmount = amount;
                    objtblLoanOffers.OfferedRate = rate;
                    objtblLoanOffers.DateCreated = DateTime.UtcNow;
                    db.tbl_Loanoffers.Add(objtblLoanOffers);
                    db.SaveChanges();

                    decimal? Userbalaceafteroffer = Balancecheck.LedgerAmount - Convert.ToDecimal(Amount);
                    //make a transaction
                    LoanTransactionDebit(Balancecheck.LedgerAmount, Convert.ToDecimal(Amount), Convert.ToInt64(LoanRequestID),
                        "Internet", "Make Offer", id, Userbalaceafteroffer);
                    _ITransactionDA.InternalTransactionCredit(Settings.EarmarkAccountBalanceID, Convert.ToDecimal(Amount),
                        Convert.ToInt64(LoanRequestID), "Internet", "Make Offer", id.Value);

                    //debit the user account and change user balance in balances                                                    
                    Balancecheck.ActualAmount = Balancecheck.ActualAmount - Convert.ToDecimal(Amount);
                    Balancecheck.LedgerAmount = Balancecheck.LedgerAmount - Convert.ToDecimal(Amount);
                    db.SaveChanges();

                    _IEmailSender.InvestorOfferMade(HttpContext.Current.User.Identity.Name,
                        borrowerAccountDetails != null ? borrowerAccountDetails.BusinessName : string.Empty,
                        (double)amount, (double)rate);

                    return ContollerExtensions.ReturnSuccess();

                }
                else
                    return "You have exceeded the balance in your account";

            }

            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Try Latter";
            }
        }
        #endregion
        #region LiveLoanListings
        ///////////Get Loans Detail on Which Investor has made offeres which are in offer time and not 100% funded/////////

        public LiveLoanListings GetLiveLoanListings()
        {
            LiveLoanListings objLiveLoanListing;
            try
            {

                long? UsreID = Convert.ToInt32((from p in db.tbl_Users
                                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                                select p.UserID).FirstOrDefault());
                var LiveLoanDetail = db.tbl_LoanRequests.Join(db.tbl_Loanoffers, l => l.RequestId, lo => lo.LoanRequest_Id, (l, lo) => new { tbl_LoanRequests = l, tbl_Loanoffers = lo }).Where(lo => lo.tbl_Loanoffers.Investor_Id == UsreID && lo.tbl_Loanoffers.OfferStatus == (int)LoanOfferStatus.Accepted).ToList();
                var DaysforOfferPeriod = db.tbl_GlobalVariables.FirstOrDefault();

                List<LoanListings> objListLoanListing = new List<LoanListings>();

                foreach (var items in LiveLoanDetail)
                {
                    int[] Daytimeremaing = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(items.tbl_LoanRequests.DateCreated), Convert.ToInt32(DaysforOfferPeriod.RequestCompletionDays));
                    decimal?[] FundedandAvgInterest = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(items.tbl_LoanRequests.RequestId, items.tbl_LoanRequests.Amount);//0 element is avg funded and 1 elemint is avg interest in array    
                    if (Daytimeremaing[2] > 0 && FundedandAvgInterest[0] < 100)///if offer period is not expiered then add to live listing and loan funded is less then 100
                    {
                        LoanListings obj = new LoanListings();
                        var accountdetal = db.tbl_AccountDetails.FirstOrDefault(a => a.User_ID == items.tbl_LoanRequests.User_ID);

                        obj.Company = accountdetal.BusinessName;

                        obj.Days = Daytimeremaing[0];
                        obj.Hours = Daytimeremaing[1];



                        obj.Funded = Math.Round(Convert.ToDecimal(FundedandAvgInterest[0]), 2);
                        obj.InterestRate = items.tbl_Loanoffers.OfferedRate;
                        obj.Lenders = Convert.ToInt32(FundedandAvgInterest[2]);
                        obj.LoanAmount = items.tbl_LoanRequests.Amount;
                        obj.LoanRequestID = items.tbl_LoanRequests.RequestId;
                        obj.PaymentGrade = "Fair Static";
                        obj.PG = "yes static";
                        obj.Purpose = items.tbl_LoanRequests.tbl_LoanPurposesList.PurposeName;
                        obj.Tenor = items.tbl_LoanRequests.Terms;
                        objListLoanListing.Add(obj);
                    }
                }


                #region rgnFundedLoans / rgnMaturedLoans
                List<FundedLoansListings> objListFundedLoansListings = new List<FundedLoansListings>();
                List<InvestorMaturedLoans> objListInvestorMaturedLoans = new List<InvestorMaturedLoans>();
                foreach (var items in LiveLoanDetail)
                {
                    int[] Daytimeremaing = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(items.tbl_LoanRequests.DateCreated), Convert.ToInt32(DaysforOfferPeriod.RequestCompletionDays));
                    decimal?[] FundedandAvgInterest = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(items.tbl_LoanRequests.RequestId, items.tbl_LoanRequests.Amount);//0 element is avg funded and 1 elemint is avg interest in array    
                    if (FundedandAvgInterest[0] == 100 && (ChkLastEmiIsPending(items.tbl_LoanRequests.RequestId) == null || ChkLastEmiIsPending(items.tbl_LoanRequests.RequestId) == false))///if loan is 100% funded, we have to make a check when user is adding a offer to loan we have to check that loan is in offer period otherwise he should not be able to make his offer
                    {
                        FundedLoansListings objFundedLoanListings = new FundedLoansListings();
                        objFundedLoanListings.Funded = FundedandAvgInterest[0];
                        objFundedLoanListings.InterestRate = items.tbl_Loanoffers.OfferedRate;
                        objFundedLoanListings.LoanAmount = items.tbl_LoanRequests.Amount;
                        objFundedLoanListings.LoanRequestID = items.tbl_LoanRequests.RequestId;
                        objFundedLoanListings.MaturityDate = Convert.ToDateTime(GetLastEmiDatebyLoanRequestID(items.tbl_LoanRequests.RequestId)).ToString("mm/dd/yyyy");
                        objFundedLoanListings.PaymentGrade = "Fair Static";
                        objFundedLoanListings.PG = "Yes static";
                        objFundedLoanListings.Purpose = items.tbl_LoanRequests.tbl_LoanPurposesList.PurposeName;
                        objFundedLoanListings.Tenor = items.tbl_LoanRequests.Terms;
                        objFundedLoanListings.Referece = Convert.ToString(items.tbl_LoanRequests.RequestId).PadLeft(7, '0');
                        objListFundedLoansListings.Add(objFundedLoanListings);
                    }
                    else if (FundedandAvgInterest[0] == 100 && ChkLastEmiIsPending(items.tbl_LoanRequests.RequestId) == true)////if last emi is purchased loan is matured
                    {
                        InvestorMaturedLoans objInvestorMaturedLoans = new InvestorMaturedLoans();
                        objInvestorMaturedLoans.Interest = items.tbl_Loanoffers.OfferedRate;
                        objInvestorMaturedLoans.LoanAmount = items.tbl_LoanRequests.Amount;
                        objInvestorMaturedLoans.LoanReqestID = items.tbl_LoanRequests.RequestId;
                        objInvestorMaturedLoans.MaturityDate = Convert.ToDateTime(GetLastEmiDatebyLoanRequestID(items.tbl_LoanRequests.RequestId)).ToString("mm/dd/yyyy");
                        objInvestorMaturedLoans.NumberofLenders = Convert.ToInt32(FundedandAvgInterest[3]);
                        objInvestorMaturedLoans.PaymentGrade = "Fair static";
                        objInvestorMaturedLoans.PG = "Yes static";
                        objInvestorMaturedLoans.Purpose = items.tbl_LoanRequests.tbl_LoanPurposesList.PurposeName;
                        objInvestorMaturedLoans.Referece = Convert.ToString(items.tbl_LoanRequests.RequestId).PadLeft(7, '0');
                        objInvestorMaturedLoans.Tenor = items.tbl_LoanRequests.Terms;
                        objListInvestorMaturedLoans.Add(objInvestorMaturedLoans);
                    }
                }

                #endregion

                objLiveLoanListing = new LiveLoanListings();
                objLiveLoanListing.LoanListings = objListLoanListing;
                objLiveLoanListing.FundedListings = objListFundedLoansListings;
                objLiveLoanListing.MaturedListings = objListInvestorMaturedLoans;
                return objLiveLoanListing;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return objLiveLoanListing = null;
            }
        }
        #endregion
        private DateTime? GetLastEmiDatebyLoanRequestID(long LoanRequestID)
        {
            //maturity date

            var LastEmiDate = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == LoanRequestID).OrderByDescending(l => l.EmiDate).FirstOrDefault();
            return LastEmiDate.EmiDate;




        }

        private bool? ChkLastEmiIsPending(long RequestID)
        {
            var LastEmiIsPending = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID.Equals(RequestID)).OrderByDescending(l => l.EmiDate).FirstOrDefault();
            return LastEmiIsPending.PayStatus == (int)RepaymentPayStatus.Pending;
        }
        #region Dashboard

        public InvestorDashboard LoadDashboardInformation()
        {
            InvestorDashboard objInvestorDasjboard;
            try
            {

                long? UsreID = Convert.ToInt32((from p in db.tbl_Users
                                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                                select p.UserID).FirstOrDefault());

                var AccountDetail = ((from p in db.tbl_AccountDetails
                                      where p.User_ID == UsreID
                                      select new { p.DisplayName, p.AccountNumber, p.tbl_BanksList.BankName }).FirstOrDefault());


                var GetAllLoanOffers = db.tbl_Loanoffers.Where(l => l.Investor_Id == UsreID);//all loan offers will be displayed to user on summary section
                var LoanOfferes = GetAllLoanOffers.Where(l => l.Investor_Id == UsreID && l.OfferStatus == (int)LoanOfferStatus.Accepted)
                    .GroupBy(x => x.LoanRequest_Id).ToList();//make check that for unique loan request id on which user has make offers
                //get all approved offeres for user
                #region Funds Summary

                //total interest earned from emis interest
                decimal? varAccruedInterest = 0;
                foreach (var i in LoanOfferes)
                {
                    var requestId = i.Max(x => x.LoanRequest_Id);
                    var finalAmount = i.Max(x => x.tbl_LoanRequests.FinalAmount.Value);
                    var acceptedAmount = i.Sum(x => x.AcceptedAmount.Value);
                    var rate = i.Max(x => x.tbl_LoanRequests.Rate.Value);
                    var tenor = i.Max(x => x.tbl_LoanRequests.Terms);

                    //var offerReps = _ILoanPaymentTask.GetRepaymentSchedule((double)acceptedAmount, (double)rate, tenor, DateTime.UtcNow).ToList();

                    //decimal SumofPaidInterstbyBorrower = 0;

                    //var loanReps = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == requestId).ToList();
                    //for (int l = 0; l < loanReps.Count; l++)
                    //{
                    //    if (loanReps[l].PayStatus == (int)RepaymentPayStatus.Paid)
                    //    {
                    //        SumofPaidInterstbyBorrower += (decimal)offerReps[l].Interest;
                    //    }
                    //}

                    var EmiPayment = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == requestId
                        && la.PayStatus == (int)RepaymentPayStatus.Paid).ToList();

                    decimal? SumofPaidInterstbyBorrower = Math.Round(EmiPayment.Sum(l => (l.Interest.Value * acceptedAmount / finalAmount)), 2);

                    var EmiLatePayments = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == requestId
                        && la.LateInterestPayStatus == (int)RepaymentPayStatus.Paid).ToList();
                    decimal? lateInterest = Math.Round(EmiLatePayments.Sum(l => (l.LateInterest.Value * acceptedAmount / finalAmount)), 2);

                    varAccruedInterest += lateInterest + SumofPaidInterstbyBorrower;

                }

                ///user offer is approved but not recieved any emi amount from borroouer is outstanind loan investment
                decimal? varOutstandingLoanInv = 0;

                foreach (var i in LoanOfferes)
                {
                    var requestId = i.Max(x => x.LoanRequest_Id);
                    var finalAmount = i.Max(x => x.tbl_LoanRequests.FinalAmount.Value);
                    var acceptedAmount = i.Sum(x => x.AcceptedAmount.Value);
                    var rate = i.Max(x => x.tbl_LoanRequests.Rate.Value);
                    var tenor = i.Max(x => x.tbl_LoanRequests.Terms);

                    //decimal SumofPaidPrincipalbyBorrower = 0;

                    //var loanReps = db.tbl_LoanAmortization.Where(la => la.LoanRequest_ID == requestId).ToList();

                    //if (loanReps.Any(l=>l.PayStatus != (int)RepaymentPayStatus.Paid))
                    //{
                    //    var offerReps = _ILoanPaymentTask.GetRepaymentSchedule((double)acceptedAmount, (double)rate, tenor, DateTime.UtcNow).ToList();
                    //    for (int l = 0; l < loanReps.Count; l++)
                    //    {
                    //        if (loanReps[l].PayStatus == (int)RepaymentPayStatus.Paid)
                    //        {
                    //            SumofPaidPrincipalbyBorrower += (decimal)offerReps[l].Principal;
                    //        }
                    //    }

                    //    varOutstandingLoanInv += acceptedAmount - SumofPaidPrincipalbyBorrower;    
                    //}



                    if (db.tbl_LoanAmortization.Any(l => l.LoanRequest_ID == requestId
                        && l.PayStatus != (int)RepaymentPayStatus.Paid))
                    {
                        decimal? pendingRep = null;
                        if (db.tbl_LoanAmortization.Any(l => l.LoanRequest_ID == requestId
                        && l.PayStatus == (int)RepaymentPayStatus.Paid))
                        {
                            pendingRep = db.tbl_LoanAmortization.Where(l => l.LoanRequest_ID == requestId
                                && l.PayStatus == (int)RepaymentPayStatus.Paid)
                                .Sum(
                                l => l.Principal.Value * acceptedAmount / finalAmount
                                );
                        }

                        pendingRep = pendingRep.HasValue ? Math.Round(pendingRep.Value, 2) : 0;

                        varOutstandingLoanInv += acceptedAmount - pendingRep;
                    }

                }

                Summary_InvestorDashboard objSummary = new Summary_InvestorDashboard();
                objSummary.AccruedInterest = varAccruedInterest;

                var UserBalance = db.tbl_Balances.FirstOrDefault(u => u.User_ID == UsreID);
                if (UserBalance != null)
                    objSummary.AvailFunds = Math.Round(Convert.ToDecimal(UserBalance.LedgerAmount), 2);
                else
                    objSummary.AvailFunds = 0;
                objSummary.OutstandingLoans = Math.Round(Convert.ToDecimal(varOutstandingLoanInv), 2);

                var pendingOffers = db.tbl_Loanoffers.Where(u => u.Investor_Id == UsreID && u.OfferStatus == (int)LoanOfferStatus.Pending);

                if (pendingOffers.Count() > 0)
                    objSummary.CurrentBids = Math.Round(Convert.ToDecimal(pendingOffers.Sum(u => u.OfferedAmount)), 2);

                objSummary.GrossYield = db.tbl_LoanTransactions.Where(l => l.PaymentMode == "Repayment Share" && l.User_ID == UsreID).Sum(l => l.Credited);
                objSummary.TootalMoolah =
                     (objSummary.AvailFunds.HasValue ? objSummary.AvailFunds : 0)
                    + (objSummary.OutstandingLoans.HasValue ? objSummary.OutstandingLoans : 0)
                    + (objSummary.CurrentBids.HasValue ? objSummary.CurrentBids : 0);
                objSummary.AccountName = AccountDetail.DisplayName;
                objSummary.LoanInvestment = varOutstandingLoanInv;

                var user = db.tbl_Users.Where(x => x.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault();

                if (user != null)
                    objSummary.AccountNumber = CommonMethods.GetAccountNumber(user.UserID, user.UserRole);

                #endregion

                #region Investor details

                var details = new InvestorDetails();

                if (user != null)
                {
                    if (user.DateCreated.HasValue)
                        details.MemberSince = user.DateCreated.Value;
                    var loanOffers = db.tbl_Loanoffers.Where(x => x.Investor_Id == user.UserID
                        && (x.OfferStatus == (int)LoanOfferStatus.Accepted
                        || x.OfferStatus == (int)LoanOfferStatus.Pending));
                    details.LoanOffersCount = loanOffers.Count();
                    if (loanOffers.Count() > 0)
                        details.CummulativeAmountOfLoanOffers = (double)loanOffers.Sum(x => x.OfferedAmount);

                    var matchedLoanOffers = db.tbl_Loanoffers.Where(x => x.Investor_Id == user.UserID && x.OfferStatus == (int)LoanOfferStatus.Accepted);
                    if (matchedLoanOffers != null)
                    {
                        details.MatachedLoanOffersCount = matchedLoanOffers.Count();
                        var amount = matchedLoanOffers.Sum(x => x.AcceptedAmount);
                        details.MatachedLoanOffersAmount = amount != null ? (double)amount : 0;
                    }
                    details.WeightedAverageNominalYeidl = _IInvestorLoanSummaryDA.GetWeightedAverageNominalYeildForUser(HttpContext.Current.User.Identity.Name);
                }

                #endregion

                objInvestorDasjboard = new InvestorDashboard();
                objInvestorDasjboard.Summary = objSummary;
                objInvestorDasjboard.Summary.Details = details;

                #region TransferMoney
                TransferMoney objTransferMoney = new TransferMoney();

                objTransferMoney.CurrentBlance = db.tbl_Balances.Where(l => l.User_ID == UsreID).Sum(l => l.LedgerAmount);
                objTransferMoney.MinimumBalancetoWithdraw = db.tbl_GlobalVariables.Select(l => l.MinimuAmountToWithdraw).FirstOrDefault();
                objTransferMoney.AccountNumber = AccountDetail.AccountNumber;
                objTransferMoney.BankName = AccountDetail.BankName;
                #endregion
                objInvestorDasjboard.TransferMoney = objTransferMoney;
                return objInvestorDasjboard;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return objInvestorDasjboard = null;
            }


        }

        #region Dashboard Profile
        /// <summary>
        /// ///////////////Get User Profile by User ID
        /// </summary>
        /// <returns></returns>
        public tbl_AccountDetails GetUserProfileDetailbyUserID()
        {
            return db.tbl_AccountDetails.Where(x => x.tbl_Users.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).SingleOrDefault() ?? new tbl_AccountDetails();
        }

        #endregion

        #endregion

        #region Loan Transactions
        public IEnumerable<tbl_LoanTransactions> GetAllTransactionbyUserID()
        {
            long? UsreID = Convert.ToInt32((from p in db.tbl_Users
                                            where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                            select p.UserID).FirstOrDefault());
            return db.tbl_LoanTransactions.Where(l => l.User_ID == UsreID);
        }
        #endregion


        /// <summary>
        /// chekc current balance of user and return where the withdraw transaction can be done or not
        /// </summary>
        /// <param name="Amount"></param>
        /// <returns></returns>
        public string CheckCurrentBalance(decimal Amount)
        {
            try
            {
                var user = (from p in db.tbl_Users
                            where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                            select p).SingleOrDefault();

                if (user == null)
                {
                    return ContollerExtensions.ReturnFailure();
                }

                decimal? CurrentBalance = db.tbl_Balances.Where(l => l.User_ID == user.UserID)
                    .Select(l => l.LedgerAmount).FirstOrDefault();

                if (CurrentBalance >= Amount)
                {
                    //deduct balace from tbl_balances and update it user blance
                    decimal? NewAvailabebalance = CurrentBalance - Amount;


                    tbl_WithDrawMoney objWtihdraw = new tbl_WithDrawMoney();
                    objWtihdraw.CurrentBalance = CurrentBalance;
                    objWtihdraw.AfterBalance = NewAvailabebalance;
                    objWtihdraw.DateCreated = DateTime.UtcNow;
                    objWtihdraw.User_ID = user.UserID;
                    objWtihdraw.Status = "Pending";
                    objWtihdraw.WithDrawAmount = Amount;
                    db.tbl_WithDrawMoney.Add(objWtihdraw);
                    db.SaveChanges();
                    long WithDrawID = objWtihdraw.WithDrawID;

                    tbl_Balances obj = new tbl_Balances();
                    obj = db.tbl_Balances.FirstOrDefault(l => l.User_ID == user.UserID);


                    ///We have passed the Withdraw id instead of loan request id because we have to make join of this for admin transaction reference
                    LoanTransactionDebit(CurrentBalance, Amount, WithDrawID, "Withdrawl money", AuditMessages.WITHDRAW_FUND,
                        user.UserID, NewAvailabebalance);
                    _ITransactionDA.InternalTransactionCredit(Settings.FundTransferAccountBalanceID,
                        Amount, null, "Withdrawl money", AuditMessages.WITHDRAW_FUND, user.UserID);

                    obj.LedgerAmount = NewAvailabebalance;
                    db.SaveChanges();

                    _IEmailSender.AdminUserCreatedFundTransferRequest(user.UserName,
                        CommonMethods.GetAccountNumber(user.UserID, user.UserRole),
                        (double)Amount,
                        (double)(CurrentBalance.HasValue ? CurrentBalance.Value : 0));


                    return ContollerExtensions.ReturnSuccess();
                }
                else
                    return ContollerExtensions.ReturnFailure();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return "Some error occured please try latter. Or contact Admin";
            }

        }
        /// <summary>
        /// Delte the offer from offer table as user is withdrawing his offer, and credit the offer amount to user balance make a entry in transaction table 
        /// </summary>
        /// <param name="OfferID"></param>
        /// <returns></returns>
        public string WithDrawOfferbyOfferID(int OfferID)
        {
            try
            {
                var user = (from p in db.tbl_Users
                            where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                            select p).SingleOrDefault();

                if (user == null)
                {
                    return ContollerExtensions.ReturnFailure();
                }

                tbl_Loanoffers objOffers = new tbl_Loanoffers();
                objOffers = db.tbl_Loanoffers.FirstOrDefault(l => l.OfferId == OfferID);

                var request = db.tbl_LoanRequests.Where(r => r.RequestId == objOffers.LoanRequest_Id).SingleOrDefault();

                if (request.LoanStatus == (int)LoanRequestStatus.Matched)
                {
                    return ErrorMessages.LOCK_OFFER_WITHDRAW_PROCESSING;
                }

                tbl_Balances obj = new tbl_Balances();
                obj = db.tbl_Balances.FirstOrDefault(l => l.User_ID == user.UserID);

                //make a transaction
                decimal? balaftertransaction = obj.LedgerAmount + objOffers.OfferedAmount;

                _ITransactionDA.InternalTransactionDebit(Settings.EarmarkAccountBalanceID,
                    objOffers.OfferedAmount, objOffers.LoanRequest_Id, "Internet", "Withdraw Offer", user.UserID);
                LoanTransactionCredit(obj.LedgerAmount, objOffers.OfferedAmount, objOffers.LoanRequest_Id, "Internet", "Withdraw Offer", user.UserID, balaftertransaction);


                objOffers.OfferStatus = (int)LoanOfferStatus.Withdrawn;
                db.SaveChanges();

                //add the amount in balance table
                obj.ActualAmount = obj.ActualAmount + objOffers.OfferedAmount;//add offer amount to current balance of user;
                obj.LedgerAmount = obj.LedgerAmount + objOffers.OfferedAmount;//add offer amount to ledger balance of user;
                db.SaveChanges();



                _IEmailSender.InvestorOfferWithdrawn(user.UserName,
                    request != null ? request.tbl_Users.tbl_AccountDetails.FirstOrDefault().BusinessName : string.Empty,
                    (double)objOffers.OfferedAmount,
                    (double)objOffers.OfferedRate);

                return ContollerExtensions.ReturnSuccess();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return StatusMessages.SYSTEM_ERROR;
            }


        }

        /// <summary>
        /// make transaction
        /// </summary>
        /// <param name="AmountBefore"></param>
        /// <param name="AmountToCredited"></param>
        /// <param name="LoanRequestID"></param>
        /// <param name="PaymentMode"></param>
        /// <param name="PaymentTerms"></param>
        /// <param name="UserID"></param>
        /// <param name="BalanceAfter"></param>
        public void LoanTransactionCredit(decimal? AmountBefore, decimal? AmountToCredited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
            objTransaction.Amount = Convert.ToDecimal(AmountBefore);///its the the amount user have in tbl_blances  before the transaction has occured
            objTransaction.Credited = Convert.ToDecimal(AmountToCredited);//amount of offer that should be credit in user balace
            objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime
            objTransaction.Request_Id = LoanRequestID;//loan request from which offer has withdrawn
            objTransaction.PaymentMode = PaymentMode;
            objTransaction.PaymentTerms = PaymentTerms;
            objTransaction.User_ID = UserID;
            objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
            objTransaction.Balance = Convert.ToDecimal(BalanceAfter);//balance of user after crediting the amount in his account
            dbMoolahConnectEntities db = new dbMoolahConnectEntities();
            db.tbl_LoanTransactions.Add(objTransaction);
            db.SaveChanges();

        }
        /// <summary>
        /// make transaction while make an loan offer
        /// </summary>
        /// <param name="AmountBefore"></param>
        /// <param name="AmountToCredited"></param>
        /// <param name="LoanRequestID"></param>
        /// <param name="PaymentMode"></param>
        /// <param name="PaymentTerms"></param>
        /// <param name="UserID"></param>
        /// <param name="BalanceAfter"></param>
        public static void LoanTransactionDebit(decimal? AmountBefore, decimal? AmountTobeDebited, long? LoanRequestID, string PaymentMode, string PaymentTerms, long? UserID, decimal? BalanceAfter)
        {
            tbl_LoanTransactions objTransaction = new tbl_LoanTransactions();
            objTransaction.Amount = AmountBefore;///its the the amount user have in tbl_blances  before the transaction has occured
            objTransaction.Debted = AmountTobeDebited;//amount of offer that should be debited in user balace
            objTransaction.DateCreated = DateTime.UtcNow;//singapore datetime
            if (PaymentMode != "Withdrawl money")
                objTransaction.Request_Id = LoanRequestID;//loan request from which offer has withdrawn
            else
                objTransaction.Withdraw_ID = LoanRequestID;
            objTransaction.PaymentMode = PaymentMode;
            objTransaction.PaymentTerms = PaymentTerms;
            objTransaction.User_ID = UserID;
            objTransaction.Balance = BalanceAfter;//balance of user after debting the amount from his account
            objTransaction.Reference = UniqueReference.GetRandomUniqueNumber();
            dbMoolahConnectEntities db = new dbMoolahConnectEntities();
            db.tbl_LoanTransactions.Add(objTransaction);
            db.SaveChanges();
        }


        /// <summary>
        /// Method for getting list of litigation List
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> OutstandingLitigation()
        {
            var Litigation = new List<SelectListItem>()
                           {
                                new SelectListItem(){ Value = "Overdraft Facility", Text = "Overdraft Facility" }  ,     
                                new SelectListItem(){ Value = "Unsecured Loan", Text = "Unsecured Loan" }, 
                                new SelectListItem(){ Value = "Trade Creditors", Text = "Trade Creditors" },
                                new SelectListItem(){ Value = "Secured Loan", Text = "Secured Loan" },  
                                new SelectListItem(){ Value = "Factoring Discount", Text = "Factoring Discount" }
                            };
            return Litigation;
        }

        /// <summary>
        /// Method for getting list of ispersonalguarantee List
        /// </summary>
        /// <returns></returns>
        private List<SelectListItem> IsPersonalguarantee()
        {
            var Titles = new List<SelectListItem>()
                           {
                                new SelectListItem(){ Text = "No", Value= "false", Selected = false}  ,     
                               new SelectListItem(){ Text = "Yes", Value= "true", Selected = false} 
                                                    
                            };
            return Titles;
        }
        ///////////////////////Add a custom value for Moolah perks for lenders
        private List<SelectListItem> AddCoustomValueinEnd()
        {
            var AddinListItem = db.tbl_MoolahPerksForLenders.Where(p => p.Isactive == true).OrderBy(p => p.MoolahPerk).ToList();
            var FinalList = new List<SelectListItem>();
            foreach (var moolahperk in AddinListItem)
            {
                FinalList.Add(new SelectListItem() { Value = moolahperk.MoolahPerkId.ToString(), Text = moolahperk.MoolahPerk.ToString() });
            }
            FinalList.Add(new SelectListItem() { Value = "Custom", Text = "Custom" });

            return FinalList;

        }
        private List<SelectListItem> MoolaCoreTime()
        {
            var Time = new List<SelectListItem>()
            {
                new SelectListItem(){ Value = "Weekly", Text = "Weekly" }  ,     
                new SelectListItem(){ Value = "Monthly", Text = "Monthly" }, 
            };
            return Time;
        }
        /// <summary>
        /// Function used for getting values of indicators and return correspondance image name
        /// </summary>
        /// <returns></returns>
        public string TestUserVerification(int? Status)
        {
            switch (Status)
            {
                case (int)VerificationStatus.Verified:
                    return "tick.png";
                    break;
                case (int)VerificationStatus.Unverified:
                    return "cross.png";
                    break;
                case (int)VerificationStatus.Disclosed:
                    return "cross.png";
                    break;
                case (int)VerificationStatus.Undisclosed:
                    return "cross.png";
                    break;
                default:
                    return "cross.png";
                    break;
            }
        }
        /// <summary>
        /// for checking document verification
        /// </summary>
        /// <param name="status"></param>
        /// <returns></returns>
        //public string TestDocumentVerification(bool? status)
        //{
        //    switch (status)
        //    {
        //        case true:
        //            return "tick.png";

        //        default:
        //            return "cross.png";

        //    }
        //}
        /// <summary>
        /// check user verification 
        /// </summary>
        /// <param name="ColumnValue"></param>
        /// <returns></returns>

        public bool CheckVerificationStatus(string ColumnValue)
        {
            switch (ColumnValue)
            {
                case "Verified":
                    return true;
                case "Unverified":
                    return false;
                default:
                    return true;
            }
        }

        /// <summary>
        ///investor further refferences to loan request, by adding it in his watch list
        /// </summary>
        public string AddToWatchList(long LoanRequestID)
        {
            try
            {

                long? UserID = Convert.ToInt32((from p in db.tbl_Users
                                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                                select p.UserID).FirstOrDefault());
                var IfExists = this.db.tbl_WatchList.FirstOrDefault(l => l.User_ID == UserID && l.Request_ID == LoanRequestID);
                if (IfExists != null)
                {
                    return "You have already added this to watchlist";
                }
                else
                {
                    tbl_WatchList obj = new tbl_WatchList();
                    obj.Request_ID = LoanRequestID;
                    obj.User_ID = UserID;
                    obj.Datecreated = DateTime.UtcNow;
                    this.db.tbl_WatchList.Add(obj);
                    this.db.SaveChanges();
                    return ContollerExtensions.ReturnSuccess();
                }

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }

        /// <summary>
        /// Method for binding details of user at create corporate action load
        /// </summary>
        /// <returns></returns>
        public BorrowerAccount CorporateUserLoad()
        {
            try
            {
                string id = (from p in db.tbl_Users
                             where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                             select p.UserID).FirstOrDefault().ToString();


                BorrowerAccountDetailsModel objaccount = new BorrowerAccountDetailsModel();
                Knowledgeassessment objknowledgeassessment = new Knowledgeassessment();
                //BorrowerDocuments objdocuments = new BorrowerDocuments();


                //////////////////////////////////// Get list of Security Questions , Nature of buisness and banks list for binding with drop downs ///////////////////////// 

                objaccount.SecurityQuestionList = db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();
                objaccount.NatureofBusinessList = db.tbl_NatureofBusinessList.Where(p => p.Isactive == true).ToList();
                objaccount.BanksList = db.tbl_BanksList.Where(p => p.Isactive == true).ToList();
                objaccount._Nationality = new SelectList(new List<SelectListItem>
                            {
                                new SelectListItem
                                    {
                                        Text = "Singapore Citizen",
                                        Value = "Singapore Citizen",
                                        Selected = true
                                    },
                                new SelectListItem {Text = "Singapore PR", Value = "Singapore PR"},
                                new SelectListItem {Text = "Other Nationalities", Value = "Other Nationalities"}
                            }, "Value", "Text", "Singapore");

                // Fill up knowladge assesment data
                objknowledgeassessment.Parentlist =
                    db.tbl_Knowledgeassessment_Parentlist.Where(k => k.InvType == "B" || k.InvType == "C")
                      .OrderBy(p => p.Priority)
                      .ToList();

                tbl_AccountDetails objuser = new tbl_AccountDetails();

                //////////////////////////////////// If user is logged in then get details of user from Userid for displaying in account details ///////////////////

                if (id != "0" && id != null)
                {
                    int Userid = Convert.ToInt32(id);

                    var commonUser = new UserCommonOperations();
                    //objdocuments.Userid = Userid;
                    objuser = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);

                    if (objuser != null)
                    {
                        objaccount._Titles = new SelectList(TitleItems(), "Value", "Text", objuser.Title);
                        objaccount.Title = objuser.Title;
                        objaccount.FirstName = objuser.FirstName;
                        objaccount.LastName = objuser.LastName;
                        objaccount.DisplayName = objuser.DisplayName;
                        objaccount.RoleJobTitle = objuser.RoleJobTitle;
                        objaccount.BusinessName = objuser.BusinessName;
                        objaccount.BusinessRegistrationnumber = objuser.IC_CompanyRegistrationNumber;
                        objaccount.BusinessOrganisation = objuser.BusinessOrganisation;
                        objaccount.NatureofBusinessId = objuser.Natureofbusiness_Id == null ? 1 : Convert.ToInt32(objuser.Natureofbusiness_Id);
                        var regAddress = CommonMethods.LoadAddressField(objuser.RegisteredAddress);
                        objaccount.RegisteredAddress1 = regAddress[0];
                        objaccount.RegisteredAddress2 = regAddress[1];
                        objaccount.RegisteredPostalCode = regAddress[2];
                        var mailingAddress = CommonMethods.LoadAddressField(objuser.BusinessMailingAddress);
                        objaccount.BusinessMailingAddress1 = mailingAddress[0];
                        objaccount.BusinessMailingAddress2 = mailingAddress[1];
                        objaccount.BusinessMailingPostalCode = mailingAddress[2];
                        objaccount.DirectLine = objuser.Main_OfficeContactnumber;
                        objaccount.Mobilenumber = objuser.Mobilenumber;
                        objaccount.Othercontactnumber = objuser.OptionalContactnumber;
                        objaccount.BankId = Convert.ToInt32(objuser.Bank_Id);
                        objaccount.BankAccountNumber = objuser.AccountNumber;
                        objaccount.BankNumber = objuser.BankNumber;
                        objaccount.BranchName = objuser.BranchName;
                        objaccount.BranchNumber = objuser.BranchNumber;
                        objaccount.DateofIncorporation = objuser.DateofIncorporation;
                        objaccount.BankAccountName = objuser.AccountName;
                        objaccount.NricNumber = objuser.NRIC_Number;
                        objaccount.SiccCode = objuser.SiccCode;
                        objaccount.PassportNumber = objuser.PassportNumber;
                        objaccount.Nationality = objuser.Nationality;
                        objaccount.EmailAddress = commonUser.GetCurrentUserEmail();
                        objaccount.SecurityQuestion1 = Convert.ToInt32(db.tbl_SecurityQuestionsForUsers.Where(p => p.User_ID == Userid).Select(p => p.Question_Id).FirstOrDefault());
                        objaccount.SecurityQuestion2 = Convert.ToInt32(db.tbl_SecurityQuestionsForUsers.Where(p => p.Question_Id != objaccount.SecurityQuestion1 && p.User_ID == Userid).Select(p => p.Question_Id).FirstOrDefault());

                        var securityanswer1 = (from p in db.tbl_SecurityQuestionsForUsers
                                               where p.Question_Id == objaccount.SecurityQuestion1 && p.User_ID == Userid
                                               select p.Answer).FirstOrDefault();

                        objaccount.SecurityAnswer1 = securityanswer1;
                        var securityanswer2 = (from p in db.tbl_SecurityQuestionsForUsers
                                               where p.Question_Id == objaccount.SecurityQuestion2 && p.User_ID == Userid
                                               select p.Answer).FirstOrDefault();
                        objaccount.SecurityAnswer2 = securityanswer2;
                    }
                    else
                    {
                        objaccount._Titles = new SelectList(TitleItems(), "Value", "Text", "Mr.");
                        objaccount.EmailAddress = commonUser.GetCurrentUserEmail();
                    }
                }
                else
                {
                    objaccount._Titles = new SelectList(TitleItems(), "Value", "Text");

                }

                ///////////////////////////// for setting steps value for enabling the tabs depending upon the current step //////////////////////////////////////////

                BorrowerAccount objborrower = new BorrowerAccount(objaccount, objknowledgeassessment);
                objborrower.Step = "1";
                if (HttpContext.Current.Session["step"] == null || HttpContext.Current.Session["step"].ToString() == "")
                {
                    if (objuser == null)
                    {

                        HttpContext.Current.Session["step"] = 1;
                    }
                    else
                    {

                        HttpContext.Current.Session["step"] = 3;
                    }
                }

                return objborrower;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }
        }



        /// <summary>
        /// Creating corporate investors
        /// </summary>
        /// <param name="objborrower"></param>
        /// <returns></returns>
        public string CreateCorporateInestor(BorrowerAccountDetailsModel objborrower)
        {
            try
            {

                Int64 Userid = (from p in db.tbl_Users
                                where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                                select p.UserID).FirstOrDefault();


                tbl_AccountDetails objaccounts = db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == Userid);
                if (objaccounts != null)
                {
                    ///////////////////////////////// for updating record in tbl_accountdetails table //////////////////////////////////
                    objaccounts.Title = objborrower.Title;
                    objaccounts.FirstName = objborrower.FirstName;
                    objaccounts.LastName = objborrower.LastName;
                    objaccounts.DisplayName = objborrower.DisplayName;
                    objaccounts.RoleJobTitle = objborrower.RoleJobTitle;
                    objaccounts.BusinessName = objborrower.BusinessName;
                    objaccounts.IC_CompanyRegistrationNumber = objborrower.BusinessRegistrationnumber;
                    objaccounts.BusinessOrganisation = objborrower.BusinessOrganisation;
                    var regAdd = ContollerExtensions.AppendAddressField(objborrower.RegisteredAddress1,
                                                           objborrower.RegisteredAddress2,
                                                           objborrower.RegisteredPostalCode);
                    objaccounts.RegisteredAddress = regAdd;
                    var mailingAdd = ContollerExtensions.AppendAddressField(objborrower.BusinessMailingAddress1,
                                                           objborrower.BusinessMailingAddress2,
                                                           objborrower.BusinessMailingPostalCode);
                    objaccounts.BusinessMailingAddress = mailingAdd;
                    objaccounts.Main_OfficeContactnumber = objborrower.DirectLine;
                    objaccounts.Mobilenumber = objborrower.Mobilenumber;
                    objaccounts.OptionalContactnumber = objborrower.Othercontactnumber;
                    objaccounts.Bank_Id = objborrower.BankId;
                    objaccounts.BankNumber = objborrower.BankNumber;
                    objaccounts.BranchName = objborrower.BranchName;
                    objaccounts.BranchNumber = objborrower.BranchNumber;
                    objaccounts.AccountNumber = objborrower.BankAccountNumber;
                    objaccounts.AccountName = objborrower.BankAccountName;
                    objaccounts.NRIC_Number = objborrower.NricNumber;
                    objaccounts.PassportNumber = objborrower.PassportNumber;
                    objaccounts.SiccCode = objborrower.SiccCode;
                    objaccounts.Nationality = objborrower.Nationality;

                    string dateString;
                    CultureInfo provider = CultureInfo.InvariantCulture;
                    dateString = objborrower.DateofIncorporation.Value.ToShortDateString();
                    DateTime dt = Convert.ToDateTime(dateString);
                    DateTime? dr = dt;
                    objaccounts.DateofIncorporation = dr;
                    db.SaveChanges();

                }
                else
                {
                    ///////////////////////////////// for inserting new record in tbl_accountdetails table //////////////////////////////////
                    tbl_AccountDetails objaccountsnewuser = new tbl_AccountDetails();
                    objaccountsnewuser.Title = objborrower.Title;
                    objaccountsnewuser.FirstName = objborrower.FirstName;
                    objaccountsnewuser.LastName = objborrower.LastName;
                    objaccountsnewuser.DisplayName = objborrower.DisplayName;
                    objaccountsnewuser.RoleJobTitle = objborrower.RoleJobTitle;
                    objaccountsnewuser.BusinessName = objborrower.BusinessName;
                    objaccountsnewuser.IC_CompanyRegistrationNumber = objborrower.BusinessRegistrationnumber;
                    objaccountsnewuser.BusinessOrganisation = objborrower.BusinessOrganisation;
                    var regAdd = ContollerExtensions.AppendAddressField(objborrower.RegisteredAddress1,
                                                           objborrower.RegisteredAddress2,
                                                           objborrower.RegisteredPostalCode);
                    objaccountsnewuser.RegisteredAddress = regAdd;
                    var mailingAdd = ContollerExtensions.AppendAddressField(objborrower.BusinessMailingAddress1,
                                                           objborrower.BusinessMailingAddress2,
                                                           objborrower.BusinessMailingPostalCode);
                    objaccountsnewuser.BusinessMailingAddress = mailingAdd;
                    objaccountsnewuser.Main_OfficeContactnumber = objborrower.DirectLine;
                    objaccountsnewuser.Mobilenumber = objborrower.Mobilenumber;
                    objaccountsnewuser.OptionalContactnumber = objborrower.Othercontactnumber;
                    objaccountsnewuser.Bank_Id = objborrower.BankId;
                    objaccountsnewuser.BankNumber = objborrower.BankNumber;
                    objaccountsnewuser.BranchName = objborrower.BranchName;
                    objaccountsnewuser.BranchNumber = objborrower.BranchNumber;
                    objaccountsnewuser.AccountNumber = objborrower.BankAccountNumber;
                    objaccountsnewuser.AccountName = objborrower.BankAccountName;
                    objaccountsnewuser.AccountType = "Investor";
                    objaccountsnewuser.NRIC_Number = objborrower.NricNumber;
                    objaccountsnewuser.PassportNumber = objborrower.PassportNumber;
                    objaccountsnewuser.SiccCode = objborrower.SiccCode;
                    objaccountsnewuser.Nationality = objborrower.Nationality;
                    string dateString;

                    CultureInfo provider = CultureInfo.InvariantCulture;
                    dateString = objborrower.DateofIncorporation.Value.ToShortDateString();
                    DateTime dt = Convert.ToDateTime(dateString);
                    DateTime? dr = dt;
                    objaccountsnewuser.DateofIncorporation = dr;
                    objaccountsnewuser.User_ID = Userid;


                    db.tbl_AccountDetails.Add(objaccountsnewuser);
                    db.SaveChanges();

                }
                HttpContext.Current.Session["step"] = 2;
                return ContollerExtensions.ReturnSuccess();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return ContollerExtensions.ReturnFailure();
            }
        }


        /// <summary>
        /// Method for checking email id's availability
        /// </summary>      
        /// <returns></returns>
        public bool CheckDisplayNameAvailbilty(string displayname, string Emailaddress)
        {
            try
            {
                var user = db.tbl_Users.Where(u => u.UserName == HttpContext.Current.User.Identity.Name.ToLower()).SingleOrDefault();

                if (user == null)
                    return false;

                if (user.UserRole == "Admin")
                {
                    if (Emailaddress == null)
                    {
                        return false;
                    }

                    user = db.tbl_Users.Where(u => u.UserName.ToLower() == Emailaddress.ToLower()).SingleOrDefault();
                }

                if (user == null)
                    return false;

                if (db.tbl_AccountDetails.Where(ad => ad.User_ID != user.UserID)
                    .Any(ad => ad.DisplayName.ToLower() == displayname.ToLower()))
                {
                    return false;
                }

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }

        }
    }

}