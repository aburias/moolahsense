﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Helpers;
using System.Web.Mvc;
using MoolahConnectClassLibrary.Models;
using MoolahConnectClassLibrary.Models.ViewModels;
using System.Web.Security;
using NLog;
using MoolahConnect.Services.Entities;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util.Logging;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Tasks.Implementation;
using System.Web;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Util.SealedClasses;
using MoolahConnect.Util.GenerateReference;
using MoolahConnect.Util;
using MoolahConnect.Util.Email;

namespace MoolahConnectClassLibrary.DAL
{

    public class UserCommonOperations : IDisposable
    {
        private dbMoolahConnectEntities db;
        private IEmailSender _IEmailSender;
        private IVerificationToken _IVerificationToken;
        private IUser _IUser;
        private ISecurityQuestions _ISecurityQuestions;

        public UserCommonOperations()
        {
            db = new dbMoolahConnectEntities();
            _IEmailSender = new EmailSender();
            _IVerificationToken = new VerificationToken();
            _IUser = new User();
            _ISecurityQuestions = new SecurityQuestions();
        }

        /// <summary>
        /// Method for performing login functionality
        /// </summary>
        /// <param name="UserName">Username entered by user</param>
        /// <param name="Password">Password entered by user</param>
        /// <returns></returns>
        public string LoginUser(string UserName, string Password)
        {
            var message = string.Empty;
            try
            {
                var User = db.tbl_Users.SingleOrDefault(p => p.UserName.Equals(UserName,StringComparison.OrdinalIgnoreCase) && p.Password.Equals(Password) && p.Isactive == true);
                if (User != null)
                {
                    FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, User.UserName, DateTime.UtcNow, DateTime.UtcNow.AddDays(2), false, User.UserRole);
                    String d1 = FormsAuthentication.Encrypt(tkt);
                    HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, d1);
                    HttpContext.Current.Response.Cookies.Add(ck);
                    if (User.InvestorType == "Corporate")
                        message = User.InvestorType;
                    else
                        message = User.UserRole;
                }
                else
                {

                    //Check if account is suspended
                    if (db.tbl_Users.Where(p => p.UserName.Equals(UserName,StringComparison.OrdinalIgnoreCase) && p.Password.Equals(Password)).Select(l => l.Isactive).FirstOrDefault() == false)
                        message = "Suspended";
                    else
                        message = ContollerExtensions.ReturnFailure();



                }
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                message = ContollerExtensions.ReturnFailure();

            }

            return message;
        }

        /// <summary>
        /// Method to be called at index load of forgot password and account number for binding questions list
        /// </summary>
        /// <returns></returns>
        public ResetPasswordAccountnumberModel BindForgotIndex()
        {
            ForgotPasswordModel objPassword = new ForgotPasswordModel();
            ForgotAccountNumberModel objAccountnumber = new ForgotAccountNumberModel();
            ResetPasswordAccountnumberModel objforgot;
            try
            {
                objPassword.SecurityQuestionList = db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();
                objAccountnumber.SecurityQuestionList = db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();
                objforgot = new ResetPasswordAccountnumberModel(objPassword, objAccountnumber);
                if (HttpContext.Current.Session["forgotstep"] == null)
                {
                    HttpContext.Current.Session["forgotstep"] = 1;
                }

            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                objforgot = new ResetPasswordAccountnumberModel(objPassword, objAccountnumber);
            }

            return objforgot;
        }

        /// <summary>
        /// Method for getting userole of user
        /// </summary>      
        /// <returns></returns>
        public string GetUserRole()
        {
            try
            {
                string Userrole = db.tbl_Users.Where(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()).Select(p => p.UserRole).FirstOrDefault();
                return Userrole;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return null;
            }

        }

        /// <summary>
        /// Method for creating user
        /// </summary>        
        /// <returns></returns>
        public bool RegisterUser(RegisterUserModel objregister, out string error)
        {
            try
            {
                String Username = objregister.EmailAddress;
                String Password = objregister.Password;
                String Userrole = objregister.Userrole;
                bool SubscribeforUpdates = objregister.SubscribeforUpdate;
                String UniqueAccountnumber = string.Empty;
                UserRole role;

                ////////////////// Custome code for generating Account number depending upon user role (Investor,Borrower)  ////////////////////////////////////////                
                if (Userrole == "Investor")
                {
                    role = UserRole.Investor;
                }
                else
                {
                    role = UserRole.Borrower;
                }

                var userKey = Guid.NewGuid();
                var status = new MembershipCreateStatus();

                Membership.CreateUser(Username, Password, null, GlobalConstansValues.SEC_QUES_ANSWER, GlobalConstansValues.SEC_QUES_ANSWER, true, userKey, out status);

                if (status != MembershipCreateStatus.Success)
                {
                    error = StatusMessages.SYSTEM_ERROR;
                    return false;
                }

                tbl_Users objUser = new tbl_Users();
                objUser.UserName = Username;
                objUser.Password = Password;
                objUser.Isactive = true;
                objUser.UserRole = Userrole;
                objUser.SubscribeforUpdates = SubscribeforUpdates;
                objUser.DateCreated = DateTime.UtcNow;
                objUser.IsSubmitted = false;
                objUser.AspnetUserId = userKey;
                objUser.IsEmailVerified = false;
                objUser.AdminVerification = AdminVerification.Pending.ToString();
                if (objregister.Userrole != "Borrower")
                    objUser.InvestorType = objregister.InverstorType;
                db.tbl_Users.Add(objUser);
                db.SaveChanges();

                _ISecurityQuestions.Add(objUser.UserID, objregister.SecurityQuestion1, objregister.SecurityQuestionAnswer1);
                _ISecurityQuestions.Add(objUser.UserID, objregister.SecurityQuestion2, objregister.SecurityQuestionAnswer2);
                _ISecurityQuestions.Add(objUser.UserID, objregister.SecurityQuestion3, objregister.SecurityQuestionAnswer3);

                var requestUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                var requestRawUrl = HttpContext.Current.Request.Url.PathAndQuery;
                var rootUrl = requestUrl.Replace(requestRawUrl, string.Empty);

                var token = Guid.NewGuid();
                _IVerificationToken.Save((Guid)Membership.GetUser(Username).ProviderUserKey, token);

                string url = string.Format("{0}/User/EmailVerification?token={1}&email={2}", rootUrl, token, Username);


                if (!_IEmailSender.EmailConfirmation(Username, url, role))
                {
                    RollBackRegisterUser(Username);
                    error = StatusMessages.MAIL_SEND_FAILURE;
                    return false;
                }

                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                RollBackRegisterUser(objregister.EmailAddress);
                error = StatusMessages.SYSTEM_ERROR;
                return false;
            }

        }

        private void RollBackRegisterUser(string username)
        {
            try
            {
                Membership.DeleteUser(username);
                _IUser.Remove(username);
            }
            catch (Exception ex)
            {

                MoolahLogManager.LogException(ex);
            }
        }

        /// <summary>
        /// Method for checking email id's availability
        /// </summary>      
        /// <returns></returns>
        public Boolean IsEmailAddressAvailable(string EmailAddress)
        {
            try
            {
                var User = db.tbl_Users.SingleOrDefault(p => p.UserName != null
                    && p.UserName.Trim().ToLower().Equals(EmailAddress.Trim().ToLower()) && p.Isactive == true);
                return User == null ? true : false;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }

        }


        public void Dispose()
        {
            throw new NotImplementedException();
        }

        #region GetAllApprovedLoans
        ///////////////////////////////////////////////Get all approved loans/////////////////
        public HomeLoanRequests GetAllAprrovedLoansList()
        {
            var GetAllApprovedLoan = (from l in db.tbl_LoanRequests
                                      join p in db.tbl_LoanPurposesList
                                      on l.LoanPurpose_Id equals p.LoanPurposeId
                                      join q in db.tbl_AccountDetails
                                      on l.User_ID equals q.User_ID
                                      select new
                                      {
                                          tbl_LoanRequests = l,
                                          tbl_LoanPurposesList = p,
                                          tbl_AccountDetails = q
                                      }).Where(l => l.tbl_LoanRequests.IsApproved.HasValue && l.tbl_LoanRequests.IsApproved.Value && (l.tbl_LoanRequests.LoanStatus == (int)LoanRequestStatus.InProgress))
                                          .OrderByDescending(l => l.tbl_LoanRequests.DateCreated).ToList();

            List<HomeApprovedLoanRequests> objApprovedLoanList = new List<HomeApprovedLoanRequests>();
            var NumberofCompletionday = db.tbl_GlobalVariables.FirstOrDefault();

            foreach (var item in GetAllApprovedLoan)
            {
                if (item.tbl_LoanRequests.ApproveDate != null)
                {
                    var FinalDate = Convert.ToDateTime(item.tbl_LoanRequests.DateCreated).AddDays(Convert.ToDouble(NumberofCompletionday.RequestCompletionDays));
                    if (DateTime.UtcNow <= FinalDate)
                    {
                        ///////get loan offers of particular id///////////////
                        var LoanOffers = db.tbl_Loanoffers.Where(l => l.LoanRequest_Id == item.tbl_LoanRequests.RequestId).ToList();//condtion removed because of cahnge in functionallity&& l.OfferStatus==true                                                                                
                        HomeApprovedLoanRequests obj = new HomeApprovedLoanRequests();
                        obj.Amount = item.tbl_LoanRequests.Amount;

                        decimal?[] LoanCalculations = ContollerExtensions.CalculateAvgFundedAvgInterestandNumofLenders(item.tbl_LoanRequests.RequestId, item.tbl_LoanRequests.Amount);
                        obj.Funded = LoanCalculations[0];
                        obj.AverageRate = item.tbl_LoanRequests.Rate;
                        obj.DateCreated = item.tbl_LoanRequests.DateCreated;
                        obj.Term = item.tbl_LoanRequests.Terms;
                        obj.PreliminaryStatus = item.tbl_LoanRequests.PreliminaryLoanStatus;
                        int[] Timeremaing = CommonMethods.GetDaysHoursandMinutes(Convert.ToDateTime(item.tbl_LoanRequests.PublishedDate), Convert.ToInt32(NumberofCompletionday.RequestCompletionDays));
                        if (Timeremaing.Count() > 0)
                        {
                            obj.days = Timeremaing[0];
                            obj.Hours = Timeremaing[1];
                            obj.minutest = Timeremaing[2];
                        }
                        obj.Purpose = item.tbl_LoanPurposesList.PurposeName;
                        //   obj.Reference = "ABC 789# static";
                        obj.LoanRequestID = item.tbl_LoanRequests.RequestId;
                        if (obj.minutest > 0)
                            objApprovedLoanList.Add(obj);
                    }
                }

            }
            HomeLoanRequests objHome = new HomeLoanRequests();

            objHome.ApprovedLoanList = objApprovedLoanList;
            return objHome;
        }
        ////////////////////////////////////////end//////////////////////////////////

        #endregion
        #region Admin Login

        public string AdminLogin(AdminLogin objAdmin)
        {
            var AdminDetail = db.tbl_Users.SingleOrDefault(l => l.UserName == objAdmin.UserName && l.Password == objAdmin.Password && l.UserRole == "Admin" && l.Isactive == true);
            if (AdminDetail != null)
            {
                FormsAuthenticationTicket tkt = new FormsAuthenticationTicket(1, AdminDetail.UserName, DateTime.UtcNow, DateTime.UtcNow.AddDays(2), false, AdminDetail.UserRole);
                String d1 = FormsAuthentication.Encrypt(tkt);
                HttpCookie ck = new HttpCookie(FormsAuthentication.FormsCookieName, d1);
                HttpContext.Current.Response.Cookies.Add(ck);

                return AdminDetail.UserRole;
            }
            else
                return "LoginFailed";

        }
        #endregion
        /// <summary>
        /// if it returns false then it redirect to create else it will rediret to loanrequest
        /// </summary>
        /// <returns></returns>
        public bool VerificationCheck()
        {
            try
            {

                var LoggedinUser = db.tbl_Users.SingleOrDefault(
                    p => p.UserName.Trim().ToLower().Equals(HttpContext.Current.User.Identity.Name.Trim().ToLower()));

                if (LoggedinUser == null)
                {
                    return false;
                }

                var objUserVerification = (from p in db.tbl_UserVerification where p.UserID == LoggedinUser.UserID select p).ToList();

                if (LoggedinUser.AdminVerification == "Rejected" || LoggedinUser.AdminVerification == "Pending")
                {
                    return false;
                }

                else
                {
                    return true;

                }



            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }

        }

        /// <summary>
        /// Method for submit user
        /// </summary>      
        /// <returns></returns>
        public bool SubmitUser()
        {
            try
            {
                var userObj = db.tbl_Users.FirstOrDefault(p => p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower());
                userObj.IsSubmitted = true;
                db.SaveChanges();

                _IEmailSender.UserSubmittedRegistration(userObj.UserName);
                _IEmailSender.OnUserSubmittedRegistration(userObj.UserName, CommonMethods.GetAccountNumber(userObj.UserID, userObj.UserRole));

                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }

        public bool UpdateUserEmail(string newEmail, out string error)
        {
            var userid = (from p in db.tbl_Users
                          where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                          select p.UserID).FirstOrDefault();
            return UpdateUserEmail(userid, newEmail, out error, true);
        }

        public bool UpdateUserEmail(long userid, string newEmail, out string error, bool isVerificationNeed)
        {
            var userObj = db.tbl_Users.SingleOrDefault(u => u.UserID == userid);
            var oldEmail = userObj.UserName;

            var oldProviderUserKey = Membership.GetUser(userObj.UserName).ProviderUserKey.ToString();
            try
            {
                if (Membership.GetUser(newEmail) != null)
                {
                    error = "E-Mail already exist";
                    return false;
                }

                // Delete membership account
                Membership.DeleteUser(oldEmail);

                var userKey = Guid.NewGuid();
                var status = new MembershipCreateStatus();

                Membership.CreateUser(newEmail, userObj.Password, null, GlobalConstansValues.SEC_QUES_ANSWER, GlobalConstansValues.SEC_QUES_ANSWER, true, userKey, out status);

                if (status != MembershipCreateStatus.Success)
                {
                    RollBackRegisterUser(userObj, oldProviderUserKey, oldEmail, newEmail);
                    error = StatusMessages.SYSTEM_ERROR;
                    return false;
                }

                // Update user info
                userObj.UserName = newEmail;
                userObj.AspnetUserId = userKey;
                if (isVerificationNeed)
                {
                    userObj.IsEmailVerified = false;
                }
                db.SaveChanges();

                if (isVerificationNeed)
                {
                    // Build & send the confirmation message
                    var requestUrl = HttpContext.Current.Request.Url.AbsoluteUri;
                    var requestRawUrl = HttpContext.Current.Request.Url.PathAndQuery;
                    var rootUrl = requestUrl.Replace(requestRawUrl, string.Empty);
                    var token = Guid.NewGuid();
                    _IVerificationToken.Save((Guid)Membership.GetUser(newEmail).ProviderUserKey, token);
                    var url = string.Format("{0}/User/EmailVerification?token={1}&email={2}", rootUrl, token, newEmail);
                    var userRole = userObj.UserRole == "Investor" ? UserRole.Investor : UserRole.Borrower;
                    if (!_IEmailSender.EmailUpdateConfirmation(newEmail, url, userRole))
                    {
                        RollBackRegisterUser(userObj, oldProviderUserKey, oldEmail, newEmail);
                        error = StatusMessages.MAIL_SEND_FAILURE;
                        return false;
                    }
                }

                error = string.Empty;
                return true;
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                RollBackRegisterUser(userObj, oldProviderUserKey, oldEmail, newEmail);
                error = StatusMessages.SYSTEM_ERROR;
                return false;
            }
        }

        private void RollBackRegisterUser(tbl_Users userObj, string oldProviderUserKey, string oldEmail, string newEmail)
        {
            try
            {
                Membership.DeleteUser(newEmail);

                if (Membership.FindUsersByEmail(oldEmail).Count == 0)
                {
                    var status = new MembershipCreateStatus();
                    Membership.CreateUser(oldEmail, userObj.Password, null, GlobalConstansValues.SEC_QUES_ANSWER, GlobalConstansValues.SEC_QUES_ANSWER, true, oldProviderUserKey, out status);
                }

                userObj.UserName = oldEmail;
                userObj.AspnetUserId = new Guid(oldProviderUserKey);
                userObj.IsEmailVerified = true;
                db.SaveChanges();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
            }
        }

        public string GetCurrentUserEmail()
        {
            try
            {
                var userid = (from p in db.tbl_Users
                              where p.UserName.Trim().ToLower() == HttpContext.Current.User.Identity.Name.Trim().ToLower()
                              select p.UserID).FirstOrDefault();
                var userObj = db.tbl_Users.SingleOrDefault(u => u.UserID == userid);
                return userObj.UserName;
            }
            catch (Exception)
            {
                return string.Empty;
            }
        }

        public bool IsValidNRICFIN(string value, string displayName)
        {
            try
            {
                if (value == null)
                {
                    return false;
                }

                return !db.tbl_AccountDetails.Where(a =>
                    a.tbl_Users != null
                    && a.tbl_Users.UserRole == "Investor"
                    && a.tbl_Users.InvestorType == "Individual"
                    && a.NRIC_Number != null
                    && (string.IsNullOrEmpty(displayName)
                    || (a.DisplayName != null && a.DisplayName.ToLower() != displayName.ToLower()))
                    && a.NRIC_Number.ToLower() == value.ToLower()).Any();
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsValidEmail(string emailAddress, long userID)
        {
            try
            {
                return !db.tbl_Users.Where(u => u.UserID != userID && u.UserName.ToLower() == emailAddress.ToLower()).Any();
            }
            catch (Exception ex)
            {
                MoolahLogManager.LogException(ex);
                return false;
            }
        }
    }
}