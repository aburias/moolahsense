﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MoolahConnect.Services.Entities;
using MoolahConnect.Tasks.Implementation;
using MoolahConnect.Tasks.Interfaces;
using MoolahConnect.Util.Logging;
using MoolahConnectClassLibrary.Models.ViewModels;
using MoolahConnectClassLibrary.Models.ViewModels.Admin;
using System.Web.Security;
using MoolahConnect.Services.Interfaces;
using MoolahConnect.Services.Implementation;
using MoolahConnect.Util.Enums;
using MoolahConnect.Util;
using MoolahConnect.Util.Email;

namespace MoolahConnectClassLibrary.DAL
{
    public class AccountOperations
    {
        private readonly dbMoolahConnectEntities _db;
        private readonly IEmailSender _IEmailSender;
        private readonly IUser _IUser;

        public AccountOperations()
        {
            if (_db == null)
            {
                _db = new dbMoolahConnectEntities();
            }

            _IEmailSender = new EmailSender();
            _IUser = new User();
        }

        public AccountDetailsBase GetUserDetailbyUserId(string userId, bool isAdmin)
        {
            try
            {
                var userid = Convert.ToInt32(userId);
                var userAccount = _db.tbl_Users.FirstOrDefault(l => l.UserID == userid);

                if (userAccount == null) return null;

                AccountDetailsBase objaccount;
                if (isAdmin)
                {
                    objaccount = new AdminAccountDetail { UseriD = userid };

                    // Load verification details
                    LoadVerifications((AdminAccountDetail)objaccount);
                }
                else
                {
                    objaccount = new AccountDetailsBase { UseriD = userid };
                }

                objaccount.IsEmailVerified = userAccount.IsEmailVerified;
                objaccount.InvestorType = userAccount.InvestorType;
                objaccount.UserRole = userAccount.UserRole;
                objaccount.AccountType = userAccount.UserRole;

                // Load account details
                LoadAccountDetails(objaccount);

                // Load security information
                LoadSecurityQuestions(objaccount);
                
                MembershipUser user = Membership.GetUser(userAccount.UserName);
                
                
                objaccount.IsLocked = user != null ? user.IsLockedOut : false;
                objaccount.Emailaddress = userAccount.UserName;

                return objaccount;
            }
            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
                return null;
            }
        }

        private void LoadAccountDetails(AccountDetailsBase objaccount)
        {
            var details = _db.tbl_AccountDetails.SingleOrDefault(p => p.User_ID == objaccount.UseriD);
            if (details != null)
            {
                objaccount.BankId = Convert.ToInt32(details.Bank_Id);
                objaccount.BankNumber = details.BankNumber;
                objaccount.BranchName = details.BranchName;
                objaccount.BranchNumber = details.BranchNumber;
                objaccount.BankAccountNumber = details.AccountNumber;
                objaccount.BankAccountName = details.AccountName;
                objaccount.FirstName = details.FirstName;
                objaccount.Nationality = details.Nationality;
                objaccount.NricNumber = details.NRIC_Number;
                objaccount.PassportNumber = details.PassportNumber;
                objaccount.BusinessName = details.BusinessName;
                objaccount.BusinessRegistrationnumber = details.IC_CompanyRegistrationNumber;
                objaccount.BusinessOrganisation = details.BusinessOrganisation;
                objaccount.SiccCode = details.SiccCode;
                objaccount.DateofIncorporationString =
                    Convert.ToDateTime(details.DateofIncorporation).ToString("dd/MM/yyyy");
                objaccount.DateofIncorporation = details.DateofIncorporation;
                var regAddress = CommonMethods.LoadAddressField(details.RegisteredAddress);
                objaccount.RegisteredAddress1 = regAddress[0];
                objaccount.RegisteredAddress2 = regAddress[1];
                objaccount.RegisteredPostalCode = regAddress[2];
                var mailingAddress = CommonMethods.LoadAddressField(details.BusinessMailingAddress);
                objaccount.BusinessMailingAddress1 = mailingAddress[0];
                objaccount.BusinessMailingAddress2 = mailingAddress[1];
                objaccount.BusinessMailingPostalCode = mailingAddress[2];
                objaccount.DirectLine = details.Main_OfficeContactnumber;
                objaccount.Mobilenumber = details.Mobilenumber;
                objaccount.PositionHeld = details.RoleJobTitle;
                objaccount.DisplayName = details.DisplayName;
                objaccount.PaidUpCapital = details.PaidUpCapital == null ? 0.0 : Convert.ToDouble(details.PaidUpCapital);
                objaccount.DateofBirth = details.DateofBirth;
                objaccount.Gender = details.Gender;
                var resAddress = CommonMethods.LoadAddressField(details.ResidentialAddress);
                objaccount.ResidentialAddress1 = resAddress[0];
                objaccount.ResidentialAddress2 = resAddress[1];
                objaccount.ResidentialPostalCode = resAddress[2];
                objaccount.MainContactNumber = details.Main_OfficeContactnumber;
            }

            objaccount.BanksList = (from p in this._db.tbl_BanksList
                                    where p.Isactive == true
                                    select p).ToList<tbl_BanksList>();

            objaccount.SecurityQuestionList = _db.tbl_SecurityQuestions.Where(p => p.Status == true).ToList();

            objaccount.Genders = new List<SelectListItem>
                {
                    new SelectListItem() {Text = "Male", Value = "Male", Selected = false},
                    new SelectListItem() {Text = "Female", Value = "Female", Selected = false}
                };
        }

        private void LoadSecurityQuestions(AccountDetailsBase objaccount)
        {
            var userquest = _db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == objaccount.UseriD).ToList();
            var q = 0;
            foreach (var ite in userquest)
            {
                switch (q)
                {
                    case 0:
                        objaccount.SecurityQuestion1 = ite.Question_Id;
                        objaccount.SecurityAnswer1 = ite.Answer;
                        break;
                    case 1:
                        objaccount.SecurityQuestion2 = ite.Question_Id;
                        objaccount.SecurityAnswer2 = ite.Answer;
                        break;
                    default:
                        objaccount.SecurityQuestion3 = ite.Question_Id;
                        objaccount.SecurityAnswer3 = ite.Answer;
                        break;
                }
                q++;
            }
        }

        private void LoadVerifications(AdminAccountDetail objaccount)
        {
            var verification = _db.tbl_UserVerification.FirstOrDefault(l => l.UserID == objaccount.UseriD);
            var userAccount = _db.tbl_Users.FirstOrDefault(l => l.UserID == objaccount.UseriD);

            if (userAccount != null)
            {
                objaccount.isSubmitted = userAccount.IsSubmitted.HasValue ? userAccount.IsSubmitted.Value : false;
            }

            if (verification != null && userAccount != null)
            {
                objaccount.BankVerification = verification.Bank;
                objaccount.BankNumberVerification = verification.BankNumber;
                objaccount.BranchNameVerification = verification.BranchName;
                objaccount.BranchNumberVerification = verification.BranchNumber;
                objaccount.BankAccountNumberVerification = verification.BankAccountNumber;
                objaccount.BankAccountNameVerification = verification.BankAccountName;
                objaccount.FirstNameVerification = verification.FirstName;
                objaccount.NationalityVerification = verification.Nationality;
                objaccount.NricNumberVerification = verification.NricNumber;
                objaccount.PassportNumberVerification = verification.PassportNumber;
                objaccount.BussinessNameVerification = verification.BussinessName;
                objaccount.BussinessRegistrationNumberVerification = verification.BussinessRegistrationNumber;
                objaccount.BussinessOrganisationVerification = verification.BussinessOrganisation;
                objaccount.SiccCodeVerification = verification.SiccCode;
                objaccount.DateofIncorporationVerification = verification.DateofIncorporation;

                var regAddressVer = CommonMethods.LoadAddressField(verification.RegisteredAddress);
                objaccount.RegisteredAddress1Verification = GetVerificationPhrase(regAddressVer[0]);
                objaccount.RegisteredAddress2Verification = GetVerificationPhrase(regAddressVer[1]);
                objaccount.RegisteredAddressPostVerification = GetVerificationPhrase(regAddressVer[2]);

                var busAddressVer = CommonMethods.LoadAddressField(verification.BussinessMailingAddress);
                objaccount.BussinessMailingAddress1Verification = GetVerificationPhrase(busAddressVer[0]);
                objaccount.BussinessMailingAddress2Verification = GetVerificationPhrase(busAddressVer[1]);
                objaccount.BussinessMailingAddressPostVerification = GetVerificationPhrase(busAddressVer[2]);

                objaccount.DirectLineVerification = verification.DirectLine;
                objaccount.MobileVerification = verification.Mobile;
                objaccount.OfficialCorrespondenceEmailVerification = verification.OptinalContact;
                objaccount.PositionHeldVerification = verification.RoleOrJobTitle;
                objaccount.DisplayNameVerification = verification.DisplayName;
                objaccount.PaidUpCapitalVerification = verification.PaidUpCapital;
                objaccount.DoBVerification = verification.DOB;
                objaccount.GenderVerification = verification.Gender;

                var resAddressVer = CommonMethods.LoadAddressField(verification.ResidentialAddress);
                objaccount.ResidentialAddress1Verification = GetVerificationPhrase(resAddressVer[0]);
                objaccount.ResidentialAddress2Verification = GetVerificationPhrase(resAddressVer[1]);
                objaccount.ResidentialAddressPostVerification = GetVerificationPhrase(resAddressVer[2]);

                objaccount.PostalCodeVerification = verification.PostalCode;
                objaccount.MainContactNumberVerification = verification.DirectLine;
                objaccount.EmailVerification = verification.Email;
                objaccount.AdminVerification = userAccount.AdminVerification;
                objaccount.AdminCommentOnUserData = verification.CommentsOnFieild;
                objaccount.AdminCommentonDocuments = verification.CommentsOnDocuments;
                objaccount.AdminPersonelComments = verification.AdminPersonelComment;
            }
        }

        public void SaveUserDetailbyUserId(AdminAccountDetail objaccount)
        {
            try
            {
                var adminUser = HttpContext.Current.User.Identity.Name;
                var userObj = _db.tbl_Users.FirstOrDefault(l => l.UserID == objaccount.UseriD);
                userObj.InvestorType = objaccount.InvestorType;
                userObj.UserRole = objaccount.UserRole;
                var user = Membership.GetUser(userObj.UserName);
                if (objaccount.IsLocked && !user.IsLockedOut)
                {
                    _IUser.LockUser((Guid)user.ProviderUserKey);
                }
                else if (!objaccount.IsLocked && user.IsLockedOut)
                {
                    user.UnlockUser();
                    _IEmailSender.OnAccountUnlocked(user.UserName);
                }

                if (objaccount.AdminVerification != null)
                {
                    var tbluser = _db.tbl_Users.FirstOrDefault(l => l.UserID == objaccount.UseriD);
                    if (tbluser != null && tbluser.AdminVerification != objaccount.AdminVerification)
                    {
                        tbluser.AdminVerification = objaccount.AdminVerification;
                        tbluser.Isactive = objaccount.AdminVerification != "Suspended";

                        string oldValue;
                        try
                        {
                            oldValue = _db.Entry(tbluser).OriginalValues["AdminVerification"].ToString();
                        }
                        catch (Exception)
                        {
                            oldValue = "NA";
                        }

                        var accDet = _db.tbl_AccountDetails.SingleOrDefault(ac => ac.User_ID == objaccount.UseriD);
                        long? accId = accDet != null ? accDet.AccountId : 0;

                        MoolahLogManager.LogAdminAudit(@"User Account Admin Verification", adminUser, null, accId, "AdminVerification",
                            oldValue, tbluser.AdminVerification);

                        _db.SaveChanges();

                        bool isBorrower = objaccount.InvestorType == null ? true : false;

                        if (objaccount.AdminVerification != AdminVerification.Pending.ToString())
                            _IEmailSender.OnUserVerificationChanged(tbluser.UserName, objaccount.AdminVerification, isBorrower);
                    }
                }

                var isNew = false;
                var verification = _db.tbl_UserVerification.FirstOrDefault(u => u.UserID == objaccount.UseriD);
                if (verification == null)
                {
                    isNew = true;
                    verification = new tbl_UserVerification();
                }

                verification.Bank = objaccount.BankVerification;
                verification.BankAccountNumber = objaccount.BankAccountNumberVerification;
                verification.BankAccountName = objaccount.BankAccountNameVerification;
                verification.BankNumber = objaccount.BankNumberVerification;
                verification.BranchName = objaccount.BranchNameVerification;
                verification.BranchNumber = objaccount.BranchNumberVerification;
                verification.FirstName = objaccount.FirstNameVerification;
                verification.Nationality = objaccount.NationalityVerification;
                verification.NricNumber = objaccount.NricNumberVerification;
                verification.PassportNumber = objaccount.PassportNumberVerification;
                verification.BussinessName = objaccount.BussinessNameVerification;
                verification.BussinessRegistrationNumber = objaccount.BussinessRegistrationNumberVerification;
                verification.BussinessOrganisation = objaccount.BussinessOrganisationVerification;
                verification.SiccCode = objaccount.SiccCodeVerification;
                verification.DateofIncorporation = objaccount.DateofIncorporationVerification;

                var regAddVer =
                    ContollerExtensions.AppendAddressField(
                        GetVerificationSymbol(objaccount.RegisteredAddress1Verification),
                        GetVerificationSymbol(objaccount.RegisteredAddress2Verification),
                        GetVerificationSymbol(objaccount.RegisteredAddressPostVerification));
                verification.RegisteredAddress = regAddVer;

                var busAddVer =
                    ContollerExtensions.AppendAddressField(
                        GetVerificationSymbol(objaccount.BussinessMailingAddress1Verification),
                        GetVerificationSymbol(objaccount.BussinessMailingAddress2Verification),
                        GetVerificationSymbol(objaccount.BussinessMailingAddressPostVerification));
                verification.BussinessMailingAddress = busAddVer;

                verification.DirectLine =
                    userObj.InvestorType == "Individual"
                        ? objaccount.MainContactNumberVerification
                        : objaccount.DirectLineVerification;
                verification.Mobile = objaccount.MobileVerification;
                verification.OptinalContact = objaccount.OfficialCorrespondenceEmailVerification;
                verification.RoleOrJobTitle = objaccount.PositionHeldVerification;
                verification.DisplayName = objaccount.DisplayNameVerification;
                verification.PaidUpCapital = objaccount.PaidUpCapitalVerification;
                verification.DOB = objaccount.DoBVerification;
                verification.Gender = objaccount.GenderVerification;

                var resAddVer =
                    ContollerExtensions.AppendAddressField(
                        GetVerificationSymbol(objaccount.ResidentialAddress1Verification),
                        GetVerificationSymbol(objaccount.ResidentialAddress2Verification),
                        GetVerificationSymbol(objaccount.ResidentialAddressPostVerification));
                verification.ResidentialAddress = resAddVer;

                verification.PostalCode = objaccount.PostalCodeVerification;
                verification.Email = objaccount.EmailVerification;
                verification.CommentsOnFieild = objaccount.AdminCommentOnUserData;
                verification.CommentsOnDocuments = objaccount.AdminCommentonDocuments;
                verification.AdminPersonelComment = objaccount.AdminPersonelComments;
                verification.UserID = objaccount.UseriD;
                verification.DateCreated = DateTime.UtcNow;

                if (isNew)
                {
                    _db.tbl_UserVerification.Add(verification);

                }
                _db.SaveChanges();

                #region Update details

                var details = _db.tbl_AccountDetails.FirstOrDefault(l => l.User_ID == objaccount.UseriD);
                const string auditMsg = @"Update Account Details";

                var isNewAccount = false;
                if (details == null)
                {
                    isNewAccount = true;
                    details = new tbl_AccountDetails();
                    details.User_ID = userObj.UserID;
                }

                //if (details != null)
                //{
                details.Bank_Id = objaccount.BankId;
                details.BankNumber = objaccount.BankNumber;
                details.BranchName = objaccount.BranchName;
                details.BranchNumber = objaccount.BranchNumber;
                details.AccountNumber = objaccount.BankAccountNumber;
                details.AccountName = objaccount.BankAccountName;
                details.FirstName = objaccount.FirstName;
                details.Nationality = objaccount.Nationality;
                details.NRIC_Number = objaccount.NricNumber;
                details.PassportNumber = objaccount.PassportNumber;
                details.BusinessName = objaccount.BusinessName;
                details.IC_CompanyRegistrationNumber = objaccount.BusinessRegistrationnumber;
                details.BusinessOrganisation = objaccount.BusinessOrganisation;
                details.SiccCode = objaccount.SiccCode;
                details.DateofIncorporation = objaccount.DateofIncorporation;
                details.PaidUpCapital = objaccount.PaidUpCapital;

                var regAdd = ContollerExtensions.AppendAddressField(objaccount.RegisteredAddress1,
                                                                    objaccount.RegisteredAddress2,
                                                                    objaccount.RegisteredPostalCode);
                var mailingAdd = ContollerExtensions.AppendAddressField(objaccount.BusinessMailingAddress1,
                                                                        objaccount.BusinessMailingAddress2,
                                                                        objaccount.BusinessMailingPostalCode);

                details.RegisteredAddress = regAdd;
                details.BusinessMailingAddress = mailingAdd;
                details.Main_OfficeContactnumber = objaccount.DirectLine ?? objaccount.MainContactNumber;
                details.Mobilenumber = objaccount.Mobilenumber;
                details.RoleJobTitle = objaccount.PositionHeld;
                details.DisplayName = objaccount.DisplayName;
                if (objaccount.PaidUpCapital > 0)
                    details.PaidUpCapital = objaccount.PaidUpCapital;
                details.DateofBirth = objaccount.DateofBirth;
                details.Gender = objaccount.Gender;

                var resAddress = ContollerExtensions.AppendAddressField(objaccount.ResidentialAddress1,
                                                                                            objaccount.ResidentialAddress2,
                                                                                            objaccount.ResidentialPostalCode);

                details.ResidentialAddress = resAddress;

                if (userObj.UserName != objaccount.Emailaddress)
                {
                    var proxy = new UserCommonOperations();
                    string msg;
                    proxy.UpdateUserEmail((long)objaccount.UseriD, objaccount.Emailaddress, out msg, false);
                }

                if (isNewAccount)
                {
                    _db.tbl_AccountDetails.Add(details);
                }
                
                _db.SaveChanges();

                // Create audit records for changed fields
                ContollerExtensions.InsertAuditRecords(details, _db, auditMsg, adminUser, null, details.AccountId);

                var firstQues =
                    _db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == objaccount.UseriD).ToList()[0];

                var accDetTemp = _db.tbl_AccountDetails.SingleOrDefault(ac => ac.User_ID == objaccount.UseriD);
                long? accIdTemp = accDetTemp != null ? accDetTemp.AccountId : 0;

                if (firstQues.Question_Id != objaccount.SecurityQuestion1)
                {
                    MoolahLogManager.LogAdminAudit(auditMsg, adminUser, null, accIdTemp, "Sequreity question 1",
                        Convert.ToString(firstQues.Question_Id), objaccount.SecurityQuestion1.ToString());
                    var objq = _db.tbl_SecurityQuestionsForUsers.FirstOrDefault(l => l.RecId == firstQues.RecId);
                    objq.Question_Id = objaccount.SecurityQuestion1;
                    objq.Answer = objaccount.SecurityAnswer1;
                    _db.SaveChanges();
                }

                var secondQues =
                    _db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == objaccount.UseriD).ToList()[1];

                if (secondQues.Question_Id != objaccount.SecurityQuestion2)
                {
                    MoolahLogManager.LogAdminAudit(auditMsg, adminUser, null, accIdTemp, "Sequreity question 2",
                        Convert.ToString(secondQues.Question_Id), objaccount.SecurityQuestion2.ToString());
                    var objq = _db.tbl_SecurityQuestionsForUsers.FirstOrDefault(l => l.RecId == secondQues.RecId);
                    objq.Question_Id = objaccount.SecurityQuestion2;
                    objq.Answer = objaccount.SecurityAnswer2;
                    _db.SaveChanges();
                }

                var thirdQues =
                    _db.tbl_SecurityQuestionsForUsers.Where(l => l.User_ID == objaccount.UseriD).ToList()[2];

                if (thirdQues.Question_Id != objaccount.SecurityQuestion3)
                {
                    MoolahLogManager.LogAdminAudit(auditMsg, adminUser, null, accIdTemp, "Sequreity question 3",
                        Convert.ToString(thirdQues.Question_Id), objaccount.SecurityQuestion3.ToString());
                    var objq = _db.tbl_SecurityQuestionsForUsers.FirstOrDefault(l => l.RecId == thirdQues.RecId);
                    objq.Question_Id = objaccount.SecurityQuestion3;
                    objq.Answer = objaccount.SecurityAnswer3;
                    _db.SaveChanges();
                }
                //}

                #endregion
            }
            catch (Exception exception)
            {
                MoolahLogManager.LogException(exception);
            }
        }

        public static string GetVerificationPhrase(string symbol)
        {
            if (symbol == "1")
            {
                return "Verified";
            }

            if (symbol == "2")
            {
                return "Unverified";
            }

            return symbol == "3" ? "Undisclosed" : "Unverified";
        }

        public static string GetVerificationSymbol(string phrase)
        {
            if (phrase == "Verified")
            {
                return "1";
            }

            if (phrase == "Unverified")
            {
                return "2";
            }

            return phrase == "Undisclosed" ? "3" : "2";
        }
    }
}